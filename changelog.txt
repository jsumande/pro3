Version 3.4.5 -
	Fixed compatible issue with Walker_Nav_Menu
	Fixed javascript issue with media upload in product edit page
	Fixed responsive issue in Theme Options
	Fixed query posts issue in taxonomy page
	Added Pin-it button to product grid
	Added new gallery layout.
	Allow more than 10 product images
	Updated MP Premium Features to version 1.0.7
	Updated CMB to version 1.2

Version 3.4 -
	Fully Compatible with WordPress 3.9
	New blog settings in Theme Options
	Added more settings added in Blog Page template
	Added "Shop Now" feature to global listing
	Added new product page layout
	New settings interface in Page Builder Block
	Ability to add product buy button into pricing package block
	Ability to disable image link in Image Block
	Fixed session issue with MarketPress 2.9+ and older version of PHP.
    Fixed SSL issue
    Fixed site restore issue in product grid.
    Fixed empty image issue in features block.
    Fixed minor css conflicts.
    Added filter hook to buy button in global listing.
Version 3.3 -
	Built-in MP Premium Features Plugin.
	Added options to enable sidebar for Product Taxonomy Page.
	Added sidebar settings for archive & search page.
	Added three-level menu support & ability to add icons into menu.
	Added caption into prettyphoto
	Fixed translation issue.
	Fixed overlapping posts issue in blog page.
	Fixed responsive issue in shopping cart page.

Version 3.2.1 -
	Fixed masonary issue in default blog view.
	Fixed search page pagination

Version 3.2.0 -
	Page Builder
		- Added preset templates
		- Added advanced options into column block
	Fully compatible with wordpress 3.8

Version 3.1.0 -
	Header section
		- added function mpt_load_header_style for loading header layout
		- added two additional header style
	Page Builder
		- Updated to version 1.1.2
		- Updated icons option with Awesome Fonts (http://fortawesome.github.io/Font-Awesome/)
		- New template selector menu
		- Added duplicate template function
		- fixed css conflicts between paga builder and bootstrap
		- added new "Post Grid" block
	Product Listing
		- Making product thumbnail clickable in mobile & tablet mode
		- Use default image when no product image uploaded
	Product Page
		- CustomPress integration
		- Hide comments tab if disabled
		- option to show / hide product short description

Version 3.0.5 - 
	Fixed Call To Action block issue
	Compatible with WP 3.7
	Added ability to hide product price in all level (only available in product listing mode)

Version 3.0.1 - 
	Fixed problems with global related products functions
	Fixed "Choose Options" button issue.

Version 3.0.0 -
	Making the theme standalone - no longer depend on flexmarket
	Fixed translation issue for certain texts.
	Update bootstrap framework to version 2.3.2
	Ultilize wp_enqueue_style to load theme css files
	New Social Icon structure
	rewrite wp blog functions.
	Updated Metaboxes functions with the new media uploader
	Integration with KickAssSlider
	Fixed deprecated functions in wp 3.6
	Compatible with WP 3.6
	Added product page settings in theme options panel
	Added Awesome Fonts (http://fortawesome.github.io/Font-Awesome/)
	Added Animate.css (http://daneden.me/animate)

Version 2.0.0 -
	Added wishlist feature
	Added Product Comparison feature
	Added Image Taxonomies menu to category / tag page.
	Added Image Taxonomies widget & block
	Added Cross Selling feature.
	Added filters to product_price and buy_button functions
	Added two new color skin - grey & purple skin
	Added hooks across the theme for better customization

Version 1.4.0 -
	Added Floating Menu
	Rewrite floating cart code
	Added Global Shop Listing Button

Version 1.3.6 -
	Floating Cart - added cart total amount

Version 1.3.5 -
	MP Dynamic Grid Integration.

Version 1.3.0 - 
	Added 'Show Total Items in Cart' feature
	Fully Compatible with MarketPress 2.8
	Make metaboxes function pluggable
	Fixed double listing in Related Products section

Version 1.2.0 - 
	Added Advance Sort feature

Version 1.1.2 - 
	Fixed product image error in safari

Version 1.1.1 - 
	Fixed double product image issue

Version 1.1 - 
	Added Sticky Nav Menu feature
	Added Floating cart settings in Theme Options Panel
	Added function - pro_list_product_in_grid
	Added link to product title in mobile and tablet view
	Added carousel nav for product image
	Fixed product grid block categories menu issue

version 1.0 - 22th Dec 2012

	first release