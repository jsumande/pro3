<div id="buddypress"> <!-- buddypress/memebers/single/home.php -->

	<?php do_action( 'bp_before_member_home_content' ); ?>

	<div id="item-header" role="complementary" class="page-content" style="margin-left: -11px; margin-right: -11px;">

		<?php bp_get_template_part( 'members/single/member-header' ) ?>		
		
		<div id="item-nav" class="row">
			<div class="item-list-tabs no-ajax col-md-9 col-md-offset-2 msgs-menu" id="object-nav" role="navigation" style="">
				<ul>
					<?php tp_bp_get_displayed_user_nav(); ?>
					<?php do_action( 'bp_member_options_nav' ); ?>
				</ul>
			</div>
		</div><!-- #item-nav -->
		
	</div><!-- #item-header -->
	<?php
	if ( bp_is_user_messages() ) {
	?>
		<style>
		#buddypress #item-body .item-list-tabs ul {
		  margin-top: 20px;
		}
		#buddypress div#message {			
			margin: 0 0 -15px;
		}
		</style>
	<?php
	}
	?>
	<div id="item-body" role="main" class="page-content" style="margin-left: -11px; margin-right: -11px; padding-bottom: -11px;">
		<?php do_action( 'template_notices' ); ?>
		
		<?php do_action( 'bp_before_member_body' );

		if ( bp_is_user_activity() || !bp_current_component() ) :
			bp_get_template_part( 'members/single/activity' );

		elseif ( bp_is_user_blogs() ) :
			bp_get_template_part( 'members/single/blogs'    );

		elseif ( bp_is_user_friends() ) :
			bp_get_template_part( 'members/single/friends'  );

		elseif ( bp_is_user_groups() ) :
			bp_get_template_part( 'members/single/groups'   );

		elseif ( bp_is_user_messages() ) :
			bp_get_template_part( 'members/single/messages' );

		elseif ( bp_is_user_profile() ) :
			bp_get_template_part( 'members/single/profile'  );

		elseif ( bp_is_user_forums() ) :
			bp_get_template_part( 'members/single/forums'   );

		elseif ( bp_is_user_notifications() ) :
			bp_get_template_part( 'members/single/notifications' );

		elseif ( bp_is_user_settings() ) :
			bp_get_template_part( 'members/single/settings' );

		// If nothing sticks, load a generic template
		else :
			bp_get_template_part( 'members/single/plugins'  );

		endif;

		do_action( 'bp_after_member_body' ); ?>

	</div><!-- #item-body -->

	<?php do_action( 'bp_after_member_home_content' ); ?>

</div><!-- #buddypress -->
