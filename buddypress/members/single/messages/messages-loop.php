<?php do_action( 'bp_before_member_messages_loop' ); ?>

<?php if ( bp_has_message_threads( bp_ajax_querystring( 'messages' ) ) ) : ?>

	<?php do_action( 'bp_after_member_messages_pagination' ); ?>

	<?php do_action( 'bp_before_member_messages_threads'   ); ?>

	<table id="message-threads" class="messages-notices">
		<?php while ( bp_message_threads() ) : bp_message_thread(); ?>

			<tr id="m-<?php bp_message_thread_id(); ?>" class="<?php bp_message_css_class(); ?><?php if ( bp_message_thread_has_unread() ) : ?> unread<?php else: ?> read<?php endif; ?>">
				<!--<td width="1%" class="thread-count">
					<span class="unread-count"><?php bp_message_thread_unread_count(); ?></span>
				</td>-->
				<td width="1%" class="thread-avatar"><?php bp_message_thread_avatar(); ?></td>

				<?php if ( 'sentbox' != bp_current_action() ) : ?>
					<td width="30%" class="thread-from">
						<?php _e( 'From:', 'buddypress' ); ?> <?php bp_message_thread_from(); ?><br />
						<span class="activity"><?php bp_message_thread_last_post_date(); ?></span>
					</td>
				<?php else: ?>
					<td width="30%" class="thread-from">
						<?php _e( 'To:', 'buddypress' ); ?> <?php bp_message_thread_to(); ?><br />
						<span class="activity"><?php bp_message_thread_last_post_date(); ?></span>
					</td>
				<?php endif; ?>

				<td width="50%" class="thread-info">
					<?php $thread = __( bp_message_thread_subject(), "buddypress" ); ?>
					<p><a href="<?php bp_message_thread_view_link(); ?>" title="<?php esc_attr_e( "View Message", "buddypress" ); ?>"><?php echo $thread; ?></a></p>
					<p class="thread-excerpt <?php echo $thread; ?>"><?php bp_message_thread_excerpt(); ?></p>
				</td>

				<?php do_action( 'bp_messages_inbox_list_item' ); ?>

				<td width="13%" class="thread-options">
					<?php /*<!-- <input type="checkbox" name="message_ids[]" value="<?php bp_message_thread_id(); ?>" /> -->
					<!--<div class="input-control-checkbox">
						<input type="checkbox" name="message_ids[]" value="<?php bp_message_thread_id(); ?>" >
						<label>&nbsp;</label>
					</div>
					<a class="button confirm" href="<?php bp_message_thread_delete_link(); ?>" title="<?php esc_attr_e( "Delete Message", "buddypress" ); ?>"><?php _e( 'Delete', 'buddypress' ); ?></a> &nbsp;-->
					*/ ?>
					
					<div class="input-control-checkbox">
						<input type="checkbox" name="message_ids[]" value="<?php bp_message_thread_id(); ?>" >
						<label><a class="button confirm" href="<?php bp_message_thread_delete_link(); ?>" title="<?php esc_attr_e( "Delete Message", "buddypress" ); ?>" style="font-size: 14px; vertical-align: sub;"><?php _e('Delete','buddypress'); ?></a> &nbsp;</label>
					</div>					
				</td>
			</tr>

		<?php endwhile; ?>
	</table><!-- #message-threads -->
	
	<div class="row" style="margin-top: 8px;">
		<div class="col-md-6">
			<div class="pagination no-ajax" id="user-pag">

				<div class="pag-count" id="messages-dir-count">
					<?php bp_messages_pagination_count(); ?>
				</div>

				<div class="pagination-links" id="messages-dir-pag">
					<?php bp_messages_pagination(); ?>
				</div>

			</div><!-- .pagination -->
		</div>
		<div class="col-md-6">
			<?php if(ICL_LANGUAGE_CODE=='bg') { ?><style> #buddypress div.messages-options-nav { width: 64% !important; }  </style> <?php } ?>
			<div id="msg" class="messages-options-nav pull-right">
				<?php bp_messages_options(); ?>
			</div><!-- .messages-options-nav -->		
		</div>
	</div>

	<?php do_action( 'bp_after_member_messages_threads' ); ?>

	<?php do_action( 'bp_after_member_messages_options' ); ?>

<?php else: ?>

	<div id="message" class="info no-msg" style="position: relative;  margin-top: 0px !important;">
		<p><?php _e( 'Sorry, no messages were found.', 'buddypress' ); ?></p>
	</div>

<?php endif;?>

<?php do_action( 'bp_after_member_messages_loop' ); ?>
