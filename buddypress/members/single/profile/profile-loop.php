<?php do_action( 'bp_before_profile_loop_content' ); ?>

<?php if ( bp_has_profile() ) : ?>

	<?php while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

		<?php if ( bp_profile_group_has_fields() ) : ?>

			<?php do_action( 'bp_before_profile_field_content' ); ?>
			<div class="row">
				<div class="col-md-12 f-bottom">
					<div class="bp-widget <?php bp_the_profile_group_slug(); ?>">
					<?php
						if(ICL_LANGUAGE_CODE=='bg') {
							$labelAP = 'Профил акаунт';
						} else {
							$labelAP = 'Account profile';
						}
					?>

						<h4><?php echo $labelAP; ?></h4>
						<table class="profile-fields table table-bordered">

							<?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>

								<?php if ( bp_field_has_data() ) : ?>
									<?php
										if(ICL_LANGUAGE_CODE=='bg') {
											ob_start();
												bp_the_profile_field_name();
												$labelName = ob_get_contents(); 
											ob_end_clean();
										} else {
											$labelName = 'Name';
										}
									?>
									<tr<?php bp_field_css_class(); ?>>

										<td class="label"><label class="base-label pull-left"><?php echo $labelName; ?></label></td>

										<td class="data"><?php bp_the_profile_field_value(); ?></td>

									</tr>

								<?php endif; ?>

								<?php do_action( 'bp_profile_field_item' ); ?>

							<?php endwhile; ?>

						</table>
					</div>
				</div>
			</div>

			<?php do_action( 'bp_after_profile_field_content' ); ?>

		<?php endif; ?>

	<?php endwhile; ?>

	<?php do_action( 'bp_profile_field_buttons' ); ?>

<?php endif; ?>

<?php do_action( 'bp_after_profile_loop_content' ); ?>
