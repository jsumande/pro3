$(document).ready(function() {
    $('#send_message_form').formValidation({
        framework: 'bootstrap',
        fields: {
            'send-to-input': {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.sendToRq
                    }				
                }
            },	            
			subject: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.subjectRq
                    }			
                }
            },
			content: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.messageRq
                    }					
                }
            }		
        }
    });
});