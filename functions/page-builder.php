<?php 

	if ( !class_exists( 'PRO_Page_Builder_Custom' ) ) {

		class PRO_Page_Builder_Custom {

		    private $theme_path;
		    private $theme_url;
		    private $theme_slug;
		    private $filter_hook_prefix;

		    function __construct() 
		    {
		        $this->theme_path = get_template_directory();
		        $this->theme_url = get_template_directory_uri();
		        $this->theme_slug = 'pro';
		        $this->filter_hook_prefix = 'pro_pb_custom_func_';
		        $this->preset_templates = apply_filters( 'pro_aqpb_preset_templates_file' , $this->theme_path. '/functions/page-builder-preset-templates.php' );

		    	// register CSS & JS
		    	add_action('aq-page-builder-admin-enqueue', array(&$this, 'register_admin_css_style') , 999 );
		    	add_action('wp_enqueue_scripts', array(&$this, 'register_js') , 999 );

				// Duplicate function
		    	add_action( 'admin_action_pro_duplicate_aqpb_template', array(&$this, 'duplicate_template' ) , 15 );

				// preset template functions
		    	add_action( 'aqpb_create_template_hook', array(&$this, 'create_preset_template' ) , 15 );
		    	
		    	
		    	$this->init();
		    }

			/* Custom CSS & JS
			------------------------------------------------------------------------------------------------------------------- */

		    function register_admin_css_style() {
		    	wp_enqueue_style('aqpb-admin-font-awesome', $this->theme_url . '/css/font-awesome.min.css', null, '3.2.1');
				wp_enqueue_style('aqpb-custom-admin-css', $this->theme_url . '/css/page-builder-admin.css', null, null);
				//wp_enqueue_script('aqpb-custom-admin-js', $this->theme_url . '/js/aqpb_custom_admin.js', array('jquery'));
		    }

		    function register_js() {
		    	wp_dequeue_script('aqpb-view-js');
		    }

			/* Init functions
			------------------------------------------------------------------------------------------------------------------- */

			function init() {

				/* Unregister default blocks
				------------------------------------------------------------------------------------------------------------------- */
				aq_unregister_block('AQ_Text_Block');
				aq_unregister_block('AQ_Column_Block');
				aq_unregister_block('AQ_Clear_Block');
				aq_unregister_block('AQ_Widgets_Block');
				aq_unregister_block('AQ_Alert_Block');
				aq_unregister_block('AQ_Tabs_Block');

				/* Register Blocks
				------------------------------------------------------------------------------------------------------------------- */
				aq_register_block('AQ_Text_Block');
				aq_register_block('AQ_Heading_Block');
				aq_register_block('AQ_Separator_Block');
				aq_register_block('AQ_Button_Block');
				aq_register_block('AQ_Image_Block');
				aq_register_block('AQ_Video_Block');
				aq_register_block('AQ_Column_Block');
				aq_register_block('AQ_Well_Block');
				aq_register_block('AQ_List_Block');
				aq_register_block('AQ_Table_Block');
				aq_register_block('AQ_CTA_Block');
				aq_register_block('AQ_Alert_Block');
				aq_register_block('AQ_Tabs_Block');
				aq_register_block('AQ_Progress_Block');
				aq_register_block('AQ_Features_Block');
				aq_register_block('AQ_Staff_Block');
				aq_register_block('AQ_Pricing_Block');
				aq_register_block('AQ_Blog_Updates_Block');
				aq_register_block('AQ_Post_Grid_Block');
				aq_register_block('AQ_Map_Block');
				aq_register_block('AQ_Contact_Block');
				aq_register_block('AQ_Testimonials_Block');
				aq_register_block('AQ_Widgets_Block');	
				aq_register_block('AQ_Shortcode_Block');
				if ( class_exists('RevSlider_Widget') ) {
					aq_register_block('AQ_Revslider_Block');
				}
				if ( class_exists('MarketPress') ) {
					aq_register_block('AQ_MP_Product_Grid_Block');
					aq_register_block('AQ_MP_Image_Taxonomies_Block');
					aq_register_block('AQ_MP_Product_Carousel_Block');
					aq_register_block('AQ_MPcart_Block');
				}

			}

			/* Duplicate template function
			 * reference: http://rudrastyh.com/wordpress/duplicate-post.html
			------------------------------------------------------------------------------------------------------------------- */

			function duplicate_template() {

				global $wpdb;

				if (! ( isset( $_GET['template']) || isset( $_POST['template'])  || ( isset($_REQUEST['action']) && 'pro_duplicate_aqpb_template' == $_REQUEST['action'] && check_admin_referer( 'duplicate-template', '_wpnonce' ) ) ) ) {
					wp_die( __( 'No template to duplicate has been supplied!' , $this->theme_slug ) );
				}

				$template_id = ( isset($_GET['template']) ? intval($_GET['template']) : intval($_POST['template']) );
				$template = get_post( $template_id );

				$current_user = wp_get_current_user();
				$new_template_author = $current_user->ID;

				if ( isset( $template ) && $template != null ) {

					$new_template_args = array(
						'comment_status' => $template->comment_status,
						'ping_status'    => $template->ping_status,
						'post_author'    => $new_template_author,
						'post_parent'    => $template->post_parent,
						'post_status'    => 'publish',
						'post_title'     => $template->post_title . ' (copy)',
						'post_type'      => $template->post_type,
						'menu_order'     => $template->menu_order
					);

					$new_template_id = wp_insert_post( $new_template_args );

					$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$template_id");
					if ( count($post_meta_infos)!=0 ) {
						$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
						foreach ($post_meta_infos as $meta_info) {
							$meta_key = $meta_info->meta_key;
							$meta_value = addslashes($meta_info->meta_value);
							$sql_query_sel[]= "SELECT $new_template_id, '$meta_key', '$meta_value'";
						}
						$sql_query.= implode(" UNION ALL ", $sql_query_sel);
						$wpdb->query($sql_query);
					}

					wp_redirect( admin_url( 'themes.php?page=aq-page-builder&action=edit&template=' . $new_template_id ) );
					exit;

				} else {
					wp_die( __( 'Template creation failed, could not find original post: ' . $template_id , $this->theme_slug ) );
				}

			}

			/* Retrieve meta info from template
			------------------------------------------------------------------------------------------------------------------- */

			function retrieve_meta_info( $template_id = NULL , $unserialize = false ) {

				global $id;
				$template_id = ( NULL === $template_id ) ? $id : $template_id;

				$meta_value = get_post_meta( $template_id );

				$output = '';

				if ( !empty($meta_value) ) {
					foreach ($meta_value as $key => $value) {
						if ( $unserialize ) {
							$output .= $key . ' = ' . print_r( $value , true ) . '<br /><br />';
						} else {
							$meta = get_post_meta( $template_id , $key , true );
							$output .= $key . ' = ' . print_r( $meta , true ) . '<br />';
						}
					}
				}

				return $output;

			}

			/* Preset Template
			------------------------------------------------------------------------------------------------------------------- */

			function select_preset_template_html() {

				require_once( $this->preset_templates );

				$output = '';

				if ( !empty($preset_templates) ) {
					$output .= '<label for="preset_template">'.__( 'Preset Template:', $this->theme_slug ).'</label>';
					$output .= '<select id="preset_template" name="preset_template">';
						$output .= '<option value="">' . __( 'None' , $this->theme_slug ) . '</option>';
						foreach ($preset_templates as $template_slug => $template_data ) {
							$output .= '<option value="'.$template_slug.'">' . $template_data['name'] . '</option>';
						}
						
					$output .= '</select>';
				}

				return apply_filters( $this->filter_hook_prefix . 'select_preset_template_html' , $output );
			}

			function create_preset_template( $args = array() ) {

				$defaults = array(
					'template_id' => NULL,
					'selected_template' =>  NULL,
				);

				$instance = wp_parse_args( $args, $defaults );
				extract( $instance );

				require_once( $this->preset_templates );
				
				if ( !empty($template_id) && !empty($selected_template) && !empty($preset_templates) ) {

					foreach ($preset_templates as $template_slug => $template_data ) {

						if ( $selected_template == $template_slug && !empty($template_data['contents']) ) {

							foreach ($template_data['contents'] as $meta_key => $meta_value) {
								add_post_meta( $template_id, $meta_key, $meta_value );
							} // end foreach

						} // end ifelse

					} // end - foreach

				} // end ifelse
				
			}

			/* Icon list
			------------------------------------------------------------------------------------------------------------------- */

		    function load_icon_select_field( $field_id = '' , $block_id = '' , $selected = '' , $custom_block_id = '' , $custom_block_name = '' ) {

				$icon_options = $this->load_awesome_icon_list();
				$output = '<div class="aq-icon-select-field-container">';
					$output .= ( !empty($selected) ? '<div class="aq-fawesome-sample-container"><i class="'. $selected .' aq-fawesome-sample"></i></div>' : '<div class="aq-fawesome-sample-container"></div>' );
					$output .= '<select id="'. ( !empty($custom_block_id) ? $custom_block_id : $block_id .'_'.$field_id ) .'" name="'.( !empty($custom_block_name) ? $custom_block_name : 'aq_blocks['.$block_id.']['.$field_id.']' ).'" class="aq-awesome-icon-select-field">';
					foreach($icon_options as $key => $value) {
						$output .= '<option value="'.$key.'" '.selected( $selected, $key, false ).'>'.htmlspecialchars($value).'</option>';
					}
					$output .= '</select>';
				$output .= '</div>'; // end - aq-icon-select-field-container

		    	return apply_filters( $this->filter_hook_prefix . 'load_icon_select_field' , $output , $field_id, $block_id, $selected , $custom_block_id , $custom_block_name );
		    }

			function load_awesome_icon_list() {

		    	$awesome_fonts = array(
					'none' => 'none',
					'icon-adjust' => 'adjust',
					'icon-adn' => 'adn',
					'icon-align-center' => 'align-center',
					'icon-align-justify' => 'align-justify',
					'icon-align-left' => 'align-left',
					'icon-align-right' => 'align-right',
					'icon-ambulance' => 'ambulance',
					'icon-anchor' => 'anchor',
					'icon-android' => 'android',
					'icon-angle-down' => 'angle-down',
					'icon-angle-left' => 'angle-left',
					'icon-angle-right' => 'angle-right',
					'icon-angle-up' => 'angle-up',
					'icon-apple' => 'apple',
					'icon-archive' => 'archive',
					'icon-arrow-down' => 'arrow-down',
					'icon-arrow-left' => 'arrow-left',
					'icon-arrow-right' => 'arrow-right',
					'icon-arrow-up' => 'arrow-up',
					'icon-asterisk' => 'asterisk',
					'icon-backward' => 'backward',
					'icon-ban-circle' => 'ban-circle',
					'icon-bar-chart' => 'bar-chart',
					'icon-barcode' => 'barcode',
					'icon-beaker' => 'beaker',
					'icon-beer' => 'beer',
					'icon-bell' => 'bell',
					'icon-bell-alt' => 'bell-alt',
					'icon-bitbucket' => 'bitbucket',
					'icon-bitbucket-sign' => 'bitbucket-sign',
					'icon-bold' => 'bold',
					'icon-bolt' => 'bolt',
					'icon-book' => 'book',
					'icon-bookmark' => 'bookmark',
					'icon-bookmark-empty' => 'bookmark-empty',
					'icon-briefcase' => 'briefcase',
					'icon-btc' => 'btc',
					'icon-bug' => 'bug',
					'icon-building' => 'building',
					'icon-bullhorn' => 'bullhorn',
					'icon-bullseye' => 'bullseye',
					'icon-calendar' => 'calendar',
					'icon-calendar-empty' => 'calendar-empty',
					'icon-camera' => 'camera',
					'icon-camera-retro' => 'camera-retro',
					'icon-caret-down' => 'caret-down',
					'icon-caret-left' => 'caret-left',
					'icon-caret-right' => 'caret-right',
					'icon-caret-up' => 'caret-up',
					'icon-certificate' => 'certificate',
					'icon-check' => 'check',
					'icon-check-empty' => 'check-empty',
					'icon-check-minus' => 'check-minus',
					'icon-check-sign' => 'check-sign',
					'icon-chevron-down' => 'chevron-down',
					'icon-chevron-left' => 'chevron-left',
					'icon-chevron-right' => 'chevron-right',
					'icon-chevron-sign-down' => 'chevron-sign-down',
					'icon-chevron-sign-left' => 'chevron-sign-left',
					'icon-chevron-sign-right' => 'chevron-sign-right',
					'icon-chevron-sign-up' => 'chevron-sign-up',
					'icon-chevron-up' => 'chevron-up',
					'icon-circle' => 'circle',
					'icon-circle-arrow-down' => 'circle-arrow-down',
					'icon-circle-arrow-left' => 'circle-arrow-left',
					'icon-circle-arrow-right' => 'circle-arrow-right',
					'icon-circle-arrow-up' => 'circle-arrow-up',
					'icon-circle-blank' => 'circle-blank',
					'icon-cloud' => 'cloud',
					'icon-cloud-download' => 'cloud-download',
					'icon-cloud-upload' => 'cloud-upload',
					'icon-cny' => 'cny',
					'icon-code' => 'code',
					'icon-code-fork' => 'code-fork',
					'icon-coffee' => 'coffee',
					'icon-cog' => 'cog',
					'icon-cogs' => 'cogs',
					'icon-collapse' => 'collapse',
					'icon-collapse-alt' => 'collapse-alt',
					'icon-collapse-top' => 'collapse-top',
					'icon-columns' => 'columns',
					'icon-comment' => 'comment',
					'icon-comment-alt' => 'comment-alt',
					'icon-comments' => 'comments',
					'icon-comments-alt' => 'comments-alt',
					'icon-compass' => 'compass',
					'icon-copy' => 'copy',
					'icon-credit-card' => 'credit-card',
					'icon-crop' => 'crop',
					'icon-css3' => 'css3',
					'icon-cut' => 'cut',
					'icon-dashboard' => 'dashboard',
					'icon-desktop' => 'desktop',
					'icon-double-angle-down' => 'double-angle-down',
					'icon-double-angle-left' => 'double-angle-left',
					'icon-double-angle-right' => 'double-angle-right',
					'icon-double-angle-up' => 'double-angle-up',
					'icon-download' => 'download',
					'icon-download-alt' => 'download-alt',
					'icon-dribbble' => 'dribbble',
					'icon-dropbox' => 'dropbox',
					'icon-edit' => 'edit',
					'icon-edit-sign' => 'edit-sign',
					'icon-eject' => 'eject',
					'icon-ellipsis-horizontal' => 'ellipsis-horizontal',
					'icon-ellipsis-vertical' => 'ellipsis-vertical',
					'icon-envelope' => 'envelope',
					'icon-envelope-alt' => 'envelope-alt',
					'icon-eraser' => 'eraser',
					'icon-eur' => 'eur',
					'icon-exchange' => 'exchange',
					'icon-exclamation' => 'exclamation',
					'icon-exclamation-sign' => 'exclamation-sign',
					'icon-expand' => 'expand',
					'icon-expand-alt' => 'expand-alt',
					'icon-external-link' => 'external-link',
					'icon-external-link-sign' => 'external-link-sign',
					'icon-eye-close' => 'eye-close',
					'icon-eye-open' => 'eye-open',
					'icon-facebook' => 'facebook',
					'icon-facebook-sign' => 'facebook-sign',
					'icon-facetime-video' => 'facetime-video',
					'icon-fast-backward' => 'fast-backward',
					'icon-fast-forward' => 'fast-forward',
					'icon-female' => 'female',
					'icon-fighter-jet' => 'fighter-jet',
					'icon-file' => 'file',
					'icon-file-alt' => 'file-alt',
					'icon-file-text' => 'file-text',
					'icon-file-text-alt' => 'file-text-alt',
					'icon-film' => 'film',
					'icon-filter' => 'filter',
					'icon-fire' => 'fire',
					'icon-fire-extinguisher' => 'fire-extinguisher',
					'icon-flag' => 'flag',
					'icon-flag-alt' => 'flag-alt',
					'icon-flag-checkered' => 'flag-checkered',
					'icon-flickr' => 'flickr',
					'icon-folder-close' => 'folder-close',
					'icon-folder-close-alt' => 'folder-close-alt',
					'icon-folder-open' => 'folder-open',
					'icon-folder-open-alt' => 'folder-open-alt',
					'icon-font' => 'font',
					'icon-food' => 'food',
					'icon-forward' => 'forward',
					'icon-foursquare' => 'foursquare',
					'icon-frown' => 'frown',
					'icon-fullscreen' => 'fullscreen',
					'icon-gamepad' => 'gamepad',
					'icon-gbp' => 'gbp',
					'icon-gift' => 'gift',
					'icon-github' => 'github',
					'icon-github-alt' => 'github-alt',
					'icon-github-sign' => 'github-sign',
					'icon-gittip' => 'gittip',
					'icon-glass' => 'glass',
					'icon-globe' => 'globe',
					'icon-google-plus' => 'google-plus',
					'icon-google-plus-sign' => 'google-plus-sign',
					'icon-group' => 'group',
					'icon-hand-down' => 'hand-down',
					'icon-hand-left' => 'hand-left',
					'icon-hand-right' => 'hand-right',
					'icon-hand-up' => 'hand-up',
					'icon-hdd' => 'hdd',
					'icon-headphones' => 'headphones',
					'icon-heart' => 'heart',
					'icon-heart-empty' => 'heart-empty',
					'icon-home' => 'home',
					'icon-hospital' => 'hospital',
					'icon-h-sign' => 'h-sign',
					'icon-html5' => 'html5',
					'icon-inbox' => 'inbox',
					'icon-indent-left' => 'indent-left',
					'icon-indent-right' => 'indent-right',
					'icon-info' => 'info',
					'icon-info-sign' => 'info-sign',
					'icon-inr' => 'inr',
					'icon-instagram' => 'instagram',
					'icon-italic' => 'italic',
					'icon-jpy' => 'jpy',
					'icon-key' => 'key',
					'icon-keyboard' => 'keyboard',
					'icon-krw' => 'krw',
					'icon-laptop' => 'laptop',
					'icon-leaf' => 'leaf',
					'icon-legal' => 'legal',
					'icon-lemon' => 'lemon',
					'icon-level-down' => 'level-down',
					'icon-level-up' => 'level-up',
					'icon-lightbulb' => 'lightbulb',
					'icon-link' => 'link',
					'icon-linkedin' => 'linkedin',
					'icon-linkedin-sign' => 'linkedin-sign',
					'icon-linux' => 'linux',
					'icon-list' => 'list',
					'icon-list-alt' => 'list-alt',
					'icon-list-ol' => 'list-ol',
					'icon-list-ul' => 'list-ul',
					'icon-location-arrow' => 'location-arrow',
					'icon-lock' => 'lock',
					'icon-long-arrow-down' => 'long-arrow-down',
					'icon-long-arrow-left' => 'long-arrow-left',
					'icon-long-arrow-right' => 'long-arrow-right',
					'icon-long-arrow-up' => 'long-arrow-up',
					'icon-magic' => 'magic',
					'icon-magnet' => 'magnet',
					'icon-mail-reply-all' => 'mail-reply-all',
					'icon-male' => 'male',
					'icon-map-marker' => 'map-marker',
					'icon-maxcdn' => 'maxcdn',
					'icon-medkit' => 'medkit',
					'icon-meh' => 'meh',
					'icon-microphone' => 'microphone',
					'icon-microphone-off' => 'microphone-off',
					'icon-minus' => 'minus',
					'icon-minus-sign' => 'minus-sign',
					'icon-minus-sign-alt' => 'minus-sign-alt',
					'icon-mobile-phone' => 'mobile-phone',
					'icon-money' => 'money',
					'icon-moon' => 'moon',
					'icon-move' => 'move',
					'icon-music' => 'music',
					'icon-off' => 'off',
					'icon-ok' => 'ok',
					'icon-ok-circle' => 'ok-circle',
					'icon-ok-sign' => 'ok-sign',
					'icon-paper-clip' => 'paper-clip',
					'icon-paste' => 'paste',
					'icon-pause' => 'pause',
					'icon-pencil' => 'pencil',
					'icon-phone' => 'phone',
					'icon-phone-sign' => 'phone-sign',
					'icon-picture' => 'picture',
					'icon-pinterest' => 'pinterest',
					'icon-pinterest-sign' => 'pinterest-sign',
					'icon-plane' => 'plane',
					'icon-play' => 'play',
					'icon-play-circle' => 'play-circle',
					'icon-play-sign' => 'play-sign',
					'icon-plus' => 'plus',
					'icon-plus-sign' => 'plus-sign',
					'icon-plus-sign-alt' => 'plus-sign-alt',
					'icon-print' => 'print',
					'icon-pushpin' => 'pushpin',
					'icon-puzzle-piece' => 'puzzle-piece',
					'icon-qrcode' => 'qrcode',
					'icon-question' => 'question',
					'icon-question-sign' => 'question-sign',
					'icon-quote-left' => 'quote-left',
					'icon-quote-right' => 'quote-right',
					'icon-random' => 'random',
					'icon-refresh' => 'refresh',
					'icon-remove' => 'remove',
					'icon-remove-circle' => 'remove-circle',
					'icon-remove-sign' => 'remove-sign',
					'icon-renren' => 'renren',
					'icon-reorder' => 'reorder',
					'icon-repeat' => 'repeat',
					'icon-reply' => 'reply',
					'icon-reply-all' => 'reply-all',
					'icon-resize-full' => 'resize-full',
					'icon-resize-horizontal' => 'resize-horizontal',
					'icon-resize-small' => 'resize-small',
					'icon-resize-vertical' => 'resize-vertical',
					'icon-retweet' => 'retweet',
					'icon-road' => 'road',
					'icon-rocket' => 'rocket',
					'icon-rss' => 'rss',
					'icon-rss-sign' => 'rss-sign',
					'icon-save' => 'save',
					'icon-screenshot' => 'screenshot',
					'icon-search' => 'search',
					'icon-share' => 'share',
					'icon-share-alt' => 'share-alt',
					'icon-share-sign' => 'share-sign',
					'icon-shield' => 'shield',
					'icon-shopping-cart' => 'shopping-cart',
					'icon-signal' => 'signal',
					'icon-sign-blank' => 'sign-blank',
					'icon-signin' => 'signin',
					'icon-signout' => 'signout',
					'icon-sitemap' => 'sitemap',
					'icon-skype' => 'skype',
					'icon-smile' => 'smile',
					'icon-sort' => 'sort',
					'icon-sort-by-alphabet' => 'sort-by-alphabet',
					'icon-sort-by-alphabet-alt' => 'sort-by-alphabet-alt',
					'icon-sort-by-attributes' => 'sort-by-attributes',
					'icon-sort-by-attributes-alt' => 'sort-by-attributes-alt',
					'icon-sort-by-order' => 'sort-by-order',
					'icon-sort-by-order-alt' => 'sort-by-order-alt',
					'icon-sort-down' => 'sort-down',
					'icon-sort-up' => 'sort-up',
					'icon-spinner' => 'spinner',
					'icon-stackexchange' => 'stackexchange',
					'icon-star' => 'star',
					'icon-star-empty' => 'star-empty',
					'icon-star-half' => 'star-half',
					'icon-star-half-empty' => 'star-half-empty',
					'icon-step-backward' => 'step-backward',
					'icon-step-forward' => 'step-forward',
					'icon-stethoscope' => 'stethoscope',
					'icon-stop' => 'stop',
					'icon-strikethrough' => 'strikethrough',
					'icon-subscript' => 'subscript',
					'icon-suitcase' => 'suitcase',
					'icon-sun' => 'sun',
					'icon-superscript' => 'superscript',
					'icon-table' => 'table',
					'icon-tablet' => 'tablet',
					'icon-tag' => 'tag',
					'icon-tags' => 'tags',
					'icon-tasks' => 'tasks',
					'icon-terminal' => 'terminal',
					'icon-text-height' => 'text-height',
					'icon-text-width' => 'text-width',
					'icon-th' => 'th',
					'icon-th-large' => 'th-large',
					'icon-th-list' => 'th-list',
					'icon-thumbs-down' => 'thumbs-down',
					'icon-thumbs-down-alt' => 'thumbs-down-alt',
					'icon-thumbs-up' => 'thumbs-up',
					'icon-thumbs-up-alt' => 'thumbs-up-alt',
					'icon-ticket' => 'ticket',
					'icon-time' => 'time',
					'icon-tint' => 'tint',
					'icon-trash' => 'trash',
					'icon-trello' => 'trello',
					'icon-trophy' => 'trophy',
					'icon-truck' => 'truck',
					'icon-tumblr' => 'tumblr',
					'icon-tumblr-sign' => 'tumblr-sign',
					'icon-twitter' => 'twitter',
					'icon-twitter-sign' => 'twitter-sign',
					'icon-umbrella' => 'umbrella',
					'icon-underline' => 'underline',
					'icon-undo' => 'undo',
					'icon-unlink' => 'unlink',
					'icon-unlock' => 'unlock',
					'icon-unlock-alt' => 'unlock-alt',
					'icon-upload' => 'upload',
					'icon-upload-alt' => 'upload-alt',
					'icon-usd' => 'usd',
					'icon-user' => 'user',
					'icon-user-md' => 'user-md',
					'icon-vk' => 'vk',
					'icon-volume-down' => 'volume-down',
					'icon-volume-off' => 'volume-off',
					'icon-volume-up' => 'volume-up',
					'icon-warning-sign' => 'warning-sign',
					'icon-weibo' => 'weibo',
					'icon-windows' => 'windows',
					'icon-wrench' => 'wrench',
					'icon-xing' => 'xing',
					'icon-xing-sign' => 'xing-sign',
					'icon-youtube' => 'youtube',
					'icon-youtube-play' => 'youtube-play',
					'icon-youtube-sign' => 'youtube-sign',
					'icon-zoom-in' => 'zoom-in',
					'icon-zoom-out' => 'zoom-out',
					'icon-zoom-out' => 'zoom-out',
		    	);

				return apply_filters( $this->filter_hook_prefix . 'load_awesome_icon_list' , $awesome_fonts );
			}


		} // end - class PRO_Page_Builder_Custom

		global $propb_custom;
		$propb_custom = new PRO_Page_Builder_Custom();

	} // end - if ( !class_exists( 'PRO_Page_Builder_Custom' ) )