<?php

	// call for global product listing functions
	add_action('pro_global_marketplace_page' , 'pro_global_list_products' , 10 , 2);
	add_action('pro_global_category_page' , 'pro_global_list_products' , 10 , 2);
	add_action('pro_global_tag_page' , 'pro_global_list_products' , 10 , 2);

	add_action( 'after_setup_theme' , 'pro_register_global_widget_blocks' , 100 );

	function pro_register_global_widget_blocks() {

		$global_rules = apply_filters( 'mpt_global_widget_block_rules' , is_main_site() );

		if ( $global_rules ) {
			add_action( 'widgets_init', 'register_custom_global_widgets_pro' );

			if( class_exists('AQ_Page_Builder') ) {
				aq_register_block('AQ_MP_Global_Product_Grid_Block');
				aq_register_block('AQ_MP_Global_Image_Taxonomies_Block');
			}	
		}

		do_action( 'pro_register_global_widget_blocks' );

	}

	function pro_global_list_products( $unique_id = '' , $context = '' ){

		$btnclass = mpt_load_mp_btn_color();
		$iconclass = mpt_load_whiteicon_in_btn();
		$labelcolor = mpt_load_mp_label_color();
		$span = mpt_load_product_listing_layout();
		$counter = mpt_load_product_listing_counter();
		//$entries = get_option('mpt_mp_listing_entries');
		$entries = get_blog_option(1,'mpt_mp_listing_entries');
		$advancedsoft = mpt_enable_advanced_sort();
		$advancedsoftbtnposition = mpt_advanced_sort_btn_position();

		$args = array(
			'unique_id' => $unique_id,
			'sort' => $advancedsoft,
			'align' => $advancedsoftbtnposition,
			'context' => $context,
			'echo' => false,
			'per_page' => $entries,
			'counter' => $counter,
			'span' => $span,
			'btnclass' => $btnclass,
			'iconclass' => $iconclass,
			'labelcolor' => $labelcolor
		);

		echo apply_filters( 'func_pro_global_list_products' , pro_global_advance_product_sort( $args ) , $args );
	}

	function pro_global_advance_product_sort( $args = array() ){

		$defaults = array(
			'unique_id' => '',
			'sort' => false,
			'align' => 'align-right',
			'context' => 'list',
			'enablecustomqueries' => false,
			'customqueries' => NULL,
			'echo' => true,
			'paginate' => true,
			'page' => '',
			'per_page' => 12,
			'order_by' => 'date',
			'order' => 'DESC',
			'category' => '',
			'tag' => '',
			's' => '',
			'sentence' => '',
			'exact' => '',
			'counter' => '3',
			'span' => 'span4',
			'btnclass' => '',
			'iconclass' => '',
			'labelcolor' => '',
			'boxclass' => '',
			'boxstyle' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		if (empty($page) && get_query_var('paged')) {
			$page = intval(get_query_var('paged'));
		} elseif (empty($page) && get_query_var('page')) {
			$page = intval(get_query_var('page'));
		} elseif ( empty($page) ) {
			$page = 1;
		}

		$output = '';

		if ($sort) {

			global $mp;

			do_action( 'mpt_start_session_hook' , 'pro_global_advanced_sort' );

			if(isset($_POST['advancedsortformsubmitted'.$unique_id])) {

				if ($context == 'list') {
					$_SESSION['advancedsortcategory'.$unique_id] = (!empty($_POST['advancedsortcategory'.$unique_id])) ? esc_html(esc_attr(trim($_POST['advancedsortcategory'.$unique_id]))) : '';
				} else {
					$_SESSION['advancedsortcategory'.$unique_id] = '';
				}
				$_SESSION['advancedsortby'.$unique_id] = (!empty($_POST['advancedsortby'.$unique_id])) ? esc_html(esc_attr(trim($_POST['advancedsortby'.$unique_id]))) : '';
				$_SESSION['advancedsortdirection'.$unique_id] = (!empty($_POST['advancedsortdirection'.$unique_id])) ? esc_html(esc_attr(trim($_POST['advancedsortdirection'.$unique_id]))) : '';
			}

			$category = (!empty($_SESSION['advancedsortcategory'.$unique_id])) ? esc_html(esc_attr(trim($_SESSION['advancedsortcategory'.$unique_id]))) : $category;
			$order_by = (!empty($_SESSION['advancedsortby'.$unique_id])) ? esc_html(esc_attr(trim($_SESSION['advancedsortby'.$unique_id]))) : $order_by;
			$order = (!empty($_SESSION['advancedsortdirection'.$unique_id])) ? esc_html(esc_attr(trim($_SESSION['advancedsortdirection'.$unique_id]))) : $order;

			$output = apply_filters( 'pro_global_listing_before_advanced_sort' , $output , $unique_id , $context , $category , $tag );

			$output .= '<div id="advanced-sort" class="'.$align.'">';
				$output .= '<a href="#adv-sort" role="button" data-toggle="modal" class="btn btn-12-19'.$btnclass.'"><i class="icon-th'.$iconclass.' icon-large"></i> '.__('Advanced Sort' , 'pro').'</a>';
				$output .= '<div class="clear padding20"></div>';
			$output .= '</div>';

			// Advanced Modal
			$output .= '<div id="adv-sort" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="advancedSort" aria-hidden="true">';
				
				$output .= '<div class="modal-header">';
			    	$output .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
			    	$output .= '<h4>'.__('Advanced Sort' , 'pro').'</h4>';
				$output .= '</div>';

				$output .= '<div class="modal-body">';

					$output .= '<div class="row-fluid">';

						$output .= '<form class="form-horizontal" action="'.esc_url( $_SERVER["REQUEST_URI"] ).'#advanced-sort" id="advanced-sort-form" method="post">';

							if ($context == 'list') {

								// By Category   
								$output .= '<div class="input-prepend">';
									$output .= '<span class="add-on">'.__('By Category:' , 'pro').'</span>';
									$output .= '<select name="advancedsortcategory'.$unique_id.'" id="advancedsortcategory'.$unique_id.'">';
										$output .= '<option value="">'.__('Show All' , 'pro').'</option>';

											$categories = pro_load_global_taxonomies_list();

											if  (!empty($categories) && is_array($categories)) {
											  foreach($categories as $cat) {
											    $output .= '<option value="'.$cat['slug'].'"'.($cat['slug'] == $category ? ' selected="selected"' : '').'>'.$cat['name'].'</option>';
											  }
											}

									$output .= '</select>';
								$output .= '</div>';

								$output .= '<div class="clear padding10"></div>';

							}

							// Sort By
							$output .= '<div class="input-prepend">';
								$output .= '<span class="add-on">'.__('Sort By' , 'pro').':</span>';
								$output .= '<select name="advancedsortby'.$unique_id.'" id="advancedsortby'.$unique_id.'">';
									$output .= '<option value="date"'.($order_by == 'date' ? ' selected="selected"' : '').'>'.__('Release Date' , 'pro').'</option>';
									$output .= '<option value="title"'.($order_by == 'title' ? ' selected="selected"' : '').'>'.__('Name' , 'pro').'</option>';
									$output .= '<option value="sales"'.($order_by == 'sales' ? ' selected="selected"' : '').'>'.__('Sales' , 'pro').'</option>';
									$output .= '<option value="rand"'.($order_by == 'rand' ? ' selected="selected"' : '').'>'.__('Random' , 'pro').'</option>';
								$output .= '</select>';
							$output .= '</div>';

							$output .= '<div class="clear padding10"></div>';

							// Sort Direction
							$output .= '<div class="input-prepend">';
								$output .= '<span class="add-on">'.__('Sort Direction' , 'pro').':</span>';
								$output .= '<select name="advancedsortdirection'.$unique_id.'" id="advancedsortdirection'.$unique_id.'">';
									$output .= '<option value="DESC"'.($order == 'DESC' ? ' selected="selected"' : '').'>'.__('Descending' , 'pro').'</option>';
									$output .= '<option value="ASC"'.($order == 'ASC' ? ' selected="selected"' : '').'>'.__('Ascending' , 'pro').'</option>';
								$output .= '</select>';
							$output .= '</div>';

							$output .= '<div class="clear padding10"></div>';

							$output .= '<button type="submit" class="btn'.$btnclass.' advanced-sort-btn pull-right" data-toggle="button"><i class="icon-th'.$iconclass.'"></i> '.__('Sort' , 'pro').'</button>';

							$output .= '<input type="hidden" name="advancedsortformsubmitted'.$unique_id.'" id="advancedsortformsubmitted'.$unique_id.'" value="true" />';

						$output .= '</form>';

				    $output .= '</div>'; // row-fluid

				$output .= '</div>'; // modal body

			$output .= '</div>'; // advanced-sort

			$output = apply_filters( 'pro_global_listing_after_advanced_sort' , $output , $unique_id , $context , $category , $tag );

		}

		if ($context == 'category')
			$category = $unique_id;
		elseif ($context == 'tag') {
			$tag = $unique_id;
		}

		$output = apply_filters( 'pro_global_listing_before_grid' , $output , $unique_id , $context , $category , $tag );

			$grid_args = array(
				'enablecustomqueries' => $enablecustomqueries,
				'customqueries' => $customqueries,
				'echo' => false,
				'paginate' => $paginate,
				'page' => $page,
				'per_page' => $per_page,
				'order_by' => $order_by,
				'order' => $order,
				'category' => $category,
				'tag' => $tag,
				's' => $s,
				'sentence' => $sentence,
				'exact' => $exact,
				'counter' => $counter,
				'span' => $span,
				'btnclass' => $btnclass,
				'iconclass' => $iconclass,
				'labelcolor' => $labelcolor,
				'boxclass' => $boxclass,
				'boxstyle' => $boxstyle
			);

			$output .= pro_global_product_listing( $grid_args );

		$output = apply_filters( 'pro_global_listing_after_grid' , $output , $unique_id , $context , $category , $tag );

	  	if ($echo)
	    	echo apply_filters( 'func_pro_global_advance_product_sort' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_global_advance_product_sort' , $output , $instance );		
	}

	function pro_global_product_listing( $args = array() ){

		$defaults = array(
			'enablecustomqueries' => false,
			'customqueries' => NULL,
			'echo' => true,
			'paginate' => true,
			'page' => 1,
			'per_page' => 12,
			'order_by' => 'date',
			'order' => 'DESC',
			'category' => '',
			'tag' => '',
			's' => '',
			'sentence' => '',
			'exact' => '',
			'counter' => '3',
			'span' => 'span4',
			'btnclass' => '',
			'iconclass' => '',
			'labelcolor' => '',
			'boxclass' => '',
			'boxstyle' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

	  	global $wp_query, $mp, $wpdb;

	  	$page = (!empty($page) ? esc_attr($page) : 1);

	  	//setup taxonomy if applicable
	  	if ($category) {
		    $category = esc_sql( sanitize_title( $category ) );
		    $query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = 'product_category' AND t.slug = '$category'";
			$query_count = "SELECT COUNT(*) FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = 'product_category' AND t.slug = '$category'";
	  	} else if ($tag) {
		    $tag = esc_sql( sanitize_title( $tag ) );
		    $query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = 'product_tag' AND t.slug = '$tag'";
		    $query_count = "SELECT COUNT(*) FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = 'product_tag' AND t.slug = '$tag'";
	  	} else {
		    $query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content FROM {$wpdb->base_prefix}mp_products p WHERE p.blog_public = 1";
		    $query_count = "SELECT COUNT(*) FROM {$wpdb->base_prefix}mp_products p WHERE p.blog_public = 1";
	  	}

	  	// get search
		if ( !empty($s) ) {
			// added slashes screw with quote grouping when done early, so done later
			$s = stripslashes($s);
			if ( !empty($sentence) ) {
				$search_terms = array($s);
			} else {
				preg_match_all('/".*?("|$)|((?<=[\r\n\t ",+])|^)[^\r\n\t ",+]+/', $s, $matches);
				$search_terms = array_map('_search_terms_tidy', $matches[0]);
			}
			$n = !empty($exact) ? '' : '%';
			$search = '';
			$searchand = '';
			foreach( (array) $search_terms as $term ) {
				$term = esc_sql( like_escape( $term ) );
				$search .= "{$searchand}((p.post_title LIKE '{$n}{$term}{$n}') OR (p.post_content LIKE '{$n}{$term}{$n}'))";
				$searchand = ' AND ';
			}
			if ( !empty($search) ) {
				$query .= " AND ({$search}) ";
			}
		}

		//get order by
		switch ($order_by) {
		    case 'title':
		      	$query .= " ORDER BY p.post_title";
		      	break;

		    case 'price':
		      	$query .= " ORDER BY p.price";
		      	break;

		    case 'sales':
		      	$query .= " ORDER BY p.sales_count";
		      	break;

		    case 'rand':
		      	$query .= " ORDER BY RAND()";
		      	break;

		    case 'date':
		    default:
		      	$query .= " ORDER BY p.post_date";
		      	break;
		}

		//get order direction
		if ($order == 'ASC') {
		    $query .= " ASC";
		} else {
		    $query .= " DESC";
		}

		//get page details
	  	$query .= " LIMIT " . intval(($page-1)*$per_page) . ", " . intval($per_page);

	  	//The Query
  		if ( $enablecustomqueries && !empty( $customqueries ) ) {
  			$results = $customqueries;
  		} else {
  			$results = $wpdb->get_results( $query );
  		}

	  	$output = '<div id="pro-product-grid">';

	  	$count = 1;
	  	$incomplete_row = false;

	  	$current_blog_id = get_current_blog_id();

		if ($results) {

		    foreach ($results as $product) {

		    	$incomplete_row = true;

		    	$output .= ( $count == 1 ? '<div class="row-fluid">' : '' );

			    global $current_blog;
			    switch_to_blog($product->blog_id);

			    $single_args = array(
					'echo' => false,
					'post_id' => $product->post_id,
					'span' => $span,
					'imagesize' => 'tb-360',
					'btnclass' => $btnclass,
					'iconclass' => $iconclass,
					'labelcolor' => $labelcolor,
					'class' => $boxclass,
					'style' => $boxstyle
			    );

				$output .= pro_global_load_single_product_in_box( $single_args );
				
				if ($count == $counter) {
					$incomplete_row = false;
					$count = 0;
					$output .= '</div>'; // End - row-fluid
				}

				$count++;

				restore_current_blog();
			}

		} else {

			// If nothing found
			$output .= '<div class="page-content">';
				$output .= '<h2>'.__( 'Nothing Found.' , 'pro' ).'</h2>';
				$output .= '<p>' . __('Perhaps try one of the links below:' , 'pro' ) . '</p>';
				$output .= '<div class="padding10"></div>';
					$output .= '<h4>' . __( 'Most Used Categories' , 'pro' ) . '</h4>';
					$output .= '<ul>';
						$output .= wp_list_categories( array( 'taxonomy' => 'product_category' , 'orderby' => 'count', 'order' => 'DESC', 'show_count' => 1, 'title_li' => '', 'number' => 10 , 'echo' => 0 ) );
					$output .= '</ul>';
				$output .= '<div class="padding20"></div>';
				$output .= '<p>' . __( 'Or, use the search box below:' , 'pro' ) . '</p>';
				$output .= get_search_form( false );
			$output .= '</div>';
			
		}

		restore_current_blog();

		$after_restore_blog_id = get_current_blog_id();
		if ( $current_blog_id != $after_restore_blog_id )
			switch_to_blog( $current_blog_id );

		if ( $incomplete_row )
			$output .= '</div>'; // End - row-fluid

	  	$output .= '</div>'; // end - pro-product-grid
	  	$output .= '<div class="clear"></div>';

	  	// load pagination
	  	if ($paginate) {

		  	$output .= '<div id="pro-product-grid-pagination" class="pull-right">';

				$total_results = $wpdb->get_var( $query_count );
				$total_pages = ceil($total_results/$per_page); 

			    if ($total_pages > 1){ 

			      	$current_page = max(1, $page);

			    	// check if current page is product listing page
			    	$productlistingslug = $mp->get_setting('slugs->store'). '/'.$mp->get_setting('slugs->products');
			    	$currenturl = get_pagenum_link($current_page);
			    	$isproductlising = strstr($currenturl, $productlistingslug);

			    	// check if current page is marketplace page
			    	$globalsettings = get_site_option('mp_network_settings');
			    	$marketplaceslug = !empty($globalsettings) ? '/'.$globalsettings['slugs']['marketplace'] : '';
			    	$ismarketplace = !empty($marketplaceslug) ? strstr($currenturl, $marketplaceslug) : true;

			    	if ( (is_home() || is_front_page()) && $isproductlising == false && $ismarketplace == false ) {
			    		$paginate_links_query = 'base='.get_home_url().'%_%&current=' . $current_page .'&total=' . $total_pages . '&type=list&prev_text='.__('« Previous' , 'pro').'&next_text=' . __('Next »' , 'pro');
			    	} else {
			    		$paginate_links_query = 'base='.get_pagenum_link(1).'%_%&format=page/%#%&current=' . $current_page .'&total=' . $total_pages . '&type=list&prev_text='.__('« Previous' , 'pro').'&next_text=' . __('Next »' , 'pro');
			    	}

				    $output .= '<div class="pagination">';
				    $output .= paginate_links( $paginate_links_query );
			       	$output .= '</div>';
			    }

		  	$output .= '</div>';
		  	$output .= '<div class="clear"></div>';

	  	}


	  	if ($echo)
	    	echo apply_filters( 'func_pro_global_product_listing' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_global_product_listing' , $output , $instance );

	}

	function pro_global_load_single_product_in_box( $args = array() ) {

		$defaults = array(
			'echo' => true,
			'post_id' => NULL,
			'span' => 'span4',
			'imagesize' => 'tb-360',
			'btnclass' => '',
			'iconclass' => '',
			'labelcolor' => '',
			'class' => '',
			'style' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');

		if (is_multisite()) {
			$blog_id = get_current_blog_id();
		} else {
			$blog_id = 1;
		}

		$instance['blog_id'] = $blog_id;

		if ($class == 'thetermsclass') {
			$class = '';
		  	$terms = get_the_terms( $post_id, 'product_category' );
			if (!empty($terms)) {
				foreach ($terms as $thesingleterm) {
					$class .= ' '.$thesingleterm->slug;
				}
			}
		} else {
			$class = esc_attr($class);
		}

		switch ($span) {
			case 'span6':
				$minheight = '30px';
				$maxwidth = '560';
				$imagesize = 'tb-860';
				break;
			case 'span3':
				$minheight = '50px';
				$maxwidth = '360';
				break;
			case 'span4':
			default:
				$minheight = '50px';
				$maxwidth = '360';
				break;
		}

		$proprice_args = array(
			'echo' => false,
			'post_id' => $post_id,
			'label' => false,
			'context' => ''
		);	

		$output = '<div class="'.$span.' well'.$class.'"'.$style.'>';

		if (has_post_thumbnail( $post_id ) ) {

			$fullimage = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full');

			$pro_image_output = '<div class="product-image-box" style="max-width: '.$maxwidth.'px;">';
				
				// product image
				$pro_image_output .= '<a href="'.get_permalink($post_id).'" class="product-image-link">' . get_the_post_thumbnail($post_id , $imagesize) . '</a>'; 
				
				// Product price
				$pro_image_output .= apply_filters( 'pro_global_product_box_price' , '<span class="label proprice'.$labelcolor.' hidden-phone">'.pro_product_price( $proprice_args ).'</span>' , $instance );

				// pinit button
				$pro_image_output .= ( function_exists('mp_pinit_button') ? '<span class="product-image-pinit-btn">' . mp_pinit_button( $post_id , 'all_view' ) . '</span>' : '' );

				$pro_image_output .= '<div class="hover-block hidden-phone">';
					$pro_image_output .= '<div class="btn-group">';
						$button = '<a href="'.$fullimage[0].'" class="btn'.$btnclass.' mpt-pp-trigger" data-pptitle="'.apply_filters( 'pro_product_box_prettyphoto_caption' , get_the_title( $post_id ) , $post_id ).'" title="'.__( 'View Product Image' , 'pro' ).'"><i class="icon-zoom-in'.$iconclass.'"></i></a>';
						$button .= '<a href="'.get_permalink($post_id).'" class="btn'.$btnclass.'" title="'.__( 'More Details' , 'pro' ).'"><i class="icon-list-alt'.$iconclass.'"></i></a>';
						$pro_image_output .= apply_filters( 'pro_global_product_box_btn_group' , $button , $instance );	
					$pro_image_output .= '</div>'; // End btn-group
				$pro_image_output .= '</div>'; // End hover-block

			$pro_image_output .= '</div>'; // End product-image-box

			$output .= apply_filters('pro_global_product_box_image' , $pro_image_output , $instance );
		} else {

			//$default_img_url = esc_url( get_option('mpt_mp_default_product_img') );
			$default_img_url = esc_url( get_blog_option(1,'mpt_mp_default_product_img') );
			$default_img_id = ( !empty($default_img_url) ? intval( get_image_id( $default_img_url ) ) : NULL );
			$pro_default_image_output = '';

			if ( !empty($default_img_id) && is_numeric($default_img_id) ) {
				$pro_default_image_output .= '<div class="product-image-box" style="max-width: '.$maxwidth.'px;">';
					// product image
					$pro_default_image_output .= '<a href="'.get_permalink($post_id).'" class="product-image-link">'.wp_get_attachment_image( $default_img_id, $imagesize ).'</a>'; 
					// Product price
					$pro_default_image_output .= apply_filters( 'pro_global_product_box_price' , '<span class="label proprice'.$labelcolor.' hidden-phone">'.pro_product_price( $proprice_args ).'</span>' , $instance );
				$pro_default_image_output .= '</div>'; // End product-image-box
			} 

			$output .= apply_filters('pro_global_product_box_image_default' , $pro_default_image_output , $instance );
		}

			$output .= '<div class="pro-product-box-contents align-center">';

				$output .= '<div class="hidden-phone align-center">';
					$output .= '<div class="product-meta" style="min-height: '.$minheight.'">';
						$output .= apply_filters( 'pro_global_product_box_title' , '<a href="'.get_permalink($post_id).'">'.get_the_title($post_id) . '</a>' , $post_id , $blog_id );	
					$output .= '</div>'; // End product-meta
				$output .= '</div>'; // End - hidden-phone

				$output .= '<p class="visible-phone">';
					$output .= apply_filters( 'pro_global_product_box_title' , '<a href="'.get_permalink($post_id).'">'.get_the_title($post_id) . '</a>' , $post_id , $blog_id ).'<br />';
					$output .= apply_filters( 'pro_global_product_box_price_mobile' , '<span class="label'.$labelcolor.'">'.pro_product_price( $proprice_args ).'</span>' , $post_id , $blog_id , $labelcolor );
				$output .= '</p>'; // End - visible-phone

				$output = apply_filters('pro_global_product_box_before_buy_button' , $output , $post_id , $blog_id );
					if ( class_exists('MPPremiumFeatures') ) {
						global $mppf;
						$global_btn = $mppf->load_buy_options( array( 'integration' => 'pro' , 'post_id' => $post_id , 'context' => 'list' , 'containerclass' => ' mppf-pro-integration mppf-container-block' , 'globalbtn' => true , 'btnclass' => $mppf->get_btn_color() ) );			
					} else {
						$global_btn .= '<a href="'.get_permalink($post_id).'" class="pro-global-buy-now-btn btn btn-block'.$btnclass.'">' . __( 'Buy Now' , 'pro' ) . '</a>';
					}
					$output .= apply_filters( 'pro_global_product_box_btn' , $global_btn , $instance );				
				$output = apply_filters('pro_global_product_box_after_buy_button' , $output , $post_id , $blog_id );

			$output .= '</div>'; // End pro-product-box-contents

		$output .= '</div>'; // End Well

	  	if ($echo)
	    	echo apply_filters( 'func_pro_global_load_single_product_in_box' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_global_load_single_product_in_box' , $output , $instance );
	}

	function pro_load_global_product_taxonomy_menu( $args = array() ) {

		$defaults = array(
			'echo' => true,
			'termslug' => '',
			'taxonomy' => 'product_category',
			'count' => 'false'
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );	

		$taxonomies = pro_load_global_taxonomies_list( array( 'include' => $taxonomy ) );
		$settings = get_site_option( 'mp_network_settings' );

		$output = apply_filters( 'pro_global_taxonomy_menu_before_listing' , '' , $termslug , $taxonomy );
		$output .= '<ul class="product-taxonomy-menu nav nav-tabs nav-stacked">';

			if  (!empty($taxonomies) && is_array($taxonomies)) {

			  	foreach($taxonomies as $cat) {
				
					// temp fix for subsite translations - Ryan -----------------------------------------------
				    if ($cat['type'] == 'product_category'){									
						if ( ! is_main_site() && ! $sitepress_settings[ 'taxonomies_sync_option' ]['product_category']) {
							global $wpdb,$sitepress,$sitepress_settings,$blog_id;
							$t_prefix = str_replace("_".$blog_id, "", $wpdb->prefix);
							$trid = $wpdb->get_var( "SELECT DISTINCT(t1.trid) FROM {$t_prefix}icl_translations AS t1 INNER JOIN {$t_prefix}terms AS t2 ON t2.term_id = t1.element_id WHERE t1.element_type='tax_".$cat['type']."' AND t2.name='".$cat['name']."' LIMIT 1" );
							$ts = $wpdb->get_var( "SELECT t1.name FROM {$t_prefix}terms AS t1 INNER JOIN {$t_prefix}term_taxonomy AS t2 ON t2.term_id = t1.term_id JOIN {$t_prefix}icl_translations t3 ON t3.element_id = t2.term_taxonomy_id WHERE t3.trid = '".$trid."' AND t3.language_code = '".$sitepress->get_current_language()."' AND t3.element_type = 'tax_".$cat['type']."' LIMIT 1" );
							$cat['name'] = empty($ts)?$cat['name']:$ts;
						}
					}
					// end - Ryan -------------------------------------------------------------------------

				    if ($cat['type'] == 'product_category')
				      	$link = get_home_url( mp_main_site_id(), $settings['slugs']['marketplace'] . '/' . $settings['slugs']['categories'] . '/' . $cat['slug'] . '/' );
				    else if ($cat['type'] == 'product_tag')
				      	$link = get_home_url( mp_main_site_id(), $settings['slugs']['marketplace'] . '/' . $settings['slugs']['tags'] . '/' . $cat['slug'] . '/' );
				    else
				    	$link = '';

				  	$output .= '<li'.( $termslug == $cat['slug'] ? ' class="selected"' : '' ).'><a href="'. $link .'">';
				  		$output .= pro_load_image_product_global_taxonomy( array( 'slug' => $cat['slug'] , 'taxonomy' => $taxonomy ) );
				    	$output .= $cat['name'].( $count == 'true' ? ' <small>('.$cat['count'].')</small>' : '' ).'</a>';
					$output .= '</li>';
			  	}
			}

		$output .= '</ul>';
		$output = apply_filters( 'pro_global_taxonomy_menu_after_listing' , $output , $termslug , $taxonomy );

  		if ($echo)
    		echo apply_filters( 'func_pro_load_global_product_taxonomy_menu' , $output , $instance );
  		else
    		return apply_filters( 'func_pro_load_global_product_taxonomy_menu' , $output , $instance );
	}

	// load category / tag image (extract from one of the post within the category)
	function pro_load_image_product_global_taxonomy( $args = array() ) {

		$defaults = array(
			'slug' => '',
			'taxonomy' => 'product_category',
			'size' => array(50, 50)
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );		

		global $wpdb;

		$slug = esc_sql( $slug );
		$taxonomy = esc_sql( $taxonomy );

		$query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = '$taxonomy' AND t.slug = '$slug' ORDER BY RAND() LIMIT 1";

	  	$results = $wpdb->get_results( $query );

	  	$current_blog_id = get_current_blog_id();

	  	foreach($results as $product) {
	  		global $current_blog;
			switch_to_blog($product->blog_id);

	  		$taxonomy_image = get_the_post_thumbnail( $product->post_id , $size );

	  		restore_current_blog();
	  	}

	  	restore_current_blog();

		$after_restore_blog_id = get_current_blog_id();
		if ( $current_blog_id != $after_restore_blog_id )
			switch_to_blog( $current_blog_id );

	  	return apply_filters( 'func_pro_load_image_product_global_taxonomy' , $taxonomy_image , $instance );
	}

	function pro_load_global_taxonomies_list( $args = array() ){

		$defaults = array(
			'include' => 'product_category',
			'limit' => 50,
			'order_by' => 'name',
			'order' => 'ASC'
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

	  	global $wpdb;

	  	$order_by = ($order_by == 'count') ? $order_by : 'name';
	  	$order = ($order == 'DESC') ? $order : 'ASC';
	  	$limit = intval($limit);

	  	if ($include == 'product_tag')
	    	$where = " WHERE t.type = 'product_tag'";
	  	else if ($include == 'product_category')
	    	$where = " WHERE t.type = 'product_category'";

	  	$tags = $wpdb->get_results( "SELECT name, slug, type, count(post_id) as count FROM {$wpdb->base_prefix}mp_terms t LEFT JOIN {$wpdb->base_prefix}mp_term_relationships r ON t.term_id = r.term_id$where GROUP BY t.term_id ORDER BY $order_by $order LIMIT $limit", ARRAY_A );

		if ( !$tags )
			return apply_filters( 'func_pro_load_global_taxonomies_list' , NULL , $instance );			
		else
		  	return apply_filters( 'func_pro_load_global_taxonomies_list' , $tags , $instance );
	}

	function pro_load_global_taxonomy_page_title() {
	    global $wpdb;

	    if ( $slug = get_query_var('global_taxonomy') ) {
	      	$slug = esc_sql( $slug );
	      	$name = $wpdb->get_var( "SELECT name FROM {$wpdb->base_prefix}mp_terms WHERE slug = '$slug'" );
	    }

	    echo apply_filters( 'func_pro_load_global_taxonomy_page_title' , $name );
	}

	function pro_load_global_taxonomies_listing( $args = array() ) {

		$defaults = array(
			'echo' => true,
			'taxonomy' => 'product_category',
			'btnclass' => '',
			'iconclass' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		$taxonomies = pro_load_global_taxonomies_list( array( 'include' => $taxonomy ) );
		$settings = get_site_option( 'mp_network_settings' );		

		$output = '<div id="pro-product-grid">';

		  	$count = 1;

			if  (!empty($taxonomies) && is_array($taxonomies)) {

			  	foreach($taxonomies as $cat) {

			  		$incomplete_row = true;
			  		$output .= ( $count == 1 ? '<div class="row-fluid">' : '' );

				    if ($cat['type'] == 'product_category') {
				    	$link = get_home_url( mp_main_site_id(), $settings['slugs']['marketplace'] . '/' . $settings['slugs']['categories'] . '/' . $cat['slug'] . '/' );
				    	$taxonomy_type = 'View Category';
				    } else if ($cat['type'] == 'product_tag') {
				    	$link = get_home_url( mp_main_site_id(), $settings['slugs']['marketplace'] . '/' . $settings['slugs']['tags'] . '/' . $cat['slug'] . '/' );
				    	$taxonomy_type = ' View Tag';
				    } else {
				    	$link = '';
				    	$taxonomy_type = 'More Info';
				    }

					$output .= '<div class="span3 well">';
						$output .= '<div class="product-image-box" style="max-width: 360px;">';
							$output = apply_filters( 'pro_global_taxonomies_listing_before_image' , $output , $cat['slug'] , $taxonomy );
							$output .= pro_load_image_product_global_taxonomy( array( 'slug' => $cat['slug'] , 'taxonomy' => $taxonomy , 'size' => 'tb-360' ) );
							$output = apply_filters( 'pro_global_taxonomies_listing_after_image' , $output , $cat['slug'] , $taxonomy );
						$output .= '</div>';
						$output .= '<div class="clear padding5"></div>';
						$output .= '<div class="align-center">';
							$output .= apply_filters( 'pro_global_taxonomies_listing_title' , '<p>'.__( $cat['name'] , 'pro' ).'</p>' , $cat['slug'] , $cat['name'] );				
							$output .= apply_filters( 'pro_global_taxonomies_listing_button' , '<a href="'.$link.'" class="btn'.$btnclass.'">'.__( $taxonomy_type , 'pro' ).'</a>' , $cat['slug'] , $link , $btnclass , $taxonomy_type );
						$output .= '</div>';
					$output .= '</div>';

					if ($count == 4) {
						$incomplete_row = false;
						$count = 0;
						$output .= '</div>';
					}

					$count++;
			  	}
			}
		if ( $incomplete_row )
			$output .= '</div>'; // End - row-fluid
		$output .= '</div>'; // End - pro-product-grid


  		if ($echo)
    		echo apply_filters( 'func_pro_load_global_taxonomies_listing' , $output , $instance );
  		else
    		return apply_filters( 'func_pro_load_global_taxonomies_listing' , $output , $instance );
	}

	function pro_load_global_related_products( $args = array() ) {

		$defaults = array(
			'echo' => false,
			'current_post' => NULL,
			'querytype' => 'By Categories',
			'btnclass' => '',
			'iconclass' => '',
			'labelcolor' => '',
			'boxclass' => '',
			'boxstyle' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

  		global $id, $wpdb;
  		$current_post = ( NULL === $current_post ) ? $id : $current_post;
  		$current_post_link = get_permalink( $current_post ); 

	  	//setup taxonomy if applicable
		if ( $querytype == 'By Tags' ) {
			$tag_slug_obj = array();
		    $tags = get_the_terms( $current_post , 'product_tag' );
		    if ( !empty( $tags ) ) {
			    foreach ($tags as $tag) {
			    	$tag_slug_obj[$tag->slug] = $tag->name;
			    }
			    $tag_slug = esc_sql( sanitize_title( array_rand( $tag_slug_obj ) ) );
		    } else {
		    	$tag_slug = '';
		    }
		    $query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = 'product_tag' AND t.slug = '$tag_slug'";
		    $query_count = "SELECT COUNT(*) FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = 'product_tag' AND t.slug = '$tag_slug'";
	  	} else {
	  		$cat_slug_obj = array();
		    $categories = get_the_terms( $current_post , 'product_category' );
		    if ( !empty($categories) ) {
			    foreach ($categories as $category) {
			    	$cat_slug_obj[$category->slug] = $category->name;
			    }
			    $category_slug = esc_sql( sanitize_title( array_rand( $cat_slug_obj ) ) );
		    } else {
				$category_slug = '';
		    }
		    $query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = 'product_category' AND t.slug = '$category_slug'";
			$query_count = "SELECT COUNT(*) FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = 'product_category' AND t.slug = '$category_slug'";
	  	}

	  	//get order by
	  	$query .= " ORDER BY RAND()";

		//get order direction
		$query .= " DESC";

		//get page details
	  	$query .= " LIMIT " . apply_filters( 'global_related_products_showposts' , 42 );

	  	//The Query
		$results = $wpdb->get_results( $query );
		$total_results = $wpdb->get_var( $query_count );

		$output = '';

		$incomplete_row = false;

		if ( $results && $total_results > 1 ) {

			$output .= '<div id="related-products-holder" class="related-products carousel slide">';

				$output .= '<div class="page-heading">';
					$output .= '<h3>' . __('Related Products', 'pro') . '</h3>';
				$output .= '</div>'; // End page-heading

					// Carousel nav
					$output .= '<div class="related-products-control-nav">';
						$output .= '<a class="related-products-control-left" href="#related-products-holder" data-slide="prev"><i class="icon-caret-left icon-3x"></i></a>';
						$output .= '<a class="related-products-control-right" href="#related-products-holder" data-slide="next"><i class="icon-caret-right icon-3x"></i></a>';
					$output .= '</div>';

				$output .= '<div id="pro-product-grid">';

					$output .= '<div class="carousel-inner">';

					$count = 1;
					$counter = 1;

					$current_blog_id = get_current_blog_id();

					foreach ($results as $product) {

						if ( $product->post_permalink == $current_post_link ) { } else {

							$incomplete_row = true;

							$output .= ( $count == 1 ? '<div class="row-fluid item'.( $counter == 1 ? ' active' : '' ).'">' : '' );

							global $current_blog;
						    switch_to_blog($product->blog_id);

						  	$single_args = array(
								'echo' => false,
								'post_id' => $product->post_id,
								'span' => 'span4',
								'imagesize' => 'tb-360',
								'btnclass' => $btnclass,
								'iconclass' => $iconclass,
								'labelcolor' => $labelcolor,
								'class' => $boxclass,
								'style' => $boxstyle
						  	);

							$output .= pro_global_load_single_product_in_box( $single_args );
							
							if ( $count == 3 ) {
								$incomplete_row = false;
								$count = 0;
								$output .= '</div>'; // End - row-fluid
							}

							$count++;
							$counter++;

							restore_current_blog();

						}
					}
					restore_current_blog();

					$after_restore_blog_id = get_current_blog_id();
					if ( $current_blog_id != $after_restore_blog_id )
						switch_to_blog( $current_blog_id );

					if ( $incomplete_row )
						$output .= '</div>'; // End - row-fluid

					$output .= '</div>'; // End Carousel-inner

				$output .= '</div>'; // End pro-product-grid

			$output .= '</div>'; // End - related-products

		}


  		if ($echo)
    		echo apply_filters( 'func_pro_load_global_related_products' , $output , $instance );
  		else
    		return apply_filters( 'func_pro_load_global_related_products' , $output , $instance );
	}

	class AQ_MP_Global_Product_Grid_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => __('[MP] Global Product Grid','pro'),
				'size' => 'span12',
			);
			
			//create the block
			parent::__construct('aq_mp_global_product_grid_block', $block_options);
		}
		
		function form($instance) {
			
			$defaults = array(
				'layout' => '3col',
				'entries' => '9',
				'showcategory' => 'no',
				'showpagination' => 'no',
				'align' => 'align-center',
				'order_by' => 'date', 
				'arrange' => 'DESC',
				'taxonomy_type' => 'none',
				'taxonomy' => '',
				'bgcolor' => '#fff',
				'btncolor' => 'lightblue',
				'pricelabelcolor' => 'blue',
			);
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);

			$layout_options = array(
				'2col' => __('2 Columns','pro'),
				'3col' => __('3 Columns','pro'),
				'4col' => __('4 Columns','pro')
			);

			$btncolor_options = array(
				'grey' => __('Grey','pro'),
				'blue' => __('Blue','pro'),
				'lightblue' => __('Light Blue','pro'),
				'green' => __('Green','pro'),
				'red' => __('Red','pro'),
				'yellow' => __('Yellow','pro'),
				'black' => __('Black','pro'),
			);

			$labelcolor_options = array(
				'grey' => __('Grey','pro'),
				'blue' => __('Blue','pro'),
				'green' => __('Green','pro'),
				'red' => __('Red','pro'),
				'yellow' => __('Yellow','pro'),
				'black' => __('Black','pro'),
			);

			$taxonomy_type_options = array(
				'none' => __('No Filter','pro'),
				'category' => __('Category','pro'),
				'tag' => __('Tag','pro'),
			);

			$showcategory_options = array(
				'yes' => __('Show Category Menu','pro'),
				'advsoft' => __('Show Advanced Sort Button','pro'),
				'no' => __('Nothing','pro'),
			);

			$showpagination_options = array(
				'yes' => __('Yes','pro'),
				'no' => __('No','pro'),
			);

			$align_options = array(
				'align-left' => __('Align Left','pro'),
				'align-center' => __('Align Center','pro'),
				'align-right' => __('Align Right','pro'),
			);

			$order_by_options = array(
				'title' => __('Product Name','pro'),
				'date' => __('Publish Date','pro'),
				'ID' => __('Product ID','pro'),
				'author' => __('Product Author','pro'),
				'sales' => __('Number of Sales','pro'),
				'rand' => __('Random','pro'),
			);

			$order_options = array(
				'DESC' => __('Descending','pro'),
				'ASC' => __('Ascending','pro'),
			);

			do_action( $id_base . '_before_form' , $instance );
			
			?>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('layout') ?>">
					<?php _e('Layout', 'pro') ?><br/>
					<?php echo aq_field_select('layout', $block_id, $layout_options, $layout); ?>
				</label>
			</div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('entries') ?>">
					<?php _e('Number of Entries (per page)', 'pro') ?><br/>
					<?php echo aq_field_input('entries', $block_id, $entries) ?>
				</label>
			</div>

			<div class="description third last">
				<label for="<?php echo $this->get_field_id('showpagination') ?>">
					<?php _e('Show Pagination', 'pro') ?><br/>
					<?php echo aq_field_select('showpagination', $block_id, $showpagination_options, $showpagination); ?>
				</label>
			</div>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('showcategory') ?>">
					<?php _e('Additional Functions', 'pro') ?><br />
					<?php echo aq_field_select('showcategory', $block_id, $showcategory_options, $showcategory); ?>
				</label><div class="cf"></div>
				<label for="<?php echo $this->get_field_id('align') ?>">
				<?php echo aq_field_select('align', $block_id, $align_options, $align); ?>
				</label>
			</div>
			
			<div class="description half last">
				<label for="<?php echo $this->get_field_id('taxonomyfilter') ?>">
					<?php _e('Taxonomy Filter:', 'pro') ?><br />
					<?php echo aq_field_select('taxonomy_type', $block_id, $taxonomy_type_options, $taxonomy_type); ?> <?php echo aq_field_input('taxonomy', $block_id, $taxonomy, $size = 'full') ?>
				</label>
			</div>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('order_by') ?>">
					<?php _e('Order Products By:', 'pro') ?><br />
					<?php echo aq_field_select('order_by', $block_id, $order_by_options, $order_by); ?>
				</label>
			</div>	

			<div class="description half last">
				<label for="<?php echo $this->get_field_id('arrange') ?>">
					<br />
					<?php echo aq_field_select('arrange', $block_id, $order_options, $arrange); ?>
				</label>
			</div>	

			<div class="description third">
				<label for="<?php echo $this->get_field_id('bgcolor') ?>">
					<?php _e('Pick a background color', 'pro') ?><br/>
					<?php echo aq_field_color_picker('bgcolor', $block_id, $bgcolor) ?>
				</label>
			</div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('btncolor') ?>">
					<?php _e('Button Color', 'pro') ?><br/>
					<?php echo aq_field_select('btncolor', $block_id, $btncolor_options, $btncolor); ?>
				</label>
			</div>

			<div class="description third last">
				<label for="<?php echo $this->get_field_id('pricelabelcolor') ?>">
					<?php _e('Price Label color', 'pro') ?><br/>
					<?php echo aq_field_select('pricelabelcolor', $block_id, $labelcolor_options, $pricelabelcolor); ?>
				</label>
			</div>
			
			<?php

			do_action( $id_base . '_after_form' , $instance );
		}
		
		function block($instance) {
			extract($instance);

			$entries = intval($entries);
			$pagination = $showpagination == 'yes' ? true : false;

		    if ($taxonomy_type == 'category') {
		      	$taxonomy_category = esc_attr($taxonomy);
		      	$taxonomy_tag = '';
		      	$context = $taxonomy_type . '_block';
		    } else if ($taxonomy_type == 'tag') {
		      	$taxonomy_tag = esc_attr($taxonomy);
		      	$taxonomy_category = '';
		      	$context = $taxonomy_type . '_block';
		    } else {
		    	$taxonomy_category = '';
		    	$taxonomy_tag = '';
		    	$context = 'list';
		    }

			switch ($btncolor) {
				case 'grey':
					$btnclass = '';
					$iconclass = '';
					break;
				case 'blue':
					$btnclass = ' btn-primary';
					$iconclass = ' icon-white';
					break;
				case 'lightblue':
					$btnclass = ' btn-info';
					$iconclass = ' icon-white';
					break;
				case 'green':
					$btnclass = ' btn-success';
					$iconclass = ' icon-white';
					break;
				case 'yellow':
					$btnclass = ' btn-warning';
					$iconclass = ' icon-white';
					break;
				case 'red':
					$btnclass = ' btn-danger';
					$iconclass = ' icon-white';
					break;
				case 'black':
					$btnclass = ' btn-inverse';
					$iconclass = ' icon-white';
					break;
				
			}

			switch ($pricelabelcolor) {
				case 'grey':
					$labelcolor = '';
					break;
				case 'blue':
					$labelcolor = ' label-info';
					break;
				case 'green':
					$labelcolor = ' label-success';
					break;
				case 'yellow':
					$labelcolor = ' label-warning';
					break;
				case 'red':
					$labelcolor = ' label-important';
					break;
				case 'Black':
					$labelcolor = ' label-inverse';
					break;
			}

			switch ($layout) {
				case '2col':
					$span = 'span6';
					$counter = '2';
					break;
				case '3col':
					$span = 'span4';
					$counter = '3';
					break;
				case '4col':
					$span = 'span3';
					$counter = '4';
					break;							
				default:
					$span = 'span4';
					$counter = '3';
					break;
			}

			if (get_query_var('paged')) {
				$page = intval(get_query_var('paged'));
			} elseif (get_query_var('page')) {
				$page = intval(get_query_var('page'));
			} else {
				$page = 1;
			}

			$output = '';

			if ($showcategory == 'yes') {

				$output .= '<ul class="mpt-product-categories ' . $align . '">';
					$output .= '<li>' . __( 'By Category:' , 'pro' ) . '</li>';
					$output .= '<li id="all">' . __( 'All' , 'pro' ) . '</li>';

					$categories = pro_load_global_taxonomies_list( array( 'include' => 'product_category' ) );

					if  ($categories) {
					  foreach($categories as $category) {
					    $output .= '<li id="'.$category['slug']. '">'.$category['name']. '</li>';
					  }
					}

				$output .= '</ul>';
				$output .= '<div class="clear"></div>';
			}

			$query_args = array(
				'unique_id' => $block_id,
				'sort' => ($showcategory == 'advsoft' ? true : false),
				'align' => $align,
				'context' => $context,
				'echo' => false,
				'paginate' => $pagination,
				'page' => $page,
				'per_page' => $entries,
				'order_by' => $order_by,
				'order' => $arrange,
				'category' => $taxonomy_category,
				'tag' => $taxonomy_tag,
				'counter' => $counter,
				'span' => $span,
				'btnclass' => $btnclass,
				'iconclass' => $iconclass,
				'labelcolor' => $labelcolor,
				'boxclass' => 'thetermsclass',
				'boxstyle' => ' background: '.esc_attr($bgcolor).';'
				);

			$output .= pro_global_advance_product_sort( $query_args );

			echo apply_filters( 'aq_mp_global_product_grid_block_output' , $output , $instance );
		}
	}

	class AQ_MP_Global_Image_Taxonomies_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => __('[MP] Global Image Taxonomies','pro'),
				'size' => 'span4',
			);
			
			//create the block
			parent::__construct('aq_mp_global_image_taxonomies_block', $block_options);
		}
		
		function form($instance) {
			
			$defaults = array(
				'title' => __('Product Categories','pro'),
				'taxonomy_type' => 'category',
				'count' => 'no',
				'class' => '',
				'style' => ''
			);
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);

			$taxonomy_type_options = array(
				'category' => __('Category','pro'),
				'tag' => __('Tag','pro')
			);

			$count_options = array(
				'yes' => __('Yes','pro'),
				'no' => __('No','pro'),
			);

			do_action( $id_base . '_before_form' , $instance );
			
			?>

			<div class="description">
				<label for="<?php echo $this->get_field_id('title') ?>">
					<?php _e('Title (optional)', 'pro') ?><br/>
					<?php echo aq_field_input('title', $block_id, $title) ?>
				</label>
			</div>

			<div class="description">
				<label for="<?php echo $this->get_field_id('taxonomy_type') ?>">
					<?php _e('Taxonomy Type:', 'pro') ?><br />
					<?php echo aq_field_select('taxonomy_type', $block_id, $taxonomy_type_options, $taxonomy_type); ?>
				</label>
			</div>

			<div class="description">
				<label for="<?php echo $this->get_field_id('count') ?>">
					<?php _e('Show Product Count', 'pro') ?><br/>
					<?php echo aq_field_select('count', $block_id, $count_options, $count); ?>
				</label>
			</div>

			<div class="cf"></div>

			<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

			<div class="description">
				<label for="<?php echo $this->get_field_id('class') ?>">
					<?php _e('class (optional)', 'pro') ?><br/>
					<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description">
				<label for="<?php echo $this->get_field_id('style') ?>">
					<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
					<?php echo aq_field_input('style', $block_id, $style) ?>
				</label>
			</div>
			
			<?php

			do_action( $id_base . '_after_form' , $instance );
		}
		
		function block($instance) {
			extract($instance);

			$class = (!empty($class) ? ' '.esc_attr($class) : '');
			$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');
			$count = ( $count == 'yes' ? 'true' : 'false' );
			$taxonomy_type = ( $taxonomy_type == 'tag' ? 'product_tag' : 'product_category' );

			$output = '<div id="sidebar">';

				$output .= '<div class="well sidebar-widget-well' . $class . '"' . $style . '>';

					$output .= ( !empty($title) ? '<h4 class="sidebar-widget-title">' . esc_attr( $title ) . '</h4>' : '' );

					$output .= pro_load_global_product_taxonomy_menu( array( 'echo' => false , 'termslug' => '' , 'taxonomy' => $taxonomy_type , 'count' => $count ) );

				$output .= '</div>'; // End - well

			$output .= '</div>'; // End - sidebar

			echo apply_filters( 'aq_mp_global_image_taxonomies_block_output' , $output , $instance );
		}
		
	}

	// Register MP Custom global Widget for PRO
	function register_custom_global_widgets_pro() {
		unregister_widget('MarketPress_Global_Product_List');
		register_widget( 'MarketPress_Global_Image_Taxonomy_Widget' );
		register_widget( 'PRO_Global_Product_List' );
	}

	//Product Taxonomy widget (with images)
	class MarketPress_Global_Image_Taxonomy_Widget extends WP_Widget {

		function MarketPress_Global_Image_Taxonomy_Widget() {
			$widget_ops = array( 'classname' => 'mp_global_image_taxonomy_widget', 'description' => __( "A list of product taxonomies (with images) from the network MarketPress stores.", 'pro' ) );
			$this->WP_Widget('mp_global_image_taxonomy_widget', __('Global Image Taxonomies Widget', 'pro'), $widget_ops);
		}

		function widget( $args, $instance ) {

			if ($instance['only_store_pages'] && !mp_is_shop_page())
				return;

			extract( $args );

			$title = apply_filters('widget_title', empty( $instance['title'] ) ? __('Global Product Categories', 'pro') : $instance['title'], $instance, $this->id_base);
			$c = $instance['count'] ? 'true' : 'false';
			$taxonomy_type = !empty($instance['taxonomy_type']) ? $instance['taxonomy_type'] : 'product_category';

			echo $before_widget;
			if ( $title )
				echo $before_title . esc_attr($title) . $after_title;

			echo pro_load_global_product_taxonomy_menu( array( 'echo' => false , 'termslug' => '' , 'taxonomy' => $taxonomy_type , 'count' => $c ) );

			echo $after_widget;
		}

		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['taxonomy_type'] = $new_instance['taxonomy_type'];
			$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
			$instance['only_store_pages'] = !empty($new_instance['only_store_pages']) ? 1 : 0;

			return $instance;
		}

		function form( $instance ) {
			//Defaults
			$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'only_store_pages' => 0 ) );
			$title = esc_attr( $instance['title'] );
			$taxonomy_type = !empty($instance['taxonomy_type']) ? $instance['taxonomy_type'] : 'product_category';
			$count = isset($instance['count']) ? (bool) $instance['count'] :false;
			$only_store_pages = isset( $instance['only_store_pages'] ) ? (bool) $instance['only_store_pages'] : false;
		?>
			<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		    <p>
			    <label><?php _e('Taxonomy Type:', 'pro') ?></label><br />
			    <select id="<?php echo $this->get_field_id('taxonomy_type'); ?>" name="<?php echo $this->get_field_name('taxonomy_type'); ?>">
			      <option value="product_category"<?php selected($taxonomy_type, 'product_category') ?>><?php _e('Category', 'pro') ?></option>
			      <option value="product_tag"<?php selected($taxonomy_type, 'product_tag') ?>><?php _e('Tag', 'pro') ?></option>
			    </select>
		    </p>

			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
			<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show product counts', 'pro' ); ?></label><br />

			<p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('only_store_pages'); ?>" name="<?php echo $this->get_field_name('only_store_pages'); ?>"<?php checked( $only_store_pages ); ?> />
			<label for="<?php echo $this->get_field_id('only_store_pages'); ?>"><?php _e( 'Only show on store pages', 'pro' ); ?></label></p>
		<?php
		}

	}

	function pro_global_product_listing_widget( $args = array() ){

		$defaults = array(
			'echo' => true,
			'per_page' => 10,
			'order_by' => 'date',
			'order' => 'DESC',
			'category' => '',
			'tag' => '',
			's' => '',
			'sentence' => '',
			'exact' => '',
			'show_thumbnail' => 1,
			'thumbnail_size' => 75,
			'show_price' => 1
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

	  	global $wp_query, $mp, $wpdb;

	  	//setup taxonomy if applicable
	  	if ($category) {
		    $category = esc_sql( sanitize_title( $category ) );
		    $query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = 'product_category' AND t.slug = '$category'";
	  	} else if ($tag) {
		    $tag = esc_sql( sanitize_title( $tag ) );
		    $query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = 'product_tag' AND t.slug = '$tag'";
	  	} else {
		    $query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content FROM {$wpdb->base_prefix}mp_products p WHERE p.blog_public = 1";
	  	}

	  	// get search
		if ( !empty($s) ) {
			// added slashes screw with quote grouping when done early, so done later
			$s = stripslashes($s);
			if ( !empty($sentence) ) {
				$search_terms = array($s);
			} else {
				preg_match_all('/".*?("|$)|((?<=[\r\n\t ",+])|^)[^\r\n\t ",+]+/', $s, $matches);
				$search_terms = array_map('_search_terms_tidy', $matches[0]);
			}
			$n = !empty($exact) ? '' : '%';
			$search = '';
			$searchand = '';
			foreach( (array) $search_terms as $term ) {
				$term = esc_sql( like_escape( $term ) );
				$search .= "{$searchand}((p.post_title LIKE '{$n}{$term}{$n}') OR (p.post_content LIKE '{$n}{$term}{$n}'))";
				$searchand = ' AND ';
			}
			if ( !empty($search) ) {
				$query .= " AND ({$search}) ";
			}
		}

		//get order by
		switch ($order_by) {
		    case 'title':
		      $query .= " ORDER BY p.post_title";
		      break;
		    case 'price':
		      $query .= " ORDER BY p.price";
		      break;
		    case 'sales':
		      $query .= " ORDER BY p.sales_count";
		      break;
		    case 'rand':
		      $query .= " ORDER BY RAND()";
		      break;
		    case 'date':
		    default:
		      $query .= " ORDER BY p.post_date";
		      break;
		  }

		//get order direction
		if ($order == 'ASC') {
		    $query .= " ASC";
		  } else {
		    $query .= " DESC";
		  }

	  	$query .= " LIMIT " . intval($per_page);

	  	$btnclass = mpt_load_mp_btn_color();

	  	$output = '<ul class="pro_product_list">';
	  	$count = 1;

		//The Query
		$results = $wpdb->get_results( $query );

		$current_blog_id = get_current_blog_id();

		if ($results) {
		    foreach ($results as $product) {

			    global $current_blog;
			    switch_to_blog($product->blog_id);
			    $output .= '<li>';
					$output .= '<div class="row-fluid">';
				        if ($show_thumbnail) {
				        	$output .= '<div class="span4">';
				        		$output .= mp_product_image( false, 'widget', $product->post_id , $thumbnail_size );
				        	$output .= '</div>'; //End span
				        	$output .= '<div class="span8">';
				        } else {
				        	$output .= '<div class="span12">';
				        }
						      	$output .= '<p><a href="' . get_permalink( $product->post_id ) . '">' . get_the_title( $product->post_id ) . '</a></p>';

						        if ($show_price) {
						        	$output .= pro_product_price( array( 'echo' => false , 'post_id' => $product->post_id , 'label' => false , 'context' => 'widget' )
	 );
						        	$output .= '<div class="clear padding5"></div>';
						        }

								if ( class_exists('MPPremiumFeatures') ) {
									global $mppf;
									$global_btn = $mppf->load_buy_options( array( 'integration' => 'pro' , 'post_id' => $product->post_id , 'context' => 'list' , 'containerclass' => ' mppf-pro-integration' , 'globalbtn' => true , 'btnclass' => $mppf->get_btn_color() ) );		
								} else {
									$global_btn = '<a href="'.get_permalink($post_id).'" class="pro-global-buy-now-btn btn btn-block'.$btnclass.'">' . __( 'Buy Now' , 'pro' ) . '</a>';
								}	
								$output .= apply_filters( 'pro_global_listing_widget_btn' , $global_btn , $instance );
						$output .= '</div>'; //End span
					$output .= '</div>'; // End Row-fluid
					$output .= '<div class="clear"></div>';
				$output .= '</li>';
				restore_current_blog();
			}
		}

		restore_current_blog();

		$after_restore_blog_id = get_current_blog_id();
		if ( $current_blog_id != $after_restore_blog_id )
			switch_to_blog( $current_blog_id );

		$output .= '</ul>';

	  	if ($echo)
	    	echo apply_filters( 'func_pro_global_product_listing_widget' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_global_product_listing_widget' , $output , $instance );
	}

	//Product listing widget
	class PRO_Global_Product_List extends WP_Widget {

		function PRO_Global_Product_List() {
			$widget_ops = array('classname' => 'pro_global_product_list_widget', 'description' => __('Shows a customizable global list of products from network MarketPress stores.', 'pro') );
			$this->WP_Widget('pro_global_product_list_widget', __('Global Product List', 'pro'), $widget_ops);
		}

		function widget($args, $instance) {
		    global $mp;
			extract( $args );

			echo $before_widget;
			
			$title = $instance['title'];
			if ( !empty( $title ) ) { echo $before_title . apply_filters('widget_title', $title) . $after_title; };

		    if ( !empty($instance['custom_text']) )
		      echo '<div id="custom_text">' . $instance['custom_text'] . '</div>';

		    $instance['as_list'] = true;
		    $instance['context'] = 'widget';

		    $query_args = array(
				'echo' => true,
				'per_page' => $instance['per_page'],
				'order_by' => $instance['order_by'],
				'order' => $instance['order'],
				'category' => $instance['category'],
				'tag' => $instance['tag'],
				's' => '',
				'sentence' => '',
				'exact' => '',
				'show_thumbnail' => $instance['show_thumbnail'],
				'thumbnail_size' => $instance['thumbnail_size'],
				'show_price' => $instance['show_price']
		    ); 
		    
		    //list global products
		    pro_global_product_listing_widget( $query_args );

		    echo $after_widget;
		}

		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title'] = wp_filter_nohtml_kses( $new_instance['title'] );
			$instance['custom_text'] = wp_filter_kses( $new_instance['custom_text'] );

			$instance['per_page'] = intval($new_instance['per_page']);
			$instance['order_by'] = $new_instance['order_by'];
			$instance['order'] = $new_instance['order'];
	    	$instance['category'] = ($new_instance['category']) ? sanitize_title($new_instance['category']) : '';
	    	$instance['tag'] = ($new_instance['tag']) ? sanitize_title($new_instance['tag']) : '';

		    $instance['show_thumbnail'] = !empty($new_instance['show_thumbnail']) ? 1 : 0;
		    $instance['thumbnail_size'] = !empty($new_instance['thumbnail_size']) ? intval($new_instance['thumbnail_size']) : 50;
		    $instance['show_price'] = !empty($new_instance['show_price']) ? 1 : 0;

			return $instance;
		}

		function form( $instance ) {
		    $instance = wp_parse_args( (array) $instance, array( 'title' => __('Global Products', 'pro'), 'custom_text' => '', 'per_page' => 10, 'order_by' => 'date', 'order' => 'DESC', 'show_thumbnail' => 1, 'thumbnail_size' => 75, 'text' => 'none' , 'category' => '' , 'tag' => '' , 'show_price' => 1 ) );
		    extract( $instance );
		  	?>
				<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'pro') ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
				<p><label for="<?php echo $this->get_field_id('custom_text'); ?>"><?php _e('Custom Text:', 'pro') ?><br />
		    		<textarea class="widefat" id="<?php echo $this->get_field_id('custom_text'); ?>" name="<?php echo $this->get_field_name('custom_text'); ?>"><?php echo esc_attr($custom_text); ?></textarea></label>
		    	</p>

		    	<h3><?php _e('List Settings', 'pro'); ?></h3>
		    	<p>
		    		<label for="<?php echo $this->get_field_id('per_page'); ?>"><?php _e('Number of Products:', 'pro') ?> <input id="<?php echo $this->get_field_id('per_page'); ?>" name="<?php echo $this->get_field_name('per_page'); ?>" type="text" size="3" value="<?php echo $per_page; ?>" /></label><br />
		    	</p>
			    <p>
			    	<label for="<?php echo $this->get_field_id('order_by'); ?>"><?php _e('Order Products By:', 'pro') ?><br />
			    	<select id="<?php echo $this->get_field_id('order_by'); ?>" name="<?php echo $this->get_field_name('order_by'); ?>">
				      	<option value="date"<?php selected($order_by, 'date') ?>><?php _e('Publish Date', 'pro') ?></option>
				      	<option value="title"<?php selected($order_by, 'title') ?>><?php _e('Product Name', 'pro') ?></option>
				      	<option value="sales"<?php selected($order_by, 'sales') ?>><?php _e('Number of Sales', 'pro') ?></option>
				      	<option value="rand"<?php selected($order_by, 'rand') ?>><?php _e('Random', 'pro') ?></option>
			    	</select><br />
			    	<label><input value="DESC" name="<?php echo $this->get_field_name('order'); ?>" type="radio"<?php checked($order, 'DESC') ?> /> <?php _e('Descending', 'pro') ?></label>
			    	<label><input value="ASC" name="<?php echo $this->get_field_name('order'); ?>" type="radio"<?php checked($order, 'ASC') ?> /> <?php _e('Ascending', 'pro') ?></label>
			    </p>
			    <p>
			    	<label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Limit To Product Category:', 'pro') ?></label><br />
			    	<input id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text" value="<?php echo $category; ?>" title="<?php _e('Enter the Slug', 'pro'); ?>" class="widefat" />
			    </p>
		    	<p>
		    		<label for="<?php echo $this->get_field_id('tag'); ?>"><?php _e('Limit To Product Tag:', 'pro') ?></label><br />
		    		<input id="<?php echo $this->get_field_id('tag'); ?>" name="<?php echo $this->get_field_name('tag'); ?>" type="text" value="<?php echo $tag; ?>" title="<?php _e('Enter the Slug', 'pro'); ?>" class="widefat" />
		    	</p>

		    	<h3><?php _e('Display Settings', 'pro'); ?></h3>
		    	<p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('show_thumbnail'); ?>" name="<?php echo $this->get_field_name('show_thumbnail'); ?>"<?php checked( $show_thumbnail ); ?> />
					<label for="<?php echo $this->get_field_id('show_thumbnail'); ?>"><?php _e( 'Show Thumbnail', 'pro' ); ?></label><br />
					<label for="<?php echo $this->get_field_id('thumbnail_size'); ?>"><?php _e('Thumbnail Size:', 'pro') ?> <input id="<?php echo $this->get_field_id('thumbnail_size'); ?>" name="<?php echo $this->get_field_name('thumbnail_size'); ?>" type="text" size="3" value="<?php echo $thumbnail_size; ?>" /></label>
		    	</p>
		    	<p>
		   			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('show_price'); ?>" name="<?php echo $this->get_field_name('show_price'); ?>"<?php checked( $show_price ); ?> />
					<label for="<?php echo $this->get_field_id('show_price'); ?>"><?php _e( 'Show Price', 'pro' ); ?></label>
		    	</p>
		<?php
		}
	}