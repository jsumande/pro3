<?php

	add_action('wp_enqueue_scripts', 'pro_register_wishlist_js');
	add_action( 'show_user_profile' , 'pro_display_wishlist_item_in_user_profile' );
	add_action( 'profile_update', 'pro_user_profile_update_wishlist_item');
	add_shortcode( 'wishlist', 'pro_display_wishlist_in_page_sc' );

    add_action( 'wp_ajax_nopriv_pro-update-wishlist', 'pro_update_wishlist_item' );
    add_action( 'wp_ajax_pro-update-wishlist', 'pro_update_wishlist_item' );

	function pro_register_wishlist_js(){
		wp_enqueue_script('pro-wishlist-ajax-js', get_template_directory_uri() . '/js/pro-wishlist-ajax.js', array('jquery'));
		wp_localize_script( 'pro-wishlist-ajax-js', 'PRO_WL_Ajax', array( 
			'ajaxUrl' => admin_url( 'admin-ajax.php', (is_ssl() ? 'https': 'http') ),
			'addingWLMsg' => __('Adding to your wish list...', 'pro'),
			'successWLMsg' => __('Item in wish list', 'pro'), 
			'errorWLMsg' => __('Failed to add item into wish list.', 'pro'),
			'imgUrl' => get_template_directory_uri() . '/img/loading.gif'
			));
	}

	function pro_update_wishlist_item() {

		$blog_id =  is_multisite() ? ( isset( $_POST['currentblogid'] ) ? esc_attr(intval($_POST['currentblogid'])) : 1 ) : 1;
		$post_id = isset( $_POST['pro_add_to_wishlist'] ) ? esc_attr(intval($_POST['pro_add_to_wishlist'])) : '';
		$currentpage = get_permalink( $post_id );

		$error = '<div class="alert alert-error nomargin">'.__( 'Failed to add item into wish list.' , 'pro' ); 
			$error .= is_user_logged_in() ? '' : ' '.__( 'You need to ' , 'pro' ).'<a href="'.wp_login_url( $currentpage ).'">'.__( 'login' , 'pro' ).'</a>'.__( ' first' , 'pro' );
			$error .= pro_item_in_wishlist($post_id) ? ' '.__( 'Item already in the list.' , 'pro' ) : '';
		$error .= '</div>';

		$error = apply_filters( 'pro_wishlist_button_error_tag', $error, $post_id , $currentpage );

		if ( is_user_logged_in() ) {

		    global $current_user;

		    //$wishlist = array();
		    $meta = get_user_meta($current_user->ID, 'pro_wishlist_items' , true );
		    $notinlist = pro_item_in_wishlist($post_id) ? false : true;

		    if (!empty($post_id) && $notinlist ) {

			    if (is_array($meta)) {
			    	$meta[] = array( 'item' => $post_id , 'blog' => $blog_id);
			    } else {
			    	$meta = array();
			    	$meta[] = array( 'item' => $post_id , 'blog' => $blog_id);
			    }
		    	update_user_meta($current_user->ID, 'pro_wishlist_items', $meta);

		    } else {
				if (defined('DOING_AJAX') && DOING_AJAX) {
					echo 'error||' . $error;
					exit;
				}
			} 

		} else {
			if (defined('DOING_AJAX') && DOING_AJAX) {
				echo 'error||' . $error;
				exit;
			}
		}
	}

	function pro_user_profile_update_wishlist_item() {

	    global $current_user;

	    if (isset($_REQUEST['user_id'])) {
	      $user_id = $_REQUEST['user_id'];
	    } else {
	      $user_id = $current_user->ID;
	    }

	    $meta = get_user_meta($user_id, 'pro_wishlist_items', true);

	    if (is_array($meta)) {

		    foreach ($meta as $key => $value) {
		    	
		    	if ( isset( $_POST['removefromwishlist'.$key] ) ) {

		    		if ( $_POST['removefromwishlist'.$key] == '1' ) {
		    			unset($meta[$key]);
		    		}
		    	}

		    }

		    $meta = array_values($meta);
		    update_user_meta($user_id, 'pro_wishlist_items', $meta); 
	    }	
	
	}

	function pro_display_wishlist_item_in_user_profile() {

	    global $current_user;

	    if (isset($_REQUEST['user_id'])) {
	      $user_id = $_REQUEST['user_id'];
	    } else {
	      $user_id = $current_user->ID;
	    }

	    $meta = get_user_meta($user_id, 'pro_wishlist_items', true);

        $output = '<h3>'.__('My Wish List', 'pro').':</h3>';

	    $output .= '<table class="form-table">';

	    	$output .= '<thead><tr>';

	    		$output .= '<th><strong>'.__('Item', 'pro').'</strong></th>';
	    		$output .= '<th><strong>'.__('Price', 'pro').'</strong></th>';
	    		$output .= '<th><strong>'.__('Action', 'pro').'</strong></th>';
	    		$output .= '<th></th>';

	    	$output .= '</tr></thead>';

	    	$output .= '<tbody>';

	    	if (is_array($meta)) {

	    		foreach ($meta as $key => $value) {
	    			
		            if (!empty($value)) {

		            	$blog_id = esc_html(esc_attr(trim($value['blog'])));

		            	if (is_multisite() && !empty($blog_id)) {
		            		switch_to_blog($blog_id);
		            	}

		            	$item_id = esc_html(esc_attr(trim($value['item'])));

			            $output .= '<tr>';
			                $output .= '<td>'.get_the_title($item_id).'</td>';
			               	$output .= '<td>'.pro_wishlist_load_product_price( array( 'echo' => false , 'post_id' => $item_id ) ).'</td>';
			               	$output .= '<td><a href="'.get_permalink($item_id).'" target="_blank">'.__( 'Buy Now' , 'pro' ).'</a></td>';
			               	$output .= '<td><input type="hidden" name="removefromwishlist'.$key.'" value="0" /><label><input type="checkbox" name="removefromwishlist'.$key.'" id="removefromwishlist'.$key.'" value="1" /> '. __( 'Remove from wish list' , 'pro' ).'</label></td>';
			            $output .= '</tr>';

		            	if (is_multisite()) {
		            		restore_current_blog();
		            	}
		            }

	    		}

	    	} else {

	    		$output .= '<tr><td><em>'.__( 'Your wish list is currently empty.' , 'pro' ).'</em></td></tr>';

	    	}

	        $output .= '</tbody>';

	    $output .= '</table>';

	    echo apply_filters( 'func_pro_display_wishlist_item_in_user_profile' , $output , $user_id );

	}

	function pro_wishlist_load_product_price( $args = array() ) {

		$defaults = array(
			'echo' => true,
			'post_id' => NULL,
			'context' => 'frontpage'
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );		

		global $id, $mp;
		$post_id = ( NULL === $post_id ) ? $id : $post_id;

		$meta = get_post_custom($post_id);

		if (empty($meta))
			return;

	  	//unserialize
	  	foreach ($meta as $key => $val) {
		  $meta[$key] = maybe_unserialize($val[0]);
		  if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file" && $key != "mp_price_sort")
		    $meta[$key] = array($meta[$key]);
		}

	  	if ( is_array($meta["mp_price"]) && count($meta["mp_price"]) == 1 ) {
		    if ($meta["mp_is_sale"]) {
			    $price = '<span class="mp_special_price"><del class="mp_old_price">'.$mp->format_currency('', $meta["mp_price"][0]).'</del>';
			    $price .= '<span itemprop="price" class="mp_current_price">'.$mp->format_currency('', $meta["mp_sale_price"][0]).'</span></span>';
			  } else {
			    $price = '<span itemprop="price" class="mp_normal_price"><span class="mp_current_price">'.$mp->format_currency('', $meta["mp_price"][0]).'</span></span>';
			  }
		} else { 
	    
		    if ($meta["mp_is_sale"]) {
		      //do some crazy stuff here to get the lowest price pair ordered by sale prices
		      asort($meta["mp_sale_price"], SORT_NUMERIC);
		      $lowest = array_slice($meta["mp_sale_price"], 0, 1, true);
		      $keys = array_keys($lowest);
		      $mp_price = $meta["mp_price"][$keys[0]];
		      $mp_sale_price = array_pop($lowest);
			    $price = __('From', 'pro').' <span class="mp_special_price"><del class="mp_old_price">'.$mp->format_currency('', $mp_price).'</del>';
			    $price .= '<span itemprop="price" class="mp_current_price">'.$mp->format_currency('', $mp_sale_price).'</span></span>';
			  } else {
		      sort($meta["mp_price"], SORT_NUMERIC);
			    $price = __('From', 'pro').' <span itemprop="price" class="mp_normal_price"><span class="mp_current_price">'.$mp->format_currency('', $meta["mp_price"][0]).'</span></span>';
			  }
		}

		$price = apply_filters( 'pro_wishlist_price_tag', $price, $post_id, $context );

	  	if ($echo)
	    	echo apply_filters( 'func_pro_wishlist_load_product_price' , $price , $instance );
	  	else
	    	return apply_filters( 'func_pro_wishlist_load_product_price' , $price , $instance );
	}

	function pro_display_wishlist_in_page_sc( $atts ) {

		extract( shortcode_atts( array(
			'showtitle' => 'yes',
			'title' => 'My Wish List',
			'btncolor' => 'lightblue',
			'style' => '',
			'class' => ''
		), $atts ) );

		$style = ( !empty($style) ? esc_attr($style) : '' );
		$class = ( !empty($class) ? esc_attr($class) : '' );
		$showtitle = ( !empty($showtitle) ? esc_attr($showtitle) : '' );
		$title = ( !empty($title) ? esc_attr($title) : '' );

		$id = get_the_ID();
		$currentpage = get_permalink( $id );

		switch ($btncolor) {
			case 'grey':
				$btnclass = '';
				$iconclass = '';
				break;
			case 'blue':
				$btnclass = ' btn-primary';
				$iconclass = ' icon-white';
				break;
			case 'lightblue':
				$btnclass = ' btn-info';
				$iconclass = ' icon-white';
				break;
			case 'green':
				$btnclass = ' btn-success';
				$iconclass = ' icon-white';
				break;
			case 'yellow':
				$btnclass = ' btn-warning';
				$iconclass = ' icon-white';
				break;
			case 'red':
				$btnclass = ' btn-danger';
				$iconclass = ' icon-white';
				break;
			case 'black':
				$btnclass = ' btn-inverse';
				$iconclass = ' icon-white';
				break;

			default:
				$btnclass = '';
				$iconclass = '';
				break;
			
		}

		if ( is_user_logged_in() ) {
			return pro_wishlist_form( array( 'echo' => false , 'showtitle' => $showtitle , 'title' => $title , 'btnclass' => $btnclass , 'iconclass' => $iconclass , 'class' => $class , 'style' => $style ) );
		} else {
			return '<div class="wish-login-link">'.__( 'Kindly ' , 'pro' ).'<a href="'.wp_login_url( $currentpage ).'">'.__( 'login' , 'pro' ).'</a>'.__( ' to view your wish list.' , 'pro' ).'</div>';
		}
	}

	function pro_wishlist_form( $args = array() ) {

		$defaults = array(
			'echo' => true,
			'showtitle' => 'no',
			'title' => '',
			'btnclass' => '',
			'iconclass' => '',
			'class' => '',
			'style' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );			

	    global $current_user, $mp;

		$style = ( !empty($style) ? ' style="'.esc_attr($style).'"' : '' );
		$class = ( !empty($class) ? ' '.esc_attr($class) : '' );

	    if (isset($_REQUEST['user_id'])) {
	      $user_id = $_REQUEST['user_id'];
	    } else {
	      $user_id = $current_user->ID;
	    }

	    $meta = get_user_meta($user_id, 'pro_wishlist_items', true);

	    if (is_array($meta) && isset( $_POST['pro-update-wishlist-form'] ) ) {

		    foreach ($meta as $key => $value) {
		    	
		    	if ( isset( $_POST['removefromwishlist'.$key] ) ) {

		    		if ( $_POST['removefromwishlist'.$key] == '1' ) {
		    			unset($meta[$key]);
		    		}
		    	}

		    }

		    $meta = array_values($meta);
		    update_user_meta($user_id, 'pro_wishlist_items', $meta); 
	    }

	    $output = '<form id="wishlist" class="wishlist-form" method="post" action="#wishlist">';	    	

			if ($showtitle == 'yes') {
				$output .= '<h3>'.__($title, 'pro').':</h3>';
			}

		    $output .= '<table class="table table-striped table-hover table-bordered table-wishlist'.$class.'"'.$style.'>';

		    	$output .= '<thead><tr>';

		    		$output .= '<th>'.__('Item', 'pro').'</th>';
		    		//$output .= ( $mp->get_setting('disable_cart') && get_option('mpt_mp_hide_product_price') == 'true' ? '' : '<th>'.__('Price', 'pro').'</th>' );
					$output .= ( $mp->get_setting('disable_cart') && get_blog_option(1,'mpt_mp_hide_product_price') == 'true' ? '' : '<th>'.__('Price', 'pro').'</th>' );
		    		$output .= '<th>'.__('Action', 'pro').'</th>';

		    	$output .= '</tr></thead>';

		    	$output .= '<tbody>';

		    	if (is_array($meta) && !empty($meta)) {

		    		foreach ($meta as $key => $value) {
		    			
			            if (!empty($value)) {

			            	$blog_id = esc_html(esc_attr(trim($value['blog'])));

			            	if (is_multisite() && !empty($blog_id)) {
			            		switch_to_blog($blog_id);
			            	}

			            	$item_id = esc_html(esc_attr(trim($value['item'])));

				            $output .= '<tr>';
				            	$output .= '<td>'.mp_product_image( false, 'widget', $item_id, 100 ).'<a href="'.get_permalink($item_id).'">'.get_the_title($item_id).'</a></td>';
				               	//$output .= ( $mp->get_setting('disable_cart') && get_option('mpt_mp_hide_product_price') == 'true' ? '' : '<td>'.pro_wishlist_load_product_price( array( 'echo' => false , 'post_id' => $item_id ) ).'</td>' );
									$output .= ( $mp->get_setting('disable_cart') && get_blog_option(1,'mpt_mp_hide_product_price') == 'true' ? '' : '<td>'.pro_wishlist_load_product_price( array( 'echo' => false , 'post_id' => $item_id ) ).'</td>' );
				               	$output .= '<td>';
					               		$output .= '<a href="'.get_permalink($item_id).'" class="btn'.$btnclass.'">'.__( 'Buy Now' , 'pro' ).'</a>';
					               		$output .= '<div class="clear padding5"></div>';
					               		$output .= '<input type="hidden" name="removefromwishlist'.$key.'" value="0" /><label><input type="checkbox" name="removefromwishlist'.$key.'" id="removefromwishlist'.$key.'" value="1" /> '. __( 'Remove from wish list' , 'pro' ).'</label>';
				               	$output .= '</td>';
				            $output .= '</tr>';

			            	if (is_multisite()) {
			            		restore_current_blog();
			            	}
			            }

		    		}

		    	} else {

		    		$output .= '<tr><td colspan="3"><span class="wishlist-empty-list">'.__( 'Your wish list is currently empty.' , 'pro' ).'</span></td></tr>';

		    	}

		    	$output .= '<tr><td colspan="3" class="align-right">';
		    		$output .= '<button type="submit" class="btn'.$btnclass.'">'.__( 'Update Your Wish List &raquo;' , 'pro' ).'</button>';
		    		$output .= '<input type="hidden" name="pro-update-wishlist-form" value="pro-update-wishlist-form" />';
		    	$output .= '</td></tr>';

		        $output .= '</tbody>';

		    $output .= '</table>';

	    $output .= '</form>';

	  	if ($echo)
	    	echo apply_filters( 'func_pro_wishlist_form' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_wishlist_form' , $output , $instance );
	}

	function pro_wishlist_button( $args = array() ) {

		$defaults = array(
			'echo' => true,
			'post_id' => NULL,
			'btnclass' => '',
			'iconclass' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );		

		$button = '<form class="pro_wishlist_form align-center" method="post" action="'.get_permalink($post_id).'" >';
			$button .= '<input class="btn'.$btnclass.' pro_button_addwishlist" type="submit" name="addwishlist" value="'.__( 'Add to Wish List' , 'pro' ).'">';
			$button .= '<input type="hidden" name="pro_add_to_wishlist" id="pro_add_to_wishlist" value="'.$post_id.'" />';
			$button .= is_multisite() ? '<input type="hidden" name="currentblogid" id="currentblogid" value="'.get_current_blog_id().'" />' : '';
			$button .= '<input type="hidden" name="action" value="pro-update-wishlist" />';	
		$button .= '</form>';

		$button = apply_filters( 'pro_wishlist_button_tag', $button, $post_id, $btnclass , $iconclass );

		$success = apply_filters( 'pro_wishlist_button_success_tag' , '<div class="alert alert-success nomargin">'.__( 'Item in wish list' , 'pro' ).'</div>' , $post_id );

	  	if ($echo)
	    	echo apply_filters( 'func_pro_wishlist_button' , is_user_logged_in() ? ( pro_item_in_wishlist($post_id) ? $success : $button ) : $button , $instance );
	  	else
	    	return apply_filters( 'func_pro_wishlist_button' , is_user_logged_in() ? ( pro_item_in_wishlist($post_id) ? $success : $button ) : $button , $instance );

	}

	function pro_item_in_wishlist($post_id) {
		global $current_user;
		$meta = get_user_meta($current_user->ID, 'pro_wishlist_items' , true );
		$blog_id = is_multisite() ? get_current_blog_id() : 1;

		if (is_array($meta)) {

			foreach ($meta as $key => $value) {

				if ( $post_id == $value['item'] && $blog_id == $value['blog'] ) {
					return apply_filters( 'func_pro_item_in_wishlist' , true , $post_id );
					exit;
				}
			}

			return apply_filters( 'func_pro_item_in_wishlist' , false , $post_id );

		} else {
			return apply_filters( 'func_pro_item_in_wishlist' , false , $post_id );
		}
	}