<?php 

/*
	PRO Theme Metaboxes
*/

	if (!function_exists('pro_theme_metaboxes')) {
		
		function pro_theme_metaboxes( array $meta_boxes ) {

			// Start with an underscore to hide fields from custom fields list
			$prefix = '_mpt_';
			$themename = 'pro';

			$yes_no_options = array(
				array( 'name' => __( 'Yes' , $themename ) , 'value' => 'yes'),
				array( 'name' => __( 'No' , $themename ) , 'value' => 'no'),
			);

			$color_options = array(
				array( 'name' => __( 'Black' , $themename ) , 'value' => 'black'),
				array( 'name' => __( 'Grey' , $themename ) , 'value' => 'grey'),
				array( 'name' => __( 'Blue' , $themename ) , 'value' => 'blue'),
				array( 'name' => __( 'Light Blue' , $themename ) , 'value' => 'lightblue'),
				array( 'name' => __( 'Green' , $themename ) , 'value' => 'green'),
				array( 'name' => __( 'Red' , $themename ) , 'value' => 'red'),
				array( 'name' => __( 'Yellow' , $themename ) , 'value' => 'yellow')
			);

			/********* Metaboxes for Products ************/
			/*
			if ( class_exists( 'MarketPress' ) ) {

				$meta_boxes[] = array(
					'id'         => 'page_metabox',
					'title'      => __( 'Additional Product Images' , $themename ),
					'pages'      => array('product'), // Post type
					'context'    => 'normal',
					'priority'   => 'high',
					'show_names' => true, // Show field names on the left
					'fields'     => array(
						array(
							'name' => __( 'Second Product Image' , $themename ),
							'desc' => __( 'Upload an image or enter an URL here.' , $themename ),
							'id' => $prefix . 'video_featured_image_2',
							'type' => 'file',
							'save_id' => true, // save ID using true
							'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
						),
						array(
							'name' => __( 'Third Product Image' , $themename ),
							'desc' => __( 'Upload an image or enter an URL here.' , $themename ),
							'id' => $prefix . 'video_featured_image_3',
							'type' => 'file',
							'save_id' => true, // save ID using true
							'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
						),
						array(
							'name' => __( 'Fourth Product Image' , $themename ),
							'desc' => __( 'Upload an image or enter an URL here.' , $themename ),
							'id' => $prefix . 'video_featured_image_4',
							'type' => 'file',
							'save_id' => true, // save ID using true
							'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
						),
						array(
							'name' => __( 'Fifth Product Image' , $themename ),
							'desc' => __( 'Upload an image or enter an URL here.' , $themename ),
							'id' => $prefix . 'video_featured_image_5',
							'type' => 'file',
							'save_id' => true, // save ID using true
							'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
						),
						array(
							'name' => __( 'Sixth Product Image' , $themename ),
							'desc' => __( 'Upload an image or enter an URL here.' , $themename ),
							'id' => $prefix . 'video_featured_image_6',
							'type' => 'file',
							'save_id' => true, // save ID using true
							'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
						),
						array(
							'name' => __( 'Seventh Product Image' , $themename ),
							'desc' => __( 'Upload an image or enter an URL here.' , $themename ),
							'id' => $prefix . 'video_featured_image_7',
							'type' => 'file',
							'save_id' => true, // save ID using true
							'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
						),
						array(
							'name' => __( 'Eighth Product Image' , $themename ),
							'desc' => __( 'Upload an image or enter an URL here.' , $themename ),
							'id' => $prefix . 'video_featured_image_8',
							'type' => 'file',
							'save_id' => true, // save ID using true
							'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
						),
						array(
							'name' => __( 'Ninth Product Image' , $themename ),
							'desc' => __( 'Upload an image or enter an URL here.' , $themename ),
							'id' => $prefix . 'video_featured_image_9',
							'type' => 'file',
							'save_id' => true, // save ID using true
							'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
						),
						array(
							'name' => __( 'Tenth Product Image' , $themename ),
							'desc' => __( 'Upload an image or enter an URL here.' , $themename ),
							'id' => $prefix . 'video_featured_image_10',
							'type' => 'file',
							'save_id' => true, // save ID using true
							'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
						),
						array(
							'name'         => __( 'Add More Images', $themename ),
							'desc'         => __( 'Use the "shift" / "Ctrl" button to select multiple files', $themename ),
							'id'           => $prefix . 'more_product_images',
							'type'         => 'file_list',
							'preview_size' => array( 100 , 100 ),
						),
					),
				);

			}
			*/
			/********* Metaboxes for Products ************/

			if ( class_exists( 'MarketPress' ) ) {

				// Related products
				$meta_boxes[] = array(
					'id'         => 'relatad_products_id_metabox',
					'title'      => __( 'Related Products (Filter By Specific Product IDs)' , $themename ),
					'pages'      => array('product'), // Post type
					'context'    => 'normal',
					'priority'   => 'high',
					'show_names' => true, // Show field names on the left
					'fields'     => array(
						array(
							'name' => __( 'Product IDs' , $themename ),
							'desc' => __( 'Enter the specific products IDs you want to use in \'Related Products\' section. The IDs should be separated by comas (ex: 1,2,3).Please note that you\'ll need to select \'By IDs\' for query type options in the Theme Options Panel in order for this to work. Also, this won\'t work in global listing.' , $themename ),
							'id'   => $prefix . 'related_products_specific_ids',
							'type' => 'textarea_small',
						)
					),
				);

				// Page layout
				$page_layout = apply_filters( 'pro_product_metabox_page_layout' , true );
				$layout_options = apply_filters( 'pro_product_metabox_page_layout_options' , array(
									array('name' => __( 'Style 1' , $themename ) , 'value' => 'style-1'),
									array('name' => __( 'Style 2' , $themename ) , 'value' => 'style-2'),
								) );
				if ( $page_layout ) {
					$meta_boxes[] = array(
						'id'         => 'single_product_page_layout',
						'title'      => __( 'Page Layout' , $themename ),
						'pages'      => array('product'), // Post type
						'context'    => 'side',
						'priority'   => 'default',
						'show_names' => false, // Show field names on the left
						'fields'     => array(
							array(
								'name' => __( 'Page Layout' , $themename ),
								'desc' => __( 'Select the page layout you want to use for this product.' , $themename ),
								'id' => $prefix . 'product_page_layout',
								'type' => 'select',
								'options' => $layout_options,
								'std'  => 'style-1'
							),
						),
					);
				} // end - if ( $page_layout )

			} // end - if ( class_exists( 'MarketPress' ) )

			/********* Metaboxes for pages ************/

			$meta_boxes[] = array(
				'id'         => 'page_metabox',
				'title'      => __( 'Page Options' , $themename ),
				'pages'      => array('page'), // Post type
				'context'    => 'normal',
				'priority'   => 'high',
				'show_names' => true, // Show field names on the left
				'fields'     => array(
					array(
			            'name' => __( 'Content Section: Background Color' , $themename ),
			            'desc' => __( 'Pick a custom background color for content section.' , $themename ),
			            'id'   => $prefix . 'page_content_bg_color',
			            'type' => 'colorpicker',
						'std'  => ''
			        ),
					array(
			            'name' => __( 'Content Section: Text Color' , $themename ),
			            'desc' => __( 'Pick a custom text color for content section.' , $themename ),
			            'id'   => $prefix . 'page_content_text_color',
			            'type' => 'colorpicker',
						'std'  => ''
			        ),		        
					array(
						'name' => __( 'Show Comments' , $themename ),
						'desc' => '',
						'id' => $prefix . 'page_show_comments',
						'type' => 'checkbox'
					),
				),
			);

			// Meta Boxes For Blog Page Template
			$meta_boxes[] = array(
				'id'         => 'blog_page_metabox',
				'title'      => __( 'Blog Options' , $themename ),
				'pages'      => array( 'page', ), // Post type
				'context'    => 'normal',
				'priority'   => 'high',
				'show_names' => true, // Show field names on the left
				'show_on'    => array( 'key' => 'page-template', 'value' => 'page-blog.php' ), // Specific page template to display this metabox
				'fields' => array(
					array(
						'name' => __( 'Number of entries' , $themename ),
						'desc' => __( 'Select the number of entries that should appear (per page).' , $themename ),
						'id' => $prefix . 'blog_page_entries',
						'type' => 'select',
						'options' => array(
							array('name' => '2', 'value' => '2'),
							array('name' => '3', 'value' => '3'),
							array('name' => '4', 'value' => '4'),		
							array('name' => '5', 'value' => '5'),
							array('name' => '6', 'value' => '6'),		
							array('name' => '7', 'value' => '7'),
							array('name' => '8', 'value' => '8'),		
							array('name' => '9', 'value' => '9'),
							array('name' => '10', 'value' => '10'),	
							array('name' => '11', 'value' => '11'),
							array('name' => '12', 'value' => '12'),	
							array('name' => '13', 'value' => '13'),
							array('name' => '14', 'value' => '14'),	
							array('name' => '15', 'value' => '15'),
							array('name' => '16', 'value' => '16'),	
							array('name' => '17', 'value' => '17'),
							array('name' => '18', 'value' => '18'),	
							array('name' => '19', 'value' => '19'),
							array('name' => '20', 'value' => '20')
						),
						'std'  => '5'
					),
					array(
						'name' => __( 'Page Layout' , $themename ),
						'desc' => __( 'Select the layout you want to display in this blog page' , $themename ),
						'id' => $prefix . 'blog_page_layout',
						'type' => 'select',
						'options' => array(
							array('name' => __( '1 Columns (with sidebar)' , $themename ) , 'value' => '1col'),
							array('name' => __( '2 Columns (without sidebar)' , $themename ) , 'value' => '2col'),
							array('name' => __( '3 Columns (without sidebar)' , $themename ) , 'value' => '3col'),
							array('name' => __( '4 Columns (without sidebar)' , $themename ) , 'value' => '4col')				
						),
						'std'  => '1col'
					),
					array(
						'name' => __( 'Post Box Style' , $themename ),
						'desc' => __( 'Select the style you want to use for the post box' , $themename ),
						'id' => $prefix . 'blog_page_box_style',
						'type' => 'select',
						'options' => array(
							array('name' => __( 'Style 1' , $themename ) , 'value' => 'style-1'),
							array('name' => __( 'Style 2' , $themename ) , 'value' => 'style-2')
						),
					),
					array(
						'name' => __( 'Show Excerpt' , $themename ),
						'desc' => __( 'Whether to show excerpt.' , $themename ),
						'id' => $prefix . 'blog_page_show_excerpt',
						'type' => 'select',
						'options' => $yes_no_options,
					),
					array(
						'name' => __( 'Show Meta' , $themename ),
						'desc' => __( 'Whether to show meta info.' , $themename ),
						'id' => $prefix . 'blog_page_show_meta',
						'type' => 'select',
						'options' => $yes_no_options,
					),
					array(
						'name' => __( 'Show Author Name' , $themename ),
						'desc' => __( 'Whether to show author name.' , $themename ),
						'id' => $prefix . 'blog_page_show_author_name',
						'type' => 'select',
						'options' => $yes_no_options,
					),
					array(
						'name' => __( 'Show Comment Number' , $themename ),
						'desc' => __( 'Whether to show comment number.' , $themename ),
						'id' => $prefix . 'blog_page_show_comments',
						'type' => 'select',
						'options' => $yes_no_options,
					),
					array(
						'name' => __( 'Show Date Tag' , $themename ),
						'desc' => __( 'Whether to show date tag.' , $themename ),
						'id' => $prefix . 'blog_page_show_date_tag',
						'type' => 'select',
						'options' => $yes_no_options,
					),
					array(
						'name' => __( 'Date Tag Color' , $themename ),
						'desc' => __( 'Select the date tag color.' , $themename ),
						'id' => $prefix . 'blog_page_date_tag_color',
						'type' => 'select',
						'options' => $color_options,
					),
					array(
						'name' => __( 'Button Color' , $themename ),
						'desc' => __( 'Select the button color ( only applicable to box style 1 ).' , $themename ),
						'id' => $prefix . 'blog_page_btn_color',
						'type' => 'select',
						'options' => $color_options,
					),
					array(
						'name' => __( 'Category Tag Color' , $themename ),
						'desc' => __( 'Select the category tag color ( only applicable to box style 2 ).' , $themename ),
						'id' => $prefix . 'blog_page_meta_tag_color',
						'type' => 'select',
						'options' => $color_options,
					),
				)
			);

			// Meta Boxes For Blog Page Template
			$meta_boxes[] = array(
				'id'         => 'canvas_page_metabox',
				'title'      => __( 'Canvas Options' , $themename ),
				'pages'      => array( 'page', ), // Post type
				'context'    => 'normal',
				'priority'   => 'high',
				'show_names' => true, // Show field names on the left
				'show_on'    => array( 'key' => 'page-template', 'value' => 'page-canvas.php' ), // Specific page template to display this metabox
				'fields' => array(
					array(
						'name' => __( 'Show Page Title' , $themename ),
						'desc' => '',
						'id' => $prefix . 'canvas_show_page_title',
						'type' => 'checkbox'
					),
				)
			);

			/********* Metaboxes for Posts ************/

			$meta_boxes[] = array(
				'id'         => 'post_metabox',
				'title'      => __( 'Post Options' , $themename ),
				'pages'      => array('post'), // Post type
				'context'    => 'normal',
				'priority'   => 'high',
				'show_names' => true, // Show field names on the left
				'fields'     => array(
					array(
						'name' => __( 'Select Post Template' , $themename ),
						'desc' => __( 'Select the post template you want to display for this post.' , $themename ),
						'id' => $prefix . 'post_select_temp',
						'type' => 'select',
						'options' => array(
							array('name' => __( 'None' , $themename ), 'value' => 'none'),
							array('name' => __( 'Image Slider' , $themename ), 'value' => 'image-carousel'),
							array('name' => __( 'Video Post' , $themename ), 'value' => 'video')				
						)
					),
					array(
						'name' => __( 'Second Image For Silder' , $themename ),
						'desc' => __( 'Upload a second image or enter an URL here. <em>Image format: JPEG, PNG, or GIF only</em>' , $themename ),
						'id' => $prefix . 'video_featured_image_2',
						'type' => 'file',
						'save_id' => true, // save ID using true
						'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
					),
					array(
						'name' => __( 'Third Image For Slider' , $themename ),
						'desc' => __( 'Upload a third image or enter an URL here. <em>Image format: JPEG, PNG, or GIF only</em>' , $themename ),
						'id' => $prefix . 'video_featured_image_3',
						'type' => 'file',
						'save_id' => true, // save ID using true
						'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
					),
					array(
						'name' => __( 'Video URL' , $themename ),
						'desc' => __( 'Enter the video url here. <em>(example: https://vimeo.com/51333291 or https://youtu.be/iOiE6XMy0y8)</em>' , $themename ),
						'id' => $prefix . 'post_video_url',
						'type' => 'text'
					),
					array(
						'name' => __( 'Select Video Type' , $themename ),
						'desc' => '',
						'id' => $prefix . 'post_video_type',
						'type' => 'select',
						'options' => array(
							array('name' => 'Youtube', 'value' => 'youtube'),
							array('name' => 'Vimeo', 'value' => 'vimeo')				
						)
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'post_custom_color_metabox',
				'title'      => __( 'Custom Color Options' , $themename ),
				'pages'      => ( class_exists( 'MarketPress' ) ? array('post','product') : array('post') ), // Post type
				'context'    => 'normal',
				'priority'   => 'high',
				'show_names' => true, // Show field names on the left
				'fields'     => array(
					array(
			            'name' => __( 'Content Section: Background Color' , $themename ),
			            'desc' => __( 'Pick a custom background color for content section.' , $themename ),
			            'id'   => $prefix . 'post_content_bg_color',
			            'type' => 'colorpicker',
						'std'  => ''
			        ),
					array(
			            'name' => __( 'Content Section: Text Color' , $themename ),
			            'desc' => __( 'Pick a custom text color for content section.' , $themename ),
			            'id'   => $prefix . 'post_content_text_color',
			            'type' => 'colorpicker',
						'std'  => ''
			        )
				),
			);

			return $meta_boxes;
		}

	}