<?php

	/* Product Listing functions
	------------------------------------------------------------------------------------------------------------------- */

	function pro_advance_product_sort( $args = array() ) {

		$defaults = array(
			'unique_id' => '',
			'sort' => false,
			'align' => 'align-right',
			'context' => 'list',
			'enablecustomqueries' => false,
			'customqueries' => NULL,
			'echo' => true,
			'paginate' => true,
			'page' => '',
			'per_page' => 9,
			'order_by' => 'date',
			'order' => 'DESC',
			'category' => '',
			'tag' => '',
			's' => '',
			'sentence' => '',
			'exact' => '',
			'counter' => '3',
			'span' => 'span4',
			'btnclass' => '',
			'iconclass' => '',
			'labelcolor' => '',
			'boxclass' => '',
			'boxstyle' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		$output = '';

		if ($sort) {

			global $mp;

			do_action( 'mpt_start_session_hook' , 'pro_advanced_sort' );

			if(isset($_POST['advancedsortformsubmitted'.$unique_id])) {

				if ($context == 'list') {
					$_SESSION['advancedsortcategory'.$unique_id] = (!empty($_POST['advancedsortcategory'.$unique_id])) ? esc_html(esc_attr(trim($_POST['advancedsortcategory'.$unique_id]))) : '';
				} else {
					$_SESSION['advancedsortcategory'.$unique_id] = '';
				}
				$_SESSION['advancedsortby'.$unique_id] = (!empty($_POST['advancedsortby'.$unique_id])) ? esc_html(esc_attr(trim($_POST['advancedsortby'.$unique_id]))) : '';
				$_SESSION['advancedsortdirection'.$unique_id] = (!empty($_POST['advancedsortdirection'.$unique_id])) ? esc_html(esc_attr(trim($_POST['advancedsortdirection'.$unique_id]))) : '';
			}

			$category = (!empty($_SESSION['advancedsortcategory'.$unique_id])) ? esc_html(esc_attr(trim($_SESSION['advancedsortcategory'.$unique_id]))) : $category;
			$order_by = (!empty($_SESSION['advancedsortby'.$unique_id])) ? esc_html(esc_attr(trim($_SESSION['advancedsortby'.$unique_id]))) : $order_by;
			$order = (!empty($_SESSION['advancedsortdirection'.$unique_id])) ? esc_html(esc_attr(trim($_SESSION['advancedsortdirection'.$unique_id]))) : $order;

			$output = apply_filters( 'pro_listing_before_advanced_sort' , $output , $unique_id , $context , $category , $tag );

			$output .= '<div id="advanced-sort" class="'.$align.'">';
				$output .= '<a href="#adv-sort" role="button" data-toggle="modal" class="btn btn-12-19'.$btnclass.'"><i class="icon-th'.$iconclass.' icon-large"></i> '.__('Advanced Sort' , 'pro').'</a>';
				$output .= '<div class="clear padding20"></div>';
			$output .= '</div>';

			// Advanced Modal
			$output .= '<div id="adv-sort" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="advancedSort" aria-hidden="true">';
				
				$output .= '<div class="modal-header">';
			    	$output .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
			    	$output .= '<h4>'.__('Advanced Sort' , 'pro').'</h4>';
				$output .= '</div>';

				$output .= '<div class="modal-body">';

					$output .= '<div class="row-fluid">';

						$output .= '<form class="form-horizontal" action="'.esc_url( $_SERVER["REQUEST_URI"] ).'#advanced-sort" id="advanced-sort-form" method="post">';

							if ($context == 'list') {

								// By Category   
								$output .= '<div class="input-prepend">';
									$output .= '<span class="add-on">'.__('By Category:' , 'pro').'</span>';
									$output .= '<select name="advancedsortcategory'.$unique_id.'" id="advancedsortcategory'.$unique_id.'">';
										$output .= '<option value="">'.__('Show All' , 'pro').'</option>';

											$args = array(
												'taxonomy' => 'product_category',
												'orderby' => 'name',
												'order' => 'ASC'
											  );

											$categories = get_categories($args);

											if  ($categories) {
											  foreach($categories as $cat) {
											    $output .= '<option value="'.$cat->category_nicename.'"'.($cat->category_nicename == $category ? ' selected="selected"' : '').'>'.$cat->name.'</option>';
											  }
											}

									$output .= '</select>';
								$output .= '</div>';

								$output .= '<div class="clear padding10"></div>';

							}

							// Sort By
							$output .= '<div class="input-prepend">';
								$output .= '<span class="add-on">'.__('Sort By' , 'pro').':</span>';
								$output .= '<select name="advancedsortby'.$unique_id.'" id="advancedsortby'.$unique_id.'">';
									$output .= '<option value="date"'.($order_by == 'date' ? ' selected="selected"' : '').'>'.__('Release Date' , 'pro').'</option>';
									$output .= '<option value="title"'.($order_by == 'title' ? ' selected="selected"' : '').'>'.__('Name' , 'pro').'</option>';
									$output .= '<option value="price"'.($order_by == 'price' ? ' selected="selected"' : '').'>'.__('Price' , 'pro').'</option>';
									$output .= '<option value="sales"'.($order_by == 'sales' ? ' selected="selected"' : '').'>'.__('Sales' , 'pro').'</option>';
								$output .= '</select>';
							$output .= '</div>';

							$output .= '<div class="clear padding10"></div>';

							// Sort Direction
							$output .= '<div class="input-prepend">';
								$output .= '<span class="add-on">'.__('Sort Direction' , 'pro').':</span>';
								$output .= '<select name="advancedsortdirection'.$unique_id.'" id="advancedsortdirection'.$unique_id.'">';
									$output .= '<option value="DESC"'.($order == 'DESC' ? ' selected="selected"' : '').'>'.__('Descending' , 'pro').'</option>';
									$output .= '<option value="ASC"'.($order == 'ASC' ? ' selected="selected"' : '').'>'.__('Ascending' , 'pro').'</option>';
								$output .= '</select>';
							$output .= '</div>';

							$output .= '<div class="clear padding10"></div>';

							$output .= '<button type="submit" class="btn'.$btnclass.' advanced-sort-btn pull-right" data-toggle="button"><i class="icon-th'.$iconclass.'"></i> '.__('Sort' , 'pro').'</button>';

							$output .= '<input type="hidden" name="advancedsortformsubmitted'.$unique_id.'" id="advancedsortformsubmitted'.$unique_id.'" value="true" />';

						$output .= '</form>';

				    $output .= '</div>'; // row-fluid

				$output .= '</div>'; // modal body

			$output .= '</div>'; // advanced-sort

			$output = apply_filters( 'pro_listing_after_advanced_sort' , $output , $unique_id , $context , $category , $tag );

		}

		$output = apply_filters( 'pro_listing_before_grid' , $output , $unique_id , $context , $category , $tag );

			$grid_args = array(
				'enablecustomqueries' => $enablecustomqueries,
				'customqueries' => $customqueries,
				'echo' => false,
				'paginate' => $paginate,
				'page' => $page,
				'per_page' => $per_page,
				'order_by' => $order_by,
				'order' => $order,
				'category' => $category,
				'tag' => $tag,
				'counter' => $counter,
				'span' => $span,
				'btnclass' => $btnclass,
				'iconclass' => $iconclass,
				'labelcolor' => $labelcolor,
				'boxclass' => $boxclass,
				'boxstyle' => $boxstyle
				);

			$output .= pro_list_product_in_grid( $grid_args );

		$output = apply_filters( 'pro_listing_after_grid' , $output , $unique_id , $context , $category , $tag );

	  	if ($echo)
	    	echo apply_filters( 'func_pro_advance_product_sort' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_advance_product_sort' , $output , $instance );

	}

	function pro_list_product_in_grid( $args = array() ) {

		$defaults = array(
			'enablecustomqueries' => false,
			'customqueries' => NULL,
			'echo' => true,
			'paginate' => true,
			'page' => '',
			'per_page' => 9,
			'order_by' => 'date',
			'order' => 'DESC',
			'category' => '',
			'tag' => '',
			's' => '',
			'sentence' => '',
			'exact' => '',
			'counter' => '3',
			'span' => 'span4',
			'btnclass' => '',
			'iconclass' => '',
			'labelcolor' => '',
			'boxclass' => '',
			'boxstyle' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		global $wp_query, $mp;

		//setup taxonomy if applicable
		if ($category) {
		    $taxonomy_query = '&product_category=' . sanitize_title($category);
		} else if ($tag) {
		    $taxonomy_query = '&product_tag=' . sanitize_title($tag);
		} else if (isset($wp_query->query_vars['taxonomy']) && ($wp_query->query_vars['taxonomy'] == 'product_category' || $wp_query->query_vars['taxonomy'] == 'product_tag')) {
		    $term = get_queried_object(); //must do this for number tags
		    $taxonomy_query = '&' . $term->taxonomy . '=' . $term->slug;
		} else {
		    $taxonomy_query = '';
		}
		  
		//setup pagination
		$paged = false;
		if ($paginate) {
		  	if ($paginate === 'nopagingblock') {
		  		$paginate_query = '&showposts='.intval($per_page);
		  	} else {
		  		$paged = true;	
		  	}
		} else if ($paginate === '') {
		    if ($mp->get_setting('paginate'))
		      $paged = true;
		    else
		      $paginate_query = '&nopaging=true';
		} else {
		    $paginate_query = '&nopaging=true';
		}

		//get page details
		if ($paged) {
		    //figure out perpage
		    if (intval($per_page)) {
		      $paginate_query = '&posts_per_page='.intval($per_page);
		    } else {
		      $paginate_query = '&posts_per_page='.$mp->get_setting('per_page');
			}

		    //figure out page
		    if (isset($wp_query->query_vars['paged']) && $wp_query->query_vars['paged'])
		      	$paginate_query .= '&paged='.intval($wp_query->query_vars['paged']);

		    if (intval($page))
		      	$paginate_query .= '&paged='.intval($page);
		    else if ( $wp_query->query_vars['paged'] )
		      	$paginate_query .= '&paged='.intval($wp_query->query_vars['paged']);
		    else if ( get_query_var('page') )
		      	$paginate_query .= '&paged='.intval(get_query_var('page'));
		    else
		    	$paginate_query .= '&paged=1';
		}

		//get order by
		if (!$order_by) {
		    if ($mp->get_setting('order_by') == 'price')
		      $order_by_query = '&meta_key=mp_price_sort&orderby=meta_value_num';
		    else if ($mp->get_setting('order_by') == 'sales')
		      $order_by_query = '&meta_key=mp_sales_count&orderby=meta_value_num';
		    else
		      $order_by_query = '&orderby='.$mp->get_setting('order_by');
		} else {
		  	if ('price' == $order_by)
		  		$order_by_query = '&meta_key=mp_price_sort&orderby=meta_value_num';
		    else if('sales' == $order_by)
		      $order_by_query = '&meta_key=mp_sales_count&orderby=meta_value_num';
		    else
		    	$order_by_query = '&orderby='.$order_by;
		}

		//get order direction
		if (!$order) {
		    $order_query = '&order='.$mp->get_setting('order');
		} else {
		    $order_query = '&order='.$order;
		}

		//The Query
		if ( $enablecustomqueries && !empty($customqueries) ) {
			$the_query = $customqueries;
		} else {
			$the_query = new WP_Query('post_type=product&post_status=publish' . $taxonomy_query . $paginate_query . $order_by_query . $order_query);
		}

		$output = '<div id="pro-product-grid">';

			$count = 1;
			$incomplete_row = false;

			if ($the_query->have_posts()) : 

				while ($the_query->have_posts()) : $the_query->the_post();

				$product_id = get_the_ID();

					$incomplete_row = true;
					$output .= ( $count == 1 ? '<div class="row-fluid">' : '' );				  	

				  	$single_args = array(
						'echo' => false,
						'post_id' => $product_id,
						'span' => $span,
						'imagesize' => 'tb-360',
						'btnclass' => $btnclass,
						'iconclass' => $iconclass,
						'labelcolor' => $labelcolor,
						'class' => $boxclass,
						'style' => $boxstyle
				  	);

					$output .= pro_load_single_product_in_box( $single_args );
					
					if ($count == $counter) {
						$incomplete_row = false;
						$count = 0;
						$output .= '</div>';
					}

					$count++;

				endwhile;

			else :

				// If nothing found
				$output .= '<div class="page-content">';
					$output .= '<h2>'.__( 'Nothing Found.' , 'pro' ).'</h2>';
					$output .= '<p>' . __('Perhaps try one of the links below:' , 'pro' ) . '</p>';
					$output .= '<div class="padding10"></div>';
						$output .= '<h4>' . __( 'Most Used Categories' , 'pro' ) . '</h4>';
						$output .= '<ul>';
							$output .= wp_list_categories( array( 'taxonomy' => 'product_category' , 'orderby' => 'count', 'order' => 'DESC', 'show_count' => 1, 'title_li' => '', 'number' => 10 , 'echo' => 0 ) );
						$output .= '</ul>';
					$output .= '<div class="padding20"></div>';
					$output .= '<p>' . __( 'Or, use the search box below:' , 'pro' ) . '</p>';
					$output .= get_search_form( false );
				$output .= '</div>';

			endif;
			wp_reset_query();

			if ( $incomplete_row )
				$output .= '</div>'; // End - row-fluid

		$output .= '</div>'; // End - pro-product-grid
		$output .= '<div class="clear"></div>';

		// load pagination
		if ($paged) {

			$output .= '<div id="pro-product-grid-pagination" class="pull-right">';

			$total_pages = $the_query->max_num_pages;  

		    if ($total_pages > 1){ 

		    	if ( get_query_var('paged') ) {
		    		$current_page = max(1, get_query_var('paged') ); 
		    	} elseif ( get_query_var('page') ) {
		    		$current_page = max(1, get_query_var('page') ); 
		    	} else {
		    		$current_page = 1;
		    	}

		    	// check if current page is product listing page
		    	$productlistingslug = $mp->get_setting('slugs->store'). '/'.$mp->get_setting('slugs->products');
		    	$currenturl = get_pagenum_link($current_page);
		    	$isproductlising = strstr($currenturl, $productlistingslug);

		    	if ( (is_home() || is_front_page()) && $isproductlising == false ) {
		    		$paginate_links_query = 'base='.get_home_url().'%_%&current=' . $current_page .'&total=' . $total_pages . '&type=list&prev_text='.__('« Previous' , 'pro').'&next_text=' . __('Next »' , 'pro');
		    	} else {
		    		$paginate_links_query = 'base='.get_pagenum_link(1).'%_%&format=page/%#%&current=' . $current_page .'&total=' . $total_pages . '&type=list&prev_text='.__('« Previous' , 'pro').'&next_text=' . __('Next »' , 'pro');
		    	}

			    $output .= '<div class="pagination">';
			    	$output .= paginate_links( $paginate_links_query );
		       	$output .= '</div>';
		    }

			$output .= '</div>'; // End pro-product-grid-pagination
			$output .= '<div class="clear"></div>';
		}

		wp_reset_query();

	  	if ($echo)
	    	echo apply_filters( 'func_pro_list_product_in_grid' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_list_product_in_grid' , $output , $instance );

	}

	function pro_load_single_product_in_box( $args = array() ) {

		$defaults = array(
			'echo' => true,
			'post_id' => NULL,
			'span' => 'span4',
			'imagesize' => 'tb-360',
			'btnclass' => '',
			'iconclass' => '',
			'labelcolor' => '',
			'class' => '',
			'style' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

  		global $id, $mp;
  		$post_id = ( NULL === $post_id ) ? $id : $post_id;

		$style = (!empty($style) ? ' style="'.$style.'"' : '');

		if (is_multisite()) {
			$blog_id = get_current_blog_id();
		} else {
			$blog_id = 1;
		}

		if ($class == 'thetermsclass') {
			$class = '';
		  	$terms = get_the_terms( $post_id, 'product_category' );
			if (!empty($terms)) {
				foreach ($terms as $thesingleterm) {
					$class .= ' '.$thesingleterm->slug;
				}
			}
		}

		switch ($span) {
			case 'span6':
				$minheight = '30px';
				$maxwidth = '560';
				break;
			case 'span3':
				$minheight = '50px';
				$maxwidth = '360';
				break;
			case 'span4':
			default:
				$minheight = '50px';
				$maxwidth = '360';
				break;
		}

		$output = '<div class="'.$span.' well'.$class.'"'.$style.'>';

		$proprice_args = array(
			'echo' => false,
			'post_id' => $post_id,
			'label' => false,
			'context' => ''
		);

		$probtn_args = array(
			'echo' => false,
			'post_id' => $post_id,
			'btnclass' => $btnclass . ' btn-block',
			'context' => 'list'
		);

		if ( has_post_thumbnail( $post_id ) ) {

			$fullimage = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' );

			$pro_image_output = '<div class="product-image-box" style="max-width: '.$maxwidth.'px;">';
				
				// product image
				$pro_image_output .= '<a href="'.get_permalink($post_id).'" class="product-image-link">' . get_the_post_thumbnail($post_id , $imagesize) . '</a>'; 
				
				// Product price
				$pro_image_output .= apply_filters( 'pro_product_box_price' , '<span class="label proprice'.$labelcolor.' hidden-phone">'.pro_product_price( $proprice_args ).'</span>' , $instance );

				// pinit button
				$pro_image_output .= ( function_exists('mp_pinit_button') ? '<span class="product-image-pinit-btn">' . mp_pinit_button( $post_id , 'all_view' ) . '</span>' : '' );

				$pro_image_output .= '<div class="hover-block hidden-phone">';
					$pro_image_output .= '<div class="btn-group">';
						$button = '<a href="'.$fullimage[0].'" class="btn'.$btnclass. ' mpt-pp-trigger" data-pptitle="'.apply_filters( 'pro_product_box_prettyphoto_caption' , get_the_title( $post_id ) , $post_id ).'" title="'.__( 'View Product Image' , 'pro' ).'"><i class="icon-zoom-in'.$iconclass.'"></i></a>';
						$button .= '<a href="'.get_permalink($post_id).'" class="btn'.$btnclass.'" title="'.__( 'More Details' , 'pro' ).'"><i class="icon-list-alt'.$iconclass.'"></i></a>';
						$pro_image_output .= apply_filters( 'pro_product_box_btn_group' , $button , $instance );	
					$pro_image_output .= '</div>'; // End btn-group
				$pro_image_output .= '</div>'; // End hover-block

			$pro_image_output .= '</div>'; // End product-image-box

			$output .= apply_filters('pro_product_box_image' , $pro_image_output , $instance );

		} else {

			//$default_img_url = esc_url( get_option('mpt_mp_default_product_img') );
			$default_img_url = esc_url( get_blog_option(1,'mpt_mp_default_product_img') );
			$default_img_id = ( !empty($default_img_url) ? intval( get_image_id( $default_img_url ) ) : NULL );
			$pro_default_image_output = '';

			if ( !empty($default_img_id) && is_numeric($default_img_id) ) {
				$pro_default_image_output .= '<div class="product-image-box" style="max-width: '.$maxwidth.'px;">';
					// product image
					$pro_default_image_output .= '<a href="'.get_permalink($post_id).'" class="product-image-link">'.wp_get_attachment_image( $default_img_id, $imagesize ).'</a>'; 
					// Product price
					$pro_default_image_output .= apply_filters( 'pro_product_box_price' , '<span class="label proprice'.$labelcolor.' hidden-phone">'.pro_product_price( $proprice_args ).'</span>' , $instance );
				$pro_default_image_output .= '</div>'; // End product-image-box
			} 

			$output .= apply_filters('pro_product_box_image_default' , $pro_default_image_output , $instance );
		}

			$output .= '<div class="pro-product-box-contents align-center">';

				$output .= '<div class="hidden-phone align-center">';
					$output .= '<div class="product-meta" style="min-height: '.$minheight.'">';
						$output .= apply_filters( 'pro_product_box_title' , '<a href="'.get_permalink($post_id).'">'.get_the_title($post_id) . '</a>' , $post_id , $blog_id );	
					$output .= '</div>'; // End product-meta
				$output .= '</div>'; // End - hidden-phone

				$output .= '<p class="visible-phone">';
					$output .= apply_filters( 'pro_product_box_title' , '<a href="'.get_permalink($post_id).'">'.get_the_title($post_id) . '</a>' , $post_id , $blog_id ).'<br />';
					$output .= apply_filters( 'pro_product_box_price_mobile' , '<span class="label'.$labelcolor.'">'.pro_product_price( $proprice_args ).'</span>' , $post_id , $blog_id , $labelcolor );
				$output .= '</p>'; // End - visible-phone

				$output = apply_filters('pro_product_box_before_buy_button' , $output , $post_id , $blog_id );					
					$output .= pro_buy_button( $probtn_args );
				$output = apply_filters('pro_product_box_after_buy_button' , $output , $post_id , $blog_id );

			$output .= '</div>'; // End pro-product-box-contents

		$output .= '</div>'; // End Well

  	if ($echo)
    	echo apply_filters( 'func_pro_load_single_product_in_box' , $output , $instance );
  	else
    	return apply_filters( 'func_pro_load_single_product_in_box' , $output , $instance );
		
	}

	/* Product price functions
	------------------------------------------------------------------------------------------------------------------- */

	function pro_product_price( $args = array() ) {

		$defaults = array(
			'echo' => false,
			'post_id' => NULL,
			'label' => true,
			'context' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

	  	global $id, $mp;
	  	$post_id = ( NULL === $post_id ) ? $id : $post_id;

	  	$label = ($label === true) ? __('Price: ', 'pro') : $label;

	  	$meta = get_post_custom($post_id);

	  	//unserialize
	  	foreach ($meta as $key => $val) {
		  	$meta[$key] = maybe_unserialize($val[0]);
		  	if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file" && $key != "mp_price_sort")
		    	$meta[$key] = array($meta[$key]);
		}

	  	if ((is_array($meta["mp_price"]) && count($meta["mp_price"]) == 1) || !empty($meta["mp_file"])) {
	    	if ($meta["mp_is_sale"]) {
		    	$price = '<span class="mp_special_price"><del class="mp_old_price">'.$mp->format_currency('', $meta["mp_price"][0]).'</del> ';
		    	$price .= '<span itemprop="price" class="mp_current_price">'.$mp->format_currency('', $meta["mp_sale_price"][0]).'</span></span>';
		  	} else {
		    	$price = '<span itemprop="price" class="mp_normal_price functions_pro"><span class="mp_current_price">'.$mp->format_currency('', $meta["mp_price"][0]).'</span></span>';
		  	}
		} else if (is_array($meta["mp_price"]) && count($meta["mp_price"]) > 1 && !is_singular('product')) { //only show from price in lists
		    if ($meta["mp_is_sale"]) {
			        //do some crazy stuff here to get the lowest price pair ordered by sale prices
			      	asort($meta["mp_sale_price"], SORT_NUMERIC);
			      	$lowest = array_slice($meta["mp_sale_price"], 0, 1, true);
			      	$keys = array_keys($lowest);
			      	$mp_price = $meta["mp_price"][$keys[0]];
			     	$mp_sale_price = array_pop($lowest);
				    $price = __('From', 'pro').' <span class="mp_special_price"><del class="mp_old_price">'.$mp->format_currency('', $mp_price).'</del>';
				    $price .= '<span itemprop="price" class="mp_current_price">'.$mp->format_currency('', $mp_sale_price).'</span></span>';
			  	} else {
		      		sort($meta["mp_price"], SORT_NUMERIC);
			    	$price = __('From', 'pro').' <span itemprop="price" class="mp_normal_price"><span class="mp_current_price">'.$mp->format_currency('', $meta["mp_price"][0]).'</span></span>';
			  	}
		} else {
			if ($context == 'widget') {
			    if ($meta["mp_is_sale"]) {
			        //do some crazy stuff here to get the lowest price pair ordered by sale prices
			      	asort($meta["mp_sale_price"], SORT_NUMERIC);
			      	$lowest = array_slice($meta["mp_sale_price"], 0, 1, true);
			      	$keys = array_keys($lowest);
			      	$mp_price = $meta["mp_price"][$keys[0]];
			     	$mp_sale_price = array_pop($lowest);
				    $price = __('From', 'pro').' <span class="mp_special_price"><del class="mp_old_price">'.$mp->format_currency('', $mp_price).'</del>';
				    $price .= '<span itemprop="price" class="mp_current_price">'.$mp->format_currency('', $mp_sale_price).'</span></span>';
				} else {
					if ( !empty($meta["mp_price"]) )
			      		sort($meta["mp_price"], SORT_NUMERIC);
				    $price = __('From', 'pro').' <span itemprop="price" class="mp_normal_price"><span class="mp_current_price">'.$mp->format_currency('', $meta["mp_price"][0]).'</span></span>';
				}
			} else {
				return '';
			}
		}

	  	$price = apply_filters( 'pro_product_price_tag', $label . $price, $post_id, $label );

	  	if ($echo)
	    	echo apply_filters( 'func_pro_product_price' , $price , $instance );
	  	else
	    	return apply_filters( 'func_pro_product_price' , $price , $instance );
	}

	/* Buy button function
	------------------------------------------------------------------------------------------------------------------- */

	function pro_buy_button( $args = array() ) {

		$defaults = array(
			'echo' => false,
			'post_id' => NULL,
			'btnclass' => '',
			'context' => 'list'
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

	  	global $id, $mp;
	  	$post_id = ( NULL === $post_id ) ? $id : $post_id;

	  	$meta = get_post_custom($post_id);
	  	//unserialize
	  	foreach ($meta as $key => $val) {
		  	$meta[$key] = maybe_unserialize($val[0]);
		  	if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file")
		    	$meta[$key] = array($meta[$key]);
		}

	  	//check stock
	  	$no_inventory = array();
	  	$all_out = false;
	  	if ($meta['mp_track_inventory']) {

	    $cart = $mp->get_cart_contents();
	    if (isset($cart[$post_id]) && is_array($cart[$post_id])) {

		    	foreach ($cart[$post_id] as $variation => $data) {
			      if ($meta['mp_inventory'][$variation] <= $data['quantity'])
			        $no_inventory[] = $variation;
				}

				foreach ($meta['mp_inventory'] as $key => $stock) {
			      if (!in_array($key, $no_inventory) && $stock <= 0)
			        $no_inventory[] = $key;
				}
			}

			//find out of stock items that aren't in the cart
			foreach ($meta['mp_inventory'] as $key => $stock) {
	      	if (!in_array($key, $no_inventory) && $stock <= 0)
	        	$no_inventory[] = $key;
			}

			if (count($no_inventory) >= count($meta["mp_price"]))
			  $all_out = true;
	  	}

	  	//display an external link or form button
	  	if (isset($meta['mp_product_link']) && $product_link = $meta['mp_product_link']) {

	    	$button = '<a class="mp_link_buynow btn'.$btnclass.'" href="' . esc_url($product_link) . '">' . __('Buy Now', 'pro') . '</a>';

	  	} else if ($mp->get_setting('disable_cart')) {
	    
	    	$button = '';
	    
	  	} else {

		    $variation_select = '';
		    $button = '<form class="mp_buy_form" method="post" action="' . mp_cart_link(false, true) . '">';

		    if ($all_out) {
		      $button .= '<span class="mp_no_stock btn disabled'.$btnclass.'">' . __('Out of Stock', 'pro') . '</span>';
		    } else {

			    $button .= '<input type="hidden" name="product_id" value="' . $post_id . '" />';

					//create select list if more than one variation
				    if (is_array($meta["mp_price"]) && count($meta["mp_price"]) > 1 && empty($meta["mp_file"])) {

				    	$variation_select = '<div class="input-prepend">';

				    	$variation_select .= '<span class="add-on">'.__('Choose', 'pro').'</span>';

			      		$variation_select .= '<select class="mp_product_variations" name="variation">';

						foreach ($meta["mp_price"] as $key => $value) {

						  $disabled = (in_array($key, $no_inventory)) ? ' disabled="disabled"' : '';
						  $variation_select .= '<option value="' . $key . '"' . $disabled . '>' . esc_html($meta["mp_var_name"][$key]) . ' - ';
							
						  if ($meta["mp_is_sale"] && $meta["mp_sale_price"][$key]) {

				        		$variation_select .= $mp->format_currency('', $meta["mp_sale_price"][$key]);
				     	 	
				     	 	} else {

				        		$variation_select .= $mp->format_currency('', $value);

				      		}

				      		$variation_select .= "</option>\n";
				    	
				    	}

			      		$variation_select .= "</select>&nbsp;\n";

			      		$variation_select .= '</div>'; // End input-prepend

			 		} else {
			 			// No variation
				      	$button .= '<input type="hidden" name="variation" value="0" />';
					}

					if ( $context == 'list' ) {

				      	if ($variation_select) {
			        		$button .= '<a href="' . get_permalink($post_id) . '" class="mp_link_buynow btn'.$btnclass.'">' . __('Choose Option', 'pro') . '</a>';
				      	} else if ($mp->get_setting('list_button_type') == 'addcart') {
				        	$button .= '<input type="hidden" name="action" value="mp-update-cart" />';
				        	$button .= '<input class="mp_button_addcart btn'.$btnclass.'" type="submit" name="addcart" value="' . __('Add To Cart', 'pro') . '" />';
				      	} else if ($mp->get_setting('list_button_type') == 'buynow') {
				        	$button .= '<input class="mp_button_buynow btn'.$btnclass.'" type="submit" name="buynow" value="' . __('Buy Now', 'pro') . '" />';
				      	}

					} else {

				      	$button .= $variation_select;

					    //add quantity field if not downloadable
					    if ($mp->get_setting('show_quantity') && empty($meta["mp_file"])) {
					      	$button .= '<div class="input-prepend">';
					        $button .= '<span class="mp_quantity add-on">' . __('Quantity', 'pro') . '</span>
					        			<input class="mp_quantity_field input-mini" type="text" name="quantity" value="1" />';
					       	$button .= '</div>';
					    }

					    if ($mp->get_setting('product_button_type') == 'addcart') {
					        $button .= '<input type="hidden" name="action" value="mp-update-cart" />';
					        $button .= '<input class="mp_button_addcart btn'.$btnclass.'" type="submit" name="addcart" value="' . __('Add To Cart', 'pro') . '" />';
					    } else if ($mp->get_setting('product_button_type') == 'buynow') {
					        $button .= '<input class="mp_button_buynow btn'.$btnclass.'" type="submit" name="buynow" value="' . __('Buy Now', 'pro') . '" />';
					    }

					}		  
		    }

	    $button .= '</form>';
	  }

	  $button = apply_filters( 'pro_buy_button_tag', $button, $instance );

	  if ($echo)
	    echo apply_filters( 'func_pro_buy_button' , $button , $instance );
	  else
	    return apply_filters( 'func_pro_buy_button' , $button , $instance );
	}	

	/* Check for OOS
	------------------------------------------------------------------------------------------------------------------- */

	function pro_check_oos( $post_id = NULL ) {

		global $id, $mp;
		$post_id = ( NULL === $post_id ) ? $id : $post_id;

		$meta = get_post_custom($post_id);
		//unserialize
		foreach ($meta as $key => $val) {
				$meta[$key] = maybe_unserialize($val[0]);
				if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file")
						$meta[$key] = array($meta[$key]);
		}

		$no_inventory = array();
		$all_out = false;
		if ( $meta['mp_track_inventory'] ) {
				$cart = $mp->get_cart_contents();
				if (isset($cart[$post_id]) && is_array($cart[$post_id])) {
						foreach ($cart[$post_id] as $variation => $data) {
								if ($meta['mp_inventory'][$variation] <= $data['quantity'])
										$no_inventory[] = $variation;
						}
						foreach ($meta['mp_inventory'] as $key => $stock) {
								if (!in_array($key, $no_inventory) && $stock <= 0)
										$no_inventory[] = $key;
						}
				}

				//find out of stock items that aren't in the cart
				if ( !empty($meta['mp_inventory']) ) {
					foreach ($meta['mp_inventory'] as $key => $stock) {
							if (!in_array($key, $no_inventory) && $stock <= 0)
									$no_inventory[] = $key;
					}
				}

				if (count($no_inventory) >= count($meta["mp_price"]))
						$all_out = true;
		}

		return apply_filters( 'func_pro_check_oos' , $all_out , $post_id );
	}

	/* Product images functions
	------------------------------------------------------------------------------------------------------------------- */

	function pro_check_multi_product_image($id) {

		$counter = 0;

	   	for ($i=2; $i <= 10; $i++) {

	   		$image = get_post_meta( $id, '_mpt_video_featured_image_'.$i, true );

	   		if (!empty($image)) {
			    $counter++;
	   		}
	   	}

	   	$moreimages = get_post_meta( $id, '_mpt_more_product_images' , true );
	   	if ( !empty( $moreimages ) && is_array( $moreimages ) ) {
	   		foreach ( $moreimages as $image_id => $image_url ) {
	   			if ( !empty( $image_url ) && esc_url( $image_url ) )
	   				$counter++;
	   		}
	   	}

	   	if ($counter == 0) {
	   		return apply_filters( 'func_pro_check_multi_product_image' , false , $id );
	   	} else {
	   		return apply_filters( 'func_pro_check_multi_product_image' , true , $id );
	   	}
	}

	/* Floating Menu functions
	------------------------------------------------------------------------------------------------------------------- */

	function pro_floating_menu( $echo = true ) {

		//$floatingcart = get_option('mpt_enable_floating_cart');
		//$globalstore = get_option('mpt_enable_global_store');
		
		$floatingcart = get_blog_option(1,'mpt_enable_floating_cart');
		$globalstore = get_blog_option(1,'mpt_enable_global_store');

		$output = '';

		if ( ( $floatingcart != 'false' || $globalstore == 'true' ) && class_exists( 'MarketPress' ) ) {
			$output .= '<div id="floating-menu" class="hidden-phone add-fadeinright-effects-parent">';
				$output = apply_filters( 'floating_menu_before_cart_btn' , $output );
				$output .= apply_filters( 'floating_menu_cart_btn' , ( $floatingcart != 'false' ? pro_floating_cart_load_button(false) : '' ) , $floatingcart , $globalstore );
				$output .= apply_filters( 'floating_menu_store_btn' , ( $globalstore == 'true' ? pro_global_store_load_button(false) : '' ) , $floatingcart , $globalstore );
				$output = apply_filters( 'floating_menu_after_store_btn' , $output );
			$output .= '</div>';
			$output .= apply_filters( 'floating_menu_cart_modal' , ( $floatingcart != 'false' ? pro_floating_cart_load_modal(false) : '' ) , $floatingcart , $globalstore );
			$output .= apply_filters( 'floating_menu_store_modal' , ( $globalstore == 'true' ? pro_global_store_load_modal(false) : '' ) , $floatingcart , $globalstore );
		}

  		if ($echo)
    		echo apply_filters( 'func_pro_floating_menu' , $output , $floatingcart , $globalstore );
  		else
    		return apply_filters( 'func_pro_floating_menu' , $output , $floatingcart , $globalstore );
	}

	function pro_floating_cart_load_button( $echo = true  ) {

		//$cartbtncolor = get_option('mpt_floating_cart_button_color');
		$cartbtncolor = get_blog_option(1,'mpt_floating_cart_button_color');
		switch ($cartbtncolor) {
			case 'Grey':
				$cartbtncolor = '';
				$carticoncolor = '';
				break;

			case 'Blue':
				$cartbtncolor = ' btn-primary';
				$carticoncolor = ' icon-white';
				break;

			case 'Light Blue':
				$cartbtncolor = ' btn-info';
				$carticoncolor = ' icon-white';
				break;

			case 'Green':
				$cartbtncolor = ' btn-success';
				$carticoncolor = ' icon-white';
				break;

			case 'Red':
				$cartbtncolor = ' btn-danger';
				$carticoncolor = ' icon-white';
				break;

			case 'Yellow':
				$cartbtncolor = ' btn-warning';
				$carticoncolor = ' icon-white';
				break;

			case 'Black':
				$cartbtncolor = ' btn-inverse';
				$carticoncolor = ' icon-white';
				break;
			
			default:
				$cartbtncolor = '';
				$carticoncolor = '';
				break;
		}

		$output = '<button class="btn btn-large'.$cartbtncolor.' add-fadeinright-effects-child" type="button" data-toggle="modal" data-target="#cart-section">';
			$output .= '<i class="icon-shopping-cart'.$carticoncolor.'"></i>';
			$output .= ' <small>'.pro_load_floating_cart_text_items( false ).'</small>';
		$output .= '</button>';

  		if ($echo)
    		echo apply_filters( 'func_pro_floating_cart_load_button' , $output , $echo );
  		else
    		return apply_filters( 'func_pro_floating_cart_load_button' , $output , $echo );
	}

	function pro_floating_cart_load_modal( $echo = true ) {

		//$cartbtncolor = get_option('mpt_floating_cart_button_color');
		$cartbtncolor = get_blog_option(1,'mpt_floating_cart_button_color');
		switch ($cartbtncolor) {
			case 'Grey':
				$cartbtncolor = '';
				break;

			case 'Blue':
				$cartbtncolor = ' btn-primary';
				break;

			case 'Light Blue':
				$cartbtncolor = ' btn-info';
				break;

			case 'Green':
				$cartbtncolor = ' btn-success';
				break;

			case 'Red':
				$cartbtncolor = ' btn-danger';
				break;

			case 'Yellow':
				$cartbtncolor = ' btn-warning';
				break;

			case 'Black':
				$cartbtncolor = ' btn-inverse';
				break;
			
			default:
				$cartbtncolor = '';
				break;
		}

		$output = '<div id="cart-section" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="cartsection" aria-hidden="true">';
			$output .= '<div class="modal-header">';
				$output .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
				$output .= '<h3 id="cartsection">'.__( 'Shopping Cart', 'pro' ).'</h3>';
			$output .= '</div>';
			$output .= '<div class="modal-body">';
				$output = apply_filters( 'floating_cart_before_cart' , $output );
				$output .= '<div id="mp_cart_page" class="mp_cart_widget_content">'.mp_show_cart('widget' , NULL , false).'</div>';
				$output = apply_filters( 'floating_cart_after_cart' , $output );
			$output .= '</div>';
			$output .= '<div class="modal-footer">';
				$output .= '<button class="btn'.$cartbtncolor.'" data-dismiss="modal" aria-hidden="true">'.__( 'Close', 'pro' ).'</button>';
			$output .= '</div>';

		$output .= '</div>';

  		if ($echo)
    		echo apply_filters( 'func_pro_floating_cart_load_modal' , $output , $echo );
  		else
    		return apply_filters( 'func_pro_floating_cart_load_modal' , $output , $echo );
	}

	function pro_global_store_load_button( $echo = true ) {

		//$btncolor = get_option('mpt_global_store_button_color');
		//$showtext = get_option('mpt_global_store_show_button_text');
		//$text = esc_attr(get_option('mpt_global_store_text'));
		
		$btncolor = get_blog_option(1,'mpt_global_store_button_color');
		$showtext = get_blog_option(1,'mpt_global_store_show_button_text');
		$text = esc_attr(get_blog_option(1,'mpt_global_store_text'));
		
		switch ($btncolor) {
			case 'Grey':
				$btncolor = '';
				$iconcolor = '';
				break;

			case 'Blue':
				$btncolor = ' btn-primary';
				$iconcolor = ' icon-white';
				break;

			case 'Light Blue':
				$btncolor = ' btn-info';
				$iconcolor = ' icon-white';
				break;

			case 'Green':
				$btncolor = ' btn-success';
				$iconcolor = ' icon-white';
				break;

			case 'Red':
				$btncolor = ' btn-danger';
				$iconcolor = ' icon-white';
				break;

			case 'Yellow':
				$btncolor = ' btn-warning';
				$iconcolor = ' icon-white';
				break;

			case 'Black':
				$btncolor = ' btn-inverse';
				$iconcolor = ' icon-white';
				break;
			
			default:
				$btncolor = '';
				$iconcolor = '';
				break;
		}

		$output = '<div class="clear"></div>';
	    $output .= '<button class="btn btn-large'.$btncolor.' btn-global-store" type="button" data-toggle="modal" data-target="#global-store-modal">';
	    	$output .= '<i class="icon-home'.$iconcolor.'"></i>';
	    	$output .= ( $showtext == 'true' ? ' <small><span class="select-store">'.__( $text , 'pro' ).'</span></small>' : '');
	    $output .= '</button>';

  		if ($echo)
    		echo apply_filters( 'func_pro_global_store_load_button' , $output , $echo );
  		else
    		return apply_filters( 'func_pro_global_store_load_button' , $output , $echo );
	}

	function pro_global_store_load_modal( $echo = true ) {

		If ( is_multisite() ) {

			global $wpdb;

		    $query = "SELECT blog_id FROM " . $wpdb->base_prefix . "blogs WHERE spam != '1' AND archived != '1' AND deleted != '1' AND public = '1' ORDER BY path";
		    
		    $blogs = $wpdb->get_results($query);
		}

		//$btncolor = get_option('mpt_global_store_button_color');
		//$text = esc_attr(get_option('mpt_global_store_text'));
		
		$btncolor = get_blog_option(1,'mpt_global_store_button_color');
		$text = esc_attr(get_blog_option(1,'mpt_global_store_text'));
		
		switch ($btncolor) {
			case 'Grey':
				$btncolor = '';
				$iconcolor = '';
				break;

			case 'Blue':
				$btncolor = ' btn-primary';
				$iconcolor = ' icon-white';
				break;

			case 'Light Blue':
				$btncolor = ' btn-info';
				$iconcolor = ' icon-white';
				break;

			case 'Green':
				$btncolor = ' btn-success';
				$iconcolor = ' icon-white';
				break;

			case 'Red':
				$btncolor = ' btn-danger';
				$iconcolor = ' icon-white';
				break;

			case 'Yellow':
				$btncolor = ' btn-warning';
				$iconcolor = ' icon-white';
				break;

			case 'Black':
				$btncolor = ' btn-inverse';
				$iconcolor = ' icon-white';
				break;
			
			default:
				$btncolor = '';
				$iconcolor = '';
				break;
		}


		$output = '<div id="global-store-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="globalstoremodal" aria-hidden="true">';
			$output .= '<div class="modal-header">';
				$output .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
				$output .= '<h3 id="cartsection">'.__( $text , 'pro' ).'</h3>';
			$output .= '</div>';

			$output .= '<div class="modal-body">';

				$output = apply_filters( 'floating_menu_global_store_before_table' , $output );

				// Visit a Shop   
				$output .= '<table class="table table-striped table-hover">';

					if (!empty($blogs)) {
					    foreach($blogs as $blog){
        					$blog_details = get_blog_details($blog->blog_id);
        					$output .= '<tr><td>'.$blog_details->blogname.'</td><td><a href="'.$blog_details->siteurl.'" class="btn btn-small pull-right'.$btncolor.'"><i class="icon-arrow-right'.$iconcolor.'"></i> '.__( 'Visit Shop' , 'pro').'</a></td></tr>';
        				}
					}

				$output .= '</table>';

				$output = apply_filters( 'floating_menu_global_store_after_table' , $output );

			$output .= '</div>';

			$output .= '<div class="modal-footer">';
				$output .= '<button class="btn'.$btncolor.'" data-dismiss="modal" aria-hidden="true">'.__( 'Close', 'pro' ).'</button>';
			$output .= '</div>';

		$output .= '</div>';

  		if ($echo)
    		echo apply_filters( 'func_pro_global_store_load_modal' , $output , $echo );
  		else
    		return apply_filters( 'func_pro_global_store_load_modal' , $output , $echo );
	}

	/* Product taxonomy functions
	------------------------------------------------------------------------------------------------------------------- */

	function pro_load_product_taxonomy_menu( $args = array() ) {

		$defaults = array(
			'echo' => false,
			'termslug' => NULL,
			'taxonomy' => 'product_category',
			'count' => 'false'
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		$taxonomy_args = array(
			'taxonomy' => $taxonomy,
			'orderby' => 'name',
			'order' => 'ASC',
			'parent' => 0
		  );

		$taxonomies = get_categories($taxonomy_args);

		$output = apply_filters( 'pro_taxonomy_menu_before_listing' , '' , $termslug , $taxonomy );

		$output .= '<ul class="product-taxonomy-menu nav nav-tabs nav-stacked">';

			if  ($taxonomies) {

			  	foreach($taxonomies as $cat) {

				  	$output .= '<li'.( $termslug == $cat->slug ? ' class="selected add-fadeindown-effects-parent"' : ' class="add-fadeindown-effects-parent"' ).'><a href="'. get_term_link( $cat->slug , $taxonomy ) .'">';
				  	$output .= pro_load_image_product_taxonomy($cat->slug , $taxonomy);
				    $output .= $cat->name.( $count == 'true' ? ' <small>('.$cat->count.')</small>' : '' ).'</a>';

				    if ($taxonomy == 'product_category') {
					    // load sub categories (if available)
						$subcategories_args = array(
							'taxonomy' => $taxonomy,
							'orderby' => 'name',
							'order' => 'ASC',
							'parent' => $cat->term_id
						  );

						$subcategories = get_categories($subcategories_args);

						if ($subcategories) {

							$output .= '<ul class="sub-categories-menu nav nav-tabs nav-stacked add-fadeindown-effects-child">';

							foreach ($subcategories as $subcat) {
							  	$output .= '<li class="'.( $termslug == $subcat->slug ? 'selected subselected' : 'notselected' ).'"><a href="'. get_term_link( $subcat->slug , $taxonomy ) .'">';
							  	$output .= pro_load_image_product_taxonomy($subcat->slug , $taxonomy);
							    $output .= $subcat->name.( $count == 'true' ? ' <small>('.$subcat->count.')</small>' : '' ).'</a></li>';
							}

							$output .= '</ul>';
						}
				    }

					$output .= '</li>';
			  	}
			}

		$output .= '</ul>';

		$output = apply_filters( 'pro_taxonomy_menu_after_listing' , $output , $termslug , $taxonomy );

  		if ($echo)
    		echo apply_filters( 'func_pro_load_product_taxonomy_menu' , $output , $instance );
  		else
    		return apply_filters( 'func_pro_load_product_taxonomy_menu' , $output , $instance );
	}

	// load category / tag image (extract from one of the post within the category)
	function pro_load_image_product_taxonomy( $slug = '' , $taxonomy = 'product_category' ) {

	  	$post_args = array('numberposts' => 1, 'post_type' => 'product' , $taxonomy => $slug, 'orderby' => 'rand', 'order' => 'DESC');

	  	$posts_array = get_posts( $post_args );

	  	foreach($posts_array as $post) {
	  		return get_the_post_thumbnail( $post->ID , array(50, 50) );
	  	}
	}

	/* Related Products functions
	------------------------------------------------------------------------------------------------------------------- */

	function pro_load_related_products( $args = array() ) {

		$defaults = array(
			'echo' => false,
			'current_post' => NULL,
			'termslug' => NULL,
			'querytype' => 'By Categories',
			'btnclass' => '',
			'iconclass' => '',
			'labelcolor' => '',
			'boxclass' => '',
			'boxstyle' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

  		global $id;
  		$current_post = ( NULL === $current_post ) ? $id : $current_post;

		$related_query_args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'showposts' => apply_filters( 'related_products_showposts' , 42 ),
			'orderby' => 'rand',
			'post__not_in' => array( $current_post )
			);

		if ( $querytype == 'By Tags' ) {

			$terms = get_the_terms( $current_post , 'product_tag' );
			if (!empty($terms)) {
				$termsselected = '';
				foreach ($terms as $term) {
					$termsselected .= $term->slug . ',';
				}
			}
			$related_query_args['product_tag'] = $termsselected;

		} elseif ( $querytype == 'By IDs' ) {

			$specificids = esc_attr(get_post_meta( $current_post , '_mpt_related_products_specific_ids', true ) );
  			$specificids = explode(',' , $specificids);
  			$idlist = array();
  			if (!empty($specificids)) {
  				foreach ($specificids as $singleid) {
  					$idlist[] = is_numeric( $singleid ) ? ( $singleid == $current_post ? NULL : $singleid ) : NULL;
  				}
  			}
			$related_query_args['post__in'] = $idlist;

		} else {

			$terms = get_the_terms( $current_post , 'product_category' );
			if (!empty($terms)) {
				$termsselected = '';
				foreach ($terms as $term) {
					$termsselected .= $term->slug . ',';
				}
			}
			$related_query_args['product_category'] = $termsselected;

		}

		$related_query = new WP_Query( $related_query_args );

		$output = '';

		if ($related_query->have_posts()) : 

			$output .= '<div id="related-products-holder" class="related-products carousel slide">';

				$output .= '<div class="page-heading">';
					$output .= '<h3>' . __('Related Products', 'pro') . '</h3>';
				$output .= '</div>'; // End page-heading

					// Carousel nav
					$output .= '<div class="related-products-control-nav">';
						$output .= '<a class="related-products-control-left" href="#related-products-holder" data-slide="prev"><i class="icon-caret-left icon-3x"></i></a>';
						$output .= '<a class="related-products-control-right" href="#related-products-holder" data-slide="next"><i class="icon-caret-right icon-3x"></i></a>';
					$output .= '</div>';

				$output .= '<div id="pro-product-grid">';

					$output .= '<div class="carousel-inner">';

					$count = 1;
					$counter = 1;

					while ($related_query->have_posts()) : $related_query->the_post();

						$incomplete_row = true;

						$output .= ( $count == 1 ? '<div class="row-fluid item'.( $counter == 1 ? ' active' : '' ).'">' : '' );

					  	$id = get_the_ID();

					  	$single_args = array(
							'echo' => false,
							'post_id' => $id,
							'span' => 'span4',
							'imagesize' => 'tb-360',
							'btnclass' => $btnclass,
							'iconclass' => $iconclass,
							'labelcolor' => $labelcolor,
							'class' => $boxclass,
							'style' => $boxstyle
					  	);

						$output .= pro_load_single_product_in_box( $single_args );
						
						if ( $count == 3 ) {
							$incomplete_row = false;
							$count = 0;
							$output .= '</div>'; // End - row-fluid
						}

						$count++;
						$counter++;

					endwhile;
					wp_reset_query();

					if ( $incomplete_row )
						$output .= '</div>'; // End - row-fluid

					$output .= '</div>'; // End Carousel-inner

				$output .= '</div>'; // End pro-product-grid

			$output .= '</div>'; // End - related-products

		endif;


  		if ($echo)
    		echo apply_filters( 'func_pro_load_related_products' , $output , $instance );
  		else
    		return apply_filters( 'func_pro_load_related_products' , $output , $instance );
	}

	/* Product Carousel functions
	------------------------------------------------------------------------------------------------------------------- */

	function pro_load_product_carousel( $args = array() ) {

		$defaults = array(
			'echo' => false,
			'slider_id' => '',
			'layout' => '3col',
			'perpage' => 9,
			'taxonomy_type' => '',
			'taxonomy' => '',
			'order_by' => 'date',
			'order' => 'DESC',
			'sliderpause' => 'yes',
			'sliderspeed' => '4000', 
			'btnclass' => '',
			'iconclass' => '',
			'labelcolor' => '',
			'carouselclass' => '',
			'carouselstyle' => '',
			'boxclass' => '',
			'boxstyle' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		switch ($layout) {
			case '2col':
				$span = 'span6';
				$max_counter = '2';
				break;
			case '3col':
			default:
				$span = 'span4';
				$max_counter = '3';
				break;
			case '4col':
				$span = 'span3';
				$max_counter = '4';
				break;
		}

	    //setup taxonomy if applicable
	    if ($taxonomy_type == 'category') {
	      	$taxonomy_query = '&product_category=' . $taxonomy;
	    } else if ($taxonomy_type == 'tag') {
	      	$taxonomy_query = '&product_tag=' . $taxonomy;
	    } else {
	    	$taxonomy_query = '';
	    }

	    //get order by
	    if ($order_by) {
	      	if ($order_by == 'price')
	        	$order_by_query = '&meta_key=mp_price&orderby=mp_price';
	      	else if ($order_by == 'sales')
	        	$order_by_query = '&meta_key=mp_sales_count&orderby=mp_sales_count';
	      	else
	        	$order_by_query = '&orderby='.$order_by;
	    } else {
	      	$order_by_query = '&orderby=title';
	    }

		$the_query = new WP_Query( 'post_type=product&post_status=publish&showposts=' . $perpage . $taxonomy_query . $order_by_query . '&order=' . $order );

		$output = '';

		if ($the_query->have_posts()) :

			$output .= '<div id="productCarousel-' . $slider_id . '" class="carousel productcarousel slide">';

				// Control
				$output .= '<div class="carousel-control-holder">';
					$output .= '<a class="carousel-control-link left-slide-link" href="#productCarousel-' . $slider_id . '" data-slide="prev"><i class="icon-chevron-left icon-large"></i></a>';
					$output .= '<a class="carousel-control-link right-slide-link" href="#productCarousel-' . $slider_id . '" data-slide="next"><i class="icon-chevron-right icon-large"></i></a>';
				$output .= '</div>';

				$output .= '<div class="clear"></div>';

				$output .= '<div id="pro-product-grid">';

					$output .= '<div class="carousel-inner">';

					$count = 1;
					$counter = 1;

					while ($the_query->have_posts()) : $the_query->the_post();

						$incomplete_row = true;

						$output .= ( $count == 1 ? '<div class="row-fluid item'.( $counter == 1 ? ' active' : '' ).'">' : '' );

					  	$id = get_the_ID();

					  	$single_args = array(
							'echo' => false,
							'post_id' => $id,
							'span' => $span,
							'imagesize' => 'tb-360',
							'btnclass' => $btnclass,
							'iconclass' => $iconclass,
							'labelcolor' => $labelcolor,
							'class' => $boxclass,
							'style' => $boxstyle
					  	);

						$output .= pro_load_single_product_in_box( $single_args );
						
						if ( $count == $max_counter ) {
							$incomplete_row = false;
							$count = 0;
							$output .= '</div>'; // End - row-fluid
						}

						$count++;
						$counter++;

					endwhile;
					wp_reset_query();

					if ( $incomplete_row )
						$output .= '</div>'; // End - row-fluid

					$output .= '</div>'; // End Carousel-inner

				$output .= '</div>'; // End pro-product-grid

			$output .= '</div>'; // End productcarousel

			// carousel JS
			$output .= '
			<script type="text/javascript">
				jQuery(document).ready(function () {
					jQuery(".productcarousel").carousel({
						interval: ' . esc_attr($sliderspeed) . '
						' . ($sliderpause == 'yes' ? ',pause: "hover"' : '') . '
					})
				});
			</script>';

		endif;

  		if ($echo)
    		echo apply_filters( 'func_pro_load_product_carousel' , $output , $instance );
  		else
    		return apply_filters( 'func_pro_load_product_carousel' , $output , $instance );		
	}

	/* Product Listing Mode functions
	------------------------------------------------------------------------------------------------------------------- */

	add_action( 'init' , 'pro_hide_product_price_in_all_level' , 15 );

	function pro_hide_product_price_in_all_level() {

		global $mp;
		//if ( $mp->get_setting('disable_cart') && get_option('mpt_mp_hide_product_price') == 'true' ) {
		if ( $mp->get_setting('disable_cart') && get_blog_option(1,'mpt_mp_hide_product_price') == 'true' ) {

			add_filter( 'func_pro_product_price' , '__return_false' );
			add_filter( 'mp_product_price_tag' , '__return_false' );
		}
	}

	/* CustomPress Integration
	------------------------------------------------------------------------------------------------------------------- */

	function pro_check_if_cf_available(){
		
		if ( class_exists('CustomPress_Content_Types') ) {

			global $post, $CustomPress_Core;

			$custom_fields = $CustomPress_Core->all_custom_fields;
			if (empty($custom_fields)) $custom_fields = array();

			foreach ( $custom_fields as $custom_field ){
				$output = in_array($post->post_type, $custom_field['object_type']);
				if ( $output ){

					$prefix = ( empty( $custom_field['field_wp_allow'] ) ) ? '_ct_' : 'ct_';
					$fid = $prefix . $custom_field['field_id'];
					$value = do_shortcode('[ct id="' . $fid . '"]');
					if($value != ''){
						return apply_filters('func_pro_check_if_cf_available', true );
					}
				}
			}

		} else {
			return false;
		}
	}

	/* Single Product Page
	------------------------------------------------------------------------------------------------------------------- */

	function pro_product_page_load_images( $args = array() ) {

		$defaults = array(
			'post_id' => NULL,
			'default_img_id' => NULL,
			'carouselitemwidth' => 85,
			'carouselitemmargin' => 5,
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		global $id;
		$post_id = ( NULL === $post_id ) ? $id : $post_id;
		$output = '';

		if ( has_post_thumbnail( $post_id ) ) {

			$fullimage2 = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ) , 'full');
			$fullimage = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ) , 'tb-600');
			$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ) , 'thumbnail');

          	// load additional images
		   	$moreimages = get_post_meta( $post_id, '_mpt_more_product_images' , true );

			$output .= '<div class="flexslider main-pro-slider">';
				$output .= '<ul class="slides">';
					// Featured image
					$output .= '<li>' . pro_product_page_product_image( $fullimage[0] , $post_id , 1 , $instance ) . '</li>';

					// other product images
					for ($i=2; $i <= 10; $i++) {
						$image = get_post_meta( $post_id, '_mpt_video_featured_image_'.$i, true );
						if($fullimage2[0] != $image){
							$output .= ( esc_url( $image ) ? '<li>' . pro_product_page_product_image( $image , $post_id , $i , $instance ) . '</li>' : '' );
						}
					}

			   	if ( !empty( $moreimages ) && is_array( $moreimages ) ) {
			   		$image_count = 11;
			   		foreach ( $moreimages as $image_id => $image_url ) {
                    	$output .= ( esc_url( $image_url ) ? '<li>' . pro_product_page_product_image( $image_url , $post_id , $image_count , $instance ) . '</li>' : '' );
                    	$image_count++;
			   		}
			   	}

				$output .= '</ul>'; // end - slides
			$output .= '</div>'; // end - flexslider

			if ( pro_check_multi_product_image($post_id) ) {

				$output .= '<div id="flexcarousel" class="flexslider">';
					$output .= '<ul class="slides">';

					$output2 = '';
					// other product images
					for ($i=2; $i <= 10; $i++) {
						$image_url = get_post_meta( $post_id, '_mpt_video_featured_image_'.$i, true );
						if ( !empty($image_url) ) {
							$image_id = intval( get_image_id( $image_url ) );
							$image_thumb = ( !empty($image_id) ? wp_get_attachment_image_src( $image_id , 'thumbnail') : array() );
							if($fullimage2[0] != $image_url){
								$output2 .= ( esc_url( $image_thumb[0] ) ? '<li><img src="'.$image_thumb[0].'" /></li>' : '' );
							}
						}
					}
					
					if(!empty($output2)) {
						// Featured image
						$output .= '<li><img src="'.$thumbnail[0].'" /></li>';
						$output .= $output2;
					}
					
				   	if ( !empty( $moreimages ) && is_array( $moreimages ) ) {
				   		$image_count = 11;
				   		foreach ( $moreimages as $image_id => $image_url ) {
							$thumbnail_image = ( !empty($image_id) && esc_url( $image_url ) ? wp_get_attachment_image_src( $image_id , 'thumbnail' ) : array() );
	                    	$output .= ( esc_url( $thumbnail_image[0] ) ? '<li><img src="'.$thumbnail_image[0].'" /></li>' : '' );
	                    	$image_count++;
				   		}
				   	}
						
					$output .= '</ul>'; // end - slides
				$output .= '</div>'; // end - flexcarousel

			}

		} elseif ( !empty($default_img_id) && is_numeric($default_img_id) ) {

			$fullimage = wp_get_attachment_image_src( $default_img_id , 'tb-600');
			$output .= '<div class="flexslider main-pro-slider">';
				$output .= '<ul class="slides">';
					$output .= '<li>' . pro_product_page_product_image( $fullimage[0] , $post_id , 'default' , $instance ) . '</li>';
				$output .= '</ul>'; // end - slides
			$output .= '</div>'; // end - flexslider

		}

		if ( has_post_thumbnail( $post_id ) || ( !empty($default_img_id) && is_numeric($default_img_id) ) ) {

			$output .= '
			<script type="text/javascript">
				jQuery(document).ready(function () {
					' . ( pro_check_multi_product_image($post_id) ? '
					jQuery("#flexcarousel").flexslider({
					    animation: "slide",
					    controlNav: false,
					    animationLoop: false,
					    slideshow: false,
					    itemWidth: '.$carouselitemwidth.',
					    itemMargin: '.$carouselitemmargin.',
					    asNavFor: ".flexslider"
					  });
						' : '' ) . '
					jQuery(".flexslider").flexslider({
						animationLoop: false,
						smoothHeight: true,
						slideshow: false,
						pauseOnHover: true,
						controlNav: "", 
						directionNav: false,
						'. ( pro_check_multi_product_image($post_id) ? 'sync: "#flexcarousel",' : '' ) .'
					});
					'.( get_blog_option(1,'mpt_pro_page_enable_image_zoom_feature') != 'false' ? '
					jQuery("img.pro-product-images").zoome({
						showZoomState:true,
						zoomRange:[1,5],
						zoomStep:0.5,
						defaultZoom:2.0,
						magnifierSize:[200,200]
					});' : '' ).'
				});
			</script>
			';
		}
		
		return apply_filters( 'func_pro_product_page_load_images' , $output , $instance );	 
	}

	function pro_product_page_product_image( $image = '' , $post_id = NULL , $num = 1 , $instance = array() ) {

		global $id;
		$post_id = ( NULL === $post_id ) ? $id : $post_id;
		//$zoomfeature = get_option('mpt_pro_page_enable_image_zoom_feature');
		$zoomfeature = get_blog_option(1,'mpt_pro_page_enable_image_zoom_feature');

		if ( $zoomfeature == 'false' )
			$output = '<a href="'.$image.'" class="pro-product-image-lightbox" rel="prettyPhoto[product-'.$post_id.']"><img src="'.$image.'" class="pro-product-images" alt="'.apply_filters( 'pro_product_page_featured_image_caption' , get_the_title( $post_id ) , $post_id , $num ).'" /><div class="hover-block"><i class="icon-plus"></i></div></a>';
		else
			$output = '<img src="'.$image.'" class="pro-product-images" />';

		return apply_filters( 'pro_product_page_product_image' , $output , $image , $post_id , $num , $instance );
	}

	function pro_load_product_page_category( $args = array() ) {

		$defaults = array(
			'post_id' => NULL,
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		global $id;
		$post_id = ( NULL === $post_id ) ? $id : $post_id;
		$categories = get_the_terms( $post_id , 'product_category' );
		$globaltags = false;
		$output = '';

		//if ( is_multisite() && get_option('mpt_pro_page_enable_global_tags') == 'true' ) {
		
		if ( is_multisite() && get_blog_option(1,'mpt_pro_page_enable_global_tags') == 'true' ) {

			$mp_main_site_id = function_exists('mp_main_site_id') ? mp_main_site_id() : 1;
			$globalsettings = get_site_option('mp_network_settings');
			$globaltags = true;
		}

		if (!empty($categories)) {
			foreach ($categories as $category) {
				$output .= '<span class="label label-category">';

					if ( $globaltags )
						$output .= '<a href="'.get_home_url( $mp_main_site_id , $globalsettings['slugs']['marketplace'] . '/' . $globalsettings['slugs']['categories'] . '/' . $category->slug . '/' ).'">';
					else
						$output .= '<a href="'.get_term_link( $category->slug, 'product_category' ).'">';

					$output .= $category->name . '</a>';
				$output .= '</span> ';
			}
		}

		return apply_filters( 'func_pro_load_product_page_category' , $output , $instance );
	}

	function pro_load_product_page_tags( $args = array() ) {

		$defaults = array(
			'post_id' => NULL,
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		global $id;
		$post_id = ( NULL === $post_id ) ? $id : $post_id;
		$tags = get_the_terms( $post_id , 'product_tag' );
		$globaltags = false;
		$output = '';

		//if ( is_multisite() && get_option('mpt_pro_page_enable_global_tags') == 'true' ) {
		
		if ( is_multisite() && get_blog_option(1,'mpt_pro_page_enable_global_tags') == 'true' ) {

			$mp_main_site_id = function_exists('mp_main_site_id') ? mp_main_site_id() : 1;
			$globalsettings = get_site_option('mp_network_settings');
			$globaltags = true;
		}
		if (!empty($tags)) {
			foreach ($tags as $tag) {
				$output .= '<span class="label label-tag">';

					if ( $globaltags )
						$output .= '<a href="'.get_home_url( $mp_main_site_id , $globalsettings['slugs']['marketplace'] . '/' . $globalsettings['slugs']['tags'] . '/' . $tag->slug . '/' ).'">';
					else
						$output .= '<a href="'.get_term_link( $tag->slug, 'product_tag' ).'">';

					$output .= $tag->name . '</a>';
				$output .= '</span> ';
			}
		}

		return apply_filters( 'func_pro_load_product_page_tags' , $output , $instance );
	}	
	
	function category_page_slider($category){
		global $wpdb,$mp,$post;
				
		$query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content 
				FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id 
				INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id 
				WHERE p.blog_public = 1 AND t.type = 'product_category' AND t.slug = '$category'";
				
		$fproducts = $wpdb->get_results($query);
		
		if(null !== $fproducts){  ?>
			<div id="slider-wrapper">
				<div class="slider-inner-con">	
					<div id="pro-product-grid" class="homepage-newest-products-slide">
						<div id="newest-products-slides-con">						
					<?php
					
					$loopcount = 0;
					$counter = 0;
					$loopcolumn = 6;
					$current_blog_id = get_current_blog_id();
					$current_post = $post;
					
					foreach($fproducts as $fproduct){
						$loopcount++;
						$image = '';
						$imagel = '';
						++$counter;
						$offset = '';
						switch_to_blog($fproduct->blog_id);
						$post = get_post($fproduct->post_id);
						setup_postdata($post);			
						$blogname = get_blog_option($fproduct->blog_id,'blogname');		
						$blogurl = get_site_url($fproduct->blog_id);
						$avg_rating = get_post_meta($post->ID, 'tp_product_rating', true );
						if(empty($avg_rating))
							$avg_rating = 0; 
							
						if ( has_post_thumbnail( $fproduct->post_id ) ) { 
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $fproduct->post_id ), 'medium' );
							$imagel = wp_get_attachment_image_src( get_post_thumbnail_id( $fproduct->post_id ), 'large' );
							$image = $image[0];
							$imagel = $imagel[0];
						}
						if ( $counter == 0 ) {	$offset = "offset5"; }
						elseif ( $counter == 1 ) {	$offset = "offset4"; }
						elseif ( $counter == 2 ) { $offset = "offset3"; }
						elseif ( $counter == 3 ) {	$offset = "offset2"; }
						elseif ( $counter == 4 ) {	$offset = "offset1"; }						
						?>
						
						<?php if (0 == ( $loopcount - 1 ) % $loopcolumn || 1 == $loopcolumn ) { ?>
								<div class="newest-products-slides row-fluid">
						<?php } ?>														
									<div class="span2 well <?php if ( $loopcount == 1  ) { echo $offset ; } echo " " . $counter; ?>">	
										<div class="product-image-box" style="max-width: 360px;background-image:url(<?php echo $image; ?>);">
											<span class="label proprice label-info hidden-phone">
												<?php echo mp_product_price(false, $fproduct->post_id,false); ?>
											</span>
											<span class="product-image-pinit-btn"></span>
											<div class="hover-block hidden-phone">
												<div class="btn-group">
													<a href="<?php echo $imagel; ?>" class="btn btn-info mpt-pp-trigger" data-pptitle="<?php echo $post->post_title; ?>" title="View Product Image"><i class="icon-zoom-in icon-white"></i></a>
													<a href="<?php echo add_lang_vars_custom($fproduct->post_permalink); ?>" class="btn btn-info" title="More Details"><i class="icon-list-alt icon-white"></i></a>
												</div>
											</div>
										</div>								
										<div class="pro-product-box-contents align-center">
											<div class="hidden-phone align-center">
												<div class="product-meta" style="min-height: 50px">
													<a href="<?php echo add_lang_vars_custom($fproduct->post_permalink); ?>"><?php echo $post->post_title; ?></a>
													<?php echo show_store_ratings($avg_rating,false,add_lang_vars_custom($fproduct->post_permalink.'/').'#product-reviews'); ?>
												</div>
											</div>
											<div id="mp-premium-features" class="mppf-container mppf-container-list mppf-pro-integration mppf-container-block">
												<button class="mppf-btn mppf-shop-now-btn mppf-btn-black" value="<?php echo $fproduct->post_id;?>">
													<span class="mppf-btn-text mppf-show mppf-btn-text-right"><?php _e('Shop Now'); ?></span>
													<i class="fa fa-arrow-circle-o-right"></i>
												</button>
												<input type="hidden" id="snm-blog-id" name="current_blog_id" value="<?php echo $fproduct->blog_id;?>"><input type="hidden" id="snm-global-btn" name="global_btn" value="yes">
											</div>
										</div>									
									</div>
								<?php if ( 0 == $loopcount % $loopcolumn) { ?>
									</div>
								<?php } ?>	
						<?php }
						$post = $current_post;
						switch_to_blog($current_blog_id);
						?>
										<?php if ($loopcount % $loopcolumn != 0) { //add fix if not?>
										</div>
									<?php } ?>	
							</ul>	
						</div>	
							<div id="newest-products-slides_next" class="newest-products-slides_next newest-products-slides_navi"></div>
							<div id="newest-products-slides_prev" class="newest-products-slides_prev newest-products-slides_navi"></div>			 
					</div>	
				</div>
		<?php }					
					
	}
