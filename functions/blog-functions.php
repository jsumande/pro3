<?php

	/* Post Query function
	------------------------------------------------------------------------------------------------------------------- */

	function pro_display_wp_post_query( $args = array() ) {

	  	$defaults = array(
			'echo' => true,
			'enablecustomquery' => false,
			'customqueries' => NULL,
			'posttype' => 'post',
			'paginate' => true,
			'page' => '', 
			'per_page' => '', 
			'order_by' => 'date', 
			'order' => 'DESC',
			'searchterms' => '',
			'issearchpage' => false,
			'taxonomy' => '',
			'category' => '', 
			'tag' => '',
			'author' => '',
			'day' => '',
			'month' => '',
			'year' => '',
			'columns' => '1col',
			'poststyle' => 'style-1',
			'masonry' => false,
			'imagesize' => 'full',
			'btnclass' => '',
			'iconclass' => '',
			'boxclass' => '',
			'boxstyle' => '',
			'extra' => array()
		);

	  	$instance = wp_parse_args( $args, $defaults );
	  	extract( $instance );

	  	//$per_page = !empty($per_page) ? $per_page : get_option('posts_per_page');
		$per_page = !empty($per_page) ? $per_page : get_blog_option(1,'posts_per_page');

		if ( empty($page) && get_query_var('paged') ) {
			$page = intval(get_query_var('paged'));
		} elseif ( empty($page) && get_query_var('page') ) {
			$page = intval(get_query_var('page'));
		} elseif ( empty($page) ) {
			$page = 1;
		}

		//setup taxonomy if applicable
		if ( $posttype == 'post' ) {

			if ($category) {
			    $taxonomy_query = '&category_name=' . sanitize_title($category);
			} else if ($tag) {
			    $taxonomy_query = '&tag=' . sanitize_title($tag);
			} else {
			    $taxonomy_query = '';
			}

		} else {

			if ( !empty( $taxonomy ) && $category ) {
				$taxonomy_query = '&' . esc_attr( $taxonomy ) . '=' . sanitize_title($category);
			} elseif ( !empty($taxonomy) && $tag ) {
				$taxonomy_query = '&' . esc_attr( $taxonomy ) . '=' . sanitize_title($tag);
			} else {
				$taxonomy_query = '';
			}

		}
		  
		//setup pagination
		$paged = false;
		if ($paginate) {
		  	if ($paginate === 'nopagingblock') {
		  		$paginate_query = '&showposts='.intval($per_page);
		  	} else {
		  		$paged = true;
		  	}
		} else {
		    $paginate_query = '&nopaging=true';
		}

		  //get page details
		if ($paged) {
		    //figure out perpage
		    if (intval($per_page)) {
		      $paginate_query = '&posts_per_page='.intval($per_page);
			} else {
		      //$paginate_query = '&posts_per_page='.get_option('posts_per_page');
			  $paginate_query = '&posts_per_page='.get_blog_option(1,'posts_per_page');
			}

			if ($page) {
				$paginate_query .= '&paged='.intval($page);
			} else {
				$paginate_query .= '&paged=1';
			}
		}

	  	// get search
		if ( !empty($searchterms) ) {
			$search_query = '&s=' . esc_attr( $searchterms );
		} else {
			$search_query = '';
		}

		$author_query = !empty($author) ? '&author_name=' . esc_attr($author) : '';
		$day_query = !empty($day) ? '&day=' . esc_attr($day) : '';
		$month_query = !empty($month) ? '&monthnum=' . esc_attr($month) : '';
		$year_query = !empty($year) ? '&year=' . esc_attr($year) : '';

		//get order by
		$order_by_query = '&orderby='.esc_attr($order_by);

		//get order direction
		$order_query = '&order='.esc_attr($order);

		//The Query
		if ( $enablecustomquery && !empty($customqueries) )
			$the_query = $customqueries;
		else 
			$the_query = new WP_Query( 'post_type=' . esc_attr($posttype) . '&post_status=publish' . $taxonomy_query . $paginate_query . $order_by_query . $order_query . $search_query . $author_query . $year_query . $month_query . $day_query);

		// load pagination
		if ($paged) {

		  	$paginate_output = '<div class="clear"></div>';
			$paginate_output .= '<div class="pull-right">';

			    $total_pages = $the_query->max_num_pages;  

			    if ($total_pages > 1){ 

			    	if ( get_query_var('paged') ) {
			    		$current_page = max(1, get_query_var('paged') ); 
			    	} elseif ( get_query_var('page') ) {
			    		$current_page = max(1, get_query_var('page') ); 
			    	} else {
			    		$current_page = 1;
			    	}

			    	if ( is_home() || is_front_page() || $issearchpage ) {
			    		$sanitized_searchterms = ( !empty($searchterms) ? str_replace( '-' , '+' , sanitize_title($searchterms) ) : '' );
			    		$paginate_links_args = array(
							'base'         => get_home_url() . ( $issearchpage && !empty($sanitized_searchterms) ? '?s='.$sanitized_searchterms : '' ) . '%_%',
							'format'       => ( $issearchpage && !empty($sanitized_searchterms) ? '&page=%#%' : '?page=%#%' ),
							'total'        => $total_pages,
							'current'      => $current_page,
							'prev_text'    => __('« Previous' , 'pro'),
							'next_text'    => __('Next »' , 'pro'),
							'type'         => 'list'
			    			);							
			    	} else {
						if ( is_page( 2317 ) ) 
						//if ( is_page( 747 ) ) // Mae local for fixes purposes bg
						{
			    		$paginate_links_args = array(
							'base' =>  $_SERVER["HTTP_HOST"] . '/блог/%_%',
							'format'       => 'page/%#%/',
							'total'        => $total_pages,
							'current'      => $current_page,
							'prev_text'    => __('<i class="fa fa-angle-left"></i> &nbsp; Предишна' , 'pro'),
							'next_text'    => __('Следваща &nbsp; <i class="fa fa-angle-right"></i>' , 'pro'),
							'type'         => 'list'
			    			);
						}
						if ( is_page( 2313 ) ) 
						//if ( is_page( 637 ) ) // Mae local for fixes purposes
						{
			    		$paginate_links_args = array(
							'base'         => $_SERVER["HTTP_HOST"] . '/blog/%_%',
							'format'       => 'page/%#%/',
							'total'        => $total_pages,
							'current'      => $current_page,
							'prev_text'    => __('<i class="fa fa-angle-left"></i> &nbsp; Previous' , 'pro'),
							'next_text'    => __('Next &nbsp; <i class="fa fa-angle-right"></i>' , 'pro'),
							'type'         => 'list'
			    			);
						}
					}

				    $paginate_output .= '<div class="paging">';
				    	$paginate_output .= paginate_links( $paginate_links_args );
			       	$paginate_output .= '</div>';
			    }

			$paginate_output .= '</div>';

	  	} else {
	  		$paginate_output = '';
	  	}

		$output = '<div id="pro-wp-posts"'.( $columns == '2col' || $columns == '3col' || $columns == '4col' ? ' class="span-box-container"' : '' ).'>';

			if ($the_query->have_posts()) : 

				$nopost = false;

				while ($the_query->have_posts()) : $the_query->the_post();

					$post_id = get_the_ID();

				  	$single_args = array(
						'echo' => false,
						'post_id' => $post_id,
						'taxonomy' => esc_attr( $taxonomy ),
						'imagesize' => $imagesize,
						'columns' => $columns,
						'masonry' => ( $columns == '1col' ? false : true ),
						'btnclass' => $btnclass,
						'iconclass' => $iconclass,
						'class' => $boxclass,
						'style' => $boxstyle
					);

					if ( !empty($extra) )
						$single_args['extra'] = $extra;

					if ( $poststyle == 'style-2' ) {
						$output .= pro_load_wp_single_post_style_2( $single_args );
					} else {
						$output .= pro_load_wp_single_post( $single_args );
					}

				endwhile;

			else :

				// If nothing found
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: ".get_bloginfo('url')."/noresults/?sr=".$_GET['s']);
				exit();
				/*$nopost = true;
				$output .= '<div class="page-content">';
					$output .= '<h2>'.__( 'Nothing Founds' , 'pro' ).'</h2>';
					$output .= '<p>' . __('Perhaps try one of the links below:' , 'pro' ) . '</p>';
					$output .= '<div class="padding10"></div>';
						$output .= '<h4>' . __( 'Most Used Categories' , 'pro' ) . '</h4>';
						$output .= '<ul>';
							$output .= wp_list_categories( array( 'taxonomy' => 'category' , 'orderby' => 'count', 'order' => 'DESC', 'show_count' => 1, 'title_li' => '', 'number' => 10 , 'echo' => 0 ) );
						$output .= '</ul>';
					$output .= '<div class="padding20"></div>';
					$output .= '<p>' . __( 'Or, use the search box below:' , 'pro' ) . '</p>';
					$output .= get_search_form( false );
				$output .= '</div>';*/
				
			endif;
			wp_reset_query();
		  	$output .= '<div class="clear"></div>';

		$output .= '</div>'; // End - pro-wp-posts

		$output .= $paginate_output;

	  	$output .= '<div class="clear"></div>';

	  	$randnum = rand( 1 , 9999 );
		$output .= ( $columns == '1col' || $nopost ? '' : '<script type="text/javascript">
						jQuery(document).ready(function () {
							var conTaiNer'.$randnum.' = jQuery("#pro-wp-posts");
					    	conTaiNer'.$randnum.'.imagesLoaded( function(){
							  	conTaiNer'.$randnum.'.masonry({
							    	itemSelector : ".span-box"
							  	});
							});
						});
					</script>' );

	  	if ($echo)
	    	echo apply_filters( 'func_pro_display_wp_post_query' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_display_wp_post_query' , $output , $instance );		
	}

	/* Post Box
	------------------------------------------------------------------------------------------------------------------- */

	function pro_load_wp_single_post( $args = array() ) {

	  	$defaults = array(
			'echo' => true,
			'post_id' => NULL,
			'taxonomy' => '',
			'columns' => '1col',
			'masonry' => false,
			'imagesize' => 'full',
			'btnclass' => '',
			'iconclass' => '',
			'class' => '',
			'style' => '',
			'extra' => array(
				'showexcerpt' => true,
				'showmeta' => true,
				'showauthordetails' => true,
				'showcomments' => true,
				'showhovertag' => true,
				'titlecolor' => '',
				'hovertagcolor' => '',
				'metatagscolor' => 'meta-tag-lightblue'
				)
		);

	  	$instance = wp_parse_args( $args, $defaults );
	  	extract( $instance );

	  	$post = get_post( $post_id );
	  	$comments = $post->comment_count == 0 ? __( 'No comments' , 'pro' ) : ( $post->comment_count > 1 ? $post->comment_count . __(' comments' , 'pro' ) : __( '1 comment' , 'pro' ) );
	  	$temp = get_post_meta( $post_id , '_mpt_post_select_temp', true );

		$year  = get_the_time('Y' , $post_id); 
		$month = get_the_time('M' , $post_id); 
		$day   = get_the_time('j' , $post_id);

		$style = ( !empty($style) ? ' style="'.esc_attr($style).'"' : '' );
		$class = ( !empty($class) ? ' '.esc_attr($class) : '' );

		switch ( $columns ) {
			case '1col':
			default:
				$column_class = '';
				break;
			case '2col':
				$column_class = 'span-2x';
				break;
			case '3col':
				$column_class = 'span-3x';
				break;
			case '4col':
				$column_class = 'span-4x';
				break;		
		}

		if ( has_post_thumbnail($post_id) || $temp == 'video' ) {
			// featured image(s)
			$featured = '<div class="'.( $columns == '1col' ? 'col-md-5 ' : '' ).'align-center">';

			$featured_args = array(
				'echo' => false,
				'post_id' => $post_id,
				'content_type' => 'list',
				'prettyphoto' => false,
				'imagesize' => ( $columns == '2col' || $columns == '3col' ? 'tb-860' : 'tb-360' ),
				'masonry' => $masonry,
				'videoheight' => 195,
				'btnclass' => '',
				'iconclass' => '',
			); 

			if ($temp == 'image-carousel') {
				$featured .= mpt_load_image_carousel( $featured_args );
			} else if ($temp == 'video') {
				$featured .= mpt_load_video_post( $featured_args );
			} else {
				$featured .= mpt_load_featured_image( $featured_args );
			}
									
			$featured .= '</div>'; // End - span	  	
			$featured .= '<div class="clear padding10'.( $columns == '1col' ? ' visible-phone' : '' ).'"></div>';

		} else {
			$featured = '';
		}
		
			$output = ( $columns == '1col' ? '' : '<div class="span-box '.$column_class.'">' );
		  	$output .= '<div class="post-grid well single-post-box box-style-2 '.( $columns == '1col' && ( has_post_thumbnail($post_id) || $temp == 'video' ) ? ' single-post-box-full' : '' ).$class.'"'.$style.'>';

		  		/*if ( $extra['showhovertag'] ) {
			  		$output .= '<div class="hover-meta-tag'.( !empty($extra['hovertagcolor']) ? ' ' . esc_attr( $extra['hovertagcolor'] ) : '' ).'">';
			  			$output .= ( $temp == 'video' ? '<i class="icon-youtube-play"></i>' : ( $temp == 'image-carousel' ? '<i class="icon-picture"></i>' : '<i class="icon-file-text-alt"></i>' ) );
			  			//$output .= apply_filters( 'single_post_style_2_date' , '<span class="month">' . $month . '</span><span class="day">' . $day . '</span>' , $instance );
			  		$output .= '</div>';
		  		}*/
		  		$output .= ( $columns == '1col' ? '<div id="blog-list" class="row" style="margin-left: 0px;">' : '' );

				//new design for date display
				$output .= "<div class='col-md-".( has_post_thumbnail($post_id) ? '6' : '12' )."'><div class='row'><div class='blog-col1 col-md-".( has_post_thumbnail($post_id) ? '4' : '2' )."'".( !empty($extra['hovertagcolor']) ? ' ' . esc_attr( $extra['hovertagcolor'] ) : '' ).">";
				$output .= apply_filters( 'single_post_style_2_date' , '<h1 class="day">' . $day . '</h1><span class="month">' . $month .' '. $year . '</span>' , $instance );
				$output .= "<hr>";
				$output .= ( comments_open( $post_id ) && $extra['showcomments'] ? '<span class="comments"><a href="' . get_comments_link( $post_id ) . '">' . $comments . '</a></span>' : '' );
				$output .= "</div>";
				
			  		// image
					if ( has_post_thumbnail($post_id) || $temp == 'video' ) {
						$output .= ( $columns == '1col' ? '<div class="col-md-8 blog-col2">' : '' );
							$output .= '<div class="post-image-container'.( $temp == 'image-carousel' ? ' image-slider' : '' ).'">';
								$featured_args = array(
									'echo' => false,
									'post_id' => $post_id,
									'content_type' => 'list',
									'prettyphoto' => false,
									'imagesize' => 'tb-360',
									'masonry' => $masonry,
									'widthcontrol' => false,
									'videoheight' => 195,
									'btnclass' => '',
									'iconclass' => '',
								); 

								if ($temp == 'image-carousel') {
									$output .= mpt_load_image_carousel( $featured_args );
								} else if ($temp == 'video') {
									$output .= mpt_load_video_post( $featured_args );
								} else {
									$output .= mpt_load_featured_image( $featured_args );
								}
							$output .= '</div></div></div>'; // end - post-image-container
						$output .= ( $columns == '1col' ? '</div>' : '' ); // end - span
					}
					
					// contents 
					$output .= ( $columns == '1col' ? '<div class="blog-col3 col-md-'.( has_post_thumbnail($post_id) || $temp == 'video' ? '6' : '10' ).'" '.( has_post_thumbnail($post_id) ? "" : "style='margin: 0px 0px 0px -12px; padding-left: 30px;'" ).'>' : '' );
						$output .= '<div class="post-box-contents">';
							$output .= '<a href="' . get_permalink( $post_id ) . '"><h4 class="post-title"'.( !empty($extra['titlecolor']) && $extra['titlecolor'] != '#' ? ' style="color: ' . esc_attr($extra['titlecolor']) . ';"' : '' ).'>' . get_the_title( $post_id ) . '</h4></a><p class="blog-auth">';

							// author& comments
							$output .= ( $extra['showauthordetails'] ? __( 'Posted By ' , 'pro' ) . '<a style="color: rgb(79, 147, 182);" href="'.get_author_posts_url( $post->post_author ).'">'.get_the_author_meta( 'display_name' , $post->post_author ).'</a></p>' : '' );

						if ( $extra['showexcerpt'] )
								$output .= '<div id="blog-con" class="post-excerpt">' . ( !empty($post->post_excerpt) ? $post->post_excerpt : ( !empty($post->post_content) ? wp_trim_words($post->post_content , apply_filters( 'single_post_style_2_excerpt_words' , ( $columns == '1col' ? 45 : 30 ) , $instance ) ) : '' ) ) . '</div>';
							
							if ( $extra['showmeta'] ) {
								$post_meta_output = '<div id="blog-rm" class="post-meta-tags'.( !empty($extra['metatagscolor']) ? ' ' . esc_attr( $extra['metatagscolor'] ) : '' ).'">';
									if ( $post->post_type == 'product' ) {
										$post_meta_output .= get_the_term_list( $post_id , 'product_category' , '<span>' , '</span><span>' , '</span>' ) . get_the_term_list( $post_id , 'product_tag' , '<span>' , '</span><span>' , '</span>' );
									} elseif ( !empty($taxonomy) ) {
										$post_meta_output .= get_the_term_list( $post_id , $taxonomy , '<span>' , '</span><span>' , '</span>' );
									} else {
										//$post_meta_output .= get_the_term_list(  $post_id , 'category' , '<span>' , '</span><span>' , '</span>'  ) . get_the_tag_list(  '<span>' , '</span><span>' , '</span>'  );
										if(ICL_LANGUAGE_CODE=='bg'):
										$post_meta_output .= "<span style='margin-left: -135px;'><a href='" . get_permalink( $post_id ) . "'>".__('Read More','pro')."</a></span>";
										else:
										$post_meta_output .= "<span><a href='" . get_permalink( $post_id ) . "'>".__('Read More','pro')."</a></span>";
										endif;
									}
									
								$post_meta_output .= '</div>'; // end - post-meta-tags
								$output .= apply_filters( 'pro_single_post_box_meta_section' , $post_meta_output , $instance );
							}												
						if ( !has_post_thumbnail($post_id) ) $output .= "</div></div>";
						$output .= '</div>'; // end post-box-contents
					$output .= ( $columns == '1col' ? '</div>' : '' ); // end - span
				$output .= ( $columns == '1col' ? '</div>' : '' ); // end - row
		  	$output .= '</div>'; // End - single-post-box
		$output .= ( $columns == '1col' ? '' : '</div>' ); // End - span-box  	

	  	if ($echo)
	    	echo apply_filters( 'func_pro_load_wp_single_post' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_load_wp_single_post' , $output , $instance );
	}

	//pro_load_wp_single_post_style_2 Modified by Mae
	function pro_load_wp_single_post_style_2( $args = array() ) {
	  	$defaults = array(
			'echo' => true,
			'post_id' => NULL,
			'taxonomy' => '',
			'columns' => '1col',
			'imagesize' => 'full',
			'masonry' => false,
			'btnclass' => '',
			'iconclass' => '',
			'class' => '',
			'style' => '',
			'extra' => array(
				'showexcerpt' => true,
				'showmeta' => true,
				'showauthordetails' => true,
				'showcomments' => true,
				'showhovertag' => true,
				'titlecolor' => '',
				'hovertagcolor' => 'hover-tag-black',
				'metatagscolor' => 'meta-tag-lightblue'
				)
		);


	  	$instance = wp_parse_args( $args, $defaults );
	  	extract( $instance );
	  	$post = get_post( $post_id );
	  	$comments = $post->comment_count == 0 ? __( 'No comments' , 'pro' ) : ( $post->comment_count > 1 ? $post->comment_count . __(' comments' , 'pro' ) : __( '1 comment' , 'pro' ) );
	  	$temp = get_post_meta( $post_id , '_mpt_post_select_temp', true );
		$year  = get_the_time('Y' , $post_id); 
		$month = get_the_time('M' , $post_id); 
		$day   = get_the_time('j' , $post_id);
		$style = ( !empty($style) ? ' style="'.esc_attr($style).'"' : '' );
		$class = ( !empty($class) ? ' '.esc_attr($class) : '' );

		switch ( $columns ) {
			case '1col':
				$column_class = '';
				break;
			case '2col':
				$column_class = 'span-2x';
				break;
			case '3col':
			default:
				$column_class = 'span-3x';
				break;
			case '4col':
				$column_class = 'span-4x';
				break;		
		}

		$output = ( $columns == '1col' ? '' : '<div class="span-box '.$column_class.'">' );
		  	$output .= '<div class="post-grid well single-post-box box-style-2 '.( $columns == '1col' && ( has_post_thumbnail($post_id) || $temp == 'video' ) ? ' single-post-box-full' : '' ).$class.'"'.$style.'>';

		  		/*if ( $extra['showhovertag'] ) {
			  		$output .= '<div class="hover-meta-tag'.( !empty($extra['hovertagcolor']) ? ' ' . esc_attr( $extra['hovertagcolor'] ) : '' ).'">';
			  			$output .= ( $temp == 'video' ? '<i class="icon-youtube-play"></i>' : ( $temp == 'image-carousel' ? '<i class="icon-picture"></i>' : '<i class="icon-file-text-alt"></i>' ) );
			  			//$output .= apply_filters( 'single_post_style_2_date' , '<span class="month">' . $month . '</span><span class="day">' . $day . '</span>' , $instance );
			  		$output .= '</div>';
		  		}*/
		  		$output .= ( $columns == '1col' ? '<div id="blog-list" class="row" style="margin-left: 0px;">' : '' );

				//new design for date display
				$output .= "<div class='col-md-".( has_post_thumbnail($post_id) ? '6' : '12' )."'><div class='row'><div class='blog-col1 col-md-".( has_post_thumbnail($post_id) ? '4' : '2' )."'".( !empty($extra['hovertagcolor']) ? ' ' . esc_attr( $extra['hovertagcolor'] ) : '' ).">";
				$output .= apply_filters( 'single_post_style_2_date' , '<h1 class="day">' . $day . '</h1><span class="month">' . $month .' '. $year . '</span>' , $instance );
				$output .= "<hr>";
				$output .= ( comments_open( $post_id ) && $extra['showcomments'] ? '<span class="comments"><a href="' . get_comments_link( $post_id ) . '">' . $comments . '</a></span>' : '' );
				$output .= "</div>";
				
			  		// image
					if ( has_post_thumbnail($post_id) || $temp == 'video' ) {
						$output .= ( $columns == '1col' ? '<div class="col-md-8 blog-col2">' : '' );
							$output .= '<div class="post-image-container'.( $temp == 'image-carousel' ? ' image-slider' : '' ).'">';
								$featured_args = array(
									'echo' => false,
									'post_id' => $post_id,
									'content_type' => 'list',
									'prettyphoto' => false,
									'imagesize' => 'tb-360',
									'masonry' => $masonry,
									'widthcontrol' => false,
									'videoheight' => 195,
									'btnclass' => '',
									'iconclass' => '',
								); 

								if ($temp == 'image-carousel') {
									$output .= mpt_load_image_carousel( $featured_args );
								} else if ($temp == 'video') {
									$output .= mpt_load_video_post( $featured_args );
								} else {
									$output .= mpt_load_featured_image( $featured_args );
								}
							$output .= '</div></div></div>'; // end - post-image-container
						$output .= ( $columns == '1col' ? '</div>' : '' ); // end - span
					}
					
					// contents 
					$output .= ( $columns == '1col' ? '<div class="blog-col3 col-md-'.( has_post_thumbnail($post_id) || $temp == 'video' ? '6' : '10' ).'" '.( has_post_thumbnail($post_id) ? "" : "style='margin: 0px 0px 0px -12px; padding-left: 30px;'" ).'>' : '' );
						$output .= '<div class="post-box-contents">';
							$output .= '<a href="' . get_permalink( $post_id ) . '"><h4 class="post-title"'.( !empty($extra['titlecolor']) && $extra['titlecolor'] != '#' ? ' style="color: ' . esc_attr($extra['titlecolor']) . ';"' : '' ).'>' . get_the_title( $post_id ) . '</h4></a><p class="blog-auth">';

							// author& comments
							$output .= ( $extra['showauthordetails'] ? __( 'Posted By ' , 'pro' ) . '<a style="color: rgb(79, 147, 182);" href="'.get_author_posts_url( $post->post_author ).'">'.get_the_author_meta( 'display_name' , $post->post_author ).'</a></p>' : '' );

						if ( $extra['showexcerpt'] )
								$output .= '<div id="blog-con" class="post-excerpt">' . ( !empty($post->post_excerpt) ? $post->post_excerpt : ( !empty($post->post_content) ? wp_trim_words($post->post_content , apply_filters( 'single_post_style_2_excerpt_words' , ( $columns == '1col' ? 45 : 30 ) , $instance ) ) : '' ) ) . '</div>';
							
							if ( $extra['showmeta'] ) {
								$post_meta_output = '<div id="blog-rm" class="post-meta-tags'.( !empty($extra['metatagscolor']) ? ' ' . esc_attr( $extra['metatagscolor'] ) : '' ).'">';
									if ( $post->post_type == 'product' ) {
										$post_meta_output .= get_the_term_list( $post_id , 'product_category' , '<span>' , '</span><span>' , '</span>' ) . get_the_term_list( $post_id , 'product_tag' , '<span>' , '</span><span>' , '</span>' );
									} elseif ( !empty($taxonomy) ) {
										$post_meta_output .= get_the_term_list( $post_id , $taxonomy , '<span>' , '</span><span>' , '</span>' );
									} else {
										//$post_meta_output .= get_the_term_list(  $post_id , 'category' , '<span>' , '</span><span>' , '</span>'  ) . get_the_tag_list(  '<span>' , '</span><span>' , '</span>'  );
										if(ICL_LANGUAGE_CODE=='bg'):
										$post_meta_output .= "<span style='margin-left: -135px;'><a href='" . get_permalink( $post_id ) . "'>".__('Read More','pro')."</a></span>";
										else:
										$post_meta_output .= "<span><a href='" . get_permalink( $post_id ) . "'>".__('Read More','pro')."</a></span>";
										endif;
									}
									
								$post_meta_output .= '</div>'; // end - post-meta-tags
								$output .= apply_filters( 'pro_single_post_box_meta_section' , $post_meta_output , $instance );
							}												
						if ( !has_post_thumbnail($post_id) ) $output .= "</div></div>";
						$output .= '</div>'; // end post-box-contents
					$output .= ( $columns == '1col' ? '</div>' : '' ); // end - span
				$output .= ( $columns == '1col' ? '</div>' : '' ); // end - row
		  	$output .= '</div>'; // End - single-post-box
		$output .= ( $columns == '1col' ? '' : '</div>' ); // End - span-box  		
		

	  	if ($echo)
	    	echo apply_filters( 'func_pro_load_wp_single_post_style_2' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_load_wp_single_post_style_2' , $output , $instance );
	}

	/* Shortcode version of Post Query
	------------------------------------------------------------------------------------------------------------------- */

	add_shortcode( 'pro_postquery', 'pro_load_post_query_sc' );
	function pro_load_post_query_sc( $atts = '' , $content = null ) {

		extract( shortcode_atts(array(
			'posttype' => 'post',
			'paginate' => 'yes',
			'page' => '',
			'perpage' => '',
			'orderby' => 'date',
			'order' => 'DESC',
			'searchterms' => '',
			'taxonomy' => '',
			'category' => '',
			'tag' => '',
			'author' => '',
			'day' => '',
			'month' => '',
			'year' => '',
			'columns' => '3',
			'poststyle' => 'style-2',
			'btncolor' => 'black',
			'showexcerpt' => 'yes',
			'showmeta' => 'yes',
			'showauthorname' => 'yes',
			'showcomments' => 'yes',
			'showdate' => 'yes',
			'titlecolor' => '',
			'datetagcolor' => 'black',
			'metatagscolor' => 'lightblue',
			'class' => '',
			'style' => '',
		), $atts , 'pro_postquery' ) );

		// check if post type exists
		if  ( !post_type_exists( $posttype ) )
			return '<div class="alert alert-error">' . __( 'The selected post type doesn\'t exist.' , 'pro' ) . '</div>';

		switch ( $columns ) {
			case '1':
				$columns = '1col';
				break;

			case '2':
				$columns = '2col';
				break;

			case '3':
			default:
				$columns = '3col';
				break;

			case '4':
				$columns = '4col';
				break;
		}

		switch ($btncolor) {
			case 'grey':
				$btnclass = '';
				break;
			case 'blue':
				$btnclass = ' btn-primary';
				break;
			case 'lightblue':
				$btnclass = ' btn-info';
				break;
			case 'green':
				$btnclass = ' btn-success';
				break;
			case 'yellow':
				$btnclass = ' btn-warning';
				break;
			case 'red':
				$btnclass = ' btn-danger';
				break;
			case 'black':
			default:
				$btnclass = ' btn-inverse';
				break;			
		}

		$args = array(
			'echo' => false,
			'posttype' => esc_attr( $posttype ),
			'paginate' => ( $paginate == 'yes' ? true : false ),
			'page' => ( !empty($page) ? intval($page) : '' ), 
			'per_page' => ( !empty($perpage) ? intval($perpage) : '' ), 
			'order_by' => $orderby, 
			'order' => $order,
			'searchterms' => $searchterms,
			'taxonomy' => esc_attr( $taxonomy ),
			'category' => esc_attr( $category ), 
			'tag' => esc_attr( $tag ),
			'author' => esc_attr( $author ),
			'day' => esc_attr( $day ),
			'month' => esc_attr( $month ),
			'year' => esc_attr( $year ),
			'columns' => $columns,
			'poststyle' => ( $poststyle == 'style-2' ? 'style-2' : 'style-1' ),
			'btnclass' => $btnclass,
			'boxclass' => ( $poststyle == 'style-2' ? 'post-grid' : '' ) . ( !empty($class) ? ' ' . esc_attr($class) : '' ),
			'boxstyle' => $style,
			'extra' => array(
				'showexcerpt' => ( $showexcerpt == 'yes' ? true : false ),
				'showmeta' => ( $showmeta == 'yes' ? true : false ),
				'showauthordetails' => ( $showauthorname == 'yes' ? true : false ),
				'showcomments' => ( $showcomments == 'yes' ? true : false ),
				'showhovertag' => ( $showdate == 'yes' ? true : false ),
				'titlecolor' => esc_attr( $titlecolor ),
				'hovertagcolor' => 'hover-tag-' . esc_attr( $datetagcolor ),
				'metatagscolor' => 'meta-tag-' . esc_attr( $metatagscolor )
				)
			);

		return apply_filters( 'func_pro_load_post_query_sc' , pro_display_wp_post_query( $args ) , $atts );
	}