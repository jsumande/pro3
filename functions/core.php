<?php

/**
 * general setting functions.
 *
 */

	// load site logo
	function mpt_load_site_logo_pro( $echo = true ) {
		global $wmpl_lang2;
		$themefolder = get_template_directory_uri();
		//$mpt_logo_upload = esc_url(get_option('mpt_sitelogo'));
		$mpt_logo_upload = esc_url(get_blog_option(1, 'mpt_sitelogo'));
		//$mpt_logo_position = get_option('mpt_sitelogo_position');
		$mpt_logo_position = get_blog_option(1,'mpt_sitelogo_position');
		switch ($mpt_logo_position) {
			case 'Align Left':
				$position = ' align-left';
				break;

			case 'Align Center':
				$position = ' align-center';
				break;

			case 'Align Right':
				$position = ' align-right';
				break;
			
			default:
				$position = ' align-center';
				break;
		}

	  	if ($echo)
            echo apply_filters( 'func_mpt_load_site_logo_pro' , '<div class="col-md-3 col-sm-12 col-xs-12"><div class="pro-site-logo'.$position.'"><a href="'.get_site_url(1).'/'.$wmpl_lang2.'"><img src="'.( !empty($mpt_logo_upload) ? $mpt_logo_upload : $themefolder.'/img/site-logo.png' ).'" alt="" /></a></div></div>' );
	  	else
            return apply_filters( 'func_mpt_load_site_logo_pro' , '<div class="col-md-3 col-sm-12 col-xs-12"><div class="pro-site-logo'.$position.'"><a href="'.get_site_url(1).'/'.$wmpl_lang2.'"><img src="'.( !empty($mpt_logo_upload) ? $mpt_logo_upload : $themefolder.'/img/site-logo.png' ).'" alt="" /></a></div></div>' );
	}

	//load site favicon
	function mpt_load_favicon() {
		//$mpt_favicon_upload = esc_url(get_option('mpt_favicon'));
		$mpt_favicon_upload = esc_url(get_blog_option(1,'mpt_favicon'));
		$mpt_favicon_upload  = is_ssl()? set_url_scheme($mpt_favicon_upload,'https') : $mpt_favicon_upload;
		if(!empty($mpt_favicon_upload)) {
			echo apply_filters( 'func_mpt_load_favicon' , $mpt_favicon_upload );
		 } else {
			echo apply_filters('func_mpt_load_favicon' , '' );
		}
	}

	//load footer text
	function mpt_load_footer_text() {
		//$customfooter = get_option('mpt_cus_footer');
		$customfooter = get_blog_option(1,'mpt_cus_footer');
		$custom = wp_kses( $customfooter, array(
					'a' => array(
						'href' => array(),
						'title' => array()
						),
					'br' => array(),
					'em' => array(),
					'strong' => array() 
					) ); 
		$date = date("Y");
		$sitename = esc_attr(get_bloginfo('name'));
			if(!empty($custom)) {
				echo apply_filters( 'func_mpt_load_footer_text' , '<p>'.$custom.'</p>' );
			 } else {
				echo apply_filters( 'func_mpt_load_footer_text' , '<p>'. __( 'Copyright &copy;' , 'pro' ) .$date.' '.$sitename.' | '.__( 'Designed by' , 'pro' ).' <a href="http://www.marketpressthemes.com"><b>MarketPressThemes.com</b></a></p>' );
			}
	}
	
/**
 * Styling Options.
 *
 */	

	function mpt_load_google_web_font( $echo = true ) {
		//$selected = get_option('mpt_enable_google_web_font');
		$selected = get_blog_option(1,'mpt_enable_google_web_font');
		if ( $selected == 'false' ) {
			$output = '';
		} else {
			$output = mpt_load_google_web_font_header();
			$output .= mpt_load_google_web_font_body();
			$output .= mpt_load_custom_google_font_header();
			$output .= mpt_load_custom_google_font_body();
		}

	  	if ($echo)
	    	echo apply_filters( 'func_mpt_load_google_web_font' , $output );
	  	else
	    	return apply_filters( 'func_mpt_load_google_web_font' , $output );
	}
	
	function mpt_load_google_web_font_header() {
		//$selected = get_option('mpt_theme_header_font');
		$selected = get_blog_option(1,'mpt_theme_header_font');
		//$customfont = get_option('mpt_theme_custom_web_font');
		$customfont = get_blog_option(1,'mpt_theme_custom_web_font');
		//$customheaderfont = esc_attr(get_option('mpt_theme_custom_web_font_header'));
		$customheaderfont = esc_attr(get_blog_option(1,'mpt_theme_custom_web_font_header'));

		if ($customfont != 'true' || empty($customheaderfont)) {
			$selected = str_replace(' ', '+', $selected);
			return apply_filters( 'func_mpt_load_google_web_font_header' , '<link href="'.( is_ssl() ? 'https' : 'http' ).'://fonts.googleapis.com/css?family='.$selected.'" rel="stylesheet" type="text/css">' );
		}
	}
	
	function mpt_load_google_web_font_body() {
		//$selected = get_option('mpt_theme_body_font');
		$selected = get_blog_option(1,'mpt_theme_body_font');
		//$customfont = get_option('mpt_theme_custom_web_font');
		$customfont = get_blog_option(1,'mpt_theme_custom_web_font');
		//$custombodyfont = esc_attr(get_option('mpt_theme_custom_web_font_body'));
		$custombodyfont = esc_attr(get_blog_option(1,'mpt_theme_custom_web_font_body'));

		if ($customfont != 'true' || empty($custombodyfont)) {
			$selected = str_replace(' ', '+', $selected);
			return apply_filters( 'func_mpt_load_google_web_font_body' , '<link href="'.( is_ssl() ? 'https' : 'http' ).'://fonts.googleapis.com/css?family='.$selected.'" rel="stylesheet" type="text/css">' );
		}
	}

	function mpt_load_custom_google_font_header() {
		//$customfont = get_option('mpt_theme_custom_web_font');
		$customfont = get_blog_option(1,'mpt_theme_custom_web_font');
		//$customheaderfont = esc_attr(get_option('mpt_theme_custom_web_font_header'));
		$customheaderfont = esc_attr(get_blog_option(1,'mpt_theme_custom_web_font_header'));

		if ($customfont == 'true' && !empty($customheaderfont)) {
			return apply_filters( 'func_mpt_load_custom_google_font_header' , '<link href="'.( is_ssl() ? 'https' : 'http' ).'://fonts.googleapis.com/css?family='.$customheaderfont.'" rel="stylesheet" type="text/css">' );
		}
	}

	function mpt_load_custom_google_font_body() {
		//$customfont = get_option('mpt_theme_custom_web_font');
		$customfont = get_blog_option(1,'mpt_theme_custom_web_font');
		//$custombodyfont = esc_attr(get_option('mpt_theme_custom_web_font_body'));
		$custombodyfont = esc_attr(get_blog_option(1,'mpt_theme_custom_web_font_body'));

		if ($customfont == 'true' && !empty($custombodyfont)) {
			return apply_filters( 'func_mpt_load_custom_google_font_body' , '<link href="'.( is_ssl() ? 'https' : 'http' ).'://fonts.googleapis.com/css?family='.$custombodyfont.'" rel="stylesheet" type="text/css">' );
		}
	}

	function mpt_load_bg_patterns($bgpattern) {
		$themefolder =  get_template_directory_uri();
		switch ($bgpattern) {
			case 'pattern_1':
				return apply_filters( "func_mpt_load_bg_patterns" , "background-image: url('".$themefolder."/img/patterns/pat_01.png');" , $bgpattern );
			break;
			case 'pattern_2':
				return apply_filters( "func_mpt_load_bg_patterns" , "background-image: url('".$themefolder."/img/patterns/pat_02.png');" , $bgpattern );
			break;
			case 'pattern_3':
				return apply_filters( "func_mpt_load_bg_patterns" , "background-image: url('".$themefolder."/img/patterns/pat_03.png');" , $bgpattern );
			break;
			case 'pattern_4':
				return apply_filters( "func_mpt_load_bg_patterns" , "background-image: url('".$themefolder."/img/patterns/pat_04.png');" , $bgpattern );
			break;
			case 'pattern_5':
				return apply_filters( "func_mpt_load_bg_patterns" , "background-image: url('".$themefolder."/img/patterns/pat_05.png');" , $bgpattern );
			break;
			case 'pattern_6':
				return apply_filters( "func_mpt_load_bg_patterns" , "background-image: url('".$themefolder."/img/patterns/pat_06.png');" , $bgpattern );
			break;
			case 'pattern_7':
				return apply_filters( "func_mpt_load_bg_patterns" , "background-image: url('".$themefolder."/img/patterns/pat_07.png');" , $bgpattern );
			break;
			case 'pattern_8':
				return apply_filters( "func_mpt_load_bg_patterns" , "background-image: url('".$themefolder."/img/patterns/pat_08.png');" , $bgpattern );
			break;
			case 'pattern_9':
				return apply_filters( "func_mpt_load_bg_patterns" , "background-image: url('".$themefolder."/img/patterns/pat_09.png');" , $bgpattern );
			break;
			case 'pattern_10':
				return apply_filters( "func_mpt_load_bg_patterns" , "background-image: url('".$themefolder."/img/patterns/pat_10.png');" , $bgpattern );
			break;

		}
	}	

/**
 * Header Setting functions.
 *
 */	

	function mpt_load_header_style( $echo = true ) {

		$themefolder = get_template_directory_uri();
		//$selected = get_option('mpt_header_section_main_style');
		$selected = get_blog_option(1,'mpt_header_section_main_style');
		//$mpt_logo_upload = esc_url(get_option('mpt_sitelogo'));
		$mpt_logo_upload = esc_url(get_blog_option(1,'mpt_sitelogo'));
		
		$output = '';
		
		if ( $selected == 'Style 3' ) {

			$output .= '<div id="header-wrapper" class="header-style-3">';
				$output .= '<div class="outercontainer">';
					$output .= '<div class="container">	';

						$output .= '<div class="pro-site-logo"><a href="'.home_url().'"><img src="'.( !empty($mpt_logo_upload) ? $mpt_logo_upload : $themefolder.'/img/site-logo-small.png' ).'" alt="" />'.apply_filters( 'pro_header3_logo_tagline' , '<div class="clear hidden-desktop"></div><span class="pro-logo-tagline">' . esc_attr(get_bloginfo('description')) . '</span>' ).'</a></div>';

						$output .= '<div class="clear hidden-desktop"></div>';

						$output .= '<div class="main-menu-holder">';
							$output .= wp_nav_menu( array( 'theme_location' => 'mainmenu' , 'depth'  => 3 , 'menu_id'  => 'main-nav-menu' , 'container'	=> false , 'echo' => false , 'menu_class'	=> 'nav nav-pills pull-right' , 'walker' => new twitter_bootstrap_nav_walker() ) );
						$output .= '</div>'; // end - main-menu-holder

						$output .= '<div class="header-top-right-menu">';
							//jeff new code
							if ( !is_user_logged_in() ) {
								$output .= '<a href="/login/">Login</a>'; 
								$output .= '<a href="/signup/">Signup</a>';
							}
						$output .= '</div>';						
						$output .= '<div class="clear"></div>';

					$output .= '</div>'; // end - container
				$output .= '</div>'; // end - outercontainer
			$output .= '</div>'; // end - header-wrapper

		} elseif ( $selected == 'Style 2' ) {

            $output .= '<div id="header-wrapper">';
            $output .= '<div id="page" class="hfeed site">';
                $output .= '<header id="masthead" class="site-header" role="banner">';
                    $output .= '<div class="full-width-blue">';
						$output .= '<div class="container">	';
                        $output .= '<div class="row site-branding ">';

							$output .= mpt_load_site_logo_pro( false );
                            $output .= '<div class="col-md-9  header-top-right-menu">';
                            $output .= '<div class="row">';
                            $output .= '<div class="col-md-12">';
                            $output .= '<div class="site-branding tool-menu nav-top-right align-right">';
							$output .= manual_menu();
							$output .= '<div class="header-menu-cart">';		
							$output .= do_shortcode('[mpawc design="box" cartstyle="modal" btncolor="black"]');
                            $output .= '</div>'; //site-branding tool-menu nav-top-right
                            $output .= '</div>'; //col-md-12
							$output .= '</div>';
                            $output .= '</div>'; //row
                            $output .= '</div>'; //col-md-12  header-top-right-menu
							$output .= '<div class="clear"></div>';
                        $output .= '</div>'; // end - row site-branding
						$output .= '</div>'; // end - container
                    $output .= '</div>'; // end - full-width-blue
                $output .= '</header>'; // end - header-container

				$output .= '<div id="menu-wrapper" class="navbar navbar-inverse">';
					$output .= '<div class="navbar-inner">';
					$output .= '<div class="outercontainer">';
                        $output .='
                                <div class="row">
                                    <div class="col-sm-7 col-md-7 menu-nav-indent">
                        ';
                                    $output .= '<nav id="site-navigation" class="navbar" role="navigation">';
                                    $output .= '
                                                <div class="navbar-header">
                                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                  </button>
                                                </div>
                                            ';
                                   // JAYVEE STATIC LIST DETECTED NEED TO MODIFY THIS

                              $output .= (!is_admin() ? getNavString() : "");

                                    $output .= '</nav>';
                                $output .= '</div>';

								//Jeff get the widget
                                $output .= '<div class="col-sm-5 col-md-5"><div class="header-search-menu">';
								
								$output .= get_the_widget( 'MarketPress_Global_Product_Search_Widget','showcategory=0&showtag=0&showpricerange=0' );
                                $output .= '</div></div>';
								//Jeff AnyWhere Cart
						$output .= '</div>'; // end - container
					$output .= '</div>'; // end - outercontainer
					$output .= '</div>'; // end - navbar-inner
				$output .= '</div>'; // end - navbar 

            $output .= '</div>'; // end - page
			$output .= '</div>'; // end - header-wrapper

		} else {
			$output .= '<div id="header-wrapper">';
				$output .= '<div class="outercontainer">';
					$output .= '<div class="container">	';

						$output .= mpt_load_site_logo_pro( false );
                        $output .= '<div id="menu-wrapper" class="navbar-inverse">';
							$output .= '<div class="navbar-inner">';
								$output .= '<div class="visible-desktop">' . wp_nav_menu( array( 'theme_location' => 'mainmenu' , 'depth'  => 3 , 'container'	=> false , 'echo' => false , 'menu_class'	=> 'nav desktop' , 'walker' => new twitter_bootstrap_nav_walker() ) ) . '</div>';
								$output .= '<div class="hidden-desktop">';
									$output .= '<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a>';
									$output .= '<div class="nav-collapse">' . wp_nav_menu( array( 'theme_location' => 'mainmenu' , 'depth'  => 3 , 'container'	=> false , 'echo' => false , 'menu_class'	=> 'nav nav-pills pull-right' , 'walker' => new twitter_bootstrap_nav_walker() ) ) . '</div>';
								$output .= '</div>'; // end - hidden-desktop
							$output .= '</div>'; // end - navbar-inner
						$output .= '</div>'; // end - navbar 

					$output .= '</div>'; // end - container
				$output .= '</div>'; // end - outercontainer
			$output .= '</div>'; // end - header-wrapper
		}

	  	if ($echo)
	    	echo apply_filters( 'func_mpt_load_header_style' , $output );
	  	else
	    	return apply_filters( 'func_mpt_load_header_style' , $output );

	}
	
/**
 * Footer Setting functions.
 *
 */	

	function mpt_footer_widget_setting_1() {
		//$selected = get_option('mpt_footer_widget_setting');
		$selected = get_blog_option(1,'mpt_footer_widget_setting');
		if($selected == 'widget633') {
				echo apply_filters( 'func_mpt_footer_widget_setting_1' , '6' );
		} elseif ($selected == 'widget336') {
				echo apply_filters( 'func_mpt_footer_widget_setting_1' , '3' );
		} elseif ($selected == 'widget3333') {
				echo apply_filters( 'func_mpt_footer_widget_setting_1' , '3' );
		} else {
				echo apply_filters( 'func_mpt_footer_widget_setting_1' , '4' );
		}
	}
	
	function mpt_footer_widget_setting_2() {
		//$selected = get_option('mpt_footer_widget_setting');
		$selected = get_blog_option(1,'mpt_footer_widget_setting');
		if($selected == 'widget633') {
				echo apply_filters( 'func_mpt_footer_widget_setting_2' , '3' );
		} elseif ($selected == 'widget336') {
				echo apply_filters( 'func_mpt_footer_widget_setting_2' , '3' );
		} elseif ($selected == 'widget3333') {
				echo apply_filters( 'func_mpt_footer_widget_setting_2' , '3' );
		} else {
				echo apply_filters( 'func_mpt_footer_widget_setting_2' , '4' );
		}
	}
	
	function mpt_footer_widget_setting_3() {
		//$selected = get_option('mpt_footer_widget_setting');
		$selected = get_blog_option(1,'mpt_footer_widget_setting');
		if($selected == 'widget633') {
				echo apply_filters( 'func_mpt_footer_widget_setting_3' , '3' );
		} elseif ($selected == 'widget336') {
				echo apply_filters( 'func_mpt_footer_widget_setting_3' , '6' );
		} elseif ($selected == 'widget3333') {
				echo apply_filters( 'func_mpt_footer_widget_setting_3' , '3' );
		} else {
				echo apply_filters( 'func_mpt_footer_widget_setting_3' , '4' );
		}
	}

	function mpt_footer_widget_setting_4() {
		//$selected = get_option('mpt_footer_widget_setting');
		$selected = get_blog_option(1,'mpt_footer_widget_setting');
		if($selected == 'widget3333') {
				echo apply_filters( 'func_mpt_footer_widget_setting_4' , '3' );
		} else {
				echo apply_filters( 'func_mpt_footer_widget_setting_4' , '' );
		}
	}

	function mpt_footer_widget_4_columns() {
		//$selected = get_option('mpt_footer_widget_setting');
		$selected = get_blog_option(1,'mpt_footer_widget_setting');
		if($selected == 'widget3333') {
				return apply_filters( 'func_mpt_footer_widget_4_columns' , true );
		} else {
				return apply_filters( 'func_mpt_footer_widget_4_columns' , false );
		}
	}


/**
 * Social Icon functions.
 *
 */	

	function mpt_social_icon_section( $echo = true ) {

		$output = '';

		for ($i=1; $i < 9; $i++) { 
			
			//$profileurl = esc_url(get_option('mpt_social_icon_'.$i.'_url'));
			$profileurl = esc_url(get_blog_option(1,'mpt_social_icon_'.$i.'_url'));
			//$selected = get_option('mpt_social_icon_'.$i.'_icon');
			$selected = get_blog_option(1,'mpt_social_icon_'.$i.'_icon');

			switch ( $selected ) {
				case 'Facebook':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Facebook"><i class="icon-facebook-sign icon-large"></i></a></li>';
					break;

				case 'Dribbble':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Dribbble"><i class="icon-dribbble icon-large"></i></a></li>';
					break;

				case 'Flickr':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Flickr"><i class="icon-flickr icon-large"></i></a></li>';
					break;

				case 'FourSquare':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="FourSquare"><i class="icon-foursquare icon-large"></i></a></li>';
					break;

				case 'Google Plus':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Google Plus"><i class="icon-google-plus-sign icon-large"></i></a></li>';
					break;

				case 'Instagram':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Instagram"><i class="icon-instagram icon-large"></i></a></li>';
					break;

				case 'Linkedin':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Linkedin"><i class="icon-linkedin-sign icon-large"></i></a></li>';
					break;

				case 'Pinterest':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Pinterest"><i class="icon-pinterest-sign icon-large"></i></a></li>';
					break;

				case 'RenRen':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="RenRen"><i class="icon-renren icon-large"></i></a></li>';
					break;

				case 'Skype':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Skype"><i class="icon-skype icon-large"></i></a></li>';
					break;

				case 'Tumblr':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Tumblr"><i class="icon-tumblr-sign icon-large"></i></a></li>';
					break;				

				case 'Twitter':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Twitter"><i class="icon-twitter-sign icon-large"></i></a></li>';
					break;

				case 'VK':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="VK"><i class="icon-vk icon-large"></i></a></li>';
					break;				

				case 'Weibo':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Weibo"><i class="icon-weibo icon-large"></i></a></li>';
					break;

				case 'Xing':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Xing"><i class="icon-xing icon-large"></i></a></li>';
					break;				

				case 'YouTube':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="YouTube"><i class="icon-youtube-sign icon-large"></i></a></li>';
					break;

				case 'RSS':
					$output .= '<li><a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="RSS"><i class="icon-rss-sign icon-large"></i></a></li>';
					break;

				case 'None':
				default:
					$output .= '';
					break;
			}
		}

		if ( $echo )
			echo apply_filters( 'func_mpt_social_icon_section' , $output );
		else
			return apply_filters( 'func_mpt_social_icon_section' , $output );
	}
	
/**
 * SEO functions.
 *
 */	
	//load site title
	function mpt_load_site_title() {
		//$customtitle = esc_attr(get_option('mpt_cus_title'));
		$customtitle = esc_attr(get_blog_option(1,'mpt_cus_title'));
		//$selected = get_option('mpt_enable_custom_title');
		$selected = get_blog_option(1,'mpt_enable_custom_title');
		//Jeff 1
		$blogtitle = esc_attr(get_bloginfo('name'));
		if($selected == 'true') {
			if(!empty($customtitle)) {
				echo apply_filters( 'func_mpt_load_site_title' , $customtitle );
			 } else {
				echo apply_filters( 'func_mpt_load_site_title' , $blogtitle );
			}
		} else {
				echo apply_filters( 'func_mpt_load_site_title' , $blogtitle );
			}
	}
	
	//load meta description
	function mpt_load_meta_desc() {
		global  $blog_id;
		//$metadesc = esc_attr(get_option('mpt_cus_meta_desc'));
		$metadesc = esc_attr(get_blog_option(1,'mpt_cus_meta_desc'));
		//$selected = get_option('mpt_enable_meta_desc');
		$selected = get_blog_option(1,'mpt_enable_meta_desc');
		//$blogdesc = esc_attr(get_bloginfo('description'));
		$blogdesc = esc_attr(get_blog_option(1,'blogdescription'));
		
		if(!is_main_site()){
			$blogdesc = wp_trim_words(tpsm_trim_all(strip_shortcodes(strip_tags(get_blog_option($blog_id,'blogdescription')))),50);
		}
		
		if($selected == 'true') {
			if(!empty($metadesc)) {
				echo apply_filters( 'func_mpt_load_meta_desc' , $metadesc );
			 } else {
				echo apply_filters( 'func_mpt_load_meta_desc' , $blogdesc );
			}
		} else {
				echo apply_filters( 'func_mpt_load_meta_desc' , $blogdesc );
			}
	}

	//Extract keywords function from description
	function extractKeyWords($string) {
	  mb_internal_encoding('UTF-8');
	  $stopwords = array('i','a','about','an','and','are','as','at','be','by','com','de','en','for','from','how','in','is','it','la','of','on','or','that','the','this','to','was','what','when','where','who','will','with','und','the','www');
	  $string = preg_replace('/[\pP]/u', '', trim(preg_replace('/\s\s+/iu', '', mb_strtolower($string))));
	  $matchWords = array_filter(explode(' ',$string) , function ($item) use ($stopwords) { return !($item == '' || in_array($item, $stopwords) || mb_strlen($item) <= 2 || is_numeric($item));});
	  $wordCountArr = array_count_values($matchWords);
	  arsort($wordCountArr);
	  return array_keys(array_slice($wordCountArr, 0, 10));
	}
	
	//load meta keywords
	function mpt_load_meta_keywords() {
		//$metakey = esc_attr(get_option('mpt_cus_meta_keywords'));
		$metakey = esc_attr(get_blog_option(1,'mpt_cus_meta_keywords'));
		//$selected = get_option('mpt_enable_meta_keywords');
		$selected = get_blog_option(1,'mpt_enable_meta_keywords');
		
		$selected = true;
		
		if(empty($metakey)){
			global $blog_id;
			$metakeys = wp_trim_words(tpsm_trim_all(strip_shortcodes(strip_tags(get_blog_option($blog_id,'blogdescription')))),50);
			$metakey = implode(',', extractKeyWords($metakeys));
		}	
		
		if($selected == 'true') {
			if(!empty($metakey)) {
				echo apply_filters( 'func_mpt_load_meta_keywords' , $metakey );
			 } else {
				echo apply_filters( 'func_mpt_load_meta_keywords' , '' );
			}
		} else {
				echo apply_filters( 'func_mpt_load_meta_keywords' , '' );
			}
	}

/**
 * Code Integration function.
 *
 */
	function mpt_load_header_code() {
		//$headercode = get_option('mpt_header_code');
		$headercode = get_blog_option(1,'mpt_header_code');
		//$selected = get_option('mpt_enable_header_code');
		$selected = get_blog_option(1,'mpt_enable_header_code');
		if($selected == 'true') {
			if(!empty($headercode)) {
				echo apply_filters( 'func_mpt_load_header_code' , $headercode );
			 }
		}
	}

	function mpt_load_body_code() {
		//$bodycode = get_option('mpt_body_code');
		$bodycode = get_blog_option(1,'mpt_body_code');
		//$selected = get_option('mpt_enable_body_code');
		$selected = get_blog_option(1,'mpt_enable_body_code');
		if($selected == 'true') {
			if(!empty($bodycode)) {
				echo apply_filters( 'func_mpt_load_body_code' , $bodycode );
			 }
		}
	}
	
	function mpt_load_top_code() {
		//$topcode = get_option('mpt_top_code');
		$topcode = get_blog_option(1,'mpt_top_code');
		//$selected = get_option('mpt_enable_top_code');
		$selected = get_blog_option(1,'mpt_enable_top_code');
		if($selected == 'true') {
			if(!empty($topcode)) {
				echo apply_filters( 'func_mpt_load_top_code' , $topcode.'<div class="clear padding10"></div>' );
			 }
		}
	}
	
	function mpt_load_bottom_code() {
		//$bottomcode = get_option('mpt_bottom_code');
		$bottomcode = get_blog_option(1,'mpt_bottom_code');
		//$selected = get_option('mpt_enable_bottom_code');
		$selected = get_blog_option(1,'mpt_enable_bottom_code');
		if($selected == 'true') {
			if(!empty($bottomcode)) {
				echo apply_filters( 'func_mpt_load_bottom_code' , $bottomcode.'<div class="clear padding10"></div>' );
			 }
		}
	}

/**
 * Post Functions.
 *
 */

	function mpt_load_featured_image( $args = array() ) {

	  	$defaults = array(
			'echo' => false,
			'post_id' => NULL,
			'content_type' => 'single',
			'imagesize' => 'full',
			'widthcontrol' => true,
			'prettyphoto' => false,
			'btnclass' => '',
			'iconclass' => '',
		);

	  	$instance = wp_parse_args( $args, $defaults );
	  	extract( $instance );

	  	global $id;
	  	$post_id = ( NULL === $post_id ) ? $id : $post_id;

		$fullimage = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' );
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $imagesize );

		

		if ( $widthcontrol ) {
			if ( $image[1] < 360 ) {
				$style = ' style="max-width: 360px;"';
			} elseif ( $image[1] < 560 ) {
				$style = ' style="max-width: 560px;"';
			}
			else {
				$style = ' style="max-width: 860px;"';
			}
		} else {
			$style = '';
		}
		
		//get the image
		$ximageid = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'tb-360' );
		$xfeatimage = $ximageid['0'];		
		//if ( is_page( 747 )  xor  is_page( 637 ) )  //  Mae local for fixes purposes
		if ( is_page( 2317 )  xor  is_page( 2313 ) ) 
		$output .= '<div class="featured-image-box add-fadeinup-effects-parent"'.$style.' id="xx" style="background-image: url('.$ximageid['0'].');">';
		else
		$output .= '<div class="featured-image-box add-fadeinup-effects-parent"'.$style.'>';
	
			$output .= '<div class="hover-btn add-fadeinup-effects-child">';

			if ($prettyphoto == 'true') {
				$output .= '<a href="'.$fullimage[0].'" class="btn btn-flat btn-block btn-block-20'.$btnclass.' mpt-pp-trigger" data-pptitle="'.apply_filters( 'mpt_featured_image_prettyphoto_caption' , get_the_title( $post_id ) , $post_id ).'"><i class="icon-search'.$iconclass.'"></i>'.__( ' View Image' , 'pro' ).'</a>';
			} else {
				$output .= '<a href="'.get_permalink( $post_id ).'" class="btn btn-flat btn-block btn-block-20'.$btnclass.'"><i class="icon-search'.$iconclass.'"></i>'.__( ' Read Post' , 'pro' ).'</a>';
			}
			$output .= '</div>'; // End hover-btn

		if ($prettyphoto == 'true') {
			$output .= '<a href="'.$fullimage[0].'" class="mpt-pp-trigger" data-pptitle="'.apply_filters( 'mpt_featured_image_prettyphoto_caption' , get_the_title( $post_id ) , $post_id ).'">';
		} else {
			$output .= '<a href="'.get_permalink($post_id).'" >';
		}
		
		if ( $image[2] < 275  && $image[1] < 270 ) {
			//$output .= get_the_post_thumbnail( $post_id , $imagesize );
			$imageid = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'tb-360' );
			$featimage = $imageid['0'];
			$img_url = '<img src="' . $featimage . '">';			
			//if ( !is_page( 747 ) xor is_page( 637 ) ) // Mae local for fixes purposes
			if ( is_page( 2317 )  xor  is_page( 2313 ) ) 
			{ 
			$output .= '<img src="" class="trcksr-img">'; 
			$output .= '<img src="' . $featimage . '" style="display: none;" class="trcksr-img2">'; 
			}
			else {			
			$output .= $img_url; 
			}
			
		} else {
			//$output .= get_the_post_thumbnail( $post_id , $imagesize );
			$imageid = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'tb-360' );
			$featimage = $imageid['0'];
			$img_url = '<img src="' . $featimage . '">';			
			//if ( !is_page( 747 ) xor is_page( 637 ) ) // Mae local for fixes purposes
			if ( is_page( 2317 )  xor  is_page( 2313 ) ) 
			{ 
			$output .= '<img src="" class="trcksr-img">'; 
			$output .= '<img src="' . $featimage . '" style="display: none;" class="trcksr-img2">'; 
			}
			else {			
			$output .= $img_url; 
			}
		}		
			$output .= '</a>';
		$output .= '</div>'; // End image-box
		
	  	if ($echo)
	    	echo apply_filters( 'func_mpt_load_featured_image' , $output , $instance );
	  	else
	    	return apply_filters( 'func_mpt_load_featured_image' , $output , $instance );
	}

	function mpt_load_image_carousel( $args = array() ) {

	  	$defaults = array(
			'echo' => false,
			'post_id' => NULL,
			'content_type' => 'single',
			'imagesize' => 'full',
			'masonry' => false,
			'prettyphoto' => false,
			'btnclass' => '',
			'iconclass' => '',
		);

	  	$instance = wp_parse_args( $args, $defaults );
	  	extract( $instance );

	  	global $id;
	  	$post_id = ( NULL === $post_id ) ? $id : $post_id;

		$image1 = get_post_meta( $id, '_mpt_video_featured_image_2', true );
		$imageurl1 = esc_url($image1);
		$image2 = get_post_meta( $id, '_mpt_video_featured_image_3', true );
		$imageurl2 = esc_url($image2);
		$fullimage = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'full');

		$output = '<div class="flexslider featured-images-slider">';
			$output .= '<ul class="slides">';
				// silde 1
				if ( !empty($fullimage[0]) ) {
					$output .= '<li>';
						$output .= ( $prettyphoto == 'true' ? '<a href="'.$fullimage[0].'" rel="prettyPhoto[carousel-'.$post_id.']">' : '<a href="'.get_permalink($post_id).'">' );
							$output .= '<img src="'.$fullimage[0].'" alt="'.apply_filters( 'mpt_image_carousel_prettyphoto_caption' , get_the_title( $post_id ) , $post_id , 'image-1' ).'" />';
						$output .= '</a>';
					$output .= '</li>';
				}

				// slide 2
				if ( !empty($imageurl1) ) {
					$output .= '<li>';
						$output .= ( $prettyphoto == 'true' ? '<a href="'.$imageurl1.'" rel="prettyPhoto[carousel-'.$post_id.']">' : '<a href="'.get_permalink($post_id).'">' );
							$output .= '<img src="'.$imageurl1.'" alt="'.apply_filters( 'mpt_image_carousel_prettyphoto_caption' , get_the_title( $post_id ) , $post_id , 'image-2' ).'" />';
						$output .= '</a>';
					$output .= '</li>';
				}


				// slide 3
				if ( !empty($imageurl2) ) {
					$output .= '<li>';
						$output .= ( $prettyphoto == 'true' ? '<a href="'.$imageurl2.'" rel="prettyPhoto[carousel-'.$post_id.']">' : '<a href="'.get_permalink($post_id).'">' );
							$output .= '<img src="'.$imageurl2.'" alt="'.apply_filters( 'mpt_image_carousel_prettyphoto_caption' , get_the_title( $post_id ) , $post_id , 'image-3' ).'" />';
						$output .= '</a>';
					$output .= '</li>';
				}

			$output .= '</ul>';
		$output .= '</div>';

		$output .= '<script type="text/javascript">jQuery(document).ready(function () {
						jQuery(".flexslider").flexslider({
							namespace: "flex-",
							selector: ".slides > li",
							animation: "fade",
							slideshow: '.( $content_type == 'single' ? 'true' : 'false' ).',
							animationLoop: false,
							smoothHeight: true,
							'. ( $masonry ? 'after: function () {
									jQuery("#pro-wp-posts").masonry("reloadItems").masonry("layout");
								}' : '' ) . '
						});
					});</script>';
		
	  	if ($echo)
	    	echo apply_filters( 'func_mpt_load_image_carousel' , $output , $instance );
	  	else
	    	return apply_filters( 'func_mpt_load_image_carousel' , $output , $instance );
	}

	function get_image_id($image_url) {
		global $wpdb;
		$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_url'";
		$id = $wpdb->get_var($query);
		return $id;
	}

	function mpt_load_video_post( $args = array() ) {

	  	$defaults = array(
			'echo' => false,
			'post_id' => NULL,
			'content_type' => 'single',
			'imagesize' => 'full',
			'prettyphoto' => false,
			'videoheight' => 195,
			'btnclass' => '',
			'iconclass' => '',
		);

	  	$instance = wp_parse_args( $args, $defaults );
	  	extract( $instance );

	  	global $id;
	  	$post_id = ( NULL === $post_id ) ? $id : $post_id;
		
		$video = get_post_meta( $post_id, '_mpt_post_video_url', true );
		$videourl = esc_url($video);
		$videotype = get_post_meta( $post_id, '_mpt_post_video_type', true );
		$videocode = '';

		if (!empty($videoheight)) {
			$videoheight = ' height="'.$videoheight.'"';
		}

		switch ($videotype) {
			case 'youtube':
				$youtube = array(
					"http://youtu.be/",
					"http://www.youtube.com/watch?v=",
					"http://www.youtube.com/embed/",
					"https://youtu.be/",
					"https://www.youtube.com/watch?v=",
					"https://www.youtube.com/embed/"
					);
				$videonum = str_replace($youtube, "", $videourl);
				$videocode = ( is_ssl() ? 'https' : 'http' ) . '://www.youtube.com/embed/' . $videonum;
				break;
			case 'vimeo':
				$vimeo = array(
					"http://vimeo.com/",
					"http://player.vimeo.com/video/",
					"https://vimeo.com/",
					"https://player.vimeo.com/video/",
					);
				$videonum = str_replace($vimeo, "", $videourl);
				$videocode = ( is_ssl() ? 'https' : 'http' ) . '://player.vimeo.com/video/' . $videonum;
				break;
		}

		$output = '<div class="video-box featured-video">';
			$output .= '<iframe src="'.$videocode.'?title=1&amp;byline=1&amp;portrait=1" width="100%"'.$videoheight.' frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		$output .= '</div>';
		
	  	if ($echo)
	    	echo apply_filters( 'func_mpt_load_video_post' , $output , $instance );
	  	else
	    	return apply_filters( 'func_mpt_load_video_post' , $output , $instance );
	}


/**
 * MarketPress Settings Functions.
 *
 */

	function mpt_load_mp_btn_color() {
		//$selected = get_option('mpt_mp_main_btn_color');
		$selected = get_blog_option(1,'mpt_mp_main_btn_color');

		switch ($selected) {
			case 'Grey':
				$class = '';
				break;
			case 'Blue':
				$class = ' btn-primary';
				break;
			case 'Light Blue':
				$class = ' btn-info';
				break;
			case 'Green':
				$class = ' btn-success';
				break;
			case 'Yellow':
				$class = ' btn-warning';
				break;
			case 'Red':
				$class = ' btn-danger';
				break;
			case 'Black':
				$class = ' btn-inverse';
				break;
			
			default:
				$class = ' btn-inverse';
				break;
		}

		return apply_filters( 'func_mpt_load_mp_btn_color' , $class );
	}

	function mpt_load_whiteicon_in_btn() {
		//$selected = get_option('mpt_mp_main_btn_color');
		$selected = get_blog_option(1,'mpt_mp_main_btn_color');
		switch ($selected) {
			case 'Grey':
				$class = '';
				break;
			case 'Blue':
				$class = ' icon-white';
				break;
			case 'Light Blue':
				$class = ' icon-white';
				break;
			case 'Green':
				$class = ' icon-white';
				break;
			case 'Yellow':
				$class = ' icon-white';
				break;
			case 'Red':
				$class = ' icon-white';
				break;
			case 'Black':
				$class = ' icon-white';
				break;
			
			default:
				$class = ' icon-white';
				break;
		}

		return apply_filters( 'func_mpt_load_whiteicon_in_btn' , $class );
	}

	function mpt_load_icontag_color() {
		//$selected = get_option('mpt_mp_main_icon_tag_color');
		$selected = get_blog_option(1,'mpt_mp_main_icon_tag_color');
		switch ($selected) {
			case 'White':
				$class = ' icon-white';
				break;
			case 'Blue':
				$class = ' icon-blue';
				break;
			case 'Light Blue':
				$class = ' icon-lightblue';
				break;
			case 'Green':
				$class = ' icon-green';
				break;
			case 'Yellow':
				$class = ' icon-yellow';
				break;
			case 'Red':
				$class = ' icon-red';
				break;
			case 'Black':
				$class = '';
				break;
			
			default:
				$class = '';
				break;
		}

		return apply_filters( 'func_mpt_load_icontag_color' , $class );
	}

	function mpt_load_product_listing_layout() {
		//$selected = get_option('mpt_mp_listing_layout');
		$selected = get_blog_option(1,'mpt_mp_listing_layout');
		switch ($selected) {
			case '2 Columns':
				return apply_filters( 'func_mpt_load_product_listing_layout' , 'span6' );
				break;
			case '3 Columns':
				return apply_filters( 'func_mpt_load_product_listing_layout' , 'span4' );
				break;
			case '4 Columns':
				return apply_filters( 'func_mpt_load_product_listing_layout' , 'span3' );
				break;
			
			default:
				return apply_filters( 'func_mpt_load_product_listing_layout' , 'span4' );
				break;
		}

	}

	function mpt_load_product_listing_counter() {
		//$selected = get_option('mpt_mp_listing_layout');
		$selected = get_blog_option(1,'mpt_mp_listing_layout');
		switch ($selected) {
			case '2 Columns':
				return apply_filters( 'func_mpt_load_product_listing_counter' , '2' );
				break;
			case '3 Columns':
				return apply_filters( 'func_mpt_load_product_listing_counter' , '3' );
				break;
			case '4 Columns':
				return apply_filters( 'func_mpt_load_product_listing_counter' , '4' );
				break;
			
			default:
				return apply_filters( 'func_mpt_load_product_listing_counter' , '3' );
				break;
		}
	}
	//Jeff
	function mpt_enable_advanced_sort() {

		//if (get_option('mpt_enable_advanced_sort') == 'true') 
		//if (get_blog_option(1,'mpt_enable_advanced_sort') == 'true') 
			return apply_filters( 'func_mpt_enable_advanced_sort' , true );
		//else 
		//	return apply_filters( 'func_mpt_enable_advanced_sort' , false );
	}

	function mpt_advanced_sort_btn_position() {
		//$selected = get_option('mpt_advanced_soft_btn_position');
		//$selected = get_blog_option(1,'mpt_advanced_soft_btn_position');
		$selected = 'Right';
		switch ($selected) {
			case 'Left':
				return apply_filters( 'func_mpt_advanced_sort_btn_position' , 'align-left' );
				break;
			case 'Center':
				return apply_filters( 'func_mpt_advanced_sort_btn_position' , 'align-center' );
				break;
			case 'Right':
				return apply_filters( 'func_mpt_advanced_sort_btn_position' , 'align-right' );
				break;
			
			default:
				return apply_filters( 'func_mpt_advanced_sort_btn_position' , 'align-right' );
				break;
		}
	}

	function mpt_load_mp_label_color() {
		//$selected = get_option('mpt_mp_main_label_color');
		$selected = get_blog_option(1,'mpt_mp_main_label_color');
		switch ($selected) {
			case 'Grey':
				$class = '';
				break;
			case 'Blue':
				$class = ' label-info';
				break;
			case 'Green':
				$class = ' label-success';
				break;
			case 'Yellow':
				$class = ' label-warning';
				break;
			case 'Red':
				$class = ' label-important';
				break;
			case 'Black':
				$class = ' label-inverse';
				break;
			
			default:
				$class = ' label-inverse';
				break;
		}

		return apply_filters( 'func_mpt_load_mp_label_color' , $class );
	}

	function pro_load_floating_cart_text_items( $echo = true ) {

		//$text = esc_attr(get_option('mpt_floating_cart_text'));
		$text = esc_attr(get_blog_option(1,'mpt_floating_cart_text'));
		$output = '';

		//if ( get_option('mpt_floating_cart_show_cart_item') == 'true' )
		if ( get_blog_option(1,'mpt_floating_cart_show_cart_item') == 'true' )
			$output .= '<span class="cart-total-items">'.mp_items_count_in_cart().'</span>'.__( ' item(s)' , 'pro' );

		//if ( get_option('mpt_floating_cart_show_cart_item') == 'true' && get_option('mpt_floating_cart_show_cart_amount') == 'true' )
		if ( get_blog_option(1,'mpt_floating_cart_show_cart_item') == 'true' && get_blog_option(1,'mpt_floating_cart_show_cart_amount') == 'true' )
			$output .= ' - ';

		//if ( get_option('mpt_floating_cart_show_cart_amount') == 'true')
		if ( get_blog_option(1,'mpt_floating_cart_show_cart_amount') == 'true')
			$output .= '<span class="cart-total-amount">'.pro_total_amount_in_cart( false ).'</span>';
		
		//if ( get_option('mpt_floating_cart_show_button_text') != 'false' )
		if ( get_blog_option(1,'mpt_floating_cart_show_button_text') != 'false' )
			$output .= ( !empty($text) ? '<span class="view-cart"> - '.__( $text, 'pro' ).'</span>' : '' );

		if ($echo) {
		    echo apply_filters( 'func_pro_load_floating_cart_text_items' , $output );
		} else {
		    return apply_filters( 'func_pro_load_floating_cart_text_items' , $output );
		}


	}


	function pro_total_amount_in_cart( $echo = true ) {

	  	global $mp, $blog_id;
	  	$blog_id = (is_multisite()) ? $blog_id : 1;
	  	$current_blog_id = $blog_id;

		$global_cart = $mp->get_cart_contents(true);

	  	if (!$mp->global_cart)  //get subset if needed
	  		$selected_cart[$blog_id] = $global_cart[$blog_id];
	  	else
	    	$selected_cart = $global_cart;

	    $totals = array();

	    if (!empty($selected_cart) && is_array($selected_cart)) {
		    foreach ($selected_cart as $bid => $cart) {

			if (is_multisite())
		        switch_to_blog($bid);

		    if (!empty($cart) && is_array($cart)){
			   	foreach ($cart as $product_id => $variations) {
			        foreach ($variations as $variation => $data) {
			          $totals[] = $data['price'] * $data['quantity'];
			        }
			      }
			    }
		    }
	    } 
	    
		if (is_multisite())
	      switch_to_blog($current_blog_id);

	    $total = array_sum($totals);

		if ($echo) {
		    echo apply_filters( 'func_pro_total_amount_in_cart' , $mp->format_currency('', $total) );
		} else {
		    return apply_filters( 'func_pro_total_amount_in_cart' , $mp->format_currency('', $total) );
		}
	}

	function mpt_load_selected_sidebar( $context = 'post' ) {

		if ( $context == 'product' ) {
			//$selected = get_option('mpt_pro_page_select_sidebar');
			$selected = get_blog_option(1,'mpt_pro_page_select_sidebar');
			switch ($selected) {
				case 'Sidebar 2':
					$sidebar = '2';
					break;

				case 'Sidebar 3':
					$sidebar = '3';
					break;

				case 'Sidebar 4':
					$sidebar = '4';
					break;
				
				case 'Sidebar 1':
				default:
					$sidebar = '';
					break;
			}
		} elseif ( $context == 'page' ) {
			//$selected = get_option('mpt_wp_page_select_sidebar');
			$selected = get_blog_option(1,'mpt_wp_page_select_sidebar');
			switch ($selected) {
				case 'Sidebar 2':
					$sidebar = '2';
					break;

				case 'Sidebar 3':
					$sidebar = '3';
					break;

				case 'Sidebar 4':
					$sidebar = '4';
					break;
				
				case 'Sidebar 1':
				default:
					$sidebar = '';
					break;
			}
		} elseif ( $context == 'post' ) {
			//$selected = get_option('mpt_wp_post_select_sidebar');
			$selected = get_blog_option(1,'mpt_wp_post_select_sidebar');
			switch ($selected) {
				case 'Sidebar 2':
					$sidebar = '2';
					break;

				case 'Sidebar 3':
					$sidebar = '3';
					break;

				case 'Sidebar 4':
					$sidebar = '4';
					break;
				
				case 'Sidebar 1':
				default:
					$sidebar = '';
					break;
			}
		} elseif ( $context == 'taxonomypage' ) {
			//$selected = get_option('mpt_taxonomy_page_select_sidebar');
			$selected = get_blog_option(1,'mpt_taxonomy_page_select_sidebar');
			switch ($selected) {
				case 'Sidebar 2':
					$sidebar = '2';
					break;

				case 'Sidebar 3':
					$sidebar = '3';
					break;

				case 'Sidebar 4':
					$sidebar = '4';
					break;
				
				case 'Sidebar 1':
				default:
					$sidebar = '';
					break;
			}
		} elseif ( $context == 'fepage' ) {
			//$selected = get_option('mpt_fe_page_select_sidebar');
			$selected = get_blog_option(1,'mpt_fe_page_select_sidebar');
			switch ($selected) {
				case 'Sidebar 2':
					$sidebar = '2';
					break;

				case 'Sidebar 3':
					$sidebar = '3';
					break;

				case 'Sidebar 4':
					$sidebar = '4';
					break;
				
				case 'Sidebar 1':
				default:
					$sidebar = '';
					break;
			}
		} else {
			$sidebar = '';
		}

		return apply_filters( 'func_mpt_load_selected_sidebar' , $sidebar , $context );
	}

	/* Get Option Functions
	------------------------------------------------------------------------------------------------------------------- */

	function mpt_get_post_meta( $post_id = NULL , $option = '' , $default = '' , $esc = false , $single = true ) {

		if ( empty($option) || is_null($post_id) )
			return;

		if ( $esc )
			$value = esc_attr( get_post_meta( $post_id , $option , $single ) );
		else
			$value = get_post_meta( $post_id , $option , $single );

		$value = ( !empty($value) ? $value : $default );

		return apply_filters( 'func_mpt_get_post_meta' , $value , $post_id , $option , $default , $esc , $single );
	}

	function mpt_get_option( $option = '' , $default = '' , $esc = false ) {

		if ( empty($option) )
			return;

		if ( $esc )
			$value = esc_attr( get_blog_option(1, $option ) );
		else
			$value = get_blog_option(1, $option );

		$value = mpt_option_switcher( $value );
		$value = ( !empty($value) ? $value : $default );

		return apply_filters( 'func_mpt_get_option' , $value , $option , $default , $esc );
	}

	function mpt_option_switcher( $value = '' ) {

		switch ( $value ) {
			case '1 Columns': $new_value = '1col'; break;
			case '2 Columns': $new_value = '2col'; break;
			case '3 Columns': $new_value = '3col'; break;
			case '4 Columns': $new_value = '4col'; break;
			case 'Yes': $new_value = 'yes'; break;
			case 'No': $new_value = 'no'; break;
			case 'Style 1': $new_value = 'style-1'; break;
			case 'Style 2': $new_value = 'style-2'; break;
			case 'Black': $new_value = 'black'; break;
			case 'Grey': $new_value = 'grey'; break;
			case 'Blue': $new_value = 'blue'; break;
			case 'Yellow': $new_value = 'yellow'; break;
			case 'Red': $new_value = 'red'; break;
			case 'Light Blue': $new_value = 'lightblue'; break;
			case 'Green': $new_value = 'green'; break;
			case 'Left': $new_value = 'left'; break;
			case 'Right': $new_value = 'right'; break;
			case 'Sidebar 1': $new_value = 'sidebar-1'; break;
			case 'Sidebar 2': $new_value = 'sidebar-2'; break;
			case 'Sidebar 3': $new_value = 'sidebar-3'; break;
			case 'Sidebar 4': $new_value = 'sidebar-4'; break;
			case 'By Categories': $new_value = 'by-categories'; break;
			case 'By Tags': $new_value = 'by-tags'; break;
			case 'By IDs': $new_value = 'by-ids'; break;
			//case 'value123': $new_value = 'value123'; break;

			default: $new_value = apply_filters( 'mpt_option_switcher_default_hook' , $value ); break;
		}

		return apply_filters( 'func_mpt_option_switcher' , $new_value , $value );
	}