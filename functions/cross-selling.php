<?php

	add_action('wp_enqueue_scripts', 'pro_register_cs_js');
	add_filter( 'pro_buy_button_tag' , 'pro_cross_selling_buy_button' , 10 , 2);
	add_filter( 'cmb_meta_boxes', 'pro_cross_selling_metaboxes' );

    add_action( 'wp_ajax_nopriv_pro-update-cart', 'pro_update_cart' );
    add_action( 'wp_ajax_pro-update-cart', 'pro_update_cart' );

	function pro_register_cs_js(){
		wp_enqueue_script('pro-cs-ajax-js', get_template_directory_uri() . '/js/pro-cs-ajax.js', array('jquery'));
		wp_localize_script( 'pro-cs-ajax-js', 'PRO_CS_Ajax', array( 
			'ajaxUrl' => admin_url( 'admin-ajax.php', (is_ssl() ? 'https': 'http') ),
			'addingMPMsg' => __('Adding to your cart...', 'pro'),
			'successMPMsg' => __('Item(s) Added!', 'pro'), 
			'outMPMsg' => __('In Your Cart.', 'pro'),
			'emptyCartMsg' => __('Are you sure you want to remove all items from your cart?', 'pro'),
			'imgUrl' => get_template_directory_uri() . '/img/loading.gif'
			));
	}

	function pro_cross_selling_buy_button( $content = '' , $args = array() ) {

		$defaults = array(
			'echo' => true,
			'post_id' => NULL,
			'btnclass' => '',
			'context' => 'list'
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );		

	  	global $id, $mp;
	  	$post_id = ( NULL === $post_id ) ? $id : $post_id;

	  	$meta = get_post_custom($post_id);
	  	//unserialize
	  	foreach ($meta as $key => $val) {
		  	$meta[$key] = maybe_unserialize($val[0]);
		  	if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file")
		    	$meta[$key] = array($meta[$key]);
		}

  		//check stock
	  	$no_inventory = array();
	  	$all_out = false;
	  	if ($meta['mp_track_inventory']) {

	    	$cart = $mp->get_cart_contents();
	    	if (isset($cart[$post_id]) && is_array($cart[$post_id])) {

		    	foreach ($cart[$post_id] as $variation => $data) {
			      if ($meta['mp_inventory'][$variation] <= $data['quantity'])
			        $no_inventory[] = $variation;
				}

				foreach ($meta['mp_inventory'] as $key => $stock) {
			      if (!in_array($key, $no_inventory) && $stock <= 0)
			        $no_inventory[] = $key;
				}
			}

			//find out of stock items that aren't in the cart
			foreach ($meta['mp_inventory'] as $key => $stock) {
	      	if (!in_array($key, $no_inventory) && $stock <= 0)
	        	$no_inventory[] = $key;
			}

			if (count($no_inventory) >= count($meta["mp_price"]))
		  		$all_out = true;
  		}

	  	//display an external link or form button
	  	if (isset($meta['mp_product_link']) && $product_link = $meta['mp_product_link']) {

	    	$button = '<a class="mp_link_buynow btn'.$btnclass.'" href="' . esc_url($product_link) . '">' . __('Buy Now', 'pro') . '</a>';

	  	} else if ($mp->get_setting('disable_cart')) {
	    
	    $button = '';
	    
	  	} else {

		    $variation_select = '';
		    $button = '<form class="pro_buy_form" method="post" action="' . mp_cart_link(false, true) . '">';

		    if ($all_out) {
		      $button .= '<span class="mp_no_stock btn disabled'.$btnclass.'">' . __('Out of Stock', 'pro') . '</span>';
		    } else {

			    $button .= '<input type="hidden" name="product_id" value="' . $post_id . '" />';

					//create select list if more than one variation
				    if (is_array($meta["mp_price"]) && count($meta["mp_price"]) > 1 && empty($meta["mp_file"])) {

				    	$variation_select = '<div class="input-prepend">';

				    	$variation_select .= '<span class="add-on">'.__('Choose', 'pro').'</span>';

			      		$variation_select .= '<select class="mp_product_variations" name="variation">';

						foreach ($meta["mp_price"] as $key => $value) {

						  	$disabled = (in_array($key, $no_inventory)) ? ' disabled="disabled"' : '';
						  	$variation_select .= '<option value="' . $key . '"' . $disabled . '>' . esc_html($meta["mp_var_name"][$key]) . ' - ';
							
						  	if ($meta["mp_is_sale"] && $meta["mp_sale_price"][$key]) {

				        		$variation_select .= $mp->format_currency('', $meta["mp_sale_price"][$key]);
				     	 	
				     	 	} else {

				        		$variation_select .= $mp->format_currency('', $value);

				      		}

				      		$variation_select .= "</option>\n";
				    	
				    	}

			      		$variation_select .= "</select>&nbsp;\n";

			      		$variation_select .= '</div>';

			 		} else {

				      	$button .= '<input type="hidden" name="variation" value="0" />';
					}

			    if ($context == 'list') {
			      	if ($variation_select) {
		        		$button .= '<a class="mp_link_buynow btn'.$btnclass.'" href="' . get_permalink($post_id) . '">' . __('Choose Option', 'pro') . '</a>';
			      	} else if ($mp->get_setting('list_button_type') == 'addcart') {
			        	$button .= '<input type="hidden" name="action" value="pro-update-cart" />';
			        	$button .= '<input class="pro_button_addcart btn'.$btnclass.'" type="submit" name="addcart" value="' . __('Add To Cart', 'pro') . '" />';
			      	} else if ($mp->get_setting('list_button_type') == 'buynow') {
			        	$button .= '<input class="mp_button_buynow btn'.$btnclass.'" type="submit" name="buynow" value="' . __('Buy Now', 'pro') . '" />';
			      	}
			    } else {

			      	$button .= $variation_select;

			      	$button .= '<div class="">';

				      	//add quantity field if not downloadable
				      	if ($mp->get_setting('show_quantity') && empty($meta["mp_file"])) {
					      	$button .= '<div class="input-prepend">';
					        $button .= '<span class="mp_quantity add-on">' . __('Quantity', 'pro') . '</span>
					        			<input class="mp_quantity_field input-mini" type="text" name="quantity" value="1" />';
					       	$button .= '</div>';
				      	}

				      	if ($mp->get_setting('product_button_type') == 'addcart') {
					        $button .= '<input type="hidden" name="action" value="pro-update-cart" />';
					        $button .= '<input class="pro_button_addcart btn'.$btnclass.'" type="submit" name="addcart" value="' . __('Add To Cart', 'pro') . '" />';
				      	} else if ($mp->get_setting('product_button_type') == 'buynow') {
				        	$button .= '<input class="mp_button_buynow btn'.$btnclass.'" type="submit" name="buynow" value="' . __('Buy Now', 'pro') . '" />';
				      	}

				    $button .= '</div>';
				}			  
		    }

	    	$button .= '</form>';
	  	}

	  	//cross selling modal

		$cs_modal_output = '<div id="cross-selling-modal-'.$post_id.'" class="modal cross-selling-modal hide fade" tabindex="-1" role="dialog" aria-labelledby="crosssellingmodal" aria-hidden="true">';
			$cs_modal_output .= '<div class="modal-header">';
				$cs_modal_output .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
				$cs_modal_output .= '<h3 class="align-left text-success">'.__( 'Item(s) Added To Cart' , 'pro' ).'</h3>';
					$cs_modal_output .= '<div class="clear padding10"></div>';
					$cs_modal_output .= '<div class="row-fluid">';
						$cs_modal_output .= '<div class="span7">';
							$cs_modal_output .= '<div class="cs-cart-info align-left"></div>';
						$cs_modal_output .= '</div>'; // End Span
						$cs_modal_output .= '<div class="span5">';
							$cs_modal_output .= '<a href="'.mp_cart_link(false, true).'"><button class="btn btn-success pull-right">'.__( 'Proceed to Checkout &raquo;' , 'pro').'</button></a>';
							$cs_modal_output .= '<div class="clear"></div>';
						$cs_modal_output .= '</div>'; // End Span
					$cs_modal_output .= '</div>';  // End row-fluid
			$cs_modal_output .= '</div>'; //End modal-header

			$cs_modal_output .= pro_load_cross_selling_products( false , $post_id );

			$cs_modal_output .= '<div class="modal-footer">';
				$cs_modal_output .= '<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">'.__( 'Close and Continue Shopping &raquo;', 'pro' ).'</button>';
			$cs_modal_output .= '</div>';

		$cs_modal_output .= '</div>';

		$button .= apply_filters( 'pro_cross_selling_modal' , $cs_modal_output , $instance );

		$button = apply_filters( 'pro_cross_selling_buy_button_tag', $button, $instance );

		return apply_filters( 'func_pro_cross_selling_buy_button' , $button , $instance );
	}

	function pro_load_cross_selling_products( $echo = true , $post_id = NULL ){

		$csproduct = array();

		$enablecs =  esc_attr(get_post_meta( $post_id, '_mpt_enable_cross_selling', true ));

		for ($i=1; $i < 5 ; $i++) { 
			$csproduct[$i] = esc_attr(get_post_meta( $post_id, '_mpt_cs_product_'.$i, true ));
			$csproduct[$i] = ( $csproduct[$i] == $post_id ? 'empty' : $csproduct[$i] );
		}

		$product_output = '';
		$count = 0;

		if ( $enablecs == 'yes' ) {

			for ($i=1; $i < 5; $i++) { 
				
				if ($csproduct[$i] != 'empty') {

					$product_output .= '<div class="well-box">';
						$product_output .= '<div class="row-fluid">';
							$product_output .= '<div class="span4">';
								$product_output .= '<div class="cs-product-image align-center">'.mp_product_image( false, 'widget', $csproduct[$i] , 150 ).'</div>';
							$product_output .= '</div>'; // End span
							$product_output .= '<div class="span8 align-left">';
								$product_output .= '<h4><a href="'.get_permalink($csproduct[$i]).'">'.get_the_title($csproduct[$i]).'</a></h4>';
								$product_output .= '<p>'.pro_product_price( array( 'echo' => false , 'post_id' => $csproduct[$i] , 'label' => false , 'context' => 'widget' ) ).'</p>';
								$post = get_post($csproduct[$i] , ARRAY_A);
								$product_output .= '<p>' . (!empty($post['post_excerpt']) ? $post['post_excerpt'] : wp_trim_words($post['post_content'] , 15) ) .'</p>';
								$product_output .= '<div class="cs-buy-button">'.mp_buy_button(false, 'list', $csproduct[$i]).'</div>';
							$product_output .= '</div>'; // End span						
						$product_output .= '</div>'; // End row-fluid
					$product_output .= '</div>'; // End well box
					$product_output .= '<div class="clear padding10"></div>';
				} else {
					$count++;
				}
			} 

		}

		if ($count == 4 || $enablecs != 'yes' ) {
			$output = '';
			$output = apply_filters('pro_cross_selling_before_listing' , $output , $post_id);
			$output = apply_filters('pro_cross_selling_after_listing' , $output , $post_id);
		} else {
			$output = '<div class="modal-body">';
				$output = apply_filters('pro_cross_selling_before_listing' , $output , $post_id);
				$output .= apply_filters( 'pro_cross_selling_modal_body_title' , '<h4 class="cs-title">'.__( 'Customers Who Bought' , 'pro').' <span class="text-success">'.get_the_title($post_id).'</span> '.__( 'Also Bought' , 'pro').':</h4>' , $post_id );
				$output .= $product_output;
				$output = apply_filters('pro_cross_selling_after_listing' , $output , $post_id);
			$output .= '</div>';
		}

  		if ($echo)
    		echo apply_filters( 'func_pro_load_cross_selling_products' , $output , $post_id );
  		else
    		return apply_filters( 'func_pro_load_cross_selling_products' , $output , $post_id );
	}

	function pro_cross_selling_metaboxes( array $meta_boxes ) {

		$prefix = '_mpt_';

		$inherit_meta_boxes = $meta_boxes;

		$productlisting = array( array( 'name' => 'Select a Product', 'value' => 'empty' ) );
		$products = get_posts( array('post_type' => 'product' , 'orderby' => 'title', 'order' => 'ASC' , 'numberposts' => -1) );

		if (!empty($products) && is_array($products)) {
			foreach ($products as $product) {
				$productlisting[] = array( 
					'name' => $product->post_title, 
					'value' => $product->ID
					);
			}
		}

		$meta_boxes[] = array(
			'id'         => 'cross_selling_metabox',
			'title'      => 'Cross Selling',
			'pages'      => array('product'), // Post type
			'context'    => 'normal',
			'priority'   => 'core',
			'show_names' => true, // Show field names on the left
			'fields'     => array(
				array(
					'name'    => 'Enable Cross Selling',
					'desc'    => '',
					'id'      => $prefix . 'enable_cross_selling',
					'type'    => 'select',
					'options' => array(
						array( 'name' => 'No', 'value' => 'no', ),
						array( 'name' => 'Yes', 'value' => 'yes', )
					),
				),
				array(
					'name'    => 'First Cross Selling Product',
					'desc'    => '',
					'id'      => $prefix . 'cs_product_1',
					'type'    => 'select',
					'options' => $productlisting,
				),
				array(
					'name'    => 'Second Cross Selling Product',
					'desc'    => '',
					'id'      => $prefix . 'cs_product_2',
					'type'    => 'select',
					'options' => $productlisting,
				),
				array(
					'name'    => 'Third Cross Selling Product',
					'desc'    => '',
					'id'      => $prefix . 'cs_product_3',
					'type'    => 'select',
					'options' => $productlisting,
				),
				array(
					'name'    => 'Fourth Cross Selling Product',
					'desc'    => '',
					'id'      => $prefix . 'cs_product_4',
					'type'    => 'select',
					'options' => $productlisting,
				),
			),
		);

		return apply_filters( 'func_pro_cross_selling_metaboxes' , $meta_boxes , $inherit_meta_boxes );	
	}

	function pro_update_cart(){

		global $mp;
		global $blog_id, $mp_gateway_active_plugins;
		$blog_id = (is_multisite()) ? $blog_id : 1;
		$current_blog_id = $blog_id;

    	$cart = $mp->get_cart_cookie();

		if (isset($_POST['product_id'])) { //add a product to cart

			//if not valid product_id return
	      	$product_id = apply_filters('mp_product_id_add_to_cart', intval($_POST['product_id']));
	      	$product = get_post($product_id);
	      	if (!$product || $product->post_type != 'product' || $product->post_status != 'publish')
	        	return false;

			//get quantity
	      	$quantity = (isset($_POST['quantity'])) ? intval(abs($_POST['quantity'])) : 1;

	      	//get variation
	      	$variation = (isset($_POST['variation'])) ? intval(abs($_POST['variation'])) : 0;

	      	//check max stores
	      	if ($mp->global_cart && count($global_cart = $mp->get_cart_cookie(true)) >= $mp_gateway_active_plugins[0]->max_stores && !isset($global_cart[$blog_id])) {
	        	if (defined('DOING_AJAX') && DOING_AJAX) {
		  			echo 'error||' . sprintf(__("Sorry, currently it's not possible to checkout with items from more than %s stores.", 'mp'), $mp_gateway_active_plugins[0]->max_stores);
	          		exit;
	        	} else {
	          		$mp->cart_checkout_error(sprintf(__("Sorry, currently it's not possible to checkout with items from more than %s stores.", 'mp'), $mp_gateway_active_plugins[0]->max_stores));
	          	return false;
	      		}
	    	}

	      	//calculate new quantity
	      	$new_quantity = $cart[$product_id][$variation] + $quantity;

	      	//check stock
	      	if (get_post_meta($product_id, 'mp_track_inventory', true)) {
	        	$stock = maybe_unserialize(get_post_meta($product_id, 'mp_inventory', true));
	        	if (!is_array($stock))
					$stock[0] = $stock;
	        	if ($stock[$variation] < $new_quantity) {
	          	if (defined('DOING_AJAX') && DOING_AJAX) {
	            	echo 'error||' . sprintf(__("Sorry, we don`t have enough of this item in stock. (%s remaining)", 'mp'), number_format_i18n($stock[$variation]-$cart[$product_id][$variation]));
	            	exit;
	          	} else {
	            	$mp->cart_checkout_error( sprintf(__("Sorry, we don`t have enough of this item in stock. (%s remaining)", 'mp'), number_format_i18n($stock[$variation]-$cart[$product_id][$variation])) );
	            	return false;
	          	}
	        }
	        	//send ajax leftover stock
	        	if (defined('DOING_AJAX') && DOING_AJAX) {
	          		$return = array_sum($stock)-$new_quantity . '||';
	        	}
	      	} else {
	        	//send ajax always stock if stock checking turned off
	        	if (defined('DOING_AJAX') && DOING_AJAX) {
	          		$return = 1 . '||';
	        	}
	      	}

	      	//check limit if tracking on or downloadable
	    	if (get_post_meta($product_id, 'mp_track_limit', true) || $file = get_post_meta($product_id, 'mp_file', true)) {

				$limit = empty($file) ? maybe_unserialize(get_post_meta($product_id, 'mp_limit', true)) : array($variation => 1);

		      	if ($limit[$variation] && $limit[$variation] < $new_quantity) {
		        	if (defined('DOING_AJAX') && DOING_AJAX) {
			  			echo 'error||' . sprintf(__('Sorry, there is a per order limit of %1$s for "%2$s".', 'mp'), number_format_i18n($limit[$variation]), $product->post_title);
		          		exit;
		        	} else {
		          		$mp->cart_checkout_error( sprintf(__('Sorry, there is a per order limit of %1$s for "%2$s".', 'mp'), number_format_i18n($limit[$variation]), $product->post_title) );
		          		return false;
		        	}
		      	}
	      	}

	      	$cart[$product_id][$variation] = $new_quantity;

	  		//save items to cookie
	  		$mp->set_cart_cookie($cart);

	  		$cartinfo = '<span class="cart-items">'.mp_items_count_in_cart().'</span>'.__( ' item(s) in your Cart' , 'pro' ).'<br /><strong>'.__( 'Order subtotal: ' , 'pro' ).'</strong><span class="cart-subtotal text-error">'.pro_total_amount_in_cart(false).'</span>';

	      	//if running via ajax return updated cart and die
	      	if (defined('DOING_AJAX') && DOING_AJAX) {
	        	$return .= mp_show_cart('widget', false, false) . '||' . $product_id . '||' . $cartinfo;
	        	echo apply_filters( 'func_pro_update_cart' , $return , $product_id , $quantity , $variation );
				exit;
	      	}
	    }
	}

	if( class_exists('MPDG') ) {

		//if ( get_option('mpt_enable_cross_selling_feature') == 'true' ) {
		if ( get_blog_option(1,'mpt_enable_cross_selling_feature') == 'true' ) {
			add_filter( 'mpdg_buy_button_tag' , 'mpdg_cs_buy_button' , 10 , 4);
		}

		function mpdg_cs_buy_button( $output = '' , $post_id = NULL , $context = 'list' , $btnclass = '' ) {

		  	global $mp;

		  	$meta = get_post_custom($post_id);
		  	//unserialize
		  	foreach ($meta as $key => $val) {
			  	$meta[$key] = maybe_unserialize($val[0]);
			  	if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file")
			    	$meta[$key] = array($meta[$key]);
			}

		  	//check stock
		  	$no_inventory = array();
		  	$all_out = false;
		  	if ($meta['mp_track_inventory']) {
		    	$cart = $mp->get_cart_contents();
		    	if (isset($cart[$post_id]) && is_array($cart[$post_id])) {
			    	foreach ($cart[$post_id] as $variation => $data) {
			      		if ($meta['mp_inventory'][$variation] <= $data['quantity'])
			        		$no_inventory[] = $variation;
					}
					foreach ($meta['mp_inventory'] as $key => $stock) {
			      		if (!in_array($key, $no_inventory) && $stock <= 0)
			        	$no_inventory[] = $key;
					}
				}

				//find out of stock items that aren't in the cart
				foreach ($meta['mp_inventory'] as $key => $stock) {
		      		if (!in_array($key, $no_inventory) && $stock <= 0)
		        	$no_inventory[] = $key;
				}

				if (count($no_inventory) >= count($meta["mp_price"]))
				  	$all_out = true;
		  	}

		  	//display an external link or form button
		  	if (isset($meta['mp_product_link']) && $product_link = $meta['mp_product_link']) {

		    	$button = '<a class="mp_link_buynow btn'.$btnclass.'" href="' . esc_url($product_link) . '">' . __('Buy Now', 'mpdg') . '</a>';

		  	} else if ($mp->get_setting('disable_cart')) {
		    
		    	$button = '';
		    
		  	} else {
		    	$variation_select = '';
		    	$button = '<form class="pro_buy_form'.($context == 'list' ? '' : ' form-inline').'" method="post" action="' . mp_cart_link(false, true) . '">';

			    if ($all_out) {
			      	$button .= '<span class="mp_no_stock btn disabled'.$btnclass.'">' . __('Out of Stock', 'mpdg') . '</span>';
			    } else {

				    $button .= '<input type="hidden" name="product_id" value="' . $post_id . '" />';

						//create select list if more than one variation
					if (is_array($meta["mp_price"]) && count($meta["mp_price"]) > 1 && empty($meta["mp_file"])) {
				      	$variation_select = '<select class="mp_product_variations" name="variation">';
							foreach ($meta["mp_price"] as $key => $value) {
							  	$disabled = (in_array($key, $no_inventory)) ? ' disabled="disabled"' : '';
							  	$variation_select .= '<option value="' . $key . '"' . $disabled . '>' . esc_html($meta["mp_var_name"][$key]) . ' - ';
								if ($meta["mp_is_sale"] && $meta["mp_sale_price"][$key]) {
					        		$variation_select .= $mp->format_currency('', $meta["mp_sale_price"][$key]);
					      		} else {
					        		$variation_select .= $mp->format_currency('', $value);
					      		}
					      			$variation_select .= "</option>\n";
					    	}
				      	$variation_select .= "</select>&nbsp;\n";
				 	} else {
				      	$button .= '<input type="hidden" name="variation" value="0" />';
					}

				    if ($context == 'list') {
				      	if ($variation_select) {
			        		$button .= '<a class="mp_link_buynow btn'.$btnclass.'" href="' . get_permalink($post_id) . '">' . __('Choose Option', 'mpdg') . '</a>';
				      	} else if ($mp->get_setting('list_button_type') == 'addcart') {
				        	$button .= '<input type="hidden" name="action" value="pro-update-cart" />';
				        	$button .= '<input class="pro_button_addcart btn'.$btnclass.'" type="submit" name="addcart" value="' . __('Add To Cart', 'mpdg') . '" />';
				      	} else if ($mp->get_setting('list_button_type') == 'buynow') {
				        	$button .= '<input class="mp_button_buynow btn'.$btnclass.'" type="submit" name="buynow" value="' . __('Buy Now', 'mpdg') . '" />';
				      	}
				    } else {

				      	$button .= $variation_select;

				      	//add quantity field if not downloadable
				      	if ($mp->get_setting('show_quantity') && empty($meta["mp_file"])) {
				        	$button .= '<span class="mp_quantity"><label>' . __('Quantity:', 'mpdg') . ' <input class="mp_quantity_field input-mini" type="text" name="quantity" value="1" /></label></span>&nbsp;';
				      	}

				      	if ($mp->get_setting('product_button_type') == 'addcart') {
				        	$button .= '<input type="hidden" name="action" value="pro-update-cart" />';
				        	$button .= '<input class="pro_button_addcart btn'.$btnclass.'" type="submit" name="addcart" value="' . __('Add To Cart', 'mpdg') . '" />';
				      	} else if ($mp->get_setting('product_button_type') == 'buynow') {
				        	$button .= '<input class="mp_button_buynow btn'.$btnclass.'" type="submit" name="buynow" value="' . __('Buy Now', 'mpdg') . '" />';
				      	}
				    }
			    }
		    	$button .= '</form>';
			}

			$button = apply_filters( 'mpdg_cross_selling_buy_button_tag', $button, $post_id, $context, $btnclass );

		   	return apply_filters( 'func_mpdg_cs_buy_button' , $button , $post_id, $context, $btnclass );
		}
	}

