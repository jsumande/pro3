<?php

	add_action('wp_enqueue_scripts', 'pro_register_compare_js');
	add_shortcode( 'compare', 'pro_display_compare_results_sc' );
    add_action( 'wp_ajax_nopriv_pro-update-compare', 'pro_compareproducts_submit_for_compare' );
    add_action( 'wp_ajax_pro-update-compare', 'pro_compareproducts_submit_for_compare' );
    add_action( 'wp_ajax_nopriv_pro-update-compare-results', 'pro_update_compare_results' );
    add_action( 'wp_ajax_pro-update-compare-results', 'pro_update_compare_results' );

	function pro_register_compare_js(){
		wp_enqueue_script('pro-compare-ajax-js', get_template_directory_uri() . '/js/pro-compare-ajax.js', array('jquery'));
		wp_localize_script( 'pro-compare-ajax-js', 'PRO_PC_Ajax', array( 
			'ajaxUrl' => admin_url( 'admin-ajax.php', (is_ssl() ? 'https': 'http') ),
			'addingPCMsg' => __('Adding item...', 'pro'),
			'successPCMsg' => __('Item Added', 'pro'), 
			'errorPCMsg' => __('Failed to add item.', 'pro'), 
			'loadingPCMsg' => __('Loading...', 'pro'),
			'updatedMsg' => __('List updated', 'pro'),
			'imgUrl' => get_template_directory_uri() . '/img/loading.gif'
			));
	}

	function pro_compareproducts_submit_for_compare() {

    	global $mp;
		do_action( 'mpt_start_session_hook' , 'pro_compare_submit' );

		$blog_id =  is_multisite() ? ( isset( $_POST['currentblogid'] ) ? esc_attr(intval($_POST['currentblogid'])) : 1 ) : 1;
		$post_id = isset( $_POST['pro_add_to_compare'] ) ? esc_attr(intval($_POST['pro_add_to_compare'])) : '';

		$settings = !empty($_SESSION['pro_compare_results_settings']) ? $_SESSION['pro_compare_results_settings'] : array( 'showtitle' => 'no' , 'title' => '' , 'btnclass' => '' , 'iconclass' => '' , 'class' => '' , 'style' => '' );

		$error = '<div class="clear padding10"></div>';
		$error .= '<div class="alert alert-error nomargin"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>'.__( 'Failed to add item.' , 'pro' ).'</strong>'; 
			$error .= pro_item_in_compare($post_id) ? ' '.__( 'Item already in the list.' , 'pro' ).' <a href="#" class="hidden-phone" data-toggle="modal" data-target="#compare-results-modal">'.__( 'View Compare Results' , 'pro' ).'</a>' : ( pro_max_items_in_compare() ? ' '.__( 'There are already 3 items in the list. If you want to add this to the comparison you need to remove one of the other 3 items.' , 'pro' ). ' <a href="#" class="hidden-phone" data-toggle="modal" data-target="#compare-results-modal">'.__( 'View Compare Results' , 'pro' ).'</a>' : '' );
		$error .= '</div>';

		$error = apply_filters( 'pro_compareproducts_button_error_tag', $error, $post_id );

		$success = '<div class="alert alert-success nomargin">'.__('Item Added.', 'pro').' <a href="#" class="hidden-phone" data-toggle="modal" data-target="#compare-results-modal">'.__( 'View Compare Results' , 'pro' ).'</a></div>';

		$success = apply_filters( 'pro_compareproducts_button_success_tag', $success , $post_id );

	    $notinlist = pro_item_in_compare($post_id) ? false : true;
	    $spaceinlist = pro_max_items_in_compare() ? false : true;  

	    if (!empty($post_id) && $notinlist && $spaceinlist ) {

		    if (!empty($_SESSION['pro_compare_items']) && is_array($_SESSION['pro_compare_items'])) {
		    	$_SESSION['pro_compare_items'][] = array( 'item' => $post_id , 'blog' => $blog_id);
		    } else {
		    	$_SESSION['pro_compare_items'] = array();
		    	$_SESSION['pro_compare_items'][] = array( 'item' => $post_id , 'blog' => $blog_id);
		    }

			if (defined('DOING_AJAX') && DOING_AJAX) {
				echo apply_filters ( 'func_pro_compareproducts_submit_for_compare' , 'success||' . $success . '||' . pro_compare_results( 
					array( 'echo' => false , 'showtitle' => $settings['showtitle'] , 'title' => $settings['title'] , 'btnclass' => $settings['btnclass'] , 'iconclass' => $settings['iconclass'] , 'class' => $settings['class'] , 'style' => $settings['style'] ) ) , $post_id , $blog_id , $settings );
				exit;
			}
	    	
	    } else {
			if (defined('DOING_AJAX') && DOING_AJAX) {
				echo apply_filters( 'func_pro_compareproducts_submit_for_compare' , 'error||' . $error , $post_id , $blog_id , $settings );
				exit;
			}
		} 
	}

	function pro_compareproducts_button( $args = array() ) {

		$defaults = array(
			'echo' => true,
			'post_id' => NULL,
			'btnclass' => '',
			'iconclass' => ''
		);

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		$button = '<form class="pro_compareproducts_form align-center" method="post" action="'.get_permalink($post_id).'" >';

			$button .= '<input class="btn'.$btnclass.' pro_button_addcompare" type="submit" name="addcompare" value="'.__( 'Add to Compare' , 'pro' ).'">';
			$button .= '<input type="hidden" name="pro_add_to_compare" id="pro_add_to_compare" value="'.$post_id.'" />';
			$button .= is_multisite() ? '<input type="hidden" name="currentblogid" id="currentblogid" value="'.get_current_blog_id().'" />' : '';
			$button .= '<input type="hidden" name="action" value="pro-update-compare" />';

		$button .= '</form>';

		$button .= pro_compare_results_modal( array( 'echo' => false , 'btnclass' => $btnclass , 'iconclass' => $iconclass ) );

		$button = apply_filters( 'pro_compareproducts_button_tag', $button, $post_id, $btnclass , $iconclass );

	  	if ($echo)
	    	echo apply_filters( 'func_pro_compareproducts_button' , $button , $instance );
	  	else
	    	return apply_filters( 'func_pro_compareproducts_button' , $button , $instance );
	}

	function pro_compare_results( $args = array() ) {

		$defaults = array(
			'echo' => true,
			'showtitle' => 'no',
			'title' => '',
			'btnclass' => '',
			'iconclass' => '',
			'class' => '',
			'style' => ''
		);		

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

    	global $mp;
		do_action( 'mpt_start_session_hook' , 'pro_compare_load_results' );

		$style = ( !empty($style) ? ' style="'.esc_attr($style).'"' : '' );
		$class = ( !empty($class) ? ' '.esc_attr($class) : '' );

		$session = !empty($_SESSION['pro_compare_items']) ? $_SESSION['pro_compare_items'] : '';

		$_SESSION['pro_compare_results_settings'] = array( 'showtitle' => $showtitle , 'title' => $title , 'btnclass' => $btnclass , 'iconclass' => $iconclass , 'class' => $class , 'style' => $style );

	    $output = '<form id="compare-products" class="compare-products-form" method="post" action="">';	    	

			if ($showtitle == 'yes') {
				$output .= '<h3>'.__($title, 'pro').':</h3>';
			}

		    $output .= '<table class="table table-striped table-hover table-bordered table-compare-products'.$class.'"'.$style.'>';

		    	$output .= '<thead><tr>';

		    		$output .= '<th class="align-center span3"></th>';
		    		$output .= '<th class="align-center span3">'.__('Item 1', 'pro').'</th>';
		    		$output .= '<th class="align-center span3">'.__('Item 2', 'pro').'</th>';
		    		$output .= '<th class="align-center span3">'.__('Item 3', 'pro').'</th>';

		    	$output .= '</tr></thead>';

		    	$output .= '<tbody>';

		    	if (is_array($session) && !empty($session)) {

		    		$output .= pro_compare_results_load_table_row( $session , 'basic' , '' , $btnclass );
		    		$output .= pro_compare_results_load_table_row( $session , 'excerpt' , '<strong>'.__('Description: ' , 'pro').'</strong>');
		    		//$output .= ( $mp->get_setting('disable_cart') && get_option('mpt_mp_hide_product_price') == 'true' ? '' : pro_compare_results_load_table_row( $session , 'price' , '<strong>'.__('Price: ' , 'pro').'</strong>') );
					$output .= ( $mp->get_setting('disable_cart') && get_blog_option(1,'mpt_mp_hide_product_price') == 'true' ? '' : pro_compare_results_load_table_row( $session , 'price' , '<strong>'.__('Price: ' , 'pro').'</strong>') );
		    		$output .= pro_compare_results_load_table_row( $session , 'variation' , '<strong>'.__('Variation: ' , 'pro').'</strong>');
		    		$output .= pro_compare_results_load_table_row( $session , 'category' , '<strong>'.__('Product Categories: ' , 'pro').'</strong>');
		    		$output .= pro_compare_results_load_table_row( $session , 'tag' , '<strong>'.__('Product Tags: ' , 'pro').'</strong>');
		    		$output .= pro_compare_results_load_table_row( $session , 'buynow' , '' , $btnclass );
		    		$output .= pro_compare_results_load_table_row( $session , 'remove' );

		    	} else {

		    		$output .= '<tr><td colspan="4"><span class="wishlist-empty-list">'.__( 'The list is currently empty' , 'pro').'</span></td></tr>';

		    	}

		    	$output .= '<tr><td colspan="4" class="align-right">';
		    		$output .= '<button type="submit" class="btn'.$btnclass.' update-compare-results">'.__( 'Update &raquo;' , 'pro' ).'</button>';
		    		$output .= '<input type="hidden" name="action" value="pro-update-compare-results" />';
		    	$output .= '</td></tr>';

		        $output .= '</tbody>';

		    $output .= '</table>';

	    $output .= '</form>';

	  	if ($echo)
	    	echo apply_filters( 'func_pro_compare_results' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_compare_results' , $output , $instance );
	}

	function pro_update_compare_results(){

		global $mp;
		do_action( 'mpt_start_session_hook' , 'pro_compare_update_results' );

		$session = !empty($_SESSION['pro_compare_items']) ? $_SESSION['pro_compare_items'] : '';
		$settings = !empty($_SESSION['pro_compare_results_settings']) ? $_SESSION['pro_compare_results_settings'] : array( 'showtitle' => 'no' , 'title' => '' , 'btnclass' => '' , 'iconclass' => '' , 'class' => '' , 'style' => '' );

	    if (!empty($session) && is_array($session)) {

		    foreach ($session as $key => $value) {
		    	
		    	if ( isset( $_POST['removefromcompare'.$key] ) ) {

		    		if ( $_POST['removefromcompare'.$key] == '1' ) {
		    			unset($session[$key]);
		    		}
		    	}

		    }

		    $_SESSION['pro_compare_items'] = $session = array_values($session);
	    }

      	if (defined('DOING_AJAX') && DOING_AJAX) {
        	echo apply_filters( 'func_pro_update_compare_results' , 1 . '||' . pro_compare_results( array( 'echo' => false , 'showtitle' => $settings['showtitle'] , 'title' => $settings['title'] , 'btnclass' => $settings['btnclass'] , 'iconclass' => $settings['iconclass'] , 'class' => $settings['class'] , 'style' => $settings['style'] ) ) , $session , $settings );
			exit;
      	}
	}

	function pro_compare_results_load_table_row( $session = '' , $context = '' , $frontrow = '' , $btnclass = '' ){

		$count = 0;

		$output = '<tr>';
		$output .= '<td class="align-left span3">'.$frontrow.'</td>';

		foreach ($session as $key => $value) {
            if (!empty($value)) {

            	$blog_id = esc_html(esc_attr(trim($value['blog'])));

            	if (is_multisite() && !empty($blog_id)) {
            		switch_to_blog($blog_id);
            	}

            	$item_id = esc_html(esc_attr(trim($value['item'])));

            	if ($context == 'basic') {
	            	$output .= '<td class="align-center span3">';
	            		$output .= '<div class="compare-product-image hidden-phone">'.mp_product_image( false, 'list', $item_id , 'full' ).'</div class="compare-product-image">';
	            		$output .= '<div class="clear"></div>';
	            		$output .= '<h4><a href="'.get_permalink($item_id).'">'.get_the_title($item_id).'</a></h4>';
	            		$output .= '<a href="'.get_permalink($item_id).'" class="btn btn-block'.$btnclass.'">'.__( 'Buy Now' , 'pro' ).'</a>';
	            	$output .= '</td>';
            	}   elseif ( $context == 'price' ) {
            			$output .= '<td class="align-center span3">'.( function_exists('pro_product_price') ? pro_product_price( array( 'echo' => false , 'post_id' => $item_id , 'label' => false , 'context' => 'widget' ) ) : ' ' ).'</td>';
            	}	elseif ($context == 'remove') {
		            	$output .= '<td class="align-center span3">';
		            		$output .= '<input type="hidden" name="removefromcompare'.$key.'" value="0" /><label><input type="checkbox" name="removefromcompare'.$key.'" id="removefromcompare'.$key.'" value="1" /> '. __( 'Remove' , 'pro' ).'</label>';
		            	$output .= '</td>';
            	}	elseif ( $context == 'buynow' ) {
	            		$output .= '<td class="align-center span3">';
	            			$output .= '<a href="'.get_permalink($item_id).'" class="btn btn-block'.$btnclass.'">'.__( 'Buy Now' , 'pro' ).'</a>';
	            		$output .= '</td>';
            	}	elseif ( $context == 'excerpt' ) {
            			$post = get_post($item_id , ARRAY_A);
	            		$output .= '<td class="align-left span3">';
	            			$output .= !empty($post['post_excerpt']) ? $post['post_excerpt'] : wp_trim_words($post['post_content']);
	            		$output .= '</td>';
            	} 	elseif ($context == 'variation') {
            			global $mp;
					  	$meta = get_post_custom($item_id);
					  	if (!empty($meta)) {
						  	//unserialize
						  	foreach ($meta as $key => $val) {
							  	$meta[$key] = maybe_unserialize($val[0]);
							  	if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file")
							    	$meta[$key] = array($meta[$key]);
							}

							if (is_array($meta["mp_price"]) && count($meta["mp_price"]) > 1 && empty($meta["mp_file"])) {
			            		$output .= '<td class="align-left span3">';
			            			$output .= '<table class="product-variation table table-bordered">';
										foreach ($meta["mp_price"] as $key => $value) {
											if ($meta["mp_is_sale"] && $meta["mp_sale_price"][$key]) {
								        		$output .= '<tr><td>'.esc_html($meta["mp_var_name"][$key]) . '</td><td>' .$mp->format_currency('', $meta["mp_sale_price"][$key]).'</td></tr>';

								      		} else {
								        		$output .= '<tr><td>'.esc_html($meta["mp_var_name"][$key]) . '</td><td>' .$mp->format_currency('', $value).'</td></tr>';
								      		}
								   		}
							   		$output .= '</table>';
			            		$output .= '</td>';
							} else {
								$output .= '<td class="align-left span3"></td>';
							}
					  	}
            	} elseif ($context == 'category') {
            		$categories = get_the_terms( $item_id, 'product_category' );
            		$output .= '<td class="align-left span3">';
	            		if ( $categories && !is_wp_error( $categories ) ) {
		            		foreach ($categories as $category) {
		            			$output .= '<a href="'.get_term_link($category->slug, 'product_category').'" target="_blank">';
			            		$output .= '<span class="label label-tag">'.$category->name.'</span>';
			            		$output .= '</a> ';
		            		}
	            		}
	            	$output .= '</td>';
            	} elseif ($context == 'tag') {
            		$tags = get_the_terms( $item_id, 'product_tag' );
            		$output .= '<td class="align-left span3">';
	            		if ( $tags && !is_wp_error( $tags ) ) {
		            		foreach ($tags as $tag) {
		            			$output .= '<a href="'.get_term_link($tag->slug, 'product_tag').'" target="_blank">';
			            		$output .= '<span class="label label-tag">'.$tag->name.'</span>';
			            		$output .= '</a> ';
		            		}
	            		}
            		$output .= '</td>';
            	}

            	if (is_multisite()) {
            		restore_current_blog();
            	}
            }
            $count++;
		}

		if ($count == 1)
			$output .= '<td class="align-left span3"></td><td class="align-left span3"></td>';
		elseif ($count == 2) 
			$output .= '<td class="align-left span3"></td>';

		$output .= '</tr>';

		return apply_filters( 'func_pro_compare_results_load_table_row' , $output , $session , $context , $frontrow , $btnclass );
	}

	function pro_compare_results_modal( $args = array() ) {

		$defaults = array(
			'echo' => true,
			'btnclass' => '',
			'iconclass' => '',
			'class' => '',
			'style' => ''
		);		

		$instance = wp_parse_args( $args, $defaults );
		extract( $instance );

		$output = '<div id="compare-results-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="compareresultsmodal" aria-hidden="true">';
			$output .= '<div class="modal-header">';
				$output .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
				$output .= apply_filters( 'pro_compareproducts_modal_title' , '<h3>'.__( 'Compare Results' , 'pro' ).'</h3>' );
			$output .= '</div>';

			$output .= '<div class="modal-body">';
				$output .= '<div class="row-fluid">';
				$output = apply_filters( 'pro_compareproducts_modal_body_before_results' , $output );
				$output .= pro_compare_results( array( 'echo' => false , 'showtitle' => 'no' , 'title' => '' , 'btnclass' => $btnclass , 'iconclass' => $iconclass , 'class' => $class , 'style' => $style ) );
				$output = apply_filters( 'pro_compareproducts_modal_body_after_results' , $output );
				$output .= '</div>';
			$output .= '</div>';

			$output .= '<div class="modal-footer align-right">';
				$output .= '<button class="btn'.$btnclass.' compare-modal-close-btn" data-dismiss="modal" aria-hidden="true">'.__( 'Close', 'pro' ).'</button>';
			$output .= '</div>';

		$output .= '</div>';

	  	if ($echo)
	    	echo apply_filters( 'func_pro_compare_results_modal' , $output , $instance );
	  	else
	    	return apply_filters( 'func_pro_compare_results_modal' , $output , $instance );

	}

	function pro_display_compare_results_sc( $atts ) {

		extract( shortcode_atts( array(
			'showtitle' => 'yes',
			'title' => 'Compare Results',
			'btncolor' => 'lightblue',
			'style' => '',
			'class' => ''
		), $atts ) );

		$style = ( !empty($style) ? esc_attr($style) : '' );
		$class = ( !empty($class) ? esc_attr($class) : '' );
		$showtitle = ( !empty($showtitle) ? esc_attr($showtitle) : '' );
		$title = ( !empty($title) ? esc_attr($title) : '' );

		switch ($btncolor) {
			case 'grey':
				$btnclass = '';
				$iconclass = '';
				break;
			case 'blue':
				$btnclass = ' btn-primary';
				$iconclass = ' icon-white';
				break;
			case 'lightblue':
				$btnclass = ' btn-info';
				$iconclass = ' icon-white';
				break;
			case 'green':
				$btnclass = ' btn-success';
				$iconclass = ' icon-white';
				break;
			case 'yellow':
				$btnclass = ' btn-warning';
				$iconclass = ' icon-white';
				break;
			case 'red':
				$btnclass = ' btn-danger';
				$iconclass = ' icon-white';
				break;
			case 'black':
				$btnclass = ' btn-inverse';
				$iconclass = ' icon-white';
				break;

			default:
				$btnclass = '';
				$iconclass = '';
				break;
			
		}

		$args = array( 'echo' => false , 'showtitle' => $showtitle , 'title' => $title , 'btnclass' => $btnclass , 'iconclass' => $iconclass , 'class' => $class , 'style' => $style );

		return apply_filters( 'func_pro_display_compare_results_sc' , pro_compare_results( $args ) , $args );
	}

	function pro_item_in_compare($post_id) {

		$session = !empty($_SESSION['pro_compare_items']) ? $_SESSION['pro_compare_items'] : '';
		$blog_id = is_multisite() ? get_current_blog_id() : 1;

		if (!empty($session) && is_array($session)) {

			foreach ($session as $key => $value) {

				$item = esc_html(esc_attr(trim($value['item'])));
				$blog = esc_html(esc_attr(trim($value['blog'])));

				if ( $post_id == $item && $blog_id == $blog ) {
					return apply_filters( 'func_pro_item_in_compare' , true , $post_id , $blog_id , $session );
					exit;
				}
			}

			return apply_filters( 'func_pro_item_in_compare' , false , $post_id , $blog_id , $session );

		} else {
			return apply_filters( 'func_pro_item_in_compare' , false , $post_id , $blog_id , $session );
		}
	}

	function pro_max_items_in_compare() {

		$session = !empty($_SESSION['pro_compare_items']) ? $_SESSION['pro_compare_items'] : '';
		$count = 0;

		if (!empty($session) && is_array($session)) {

			foreach ($session as $key => $value) {
				$item = esc_html(esc_attr(trim($value['item'])));
				if (!empty($item))
					$count++;
			}

			if ($count >= 3) 
				return apply_filters( 'func_pro_max_items_in_compare' , true , $session );
			else
				return apply_filters( 'func_pro_max_items_in_compare' , false , $session );

		} else {
			return apply_filters( 'func_pro_max_items_in_compare' , false , $session );
		}
	}