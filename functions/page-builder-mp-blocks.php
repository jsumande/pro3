<?php 

/** AQ_MP_Product_Grid_Block ****************************************************************************************************/

class AQ_MP_Product_Grid_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('[MP] Product Grid','pro'),
			'size' => 'span12',
		);
		
		//create the block
		parent::__construct('aq_mp_product_grid_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'layout' => '3col',
			'entries' => '9',
			'showcategory' => 'no',
			'showpagination' => 'no',
			'align' => 'align-center',
			'order_by' => 'date', 
			'arrange' => 'DESC',
			'taxonomy_type' => 'none',
			'taxonomy' => '',
			'bgcolor' => '#fff',
			'btncolor' => 'lightblue',
			'pricelabelcolor' => 'blue',
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$layout_options = array(
			'2col' => __('2 Columns','pro'),
			'3col' => __('3 Columns','pro'),
			'4col' => __('4 Columns','pro')
		);

		$btncolor_options = array(
			'grey' => __('Grey','pro'),
			'blue' => __('Blue','pro'),
			'lightblue' => __('Light Blue','pro'),
			'green' => __('Green','pro'),
			'red' => __('Red','pro'),
			'yellow' => __('Yellow','pro'),
			'black' => __('Black','pro'),
		);

		$labelcolor_options = array(
			'grey' => __('Grey','pro'),
			'blue' => __('Blue','pro'),
			'green' => __('Green','pro'),
			'red' => __('Red','pro'),
			'yellow' => __('Yellow','pro'),
			'black' => __('Black','pro'),
		);

		$taxonomy_type_options = array(
			'none' => __('No Filter','pro'),
			'category' => __('Category','pro'),
			'tag' => __('Tag','pro'),
		);

		$showcategory_options = array(
			'yes' => __('Show Category Menu','pro'),
			'advsoft' => __('Show Advanced Sort Button','pro'),
			'no' => __('Nothing','pro'),
		);

		$showpagination_options = array(
			'yes' => __('Yes','pro'),
			'no' => __('No','pro'),
		);

		$align_options = array(
			'align-left' => __('Align Left','pro'),
			'align-center' => __('Align Center','pro'),
			'align-right' => __('Align Right','pro'),
		);

		$order_by_options = array(
			'title' => __('Product Name','pro'),
			'date' => __('Publish Date','pro'),
			'ID' => __('Product ID','pro'),
			'author' => __('Product Author','pro'),
			'sales' => __('Number of Sales','pro'),
			'price' => __('Product Price','pro'),
			'rand' => __('Random','pro'),
		);

		$order_options = array(
			'DESC' => __('Descending','pro'),
			'ASC' => __('Ascending','pro'),
		);

		do_action( $id_base . '_before_form' , $instance );
		
		?>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('layout') ?>">
				<?php _e('Layout', 'pro') ?><br/>
				<?php echo aq_field_select('layout', $block_id, $layout_options, $layout); ?>
			</label>
		</div>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('entries') ?>">
				<?php _e('Number of Entries (per page)', 'pro') ?><br/>
				<?php echo aq_field_input('entries', $block_id, $entries) ?>
			</label>
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('showpagination') ?>">
				<?php _e('Show Pagination', 'pro') ?><br/>
				<?php echo aq_field_select('showpagination', $block_id, $showpagination_options, $showpagination); ?>
			</label>
		</div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('showcategory') ?>">
				<?php _e('Additional Functions', 'pro') ?><br />
				<?php echo aq_field_select('showcategory', $block_id, $showcategory_options, $showcategory); ?>
			</label><div class="cf"></div>
			<label for="<?php echo $this->get_field_id('align') ?>">
			<?php echo aq_field_select('align', $block_id, $align_options, $align); ?>
			</label>
		</div>
		
		<div class="description half last">
			<label for="<?php echo $this->get_field_id('taxonomyfilter') ?>">
				<?php _e('Taxonomy Filter:', 'pro') ?><br />
				<?php echo aq_field_select('taxonomy_type', $block_id, $taxonomy_type_options, $taxonomy_type); ?> <?php echo aq_field_input('taxonomy', $block_id, $taxonomy, $size = 'full') ?>
			</label>
		</div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('order_by') ?>">
				<?php _e('Order Products By:', 'pro') ?><br />
				<?php echo aq_field_select('order_by', $block_id, $order_by_options, $order_by); ?>
			</label>
		</div>	

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('arrange') ?>">
				<br />
				<?php echo aq_field_select('arrange', $block_id, $order_options, $arrange); ?>
			</label>
		</div>	

		<div class="description third">
			<label for="<?php echo $this->get_field_id('bgcolor') ?>">
				<?php _e('Pick a background color', 'pro') ?><br/>
				<?php echo aq_field_color_picker('bgcolor', $block_id, $bgcolor) ?>
			</label>
		</div>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('btncolor') ?>">
				<?php _e('Button Color', 'pro') ?><br/>
				<?php echo aq_field_select('btncolor', $block_id, $btncolor_options, $btncolor); ?>
			</label>
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('pricelabelcolor') ?>">
				<?php _e('Price Label color', 'pro') ?><br/>
				<?php echo aq_field_select('pricelabelcolor', $block_id, $labelcolor_options, $pricelabelcolor); ?>
			</label>
		</div>
		
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);

		$entries = intval($entries);
		$pagination = !empty($showpagination) ? ( $showpagination == 'yes' ? true : 'nopagingblock') : 'nopagingblock';

	    if ($taxonomy_type == 'category') {
	      $taxonomy_category = esc_attr($taxonomy);
	      $taxonomy_tag = '';
	      $context = $taxonomy_type;
	    } else if ($taxonomy_type == 'tag') {
	      $taxonomy_tag = esc_attr($taxonomy);
	      $taxonomy_category = '';
	      $context = $taxonomy_type;
	    } else {
	    	$taxonomy_category = '';
	    	$taxonomy_tag = '';
	    	$context = 'list';
	    }

		switch ($btncolor) {
			case 'grey':
			default:
				$btnclass = '';
				$iconclass = '';
				break;
			case 'blue':
				$btnclass = ' btn-primary';
				$iconclass = ' icon-white';
				break;
			case 'lightblue':
				$btnclass = ' btn-info';
				$iconclass = ' icon-white';
				break;
			case 'green':
				$btnclass = ' btn-success';
				$iconclass = ' icon-white';
				break;
			case 'yellow':
				$btnclass = ' btn-warning';
				$iconclass = ' icon-white';
				break;
			case 'red':
				$btnclass = ' btn-danger';
				$iconclass = ' icon-white';
				break;
			case 'black':
				$btnclass = ' btn-inverse';
				$iconclass = ' icon-white';
				break;
			
		}

		switch ($pricelabelcolor) {
			case 'grey':
			default:
				$labelcolor = '';
				break;
			case 'blue':
				$labelcolor = ' label-info';
				break;
			case 'green':
				$labelcolor = ' label-success';
				break;
			case 'yellow':
				$labelcolor = ' label-warning';
				break;
			case 'red':
				$labelcolor = ' label-important';
				break;
			case 'black':
				$labelcolor = ' label-inverse';
				break;
		}

		switch ($layout) {
			case '2col':
				$span = 'span6';
				$counter = '2';
				break;
			case '3col':
			default:
				$span = 'span4';
				$counter = '3';
				break;
			case '4col':
				$span = 'span3';
				$counter = '4';
				break;
		}

		if (get_query_var('paged')) {
			$page = intval(get_query_var('paged'));
		} elseif (get_query_var('page')) {
			$page = intval(get_query_var('page'));
		} else {
			$page = 1;
		}

		$output = '';
		
		if ($showcategory == 'yes') {

			$output .= '<ul class="mpt-product-categories ' . $align . '">';

				$output .= '<li>' . __( 'By Category:' , 'pro' ) . '</li>';
				$output .= '<li id="all">' . __( 'All' , 'pro' ) . '</li>';

				$args = array(
					'taxonomy' => 'product_category',
					'orderby' => 'name',
					'order' => 'ASC'
				  );

				$categories = get_categories($args);

				if  ($categories) {
				  foreach($categories as $category) {
				    $output .= '<li id="'.$category->slug. '">'.$category->name. '</li>';
				  }
				}

			$output .= '</ul>';

			$output .= '<div class="clear"></div>';

		}

		$quert_args = array(
			'unique_id' => $block_id,
			'sort' => ($showcategory == 'advsoft' ? true : false),
			'align' => $align,
			'context' => $context,
			'echo' => false,
			'paginate' => $pagination,
			'page' => $page,
			'per_page' => $entries,
			'order_by' => $order_by,
			'order' => $arrange,
			'category' => $taxonomy_category,
			'tag' => $taxonomy_tag,
			'counter' => $counter,
			'span' => $span,
			'btnclass' => $btnclass,
			'iconclass' => $iconclass,
			'labelcolor' => $labelcolor,
			'boxclass' => 'thetermsclass',
			'boxstyle' => ' background: '.esc_attr($bgcolor).';'
		);

		$output .= pro_advance_product_sort( $quert_args );

		echo apply_filters( 'aq_mp_product_grid_block' , $output , $instance );

	}
	
}

/** AQ_MP_Image_Taxonomies_Block ****************************************************************************************************/

class AQ_MP_Image_Taxonomies_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('[MP] Image Taxonomies','pro'),
			'size' => 'span4',
		);
		
		//create the block
		parent::__construct('aq_mp_image_taxonomies_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'title' => __('Product Categories','pro'),
			'taxonomy_type' => 'category',
			'count' => 'no',
			'class' => '',
			'style' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$taxonomy_type_options = array(
			'category' => __('Category','pro'),
			'tag' => __('Tag','pro')
		);

		$count_options = array(
			'yes' => __('Yes','pro'),
			'no' => __('No','pro'),
		);

		do_action( $id_base . '_before_form' , $instance );
		
		?>

		<div class="description">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Title (optional):', 'pro') ?><br/>
				<?php echo aq_field_input('title', $block_id, $title) ?>
			</label>
		</div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('taxonomytype') ?>">
				<?php _e('Taxonomy Type:', 'pro') ?><br />
				<?php echo aq_field_select('taxonomy_type', $block_id, $taxonomy_type_options, $taxonomy_type); ?>
			</label>
		</div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('count') ?>">
				<?php _e('Show Product Count:', 'pro') ?><br/>
				<?php echo aq_field_select('count', $block_id, $count_options, $count); ?>
			</label>
		</div>

		<div class="cf"></div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

		<div class="description">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional):', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional):', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>
		
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);

		$class = (!empty($class) ? ' '.esc_attr($class) : '');
		$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');
		$count = ( $count == 'yes' ? 'true' : 'false' );
		$taxonomy_type = ( $taxonomy_type == 'tag' ? 'product_tag' : 'product_category' );

		$output = '<div id="sidebar">';

			$output .= '<div class="well sidebar-widget-well' . $class . '"' . $style . '>';

				$output .= ( !empty($title) ? '<h4 class="sidebar-widget-title">' . esc_attr( $title ) . '</h4>' : '' );
 
				$output .= pro_load_product_taxonomy_menu( array( 'echo' => false , 'termslug' => '' , 'taxonomy' => $taxonomy_type , 'count' => $count ) );

			$output .= '</div>'; // End well

		$output .= '</div>'; // End sidebar

		echo apply_filters( 'aq_mp_image_taxonomies_block_output' , $output , $instance );
	}
	
}

/** AQ_MPcart_Block ****************************************************************************************************/

class AQ_MPcart_Block extends AQ_Block {
	
	function __construct() {
		$block_options = array(
			'name' => __('[MP] Shopping Cart','pro'),
			'size' => 'span4',
		);
		
		parent::__construct('AQ_MPcart_Block', $block_options);
	}
	
	function form($instance) {

	    $instance = wp_parse_args( (array) $instance, array( 'title' => __('Shopping Cart', 'pro'), 'custom_text' => '', 'only_store_pages' => 0 ) );
		$title = $instance['title'];
		$custom_text = $instance['custom_text'];

		do_action( 'aq_mpcart_block_before_form' , $instance );

	  	?>
			<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'pro') ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
			<p><label for="<?php echo $this->get_field_id('custom_text'); ?>"><?php _e('Custom Text:', 'pro') ?><br />
		    <textarea class="widefat" id="<?php echo $this->get_field_id('custom_text'); ?>" name="<?php echo $this->get_field_name('custom_text'); ?>"><?php echo esc_attr($custom_text); ?></textarea></label>
		    </p>
	  	<?php

	  	do_action( 'aq_mpcart_block_after_form' , $instance );

	}
	
	function block($instance) {

		global $mp;

		$output = '<div id="sidebar" style="border-left: none; padding: 0px; margin-bottom: 0px;">';

			$output .= '<div class="well well-small">';

			  $title = $instance['title'];
				if ( !empty( $title ) ) { $output .= '<h4 class="page-header">' . apply_filters('widget_title', $title) . '</h4>'; };

		    if ( !empty($instance['custom_text']) )
		      $output .= '<div class="custom_text">' . $instance['custom_text'] . '</div>';

			    $output .= '<div id="mp_cart_page" class="mp_cart_widget_content">';
			    	$output .= mp_show_cart( 'widget' , NULL , false );
			    $output .= '</div>';

		    $output .= '</div>'; // End well

		$output .= '</div>'; // End #sidebar

		echo apply_filters( 'aq_mpcart_block_output' , $output , $instance );
	}
	
}

/** AQ_MP_Product_Carousel_Block ****************************************************************************************************/

class AQ_MP_Product_Carousel_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('[MP] Product Carousel','pro'),
			'size' => 'span12',
		);
		
		//create the block
		parent::__construct('aq_mp_product_carousel_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'layout' => '3col',
			'entries' => '9',
			'showcategory' => 'no',
			'order_by' => 'date', 
			'arrange' => 'DESC',
			'taxonomy_type' => 'none',
			'taxonomy' => '',
			'bgcolor' => '#fff',
			'btncolor' => 'lightblue',
			'pricelabelcolor' => 'blue',
			'speed' => '4000',
			'pause' => 'yes'
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$layout_options = array(
			'2col' => __('2 Columns','pro'),
			'3col' => __('3 Columns','pro'),
			'4col' => __('4 Columns','pro')
		);

		$entries_options = array(
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
			'13' => '13',
			'14' => '14',
			'15' => '15',
			'16' => '16',
			'17' => '17',
			'18' => '18',
			'19' => '19',
			'20' => '20'
		);

		$btncolor_options = array(
			'grey' => __('Grey','pro'),
			'blue' => __('Blue','pro'),
			'lightblue' => __('Light Blue','pro'),
			'green' => __('Green','pro'),
			'red' => __('Red','pro'),
			'yellow' => __('Yellow','pro'),
			'black' => __('Black','pro'),
		);

		$labelcolor_options = array(
			'grey' => __('Grey','pro'),
			'blue' => __('Blue','pro'),
			'green' => __('Green','pro'),
			'red' => __('Red','pro'),
			'yellow' => __('Yellow','pro'),
			'black' => __('Black','pro'),
		);

		$taxonomy_type_options = array(
			'none' => __('No Filter','pro'),
			'category' => __('Category','pro'),
			'tag' => __('Tag','pro'),
		);

		$pause_options = array(
			'yes' => __('Yes','pro'),
			'no' => __('No','pro'),
		);

		$order_by_options = array(
			'title' => __('Product Name','pro'),
			'date' => __('Publish Date','pro'),
			'ID' => __('Product ID','pro'),
			'author' => __('Product Author','pro'),
			'sales' => __('Number of Sales','pro'),
			'price' => __('Product Price','pro'),
			'rand' => __('Random','pro'),
		);

		$order_options = array(
			'DESC' => __('Descending','pro'),
			'ASC' => __('Ascending','pro'),
		);

		do_action( $id_base . '_before_form' , $instance );
		
		?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('layout') ?>">
				<?php _e('Layout', 'pro') ?><br/>
				<?php echo aq_field_select('layout', $block_id, $layout_options, $layout); ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('entries') ?>">
				<?php _e('Number of Entries', 'pro') ?><br/>
				<?php echo aq_field_select('entries', $block_id, $entries_options, $entries); ?>
			</label>
		</div>

		<div class="description fourth">
			<label for="<?php echo $this->get_field_id('speed') ?>">
				<?php _e('interval (in millisecond)', 'pro') ?><br />
				<?php echo aq_field_input('speed', $block_id, $speed, $size = 'full') ?>
			</label>
		</div>

		<div class="description fourth">
			<label for="<?php echo $this->get_field_id('pause') ?>">
				<?php _e('Pause on hover?', 'pro') ?><br />
				<?php echo aq_field_select('pause', $block_id, $pause_options, $pause); ?>
			</label>
		</div>
		
		<div class="description half last">
			<label for="<?php echo $this->get_field_id('taxonomyfilter') ?>">
				<?php _e('Taxonomy Filter:', 'pro') ?><br />
				<?php echo aq_field_select('taxonomy_type', $block_id, $taxonomy_type_options, $taxonomy_type); ?> <?php echo aq_field_input('taxonomy', $block_id, $taxonomy, $size = 'full') ?>
			</label>
		</div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('order_by') ?>">
				<?php _e('Order Products By:', 'pro') ?><br />
				<?php echo aq_field_select('order_by', $block_id, $order_by_options, $order_by); ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('arrange') ?>">
				<br />
				<?php echo aq_field_select('arrange', $block_id, $order_options, $arrange); ?>
			</label>
		</div>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('bgcolor') ?>">
				<?php _e('Pick a background color', 'pro') ?><br/>
				<?php echo aq_field_color_picker('bgcolor', $block_id, $bgcolor) ?>
			</label>
		</div>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('btncolor') ?>">
				<?php _e('Button Color', 'pro') ?><br/>
				<?php echo aq_field_select('btncolor', $block_id, $btncolor_options, $btncolor); ?>
			</label>
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('pricelabelcolor') ?>">
				<?php _e('Price Label color', 'pro') ?><br/>
				<?php echo aq_field_select('pricelabelcolor', $block_id, $labelcolor_options, $pricelabelcolor); ?>
			</label>
		</div>
		
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);

		switch ($btncolor) {
			case 'grey':
				$btnclass = '';
				$iconclass = '';
				break;
			case 'blue':
				$btnclass = ' btn-primary';
				$iconclass = ' icon-white';
				break;
			case 'lightblue':
				$btnclass = ' btn-info';
				$iconclass = ' icon-white';
				break;
			case 'green':
				$btnclass = ' btn-success';
				$iconclass = ' icon-white';
				break;
			case 'yellow':
				$btnclass = ' btn-warning';
				$iconclass = ' icon-white';
				break;
			case 'red':
				$btnclass = ' btn-danger';
				$iconclass = ' icon-white';
				break;
			case 'black':
				$btnclass = ' btn-inverse';
				$iconclass = ' icon-white';
				break;
			
		}

		switch ($pricelabelcolor) {
			case 'grey':
				$labelcolor = '';
				break;
			case 'blue':
				$labelcolor = ' label-info';
				break;
			case 'green':
				$labelcolor = ' label-success';
				break;
			case 'yellow':
				$labelcolor = ' label-warning';
				break;
			case 'red':
				$labelcolor = ' label-important';
				break;
			case 'Black':
				$labelcolor = ' label-inverse';
				break;
		}

		$query_args = array(
			'echo' => false,
			'slider_id' => $block_id,
			'layout' => $layout,
			'perpage' => $entries,
			'taxonomy_type' => $taxonomy_type,
			'taxonomy' => $taxonomy,
			'order_by' => $order_by,
			'order' => $arrange,
			'sliderpause' => $pause,
			'sliderspeed' => $speed, 
			'btnclass' => $btnclass,
			'iconclass' => $iconclass,
			'labelcolor' => $labelcolor,
			'carouselclass' => '',
			'carouselstyle' => '',
			'boxclass' => '',
			'boxstyle' => ' background: '.esc_attr($bgcolor).';'
		);

		$output = pro_load_product_carousel( $query_args );

		echo apply_filters( 'aq_mp_product_carousel_block_output' , $output , $instance );

	}
}