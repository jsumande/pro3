<?php 

/* Separator block
----------------------------------------------------------------------------------------------------------- */

	class AQ_Separator_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => __('Separator','pro'),
				'size' => 'span12',
			);
			
			//create the block
			parent::__construct('aq_separator_block', $block_options);
		}
		
		function form($instance) {
			
			$defaults = array(
				'horizontal_line' => 'none',
				'line_color' => '#e5e5e5',
				'pattern' => '1',
				'height' => '5'
			);
			
			$line_options = array(
				'none' => __('None','pro'),
				'solid' => __('Solid','pro'),
				'dashed' => __('Dashed','pro'),
				'dotted' => __('Dotted','pro'),
			);
			
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);
			
			$line_color = isset($line_color) ? $line_color : '#353535';

			do_action( $id_base . '_before_form' , $instance );
			
			?>
			<div class="description note">
				<?php _e('Use this block to clear the floats between two or more separate blocks vertically.', 'pro') ?>
			</div>

			<div class="description fourth">
				<label for="<?php echo $this->get_field_id('horizontal_line') ?>">
					<?php _e('Pick a horizontal line', 'pro') ?><br/>
					<?php echo aq_field_select('horizontal_line', $block_id, $line_options, $horizontal_line); ?>
				</label>
			</div>
			<div class="description fourth">
				<label for="<?php echo $this->get_field_id('height') ?>">
					<?php _e('Height', 'pro') ?><br/>
					<?php echo aq_field_input('height', $block_id, $height, 'min', 'number') ?> px
				</label>
				
			</div>
			<div class="description half last">
				<label for="<?php echo $this->get_field_id('line_color') ?>">
					<?php _e('Pick a line color', 'pro') ?><br/>
					<?php echo aq_field_color_picker('line_color', $block_id, $line_color, $defaults['line_color']) ?>
				</label>
				
			</div>
			<?php

			do_action( $id_base . '_after_form' , $instance );
			
		}
		
		function block($instance) {
			extract($instance);

			$output = '';
			
			switch($horizontal_line) {
				case 'none':
					$output .= '<div class="cf" style="height: '.esc_attr($height).'px;margin-bottom: '.esc_attr($height).'px;"></div><div class="clear"></div>';
					break;
				case 'solid':
					$output .= '<div class="cf" style="border-bottom: 1px solid '.$line_color.';height: '.esc_attr($height).'px;margin-bottom: '.esc_attr($height).'px;"></div><div class="clear"></div>';
					break;
				case 'dashed':
					$output .= '<div class="cf" style="border-bottom: 1px dashed '.$line_color.';height: '.esc_attr($height).'px;margin-bottom: '.esc_attr($height).'px;"></div><div class="clear"></div>';
					break;
				case 'dotted':
					$output .= '<div class="cf" style="border-bottom: 1px dotted '.$line_color.';height: '.esc_attr($height).'px;margin-bottom: '.esc_attr($height).'px;"></div><div class="clear"></div>';
					break;
			}

			echo apply_filters( 'aq_separator_block_output' , $output , $instance );
			
		}
		
	}


/* AQ_Post_Grid_Block
----------------------------------------------------------------------------------------------------------- */

	class AQ_Post_Grid_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => __('Post Grid','pro'),
				'size' => 'span12',
			);
			
			//create the block
			parent::__construct('aq_post_grid_block', $block_options);
		}
		
		function form($instance) {
			
			$defaults = array(
				'layout' => '3col',
				'entries' => '9',
				'showpagination' => 'no',
				'order_by' => 'date', 
				'arrange' => 'DESC',
				'taxonomy_type' => 'none',
				'taxonomy' => '',
				'boxshadowtype' => 'shadow-type-2',
				'bgcolor' => '#fafafa',
				'bordercolor' => '#e5e5e5',
				'textcolor' => '#676767',
				'titlecolor' => '#242424',
				'showexcerpt' => 'yes',
				'showmeta' => 'yes',
				'showauthordetails' => 'yes',
				'showcomments' => 'yes',
				'showhovertag' => 'yes',
				'hovertagcolor' => 'hover-tag-black',
				'metatagscolor' => 'meta-tag-lightblue'
			);

			$instance = wp_parse_args($instance, $defaults);
			extract($instance);

			$layout_options = array(
				'1col' => __('1 Column','pro'),
				'2col' => __('2 Columns','pro'),
				'3col' => __('3 Columns','pro'),
				'4col' => __('4 Columns','pro')
			);

			$boxshadow_options = array(
				'none' => __('None','pro'),
				'shadow-type-1' => __('Type 1','pro'),
				'shadow-type-2' => __('Type 2','pro')
			);

			$yes_no_options = array(
				'yes' => __('Yes','pro'),
				'no' => __('No','pro'),
			);

			$hovertagcolor_options = array(
				'hover-tag-black' => __('Black','pro'),
				'hover-tag-red' => __('Red','pro'),
				'hover-tag-blue' => __('Blue','pro'),
				'hover-tag-lightblue' => __('Light Blue','pro'),
				'hover-tag-green' => __('Green','pro'),
				'hover-tag-yellow' => __('Yellow','pro'),
				'hover-tag-grey' => __('Grey','pro'),
			);

			$metatagcolor_options = array(
				'meta-tag-black' => __('Black','pro'),
				'meta-tag-red' => __('Red','pro'),
				'meta-tag-blue' => __('Blue','pro'),
				'meta-tag-lightblue' => __('Light Blue','pro'),
				'meta-tag-green' => __('Green','pro'),
				'meta-tag-yellow' => __('Yellow','pro'),
				'meta-tag-grey' => __('Grey','pro'),
			);

			$taxonomy_type_options = array(
				'none' => __('No Filter','pro'),
				'category' => __('Category','pro'),
				'tag' => __('Tag','pro'),
			);

			$showpagination_options = array(
				'yes' => __('Yes','pro'),
				'no' => __('No','pro'),
			);

			$order_by_options = array(
				'title' => __('Post Name','pro'),
				'date' => __('Publish Date','pro'),
				'ID' => __('Post ID','pro'),
				'author' => __('Post Author','pro'),
				'rand' => __('Random','pro'),
			);

			$order_options = array(
				'DESC' => __('Descending','pro'),
				'ASC' => __('Ascending','pro'),
			);

			do_action( $id_base . '_before_form' , $instance );
			
			?>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('layout') ?>">
					<?php _e('Layout', 'pro') ?><br/>
					<?php echo aq_field_select('layout', $block_id, $layout_options, $layout); ?>
				</label>
			</div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('entries') ?>">
					<?php _e('Number of Entries', 'pro') ?><br/>
					<?php echo aq_field_input('entries', $block_id, $entries, $size = 'full') ?>
				</label>
			</div>

			<div class="description third last">
				<label for="<?php echo $this->get_field_id('showpagination') ?>">
					<?php _e('Show Pagination', 'pro') ?><br/>
					<?php echo aq_field_select('showpagination', $block_id, $showpagination_options, $showpagination); ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('taxonomyfilter') ?>">
					<?php _e('Taxonomy Filter:', 'pro') ?><br />
					<?php echo aq_field_select('taxonomy_type', $block_id, $taxonomy_type_options, $taxonomy_type); ?> <?php echo aq_field_input('taxonomy', $block_id, $taxonomy, $size = 'full') ?>
				</label>
			</div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('order_by') ?>">
					<?php _e('Order Post By:', 'pro') ?><br />
					<?php echo aq_field_select('order_by', $block_id, $order_by_options, $order_by); ?>
				</label>
			</div>	

			<div class="description third last">
				<label for="<?php echo $this->get_field_id('arrange') ?>">
					<br />
					<?php echo aq_field_select('arrange', $block_id, $order_options, $arrange); ?>
				</label>
			</div>	

			<div class="cf"></div>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('showhovertag') ?>">
					<?php _e('Show hover tag', 'pro') ?><br/>
					<?php echo aq_field_select('showhovertag', $block_id, $yes_no_options, $showhovertag); ?>
				</label>
			</div>

			<div class="description half last">
				<label for="<?php echo $this->get_field_id('showauthordetails') ?>">
					<?php _e('Show Author Name', 'pro') ?><br/>
					<?php echo aq_field_select('showauthordetails', $block_id, $yes_no_options, $showauthordetails); ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('showcomments') ?>">
					<?php _e('Show Comments', 'pro') ?><br/>
					<?php echo aq_field_select('showcomments', $block_id, $yes_no_options, $showcomments); ?>
				</label>
			</div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('showexcerpt') ?>">
					<?php _e('Show Excerpt', 'pro') ?><br/>
					<?php echo aq_field_select('showexcerpt', $block_id, $yes_no_options, $showexcerpt); ?>
				</label>
			</div>

			<div class="description third last">
				<label for="<?php echo $this->get_field_id('showmeta') ?>">
					<?php _e('Show Meta Tag', 'pro') ?><br/>
					<?php echo aq_field_select('showmeta', $block_id, $yes_no_options, $showmeta); ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description fourth">
				<label for="<?php echo $this->get_field_id('bgcolor') ?>">
					<?php _e('Pick a background color', 'pro') ?><br/>
					<?php echo aq_field_color_picker('bgcolor', $block_id, $bgcolor) ?>
				</label>
			</div>

			<div class="description fourth">
				<label for="<?php echo $this->get_field_id('textcolor') ?>">
					<?php _e('Pick a text color', 'pro') ?><br/>
					<?php echo aq_field_color_picker('textcolor', $block_id, $textcolor) ?>
				</label>
			</div>

			<div class="description fourth">
				<label for="<?php echo $this->get_field_id('bordercolor') ?>">
					<?php _e('Pick a border color', 'pro') ?><br/>
					<?php echo aq_field_color_picker('bordercolor', $block_id, $bordercolor) ?>
				</label>
			</div>

			<div class="description fourth last">
				<label for="<?php echo $this->get_field_id('titlecolor') ?>">
					<?php _e('Pick a Title color', 'pro') ?><br/>
					<?php echo aq_field_color_picker('titlecolor', $block_id, $titlecolor) ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('hovertagcolor') ?>">
					<?php _e('Hover tag color', 'pro') ?><br/>
					<?php echo aq_field_select('hovertagcolor', $block_id, $hovertagcolor_options, $hovertagcolor); ?>
				</label>
			</div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('metatagscolor') ?>">
					<?php _e('Meta tag color', 'pro') ?><br/>
					<?php echo aq_field_select('metatagscolor', $block_id, $metatagcolor_options, $metatagscolor); ?>
				</label>
			</div>

			<div class="description third last">
				<label for="<?php echo $this->get_field_id('boxshadowtype') ?>">
					<?php _e('Box Shadow', 'pro') ?><br/>
					<?php echo aq_field_select('boxshadowtype', $block_id, $boxshadow_options, $boxshadowtype); ?>
				</label>
			</div>

			<div class="cf"></div>
			
			<?php

			do_action( $id_base . '_after_form' , $instance );
		}
		
		function block($instance) {
			extract($instance);

			$post_grid_args = array(
				'echo' => false,
				'paginate' => ( $showpagination == 'yes' ? true : 'nopagingblock' ),
				'per_page' => intval($entries), 
				'order_by' => $order_by, 
				'order' => $arrange,
				'category' => ( $taxonomy_type == 'category' ? esc_attr($taxonomy) : '' ), 
				'tag' => ( $taxonomy_type == 'tag' ? esc_attr($taxonomy) : '' ),
				'columns' => esc_attr($layout),
				'poststyle' => 'style-2',
				'boxclass' => ' post-grid' . ( $boxshadowtype == 'none' ? '' : ' ' . esc_attr($boxshadowtype) ),
				'boxstyle' => 'background: ' . ( !empty($bgcolor) ? $bgcolor : '#fafafa' ) . '; border-color: ' . ( !empty($bordercolor) ? $bordercolor : '#e5e5e5' ) . '; color: ' . ( !empty($textcolor) ? $textcolor : '#676767' ) . '; ',
				'extra' => array(
					'showexcerpt' => ( $showexcerpt == 'yes' ? true : false ),
					'showmeta' => ( $showmeta == 'yes' ? true : false ),
					'showauthordetails' => ( $showauthordetails == 'yes' ? true : false ),
					'showcomments' => ( $showcomments == 'yes' ? true : false ),
					'showhovertag' => ( $showhovertag == 'yes' ? true : false ),
					'titlecolor' => $titlecolor,
					'hovertagcolor' => $hovertagcolor,
					'metatagscolor' => $metatagscolor,
				)
			);

			$output = pro_display_wp_post_query( $post_grid_args );

			echo apply_filters( 'aq_post_grid_block_output' , $output , $instance );

		}
		
	}


/* Blog Updates block
----------------------------------------------------------------------------------------------------------- */

	class AQ_Blog_Updates_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => __('Blog Updates','pro'),
				'size' => 'span12',
			);
			
			//create the block
			parent::__construct('aq_blog_updates_block', $block_options);
		}
		
		function form($instance) {
			
			$defaults = array(
				'layout' => '4col',
				'entries' => '4',
				'excerpt' => '25',
			);
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);

			$layout_options = array(
				'2col' => __('2 Columns','pro'),
				'3col' => __('3 Columns','pro'),
				'4col' => __('4 Columns','pro')
			);

			$entries_options = array(
				'2' => '2',
				'3' => '3',
				'4' => '4',
				'5' => '5',
				'6' => '6',
				'7' => '7',
				'8' => '8',
				'9' => '9',
				'10' => '10',
				'11' => '11',
				'12' => '12',
				'13' => '13',
				'14' => '14',
				'15' => '15',
				'16' => '16',
				'17' => '17',
				'18' => '18',
				'19' => '19',
				'20' => '20'
			);

			do_action( $id_base . '_before_form' , $instance );
			
			?>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('layout') ?>">
					<?php _e('Layout', 'pro') ?><br/>
					<?php echo aq_field_select('layout', $block_id, $layout_options, $layout); ?>
				</label>
			</div>

			<div class="description half last">
				<label for="<?php echo $this->get_field_id('entries') ?>">
					<?php _e('Number of Entries', 'pro') ?><br/>
					<?php echo aq_field_select('entries', $block_id, $entries_options, $entries); ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('excerpt') ?>">
					<?php _e('Total words in Excerpt', 'pro') ?><br/>
					<?php echo aq_field_input('excerpt', $block_id, $excerpt, $size = 'full') ?>
					<em style="padding-left: 5px; font-size: 0.75em;">Leave it blank or enter "0" to disable excerpt.</em>
				</label>
			</div>
			
			<?php

			do_action( $id_base . '_after_form' , $instance );
		}
		
		function block($instance) {
			extract($instance);

			switch ($layout) {
				case '2col':
					$span = 'span6';
					$imagesize = 'tb-860';
					$counter = '2';
					$videoheight = '195';
					break;
				case '3col':
					$span = 'span4';
					$imagesize = 'tb-360';
					$counter = '3';
					$videoheight = '245';
					break;
				case '4col':
					$span = 'span3';
					$imagesize = 'tb-360';
					$counter = '4';
					$videoheight = '145';
					break;							
				default:
					$span = 'span3';
					$imagesize = 'tb-360';
					$counter = '4';
					$videoheight = '145';
					break;
			}

			$count = 1;
			query_posts( 'showposts='.$entries.'&post_type=post' );

			$output = '<div id="blog-updates">';

				if (have_posts()) : while (have_posts()) : the_post();

					$incomplete_row = true;

					if ( $count == 1 )
						$output .= '<div class="row-fluid">';

					$output .= '<div class="' . $span . ' well well-small">';

						$id = get_the_ID();
						$de_post = get_post( $id );
						$temp = get_post_meta( $id, '_mpt_post_select_temp', true );

						$args =  array(
							'echo' => false,
							'post_id' => $id,
							'content_type' => 'list',
							'imagesize' => $imagesize,
							'videoheight' => $videoheight
						);

						if ($temp == 'image-carousel') {
							$output .= mpt_load_image_carousel( $args );
						} else if ($temp == 'video') {
							$output .= mpt_load_video_post( $args );
						} else {
							$output .= mpt_load_featured_image( $args );
						}
					
						$output .= '<a href="' . get_permalink( $id ) . '"><h3 class="post-title">' . get_the_title( $id ) . '</h3></a>';
						$output .= '<p class="post-meta">' . __( 'Posted By ' , 'pro' ) .' '. get_the_author_meta( 'display_name' , $de_post->post_author ) . ' on ' . get_the_date() . '</p>';

						if (!empty($excerpt) || $excerpt == '0')
							$output .= ( !empty($de_post->post_excerpt) ? $de_post->post_excerpt : ( !empty($de_post->post_content) ? wp_trim_words($de_post->post_content , $excerpt ) : '' ) );

					$output .= '</div>'; // End well

					if ( $count == $counter ) {
						$output .= '</div>'; // End - row-fluid
						$incomplete_row = false;
						$count = 0;
					}

					$count++;

				endwhile; endif;
				wp_reset_query();

				if ( $incomplete_row )
					$output .= '</div>'; // End - row-fluid

			$output .= '</div>'; // End - blog-updates

			echo apply_filters( 'aq_blog_updates_block_output' , $output , $instance );

		}
		
	}

/* AQ_Button_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Button_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('Button','pro'),
			'size' => 'span6',
		);
		
		//create the block
		parent::__construct('aq_button_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'text' => __('Button','pro'),
			'link' => '#',
			'color' => 'grey',
			'btnsize' => 'default',
			'icontype' => 'none',
			'whiteicon' => '0',
			'align' => 'none',
			'btnlinkopen' => 'same',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$color_options = array(
			'grey' => __('Grey','pro'),
			'blue' => __('Blue','pro'),
			'lightblue' => __('Light Blue','pro'),
			'green' => __('Green','pro'),
			'red' => __('Red','pro'),
			'yellow' => __('Yellow','pro'),
			'black' => __('Black','pro'),
		);

		$size_options = array(
			'default' => __('Default','pro'),
			'mini' => __('Mini','pro'),
			'small' => __('Small','pro'),
			'large' => __('Large','pro'),
			'huge' => __('Huge','pro'),
			'block' => __('Block','pro'),
		);

		$align_options = array(
			'none' => __('None','pro'),
			'left' => __('Left','pro'),
			'center' => __('Center','pro'),
			'right' => __('Right','pro')
		);

		$btnlinkopen_options = array(
			'same' => __('Same Window','pro'),
			'new' => __('New Window','pro')
		);

		global $propb_custom;

		do_action( $id_base . '_before_form' , $instance );
		
		?>
		<div class="description">
			<label for="<?php echo $this->get_field_id('text') ?>">
				<?php _e('Button Text', 'pro') ?>
				<?php echo aq_field_input('text', $block_id, $text, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>
		
		<div class="description two-third">
			<label for="<?php echo $this->get_field_id('link') ?>">
				<?php _e('Button Link', 'pro') ?>
				<?php echo aq_field_input('link', $block_id, $link, $size = 'full') ?>
			</label>	
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('btnlinkopen') ?>">
				<?php _e('Link Open In', 'pro') ?><br/>
				<?php echo aq_field_select('btnlinkopen', $block_id, $btnlinkopen_options, $btnlinkopen); ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('color') ?>">
				<?php _e('Button Color', 'pro') ?><br/>
				<?php echo aq_field_select('color', $block_id, $color_options, $color); ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('btnsize') ?>">
				<?php _e('Button Size', 'pro') ?><br/>
				<?php echo aq_field_select('btnsize', $block_id, $size_options, $btnsize); ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('align') ?>">
				<?php _e('Align', 'pro') ?><br/>
				<?php echo aq_field_select('align', $block_id, $align_options, $align); ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('icontype') ?>">
				<?php _e('Icon Type', 'pro') ?><br/>
				<?php echo $propb_custom->load_icon_select_field( 'icontype', $block_id, $icontype ); ?>
			</label>
		</div>

		<div class="cf"></div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>
		
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);

		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$class = (!empty($class) ? ' '.esc_attr($class) : '');
		$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');
		
		if ($icontype == 'none') {
			$iconoutput = '';
		} else {
			$iconoutput = '<i class="'.$icontype.'"></i> ';
		}

		switch ($color) {
			case 'grey':
				$class .= '';
				break;
			case 'blue':
				$class .= ' btn-primary';
				break;
			case 'lightblue':
				$class .= ' btn-info';
				break;
			case 'green':
				$class .= ' btn-success';
				break;
			case 'yellow':
				$class .= ' btn-warning';
				break;
			case 'red':
				$class .= ' btn-danger';
				break;
			case 'black':
				$class .= ' btn-inverse';
				break;
			
		}

		switch ($btnsize) {
			case 'default':
				$class .= '';
				break;
			case 'large':
				$class .= ' btn-large';
				break;
			case 'small':
				$class .= ' btn-small';
				break;
			case 'mini':
				$class .= ' btn-mini';
				break;	
			case 'block':
				$class .= ' btn-block';
				break;	
			case 'huge':
				$class .= ' btn-big';
				break;	
		}

		switch ($align) {
			case 'none':
				$frontdiv = '';
				$enddiv = '';
				break;
			case 'left':
				$frontdiv = '<div class="align-left">';
				$enddiv = '</div>';
				break;
			case 'center':
				$frontdiv = '<div class="align-center">';
				$enddiv = '</div>';
				break;
			case 'right':
				$frontdiv = '<div class="align-right">';
				$enddiv = '</div>';
				break;

		}
		
		$output = $frontdiv.'<a href="'.esc_url($link).'"'.($btnlinkopen == 'new' ? ' target="_blank"' : '' ).'><button'.$id.' class="btn'.$class.'"'.$style.'>'.$iconoutput.strip_tags($text).'</button></a>'.$enddiv;
		
		echo apply_filters( 'aq_button_block_output' , $output , $instance );
	}
	
}

/* AQ_Contact_Block
----------------------------------------------------------------------------------------------------------- */

	class AQ_Contact_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => __('Contact Form','pro'),
				'size' => 'span6',
			);
			
			//create the block
			parent::__construct('aq_contact_block', $block_options);
		}
		
		function form($instance) {
			
			$defaults = array(
				'title' => '',
				'sendtoemail' => '',
				'btntext' => __('Send Message','pro'),
				'btncolor' => 'black',
				'btnsize' => 'large',
				'shortcode' => '',
				'id' => '',
				'class' => '',
				'style' => ''
			);
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);

			$btncolor_options = array(
				'grey' => __('Grey','pro'),
				'blue' => __('Blue','pro'),
				'lightblue' => __('Light Blue','pro'),
				'green' => __('Green','pro'),
				'red' => __('Red','pro'),
				'yellow' => __('Yellow','pro'),
				'black' => __('Black','pro'),
			);

			$btnsize_options = array(
				'default' => __('Default','pro'),
				'mini' => __('Mini','pro'),
				'small' => __('Small','pro'),
				'large' => __('Large','pro'),
				'block' => __('Block','pro'),
			);

			do_action( $id_base . '_before_form' , $instance );
			
			?>
			
			<div class="description">
				<label for="<?php echo $this->get_field_id('title') ?>">
					<?php _e('Title (optional)', 'pro') ?><br/>
					<?php echo aq_field_input('title', $block_id, $title) ?>
				</label>
			</div>

			<div class="description">
				<label for="<?php echo $this->get_field_id('sendtoemail') ?>">
					<?php _e('Send To Email', 'pro') ?><br/>
					<?php echo aq_field_input('sendtoemail', $block_id, $sendtoemail) ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('btntext') ?>">
					<?php _e('Button Text', 'pro') ?><br/>
					<?php echo aq_field_input('btntext', $block_id, $btntext) ?>
				</label>
			</div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('btnsize') ?>">
					<?php _e('Button Size', 'pro') ?><br/>
					<?php echo aq_field_select('btnsize', $block_id, $btnsize_options, $btnsize); ?>
				</label>
			</div>

			<div class="description third last">
				<label for="<?php echo $this->get_field_id('btncolor') ?>">
					<?php _e('Button Color', 'pro') ?><br/>
					<?php echo aq_field_select('btncolor', $block_id, $btncolor_options, $btncolor); ?>
				</label>
			</div>

			<div class="cf"></div>


			<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

			<div class="description">
				<label for="<?php echo $this->get_field_id('class') ?>">
					<?php _e('class (optional)', 'pro') ?><br/>
					<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description">
				<label for="<?php echo $this->get_field_id('style') ?>">
					<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
					<?php echo aq_field_input('style', $block_id, $style) ?>
				</label>
			</div>

			<?php

			do_action( $id_base . '_after_form' , $instance );
			
		}
		
		function block($instance) {
			extract($instance);

			$userclass = (!empty($class) ? ' '.esc_attr($class) : '');
			$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');

			$btnclass = 'btn';
			$theemailsent = '';

			switch ($btncolor) {
				case 'grey':
					$btnclass .= '';
					break;
				case 'blue':
					$btnclass .= ' btn-primary';
					break;
				case 'lightblue':
					$btnclass .= ' btn-info';
					break;
				case 'green':
					$btnclass .= ' btn-success';
					break;
				case 'yellow':
					$btnclass .= ' btn-warning';
					break;
				case 'red':
					$btnclass .= ' btn-danger';
					break;
				case 'black':
					$btnclass .= ' btn-inverse';
					break;
				
			}

			switch ($btnsize) {
				case 'default':
					$btnclass .= '';
					break;
				case 'large':
					$btnclass .= ' btn-large';
					break;
				case 'small':
					$btnclass .= ' btn-small';
					break;
				case 'mini':
					$btnclass .= ' btn-mini';
					break;	
				case 'block':
					$btnclass .= ' btn-block';
					break;	
			}

			  if(isset($_POST['submitted'])) {

			    if(sanitize_text_field($_POST['inputname']) === '') {
			      $thenameerror = __( 'Please enter your name below.' , 'pro' );
			      $haserror = true;
			    } else {
			      $thename = sanitize_text_field($_POST['inputname']);
			    }

			    if(sanitize_text_field($_POST['inputemail']) === '')  {
			      $theemailerror = __('Please enter your valid email address below.','pro');
			      $haserror = true;
			    } else if (!is_email(sanitize_text_field($_POST['inputemail']))) {
			      $theemailerror = __('You entered an invalid email address.','pro');
			      $haserror = true;
			    } else {
			      $theemail = sanitize_text_field($_POST['inputemail']);
			    }

			    if(sanitize_text_field($_POST['inputsubject']) === '') {
			      $subjecterror = __('Please enter the subject line below.','pro');
			      $haserror = true;
			    } else {
			      $subject = sanitize_text_field($_POST['inputsubject']);
			    }

			    if(sanitize_text_field($_POST['themessage']) === '') {
			      $commenterror = __('Please enter a message below.','pro');
			      $haserror = true;
			    } else {
			      if(function_exists('stripslashes')) {
			        $comments = stripslashes(sanitize_text_field($_POST['themessage']));
			      } else {
			        $comments = sanitize_text_field($_POST['themessage']);
			      }
			    }

			    if(!isset($haserror)) {
			      $sendtoemail = is_email($sendtoemail);

			      if (!empty($sendtoemail) && $sendtoemail !="false") {
			      	$theemailto = $sendtoemail;
			      } else {
			      	$theemailto = get_bloginfo('admin_email');
			      }
			      
			      $body = "Name: $thename \n\nEmail: $theemail \n\nMessage: $comments";
			      $headers = 'From: '.$thename.' <'.$theemailto.'>' . "\r\n" . 'Reply-To: ' . $theemail;

			      wp_mail($theemailto, $subject, $body, $headers);
			      $theemailsent = 'true';
			    }			    

			  }

			$output = '';

			if ( !empty($title) )
				$output .= '<h3 class="contact-form-block-title">' . esc_attr($title) . '</h3>';

		  	$output .= '<div id="contact-form-' . $block_id . '" class="contact-form-block well' . $userclass . '"' . $style . '>';

			  	$output .= '<form action="' . ( is_home() ? get_home_url().'/' : get_permalink() ) . '#contact-form-' . $block_id . '" class="sa-form" id="contact-form" method="post">';

				  	$output .= ( $theemailsent == 'true' ? '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>' . __('Your message was sent successfully.' , 'pro' ) . '</strong>' . __('We will get back to you as soon as possible.' , 'pro') . '</div>' : '' );

				  	// Name
				  	$output .= '<div class="control-group">';
					  	$output .= '<label class="control-label" for="inputname"><b>' . __('Your Name:' , 'pro' ) . '</b></label>';
					  	$output .= (!empty($thenameerror) ? '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>'.$thenameerror.'</div>' : '');
					  	$output .= '<div class="controls">';
					  		$output .= '<input name="inputname" class="width-100 mobile-full-width" id="inputname" type="text" value="' . ( !empty($thename) ? $thename : '' ) . '">';
					  	$output .= '</div>'; // End - Controls
				  	$output .= '</div>'; // End control-group

				  	// Email
				  	$output .= '<div class="control-group">';
					  	$output .= '<label class="control-label" for="inputemail"><b>' . __('Your Email:' , 'pro' ) . '</b></label>';
					  	$output .= (!empty($theemailerror) ? '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>'.$theemailerror.'</div>' : '');
					  	$output .= '<div class="controls">';
					  		$output .= '<input name="inputemail" class="width-100 mobile-full-width" id="inputemail" type="text" value="' . ( !empty($theemail) ? $theemail : '' ) .'">';
					  	$output .= '</div>'; // End - Controls
				  	$output .= '</div>'; // End control-group

				  	// Subject
				  	$output .= '<div class="control-group">';
					  	$output .= '<label class="control-label" for="inputsubject"><b>' . __('Subject:' , 'pro' ) . '</b></label>';
					  	$output .= (!empty($subjecterror) ? '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>'.$subjecterror.'</div>' : '');
					  	$output .= '<div class="controls">';
					  		$output .= '<input name="inputsubject" class="width-100 mobile-full-width" id="inputsubject" type="text" value="' . ( !empty($subject) ? $subject : '' ) . '">';
					  	$output .= '</div>'; // End - Controls
				  	$output .= '</div>'; // End control-group

				  	// message
				  	$output .= '<div class="control-group">';
					  	$output .= '<label class="control-label" for="inputmessage"><b>' . __('Message:' , 'pro' ) . '</b></label>';
					  	$output .= (!empty($commenterror) ? '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>'.$commenterror.'</div>' : '');
					  	$output .= '<div class="controls">';
					  		$output .= '<textarea rows="5" class="width-100 mobile-full-width" name="themessage" id="themessage">' . ( !empty($comments) ? $comments : '' ) . '</textarea>';
					  	$output .= '</div>'; // End - Controls
				  	$output .= '</div>'; // End control-group

				  	// Button
				  	$output .= '<div class="align-left">';
				  		$output .= '<button type="submit" class="' . $btnclass . '">' . esc_attr($btntext) . '</button>';
				  	$output .= '</div>';
				  	$output .= '<input type="hidden" name="submitted" id="submitted" value="true" />';

				$output .= '</form>';

		  	$output .= '</div>'; // End contact-form-block

		  	echo apply_filters( 'aq_contact_block_output' , $output , $instance );
			
		}
		
	}

/* AQ_CTA_Block
----------------------------------------------------------------------------------------------------------- */

	class AQ_CTA_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => __('Call To Action','pro'),
				'size' => 'span12',
			);
			
			//create the block
			parent::__construct('aq_cta_block', $block_options);
		}
		
		function form($instance) {
			
			$defaults = array(
				'title' => '',
				'headline' => '',
				'subheadline' => '',
				'heading' => 'h2',
				'align' => 'left',
				'bgcolor' => '#F2EFEF',
				'textcolor'	=> '#676767',
				'bordercolor' => '#00a5f7',
				'btntext' => 'Learn More',
				'btncolor' => 'grey',
				'btnsize' => 'large',
				'btnlink' => '',
				'btnicon' => 'none',
				'btnlinkopen' => 'same',
				'id' => '',
				'class' => '',
				'style' => ''
			);
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);

			global $propb_custom;

			$heading_style = array(
				'h1' => 'H1',
				'h2' => 'H2',
				'h3' => 'H3',
				'h4' => 'H4',
				'h5' => 'H5',
				'h6' => 'H6',
			);

			$align_options = array(
				'left' => __('Left','pro'),
				'center' => __('Center','pro'),
				'right' => __('Right','pro')
			);

			$btncolor_options = array(
				'grey' => __('Grey','pro'),
				'blue' => __('Blue','pro'),
				'lightblue' => __('Light Blue','pro'),
				'green' => __('Green','pro'),
				'red' => __('Red','pro'),
				'yellow' => __('Yellow','pro'),
				'black' => __('Black','pro'),
			);

			$btnsize_options = array(
				'default' => __('Default','pro'),
				'mini' => __('Mini','pro'),
				'small' => __('Small','pro'),
				'large' => __('Large','pro')
			);

			$btnlinkopen_options = array(
				'same' => __('Same Window','pro'),
				'new' => __('New Window','pro')
			);

			do_action( $id_base . '_before_form' , $instance );
			
			?>
			<div class="description">
				<label for="<?php echo $this->get_field_id('title') ?>">
					<?php _e('Title (optional - won\'t display)', 'pro') ?>
					<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
				</label>
			</div>
			
			<div class="description">
				<label for="<?php echo $this->get_field_id('headline') ?>">
					<?php _e('Headline', 'pro') ?>
					<?php echo aq_field_textarea('headline', $block_id, $headline, $size = 'full') ?>
				</label>
			</div>

			<div class="description">
				<label for="<?php echo $this->get_field_id('subheadline') ?>">
					<?php _e('Subheadline', 'pro') ?>
					<?php echo aq_field_textarea('subheadline', $block_id, $subheadline, $size = 'full') ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('heading') ?>">
					<?php _e('Heading Type', 'pro') ?><br/>
					<?php echo aq_field_select('heading', $block_id, $heading_style, $heading); ?>
				</label>
			</div>

			<div class="description half last">
				<label for="<?php echo $this->get_field_id('align') ?>">
					<?php _e('Text Align', 'pro') ?><br/>
					<?php echo aq_field_select('align', $block_id, $align_options, $align); ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('bgcolor') ?>">
					<?php _e('Pick a background color', 'pro') ?><br/>
					<?php echo aq_field_color_picker('bgcolor', $block_id, $bgcolor) ?>
				</label>
			</div>

			<div class="description third">
				<label for="<?php echo $this->get_field_id('textcolor') ?>">
					<?php _e('Pick a text color', 'pro') ?><br/>
					<?php echo aq_field_color_picker('textcolor', $block_id, $textcolor) ?>
				</label>
			</div>

			<div class="description third last">
				<label for="<?php echo $this->get_field_id('bordercolor') ?>">
					<?php _e('Pick a border color', 'pro') ?><br/>
					<?php echo aq_field_color_picker('bordercolor', $block_id, $bordercolor) ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('btntext') ?>">
					<?php _e('Button Text', 'pro') ?>
					<?php echo aq_field_input('btntext', $block_id, $btntext, $size = 'full') ?>
				</label>
			</div>
			
			<div class="description half last">
				<label for="<?php echo $this->get_field_id('btnlink') ?>">
					<?php _e('Button Link', 'pro') ?>
					<?php echo aq_field_input('btnlink', $block_id, $btnlink, $size = 'full') ?>
				</label>	
			</div>

			<div class="cf"></div>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('btnlinkopen') ?>">
					<?php _e('Link Open In', 'pro') ?><br/>
					<?php echo aq_field_select('btnlinkopen', $block_id, $btnlinkopen_options, $btnlinkopen); ?>
				</label>
			</div>

			<div class="description half last">
				<label for="<?php echo $this->get_field_id('btncolor') ?>">
					<?php _e('Button Color', 'pro') ?><br/>
					<?php echo aq_field_select('btncolor', $block_id, $btncolor_options, $btncolor); ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('btnsize') ?>">
					<?php _e('Button Size', 'pro') ?><br/>
					<?php echo aq_field_select('btnsize', $block_id, $btnsize_options, $btnsize); ?>
				</label>
			</div>

			<div class="description half last">
				<label for="<?php echo $this->get_field_id('btnicon') ?>">
					<?php _e('Button Icon', 'pro') ?><br/>
					<?php echo $propb_custom->load_icon_select_field( 'btnicon', $block_id, $btnicon ); ?>
				</label>
			</div>

			<div class="cf"></div>

			<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('id') ?>">
					<?php _e('id (optional)', 'pro') ?><br/>
					<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
				</label>
			</div>

			<div class="description half last">
				<label for="<?php echo $this->get_field_id('class') ?>">
					<?php _e('class (optional)', 'pro') ?><br/>
					<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
				</label>
			</div>

			<div class="cf"></div>

			<div class="description">
				<label for="<?php echo $this->get_field_id('style') ?>">
					<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
					<?php echo aq_field_input('style', $block_id, $style) ?>
				</label>
			</div>
			
			<?php

			do_action( $id_base . '_after_form' , $instance );
		}
		
		function block($instance) {
			extract($instance);

			$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
			$class = (!empty($class) ? ' '.esc_attr($class) : '');
			$style = (!empty($style) ? ' ' . esc_attr($style) : '');

			switch ($align) {
				case 'left':
					$alignclass = ' align-left';
					break;
				case 'center':
					$alignclass = ' align-center';
					break;
				case 'right':
					$alignclass = ' align-right';
					break;

			}

			$btnclass = 'cta-btn btn cta-btn-' . esc_attr($heading);

			switch ($btncolor) {
				case 'grey':
					$btnclass .= '';
					break;
				case 'blue':
					$btnclass .= ' btn-primary';
					break;
				case 'lightblue':
					$btnclass .= ' btn-info';
					break;
				case 'green':
					$btnclass .= ' btn-success';
					break;
				case 'yellow':
					$btnclass .= ' btn-warning';
					break;
				case 'red':
					$btnclass .= ' btn-danger';
					break;
				case 'black':
					$btnclass .= ' btn-inverse';
					break;
				
			}

			switch ($btnsize) {
				case 'default':
					$btnclass .= '';
					break;
				case 'large':
					$btnclass .= ' btn-large';
					break;
				case 'small':
					$btnclass .= ' btn-small';
					break;
				case 'mini':
					$btnclass .= ' btn-mini';
					break;	
			}
			
			$output = '<div'.$id.' class="cta well well-shadow'.$class.'" style="background: '.esc_attr($bgcolor).'; border-left: 3px solid '.esc_attr($bordercolor).'; '.$style.'">';

				$output .= '<div class="media">';
					// button
					$output .= '<a href="'.esc_url($btnlink).'" '.($btnlinkopen == 'new' ? 'target="_blank"' : '' ).'>';
							$output .= '<button class="'.$btnclass.' pull-right">'.($btnicon == 'none' ? '' : '<i class="'.$btnicon.($btncolor == 'grey' ? '' : ' icon-white').'"></i> ').esc_attr($btntext).'</button>';
						$output .= '</a>';

					$output .= '<div class="media-body">';
						$output .= '<'.$heading.' class="cta-heading-text" style="color: '.esc_attr($textcolor).';">';
								$output .= do_shortcode(mpt_content_kses(htmlspecialchars_decode($headline)));
						$output .= '</'.$heading.'>';
						$output .= '<p class="cta-headline" style="color: '.esc_attr($textcolor).';">';
							$output .= do_shortcode(mpt_content_kses(htmlspecialchars_decode($subheadline)));
						$output .= '</p>';
					$output .= '</div>';

				$output .= '</div>';

			$output .= '</div>'; // End - cta

			echo apply_filters( 'aq_cta_block_output' , $output , $instance );
		}
		
	}

/* AQ_Features_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Features_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('Features','pro'),
			'size' => 'span3',
		);
		
		//create the block
		parent::__construct('aq_features_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'title' => '',
			'heading' => 'h3',
			'text' => '',
			'align' => 'center',
			'bgcolor' => '#fff',
			'textcolor' => '#676767',
			'media' => '',
			'imagesize' => 'full',
			'imagetype' => 'none',
			'enablebtn' => '1',
			'btntext' => 'Learn More',
			'btnlink' => '',
			'btncolor' => 'black',
			'btnsize' => 'default',
			'btnlinkopen' => 'same',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$heading_style = array(
			'h1' => 'H1',
			'h2' => 'H2',
			'h3' => 'H3',
			'h4' => 'H4',
			'h5' => 'H5',
			'h6' => 'H6',
		);

		$align_options = array(
			'left' => __('Left','pro'),
			'center' => __('Center','pro'),
			'right' => __('Right','pro')
		);

		$imagetype_options = array(
			'none' => __('None','pro'),
			'rounded' => __('Rounded','pro'),
			'circle' => __('Circle','pro'),
			'polaroid' => __('Polaroid','pro')
		);

		$imagesize_options = array(
			'thumbnail' => __('Thumbnail','pro'),
			'medium' => __('Medium','pro'),
			'large' => __('Large','pro'),
			'full' => __('Full','pro'),
		);

		$btncolor_options = array(
			'grey' => __('Grey','pro'),
			'blue' => __('Blue','pro'),
			'lightblue' => __('Light Blue','pro'),
			'green' => __('Green','pro'),
			'red' => __('Red','pro'),
			'yellow' => __('Yellow','pro'),
			'black' => __('Black','pro'),
		);

		$btnsize_options = array(
			'default' => __('Default','pro'),
			'mini' => __('Mini','pro'),
			'small' => __('Small','pro'),
			'large' => __('Large','pro'),
			'block' => __('Block','pro'),
		);

		$btnlinkopen_options = array(
			'same' => __('Same Window','pro'),
			'new' => __('New Window','pro')
		);
		
		do_action( $id_base . '_before_form' , $instance );

		?>
		<div class="description two-third">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Title', 'pro') ?>
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('heading') ?>">
				<?php _e('Heading Style', 'pro') ?><br/>
				<?php echo aq_field_select('heading', $block_id, $heading_style, $heading); ?>
			</label>
		</div>

		<div class="cf"></div>
		
		<div class="description">
			<label for="<?php echo $this->get_field_id('media') ?>">
				<?php _e('Upload an Image', 'pro') ?>
				<?php echo aq_field_upload('media', $block_id, $media, 'image') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('imagesize') ?>">
				<?php _e('Image Size', 'pro') ?><br/>
				<?php echo aq_field_select('imagesize', $block_id, $imagesize_options, $imagesize); ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('imagetype') ?>">
				<?php _e('Image Type', 'pro') ?><br/>
				<?php echo aq_field_select('imagetype', $block_id, $imagetype_options, $imagetype); ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('text') ?>">
				<?php _e('Content', 'pro') ?>
				<?php echo aq_field_textarea('text', $block_id, $text, $size = 'full') ?>
			</label>
		</div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('enablebtn') ?>">
				<?php _e('Enable Button', 'pro') ?> <?php echo aq_field_checkbox('enablebtn', $block_id, $enablebtn); ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('btntext') ?>">
				<?php _e('Button Text', 'pro') ?>
				<?php echo aq_field_input('btntext', $block_id, $btntext, $size = 'full') ?>
			</label>
		</div>

		<div class="description fourth">
			<label for="<?php echo $this->get_field_id('btncolor') ?>">
				<?php _e('Button Color', 'pro') ?><br/>
				<?php echo aq_field_select('btncolor', $block_id, $btncolor_options, $btncolor); ?>
			</label>
		</div>

		<div class="description fourth last">
			<label for="<?php echo $this->get_field_id('btnsize') ?>">
				<?php _e('Button Size', 'pro') ?><br/>
				<?php echo aq_field_select('btnsize', $block_id, $btnsize_options, $btnsize); ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description two-third">
			<label for="<?php echo $this->get_field_id('btnlink') ?>">
				<?php _e('Button Link', 'pro') ?>
				<?php echo aq_field_input('btnlink', $block_id, $btnlink, $size = 'full') ?>
			</label>	
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('btnlinkopen') ?>">
				<?php _e('Link Open In', 'pro') ?><br/>
				<?php echo aq_field_select('btnlinkopen', $block_id, $btnlinkopen_options, $btnlinkopen); ?>
			</label>	
		</div>

		<div class="cf"></div>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('align') ?>">
				<?php _e('Align', 'pro') ?><br/>
				<?php echo aq_field_select('align', $block_id, $align_options, $align); ?>
			</label>
		</div>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('bgcolor') ?>">
				<?php _e('Pick a background color', 'pro') ?><br/>
				<?php echo aq_field_color_picker('bgcolor', $block_id, $bgcolor) ?>
			</label>
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('textcolor') ?>">
				<?php _e('Pick a text color', 'pro') ?><br/>
				<?php echo aq_field_color_picker('textcolor', $block_id, $textcolor) ?>
			</label>
		</div>

		<div class="cf"></div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>
		
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);

		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$userclass = (!empty($class) ? ' '.esc_attr($class) : '');
		$style = (!empty($style) ? esc_attr($style) : '');

		switch ($imagetype) {
			case 'none':
				$imageclass = '';
				break;
			case 'rounded':
				$imageclass = ' img-rounded';
				break;
			case 'circle':
				$imageclass = ' img-circle';
				break;
			case 'polaroid':
				$imageclass = ' img-polaroid';
				break;			
		}

		$imageid = get_image_id(esc_url($media));
		$image = wp_get_attachment_image_src( $imageid , $imagesize);

		$btnclass = ' btn';

		switch ($btncolor) {
			case 'grey':
				$btnclass .= '';
				break;
			case 'blue':
				$btnclass .= ' btn-primary';
				break;
			case 'lightblue':
				$btnclass .= ' btn-info';
				break;
			case 'green':
				$btnclass .= ' btn-success';
				break;
			case 'yellow':
				$btnclass .= ' btn-warning';
				break;
			case 'red':
				$btnclass .= ' btn-danger';
				break;
			case 'black':
				$btnclass .= ' btn-inverse';
				break;
			
		}

		switch ($btnsize) {
			case 'default':
				$btnclass .= '';
				break;
			case 'large':
				$btnclass .= ' btn-large';
				break;
			case 'small':
				$btnclass .= ' btn-small';
				break;
			case 'mini':
				$btnclass .= ' btn-mini';
				break;	
			case 'block':
				$btnclass .= ' btn-block';
				break;	
		}

		switch ($align) {
			case 'left':
				$alignclass = ' align-left';
				break;
			case 'center':
				$alignclass = ' align-center';
				break;
			case 'right':
				$alignclass = ' align-right';
				break;

		}

		$output = '<div'.$id.' class="features well well-shadow'.$alignclass.$userclass.'" style="background: '.$bgcolor.';color: '.$textcolor.';'.$style.'">';
			$output .= ( !empty( $image[0] ) ? '<img src="'.$image[0].'" class="features-block-image'.$imageclass.'" />' : '' );
			$output .= '<'.$heading.' class="features-block-title">'.strip_tags($title).'</'.$heading.'>';
			$output .= '<div class="features-block-text opacity8">'.wpautop(do_shortcode(mpt_content_kses(htmlspecialchars_decode($text)))).'</div>';

		if ($enablebtn == '1' ) {
			$output .= '<a href="'.esc_url($btnlink).'"'.($btnlinkopen == 'new' ? ' target="_blank"' : '').' class="features-block-btn-link">';
				$output .= '<button class="features-block-btn'.$btnclass.'">'.esc_attr($btntext).'</button>';
			$output .= '</a>';
		}

		$output .= '</div>';

		echo apply_filters( 'aq_features_block_output' , $output , $instance );
	}
	
}

/* AQ_Heading_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Heading_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('Heading Text','pro'),
			'size' => 'span6',
		);
		
		//create the block
		parent::__construct('aq_heading_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'title' => __('This is a heading text','pro'),
			'heading' => 'h1',
			'pageheader' => '0',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$heading_style = array(
			'h1' => 'H1',
			'h2' => 'H2',
			'h3' => 'H3',
			'h4' => 'H4',
			'h5' => 'H5',
			'h6' => 'H6',
		);

		do_action( $id_base . '_before_form' , $instance );
		
		?>
		<p class="description">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Heading Text', 'pro') ?>
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</p>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('heading') ?>">
				<?php _e('Heading Type', 'pro') ?><br/>
				<?php echo aq_field_select('heading', $block_id, $heading_style, $heading); ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('pageheader') ?>">
				<?php _e('Page Header?', 'pro') ?> <?php echo aq_field_checkbox('pageheader', $block_id, $pageheader); ?>
			</label>
		</div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<p class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</p>
		
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);

		$headingclass = '';

		if ($pageheader == '1') {
			$headingclass = 'page-header';
		}
		
		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$headingclass .= (!empty($class) ? ' '.esc_attr($class) : '');
		$style = (!empty($style) ? ' style="' . esc_attr($style).'"' : '');

		if (!empty($headingclass)) {
			$classoutput = ' class="'.$headingclass.'"';
		} else {
			$classoutput = '';
		}

		$output = '<'.$heading.$id.$classoutput.$style.'>'.strip_tags($title).'</'.$heading.'>';

		echo apply_filters( 'aq_heading_block_output' , $output , $instance );
	}
	
}

/* AQ_Image_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Image_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('Image','pro'),
			'size' => 'span6',
		);
		
		//create the block
		parent::__construct('aq_image_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'text' => '',
			'link' => '',
			'media' => '',
			'imagesize' => 'full',
			'disablelink' => 'no',
			'type' => 'none',
			'align' => 'none',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$align_options = array(
			'none' => __('None','pro'),
			'left' => __('Left','pro'),
			'center' => __('Center','pro'),
			'right' => __('Right','pro')
		);

		$imagetype_options = array(
			'none' => __('None','pro'),
			'rounded' => __('Rounded','pro'),
			'circle' => __('Circle','pro'),
			'polaroid' => __('Polaroid','pro')
		);

		$imagesize_options = array(
			'thumbnail' => __('Thumbnail','pro'),
			'medium' => __('Medium','pro'),
			'large' => __('Large','pro'),
			'full' => __('Full','pro'),
		);

		$yes_no_options = array(
			'yes' => __('Yes','pro'),
			'no' => __('No','pro'),
			);

		do_action( $id_base . '_before_form' , $instance );
		
		?>
		<div class="description">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Image Title (optional)', 'pro') ?><br />
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</div>
		
		<div class="description">
			<label for="<?php echo $this->get_field_id('media') ?>">
				<?php _e('Upload Your Image', 'pro') ?><br />
				<?php echo aq_field_upload('media', $block_id, $media, 'image') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description two-third">
			<label for="<?php echo $this->get_field_id('link') ?>">
				<?php _e('Link to Page / Post', 'pro') ?><br />
				<?php echo aq_field_input('link', $block_id, $link, $size = 'full') ?><br />
				<em style="font-size: 0.8em; padding-left: 5px;"><?php _e('Leave it blank if you want to link to image', 'pro') ?></em>
			</label>
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('disablelink') ?>">
				<?php _e('Disable link?', 'pro') ?><br />
				<?php echo aq_field_select('disablelink', $block_id, $yes_no_options, $disablelink); ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('imagesize') ?>">
				<?php _e('Image Size', 'pro') ?><br/>
				<?php echo aq_field_select('imagesize', $block_id, $imagesize_options, $imagesize); ?>
			</label>
		</div>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('type') ?>">
				<?php _e('Image Type', 'pro') ?><br/>
				<?php echo aq_field_select('type', $block_id, $imagetype_options, $type); ?>
			</label>
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('align') ?>">
				<?php _e('Align', 'pro') ?><br/>
				<?php echo aq_field_select('align', $block_id, $align_options, $align); ?>
			</label>
		</div>

		<div class="cf"></div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>
		
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);

		$classoutput = '';

		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$userclass = (!empty($class) ? esc_attr($class) : '');
		$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');

		switch ($type) {
			case 'none':
				$classoutput .= '';
				break;
			case 'rounded':
				$classoutput .= 'img-rounded ';
				break;
			case 'circle':
				$classoutput .= 'img-circle ';
				break;
			case 'polaroid':
				$classoutput .= 'img-polaroid ';
				break;			
		}

		switch ($align) {
			case 'none':
				$frontdiv = '';
				$enddiv = '';
				break;
			case 'left':
				$frontdiv = '<div class="align-left">';
				$enddiv = '</div>';
				break;
			case 'center':
				$frontdiv = '<div class="align-center">';
				$enddiv = '</div>';
				break;
			case 'right':
				$frontdiv = '<div class="align-right">';
				$enddiv = '</div>';
				break;

		}

		$classoutput .= $userclass;
		$imageid = get_image_id(esc_url($media));
		$fullimage = wp_get_attachment_image_src( $imageid , 'full');
		$image = wp_get_attachment_image_src( $imageid , $imagesize);
		$disablelink = ( !empty($disablelink) ? $disablelink : 'no' );

		$output = $frontdiv;
			$output .= ( $disablelink == 'yes' ? '' : (!empty($link) ? '<a href="'.esc_url($link).'">' : '<a href="'.$fullimage[0].'" rel="prettyPhoto[image-block]">') ); 
				$output .= '<img src="'.$image[0].'"'.$id.(!empty($classoutput) ? ' class="'.$classoutput.'"' : '').$style.' alt="'.esc_attr($title).'" />';
			$output .= ( $disablelink == 'yes' ? '' : '</a>' );
		$output .= $enddiv;

		echo apply_filters( 'aq_image_block_output' , $output , $instance );
	}
		
}

/* AQ_List_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_List_Block extends AQ_Block {

	function __construct() {
		$block_options = array(
			'name' => __('List','pro'),
			'size' => 'span6',
		);
		
		//create the widget
		parent::__construct('AQ_List_Block', $block_options);
		
		//add ajax functions
		add_action('wp_ajax_aq_block_list_add_new', array($this, 'add_list_item'));
		
	}
	
	function form($instance) {
	
		$defaults = array(
			'title' => '',
			'heading' => 'h4',
			'items' => array(
				1 => array(
					'title' => __('New Item','pro'),
					'content' => '',
					'icontype' => 'none',
					'iconcolor' => 'black',
				)
			),
			'type'	=> 'bullet',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		$list_type = array(
			'bullet' => __('Bullet','pro'),
			'number' => __('Number','pro'),
			'icon' => __('Icon','pro'),
			'unstyled' => __('Unstyled','pro'),
		);

		$heading_style = array(
			'h1' => 'H1',
			'h2' => 'H2',
			'h3' => 'H3',
			'h4' => 'H4',
			'h5' => 'H5',
			'h6' => 'H6',
		);

		do_action( 'mpt_start_session_hook' , 'aq_list_block' );

      	$_SESSION['session_aq_block_list_type'] = $type;

      	do_action( $id_base . '_before_form' , $instance );
		
		?>

		<div class="description two-third">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Title (optional)', 'pro') ?>
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('heading') ?>">
				<?php _e('Heading Type', 'pro') ?><br/>
				<?php echo aq_field_select('heading', $block_id, $heading_style, $heading); ?>
			</label>
		</div>

		<div class="description cf">
			<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
				<?php
				$items = is_array($items) ? $items : $defaults['items'];
				$count = 1;
				foreach($items as $item) {	
					$this->item($item, $count, $type);
					$count++;
				}
				?>
			</ul>
			<a href="#" rel="list" class="aq-sortable-add-new button"><?php _e('Add New', 'pro') ?></a>
		</div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('type') ?>">
				<?php _e('List Type', 'pro') ?><br/>
				<?php echo aq_field_select('type', $block_id, $list_type, $type) ?>
			</label>
		</div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>
		
		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>

		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function item($item = array(), $count = 0, $type = '') {

		$iconcolor_options = array(
			'black' => __('Black','pro'),
			'blue' => __('Blue','pro'),
			'green' => __('Green','pro'),
			'lightblue' => __('Light Blue','pro'),
			'red' => __('Red','pro'),
			'white' => __('White','pro'),
			'yellow' => __('Yellow','pro'),
		);

		global $propb_custom;

		?>
		<li id="sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">

			<?php do_action( 'aq_list_block_before_item_form' , $item , $count , $type ); ?>
			
			<div class="sortable-head cf">
				<div class="sortable-title">
					<strong><?php echo $item['title'] ?></strong>
				</div>
				<div class="sortable-handle">
					<a href="#"><?php _e('Open / Close', 'pro') ?></a>
				</div>
			</div>
			
			<div class="sortable-body">
				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-title">
						<?php _e('Item Name (won\'t publish)', 'pro') ?><br/>
						<input type="text" id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-title" class="input-full" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][title]" value="<?php echo $item['title'] ?>" />
					</label>
				</div>

				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-content">
						<?php _e('Item Content', 'pro') ?><br/>
						<textarea id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-content" class="textarea-full" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][content]" rows="5"><?php echo $item['content'] ?></textarea>
					</label>
				</div>	

				<?php $specialid = $this->get_field_id('items').'-'.$count; ?>

				<div class="tab-desc description half">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-icontype">
						<?php _e('Icon type', 'pro') ?><br/>
						<?php echo $propb_custom->load_icon_select_field( '' , '' , $item['icontype'] , $this->get_field_id('items') . '-' . $count . '-icontype' , $this->get_field_name('items') . '['.$count.'][icontype]' ); ?>
					</label>
				</div>

				<div class="tab-desc description half last">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-iconcolor">
						<?php _e('Icon Color', 'pro') ?><br/>
						<select id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-iconcolor" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][iconcolor]">
							<?php 
								foreach($iconcolor_options as $key=>$value) {
									echo '<option value="'.$key.'" '.selected( $item['iconcolor'] , $key, false ).'>'.htmlspecialchars($value).'</option>';
								}
							?>
						</select>
				</div>

				<p class="tab-desc description"><a href="#" class="sortable-delete"><?php _e('Delete', 'pro') ?></a></p>
			</div>

			<?php do_action( 'aq_list_block_after_item_form' , $item , $count , $type ); ?>
			
		</li>
		<?php
	}
	
	function block($instance) {
		extract($instance);
		
		$output = '';
		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$userclass = (!empty($class) ? ' '.esc_attr($class) : '');
		$style = (!empty($style) ? ' '.esc_attr($style) : '');

		$classoutput = '';

		switch ($type) {
			case 'bullet':
				$classoutput .= '';
				$liststyle = 'list-style-type:disc;';
				break;
			case 'number':
				$classoutput .= '';
				$liststyle = 'list-style-type:decimal;';
				break;	
			case 'icon':
				$classoutput .= 'unstyled';
				$liststyle = '';
				break;	
			case 'unstyled':
				$classoutput .= 'unstyled';
				$liststyle = 'list-style-type:none;';
				break;	
		}

		$classoutput .= $userclass;

		$output .= (!empty($title) ? '<'.$heading.'>'.esc_attr($title).'</'.$heading.'>' : '' );
		$output .= '<'.($type == 'number' ? 'ol' : 'ul').$id.(!empty($classoutput) ? ' class=" '.$classoutput.'"' : '').' style="'.$liststyle.$style.'">';

		if ($type == 'icon') {

			if (!empty($items)) {

				foreach( $items as $item ) {

					switch ($item['iconcolor']) {
						case 'blue':
							$tagcolor = ' icon-blue';
							break;
						case 'lightblue':
							$tagcolor = ' icon-lightblue';
							break;
						case 'green':
							$tagcolor = ' icon-green';
							break;
						case 'yellow':
							$tagcolor = ' icon-yellow';
							break;
						case 'red':
							$tagcolor = ' icon-red';
							break;
						case 'white':
							$tagcolor = ' icon-white';
							break;		
						case 'black':
							$tagcolor = '';
							break;			
					}

					$output .= '<li>';
					$output .= '<i class="'.$item['icontype'].$tagcolor.'"></i> ';
					$output .= do_shortcode(mpt_content_kses(htmlspecialchars_decode($item['content'])));
					$output .= '</li>';
				}
			}

		} else {

			if (!empty($items)) {

				foreach( $items as $item ) {
					
					$output .= '<li>';
					$output .= do_shortcode(mpt_content_kses(htmlspecialchars_decode($item['content'])));
					$output .= '</li>';
				}

			}

		}
		
		$output .= '</'.($type == 'number' ? 'ol' : 'ul').'>';
			
		echo apply_filters( 'aq_block_list_output' , $output , $instance );
		
	}
	
	/* AJAX add row */
	function add_list_item() {
		$nonce = $_POST['security'];	
		if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
		
		$count = isset($_POST['count']) ? absint($_POST['count']) : false;
		$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';

		do_action( 'mpt_start_session_hook' , 'aq_list_block_item' );

      	$type = isset( $_SESSION['session_aq_block_list_type'] ) ? esc_attr( $_SESSION['session_aq_block_list_type'] ) : 'bullet';
		
		//default key/value for the row
		$item = array(
			'title' => __('New Item','pro'),
			'content' => '',
			'icontype' => 'none',
			'iconcolor' => 'black',
		);
		
		if($count) {
			$this->item($item, $count, $type);
		} else {
			die(-1);
		}
		
		die();
	}
	
	function update($new_instance, $old_instance) {
		$new_instance = aq_recursive_sanitize($new_instance);
		return $new_instance;
	}
}

/* AQ_Map_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Map_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('Google Map','pro'),
			'size' => 'span6',
		);
		
		//create the block
		parent::__construct('aq_map_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'title' => '',
			'heading' => 'h3',
			'address' => '',
			'width' => '560',
			'height' => '280',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		$heading_style = array(
			'h1' => 'H1',
			'h2' => 'H2',
			'h3' => 'H3',
			'h4' => 'H4',
			'h5' => 'H5',
			'h6' => 'H6',
		);

		do_action( $id_base . '_before_form' , $instance );
		
		?>
		
		<div class="description two-third">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Title (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('title', $block_id, $title) ?>
			</label>
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('heading') ?>">
				<?php _e('Heading Type', 'pro') ?><br/>
				<?php echo aq_field_select('heading', $block_id, $heading_style, $heading); ?>
			</label>
		</div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('address') ?>">
				<?php _e('Address', 'pro') ?><br/>
				<?php echo aq_field_textarea('address', $block_id, $address, $size = 'full') ?>
			</label>
		</div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('width') ?>">
				<?php _e('Map Width', 'pro') ?><br/>
				<?php echo aq_field_input('width', $block_id, $width) ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('height') ?>">
				<?php _e('Map Height', 'pro') ?><br/>
				<?php echo aq_field_input('height', $block_id, $height) ?>
			</label>
		</div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>

		<?php

		do_action( $id_base . '_after_form' , $instance );
		
	}
	
	function block($instance) {
		extract($instance);

		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$userclass = (!empty($class) ? ' '.esc_attr($class) : '');
		$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');
		
		$output = '';

		$addresscode = str_replace(" ", "+", esc_attr($address));

		$output .= '<div'.$id.' class="google-map-block'.$userclass.'"'.$style.'>';
			$output .= (!empty($title) ? '<'.$heading.'>'.strip_tags($title).'</'.$heading.'>' : '');
			$output .= '<div class="well well-small">';
				$output .= '<img src="'.( is_ssl() ? 'https' : 'http' ).'://maps.google.com/maps/api/staticmap?size='.esc_attr($width).'x'.esc_attr($height).'&zoom=15&maptype=roadmap&markers=color:green|'.$addresscode.'&sensor=false" alt="Business Location" />';
			$output .= '</div>'; // End - well
		$output .= '</div>'; // End - google-map-block

		echo apply_filters( 'aq_map_block_output' , $output , $instance );
		
	}
	
}

/* AQ_Pricing_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Pricing_Block extends AQ_Block {

	function __construct() {
		$block_options = array(
			'name' => __('Pricing Package','pro'),
			'size' => 'span4',
		);
		
		//create the widget
		parent::__construct('AQ_Pricing_Block', $block_options);
		
		//add ajax functions
		add_action('wp_ajax_aq_block_pricing_add_new', array($this, 'add_item'));
		
	}
	
	function form($instance) {
	
		$defaults = array(
			'title' => __('New Package','pro'),
			'subtitle' => '',
			'items' => array(
				1 => array(
					'content' => __('New Item','pro'),
					'icontype' => 'none',
					'iconcolor' => 'black',
				)
			),
			'bgcolor' => '#fff',
			'textcolor'	=> '#676767',
			'featured' => '0',
			'price' => '$9.99',
			'btntext' => __('SIGN UP','pro'),
			'btnlink' => '',
			'btncolor' => 'black',
			'btnsize' => 'default',
			'btnlinkopen' => 'same',
			'usebuybtn' => 'no',
			'product_id' => 'none',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$btncolor_options = array(
			'grey' => __('Grey','pro'),
			'blue' => __('Blue','pro'),
			'lightblue' => __('Light Blue','pro'),
			'green' => __('Green','pro'),
			'red' => __('Red','pro'),
			'yellow' => __('Yellow','pro'),
			'black' => __('Black','pro'),
		);

		$btnsize_options = array(
			'default' => __('Default','pro'),
			'mini' => __('Mini','pro'),
			'small' => __('Small','pro'),
			'large' => __('Large','pro'),
			'block' => __('Block','pro'),
		);

		$btnlinkopen_options = array(
			'same' => __('Same Window','pro'),
			'new' => __('New Window','pro')
		);

		$yes_no_options = array(
			'yes' => __('Yes','pro'),
			'no' => __('No','pro')
		);

		$products_list = array( 'none' => __( 'None' , 'pro' ) );
		$products = get_posts( array('post_type' => 'product' , 'orderby' => 'title', 'order' => 'ASC' , 'numberposts' => -1) );

		if (!empty($products) && is_array($products)) {
			foreach ($products as $product) {
				$products_list[$product->ID] = $product->post_title;
			}
		}


		do_action( $id_base . '_before_form' , $instance );
		
		?>

		<div class="description">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Title', 'pro') ?>
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('subtitle') ?>">
				<?php _e('Subtitle', 'pro') ?>
				<?php echo aq_field_input('subtitle', $block_id, $subtitle, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description cf">
			<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
				<?php
				$items = is_array($items) ? $items : $defaults['items'];
				$count = 1;
				foreach($items as $item) {	
					$this->item($item, $count);
					$count++;
				}
				?>
			</ul>
			<p></p>
			<a href="#" rel="pricing" class="aq-sortable-add-new button"><?php _e('Add New', 'pro') ?></a>
			<p></p>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('featured') ?>">
				<?php _e('Featured?', 'pro') ?> <?php echo aq_field_checkbox('featured', $block_id, $featured); ?>
			</label>
		</div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('price') ?>">
				<?php _e('Price', 'pro') ?>
				<?php echo aq_field_input('price', $block_id, $price, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('btntext') ?>">
				<?php _e('Button Text', 'pro') ?>
				<?php echo aq_field_input('btntext', $block_id, $btntext, $size = 'full') ?>
			</label>
		</div>

		<div class="description fourth">
			<label for="<?php echo $this->get_field_id('btncolor') ?>">
				<?php _e('Button Color', 'pro') ?><br/>
				<?php echo aq_field_select('btncolor', $block_id, $btncolor_options, $btncolor); ?>
			</label>
		</div>

		<div class="description fourth last">
			<label for="<?php echo $this->get_field_id('btnsize') ?>">
				<?php _e('Button Size', 'pro') ?><br/>
				<?php echo aq_field_select('btnsize', $block_id, $btnsize_options, $btnsize); ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description two-third">
			<label for="<?php echo $this->get_field_id('btnlink') ?>">
				<?php _e('Button Link', 'pro') ?>
				<?php echo aq_field_input('btnlink', $block_id, $btnlink, $size = 'full') ?>
			</label>	
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('btnlinkopen') ?>">
				<?php _e('Link Open In', 'pro') ?><br/>
				<?php echo aq_field_select('btnlinkopen', $block_id, $btnlinkopen_options, $btnlinkopen); ?>
			</label>	
		</div>

		<div class="cf"></div>

		<?php if ( class_exists('MarketPress') ) { ?>

			<div class="description half">
				<label for="<?php echo $this->get_field_id('usebuybtn') ?>">
					<?php _e('Use Product Buy Button', 'pro') ?>
					<?php echo aq_field_select('usebuybtn', $block_id, $yes_no_options, $usebuybtn); ?>
				</label>	
			</div>

			<div class="description half last">
				<label for="<?php echo $this->get_field_id('product_id') ?>">
					<?php _e('Select Product', 'pro') ?><br/>
					<?php echo aq_field_select('product_id', $block_id, $products_list, $product_id); ?>
				</label>	
			</div>

			<div class="cf"></div>

		<?php } ?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('bgcolor') ?>">
				<?php _e('Pick a background color', 'pro') ?><br/>
				<?php echo aq_field_color_picker('bgcolor', $block_id, $bgcolor) ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('textcolor') ?>">
				<?php _e('Pick a text color', 'pro') ?><br/>
				<?php echo aq_field_color_picker('textcolor', $block_id, $textcolor) ?>
			</label>
		</div>

		<div class="cf"></div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function item($item = array(), $count = 0 ) {

		$iconcolor_options = array(
			'black' => __('Black','pro'),
			'white' => __('White','pro'),
		);

		global $propb_custom;

		?>
		<li id="sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">

			<?php do_action( 'aq_pricing_block_before_item_form' , $item , $count ); ?>
			
			<div class="sortable-head cf">
				<div class="sortable-title">
					<strong><?php echo $item['content'] ?></strong>
				</div>
				<div class="sortable-handle">
					<a href="#"><?php _e('Open / Close', 'pro') ?></a>
				</div>
			</div>
			
			<div class="sortable-body">
				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-content">
						<?php _e('Content', 'pro') ?><br/>
						<input type="text" id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-content" class="input-full" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][content]" value="<?php echo $item['content'] ?>" />
					</label>
				</div>

				<?php $specialid = $this->get_field_id('items').'-'.$count; ?>

				<div class="tab-desc description half">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-icontype">
						<?php _e('Icon type', 'pro') ?><br/>
						<?php echo $propb_custom->load_icon_select_field( '' , '' , $item['icontype'] , $this->get_field_id('items') . '-' . $count . '-icontype' , $this->get_field_name('items') . '['.$count.'][icontype]' ); ?>
					</label>
				</div>

				<div class="tab-desc description half last">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-iconcolor">
						<?php _e('Icon Color', 'pro') ?><br/>
						<select id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-iconcolor" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][iconcolor]">
							<?php 
								foreach($iconcolor_options as $key=>$value) {
									echo '<option value="'.$key.'" '.selected( $item['iconcolor'] , $key, false ).'>'.htmlspecialchars($value).'</option>';
								}
							?>
						</select>
				</div>

				<p class="tab-desc description"><a href="#" class="sortable-delete"><?php _e('Delete', 'pro') ?></a></p>
			</div>

			<?php do_action( 'aq_pricing_block_after_item_form' , $item , $count ); ?>
			
		</li>
		<?php
	}
	
	function block($instance) {
		extract($instance);
		
		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$class = (!empty($class) ? ' '. esc_attr($class) : '');
		$style = (!empty($style) ? ' '.esc_attr($style) : '');

		$usebuybtn = ( !empty($usebuybtn) && $usebuybtn == 'yes' ? true : false );
		$product_id = ( !empty($product_id) && $product_id != 'none' ? intval( $product_id ) : '' );
		$btntext = esc_attr($btntext);
		$btnclass = 'btn';

		switch ($btncolor) {
			case 'grey':
				$btnclass .= '';
				$buybtnclass = '';
				break;
			case 'blue':
				$btnclass .= ' btn-primary';
				$buybtnclass = ' mppf-btn-blue';
				break;
			case 'lightblue':
				$btnclass .= ' btn-info';
				$buybtnclass = ' mppf-btn-lightblue';
				break;
			case 'green':
				$btnclass .= ' btn-success';
				$buybtnclass = ' mppf-btn-green';
				break;
			case 'yellow':
				$btnclass .= ' btn-warning';
				$buybtnclass = ' mppf-btn-yellow';
				break;
			case 'red':
				$btnclass .= ' btn-danger';
				$buybtnclass = ' mppf-btn-red';
				break;
			case 'black':
			default:
				$btnclass .= ' btn-inverse';
				$buybtnclass = ' mppf-btn-black';
				break;
			
		}

		switch ($btnsize) {
			case 'default':
			default:
				$btnclass .= '';
				break;
			case 'large':
				$btnclass .= ' btn-large';
				$buybtnclass .= ' mppf-btn-large';
				break;
			case 'small':
				$btnclass .= ' btn-small';
				$buybtnclass .= ' mppf-btn-small';
				break;
			case 'mini':
				$btnclass .= ' btn-mini';
				$buybtnclass .= ' mppf-btn-mini';
				break;	
			case 'block':
				$btnclass .= ' btn-block';
				$buybtnclass .= ' mppf-btn-block';
				break;	
		}

		$output = '<div'.$id.' class="pricingtable'.($featured == '1' ? ' featured' : '').$class.'" style="background: '.$bgcolor.';color: '.$textcolor.';'.$style.'"">';
			$output .= '<h2 style="color: '.$textcolor.';">'.esc_attr($title);
				$output .= ( !empty($subtitle) ? '<br /><span>'.esc_attr($subtitle).'</span>' : '' );
			$output .= '</h2>';
			$output .= '<ul>';
				if (!empty($items)) {
					foreach( $items as $item ) {
						$output .= '<li>' . ( $item['icontype'] == 'none' ? '' : '<i class="'.$item['icontype'].($item['iconcolor'] == 'white' ? ' icon-white' : '').'"></i> ' ) . do_shortcode(mpt_content_kses(htmlspecialchars_decode($item['content']))) . '</li>';
					}
				}
			$output .= '</ul>';

			if ( $usebuybtn && !empty($product_id) && empty($price) ) {
				$output .= ( function_exists('pro_product_price') ? '<h4 class="pricing-table-price" style="color: '.$textcolor.';">' . pro_product_price( array(
						'echo' => false,
						'post_id' => $product_id,
						'label' => false,
						'context' => 'widget',
					) ) . '</h4>' : '' );
			} else {
				$output .= '<h4 class="pricing-table-price" style="color: '.$textcolor.';">'.esc_attr($price).'</h4>';
			}				

			if ( class_exists('MPPremiumFeatures') && $usebuybtn && !empty($product_id) ) {

				global $mppf;
				$output .= '<div class="btnclass'.( $btnsize == 'block' ? ' pricingtable-btn-block' : '' ).'">';
					$output .= '<div id="mp-premium-features">' . $mppf->load_buy_button( array(
	    						'post_id' => $product_id,
	    						'context' => 'list',
	    						'btnclass' => $buybtnclass,
	    						'showbtntext' => true,
								'addcartbtntext' => $btntext,
								'buynowbtntext' => $btntext,
		    				) ) . '</div>';
				$output .= '</div>';

			} elseif ( !empty($btnlink) ) {

				$output .= '<div class="btnclass">';
					$output .= '<a href="'.esc_url($btnlink).'"'.( $btnlinkopen == 'new' ? ' target="_blank"' : '' ).' class="'.$btnclass.'">'.$btntext.'</a>';
				$output .= '</div>';

			}

		$output .= '</div>';
			
		echo apply_filters( 'aq_block_pricing_output' , $output , $instance );
		
	}
	
	/* AJAX add item */
	function add_item() {
		$nonce = $_POST['security'];	
		if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
		
		$count = isset($_POST['count']) ? absint($_POST['count']) : false;
		$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';
		
		//default key/value for the item
		$item = array(
			'content' => __('New Item','pro'),
			'icontype' => 'none',
			'iconcolor' => 'black',
		);
		
		if($count) {
			$this->item($item, $count);
		} else {
			die(-1);
		}
		
		die();
	}
	
	function update($new_instance, $old_instance) {
		$new_instance = aq_recursive_sanitize($new_instance);
		return $new_instance;
	}
}

/* AQ_Progress_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Progress_Block extends AQ_Block {

	function __construct() {
		$block_options = array(
			'name' => __('Progress Bars','pro'),
			'size' => 'span6',
		);
		
		//create the widget
		parent::__construct('AQ_Progress_Block', $block_options);
		
		//add ajax functions
		add_action('wp_ajax_aq_block_bar_add_new', array($this, 'add_bar'));
		
	}
	
	function form($instance) {
	
		$defaults = array(
			'title' => '',
			'heading' => 'h4',
			'bars' => array(
				1 => array(
					'title' => __('New Bar','pro'),
					'width' => '80',
					'barcolor' => 'blue',
				)
			),
			'type' => 'basic',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$heading_style = array(
			'h1' => 'H1',
			'h2' => 'H2',
			'h3' => 'H3',
			'h4' => 'H4',
			'h5' => 'H5',
			'h6' => 'H6',
		);

		$type_options = array(
			'basic' => __('Basic','pro'),
			'striped' => __('Striped','pro'),
			'animated' => __('Animated','pro'),
			'stacked' => __('Stacked','pro'),
		);

		do_action( $id_base . '_before_form' , $instance );
		
		?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Title (optional)', 'pro') ?>
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('heading') ?>">
				<?php _e('Heading Type', 'pro') ?><br/>
				<?php echo aq_field_select('heading', $block_id, $heading_style, $heading); ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description cf">
			<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
				<?php
				$bars = is_array($bars) ? $bars : $defaults['bars'];
				$count = 1;
				foreach($bars as $bar) {	
					$this->bar($bar, $count);
					$count++;
				}
				?>
			</ul>
			<p></p>
			<a href="#" rel="bar" class="aq-sortable-add-new button"><?php _e('Add New', 'pro') ?></a>
			<p></p>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('type') ?>">
				<?php _e('Type', 'pro') ?><br/>
				<?php echo aq_field_select('type', $block_id, $type_options, $type) ?>
			</label>
		</div>

		<div class="cf"></div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>
		
		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function bar($bar = array(), $count = 0) {

		$barcolor_options = array(
			'blue' => __('Blue','pro'),
			'green' => __('Green','pro'),
			'yellow' => __('Yellow','pro'),
			'red' => __('Red','pro'),
		);

		?>
		<li id="sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">

			<?php do_action( 'aq_progress_block_before_item_form' , $bar , $count ); ?>
			
			<div class="sortable-head cf">
				<div class="sortable-title">
					<strong><?php echo $bar['title'] ?></strong>
				</div>
				<div class="sortable-handle">
					<a href="#"><?php _e('Open / Close', 'pro') ?></a>
				</div>
			</div>
			
			<div class="sortable-body">
				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('bars') ?>-<?php echo $count ?>-title">
						<?php _e('Name', 'pro') ?><br/>
						<input type="text" id="<?php echo $this->get_field_id('bars') ?>-<?php echo $count ?>-title" class="input-full" name="<?php echo $this->get_field_name('bars') ?>[<?php echo $count ?>][title]" value="<?php echo $bar['title'] ?>" />
					</label>
				</div>

				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('bars') ?>-<?php echo $count ?>-width">
						<?php _e('Width', 'pro') ?><br/>
						<input type="text" id="<?php echo $this->get_field_id('bars') ?>-<?php echo $count ?>-width" class="input-min" name="<?php echo $this->get_field_name('bars') ?>[<?php echo $count ?>][width]" value="<?php echo $bar['width'] ?>" /> %
					</label>
				</div>

				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('bars') ?>-<?php echo $count ?>-barcolor">
						<?php _e('Color', 'pro') ?><br/>
						<select id="<?php echo $this->get_field_id('bars') ?>-<?php echo $count ?>-barcolor" name="<?php echo $this->get_field_name('bars') ?>[<?php echo $count ?>][barcolor]">
							<?php 
								foreach($barcolor_options as $key=>$value) {
									echo '<option value="'.$key.'" '.selected($bar['barcolor'] , $key, false ).'>'.htmlspecialchars($value).'</option>';
								}
							?>
						</select>
					</label>
				</div>

				<p class="tab-desc description"><a href="#" class="sortable-delete"><?php _e('Delete', 'pro') ?></a></p>
			</div>

			<?php do_action( 'aq_progress_block_after_item_form' , $bar , $count ); ?>
			
		</li>
		<?php
	}
	
	function block($instance) {
		extract($instance);
		
		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$userclass = (!empty($class) ? ' '.esc_attr($class) : '');
		$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');

		$classoutput = '';

		switch ($type) {
			case 'basic':
				$classoutput .= 'progress';
				break;
			case 'striped':
				$classoutput .= 'progress progress-striped';
				break;
			case 'animated':
				$classoutput .= 'progress progress-striped active';
				break;
			case 'stacked':
				$classoutput .= 'progress';
				break;
		}


		$output = '<div'.$id.' class="well well-shadow'.$userclass.'"'.$style.'>';

			$output .= (!empty($title) ? '<'.$heading.'>'.strip_tags($title).'</'.$heading.'>' : '');

			if ($type == 'stacked') {

				if (!empty($bars)) {

					$output .= ( !empty($title) ? '<p>'.esc_attr($title).'</p>' : '');
					$output .= '<div class="'.$classoutput.'">';

					foreach( $bars as $bar ) {

						switch ($bar['barcolor']) {
							case 'blue':
								$barclass = ' bar-info';
								break;
							case 'green':
								$barclass = ' bar-success';
								break;
							case 'yellow':
								$barclass = ' bar-warning';
								break;
							case 'red':
								$barclass = ' bar-danger';
								break;							
						}

						$output .= '<div class="bar'.$barclass.'" style="width: '.esc_attr($bar['width']).'%"></div>';
					}

					$output .= '</div>';
				}

			} else {

				if (!empty($bars)) {

					foreach( $bars as $bar ) {

						switch ($bar['barcolor']) {
							case 'blue':
								$colorclass = ' progress-info';
								break;
							case 'green':
								$colorclass = ' progress-success';
								break;
							case 'yellow':
								$colorclass = ' progress-warning';
								break;
							case 'red':
								$colorclass = ' progress-danger';
								break;							
						}

						$output .= (!empty($bar['title']) ? '<p>'.esc_attr($bar['title']).'</p>' : '');
						$output .= '<div class="'.$classoutput.$colorclass.'">';
						$output .= '<div class="bar" style="width: '.esc_attr($bar['width']).'%"></div>';
						$output .= '</div>';
					}
				}
			
			}

		$output .= '</div>'; // End - well
			
		echo apply_filters( 'aq_block_bar_output' , $output , $instance );
		
	}
	
	/* AJAX add bar */
	function add_bar() {
		$nonce = $_POST['security'];	
		if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
		
		$count = isset($_POST['count']) ? absint($_POST['count']) : false;
		$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';
		
		//default key/value for the bar
		$bar = array(
			'title' => __('New Bar','pro'),
			'width' => '80',
			'barcolor' => 'blue',
		);
		
		if($count) {
			$this->bar($bar, $count);
		} else {
			die(-1);
		}
		
		die();
	}
	
	function update($new_instance, $old_instance) {
		$new_instance = aq_recursive_sanitize($new_instance);
		return $new_instance;
	}
}

/* AQ_Shortcode_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Shortcode_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('Shortcode Block','pro'),
			'size' => 'span6',
		);
		
		//create the block
		parent::__construct('aq_shortcode_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'title' => '',
			'shortcode' => '',
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		do_action( $id_base . '_before_form' , $instance );
		
		?>
		<div class="description">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Title (optional)', 'pro') ?>
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</div>
		
		<div class="description">
			<label for="<?php echo $this->get_field_id('shortcode') ?>">
				<?php _e('Shortcode', 'pro') ?>
				<?php echo aq_field_textarea('shortcode', $block_id, $shortcode, $size = 'full') ?><br />
				<small>( <?php _e('For a certain shortcode, double qoute might not work in this shortcode block. Please use single qoute instead.', 'pro') ?> )</small>
			</label>
		</div>
		
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);
		
		$output = do_shortcode( strip_tags( $shortcode ) );

		echo apply_filters( 'aq_shortcode_block_output' , $output , $instance );
	}
}

/* AQ_Staff_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Staff_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('Team Member','pro'),
			'size' => 'span3',
		);
		
		//create the block
		parent::__construct('aq_staff_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'title' => '',
			'position' => '',
			'media' => '',
			'text' => '',
			'fb' => '',
			'twitter' => '',
			'email' => '',
			'bgcolor' => '#F8F8F8',
			'textcolor' => '#676767',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		do_action( $id_base . '_before_form' , $instance );
		
		?>
		<div class="description half">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Name', 'pro') ?>
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('position') ?>">
				<?php _e('Title', 'pro') ?>
				<?php echo aq_field_input('position', $block_id, $position, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>
		
		<div class="description">
			<label for="<?php echo $this->get_field_id('media') ?>">
				<?php _e('Upload Photo', 'pro') ?>
				<?php echo aq_field_upload('media', $block_id, $media, 'image') ?>
				<em style="font-size: 0.8em; padding-left: 5px;"><?php _e('Recommended size: 360 x 270 pixel', 'pro') ?></em>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('text') ?>">
				<?php _e('Description', 'pro') ?>
				<?php echo aq_field_textarea('text', $block_id, $text, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('fb') ?>">
				<?php _e('Facebook Profile', 'pro') ?>
				<?php echo aq_field_input('fb', $block_id, $fb, $size = 'full') ?>
			</label>
		</div>

		<div class="description third">
			<label for="<?php echo $this->get_field_id('twitter') ?>">
				<?php _e('Twitter Profile', 'pro') ?>
				<?php echo aq_field_input('twitter', $block_id, $twitter, $size = 'full') ?>
			</label>
		</div>

		<div class="description third last">
			<label for="<?php echo $this->get_field_id('email') ?>">
				<?php _e('Email', 'pro') ?>
				<?php echo aq_field_input('email', $block_id, $email, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('bgcolor') ?>">
				<?php _e('Pick a background color', 'pro') ?><br/>
				<?php echo aq_field_color_picker('bgcolor', $block_id, $bgcolor) ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('textcolor') ?>">
				<?php _e('Pick a text color', 'pro') ?><br/>
				<?php echo aq_field_color_picker('textcolor', $block_id, $textcolor) ?>
			</label>
		</div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>
		
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);

		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$userclass = (!empty($class) ? ' '.esc_attr($class) : '');
		$style = (!empty($style) ? ' '.esc_attr($style) : '');

		$output = '<div id="staff-block">';

			$output .= '<div' . $id . ' class="well well-small' . $userclass . '" style="background: ' . $bgcolor . '; color: ' . $textcolor . ';' . $style . '">';

				if (!empty($media)) { 
					$attachid = get_image_id(esc_url($media));
					$output .= '<center>';
						$output .= wp_get_attachment_image($attachid , 'tb-360');
					$output .= '</center>';
				}

				$output .= '<div class="inner">';		
					
					$output .= '<h3 style="color: ' . $textcolor . ';">' . strip_tags($title) .' <small style="color: ' . $textcolor . ';">' . strip_tags($position) . '</small></h3>';

					$output .= wpautop( do_shortcode( mpt_content_kses( htmlspecialchars_decode($text) ) ) );
					
					$output .= '<center><div class="btn-group">';
				    
						if (!empty($fb)) { 
						   $output .= '<a href="' . esc_url($fb) . '" class="btn"><i class="icon-facebook-sign icon-large"></i></a>';
					    }
						   
						if (!empty($twitter)) { 
						    $output .= '<a href="' . esc_url($twitter) . '" class="btn"><i class="icon-twitter-sign icon-large"></i></a>';
					    }

					    if (!empty($email) && is_email($email) != 'false') { 
						    $output .= '<a href="mailto:' . is_email($email) . '" class="btn"><i class="icon-envelope icon-large"></i></a>';
					    }
			    	
			    	$output .= '</div></center>';

			    $output .= '</div>'; // end - inner

			$output .= '</div>'; // end - well

		$output .= '</div>'; // End - staff-block

		echo apply_filters( 'aq_staff_block_output' , $output , $instance );
	}
}

/* AQ_Table_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Table_Block extends AQ_Block {

	function __construct() {
		$block_options = array(
			'name' => __('Table','pro'),
			'size' => 'span12',
		);
		
		//create the widget
		parent::__construct('AQ_Table_Block', $block_options);
		
		//add ajax functions
		add_action('wp_ajax_aq_block_row_add_new', array($this, 'add_row'));
		
	}
	
	function form($instance) {
	
		$defaults = array(
			'rows' => array(
				1 => array(
					'title' => __('New Row','pro'),
					'rowcolor' => 'default',
					'column1' => '',
					'column2' => '',
					'column3' => '',
					'column4' => '',
					'column5' => '',
					'column6' => '',
					'column7' => '',
					'column8' => '',
					'column9' => '',
					'column10' => '',
					'column11' => '',
					'column12' => '',
				)
			),
			'columns' => '4',
			'types'	=> array('default'),
			'id' => '',
			'class' => '',
			'style' => ''
		);
		
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		$table_type = array(
			'default' => __('Default','pro'),
			'striped' => __('Striped','pro'),
			'bordered' => __('Bordered','pro'),
			'hover' => __('Hover','pro'),
			'condensed' => __('Condensed','pro')
		);

		$columns_per_row = array(
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12'
		);

   		do_action( 'mpt_start_session_hook' , 'aq_table_block' );

      	$_SESSION['session_aq_table_block_columns'] = $columns;

      	do_action( $id_base . '_before_form' , $instance );
		
		?>
		<div class="description cf">
			<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
				<?php
				$rows = is_array($rows) ? $rows : $defaults['rows'];
				$count = 1;
				foreach($rows as $row) {	
					$this->row($row, $count, $columns);
					$count++;
				}
				?>
			</ul>
			<p></p>
			<a href="#" rel="row" class="aq-sortable-add-new button"><?php _e('Add New', 'pro') ?></a>
			<p></p>
		</div>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('columns') ?>">
				<?php _e('Number of Columns (per row)', 'pro') ?><br/>
				<?php echo aq_field_select('columns', $block_id, $columns_per_row, $columns) ?>
			</label>
		</div>
		<div class="description half last">
			<label for="<?php echo $this->get_field_id('types') ?>">
				<?php _e('Table Type', 'pro') ?> <em style="font-size: 0.8em;"><?php _e('(CTRL + click to select multiple options)', 'pro') ?></em><br/>
				<?php echo aq_field_multiselect('types', $block_id, $table_type, $types) ?>
			</label>
		</div>

		<div class="cf"></div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function row( $row = array(), $count = 0, $columns = 4 ) {

		$color_options = array(
			'default' => __('Default','pro'),
			'green' => __('Green','pro'),
			'blue' => __('Blue','pro'),
			'red' => __('Red','pro'),
			'yellow' => __('Yellow','pro')
		);

		?>
		<li id="sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">

			<?php do_action( 'aq_table_block_before_item_form' , $row , $count , $columns ); ?>
			
			<div class="sortable-head cf">
				<div class="sortable-title">
					<strong><?php echo $row['title'] ?></strong>
				</div>
				<div class="sortable-handle">
					<a href="#"><?php _e('Open / Close', 'pro') ?></a>
				</div>
			</div>
			
			<div class="sortable-body">
				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('rows') ?>-<?php echo $count ?>-title">
						<?php _e('Row Name (won\'t publish)', 'pro') ?><br/>
						<input type="text" id="<?php echo $this->get_field_id('rows') ?>-<?php echo $count ?>-title" class="input-full" name="<?php echo $this->get_field_name('rows') ?>[<?php echo $count ?>][title]" value="<?php echo $row['title'] ?>" />
					</label>
				</div>

				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('rows') ?>-<?php echo $count ?>-rowcolor">
						<?php _e('Row Color', 'pro') ?><br/>
						<select id="<?php echo $this->get_field_id('rows') ?>-<?php echo $count ?>-rowcolor" name="<?php echo $this->get_field_name('rows') ?>[<?php echo $count ?>][rowcolor]">
							<?php 
								foreach($color_options as $key=>$value) {
									echo '<option value="'.$key.'" '.selected( $row['rowcolor'] , $key, false ).'>'.htmlspecialchars($value).'</option>';
								}
							?>
						</select>
						
					</label>
				</div>
				
				<div class="cf"></div>

				<?php for ($i=1; $i <= $columns; $i++) { ?>

				<p class="tab-desc description">
					<label for="<?php echo $this->get_field_id('rows') ?>-<?php echo $count ?>-column-<?php echo $i; ?>">
						<?php _e('Column', 'pro') ?> <?php echo $i; ?><br/>
						<input type="text" id="<?php echo $this->get_field_id('rows') ?>-<?php echo $count ?>-column-<?php echo $i; ?>" class="input-full" name="<?php echo $this->get_field_name('rows') ?>[<?php echo $count ?>][column<?php echo $i; ?>]" value="<?php echo $row['column'.$i] ?>" />
					</label>
				</p>

				<?php } ?>

				<p class="tab-desc description"><a href="#" class="sortable-delete"><?php _e('Delete', 'pro') ?></a></p>
			</div>

			<?php do_action( 'aq_table_block_after_item_form' , $row , $count , $columns ); ?>
			
		</li>
		<?php
	}
	
	function block($instance) {
		extract($instance);

		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$class = (!empty($class) ? 'table '. esc_attr($class) : 'table');
		$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');

		foreach( $types as $type ) {

			switch ($type) {
				case 'striped':
					$class .= ' table-striped';
					break;
				case 'bordered':
					$class .= ' table-bordered';
					break;	
				case 'hover':
					$class .= ' table-hover';
					break;	
				case 'condensed':
					$class .= ' table-condensed';
					break;	
			}

		}
		
		$output = '<table'.$id.' class="'.$class.'"'.$style.'>';
			
			foreach( $rows as $row ) {

				switch ($row['rowcolor']) {
					case 'default':
						$colorclass = '';
						break;

					case 'blue':
						$colorclass = ' class="info"';
						break;

					case 'green':
						$colorclass = ' class="success"';
						break;

					case 'yellow':
						$colorclass = ' class="warning"';
						break;

					case 'red':
						$colorclass = ' class="error"';
						break;
				
					default:
						$colorclass = '';
						break;
				}

				$output .= '<tr'.$colorclass.'>';

				for ($i=1; $i <= $columns; $i++) {
					$output .= '<td>';
					$output .= wpautop(do_shortcode(mpt_content_kses(htmlspecialchars_decode($row['column'.$i]))));
					$output .= '</td>';
				}

				$output .= '</tr>';
			}
		
		$output .= '</table>';
			
		echo apply_filters( 'aq_table_block_output' , $output , $instance );
		
	}
	
	/* AJAX add row */
	function add_row() {
		$nonce = $_POST['security'];	
		if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
		
		$count = isset($_POST['count']) ? absint($_POST['count']) : false;
		$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';

		$columns = isset( $_SESSION['session_aq_table_block_columns'] ) ? esc_attr( $_SESSION['session_aq_table_block_columns'] ) : 4;
		
		//default key/value for the row
		$row = array(
			'title' => __('New Row','pro'),
			'rowcolor' => 'default',
			'column1' => '',
			'column2' => '',
			'column3' => '',
			'column4' => '',
			'column5' => '',
			'column6' => '',
			'column7' => '',
			'column8' => '',
			'column9' => '',
			'column10' => '',
			'column11' => '',
			'column12' => '',
		);
		
		if($count) {
			$this->row( $row , $count , $columns );
		} else {
			die(-1);
		}
		
		die();
	}
	
	function update($new_instance, $old_instance) {
		$new_instance = aq_recursive_sanitize($new_instance);
		return $new_instance;
	}
}

/* AQ_Testimonials_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Testimonials_Block extends AQ_Block {

	function __construct() {
		$block_options = array(
			'name' => __('Testimonial Block','pro'),
			'size' => 'span4',
		);
		
		//create the widget
		parent::__construct('AQ_Testimonials_Block', $block_options);
		
		//add ajax functions
		add_action('wp_ajax_aq_block_test_add_new', array($this, 'add_test_item'));
		
	}
	
	function form($instance) {
	
		$defaults = array(
			'title' => __('What Our Clients Say','pro'),
			'items' => array(
				1 => array(
					'title' => __('New Testimonial','pro'),
					'content' => '',
					'position' => '',
					'link' => '',
					'photo' => '',
				)
			),
			'speed' => '4000',
			'pause' => 'yes',
			'bgcolor' => '#f1f1f1',
			'textcolor' => '#676767',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$pause_options = array(
			'yes' => __('Yes','pro'),
			'no' => __('No','pro'),
		);

		do_action( $id_base . '_before_form' , $instance );
		
		?>

		<div class="description">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Title', 'pro') ?>
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description cf">
			<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
				<?php
				$items = is_array($items) ? $items : $defaults['items'];
				$count = 1;
				foreach($items as $item) {	
					$this->item($item, $count);
					$count++;
				}
				?>
			</ul>
			<p></p>
			<a href="#" rel="test" class="aq-sortable-add-new button"><?php _e('Add New', 'pro') ?></a>
			<p></p>
		</div>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('speed') ?>">
				<?php _e('interval (in millisecond)', 'pro') ?><br />
				<?php echo aq_field_input('speed', $block_id, $speed, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('pause') ?>">
				<?php _e('Pause on hover?', 'pro') ?><br />
				<?php echo aq_field_select('pause', $block_id, $pause_options, $pause); ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('bgcolor') ?>">
				<?php _e('Pick a background color', 'pro') ?><br/>
				<?php echo aq_field_color_picker('bgcolor', $block_id, $bgcolor) ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('textcolor') ?>">
				<?php _e('Pick a text color', 'pro') ?><br/>
				<?php echo aq_field_color_picker('textcolor', $block_id, $textcolor) ?>
			</label>
		</div>

		<div class="cf"></div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>
		
		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function item($item = array(), $count = 0) {

		?>
		<li id="sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">

			<?php do_action( 'aq_test_block_before_item_form' , $item , $count ); ?>
			
			<div class="sortable-head cf">
				<div class="sortable-title">
					<strong><?php echo $item['title'] ?></strong>
				</div>
				<div class="sortable-handle">
					<a href="#"><?php _e('Open / Close', 'pro') ?></a>
				</div>
			</div>
			
			<div class="sortable-body">
				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-title">
						<?php _e('Name', 'pro') ?><br/>
						<input type="text" id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-title" class="input-full" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][title]" value="<?php echo $item['title'] ?>" />
					</label>
				</div>

				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-position">
						<?php _e('Position', 'pro') ?><br/>
						<input type="text" id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-position" class="input-full" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][position]" value="<?php echo $item['position'] ?>" />
					</label>
				</div>

				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-photo">
						<?php _e('Photo', 'pro') ?> <em style="font-size: 0.8em;"><?php _e('(Recommended size: 50 x 50 pixel)', 'pro') ?></em><br/>
						<input type="text" id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-photo" class="input-full input-upload" value="<?php echo $item['photo'] ?>" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][photo]">
						<a href="#" class="aq_upload_button button" rel="image"><?php _e('Upload', 'pro') ?></a><p></p>
					</label>
				</div>

				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-link">
						<?php _e('Website', 'pro') ?><br/>
						<input type="text" id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-link" class="input-full" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][link]" value="<?php echo $item['link'] ?>" />
					</label>
				</div>

				<div class="tab-desc description">
					<label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-content">
						<?php _e('Testimonial', 'pro') ?><br/>
						<textarea id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-content" class="textarea-full" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][content]" rows="5"><?php echo $item['content'] ?></textarea>
					</label>
				</div>

				<p class="tab-desc description"><a href="#" class="sortable-delete"><?php _e('Delete', 'pro') ?></a></p>
			</div>

			<?php do_action( 'aq_test_block_after_item_form' , $item , $count ); ?>
			
		</li>
		<?php
	}
	
	function block($instance) {
		extract($instance);
		
		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$class = (!empty($class) ? ' '.esc_attr($class) : '');
		$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');

		$output = '<div'.$id.' class="well well-small well-shadow'.$class.'"'.$style.'>';

		$output .= (!empty($title) ? '<h5 class="page-header" style="color: '.$textcolor.';border-bottom: 1px dashed '.$textcolor.'">'.esc_attr($title).'</h5>' : '' );

		$output .= '<div id="testimonialsCarousel-'.$block_id.'" class="carousel testimonials carousel-fade slide" style="margin-bottom: 0px;">';
			$output .= '<div class="carousel-inner">';

				if (!empty($items)) {

					$i = 1;

					foreach( $items as $item ) {

						$output .= '<div class="'.($i == 1 ? 'active ' : '').'item">';

							$output .= '<p><em style="font-size: 0.9em;line-height: 1.5em;opacity: 0.8;">';
							$output .= do_shortcode(mpt_content_kses(htmlspecialchars_decode($item['content'])));
							$output .= '</em></p>';
							$output .= (!empty($item['photo']) ? '<img src="'.esc_url($item['photo']).'" class="img-circle pull-left" style="margin-right: 10px;" />' : '');
							$output .= '<h4>';
								$output .= (!empty($item['link']) ? '<a href="'.esc_url($item['link']).'" target="_blank">' : '');
									$output .= do_shortcode(strip_tags($item['title']));
								$output .= (!empty($item['link']) ? '</a>' : '');
								$output .= (!empty($item['position']) ? '<br /><small style="font-size: 0.8em;opacity: 0.8;">' : '');
									$output .= do_shortcode(strip_tags($item['position']));
								$output .= (!empty($item['position']) ? '</small>' : '');
							$output .= '</h4>';

						$output .= '</div>'; // End -item
						$i++;
					}
				}
			
			$output .= '</div>'; // End - carousel-inner
		$output .= '</div>'; // End - testimonials

		$output .= '<div class="clear"></div>';
		$output .= '<div class="pull-right">';
		$output .= '<a class="fade-in-effect" href="#testimonialsCarousel-'.$block_id.'" data-slide="prev"><i class="icon-chevron-left"></i></a>';
		$output .= '<a class="fade-in-effect" href="#testimonialsCarousel-'.$block_id.'" data-slide="next"><i class="icon-chevron-right"></i></a>';
		$output .= '</div> ';
		$output .= '<div class="clear"></div>';

		$output .= '</div>';
		$output .= '<script type="text/javascript">jQuery(document).ready(function () {jQuery(".testimonials").carousel({interval: ' . esc_attr($speed) . '' . ($pause == 'yes' ? ',pause: "hover"' : '') . '})});</script>';

		echo apply_filters( 'aq_test_block_output' , $output , $instance );
		
	}
	
	/* AJAX add testimonial */
	function add_test_item() {
		$nonce = $_POST['security'];	
		if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
		
		$count = isset($_POST['count']) ? absint($_POST['count']) : false;
		$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';
		
		//default key/value for the testimonial
		$item = array(
			'title' => __('New Testimonial','pro'),
			'content' => '',
			'position' => '',
			'link' => '',
			'photo' => '',
		);
		
		if($count) {
			$this->item($item, $count);
		} else {
			die(-1);
		}
		
		die();
	}
	
	function update($new_instance, $old_instance) {
		$new_instance = aq_recursive_sanitize($new_instance);
		return $new_instance;
	}
}

/* AQ_Video_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Video_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('Video Box','pro'),
			'size' => 'span6',
		);
		
		//create the block
		parent::__construct('aq_video_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'title' => '',
			'heading' => 'h4',
			'height' => '260',
			'video' => '',
			'type' => 'youtube',
			'id' => '',
			'class' => '',
			'style' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		$videotype = array(
			'youtube' => 'Youtube',
			'vimeo' => 'Vimeo',
		);

		$heading_style = array(
			'h1' => 'H1',
			'h2' => 'H2',
			'h3' => 'H3',
			'h4' => 'H4',
			'h5' => 'H5',
			'h6' => 'H6',
		);

		do_action( $id_base . '_before_form' , $instance );
		
		?>
		<div class="description half">
			<label for="<?php echo $this->get_field_id('title') ?>">
				<?php _e('Title (optional)', 'pro') ?>
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('heading') ?>">
				<?php _e('Heading Type', 'pro') ?><br/>
				<?php echo aq_field_select('heading', $block_id, $heading_style, $heading); ?>
			</label>
		</div>

		<div class="cf"></div>
		
		<div class="description">
			<label for="<?php echo $this->get_field_id('video') ?>">
				<?php _e('Video URL', 'pro') ?>
				<?php echo aq_field_input('video', $block_id, $video, $size = 'full') ?>
				<em style="font-size: 0.8em; padding-left: 5px;">(<?php _e('example: ', 'pro') ?><code><?php echo ( is_ssl() ? 'https' : 'http' ); ?>://vimeo.com/51333291</code> / <code><?php echo ( is_ssl() ? 'https' : 'http' ); ?>://youtu.be/iOiE6XMy0y8</code>)</em>
			</label>
		</div>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('type') ?>">
				<?php _e('Video Type', 'pro') ?><br/>
				<?php echo aq_field_select('type', $block_id, $videotype, $type); ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('height') ?>">
				<?php _e('Video Height', 'pro') ?>
				<?php echo aq_field_input('height', $block_id, $height, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<?php do_action( $id_base . '_before_advance_settings' , $instance ); ?>

		<div class="description half">
			<label for="<?php echo $this->get_field_id('id') ?>">
				<?php _e('id (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('id', $block_id, $id, $size = 'full') ?>
			</label>
		</div>

		<div class="description half last">
			<label for="<?php echo $this->get_field_id('class') ?>">
				<?php _e('class (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('class', $block_id, $class, $size = 'full') ?>
			</label>
		</div>

		<div class="cf"></div>

		<div class="description">
			<label for="<?php echo $this->get_field_id('style') ?>">
				<?php _e('Additional inline css styling (optional)', 'pro') ?><br/>
				<?php echo aq_field_input('style', $block_id, $style) ?>
			</label>
		</div>
		
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);
		
		$id = (!empty($id) ? ' id="'.esc_attr($id).'"' : '');
		$class = (!empty($class) ? ' '. esc_attr($class) : '');
		$style = (!empty($style) ? ' style="'.esc_attr($style).'"' : '');

		$video = esc_url($video);

		switch ($type) {
			case 'youtube':
				$youtube = array(
					"http://youtu.be/",
					"http://www.youtube.com/watch?v=",
					"http://www.youtube.com/embed/",
					"https://youtu.be/",
					"https://www.youtube.com/watch?v=",
					"https://www.youtube.com/embed/",
					);
				$videonum = str_replace($youtube, "", $video);
				$videocode = ( is_ssl() ? 'https' : 'http' ) . '://www.youtube.com/embed/' . $videonum;
				break;
			case 'vimeo':
				$vimeo = array(
					"http://vimeo.com/",
					"http://player.vimeo.com/video/",
					"https://vimeo.com/",
					"https://player.vimeo.com/video/"
					);
				$videonum = str_replace($vimeo, "", $video);
				$videocode = ( is_ssl() ? 'https' : 'http' ) . '://player.vimeo.com/video/' . $videonum;
				break;
		}

		$output = (!empty($title) ? '<'.$heading.'>'.esc_attr($title).'</'.$heading.'>' : '' );;
		$output .= '<div'.$id.' class="video-box'.$class.'"'.$style.'>';
			$output .= '<iframe src="'.$videocode.'" width="100%" height="'.esc_attr($height).'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		$output .= '</div>';

		echo apply_filters( 'aq_video_block_output' , $output , $instance );
	}
	
}

/* AQ_Well_Block
----------------------------------------------------------------------------------------------------------- */

class AQ_Well_Block extends AQ_Block {
	
	/* PHP5 constructor */
	function __construct() {
		
		$block_options = array(
			'name' => __('Well Box','pro'),
			'size' => 'span6',
		);
		
		//create the widget
		parent::__construct('aq_well_block', $block_options);
		
	}

	function form($instance) {
		echo '<p class="empty-column">',
		__('Drag block items into this well box', 'pro'),
		'</p>';
		echo '<ul class="blocks column-blocks cf"></ul>';
	}
	
	function form_callback($instance = array()) {
		$instance = is_array($instance) ? wp_parse_args($instance, $this->block_options) : $this->block_options;
		
		//insert the dynamic block_id & block_saving_id into the array
		$this->block_id = 'aq_block_' . $instance['number'];
		$instance['block_saving_id'] = 'aq_blocks[aq_block_'. $instance['number'] .']';
		
		extract($instance);
		
		$col_order = $order;
		
		//column block header
		if(isset($template_id)) {
			echo '<li id="template-block-'.$number.'" class="block block-aq_column_block '.$size.'">',
					'<div class="block-settings-column cf" id="block-settings-'.$number.'">',
						'<p class="empty-column">',
							__('Drag block items into this Well box', 'pro'),
						'</p>',
						'<ul class="blocks column-blocks cf">';
					
			//check if column has blocks inside it
			$blocks = aq_get_blocks($template_id);
			
			//outputs the blocks
			if($blocks) {
				foreach($blocks as $key => $child) {
					global $aq_registered_blocks;
					extract($child);
					
					//get the block object
					$block = $aq_registered_blocks[$id_base];
					
					if($parent == $col_order) {
						$block->form_callback($child);
					}
				}
			} 
			echo 		'</ul>';
			
		} else {
			
	 		$title = $title ? '<span class="in-block-title"> : '.$title.'</span>' : '';
	 		$resizable = $resizable ? '' : 'not-resizable';
	 		
	 		echo '<li id="template-block-'.$number.'" class="block block-aq_column_block '. $size .' '.$resizable.'">',
	 				'<dl class="block-bar">',
	 					'<dt class="block-handle">',
	 						'<div class="block-title">',
	 							$name , $title, 
	 						'</div>',
	 						'<span class="block-controls">',
	 							'<a class="block-edit" id="edit-'.$number.'" title="Edit Block" href="#block-settings-'.$number.'">Edit Block</a>',
	 						'</span>',
	 					'</dt>',
	 				'</dl>',
	 				'<div class="block-settings cf" id="block-settings-'.$number.'">';

			$this->form($instance);
		}
				
		//form footer
		$this->after_form($instance);
	}
	
	//form footer
	function after_form($instance) {
		extract($instance);
		
		$block_saving_id = 'aq_blocks[aq_block_'.$number.']';
 		$userclass = !empty($userclass) ? esc_attr($userclass) : '';
 		$style = !empty($style) ? esc_attr($style) : '';

			echo '<div class="cf" style="height: 20px"></div>';
			echo 'Class (optional)<br/><input type="text" class="widefat" name="'.$this->get_field_name('userclass').'" value="'.$userclass.'" />';
			echo '<div class="cf" style="height: 10px"></div>';
			echo 'Additional inline css styling (optional)<br/><input type="text" class="widefat" name="'.$this->get_field_name('style').'" value="'.$style.'" />';
			echo '<div class="cf" style="height: 10px"></div>';
			echo '<div class="block-control-actions cf"><a href="#" class="delete">Delete</a></div>';
			echo '<input type="hidden" class="id_base" name="'.$this->get_field_name('id_base').'" value="'.$id_base.'" />';
			echo '<input type="hidden" class="name" name="'.$this->get_field_name('name').'" value="'.$name.'" />';
			echo '<input type="hidden" class="order" name="'.$this->get_field_name('order').'" value="'.$order.'" />';
			echo '<input type="hidden" class="size" name="'.$this->get_field_name('size').'" value="'.$size.'" />';
			echo '<input type="hidden" class="parent" name="'.$this->get_field_name('parent').'" value="'.$parent.'" />';
			echo '<input type="hidden" class="number" name="'.$this->get_field_name('number').'" value="'.$number.'" />';
		echo '</div>',
			'</li>';
	}
	
	function block_callback($instance) {
		$instance = is_array($instance) ? wp_parse_args($instance, $this->block_options) : $this->block_options;
		
		extract($instance);
		
		$col_order = $order;
		$col_size = absint(preg_replace("/[^0-9]/", '', $size));
		
		//column block header
		if(isset($template_id)) {

	 		$column_class = $first ? 'aq-first' : '';

	 		$userclass = (!empty($userclass) ? ' ' . esc_attr($userclass).'"' : '');
	 		$style = (!empty($style) ? ' style="' . esc_attr($style).'"' : '');
	 		
	 		echo '<div id="aq-block-'.$number.'" class="aq-block aq-block-'.$id_base.' '.$size.' '.$column_class.' well cf'.$userclass.'"'.$style.'>';
			
			//define vars
			$overgrid = 0; $span = 0; $first = false;
			
			//check if column has blocks inside it
			$blocks = aq_get_blocks($template_id);
			
			//outputs the blocks
			if($blocks) {
				foreach($blocks as $key => $child) {
					global $aq_registered_blocks;
					extract($child);
					
					if(class_exists($id_base)) {
						//get the block object
						$block = $aq_registered_blocks[$id_base];
						
						//insert template_id into $child
						$child['template_id'] = $template_id;
						
						//display the block
						if($parent == $col_order) {
							
							$child_col_size = absint(preg_replace("/[^0-9]/", '', $size));
							
							$overgrid = $span + $child_col_size;
							
							if($overgrid > $col_size || $span == $col_size || $span == 0) {
								$span = 0;
								$first = true;
							}
							
							if($first == true) {
								$child['first'] = true;
							}
							
							$block->block_callback($child);
							
							$span = $span + $child_col_size;
							
							$overgrid = 0; //reset $overgrid
							$first = false; //reset $first
						}
					}
				}
			}

			echo '<div class="clear"></div>';
			echo '</div>'; 
			
			$this->after_block($instance);
			
		} else {
			//show nothing
		}
	}
	
}

/* AQ_Revslider_Block
----------------------------------------------------------------------------------------------------------- */

if ( class_exists('RevSlider_Widget') ) {
	class AQ_Revslider_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => __('Revolution Slider','pro'),
				'size' => 'span12',
			);
			
			//create the block
			parent::__construct('aq_revslider_block', $block_options);
		}
		
		function form($instance) {
			
			$slider = new RevSlider();
	    	$arrSliders = $slider->getArrSlidersShort();
	    	
	    	$sliderID = UniteFunctionsRev::getVal($instance, "rev_slider");
	    	
			if(empty($arrSliders))
				echo __('No sliders found, Please create a slider' , 'pro');
			else{
				$field = "rev_slider";
				$fieldID = $this->get_field_id( $field );
				$fieldName = $this->get_field_name( $field );

				$select = UniteFunctionsRev::getHTMLSelect($arrSliders,$sliderID,'name="'.$fieldName.'" id="'.$fieldID.'"',true);
			}
			echo __('Choose slider: ','pro');
			echo $select;
		}
		
		function block($instance) {
			$sliderID = $instance["rev_slider"];
			
			if(empty($sliderID))
				return(false);
				
			RevSliderOutput::putSlider($sliderID);
		}

	    function update($new_instance, $old_instance) {
	    	
	        return($new_instance);
	    }
		
	}
}