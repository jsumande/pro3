<?php

/**
 * Gallery Class
 * @author Nathan Onn <nathan@smashingadvantage.com>
 * @package sat-framework
 */

if ( !class_exists('SATFW_Gallery') ) {

	class SATFW_Gallery {

		var $func_hook_prefix;

		/* Install shortcodes
		------------------------------------------------------------------------------------------------------------------- */

		function __construct() {
			
			$this->func_hook_prefix = 'satfw_gallery_';
			
			add_action( 'after_setup_theme' , array(&$this, 'add_html5_support') );
			add_filter( 'post_gallery' , array(&$this, 'load_gallery') , 10 , 2 );

		}

		/* Add HTML5 support to gallery
		------------------------------------------------------------------------------------------------------------------- */

	    function add_html5_support() {
			add_theme_support( 'html5', array( 'gallery', 'caption' ) );
	    }

		/* Gallery shortcode
		------------------------------------------------------------------------------------------------------------------- */

	    function load_gallery( $contents = '' , $attr = array() ) {

	    	$post = get_post();

			static $instance = 0;
			$instance++;

			// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
			if ( isset( $attr['orderby'] ) ) {
				$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
				if ( !$attr['orderby'] )
					unset( $attr['orderby'] );
			}

			extract( shortcode_atts( array(
				'order'      => 'ASC',
				'orderby'    => 'menu_order ID',
				'id'         => $post ? $post->ID : 0,
				'itemtag'    => 'figure',
				'icontag'    => 'div',
				'captiontag' => 'figcaption',
				'columns'    => 3,
				'size'       => 'tb-600',
				'mode'		 => 'default',
				'include'    => '',
				'exclude'    => '',
				'link'       => '',
				'carouselspeed' => '5000',
				'carouselpause' => 'hover',
				'carouselloop' => 'true',
			) , $attr , 'gallery') );

			$id = intval($id);
			if ( 'RAND' == $order )
				$orderby = 'none';

			if ( !empty($include) ) {
				$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

				$attachments = array();
				foreach ( $_attachments as $key => $val ) {
					$attachments[$val->ID] = $_attachments[$key];
				}
			} elseif ( !empty($exclude) ) {
				$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
			} else {
				$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
			}

			if ( empty($attachments) )
				return 'def' . $ids;

			if ( is_feed() ) {
				$output = "\n";
				foreach ( $attachments as $att_id => $attachment )
					$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
				return $output;
			}

			$itemtag = tag_escape($itemtag);
			$captiontag = tag_escape($captiontag);
			$icontag = tag_escape($icontag);
			$valid_tags = wp_kses_allowed_html( 'post' );
			if ( ! isset( $valid_tags[ $itemtag ] ) )
				$itemtag = 'dl';
			if ( ! isset( $valid_tags[ $captiontag ] ) )
				$captiontag = 'dd';
			if ( ! isset( $valid_tags[ $icontag ] ) )
				$icontag = 'dt';

			$columns = intval($columns);
			$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
			$float = is_rtl() ? 'right' : 'left';

			$selector = 'gallery-' . $instance;

			$size_class = sanitize_html_class( $size );

			if ( $mode == 'carousel' ) {

				$output = $this->load_gallery_in_carousel( array(
					'selector' => $selector,
					'post_id' => $id,
					'columns' => $columns,
					'size_class' => $size_class,
					'attachments' => $attachments,
					'link' => $link,
					'size' => $size,
					'itemtag'    => $itemtag,
					'icontag'    => $icontag,
					'captiontag' => $captiontag,
					'carouselspeed' => $carouselspeed,
					'carouselpause' => $carouselpause,
					'carouselloop' => $carouselloop,
				) );

			} else {

				$output = '<div id='.$selector.' class="gallery sat-gallery sat-container galleryid-'.$id.' gallery-columns-'.$columns.' gallery-size-'.$size_class.'">';
				$galleryid = 'gallery-'.$id;

				foreach ( $attachments as $id => $attachment ) {
					$output .= $this->load_gallery_item( array( 
								'galleryid' => $galleryid,
								'id' => $id,
								'attachment' => $attachment,
								'link' => $link,
								'size' => $size,
								'columns' => $columns,
								'itemtag'    => $itemtag,
								'icontag'    => $icontag,
								'captiontag' => $captiontag,
							) );
				}

				$output .= '</div>'; // end - sat-gallery
				$output .= '<div class="clear"></div>';

				$randnum = rand( 1 , 9999 );
				$output .= '
					<script type="text/javascript">
						jQuery(document).ready(function () {
							var conTaiNer'.$randnum.' = jQuery("#'.$selector.'");
					    	conTaiNer'.$randnum.'.imagesLoaded( function(){
							  	conTaiNer'.$randnum.'.masonry({
							    	itemSelector : ".gallery-item"
							  	});
							});
						});
					</script>';

			}

	    	return apply_filters( $this->func_hook_prefix . 'load_gallery' , $output , $attr );

	    }

	    function load_gallery_item( $args = array() ) {

			$defaults = array(
				'galleryid' => 'gallery-1',
				'id' => NULL,
				'attachment' => '',
				'link'       => '',
				'size'       => 'tb-600',
				'columns'    => 3,
				'itemtag'    => 'figure',
				'icontag'    => 'div',
				'captiontag' => 'figcaption',
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			if ( NULL === $id )
				return;

			if ( ! empty( $link ) && 'file' === $link ) {
				$image_link = wp_get_attachment_url( $id );
				$image_output = '<div class="sat-image-box add-bouncein-effects-parent">';
					$image_output .= wp_get_attachment_image( $id, $size, false );
					$image_output .= '<a href="'.$image_link.'" rel="prettyPhoto['.$galleryid.']" class="sat-image-box-link "><i class="fa fa-search-plus add-bouncein-effects-child"></i></a>';
				$image_output .= '</div>'; // end - gallery-image-container
			} elseif ( ! empty( $link ) && 'none' === $link ) {
				$image_output = wp_get_attachment_image( $id, $size, false );
			} else {
				$image_output = wp_get_attachment_link( $id, $size, true , false );
			}

			$image_meta  = wp_get_attachment_metadata( $id );

			$orientation = '';
			if ( isset( $image_meta['height'], $image_meta['width'] ) )
				$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';

			$output = '<'.$itemtag.' class="gallery-item sat-item sat-item-'.$columns.'x">';

				$output .= '
					<'.$icontag.' class="gallery-icon '.$orientation.'">
						' . $image_output . '
					</'.$icontag.'>';

				if ( $captiontag && trim($attachment->post_excerpt) ) {
					$output .= '
						<'.$captiontag.' class="wp-caption-text gallery-caption">
						' . wptexturize($attachment->post_excerpt) . '
						</'.$captiontag.'>';
				}

			$output .= '</'.$itemtag.'>';

	    	return apply_filters( $this->func_hook_prefix . 'load_gallery_item' , $output , $instance );

	    }

		/* Gallery Carousel
		------------------------------------------------------------------------------------------------------------------- */

	    function load_gallery_in_carousel( $args = array() ) {

			$defaults = array(
				'selector' => 'gallery-1',
				'post_id' => NULL,
				'columns'    => 3,
				'size_class' => '',
				'attachments' => array(),
				'link'       => '',
				'size'       => 'tb-600',
				'itemtag'    => 'figure',
				'icontag'    => 'div',
				'captiontag' => 'figcaption',
				'carouselspeed' => '5000',
				'carouselpause' => 'hover',
				'carouselloop' => 'true',			
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			if ( NULL === $post_id ) {
				$post = get_post();
				$post_id = ( $post ? $post->ID : 0 );
			}

			if ( !empty( $attachments ) ) {

				$output = '<div id='.$selector.' class="gallery sat-gallery sat-carousel carousel slide galleryid-'.$post_id.' gallery-columns-'.$columns.' gallery-size-'.$size_class.'">';
				$galleryid = 'gallery-'.$post_id;

					$output .= '<div class="carousel-inner">';

						$count = 0;
						$counter = 0;
						$total_items = count( $attachments );
						foreach ( $attachments as $id => $attachment ) {

							if ( $counter == 0 ) {
								$output .= '<div class="item'.( $count == 0 ? ' active' : '' ).'">';
									$output .= '<div class="sat-container">';
							}
							
							$output .= $this->load_gallery_item( array( 
										'galleryid' => $galleryid,
										'id' => $id,
										'attachment' => $attachment,
										'link' => $link,
										'size' => $size,
										'columns' => $columns,
										'itemtag'    => $itemtag,
										'icontag'    => $icontag,
										'captiontag' => $captiontag,
									) );

							$count++;
							$counter++;

							if ( $counter == $columns || $count == $total_items ) {
									$output .= '</div>'; // end - sat-container
								$output .= '</div>'; // end - item
								$counter = 0;
							}

						}

			    	$output .= '</div>'; // end - carousel-inner

			    	$output .= '<a class="left carousel-control" href="#'.$selector.'" data-slide="prev"><span class="fa fa-chevron-left"></span></a>';
			    	$output .= '<a class="right carousel-control" href="#'.$selector.'" data-slide="next"><span class="fa fa-chevron-right"></span></a>';

				$output .= '</div>'; // end - sat-gallery
				$output .= '<div class="clear"></div>';

				$output .= '
					<script type="text/javascript">
						jQuery(document).ready(function () {
							jQuery("#'.$selector.'").carousel({
								interval: '.intval( $carouselspeed ).',
								pause: '.( $carouselpause == 'hover' ? '"hover"' : '"none"' ).',
								wrap: '.( $carouselloop == 'true' ? 'true' : 'false' ).',
							});
						});
					</script>';

			} else {
				$output = '';
			}

	    	return apply_filters( $this->func_hook_prefix . 'load_gallery_in_carousel' , $output , $instance );

	    }

		/* END
		------------------------------------------------------------------------------------------------------------------- */

	} // end - class SATFW_Gallery

} // end - !class_exists('SATFW_Gallery')