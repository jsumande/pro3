<?php

	$option_settings = array();

	$btn_color_options = array(
            'grey' => __( 'Grey' , $this->plugin_slug ),
            'blue' => __( 'Blue' , $this->plugin_slug ),
            'lightblue' => __( 'Light Blue' , $this->plugin_slug ),
            'red' => __( 'Red' , $this->plugin_slug ),
            'green' => __( 'Green' , $this->plugin_slug ),
            'yellow' => __( 'Yellow' , $this->plugin_slug ),
            'black' => __( 'Black' , $this->plugin_slug )
		);

	$yes_no_options = array(
            'yes' => __( 'Yes' , $this->plugin_slug ),
            'no' => __( 'No' , $this->plugin_slug )
		);

	// Main settings
	$option_settings[] = array(
	    'section_id' => 'general',
	    'section_title' => __( 'Main Settings' , $this->plugin_slug ),
	    'section_description' => __( '' , $this->plugin_slug ),
	    'section_order' => 1,
	    'fields' => array(
	        array(
	            'id' => 'btn_color',
	            'title' => __( 'Button Color' , $this->plugin_slug ),
	            'desc' => __( 'Select the main button color.' , $this->plugin_slug ),
	            'type' => 'select',
	            'std' => 'black',
	            'choices' => $btn_color_options
	        ),
	        array(
	            'id' => 'show_inventory',
	            'title' => __( 'Show Inventory in Quantity Box' , $this->plugin_slug ),
	            'desc' => __( 'By default, this option is enabled. If you want to disable this option, then simply select "No".' , $this->plugin_slug ),
	            'type' => 'radio',
	            'std' => 'yes',
	            'choices' => $yes_no_options
	        ),
	    )
	);

	// Product Listing
	$option_settings[] = array(
	    'section_id' => 'productlisting',
	    'section_title' => __( 'Product Listing Settings' , $this->plugin_slug ),
	    'section_description' => __( '' , $this->plugin_slug ),
	    'section_order' => 2,
	    'fields' => array(
	        array(
	            'id' => 'enable_shopnow',
	            'title' => __( 'Enable Shop Now Feature' , $this->plugin_slug ),
	            'desc' => __( 'By default, this option is enabled. If you want to disable this option and switch back to the default checkout style, then simply select "No".' , $this->plugin_slug ),
	            'type' => 'radio',
	            'std' => 'yes',
	            'choices' => $yes_no_options
	        ),
	    )
	);

	// wishlist Settings
	$option_settings[] = array(
	    'section_id' => 'wishlist',
	    'section_title' => __( 'Wishlist Settings' , $this->plugin_slug ),
	    'section_description' => __( '' , $this->plugin_slug ),
	    'section_order' => 6,
	    'fields' => array(
	        array(
	            'id' => 'enable_wishlist',
	            'title' => __( 'Enable Wishlist Feature' , $this->plugin_slug ),
	            'desc' => __( 'By default, this option is disabled. If you want to enable this option, then simply select "Yes".' , $this->plugin_slug ),
	            'type' => 'radio',
	            'std' => 'no',
	            'choices' => $yes_no_options
	        ),
	    )
	);

	// compare Settings
	$global_compare = array();
	if ( is_multisite() ) {
		$global_compare = array(
	            'id' => 'enable_global_compare',
	            'title' => __( 'Allow Product Comparison Between Global Products' , $this->plugin_slug ),
	            'desc' => __( 'By default, this option is disabled. If you want to enable this option, then simply select "Yes".' , $this->plugin_slug ),
	            'type' => 'radio',
	            'std' => 'no',
	            'choices' => $yes_no_options
	        );
	}

	$option_settings[] = array(
	    'section_id' => 'compareproducts',
	    'section_title' => __( 'Product Comparison Settings' , $this->plugin_slug ),
	    'section_description' => __( '' , $this->plugin_slug ),
	    'section_order' => 7,
	    'fields' => array(
	        array(
	            'id' => 'enable_compare',
	            'title' => __( 'Enable Product Comparison Feature' , $this->plugin_slug ),
	            'desc' => __( 'By default, this option is disabled. If you want to enable this option, then simply select "Yes".' , $this->plugin_slug ),
	            'type' => 'radio',
	            'std' => 'no',
	            'choices' => $yes_no_options
	        ),
	        array(
	            'id' => 'hide_compare_in_mobile',
	            'title' => __( 'Hide Product Comparison In Mobile Mode' , $this->plugin_slug ),
	            'desc' => __( 'By default, this option is enabled. If you want to disable this option, then simply select "No".' , $this->plugin_slug ),
	            'type' => 'radio',
	            'std' => 'yes',
	            'choices' => $yes_no_options
	        ),
	        $global_compare,
	    )
	);

	// Cross Selling Settings
	$option_settings[] = array(
	    'section_id' => 'crossselling',
	    'section_title' => __( 'Cross Selling Settings' , $this->plugin_slug ),
	    'section_description' => __( '' , $this->plugin_slug ),
	    'section_order' => 8,
	    'fields' => array(
	        array(
	            'id' => 'enable_cs',
	            'title' => __( 'Enable Cross Selling Feature' , $this->plugin_slug ),
	            'desc' => __( 'By default, this option is disabled. If you want to enable this option, then simply select "Yes".' , $this->plugin_slug ),
	            'type' => 'radio',
	            'std' => 'no',
	            'choices' => $yes_no_options
	        ),
	    )
	);

	// Social Sharing Settings
	$option_settings[] = array(
	    'section_id' => 'socialsharing',
	    'section_title' => __( 'Social Sharing Settings' , $this->plugin_slug ),
	    'section_description' => __( '' , $this->plugin_slug ),
	    'section_order' => 9,
	    'fields' => array(
	        array(
	            'id' => 'enable_socialsharing',
	            'title' => __( 'Enable Social Sharing Feature' , $this->plugin_slug ),
	            'desc' => __( 'By default, this option is disabled. If you want to enable this option, then simply select "Yes".' , $this->plugin_slug ),
	            'type' => 'radio',
	            'std' => 'no',
	            'choices' => $yes_no_options
	        ),
	        array(
	            'id' => 'social_sharing_icons',
	            'title' => __( 'Social Sharing Icons' , $this->plugin_slug ),
	            'desc' => __( 'Select which icons you want to display in the social sharing section.' , $this->plugin_slug ),
	            'type' => 'checkboxes',
	            'std' => array( 'facebook' , 'twitter' , 'google-plus' , 'linkedin' ),
	            'choices' => array(
	                'facebook' => 'Facebook',
	                'twitter' => 'Twitter',
	                'google-plus' => 'Google Plus',
	                'linkedin' => 'LinkedIn',
	                'pinterest' => 'Pinterest',
	                'vk' => 'VK',
	                'xing' => 'Xing'
	            )
	        ),
	    )
	);

	// Get This Free Settings
	$option_settings[] = array(
	    'section_id' => 'getthisfree',
	    'section_title' => __( '"Get This FREE" Settings' , $this->plugin_slug ),
	    'section_description' => __( '' , $this->plugin_slug ),
	    'section_order' => 10,
	    'fields' => array(
	        array(
	            'id' => 'enable_feature',
	            'title' => __( 'Enable "Get This FREE" Feature' , $this->plugin_slug ),
	            'desc' => __( 'By default, this option is disabled. If you want to enable this option, then simply select "Yes".' , $this->plugin_slug ),
	            'type' => 'radio',
	            'std' => 'no',
	            'choices' => $yes_no_options
	        ),
	        array(
	            'id' => 'btn_text',
	            'title' => __( 'Button Text' , $this->plugin_slug ),
	            'desc' => '',
	            'type' => 'text',
	            'std' => __( 'Get This FREE' , $this->plugin_slug )
	        ),
	        array(
	            'id' => 'price_label',
	            'title' => __( 'Price Label' , $this->plugin_slug ),
	            'desc' => '',
	            'type' => 'text',
	            'std' => __( 'FREE' , $this->plugin_slug )
	        ),
	    )
	);

	// Purchase Motivator Settings
	$option_settings[] = array(
	    'section_id' => 'purchasemotivator',
	    'section_title' => __( 'Purchase Motivator Settings' , $this->plugin_slug ),
	    'section_description' => __( '' , $this->plugin_slug ),
	    'section_order' => 11,
	    'fields' => array(
	        array(
	            'id' => 'enable_motivator',
	            'title' => __( 'Enable Purchase Motivator' , $this->plugin_slug ),
	            'desc' => __( 'By default, this option is disabled. If you want to enable this option, then simply select "Yes".' , $this->plugin_slug ),
	            'type' => 'radio',
	            'std' => 'no',
	            'choices' => $yes_no_options
	        ),
	        array(
	            'id' => 'cart_goal',
	            'title' => __( 'Cart Goal' , $this->plugin_slug ),
	            'desc' => __( 'The total amount you want your customers to achieve.' , $this->plugin_slug ),
	            'type' => 'text',
	            'std' => '150'
	        ),
	        array(
	            'id' => 'pitch_message',
	            'title' => __( 'Pitch Message' , $this->plugin_slug ),
	            'desc' => __( '[BALANCE] will be replaced with the remaining balance before achieving the goal.' , $this->plugin_slug ),
	            'type' => 'textarea',
	            'std' => __( 'AWESOME! Spend [BALANCE] more to get FREE shipping!' , $this->plugin_slug )
	        ),
	        array(
	            'id' => 'success_message',
	            'title' => __( 'Success Message' , $this->plugin_slug ),
	            'desc' => '',
	            'type' => 'textarea',
	            'std' => __( 'CONGRATS! You are entitled to FREE SHIPPING on your order!' , $this->plugin_slug )
	        ),
	        array(
	            'id' => 'order_message',
	            'title' => __( 'Order Email Message' , $this->plugin_slug ),
	            'desc' => __( 'Just place this tag: PMMESSAGE into your new order email template and this message will display in the order email if the customer achieved the cart goal.' , $this->plugin_slug ),
	            'type' => 'textarea',
	            'std' => __( 'CONGRATS! You are entitled to 20% off on your next purchase at our store. Just use this coupon: COUPONCODEHERE during your next checkout.' , $this->plugin_slug )
	        ),
	        array(
	            'id' => 'message_box_color',
	            'title' => __( 'Color Skin' , $this->plugin_slug ),
	            'type' => 'select',
	            'std' => 'green',
	            'choices' => array(
	                'red' => 'Red',
	                'green' => 'Green',
	                'blue' => 'Blue',
	                'yellow' => 'Yellow',
	            )
	        ),
	    )
	);