<?php

	if( !class_exists('SAOptionsClass') ) {

		class SAOptionsClass {

		    private $plugin_path;
		    private $plugin_url;
		    private $plugin_slug;

		    function __construct( $args = array() ) 
		    {

				$defaults = array(
					'option_group' => 'plugin_settings_sample',
					'plugin_path' => NULL,
					'plugin_url' => NULL,
					'plugin_slug' => 'pss',
					'settings_hook' => 'pss_',
					'settings_file' => NULL,
					'page_hook' => NULL,
					'page_name' => __( 'Plugin Settings Sample', 'pss' ),
					'is_menu_page' => true,
					'menu_page_name' => __( 'Settings Sample', 'pss' ),
					'menu_page_slug' => 'options_class_page',
					'menu_page_access' => 'manage_options',
					'submenu_page_name' => __( 'General', 'pss' ),
					'submenu_page_slug' => 'options_class_subpage',
					'submenu_page_access' => 'manage_options',
				);

				$instance = wp_parse_args( $args, $defaults );
				extract( $instance );

				if ( NULL == $plugin_path && NULL == $plugin_url && NULL == $settings_file )
					exit();

		        $this->plugin_path = $plugin_path;
		        $this->plugin_url = $plugin_url;
		        $this->plugin_slug = $plugin_slug;
		        $this->settings_hook = $settings_hook;
		        $this->page_name = $page_name;
		        $this->option_group = $option_group;
		        $this->settings_file = $settings_file;
		        $this->page_hook = $page_hook;
		        $this->is_menu_page = $is_menu_page;
		        $this->menu_page_name = $menu_page_name;
		        $this->menu_page_slug = $menu_page_slug;
		        $this->menu_page_access = $menu_page_access;
		        $this->submenu_page_name = $submenu_page_name;
		        $this->submenu_page_slug = $submenu_page_name;
		        $this->submenu_page_access = $submenu_page_name;

		        require_once( $this->settings_file );
		        $this->option_settings = $option_settings;

		        add_action( 'admin_menu', array(&$this, 'add_menu_page'), 99 );
	            add_action( 'admin_init', array(&$this, 'admin_init') );
	            add_action( 'admin_notices', array(&$this, 'admin_notices') );
	            add_action( 'admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts') );

		    }

	        /**
			* Register menu page
			*/
			function add_menu_page() {

				if ( $this->is_menu_page ) {
					$this->page_hook = add_menu_page( $this->menu_page_name , $this->menu_page_name , $this->menu_page_access , $this->menu_page_slug , array(&$this, 'settings_page') );
				} else {
					$this->page_hook = add_submenu_page( $this->menu_page_slug  , $this->submenu_page_name , $this->submenu_page_name , $this->submenu_page_access , $this->submenu_page_slug , array(&$this, 'settings_page') );
				}

			}

	        /**
			* Registers settings page
			*/
			function settings_page() {
	        	?>
	            <div class="wrap sa-settings-page">
                    <div id="icon-options-general" class="icon32"></div>
                    <h2><?php echo $this->page_name; ?></h2>
                    <?php
                    	// Output your settings form
                    	$this->settings();
                    ?>
	            </div>
	            <?php
			}

	        /**
			* Registers the internal WordPress settings
			*/
	        function admin_init() {
	            register_setting( $this->option_group , $this->option_group .'_settings' , array(&$this, 'settings_validate') );
	            $this->process_settings();
	        }
	        
	        /**
			* Displays any errors from the WordPress settings API
			*/
	        function admin_notices() {
	        	$screen = get_current_screen();

	        	if ( $screen->id == $this->page_hook )
	            	settings_errors();
	        }
	            
            /**
			* Enqueue scripts and styles
			*/
            function admin_enqueue_scripts() {
	            wp_enqueue_style('farbtastic');
	            wp_enqueue_style('thickbox');
	            wp_enqueue_style('wp-color-picker');
	            wp_enqueue_style('sa-settings-css', $this->plugin_url . 'css/settings-css.css', null, null);
	            
	            wp_enqueue_script('jquery');
	            wp_enqueue_script('farbtastic');
	            wp_enqueue_script('media-upload');
	            wp_enqueue_script('thickbox');
				wp_enqueue_script('iris');
				wp_enqueue_script('wp-color-picker');
		        wp_enqueue_media();

		        wp_enqueue_script( 'sa-settings-js', $this->plugin_url . 'js/settings-js.js', array('jquery'));

		        do_action( $this->settings_hook . 'admin_enqueue_scripts' );
            }
	            
	        /**
			* Adds a filter for settings validation
			*
			* @param array the un-validated settings
			* @return array the validated settings
			*/
	        function settings_validate( $input ) {
	            return apply_filters( $this->option_group .'_settings_validate', $input );
	        }
	            
	        /**
			* Displays the "section_description" if speicified in $this->option_settings
			*
			* @param array callback args from add_settings_section()
			*/
	        function section_intro( $args ) {

	            if(!empty($this->option_settings)){
	               	foreach($this->option_settings as $section){
	                    if($section['section_id'] == $args['id']){
	                        if(isset($section['section_description']) && $section['section_description']) 
	                        	echo '<p>'. $section['section_description'] .'</p>';
	                        break;
	                    }
	                } // end - foreach ()
	            } // end - if (!empty)
	        }
	            
	        /**
			* Processes $this->option_settings and adds the sections and fields via the WordPress settings API
			*/
	        function process_settings() {


                if(!empty($this->option_settings)){

                 	usort($this->option_settings, array(&$this, 'sort_array'));

                    foreach($this->option_settings as $section){

                        if(isset($section['section_id']) && $section['section_id'] && isset($section['section_title'])){

                            add_settings_section( $section['section_id'] , $section['section_title'] , array(&$this, 'section_intro') , $this->option_group );

                            if(isset($section['fields']) && is_array($section['fields']) && !empty($section['fields'])){

                                foreach($section['fields'] as $field){

                                    if(isset($field['id']) && $field['id'] && isset($field['title'])){
                                     	add_settings_field( $field['id'], $field['title'], array(&$this, 'generate_setting'), $this->option_group, $section['section_id'], array('section' => $section, 'field' => $field) );
                                    }

                                } // end foreach ()

                            } // end if else

                        } // end - if else

                    } // end - foreach ()
                }
	        }
	            
	        /**
			* Usort callback. Sorts $this->option_settings by "section_order"
			*
			* @param mixed section order a
			* @param mixed section order b
			* @return int order
			*/
            function sort_array( $a, $b ) {
                return $a['section_order'] > $b['section_order'];
            }
	            
	        /**
			* Generates the HTML output of the settings fields
			*
			* @param array callback args from add_settings_field()
			*/
	        function generate_setting( $args ) {
	            $section = $args['section'];

	            $defaults = array(
	                        'id' => 'default_field',
	                        'title' => 'Default Field',
	                        'desc' => '',
	                        'std' => '',
	                        'type' => 'text',
	                        'choices' => array(),
	                        'class' => ''
	                );

                $defaults = apply_filters( $this->settings_hook . 'default_args' , $defaults );
                extract( wp_parse_args( $args['field'], $defaults ) );

                $class = esc_attr($class);
                
                $options = get_option( $this->option_group .'_settings' );
                $el_id = $this->option_group .'_'. $section['section_id'] .'_'. $id;
                $val = (isset($options[$el_id])) ? $options[$el_id] : $std;
                
                do_action($this->settings_hook . 'before_field');
                do_action($this->settings_hook . 'before_field_'. $el_id);

               	switch( $type ){
                    case 'text':
                    $val = esc_attr(stripslashes($val));
                    echo '<input type="text" name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'" value="'. $val .'" class="regular-text '. $class .'" />';
                    if($desc) echo '<p class="description">'. $desc .'</p>';
                    break;

                case 'textarea':
                    $val = esc_html(stripslashes($val));
                    echo '<textarea name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'" rows="5" cols="60" class="'. $class .'">'. $val .'</textarea>';
                    if($desc) echo '<p class="description">'. $desc .'</p>';
                    break;

                case 'select':
                    $val = esc_html(esc_attr($val));
                    echo '<select name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'" class="'. $class .'">';
                    foreach($choices as $ckey=>$cval){
                        echo '<option value="'. $ckey .'"'. (($ckey == $val) ? ' selected="selected"' : '') .'>'. $cval .'</option>';
                    }
                    echo '</select>';
                    if($desc) echo '<p class="description">'. $desc .'</p>';
                    break;

                case 'radio':
                    $val = esc_html(esc_attr($val));
                    foreach($choices as $ckey=>$cval){
                        echo '<label><input type="radio" name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'_'. $ckey .'" value="'. $ckey .'" class="'. $class .'"'. (($ckey == $val) ? ' checked="checked"' : '') .' /> '. $cval .'</label><br />';
                    }
                    if($desc) echo '<p class="description">'. $desc .'</p>';
                    break;

                case 'checkbox':
                    $val = esc_attr(stripslashes($val));
                    echo '<input type="hidden" name="'. $this->option_group .'_settings['. $el_id .']" value="0" />';
                    echo '<label><input type="checkbox" name="'. $this->option_group .'_settings['. $el_id .']" id="'. $el_id .'" value="1" class="'. $class .'"'. (($val) ? ' checked="checked"' : '') .' /> '. $desc .'</label>';
                    break;

                case 'checkboxes':
                    foreach($choices as $ckey=>$cval){

	                    $val = '';

	                    if(isset($options[$el_id][$ckey])) 
	                    	$val = $options[$el_id][$ckey];
	                    elseif(is_array($std) && in_array($ckey, $std)) 
	                    	$val = $ckey;

	                    $val = esc_html(esc_attr($val));
	                         echo '<input type="hidden" name="'. $this->option_group .'_settings['. $el_id .']['.$ckey.']" value="0" />';
	                         echo '<label><input type="checkbox" name="'. $this->option_group .'_settings['. $el_id .']['.$ckey.']" id="'. $el_id .'_'. $ckey .'" value="'. $ckey .'" class="'. $class .'"'. (($ckey == $val) ? ' checked="checked"' : '') .' /> '. $cval .'</label><br />';
                    }
                    if($desc) echo '<p class="description">'. $desc .'</p>';
                    break;

                case 'color':

                    $val = esc_attr(stripslashes($val));

                    echo '<input type="text" id="'. $el_id .'" class="input-color-picker '. $class .'" value="'. $val .'" name="'. $this->option_group .'_settings['. $el_id .']" />';
                    if($desc) echo '<p class="description">'. $desc .'</p>';

	                break;

                case 'file':
                    $val = esc_attr($val);
                    echo '<input type="text" id="'. $el_id .'" class="regular-text '. $class .'" value="'. $val .'" name="'. $this->option_group .'_settings['. $el_id .']">';
                    echo '<a href="#" class="sa_settings_upload_button button" rel="image">'.__( 'Upload' , $this->plugin_slug ).'</a><p></p>';
                   	echo ( !empty($val) ? '<div class="sa-screenshot"><img src="'. $val .'" width="300" /></div>' : '' );
                    break;

                case 'editor':
                    wp_editor( $val, $el_id, array( 'textarea_name' => $this->option_group .'_settings['. $el_id .']' ) );
                    if($desc) echo '<p class="description">'. $desc .'</p>';
                    break;

                case 'custom':
                    echo $std;
                    break;

                default:
                    break;

	            } // end - switch

	            do_action($this->settings_hook . 'after_field');
	            do_action($this->settings_hook . 'after_field_'. $el_id);
	        }
	    
	        /**
			* Output the settings form
			*/
	        function settings() {
	            do_action($this->settings_hook . 'before_settings');
	            ?>
					<form action="options.php" method="post">
						<?php do_action($this->settings_hook . 'before_settings_fields'); ?>
						<?php settings_fields( $this->option_group ); ?>
	                	<?php do_settings_sections( $this->option_group ); ?>
	                	<p class="submit"><input type="submit" class="button-primary" value="<?php _e( 'Save Changes' , $this->plugin_slug ); ?>" /></p>
	                </form>
	            <?php
	            do_action($this->settings_hook . 'after_settings');
	        }

		} // end class SAOptionsClass

	} // end if !class_exists('SAOptionsClass')