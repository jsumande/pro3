/**
 * Settings JS
 *
 */

jQuery.noConflict();

/** Fire up jQuery - let's dance! 
 */
jQuery(document).ready(function($){

	/** Colorpicker Field
	----------------------------------------------- */
	function sa_settings_page_colorpicker() {
		$('.sa-settings-page .input-color-picker').each(function(){
			var $this	= $(this),
				parent	= $this.parent();
				
			$this.wpColorPicker();
		});
	}

	function sa_colorpicker() {
		$('.sa-input-color-picker').each(function(){
			var $this	= $(this),
				parent	= $this.parent();
				
			$this.wpColorPicker();
		});
	}
	
	sa_colorpicker();
	sa_settings_page_colorpicker();
	
	/** Media Uploader
	----------------------------------------------- */	
	$(document).on('click', '.sa_settings_upload_button', function(event) {
		var $clicked = $(this), frame,
			input_id = $clicked.prev().attr('id'),
			media_type = $clicked.attr('rel');
			
		event.preventDefault();
		
		// If the media frame already exists, reopen it.
		if ( frame ) {
			frame.open();
			return;
		}
		
		// Create the media frame.
		frame = wp.media.frames.aq_media_uploader = wp.media({
			// Set the media type
			library: {
				type: media_type
			},
			view: {
				
			}
		});
		
		// When an image is selected, run a callback.
		frame.on( 'select', function() {
			// Grab the selected attachment.
			var attachment = frame.state().get('selection').first();
			
			$('#' + input_id).val(attachment.attributes.url);
			
			if(media_type == 'image') $('#' + input_id).parent().find('.sa-screenshot img').attr('src', attachment.attributes.url);
			
		});

		frame.open();
	
	});

	/** Re-trigger after ajax fired
	----------------------------------------------- */
	/*
	$(document).ajaxComplete(function(e, xhr, settings) {
		sa_colorpicker();
	});
	*/
		
});