/* MP Premium Features Script
-------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------- */

var MPPF_JS = new function() {

	var t = this;

	/* Stock tracker
	------------------------------------------------------------------------------------------------------------------- */
	var powerTipListener = function() {
		jQuery(".mppf-powertip-trigger").powerTip({
			placement: "nw-alt",
			smartPlacement: true,
		});
	}

	var updateStock = function( formElm ) {
	 	var quantityInput = formElm.find("input.mppf-product-quantity-field.mppf-powertip-trigger"),
	 		productID = formElm.find("input[name=product_id]").val(),
	 		productVariation = formElm.find("select.mppf-variations-listener").val();

		quantityInput.powerTip({ manual: true });
		jQuery.post(MPPF_Ajax.ajaxUrl, { action: 'mppf-stock-update' , product_id: productID , variation: productVariation }, function(result) {
			quantityInput.data('powertip', function() {
				return result;
			}).powerTip({ manual: false });
		});
	}

	t.variationUpdateStock = function() {
		var select = jQuery(this),
			productVariation = select.val(),
		 	formElm = select.parents('form.mppf_buy_form'),
		 	quantityInput = formElm.find("input.mppf-product-quantity-field.mppf-powertip-trigger"),
		 	productID = formElm.find("input[name=product_id]").val();
			quantityInput.powerTip({ manual: true });
			jQuery.post(MPPF_Ajax.ajaxUrl, { action: 'mppf-stock-update' , product_id: productID , variation: productVariation }, function(result) {
				quantityInput.data('powertip', function() {
					return result;
				}).powerTip({ manual: false });
			});
	}

	/* Main Modal
	------------------------------------------------------------------------------------------------------------------- */

	var modalTrigger = function( modal ) {
		if(jQuery().magnificPopup){
			modal.removeClass(".mfp-hide");
			jQuery.magnificPopup.open({
				items: {
					src: modal,
					type: 'inline',
				},
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: "scroll",
				closeBtnInside: true,
				preloader: false, 
				midClick: true,
				removalDelay: 300,
				mainClass: "mppf-mfp-zoom-in"
			});


			jQuery(".mppf-main-modal-close").click(function () {
				jQuery.magnificPopup.close({
					items: {
						src: modal,
						type: 'inline',
					},
				});
			});	
		}
	}

	/* Purchase Motivator
	------------------------------------------------------------------------------------------------------------------- */

	var purchaseMotivator = function( MessageBox ) {
		var MessageBoxContents = MessageBox.find(".mppf-pm-message-box");
		if ( MessageBoxContents ) {
			MessageBoxContents.html( MPPF_Ajax.UpdatingTxtIcon );
			jQuery.post(MPPF_Ajax.ajaxUrl, { action: 'mppf-mp-message-update' }, function(result) {
				MessageBoxContents.fadeOut( 500 , function(){
					MessageBox.html( result );
				});
			});
		}
	}

	/* Default Add To Cart Ajax
	------------------------------------------------------------------------------------------------------------------- */

	var emptyCart = function() {
		if ( jQuery("a.mp_empty_cart").attr("onClick") != undefined ) {
			return;
		}

		jQuery("a.mp_empty_cart").click(function() {
			var answer = confirm( MPPF_Ajax.emptyCartMsg );
			if (answer) {
				jQuery(this).html( MPPF_Ajax.LoadingIcon );
				jQuery.post(MPPF_Ajax.ajaxUrl, {action: 'mp-update-cart', empty_cart: 1}, function(data) {
					jQuery("div.mp_cart_widget_content").html(data);
					if (jQuery('#booking-add-button').length){
						jQuery('#booking-add-button').removeClass("mppf-btn-disabled");
						jQuery('.booking-msg-helper').html('');
					}	
				});
			}
			return false;
		});
	}
	
	t.cartListener = function() {
		
		var input = jQuery('#product_page_addtocart_live'),
			input_fake = jQuery('#product_page_addtocart'),
			tempInput = input.html(),
			tempInput_fake = input.html(),
			
			formElm = input.parents('form.mppf_buy_form'),
			tempHtml = formElm.html(),
			serializedForm = formElm.serialize();

		if(jQuery("#pp-items-cart").length !== 0) {
			var cartAddMore = jQuery('span#cartAddMore').text();
			var cartAddQty = jQuery('#pp-items-cart span').text();
			var cartQty = jQuery('form.mppf_buy_form .mppf-product-quantity-field').val();
			if(jQuery('#pp-items-cart span').is(':empty')) {
				var QtyTotal = parseInt(cartQty);
			} else {
				var QtyTotal = parseInt(cartQty) + parseInt(cartAddQty);
			}	
			var showAddCartTip = true;
		} else{
			var showAddCartTip = false;
		}
		
		input.html( MPPF_Ajax.addingItem );
		input_fake.html( MPPF_Ajax.addingItem );
		jQuery.post(MPPF_Ajax.ajaxUrl, serializedForm, function(data) {
			var result = data.split('||', 2);
			if (result[0] == 'error') {
				var re_str = result[1].replace(/\\/g, '');
				alert(re_str);
				input.html(tempInput);
				input_fake.html(tempInput);
			} else {
				input.html( MPPF_Ajax.cartSuccess ).addClass("mppf-btn-disabled");
				jQuery("div.mp_cart_widget_content").html(result[1]);
				setTimeout(function(){
					if (result[0] > 0) {
						if(showAddCartTip){
							input.html(tempInput).removeClass("mppf-btn-disabled").blur().text(cartAddMore);
							jQuery('#pp-items-cart span').text(QtyTotal);
							if(jQuery('#pp-items-cart').is(":hidden")){
								jQuery('#pp-items-cart').fadeIn();
							}
						} else {
							input.html(tempInput).removeClass("mppf-btn-disabled").blur();
						}
						
						if(showAddCartTip){
							input_fake.html(tempInput).removeClass("mppf-btn-disabled").blur().text(cartAddMore);
							jQuery('#pp-items-cart span').text(QtyTotal);
							if(jQuery('#pp-items-cart').is(":hidden")){
								jQuery('#pp-items-cart').fadeIn();
							}
						} else {
							input_fake.html(tempInput).removeClass("mppf-btn-disabled").blur();
						}						
						
						
					} else {
						input.html( MPPF_Ajax.noStockMsg );
						input_fake.html( MPPF_Ajax.noStockMsg );
					}
	   			}, MPPF_Ajax.Carttimeout );
				emptyCart(); //re-init empty script as the widget was reloaded
				updateStock( formElm );
			}
		});

		return false;
	}
	
	
	t.cartListenerBooking = function() {
		
		var input = jQuery('#booking-add-button'),
			tempInput = input.html(),
			
			formElm = input.parents('form.mppf_booking_form'),
			tempHtml = formElm.html(),
			serializedForm = formElm.serialize();

		
		input.html( MPPF_Ajax.addingItem );
		jQuery.post(MPPF_Ajax.ajaxUrl, serializedForm, function(data) {
			var result = data.split('||', 2);
			if (result[0] == 'error') {
				var re_str = result[1].replace(/\\/g, '');
				alert(re_str);
				input.html(tempInput);
			} else {
				input.addClass("mppf-btn-disabled");
				jQuery("div.mp_cart_widget_content").html(result[1]);
				setTimeout(function(){
					if (result[0] > 0) {
						input.html( MPPF_Ajax.bookingConfirm);
						//window.location.replace(MPPF_Ajax.checkoutlink);
						jQuery('.booking-msg-helper').html(MPPF_Ajax.bookingadded);
					} else {
						input.html( MPPF_Ajax.bookingNot );
						
					}
	   			}, MPPF_Ajax.Carttimeout );
				emptyCart(); //re-init empty script as the widget was reloaded
				updateStock( formElm );
			}
		});

		return false;
	}	
	
	
	
	//add to cart via link
	t.cartListener_compare = function() {
		
		var input = jQuery(this),
			tempInput = input.html(),
			datainput = input.attr( 'data' );

		input.html( MPPF_Ajax.addingItem );
		jQuery.post(MPPF_Ajax.ajaxUrl, {product_id: datainput, variation: "0", quantity: "1", action: "mppf-update-cart"}, function(data) {
			var result = data.split('||', 2);
			if (result[0] == 'error') {
				var re_str = result[1].replace(/\\/g, '');
				alert(re_str);
				input.html(tempInput);
			} else {
				input.html( MPPF_Ajax.cartSuccess ).addClass("mppf-btn-disabled");
				jQuery("div.mp_cart_widget_content").html(result[1]);
				setTimeout(function(){
					if (result[0] > 0) {
						input.html(tempInput).removeClass("mppf-btn-disabled").blur();											
					} else {
						input.html( MPPF_Ajax.noStockMsg );
					}
	   			}, MPPF_Ajax.Carttimeout );
				emptyCart(); //re-init empty script as the widget was reloaded
			}
		});

		return false;
	}	
	
	
	
	/* Wishlist
	------------------------------------------------------------------------------------------------------------------- */

	t.wishlistListener = function() {
		var btn = jQuery('#single_product_page_wishlist_button_live'),
			fake_btn = jQuery('#single_product_page_wishlist_button'),
			formElm = btn.parents('form.mppf-wishlist-form'),
		 	tempInput = btn.html(),
			tempInput_fake = fake_btn.html(),
			serializedForm = formElm.serialize();

		btn.html( MPPF_Ajax.addingItem );
		fake_btn.html( MPPF_Ajax.addingItem );
		jQuery.post(MPPF_Ajax.ajaxUrl, serializedForm, function(data) {
			var result = data.split('|SEP|', 2);
			if (result[0] == 'inthelist') {
				btn.addClass("mppf-btn-disabled").html(result[1]);
				fake_btn.addClass("mppf-btn-disabled").html(result[1])
			} else if (result[0] == 'notlogin') {
				btn.fadeOut( 1000 , function(){
					btn.after(result[1]);
				});
				fake_btn.fadeOut( 1000 , function(){
					fake_btn.after(result[1]);
				});
				
				
			} else {
				btn.addClass("mppf-btn-disabled").html(MPPF_Ajax.WLSuccess).fadeIn('fast');
				fake_btn.addClass("mppf-btn-disabled").html(MPPF_Ajax.WLSuccess).fadeIn('fast');
			}
		});
		return false;
	}

	t.updateWishlist = function() {
	    jQuery(this).html( '<i class="icon-spinner icon-spin"></i>'+MPPF_Ajax.UpdatingTxt ).addClass("mppf-btn-disabled");		
	}

	
	/* Compare Products
	------------------------------------------------------------------------------------------------------------------- */

	t.updateCompareListener = function() {
		var btn = jQuery(this),
		 	formElm = btn.parents('form.mppf-compare-products-form'),
		 	tempInput = btn.html(),
		 	tempForm = formElm.html(),
			serializedForm = formElm.serialize();

		btn.html( MPPF_Ajax.UpdatingBtn );
		jQuery.post(MPPF_Ajax.ajaxUrl, serializedForm, function(data) {
			var result = data.split('|SEP|', 2);
			formElm.fadeOut( 500 , function(){
				formElm.html(result[1]).fadeIn('fast');
				 mppf_dropdown_generator();
			});
		});

		return false;
	}

	t.compareProductListener = function( event ) {
		var btn = jQuery('#single_product_page_comparebutton_live'),
			btn_fake = jQuery('#single_product_page_comparebutton'),
			modal = event.data.modal,
		 	formElm = btn.parents("form.mppf-product-compare-form"),
		 	viewCPBtn = formElm.siblings("a.compare-results-modal-trigger"),
		 	tempInput = btn.html(),
			tempInput_fake = btn_fake.html(),
			serializedForm = formElm.serialize();

		btn.html( MPPF_Ajax.addingItem );
		btn_fake.html( MPPF_Ajax.addingItem );
		jQuery.post(MPPF_Ajax.ajaxUrl, serializedForm, function(data) {
			var result = data.split('|SEP|', 3);

			modal.html(result[2]);
			btn.addClass("mppf-btn-disabled").html(result[1]);
			btn_fake.addClass("mppf-btn-disabled").html(result[1]);
			viewCPBtn.addClass("mppf-btn-show animated fadeInRight");

			$('#mp-premium-features.mppf-pro-integration.mppf-container-single .compare-results-modal-trigger.mppf-btn-hide').attr("style","display:block!important"); 
			setTimeout(function(){
   				btn.removeClass("mppf-btn-disabled").html(tempInput).blur();
				btn_fake.removeClass("mppf-btn-disabled").html(tempInput).blur();
   			}, MPPF_Ajax.CPtimeout );

		});
		return false;
	}

	t.cpAddNewListener = function() {
		var cpForm = jQuery(this).parents("form.mppf-compare-products-form");
		cpForm.find(".mppf-compare-table-update-msg").addClass("update-msg-visible animated fadeInLeft");
	}

	t.cpUpdateProductList = function() {
		var blogSelect = jQuery(this),
			blogID = blogSelect.find("option:selected").val(),
			addnewTD = blogSelect.parent().parent(),
			itemDIV = addnewTD.find(".mppf-compare-table-select-item"),
			itemSelectID = itemDIV.find("select").attr("id");

		itemDIV.html( MPPF_Ajax.UpdatingTxtIcon );
		jQuery.post(MPPF_Ajax.ajaxUrl, {action: 'mppf-cp-update-product-list', current_blog_id: blogID , addnew_select_id: itemSelectID }, function(data) {
			itemDIV.html(data);
		});
	}

	/* Cross Selling
	------------------------------------------------------------------------------------------------------------------- */

	var csAddCart = function( modal ) {
		jQuery("button.mppf-btn-inside-cs").click(function() {
			var input = jQuery(this),
				tempInput = input.html(),
				formElm = input.parents('form.mppf_buy_form'),
				tempHtml = formElm.html(),
				serializedForm = formElm.serialize();
			input.html( MPPF_Ajax.addingItem );
			jQuery.post(MPPF_Ajax.ajaxUrl, serializedForm, function(data) {
				var result = data.split('|SEP|', 4),
					pmMessageBox = modal.find(".mppf-pm-cs-container");
				if (result[0] == 'error') {
					alert(result[1]);
					input.html(tempInput);
					updateStock( formElm );
				} else {
					input.html( MPPF_Ajax.cartSuccess ).addClass("mppf-btn-disabled");
					jQuery("div.mp_cart_widget_content").html(result[1]);
					modal.find(".mppf-cs-cart-info").html(result[3]);
					purchaseMotivator( pmMessageBox );
					setTimeout(function(){
						if (result[0] > 0) {
							input.html(tempInput).removeClass("mppf-btn-disabled").blur();
						} else {
							input.html( MPPF_Ajax.noStockMsg );
						}
		   			}, MPPF_Ajax.Carttimeout );
					emptyCart(); //re-init empty script as the widget was reloaded
					updateStock( formElm );
				}
			});
			return false;
		});
	}

	t.csListener = function( event ) {
		var input = jQuery(this),
			modal = event.data.modal,
			tempInput = input.html(),
			formElm = input.parents('form.mppf_buy_form'),
			tempHtml = formElm.html(),
			serializedForm = formElm.serialize();
		input.html( MPPF_Ajax.addingItem );
		jQuery.post(MPPF_Ajax.ajaxUrl, serializedForm, function(data) {
			var result = data.split('|SEP|', 4);
			if (result[0] == 'error') {
				alert(result[1]);
				input.html(tempInput);
			} else {
				input.html( MPPF_Ajax.cartSuccess ).addClass("mppf-btn-disabled");
				jQuery("div.mp_cart_widget_content").html(result[1]);
				modal.html(result[2]);
				modalTrigger( modal );
				setTimeout(function(){
					if (result[0] > 0) {
						input.html(tempInput).removeClass("mppf-btn-disabled").blur();
					} else {
						input.html( MPPF_Ajax.noStockMsg );
					}
	   			}, MPPF_Ajax.Carttimeout );
			}
			emptyCart(); //re-init empty script as the widget was reloaded
			updateStock( formElm );
			csAddCart( modal );
		});
		return false;
	}

	/* Shop Now Menu
	------------------------------------------------------------------------------------------------------------------- */
	/*
	t.snmTrigger = function( event ) {
		var btn = jQuery(this),
			modal = event.data.modal,
			tempInput = btn.html(),
			blogID = btn.siblings( "input#snm-blog-id" ).val(),
			globalBTN = btn.siblings( "input#snm-global-btn" ).val(),
			productID = btn.val();

		modal.html( MPPF_Ajax.ModalLoading );
		modalTrigger( modal );
		jQuery.post(MPPF_Ajax.ajaxUrl, {action: 'mppf-load-snm', product_id: productID , blog_id: blogID , globalbtn: globalBTN }, function(data) {
			modal.find(".mpt-modal-loading-mode").fadeOut( 1000 , function(){
				modal.html(data);
	   			powerTipListener();
			});
		});
		return false;
	}
	*/

	/* Run the init function
	---------------------------------------------------------- */
	jQuery(document).ready(function(){

		var MPPF_Main_Modal = jQuery("#mpt-main-modal"),
			MPPF_DOC = jQuery(this);

		// powertip listener
		powerTipListener();
		MPPF_DOC.on( "change" , "select.mppf-variations-listener" , MPPF_JS.variationUpdateStock );

		// cart listener
		//MPPF_DOC.on( "click" , "button.mppf-btn-addcart" , MPPF_JS.cartListener );
		
		$('#product_page_addtocart').on("click", MPPF_JS.cartListener);
		$('#booking-add-button').on("click", MPPF_JS.cartListenerBooking);
				
		MPPF_DOC.on( "click" , "a.add-to-cart-compare" , MPPF_JS.cartListener_compare);
		
		// wishlist listener
		//MPPF_DOC.on( "click" , "button.mppf-addtowishlist-btn" , MPPF_JS.wishlistListener );
		MPPF_DOC.on( "click" , "button.mppf-update-wishlist-btn" , MPPF_JS.updateWishlist );
		
		
		$('#single_product_page_wishlist_button').on("click", MPPF_JS.wishlistListener);		
		

		// product comparison listener
		MPPF_DOC.on( "click" , "button.mppf-update-compare-results" , MPPF_JS.updateCompareListener );
		//MPPF_DOC.on( "click" , "button.mppf-addtocompare-btn" , { modal: MPPF_Main_Modal } , MPPF_JS.compareProductListener );
		MPPF_DOC.on( "change" , ".mppf-compare-table-select-item > select" , MPPF_JS.cpAddNewListener );
		MPPF_DOC.on( "change" , ".mppf-compare-table-select-blog > select" , MPPF_JS.cpUpdateProductList );

		
		$('#single_product_page_comparebutton').on("click", { modal: MPPF_Main_Modal } , MPPF_JS.compareProductListener);		
		
		
		// cross selling listener
		MPPF_DOC.on( "click" , "button.mppf-btn-cs" , { modal: MPPF_Main_Modal } , MPPF_JS.csListener );

		// snm listener
		//MPPF_DOC.on( "click" , ".mppf-shop-now-btn" , { modal: MPPF_Main_Modal } , MPPF_JS.snmTrigger );

	});

}

///////
jQuery(document).ready(function() {
	jQuery('.mppf-modal-trigger').click(function(){
		mppf_dropdown_generator();

	});
	
	 jQuery('#select_all_boxes, #select_all_boxes2').click(function(event) {
			if(this.checked) {
				jQuery('.wishcheckbox').each(function() {
					this.checked = true;              
				});
			}else{
				jQuery('.wishcheckbox').each(function() {
					this.checked = false;       
				});         
			}
		});	


});

 function mppf_dropdown_generator(){
			jQuery("#addnewcompare1").each(function(index){
				jQuery(this).selectbox();
				jQuery('.sbHolder').css('text-align','left');

			});
			jQuery("#addnewcompare3").each(function(index){
				jQuery(this).selectbox();
				jQuery('.sbHolder').css('text-align','left');

			});
			jQuery("#addnewcompare2").each(function(index){
				jQuery(this).selectbox();
				jQuery('.sbHolder').css('min-width','100%');
				jQuery('.sbHolder').css('text-align','left');


			});	
}


function eemail_submit_ajax(url,app_id)
{   
	txt_email_newsletter = document.getElementById("eemail_txt_email");

	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(txt_email_newsletter.value=="")
    {
        alert("Please enter email address.");
        txt_email_newsletter.focus();
        return false;    
    }
	if(txt_email_newsletter.value!="" && (txt_email_newsletter.value.indexOf("@",0)==-1 || txt_email_newsletter.value.indexOf(".",0)==-1))
    {
    	var msgValidError = document.getElementById("eemail_txt_email_valid").value;
        alert(msgValidError);
        txt_email_newsletter.focus();
        txt_email_newsletter.select();
        return false;
    }
	if (!filter.test(txt_email_newsletter.value)) 
	{
		var msgValidError = document.getElementById("eemail_txt_email_valid").value;
        alert(msgValidError);
		txt_email_newsletter.focus();
        txt_email_newsletter.select();
		return false;
	}

	if(app_id){
        var rg_url = 'https://readygraph.com/api/v1/wordpress-enduser/'
        var para = "email="+txt_email_newsletter.value+"&app_id="+app_id
		eemail_submitpostrequest(rg_url,para)
	}
	//  if (['test@test.com', '123@123.com', 'test123@test.com'].indexOf(txt_email_newsletter.value) >= 0) 
	//	{
	//		alert('Please provide a valid email address. 3');
	//		txt_email_newsletter.focus();
	//      txt_email_newsletter.select();
	//		return false;
	//	}

	document.getElementById("eemail_msg").innerHTML=document.getElementById("footer-subscribe-loading").value;
	var date_now = "";
    var mynumber = Math.random();
	var str= "txt_email_newsletter="+ encodeURI(txt_email_newsletter.value) + "&timestamp=" + encodeURI(date_now) + "&action=email_subscribe_ajax";
	eemail_submitpostrequest(MPPF_Ajax.ajaxUrl, str);
}

var http_req = false;
function eemail_submitpostrequest(url, parameters) 
{
	http_req = false;
	if (window.XMLHttpRequest) 
	{
		http_req = new XMLHttpRequest();
		if (http_req.overrideMimeType) 
		{
			http_req.overrideMimeType('text/html');
		}
	} 
	else if (window.ActiveXObject) 
	{
		try 
		{
			http_req = new ActiveXObject("Msxml2.XMLHTTP");
		} 
		catch (e) 
		{
			try 
			{
				http_req = new ActiveXObject("Microsoft.XMLHTTP");
			} 
			catch (e) 
			{
				
			}
		}
	}
	if (!http_req) 
	{
		alert('Cannot create XMLHTTP instance');
		return false;
	}
	http_req.onreadystatechange = eemail_submitresult;
	http_req.open('POST', MPPF_Ajax.ajaxUrl, true);
	http_req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_req.setRequestHeader("Content-length", parameters.length);
	http_req.setRequestHeader("Connection", "close");
	http_req.send(parameters);
}


function eemail_submitresult() 
{
	//alert(http_req.readyState);
	//alert(http_req.responseText); 
	if (http_req.readyState == 4) 
	{
		if (http_req.status == 200) 
		{
		 	if (http_req.readyState==4 || http_req.readyState=="complete")
			{ 
				if((http_req.responseText).trim() == "subscribed-successfully")
				{
					document.getElementById("eemail_msg").innerHTML = document.getElementById("footer-subscribe-success").value;
					document.getElementById("eemail_txt_email").value="";
				}
				else if((http_req.responseText).trim() == "subscribed-pending-doubleoptin")
				{
					alert('You have successfully subscribed to the newsletter. You will receive a confirmation email in few minutes.\nPlease follow the link in it to confirm your subscription.\nIf the email takes more than 15 minutes to appear in your mailbox, please check your spam folder.');
					document.getElementById("eemail_msg").innerHTML = document.getElementById("footer-subscribe-success").value;
				}
				else if((http_req.responseText).trim() == "already-exist")
				{
					document.getElementById("eemail_msg").innerHTML = document.getElementById("footer-subscribe-exist").value;
				}
				else if((http_req.responseText).trim() == "unexpected-error")
				{
					document.getElementById("eemail_msg").innerHTML = document.getElementById("footer-subscribe-error").value;
				}
				else if((http_req.responseText).trim() == "invalid-email")
				{
					document.getElementById("eemail_msg").innerHTML = document.getElementById("footer-subscribe-invalid").value;
				}
				else
				{
					document.getElementById("eemail_msg").innerHTML = document.getElementById("footer-subscribe-try").value;
					document.getElementById("eemail_txt_email").value="";
				}
				
				setTimeout(function(){ document.getElementById("eemail_msg").innerHTML = ""; }, 3000);
			} 
		}
		else 
		{
			alert('There was a problem with the request.');
		}
	}
}