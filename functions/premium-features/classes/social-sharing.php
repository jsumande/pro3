<?php

	class MPPremiumFeatures_SocialSharing {

	    private $plugin_path;
	    private $plugin_url;
	    private $plugin_slug;

	    function __construct( $args = array() ) {

			$defaults = array(
				'file' => '',
				'plugin_path' => MPPF_PATH,
				'plugin_url' => MPPF_URL,
				'plugin_slug' => 'mppremiumfeatures',
				'hook_prefix' => 'mppremiumfeatures',		
			);

			$instance = wp_parse_args( $args, $defaults );

	    	$this->plugin_unique_id = $instance['file'];
	        $this->plugin_path = $instance['plugin_path'];
	        $this->plugin_url = $instance['plugin_url'];
	        $this->plugin_slug = $instance['plugin_slug'];
	        $this->hook_prefix = $instance['hook_prefix'];
	        $this->func_hook_prefix = 'mppremiumfeatures_socialsharing_func_';

	    }

		/* Social Sharing Container
		------------------------------------------------------------------------------------------------------------------- */

	    function load_social_sharing( $args = array() ) {

			$defaults = array(
				'post_id' => NULL,
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $mppf, $id;
			$post_id = ( NULL === $post_id ) ? $id : $post_id;

			$social_icons = $mppf->load_option( $mppf->option_group . '_socialsharing_social_sharing_icons' , array( 'facebook' , 'twitter' , 'google-plus' , 'linkedin' ) );

	    	$output = '<div id="mp-premium-features">';

	    		if ( !empty($social_icons) && is_array($social_icons) ) {

			    	$output .= '<div class="mppf-social-sharing">';
				    	$output .= '<span class="mppf-social-icon">'.__( 'Share:' , 'pro' ).'</span>';
				    		foreach ($social_icons as $icon) {
				    			$output .= $this->load_social_btn( array( 'selected' => $icon , 'post_id' => $post_id ) );
				    		}
		    		$output .= '</div>'; // end - mppf-social-sharing

	    		}

	    	$output .= '</div>'; // end - mp-premium-features

	    	return apply_filters( $this->func_hook_prefix . 'load_social_sharing' , $output , $instance );
	    }

		/* Social buttons
		------------------------------------------------------------------------------------------------------------------- */

	    function load_social_btn( $args = array() ) {

			$defaults = array(
				'selected' => '',
				'post_id' => NULL,
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			$post = get_post( $post_id );
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full');
			
			switch ( $selected ) {
				case 'facebook':
					$button = '<a href="https://www.facebook.com/sharer/sharer.php?u='.get_permalink( $post_id ).'&title='.get_the_title( $post_id ).'&img_src='.$image[0].'" target="_blank" class="mppf-social-icon mppf-facebook-icon"><i class="fa fa-facebook"></i></a>';
					break;
				case 'twitter':
					$button = '<a href="https://twitter.com/intent/tweet?text='.get_the_title( $post_id ).'&url='.get_permalink( $post_id ).'" target="_blank" class="mppf-social-icon mppf-twitter-icon"><i class="fa fa-twitter"></i></a>';
					break;
				case 'google-plus':
					$button = '<a href="https://plus.google.com/share?url='.get_permalink( $post_id ).'" target="_blank" class="mppf-social-icon mppf-googleplus-icon"><i class="fa fa-google-plus"></i></a>';
					break;
				case 'linkedin':
					$button = '<a href="https://www.linkedin.com/shareArticle?mini=true&url='.get_permalink( $post_id ).'&title='.get_the_title( $post_id ).'&summary='.( !empty($post->post_excerpt) ? $post->post_excerpt : ( !empty($post->post_content) ? wp_trim_words($post->post_content , apply_filters( $this->hook_prefix . '_social_sharing_desc' , 10 ) ) : '' ) ).'&source='.get_permalink( $post_id ).'" target="_blank" class="mppf-social-icon mppf-linkedin-icon"><i class="fa fa-linkedin"></i></a>';
					break;
				case 'pinterest':
					$button = '<a href="https://pinterest.com/pin/create/button/?url='.get_permalink( $post_id ).'&description='.( !empty($post->post_excerpt) ? $post->post_excerpt : ( !empty($post->post_content) ? wp_trim_words($post->post_content , apply_filters( $this->hook_prefix . '_social_sharing_desc' , 10 ) ) : '' ) ).'&media='.$image[0].'" target="_blank" class="mppf-social-icon mppf-pinterest-icon"><i class="fa fa-pinterest-p"></i></a>';
					break;
				case 'vk':
					$button = '<a href="https://vk.com/share.php?url='.get_permalink( $post_id ).'&title='.get_the_title( $post_id ).'&description='.( !empty($post->post_excerpt) ? $post->post_excerpt : ( !empty($post->post_content) ? wp_trim_words($post->post_content , apply_filters( $this->hook_prefix . '_social_sharing_desc' , 10 ) ) : '' ) ).'&image='.$image[0].'&noparse=true" target="_blank" class="mppf-social-icon mppf-vk-icon"><i class="fa fa-vk"></i></a>';
					break;
				case 'xing':
					$button = '<a href="https://www.xing-share.com/app/user?op=share;sc_p=xing-share;url='.get_permalink( $post_id ).'" target="_blank" class="mppf-social-icon mppf-xing-icon"><i class="fa fa-xing"></i></a>';
					break;

				default:
					$button = '';
					break;
			}

	    	return apply_filters( $this->func_hook_prefix . 'load_social_btn' , $button , $instance );
	    }

	} // end - class MPPremiumFeatures_SocialSharing