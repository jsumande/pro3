<?php

	class MPPremiumFeatures_SellInPost {

	    private $plugin_path;
	    private $plugin_url;
	    private $plugin_slug;

	    function __construct( $args = array() ) {

			$defaults = array(
				'file' => '',
				'plugin_path' => MPPF_PATH,
				'plugin_url' => MPPF_URL,
				'plugin_slug' => 'mppremiumfeatures',
				'hook_prefix' => 'mppremiumfeatures',		
			);

			$instance = wp_parse_args( $args, $defaults );

	    	$this->plugin_unique_id = $instance['file'];
	        $this->plugin_path = $instance['plugin_path'];
	        $this->plugin_url = $instance['plugin_url'];
	        $this->plugin_slug = $instance['plugin_slug'];
	        $this->hook_prefix = $instance['hook_prefix'];
	        $this->func_hook_prefix = 'mppremiumfeatures_sellinpost_func_';

	        // add Sell In Post shortcode
	        add_shortcode( 'sellinpost', array(&$this, 'sell_in_post_sc') );

	    }

		/* Register related CSS & JS
		------------------------------------------------------------------------------------------------------------------- */

	    function load_scripts() {
	    	wp_enqueue_style('bxslider-css', $this->plugin_url . 'css/jquery.bxslider.css', null, '4.1.1');
	    	wp_enqueue_script('bxsliderjs', $this->plugin_url . 'js/jquery.bxslider.min.js', array('jquery'));
	    }

		/* THE shortcode
		------------------------------------------------------------------------------------------------------------------- */

		function sell_in_post_sc( $atts ) {

			$instance = shortcode_atts( array(
				'pid' => NULL,
				'maintitle' => '',
				'mode' => 'single',
				'layout' => 'block',
				'align' => 'left',
				'maxwidth' => 400,
				'showimage' => 'yes',
				'showtitle' => 'yes',
				'showexcerpt' => 'yes',
				'showprice' => 'yes',
				'showbutton' => 'yes',
				'colorskin' => 'black',
				'userclass' => '',
				'inlinestyle' => '',
			), $atts );

			extract( $instance );

			if ( NULL === $pid )
				return;

			$maintitle = (!empty($maintitle) ? esc_attr($maintitle) : '' );
			$userclass = (!empty($userclass) ? esc_attr($userclass) : '' );
			$inlinestyle = (!empty($inlinestyle) ? esc_attr($inlinestyle) : '' );

			// load required scripts
			if ( $mode == 'carousel' )
				$this->load_scripts();

			switch ($align) {
				case 'left':
				default:
					$align = ' mppf-sip-alignleft';
					break;
				case 'right':
					$align = ' mppf-sip-alignright';
					break;			
			}

			switch ($colorskin) {
				case 'black':
				default:
					$colorskin = '';
					break;
				case 'blue':
					$colorskin = ' mppf-sip-blue';
					break;
				case 'lightblue':
					$colorskin = ' mppf-sip-lightblue';
					break;
				case 'yellow':
					$colorskin = ' mppf-sip-yellow';
					break;
				case 'red':
					$colorskin = ' mppf-sip-red';
					break;				
				case 'green':
					$colorskin = ' mppf-sip-green';
					break;
				case 'grey':
					$colorskin = ' mppf-sip-grey';
					break;
			}

			$args = array( 
				'product_id' => $pid,
				'maintitle' => $maintitle,
				'mode' => $mode,
				'align' => $align,
				'maxwidth' => intval($maxwidth),
				'colorskin' => $colorskin,
				'class' => $userclass,
				'style' => $inlinestyle,
				'showimage' => ( $showimage == 'no' ? false : true ),
				'showtitle' => ( $showtitle == 'no' ? false : true ),
				'showexcerpt' => ( $showexcerpt == 'no' ? false : true ),
				'showprice' => ( $showprice == 'no' ? false : true ),
				'showbutton' => ( $showbutton == 'no' ? false : true ),
			);

			$output = $this->load_product_box( $args );

			return apply_filters( $this->func_hook_prefix . 'sell_in_post_sc' , $output , $instance );		
		}

		/* Product Box
		------------------------------------------------------------------------------------------------------------------- */

	    function load_product_box( $args = array() ) {

			$defaults = array(
				'product_id' => NULL,
				'maintitle' => '',
				'mode' => 'single',
				'maxwidth' => 400,
				'align' => '',
				'class' => '',
				'style' => '',
				'colorskin' => '',
				'showimage' => true,
				'showtitle' => true,
				'showexcerpt' => true,
				'showprice' => true,
				'showbutton' => true,
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			$class = ( !empty($class) ? ' ' . esc_attr( $class ) : '' );
			$style = ( !empty($style) ? ' ' . esc_attr($style) : '' );

	    	$output = '<div id="mp-premium-features">';

		    	$output .= '<div class="mppf-sip-product-box'.$align.$colorskin.$class.'" style="max-width: '.$maxwidth.'px;'.$style.'">';

		    		$output .= ( !empty($maintitle) ? '<h3 class="mppf-sip-main-title">'.$maintitle.'</h3>' : '' );

		    		if ( $mode == 'carousel' )
		    			$output .= __( 'Carousel mode not available in this version.' , $this->plugin_slug );
		    		else
						$output .= $this->load_single_mode( array( 
			    			'product_id' => $product_id,
			    			'showimage' => $showimage,
			    			'showtitle' => $showtitle,
							'showexcerpt' => $showexcerpt,
							'showprice' => $showprice,
							'showbutton' => $showbutton,
			    		) );

		    	$output .= '</div>'; // end - mppf-sip-product-box

	    	$output .= '</div>'; // end - mp-premium-features

	    	return apply_filters( $this->func_hook_prefix . 'load_product_box' , $output , $instance );
	    }

		/* Layou
		------------------------------------------------------------------------------------------------------------------- */

	    function load_single_mode( $args = array() ) {

			$defaults = array(
				'product_id' => NULL,
				'showimage' => true,
				'showtitle' => true,
				'showexcerpt' => true,
				'showprice' => true,
				'showbutton' => true,
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $mp, $mppf;

			$product = get_post( $product_id );

	    	$output = '<div itemscope itemtype="http://schema.org/Product" class="hentry mppf-sip-single">';

	    		if ( $showimage ) {
			    	$image_output = '<div class="mppf-sip-product-image">';
		    			if ( has_post_thumbnail( $product_id ) )
		    				$image_output .= get_the_post_thumbnail( $product_id , 'medium' );
		    			else 
		    				$image_output .= '<img src="' . apply_filters('mp_default_product_img', $mp->plugin_url . 'images/default-product.png') . '" class="mp-default-product-img" />';
		    			$image_output .= '<a href="'.get_permalink($product_id).'" class="mppf-sip-product-image-link"><i class="fa fa-plus"></i></a>';
		    			$image_output .= ( function_exists('mp_pinit_button') ? '<span class="mppf-sip-pinit-btn">' . mp_pinit_button( $product_id , 'all_view') . '</span>' : '' );
			    	$image_output .= '</div>'; // end - mppf-sip-product-image
			    	$output .= apply_filters( $this->hook_prefix . '_sip_single_mode_product_image' , $image_output , $instance );
	    		}

		    	$output .= '<div class="mppf-sip-product-contents">';

			    	if ( $showtitle )
			    		$output .= apply_filters( $this->hook_prefix . '_sip_single_mode_product_name' , '<h4 itemprop="name" class="entry-title mppf-sip-product-name"><a href="'.get_permalink($product_id).'">'.get_the_title( $product_id ).'</a></h4>' , $instance );

		    		if ( $showprice )
			    		$output .= apply_filters( $this->hook_prefix . '_sip_single_mode_product_price' , '<div class="mppf-sip-product-price">' . mp_product_price( false , $product_id ) . '</div>' , $instance );

			    	if ( $showexcerpt )
			    		$output .= apply_filters( $this->hook_prefix . '_sip_single_mode_product_excerpt' , '<div itemprop="description" class="mppf-sip-product-excerpt">' . wpautop( !empty($product->post_excerpt) ? wp_trim_words( $product->post_excerpt ) : wp_trim_words($product->post_content , 15 ) ) . '</div>' , $instance );

			    	if ( $showbutton )
		    			$output .= apply_filters( $this->hook_prefix . '_sip_single_mode_product_buy_btn' , '<div class="mppf-sip-buy-button">' . $mppf->load_buy_options( array(
								'post_id' => $product_id,
								'context' => 'list',
								'btnclass' => $mppf->get_btn_color(),
		    				) ) . '</div>' , $instance );

	    			$output .= '</div>'; // end - mppf-sip-product-contents

		    	$output .= '<div class="clear"></div>';

		    	$output .= '
						<div style="display:none">
							<span class="entry-title">' . get_the_title( $product_id ) . '</span>'.__( ' was last modified:' , $this->plugin_slug ).'
							<time class="updated">' . get_the_time( 'Y-m-d\TG:i' , $product_id ) . '</time>'.__( ' by' , $this->plugin_slug ).'
							<span class="author vcard"><span class="fn">' . get_the_author_meta( 'display_name' , $product->post_author ) . '</span></span>
						</div>';

	    	$output .= '</div>'; // end - mppf-sip-single

	    	return apply_filters( $this->func_hook_prefix . 'load_single_mode' , $output , $instance );
	    }

	    function load_carousel_mode( $args = array() ) {

			$defaults = array(
				'novalueyet' => NULL,
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

	    	$output = '';

		    	$output .= '';
		    	$output .= '';
		    	$output .= '';
		    	$output .= '';
		    	$output .= '';
		    	$output .= '';

	    	$output .= '';

	    	return apply_filters( $this->func_hook_prefix . 'load_carousel_mode' , $output , $instance );
	    }

	} // end - class MPPremiumFeatures_SellInPost