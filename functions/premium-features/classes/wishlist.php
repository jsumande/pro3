<?php

	class MPPremiumFeatures_WishList {

	    private $plugin_path;
	    private $plugin_url;
	    private $plugin_slug;

	    function __construct( $args = array() ) {

			$defaults = array(
				'file' => '',
				'plugin_path' => MPPF_PATH,
				'plugin_url' => MPPF_URL,
				'plugin_slug' => 'mppremiumfeatures',
				'hook_prefix' => 'mppremiumfeatures'	
			);

			$instance = wp_parse_args( $args, $defaults );

	    	$this->plugin_unique_id = $instance['file'];
	        $this->plugin_path = $instance['plugin_path'];
	        $this->plugin_url = $instance['plugin_url'];
	        $this->plugin_slug = $instance['plugin_slug'];
	        $this->hook_prefix = $instance['hook_prefix'];
	        $this->func_hook_prefix = 'mppremiumfeatures_wishlist_func_';

	        // shortcode for wishlist table
	        if ( shortcode_exists( 'wishlist' ) )
	        	add_shortcode( 'mppfwishlist', array(&$this, 'display_wishlist_table_sc') );
	        else
	        	add_shortcode( 'wishlist', array(&$this, 'display_wishlist_table_sc') );

	        // backend user profile
	        add_action( 'show_user_profile', array(&$this, 'load_wishlist_in_profile') , 15 );
	        add_action( 'profile_update', array(&$this, 'update_wishlist_in_profile') , 15 );

	        // update wishlist via ajax
    		add_action( 'wp_ajax_nopriv_mppf-add-to-wishlist', array(&$this, 'add_item_to_wishlist') );
    		add_action( 'wp_ajax_mppf-add-to-wishlist', array(&$this, 'add_item_to_wishlist') );

	    }

		/* Wishlit button
		------------------------------------------------------------------------------------------------------------------- */

	    function load_wishlist_btn( $args = array() ) {
				$defaults = array(
					'post_id' => NULL,
					'context' => 'single',
					'integration' => 'none',
					'btnclass' => '',
					'btntext' => __( 'Add to Wishlist' ,  'pro' ),
					'addedbtntext' => __( 'Item In Wishlist' ,  'pro' )
				);	
				
			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

    		global $id;
    		$post_id = ( NULL === $post_id ) ? $id : $post_id;
    		$blog_id = is_multisite() ? get_current_blog_id() : 1;

    		$btntext = apply_filters( $this->hook_prefix . '_wishlist_btn_text' , $btntext , $instance );
    		$addedbtntext = apply_filters( $this->hook_prefix . '_in_wishlist_btn_text' , $addedbtntext , $instance );
	    	$output = '<form style="display:none" class="mppf-wishlist-form mppf-options-form" method="post" action="'.get_permalink( $post_id ).'">';
		    	$output .= '<button id="single_product_page_wishlist_button_live" class="mppf-btn'.$btnclass.( $this->item_in_wishlist($post_id) ? ' mppf-btn-disabled' : '' ).( $context == 'list' ? ' mppf-btn-block' : '' ).' mppf-addtowishlist-btn'.( $context == 'single' ? '' : ' add-fadein-effects-parent' ).'" type="submit">';
				
		    		//$output .= '<i class="fa '.( $this->item_in_wishlist($post_id) ? 'fa-heart' : 'fa-heart-o' ).( $context == 'single' ? '' : ' add-fadein-effects-child' ).'"></i>';
		    		$output .= '<span class="mppf-btn-text'.( $this->item_in_wishlist($post_id) || $context == 'single' ? ' mppf-show' : '' ).( $context == 'single' ? '' : ' add-fadein-effects-child' ).'">'.( $this->item_in_wishlist($post_id) ? $addedbtntext : $btntext ).'</span>';
		    	$output .= '</button>';
				$output .= '<input type="hidden" name="mppf_wl_item" id="mppf_wl_item" value="'.$post_id.'" />';
				$output .= is_multisite() ? '<input type="hidden" name="currentblogid" id="currentblogid" value="'.$blog_id.'" />' : '';
				$output .= '<input type="hidden" name="action" value="mppf-add-to-wishlist" />';
				
		    	$output_b .= '<div class="col-lg-4 col-md-12 col-sm-4 col-xm-12 padding-sm"><button id="single_product_page_wishlist_button" class="col-md-4 mppf-btn'.$btnclass.( $this->item_in_wishlist($post_id) ? ' mppf-btn-disabled' : '' ).( $context == 'list' ? ' mppf-btn-block' : '' ).' mppf-addtowishlist-btn'.( $context == 'single' ? '' : ' add-fadein-effects-parent' ).'" type="submit">';
		    		//$output .= '<i class="fa '.( $this->item_in_wishlist($post_id) ? 'fa-heart' : 'fa-heart-o' ).( $context == 'single' ? '' : ' add-fadein-effects-child' ).'"></i>';
		    		$output_b .= '<span class="mppf-btn-text'.( $this->item_in_wishlist($post_id) || $context == 'single' ? ' mppf-show' : '' ).( $context == 'single' ? '' : ' add-fadein-effects-child' ).'">'.( $this->item_in_wishlist($post_id) ? $addedbtntext : $btntext ).'</span>';
		    	$output_b .= '</button></div>';
				
	    	$output .= '</form>'.$output_b; // end - mppf-wishlist-form

	    	return apply_filters( $this->func_hook_prefix . 'load_wishlist_btn' , $output , $instance );
	    }

		/* Add Items to Wishlist
		------------------------------------------------------------------------------------------------------------------- */

		function add_item_to_wishlist() {

			$blog_id =  is_multisite() ? ( isset( $_POST['currentblogid'] ) ? esc_attr(intval($_POST['currentblogid'])) : 1 ) : 1;
			$post_id = isset( $_POST['mppf_wl_item'] ) ? esc_attr(intval($_POST['mppf_wl_item'])) : '';
			$currentpage = get_permalink( $post_id );

			if ( !is_user_logged_in() ) {
				$error = '<a href="'.wp_login_url( $currentpage ).'" class="mppf-btn mppf-btn-red"><span class="mppf-btn-text mppf-show">' . __( 'Kindly login first' , 'pro' ) . '</span></a>';
				$error_type = 'notlogin';
			} elseif ( $this->item_in_wishlist($post_id) ) {
				$error = '<span class="mppf-btn-text mppf-show">' . __( 'Item already in the list.' , 'pro' ) . '</span>';
				$error_type = 'inthelist';
			} else {
				$error = '';
				$error_type = 'none';
			}

			$error = apply_filters( $this->hook_prefix . '_wishlist_error_tag', $error , $post_id , $currentpage );

			if ( is_user_logged_in() ) {

			    global $current_user;

			    //$wishlist = array();
			    $meta = get_user_meta($current_user->ID, 'pro_wishlist_items' , true );
			    $notinlist = $this->item_in_wishlist($post_id) ? false : true;

			    if (!empty($post_id) && $notinlist ) {

				    if (is_array($meta)) {
				    	$meta[] = array( 'item' => $post_id , 'blog' => $blog_id, 'date' => current_time( 'mysql' ));
				    } else {
				    	$meta = array();
				    	$meta[] = array( 'item' => $post_id , 'blog' => $blog_id, 'date' => current_time( 'mysql' ));
				    }
			    	update_user_meta($current_user->ID, 'pro_wishlist_items' , $meta);

			    } else {
					if (defined('DOING_AJAX') && DOING_AJAX) {
						echo apply_filters( $this->func_hook_prefix . 'add_item_to_wishlist' , 'inthelist|SEP|' . $error );
						exit;
					}
				} 

			} else {
				if (defined('DOING_AJAX') && DOING_AJAX) {
					echo apply_filters( $this->func_hook_prefix . 'add_item_to_wishlist' , 'notlogin|SEP|' . $error );
					exit;
				}
			}
		}


		/* Wishlit Table
		------------------------------------------------------------------------------------------------------------------- */
function display_wishlist_table_sc_test( $atts = array() ) {
?>
<div class="row">
		<div class="search-wish-product">
			<input type="text" class="search">
			<button class="search-product">SEARCH</button>
		</div>
</div>
<?php
}
		function display_wishlist_table_sc( $atts = array() ) {

			extract( shortcode_atts( array(
				'showtitle' => 'no',
				'title' => __( 'My Wishlist:' , 'pro' ),
				'hideprice' => 'no',
				'btncolor' => 'black',
				'style' => '',
				'class' => ''
			), $atts ) );

			$style = ( !empty($style) ? esc_attr($style) : '' );
			$class = ( !empty($class) ? esc_attr($class) : '' );
			$showtitle = ( !empty($showtitle) ? esc_attr($showtitle) : '' );
			$title = ( !empty($title) ? esc_attr($title) : '' );

			switch ($btncolor) {
				case 'grey':
					$btnclass = '';
					break;
				case 'blue':
					$btnclass = ' mppf-btn-blue';
					break;
				case 'lightblue':
					$btnclass = ' mppf-btn-lightblue';
					break;
				case 'green':
					$btnclass = ' mppf-btn-green';
					break;
				case 'yellow':
					$btnclass = ' mppf-btn-yellow';
					break;
				case 'red':
					$btnclass = ' mppf-btn-red';
					break;
				case 'black':
				default:
					$btnclass = ' mppf-btn-black';
					break;				
			}

			$currentpage = get_permalink();

			if ( is_user_logged_in() )
				$output = $this->load_wishlist_table( array( 'showtitle' => ( $showtitle == 'yes' ? true : false ) , 'title' => $title , 'hideprice' => ( $hideprice == 'yes' ? true : false ) , 'btnclass' => $btnclass , 'class' => $class , 'style' => $style ) );
			else
				$output = '<div id="mp-premium-features"><div class="wish-login-link">'.__( 'Kindly ' , 'pro' ).' <a href="'.wp_login_url( $currentpage ).'">'.__( 'login' , 'pro' ).'</a> '.__( ' to view your wish list.' , 'pro' ).'</div></div>';

			return apply_filters( $this->func_hook_prefix . 'display_wishlist_table_sc' , $output , $atts );
		}

		function load_wishlist_table( $args = array() ) {

			$defaults = array(
				'showtitle' => true,
				'title' => __( 'My Wishlist:' , 'pro' ),
				'hideprice' => false,
				'btnclass' => '',
				'class' => '',
				'style' => ''
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );			

		    global $current_user, $mp, $mppf;

			$style = ( !empty($style) ? ' style="'.esc_attr($style).'"' : '' );
			$class = ( !empty($class) ? ' '.esc_attr($class) : '' );
			$message = '';

		    if (isset($_REQUEST['user_id'])) {
		      $user_id = $_REQUEST['user_id'];
		    } else {
		      $user_id = $current_user->ID;
		    }

		    $meta = get_user_meta($user_id, 'pro_wishlist_items', true);

		    if (is_array($meta) && isset( $_POST['mppf-update-wishlist-items'] ) ) {

			    foreach ($meta as $key => $value) {
			    	if ( isset( $_POST['mppf-remove-wishlist-'.$key] ) ) {
			    		if ( $_POST['mppf-remove-wishlist-'.$key] == '1' ) {
			    			unset($meta[$key]);
			    		}
			    	}
			    }

			    $meta = array_values($meta);
			    $updatesuccess = update_user_meta( $user_id, 'pro_wishlist_items', $meta );
			    $message = ( $updatesuccess ? '<div class="mppf-alert-message mppf-block-message mppf-message-green"><i class="fa fa-check"></i>'.apply_filters( $this->hook_prefix . '_wishlist_updated_text_success' , __( 'Wishlist Updated!' , 'pro' ) ).'</div>' : '<div class="mppf-alert-message mppf-block-message mppf-message-red"><i class="fa fa-times"></i>'.apply_filters( $this->hook_prefix . '_wishlist_updated_text_error' , __( 'Failed to update wishlist. Please try again.' , 'pro' ) ).'</div>' );
		    }
				
		    $output = '<div id="mp-premium-features">';

		    	$output .= ( $showtitle ? '<h3 class="mppf-display-wishlist-title">'.__( $title , 'pro' ).'</h3>' : '' );

		    	$output .= $message;

			    $output .= '<form id="mppf-display-wishlist" class="mppf-wishlist-form" method="post">';	    	

				    $output .= '<table class="mppf-wishlist-table mppf-table'.$class.'"'.$style.'>';

				    	$output .= '<thead class="mppf-table-head"><tr class="class="mppf-table-head-row">';
							$output .= '<th class="mppf-table-head-status"><div class="input-control-checkbox">
													<input class="wishcheckbox" type="checkbox" id="select_all_boxes" />
													<label>'.__('Status', 'pro' ).'</label>
												</div></th>';
							$output .= '<th class="mppf-table-head-date">'.__('Date', 'pro' ).'</th>';
				    		$output .= '<th class="mppf-table-head-product">'.__('Product', 'pro' ).'</th>';
							$output .= '<th class="mppf-table-head-username">'.__('User name', 'pro' ).'</th>';
				    		$output .= ( $mp->get_setting('disable_cart') && $hideprice ? '' : '<th class="mppf-table-head-price">'.__('Price', 'pro' ).'</th>' );
				    		$output .= '<th class="mppf-table-head-action">'.__('Action', 'pro' ).'</th>';

				    	$output .= '</tr></thead>';

				    	$output .= '<tbody class="mppf-table-body">';

				    	$current_blog_id = get_current_blog_id();
							
				    	if (is_array($meta) && !empty($meta)) {
							$count_searched = 0;
				    		foreach ($meta as $key => $value) {
					            if (!empty($value)) {

					            	$blog_id = esc_html(esc_attr(trim($value['blog'])));
									
									if(isset($value['date']))
										$date_added = esc_html(esc_attr(trim($value['date'])));
									else
										$date_added = '';
									
					            	if (is_multisite() && !empty($blog_id))
					            		switch_to_blog($blog_id);
					            	$item_id = esc_html(esc_attr(trim($value['item'])));

									$status = check_stock_status($item_id);
									
									$is_on_wish = true;
									if(isset($_GET['search']) && !empty($_GET['search'])) {
										if (strpos(strtolower(get_the_title($item_id)),strtolower($_GET['search'])) !== false) 
											$is_on_wish = true;
										else
											$is_on_wish = false;
									}
									
									if($is_on_wish && get_post_status( $item_id ) == 'publish'){
						            $output .= '<tr class="mppf-table-item-row">';
										$output .= '<td class="mppf-table-head-status"><div class="input-control-checkbox">
													<input type="hidden" name="mppf-remove-wishlist-'.$key.'" value="0" />
													<input type="checkbox" class="wishcheckbox" name="mppf-remove-wishlist-'.$key.'" id="mppf-remove-wishlist-'.$key.'" value="1" />
													<label>'.(($status)? __('Sold', 'pro') : __('For sale', 'pro') ).'</label>
												</div></td>';			
										$output .= '<td>'.date_i18n("d.F Y", strtotime($date_added)).'</td>';		
						            	$output .= '<td class="mppf-table-item-image"><div class="row wish-owner-data"><div class="col-md-6 wish-owner-pic">'.mp_product_image( false, 'widget', $item_id, 53 ).'</div><div class="col-md-6 wish-owner-desc"><a href="'.localize_url(get_permalink($item_id)).'" class="mppf-table-item-name">'.get_the_title($item_id).'</a><br/>'.break_text(get_post_field('post_content', $item_id),150).'</div></div></td>';
										
										$data_owner = '';
										if (!empty($blog_id)){
											
											$store_owner = get_storeowner_data($blog_id);
											if($store_owner !== false){
													$data_owner = '<div class="wish-owner-data"><div class="wish-owner-pic"><a href="'.localize_url($store_owner['site_url']).'"><img src="'.$store_owner['pictureurl'].'" alt="'.$store_owner['storename'].'" /></a></div> <div class="wish-owner-site"><a href="'.localize_url($store_owner['site_url']).'">'.$store_owner['storename'].'</a></div></div>';
											}	

										}	
									
										$output .= '<td>'.$data_owner.'</td>';
						               	$output .= ( $mp->get_setting('disable_cart') && $hideprice ? '' : '<td class="mppf-table-item-price">'.$this->load_wishlist_price( array( 'post_id' => $item_id , 'context' => 'frontpage' ) ).'</td>' );
						               	$output .= '<td class="mppf-table-item-action">';
												//Jeff
							               		/*$output .= $mppf->load_buy_button( array( 
													'post_id' => $item_id,
													'product_page_link' => true,
													'context' => 'list',
													'showbtntext' => true,
													'btnclass' => 'btn btn-primary btn-small',
							               		 ) );
							               		$output .= '<div class="clear"></div>'; */
												//Jeff end
												$output .= '<a href="'.localize_url(get_permalink($item_id)).'" class="btn btn-primary btn-small'.(($status)? ' mppf-btn-disabled' : '').'"><i class="fa fa-shopping-cart"></i>&nbsp; '.__('Shop Now','pro').'</a>';
							               		//$output .= '<input type="hidden" name="mppf-remove-wishlist-'.$key.'" value="0" /><label><input type="checkbox" name="mppf-remove-wishlist-'.$key.'" id="mppf-remove-wishlist-'.$key.'" value="1" /> '. __( 'Remove from wishlist' , 'pro'  ).'</label>';
						               	$output .= '</td>'; // end - mppf-table-item-action
						            $output .= '</tr>';
										++$count_searched;
									}

					            	if (is_multisite()) 
					            		restore_current_blog();
					            }
				    		}
							
							if($count_searched == 0 && isset($_GET['search']) && !empty($_GET['search']))
								$output .= '<tr class="mppf-table-item-row"><td colspan="6" class="mppf-table-item-empty"><span class="mppf-wishlist-empty-list">'.__( 'No wish list found in your search.' , 'pro'  ).'</span></td></tr>';
							
				    	} else {
				    		$output .= '<tr class="mppf-table-item-row"><td colspan="6" class="mppf-table-item-empty"><span class="mppf-wishlist-empty-list">'.__( 'Your wish list is currently empty.' , 'pro'  ).'</span></td></tr>';
				    	}
						
						if ( is_multisite() ) {
							$after_restore_blog_id = get_current_blog_id();
							if ( $current_blog_id != $after_restore_blog_id )
								switch_to_blog( $current_blog_id );
						}

				        $output .= '</tbody>';
						
						$output .= '<tfoot><tr class="mppf-table-item-row"><td colspan="6" class="mppf-table-item-selectall"><div class="input-control-checkbox"><input class="wishcheckbox" type="checkbox" id="select_all_boxes2" /><label>'.__('Select all', 'pro' ).'</label></div></td></tr></tfoot>';						

				    $output .= '</table>'; // end - mppf-wishlist-table
					
				    		$output .= '<button type="submit" class="wishlist-delete-button">'.__( 'Delete' , 'pro'  ).'</button>';
				    		$output .= '<input type="hidden" name="mppf-update-wishlist-items" value="mppf-update-wishlist-items" />';
				    	
			    $output .= '</form>'; // end - mppf-wishlist-form

			$output .= '</div>'; // end - mp-premium-features

		    return apply_filters( $this->func_hook_prefix . 'load_wishlist_table' , $output , $instance );
		}

		/* Wishlit price
		------------------------------------------------------------------------------------------------------------------- */

	    function load_wishlist_price( $args = array() ) {

			$defaults = array(
				'post_id' => NULL,
				'context' => 'frontpage'
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $id, $mp;
			$post_id = ( NULL === $post_id ) ? $id : $post_id;

			$meta = get_post_custom($post_id);

			if (empty($meta))
				return;

		  	//unserialize
		  	foreach ($meta as $key => $val) {
			  $meta[$key] = maybe_unserialize($val[0]);
			  if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file" && $key != "mp_price_sort")
			    $meta[$key] = array($meta[$key]);
			}

		  	if ( is_array($meta["mp_price"]) && count($meta["mp_price"]) == 1 ) {
			    if ($meta["mp_is_sale"]) {
				    $price = '<span class="mp_special_price"><del class="mp_old_price">'.$mp->format_currency('', $meta["mp_price"][0]).'</del>';
				    $price .= '<span itemprop="price" class="mp_current_price">'.$mp->format_currency('', $meta["mp_sale_price"][0]).'</span></span>';
				  } else {
				    $price = '<span itemprop="price" class="mp_normal_price"><span class="mp_current_price">'.$mp->format_currency('', $meta["mp_price"][0]).'</span></span>';
				  }
			} else { 
		    
			    if ($meta["mp_is_sale"]) {
			      //do some crazy stuff here to get the lowest price pair ordered by sale prices
			      asort($meta["mp_sale_price"], SORT_NUMERIC);
			      $lowest = array_slice($meta["mp_sale_price"], 0, 1, true);
			      $keys = array_keys($lowest);
			      $mp_price = $meta["mp_price"][$keys[0]];
			      $mp_sale_price = array_pop($lowest);
				    $price = __('From', 'pro' ).' <span class="mp_special_price"><del class="mp_old_price">'.$mp->format_currency('', $mp_price).'</del>';
				    $price .= '<span itemprop="price" class="mp_current_price">'.$mp->format_currency('', $mp_sale_price).'</span></span>';
				  } else {
			      sort($meta["mp_price"], SORT_NUMERIC);
				    $price = __('From', 'pro' ).' <span itemprop="price" class="mp_normal_price"><span class="mp_current_price">'.$mp->format_currency('', $meta["mp_price"][0]).'</span></span>';
				  }
			}

	    	return apply_filters( $this->func_hook_prefix . 'load_wishlist_price' , $price , $instance );
	    }

	    /* Check if product in wishlist
		------------------------------------------------------------------------------------------------------------------- */

		function item_in_wishlist( $post_id ) {
			global $current_user;
			$meta = get_user_meta( $current_user->ID , 'pro_wishlist_items' , true );
			$blog_id = is_multisite() ? get_current_blog_id() : 1;

			if (is_array($meta)) {

				foreach ($meta as $key => $value) {

					if ( $post_id == $value['item'] && $blog_id == $value['blog'] ) {
						return apply_filters( $this->func_hook_prefix . 'item_in_wishlist' , true , $post_id );
						exit;
					}
				}

				return apply_filters( $this->func_hook_prefix . 'item_in_wishlist' , false , $post_id );

			} else {
				return apply_filters( $this->func_hook_prefix . 'item_in_wishlist' , false , $post_id );
			}
		}

	    /* Wishlist in user profile
		------------------------------------------------------------------------------------------------------------------- */

	    function load_wishlist_in_profile() {

		    global $current_user;

		    if (isset($_REQUEST['user_id'])) {
		      $user_id = $_REQUEST['user_id'];
		    } else {
		      $user_id = $current_user->ID;
		    }

		    $meta = get_user_meta($user_id, 'pro_wishlist_items' , true);

	        $output = '<h3>'.__('My Wishlist', 'pro').':</h3>';

		    $output .= '<table class="form-table">';

		    	$output .= '<thead><tr>';

		    		$output .= '<th><strong>'.__('Item', 'pro').'</strong></th>';
		    		$output .= '<th><strong>'.__('Price', 'pro').'</strong></th>';
		    		$output .= '<th><strong>'.__('Action', 'pro').'</strong></th>';
		    		$output .= '<th></th>';

		    	$output .= '</tr></thead>';

		    	$output .= '<tbody>';

		    	$current_blog_id = get_current_blog_id();

		    	if ( is_array($meta) && !empty($meta) ) {

		    		foreach ($meta as $key => $value) {
		    			
			            if (!empty($value)) {

			            	$blog_id = esc_html(esc_attr(trim($value['blog'])));

			            	if (is_multisite() && !empty($blog_id))
			            		switch_to_blog($blog_id);

			            	$item_id = esc_html(esc_attr(trim($value['item'])));

				            $output .= '<tr>';
				                $output .= '<td>'.get_the_title($item_id).'</td>';
				               	$output .= '<td>'.$this->load_wishlist_price( array( 'post_id' => $item_id , 'context' => 'userprofile' ) ).'</td>';
				               	$output .= '<td><a href="'.get_permalink($item_id).'" target="_blank">'.__( 'Buy Now' , 'pro' ).'</a></td>';
				               	$output .= '<td><input type="hidden" name="mppf-remove-wishlist-'.$key.'" value="0" /><label><input type="checkbox" name="mppf-remove-wishlist-'.$key.'" id="mppf-remove-wishlist-'.$key.'" value="1" /> '. __( 'Remove from wishlist' , 'pro' ).'</label></td>';
				            $output .= '</tr>';

			            	if (is_multisite())
			            		restore_current_blog();
			            }

		    		}

		    	} else {
		    		$output .= '<tr><td><em>'.__( 'Your wishlist is currently empty.' , 'pro' ).'</em></td></tr>';
		    	}

				if ( is_multisite() ) {
					$after_restore_blog_id = get_current_blog_id();
					if ( $current_blog_id != $after_restore_blog_id )
						switch_to_blog( $current_blog_id );
				}

		        $output .= '</tbody>';

		    $output .= '</table>';

	    	echo apply_filters( $this->func_hook_prefix . 'load_wishlist_in_profile' , $output );
	    }

	    function update_wishlist_in_profile() {

		    global $current_user;

		    if (isset($_REQUEST['user_id'])) {
		      $user_id = $_REQUEST['user_id'];
		    } else {
		      $user_id = $current_user->ID;
		    }

		    $meta = get_user_meta($user_id, 'pro_wishlist_items' , true);

		    if (is_array($meta)) {

			    foreach ($meta as $key => $value) {
			    	
			    	if ( isset( $_POST['mppf-remove-wishlist-'.$key] ) ) {

			    		if ( $_POST['mppf-remove-wishlist-'.$key] == '1' ) {
			    			unset($meta[$key]);
			    		}
			    	}

			    }

			    $meta = array_values($meta);
			    update_user_meta($user_id, 'pro_wishlist_items' , $meta); 
		    }
	    }

	} // end - class MPPremiumFeatures_WishList