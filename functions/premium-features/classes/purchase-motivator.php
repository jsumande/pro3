<?php

	class MPPremiumFeatures_PurchaseMotivator {

	    private $plugin_path;
	    private $plugin_url;
	    private $plugin_slug;

	    function __construct( $args = array() ) {

			$defaults = array(
				'file' => '',
				'plugin_path' => MPPF_PATH,
				'plugin_url' => MPPF_URL,
				'plugin_slug' => 'mppremiumfeatures',
				'hook_prefix' => 'mppremiumfeatures',		
			);

			$instance = wp_parse_args( $args, $defaults );

	    	$this->plugin_unique_id = $instance['file'];
	        $this->plugin_path = $instance['plugin_path'];
	        $this->plugin_url = $instance['plugin_url'];
	        $this->plugin_slug = $instance['plugin_slug'];
	        $this->hook_prefix = $instance['hook_prefix'];
	        $this->func_hook_prefix = 'mppremiumfeatures_purchasemotivator_func_';

			// add message box into cs modal
			add_filter( $this->hook_prefix . '_cs_modal_after_title', array(&$this, 'message_box_container') , 15 );

			// add message box into checkout page
			add_filter( 'mp_checkout_error_checkout', array(&$this, 'message_box_container_checkout') , 9 );

			// add message box into widget cart
			add_filter( 'mp_show_cart', array(&$this, 'message_box_container_widget_cart') , 11 , 3 );

			// ajax filter
			add_action( 'wp_ajax_nopriv_mppf-mp-message-update', array(&$this, 'update_message') );
			add_action( 'wp_ajax_mppf-mp-message-update', array(&$this, 'update_message') );

			// add message into order email
			add_filter( 'mp_shipped_order_notification' , array(&$this, 'edit_order_email') , 10 , 2);
   
	    }

		/* Ajax Update
		------------------------------------------------------------------------------------------------------------------- */

	    function update_message() {

	    	global $mppf;
	    	$enabledmotivator = $mppf->load_option( $mppf->option_group . '_purchasemotivator_enable_motivator' , 'no' );

	    	if ( $enabledmotivator == 'yes' )
	    		$output = $this->load_message_box( array( 'class' => 'mppf-pm-message-box-cs' ) );
	    	else
	    		$output = '';

	      	if (defined('DOING_AJAX') && DOING_AJAX) {
	        	echo apply_filters( $this->func_hook_prefix . 'update_message' , $output );
				exit;
	      	}
	    }

		/* Message Box Container
		------------------------------------------------------------------------------------------------------------------- */

	    function message_box_container_checkout( $contents = NULL ) {

	        global $mppf, $wp_query;

	        $checkoutstep = get_query_var( 'checkoutstep' );
	        $enabledmotivator = $mppf->load_option( $mppf->option_group . '_purchasemotivator_enable_motivator' , 'no' );

	        if ( ( $checkoutstep == 'checkout-edit' || empty($checkoutstep) ) && $wp_query->query_vars['pagename'] == 'cart' && $enabledmotivator == 'yes' ) {
	        	$output = '<div class="mppf-pm-cs-container">' . $this->load_message_box( array( 'class' => 'mppf-pm-message-box-cs' ) ) . '</div><div class="clear"></div>' . $contents;
	        } else {
	        	$output = $contents;
	        }

	    	return apply_filters( $this->func_hook_prefix . 'message_box_container_checkout' , $output , $contents );
	    }

	    function message_box_container_widget_cart( $contents = NULL , $context = '' , $checkoutstep = null ) {

	        global $mppf;

	        $enabledmotivator = $mppf->load_option( $mppf->option_group . '_purchasemotivator_enable_motivator' , 'no' );

	        if ( $enabledmotivator == 'yes' && $context == 'widget' && ( mp_items_in_cart() || $checkoutstep == 'confirmation' ) ) {
	        	$output = '<div class="mppf-pm-cs-container">' . $this->load_message_box( array( 'class' => 'mppf-pm-message-box-cs' ) ) . '</div><div class="clear"></div>' . $contents;
	        } else {
	        	$output = $contents;
	        }

	    	return apply_filters( $this->func_hook_prefix . 'message_box_container_widget_cart' , $output , $contents );
	    }

	    function message_box_container() {

	        global $mppf;
	        $enabledmotivator = $mppf->load_option( $mppf->option_group . '_purchasemotivator_enable_motivator' , 'no' );

	    	if ( $enabledmotivator == 'yes' )
	    		$output = $this->load_message_box( array( 'class' => 'mppf-pm-message-box-cs' ) );
	    	else
	    		$output = '';

	    	return apply_filters( $this->func_hook_prefix . 'message_box_container' , '<div class="mppf-pm-cs-container">' . $output . '</div>' );
	    }

		/* Message Box
		------------------------------------------------------------------------------------------------------------------- */

	    function load_message_box( $args = array() ) {

			$defaults = array(
				'class' => '',
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $mp, $mppf;

			$class = ( !empty($class) ? ' ' . esc_attr( $class ) : '' );

			$cartgoal = $mppf->load_option( $mppf->option_group . '_purchasemotivator_cart_goal' , 150 );
			$pitch = $mppf->load_option( $mppf->option_group . '_purchasemotivator_pitch_message' , __( 'AWESOME! Spend [BALANCE] more to get FREE shipping!' , $this->plugin_slug ) );
			$success = $mppf->load_option( $mppf->option_group . '_purchasemotivator_success_message' , __( 'CONGRATS! You are entitled to FREE SHIPPING on your order!' , $this->plugin_slug ) );
			$colorskin = $mppf->load_option( $mppf->option_group . '_purchasemotivator_message_box_color' , 'green' );

			switch ($colorskin) {
				case 'green':
				default:
					$colorclass = ' mppf-message-green';
					break;
				case 'red':
					$colorclass = ' mppf-message-red';
					break;			
				case 'blue':
					$colorclass = ' mppf-message-blue';
					break;	
				case 'yellow':
					$colorclass = ' mppf-message-yellow';
					break;	
			}

			$balance = intval($cartgoal)-$this->get_cart_total();
			$pitch_message = str_replace( '[BALANCE]' , '<strong>' . $mp->format_currency( '' , $balance ) . '</strong>' , $mppf->kses_it($pitch) );;

	    	$output = '<div id="mp-premium-features">';

		    	$output .= '<div class="mppf-pm-message-box mppf-alert-message'.$colorclass.$class.'">';
		    		if ( $balance < 0 )
			    		$output .= $mppf->kses_it($success);
			    	else
			    		$output .= $pitch_message;
		    	$output .= '</div>';

	    	$output .= '</div>'; // end - mp-premium-motivator

	    	return apply_filters( $this->func_hook_prefix . 'load_message_box' , $output , $instance );
	    }

		/* New Order Email
		------------------------------------------------------------------------------------------------------------------- */

	    function edit_order_email( $msg = '' , $order = array() ) {

	    	global $mppf;

			$enabledmotivator = $mppf->load_option( $mppf->option_group . '_purchasemotivator_enable_motivator' , 'no' );
			$message = $mppf->load_option( $mppf->option_group . '_purchasemotivator_order_message' , '' );

			if ( $enabledmotivator == 'yes' )
				$new_msg = str_replace( 'PMMESSAGE' , ( !empty($message) ? esc_attr($message) . "\n\n" : '' ) , $msg );
			else
				$new_msg = $msg;

	    	return apply_filters( $this->func_hook_prefix . 'edit_order_email' , $new_msg , $msg , $order );
	    }

		/* Cart
		------------------------------------------------------------------------------------------------------------------- */

	    function get_cart_total() {

		  	global $mp, $blog_id;
		  	$blog_id = (is_multisite()) ? $blog_id : 1;
		  	$current_blog_id = $blog_id;

			$global_cart = $mp->get_cart_contents(true);

		  	if (!$mp->global_cart)  //get subset if needed
		  		$selected_cart[$blog_id] = $global_cart[$blog_id];
		  	else
		    	$selected_cart = $global_cart;

		    $totals = array();

		    if (!empty($selected_cart) && is_array($selected_cart)) {
			    foreach ($selected_cart as $bid => $cart) {

				if (is_multisite())
			        switch_to_blog($bid);

			    if (!empty($cart) && is_array($cart)){
				   	foreach ($cart as $product_id => $variations) {
				        foreach ($variations as $variation => $data) {
				          $totals[] = $data['price'] * $data['quantity'];
				        }
				      }
				    }
			    }
		    } 
		    
			if (is_multisite())
		      switch_to_blog($current_blog_id);

		    $total = array_sum($totals);

	    	return apply_filters( $this->func_hook_prefix . 'get_cart_total' , $total );
	    }

	} // end - class MPPremiumFeatures_PurchaseMotivator