<?php

	class MPPremiumFeatures_ImageTaxomony {

	    private $plugin_path;
	    private $plugin_url;
	    private $plugin_slug;

	    function __construct( $args = array() ) {

			$defaults = array(
				'file' => '',
				'plugin_path' => MPPF_PATH,
				'plugin_url' => MPPF_URL,
				'plugin_slug' => 'mppremiumfeatures',
				'hook_prefix' => 'mppremiumfeatures',		
			);

			$instance = wp_parse_args( $args, $defaults );

	    	$this->plugin_unique_id = $instance['file'];
	        $this->plugin_path = $instance['plugin_path'];
	        $this->plugin_url = $instance['plugin_url'];
	        $this->plugin_slug = $instance['plugin_slug'];
	        $this->hook_prefix = $instance['hook_prefix'];
	        $this->func_hook_prefix = 'mppremiumfeatures_imagetaxonomy_func_';

	        define( 'MPPF_GLOBAL_TAXONOMY_WIDGET_RULES' , apply_filters( $this->hook_prefix . '_global_taxonomy_widget_rules' , true ) );

	        // register widget
	        add_action( 'widgets_init' , create_function('', 'return register_widget("MPPF_Image_Taxonomy_Widget");') );
	        if ( is_multisite() && ( true == MPPF_GLOBAL_TAXONOMY_WIDGET_RULES ) )
	        	add_action( 'widgets_init' , create_function('', 'return register_widget("MPPF_Global_Image_Taxonomy_Widget");') );
	    }

		/* Load Menu
		------------------------------------------------------------------------------------------------------------------- */

	    function load_taxonomy_menu( $args = array() ) {

			$defaults = array(
				'global' => false,
				'termslug' => NULL,
				'taxonomy' => 'product_category',
				'count' => 'false',
				'imagesize' => array( 50 , 50 )
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $wp_query;

			if ( NULL == $termslug ) {

				if ( $global && get_query_var('global_taxonomy') )
					$termslug = esc_attr( get_query_var('global_taxonomy') );
				else if ( isset( $wp_query->queried_object->slug ) && !empty( $wp_query->queried_object->slug ) )
					$termslug = esc_attr( $wp_query->queried_object->slug );
			}

			$networksettings = ( is_multisite() && $global ? get_site_option( 'mp_network_settings' ) : '' );

			if ( $global ) {

				$taxonomies = $this->load_global_taxonomies_list( array( 'include' => $taxonomy ) );

			} else {
				$taxonomylist = get_categories( array(
					'taxonomy' => $taxonomy,
					'orderby' => 'name',
					'order' => 'ASC',
					'parent' => 0
				  ) );
				$taxonomies = array();
				if ( $taxonomylist ) {
					foreach ($taxonomylist as $term ) {
						$taxonomies[] = array( 'slug' => $term->slug , 'name' => $term->name , 'count' => $term->count , 'type' => $taxonomy , 'term_id' => $term->term_id );
					}
				}
			}

			$output = '<div id="mp-premium-features">';

				$output .= '<ul class="mppf-taxonomy-menu">';

					if  ( $taxonomies ) {

					  	foreach($taxonomies as $cat) {

					  		if ( is_multisite() && $global ) {

							    if ($cat['type'] == 'product_category')
							      	$link = get_home_url( ( function_exists('mp_main_site_id') ? mp_main_site_id() : 1 ) , $settings['slugs']['marketplace'] . '/' . $settings['slugs']['categories'] . '/' . $cat['slug'] . '/' );
							    else if ($cat['type'] == 'product_tag')
							      	$link = get_home_url( ( function_exists('mp_main_site_id') ? mp_main_site_id() : 1 ) , $settings['slugs']['marketplace'] . '/' . $settings['slugs']['tags'] . '/' . $cat['slug'] . '/' );
							    else
							    	$link = '';

					  		} else {
					  			$link = get_term_link( $cat['slug'] , $taxonomy );
					  		}

					  		$item_count = apply_filters( $this->hook_prefix . '_image_taxonomy_item_count' , $cat['count'] > 1 ? $cat['count'] . __( ' items' , $this->plugin_slug ) : $cat['count'] . __( ' item' , $this->plugin_slug ) , $cat['count'] );

						  	$output .= '<li class="mppf-taxonomy-menu-item add-fadeindown-effects-parent'.( $termslug == $cat['slug'] ? ' selected"' : '' ).'">';
						  		$output .= '<a href="'.$link.'" class="mppf-taxonomy-menu-item-link">';
						  			 $output .= ( is_multisite() && $global ? $this->load_global_product_image( array( 'slug' => $cat['slug'] , 'taxonomy' => $taxonomy , 'size' => $imagesize ) ) : $this->load_product_image( array( 'slug' => $cat['slug'] , 'taxonomy' => $taxonomy , 'size' => $imagesize ) ) );
						   			 $output .= '<span class="mppf-taxonomy-menu-item-name">' . $cat['name'] . '</span>' . ( $count == 'true' ? '<br /><span class="mppf-taxonomy-menu-item-count">'.$item_count.'</span>' : '' ); 
						   			  $output .= '<div class="clear"></div>';
						   		$output .= '</a>'; // end - mppf-taxonomy-menu-item-link

						     if ( $taxonomy == 'product_category' && !$global ) {
							    // load sub categories (if available)
								$subcategories = get_categories( array(
									'taxonomy' => $taxonomy,
									'orderby' => 'name',
									'order' => 'ASC',
									'parent' => $cat['term_id']
								  ) );

								if ($subcategories) {

									$output .= '<ul class="mppf-sub-taxonomy-menu add-fadeindown-effects-child">';
 
									foreach ($subcategories as $subcat) {

										$item_count = apply_filters( $this->hook_prefix . '_image_taxonomy_item_count' , $subcat->count > 1 ? $subcat->count . __( ' items' , $this->plugin_slug ) : $subcat->count . __( ' item' , $this->plugin_slug ) , $subcat->count );

									  	$output .= '<li class="mppf-taxonomy-menu-item'.( $termslug == $subcat->slug ? ' selected"' : '' ).'">';
									  		$output .= '<a href="'. get_term_link( $subcat->slug , $taxonomy ) .'" class="mppf-taxonomy-menu-item-link">';
									  			 $output .= $this->load_product_image( array( 'slug' => $subcat->slug , 'taxonomy' => $taxonomy ) );
									   			 $output .= '<span class="mppf-taxonomy-menu-item-name">' . $subcat->name . '</span>' . ( $count == 'true' ? '<br /><span class="mppf-taxonomy-menu-item-count">'.$item_count.'</span>' : '' ); 
									   			  $output .= '<div class="clear"></div>';
									   		$output .= '</a>'; // end - mppf-taxonomy-menu-item-link
									   	$output .= '</li>'; // end - mppf-taxonomy-menu-item
									}

									$output .= '</ul>'; // end - mppf-sub-menu
								}
						    }

							$output .= '</li>'; // end - mppf-taxonomy-menu-item
					  	}
					}

				$output .= '</ul>'; // end - mppf-taxonomy-menu

			$output .= '</div>';

	    	return apply_filters( $this->func_hook_prefix . 'load_taxonomy_menu' , $output , $instance );
	    }

		/* Randomly select product image
		------------------------------------------------------------------------------------------------------------------- */

	    function load_product_image( $args = array() ) {

			$defaults = array(
				'slug' => '',
				'taxonomy' => 'product_category',
				'size' => array(50, 50)
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $mp;

		  	$posts_array = get_posts( array( 'numberposts' => 1, 'post_type' => 'product' , $taxonomy => $slug, 'orderby' => 'rand', 'order' => 'DESC') );

		  	if ( !empty($posts_array) ) {
			  	foreach( $posts_array as $post ) {
			  		$output = '<div class="mppf-taxonomy-menu-item-image">' . ( has_post_thumbnail( $post->ID ) ? get_the_post_thumbnail( $post->ID , $size ) : '<img width="'.$size[0].'" height="'.$size[1].'" class="mppf-taxonomy-menu-default-image" src="' . apply_filters('mp_default_product_img', $mp->plugin_url . 'images/default-product.png') . '" />' ) . '</div>';
			  	}
		  	}

		  	return apply_filters( $this->func_hook_prefix . 'load_product_image' , $output , $instance );
	    }

	    function load_global_product_image( $args = array() ) {

			$defaults = array(
				'slug' => '',
				'taxonomy' => 'product_category',
				'size' => array(50, 50)
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );		

			global $wpdb, $mp;

			$slug = esc_sql( $slug );
			$taxonomy = esc_sql( $taxonomy );

			$query = "SELECT blog_id, p.post_id, post_permalink, post_title, post_content FROM {$wpdb->base_prefix}mp_products p INNER JOIN {$wpdb->base_prefix}mp_term_relationships r ON p.id = r.post_id INNER JOIN {$wpdb->base_prefix}mp_terms t ON r.term_id = t.term_id WHERE p.blog_public = 1 AND t.type = '$taxonomy' AND t.slug = '$slug' ORDER BY RAND() LIMIT 1";

		  	$results = $wpdb->get_results( $query );

		  	$current_blog_id = get_current_blog_id();

		  	if ( !empty($results) ) {
			  	foreach($results as $product) {
			  		global $current_blog;
					switch_to_blog($product->blog_id);

			  		$output = '<div class="mppf-taxonomy-menu-item-image">' . ( has_post_thumbnail( $product->post_id ) ? get_the_post_thumbnail( $product->post_id , $size ) : '<img width="'.$size[0].'" height="'.$size[1].'" class="mppf-taxonomy-menu-default-image" src="' . apply_filters('mp_default_product_img', $mp->plugin_url . 'images/default-product.png') . '" />' ) . '</div>';

			  		restore_current_blog();
			  	}
		  	}

		  	restore_current_blog();

			if ( is_multisite() ) {
				$after_restore_blog_id = get_current_blog_id();
				if ( $current_blog_id != $after_restore_blog_id )
					switch_to_blog( $current_blog_id );
			}

	    	return apply_filters( $this->func_hook_prefix . 'load_global_product_image' , $output , $instance );
	    }

		/* Load global taxomonies list
		------------------------------------------------------------------------------------------------------------------- */

	    function load_global_taxonomies_list( $args = array() ) {

			$defaults = array(
				'include' => 'product_category',
				'limit' => 50,
				'order_by' => 'name',
				'order' => 'ASC'
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

		  	global $wpdb;

		  	$order_by = ($order_by == 'count') ? $order_by : 'name';
		  	$order = ($order == 'DESC') ? $order : 'ASC';
		  	$limit = intval($limit);

		  	if ($include == 'product_tag')
		    	$where = " WHERE t.type = 'product_tag'";
		  	else if ($include == 'product_category')
		    	$where = " WHERE t.type = 'product_category'";

		  	$tags = $wpdb->get_results( "SELECT name, slug, type, count(post_id) as count FROM {$wpdb->base_prefix}mp_terms t LEFT JOIN {$wpdb->base_prefix}mp_term_relationships r ON t.term_id = r.term_id$where GROUP BY t.term_id ORDER BY $order_by $order LIMIT $limit", ARRAY_A );

	    	return apply_filters( $this->func_hook_prefix . 'load_global_taxonomies_list' , ( !empty($tags) ? $tags : NULL ) , $instance );
	    }

	} // end - class MPPremiumFeatures_ImageTaxomony

	class MPPF_Image_Taxonomy_Widget extends WP_Widget {

		function MPPF_Image_Taxonomy_Widget() {
			$widget_ops = array( 'classname' => 'mppf_image_taxonomy_widget', 'description' => __( "A list of product taxonomies (with images) from your MarketPress store.", MPPF_PLUGIN_SLUG ) );
			$this->WP_Widget('mppf_image_taxonomy_widget', __('Image Taxonomies Widget', MPPF_PLUGIN_SLUG), $widget_ops );
		}

		function widget( $args, $instance ) {

			if ($instance['only_store_pages'] && !mp_is_shop_page())
				return;

			extract( $args );

			global $mppf_imagetaxonomy;

			$title = apply_filters('widget_title', empty( $instance['title'] ) ? __('Product Categories', MPPF_PLUGIN_SLUG) : $instance['title'], $instance, $this->id_base);
			$c = $instance['count'] ? 'true' : 'false';
			$taxonomy_type = !empty($instance['taxonomy_type']) ? $instance['taxonomy_type'] : 'product_category';

			echo $before_widget;

			if ( $title )
				echo $before_title . $title . $after_title;

				echo $mppf_imagetaxonomy->load_taxonomy_menu( array( 'termslug' => '' , 'taxonomy' => $taxonomy_type , 'count' => $c ) );

			echo $after_widget;
		}

		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['taxonomy_type'] = $new_instance['taxonomy_type'];
			$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
			$instance['only_store_pages'] = !empty($new_instance['only_store_pages']) ? 1 : 0;

			return $instance;
		}

		function form( $instance ) {
			//Defaults
			$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'only_store_pages' => 0 ) );
			$title = esc_attr( $instance['title'] );
			$taxonomy_type = !empty($instance['taxonomy_type']) ? $instance['taxonomy_type'] : 'product_category';
			$count = isset($instance['count']) ? (bool) $instance['count'] : true;
			$only_store_pages = isset( $instance['only_store_pages'] ) ? (bool) $instance['only_store_pages'] : false;
		?>
			<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' , MPPF_PLUGIN_SLUG ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		    <p>
			    <label><?php _e('Taxonomy Type:', MPPF_PLUGIN_SLUG ) ?></label><br />
			    <select id="<?php echo $this->get_field_id('taxonomy_type'); ?>" name="<?php echo $this->get_field_name('taxonomy_type'); ?>">
			      <option value="product_category"<?php selected($taxonomy_type, 'product_category') ?>><?php _e('Product Category', MPPF_PLUGIN_SLUG) ?></option>
			      <option value="product_tag"<?php selected($taxonomy_type, 'product_tag') ?>><?php _e('Product Tag', MPPF_PLUGIN_SLUG) ?></option>
			    </select>
		    </p>

			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
			<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show product counts', MPPF_PLUGIN_SLUG ); ?></label><br />

			<p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('only_store_pages'); ?>" name="<?php echo $this->get_field_name('only_store_pages'); ?>"<?php checked( $only_store_pages ); ?> />
			<label for="<?php echo $this->get_field_id('only_store_pages'); ?>"><?php _e( 'Only show on store pages', MPPF_PLUGIN_SLUG ); ?></label></p>
		<?php
		}

	}

	class MPPF_Global_Image_Taxonomy_Widget extends WP_Widget {

		function MPPF_Global_Image_Taxonomy_Widget() {
			$widget_ops = array( 'classname' => 'mppf_global_image_taxonomy_widget', 'description' => __( "A list of product taxonomies (with images) from the network MarketPress stores.", MPPF_PLUGIN_SLUG ) );
			$this->WP_Widget('mppf_global_image_taxonomy_widget', __('Global Image Taxonomies Widget', MPPF_PLUGIN_SLUG), $widget_ops);
		}

		function widget( $args, $instance ) {

			global $mppf_imagetaxonomy;

			if ($instance['only_store_pages'] && !mp_is_shop_page())
				return;

			extract( $args );

			$title = apply_filters('widget_title', empty( $instance['title'] ) ? __('Global Product Categories', MPPF_PLUGIN_SLUG) : $instance['title'], $instance, $this->id_base);
			$c = $instance['count'] ? 'true' : 'false';
			$taxonomy_type = !empty($instance['taxonomy_type']) ? $instance['taxonomy_type'] : 'product_category';

			echo $before_widget;
			if ( $title )
				echo $before_title . esc_attr($title) . $after_title;

				echo $mppf_imagetaxonomy->load_taxonomy_menu( array( 'global' => true , 'termslug' => '' , 'taxonomy' => $taxonomy_type , 'count' => $c ) );

			echo $after_widget;
		}

		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['taxonomy_type'] = $new_instance['taxonomy_type'];
			$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
			$instance['only_store_pages'] = !empty($new_instance['only_store_pages']) ? 1 : 0;

			return $instance;
		}

		function form( $instance ) {
			//Defaults
			$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'only_store_pages' => 0 ) );
			$title = esc_attr( $instance['title'] );
			$taxonomy_type = !empty($instance['taxonomy_type']) ? $instance['taxonomy_type'] : 'product_category';
			$count = isset($instance['count']) ? (bool) $instance['count'] : true;
			$only_store_pages = isset( $instance['only_store_pages'] ) ? (bool) $instance['only_store_pages'] : false;
		?>
			<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' , MPPF_PLUGIN_SLUG ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		    <p>
			    <label><?php _e('Taxonomy Type:', MPPF_PLUGIN_SLUG) ?></label><br />
			    <select id="<?php echo $this->get_field_id('taxonomy_type'); ?>" name="<?php echo $this->get_field_name('taxonomy_type'); ?>">
			      <option value="product_category"<?php selected($taxonomy_type, 'product_category') ?>><?php _e('Category', MPPF_PLUGIN_SLUG ) ?></option>
			      <option value="product_tag"<?php selected($taxonomy_type, 'product_tag') ?>><?php _e('Tag', MPPF_PLUGIN_SLUG) ?></option>
			    </select>
		    </p>

			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
			<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show product counts', MPPF_PLUGIN_SLUG ); ?></label><br />

			<p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('only_store_pages'); ?>" name="<?php echo $this->get_field_name('only_store_pages'); ?>"<?php checked( $only_store_pages ); ?> />
			<label for="<?php echo $this->get_field_id('only_store_pages'); ?>"><?php _e( 'Only show on store pages', MPPF_PLUGIN_SLUG ); ?></label></p>
		<?php
		}

	}