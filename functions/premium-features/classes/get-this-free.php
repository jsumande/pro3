<?php

	class MPPremiumFeatures_GetThisFree {

	    private $plugin_path;
	    private $plugin_url;
	    private $plugin_slug;

	    function __construct( $args = array() ) {

			$defaults = array(
				'file' => '',
				'plugin_path' => MPPF_PATH,
				'plugin_url' => MPPF_URL,
				'plugin_slug' => 'mppremiumfeatures',
				'hook_prefix' => 'mppremiumfeatures',		
			);

			$instance = wp_parse_args( $args, $defaults );

	    	$this->plugin_unique_id = $instance['file'];
	        $this->plugin_path = $instance['plugin_path'];
	        $this->plugin_url = $instance['plugin_url'];
	        $this->plugin_slug = $instance['plugin_slug'];
	        $this->hook_prefix = $instance['hook_prefix'];
	        $this->func_hook_prefix = 'mppremiumfeatures_getthisfree_func_';

	        global $mppf;

	        $getthisfree = $mppf->load_option( $mppf->option_group . '_getthisfree_enable_feature' , 'no' );

	        if ( $getthisfree == 'yes' ) {
	        	add_filter( 'mp_product_price_html' , array(&$this, 'price_label_hook_generic') , 15 , 3 );
	        	add_filter( 'pro_product_price_tag' , array(&$this, 'price_label_hook_pro') , 15 , 2 );
	        	add_filter( 'flexmarket_product_price_tag' , array(&$this, 'price_label_hook_flexmarket') , 15 , 2 );
	        	add_filter( 'mpdg_product_price_tag' , array(&$this, 'price_label_hook_mpdg') , 15 , 3 );
	        	add_filter( 'mppremiumfeatures_wishlist_func_load_wishlist_price' , array(&$this, 'price_label_hook_wishlist') , 15 , 2 );
	        	add_filter( $this->hook_prefix . '_add_cart_btn_text' , array(&$this, 'load_btn_text') , 15 , 2 );
	        	add_filter( $this->hook_prefix . '_buy_now_btn_text' , array(&$this, 'load_btn_text') , 15 , 2 );
	        }
	        
	    }

		/* Product Price filter
		------------------------------------------------------------------------------------------------------------------- */

		function price_label_hook_generic( $output = '' , $post_id = NULL , $label = true ) {
			return $this->load_price_label( array( 'output' => $output , 'post_id' => $post_id , 'label' => $label ) );
		}

		function price_label_hook_wishlist( $output = '' , $instance = array() ) {

			$args = array( 
				'output' => $output, 
				'post_id' => $instance['post_id'],
				'context' => $instance['context'],
				'label' => false,
				'defaultclass' => false
			);

			return $this->load_price_label( $args );
		}

		function price_label_hook_flexmarket( $output = '' , $instance = array() ) {

			$args = array( 
				'output' => $output, 
				'post_id' => $instance['post_id'], 
				'label' => $instance['label'],
				'icon' => '<i class="icon-tags'.$instance['iconclass'].'"></i> ',
				'defaultclass' => false
			);

			return $this->load_price_label( $args );
		}

		function price_label_hook_pro( $output = '' , $instance = array() ) {

			$args = array( 
				'output' => $output, 
				'post_id' => $instance['post_id'], 
				'label' => $instance['label'],
				'icon' => '',
				'defaultclass' => false
			);

			return $this->load_price_label( $args );
		}

		function price_label_hook_mpdg( $output = '' , $post_id = NULL , $label = true ) {

	  		global $id;
	  		$post_id = ( NULL === $post_id ) ? $id : $post_id;

			$args = array( 
				'output' => $output, 
				'post_id' => $post_id, 
				'label' => $label,
				'icon' => '',
				'defaultclass' => false
			);

			return $this->load_price_label( $args );
		}		

	    function load_price_label( $args = array() ) {

			$defaults = array(
				'output' => '',
				'post_id' => NULL,
				'label' => true,
				'icon' => '',
				'defaultclass' => true,
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

    		global $id, $mp, $mppf;
    		$post_id = ( NULL === $post_id ) ? $id : $post_id;

    		$label = ( $label === true ) ? __('Price: ', $this->plugin_slug ) : $label;

    		$new_price_label = $mppf->load_option( $mppf->option_group . '_getthisfree_price_label' , __( 'FREE' , $this->plugin_slug ) );

		    $meta = get_post_custom($post_id);
		    if ( !empty($meta) ) {
			    foreach ($meta as $key => $val) {
			        $meta[$key] = maybe_unserialize($val[0]);
			        if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file" && $key != "mp_price_sort")
			            $meta[$key] = array($meta[$key]);
			    }	
		    }

		    if ( ( !empty($meta["mp_price"]) && is_array($meta["mp_price"]) && count($meta["mp_price"]) == 1 ) || !empty($meta["mp_file"]) ) {
		        if ( $meta["mp_is_sale"] ) {
		        	if ( $meta["mp_sale_price"][0] <= 0 ) {
		        		$output = $icon . ( $defaultclass ? '<span class="mp_product_price">' : '' ) . $label;
		        			$output .= '<span class="mp_special_price"><del class="mp_old_price">' . $mp->format_currency('', $meta["mp_price"][0]) . '</del>';
		        			$output .= '<span itemprop="price" class="mp_current_price">' . apply_filters( $this->hook_prefix . '_getthisfree_price_label' , $new_price_label , $post_id ) . '</span></span>';
		        		$output .= ( $defaultclass ? '</span>' : '' ); // end - mp_product_price
		        	}
		        } else {
		            if ( $meta["mp_price"][0] <= 0 )
		            	$output = $icon . ( $defaultclass ? '<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="mp_product_price">' : '' ) . $label . apply_filters( $this->hook_prefix . '_getthisfree_price_label' , $new_price_label , $post_id ) . ( $defaultclass ? '</span>' : '' ) ;
		        }
		    }

	    	return apply_filters( $this->func_hook_prefix . 'load_price_label' , $output , $instance );
	    }

		/* Button text filter
		------------------------------------------------------------------------------------------------------------------- */

	    function load_btn_text( $text = '' , $args = array() ) {

			$defaults = array(
				'post_id' => NULL,
				'global' => false,
				'context' => 'single',
				'integration' => 'none',
				'showbtntext' => false,
				'showstock' => true,
				'btnclass' => '',
				'chooseoptionbtntext' => __('Choose Option', $this->plugin_slug ),
				'addcartbtntext' => __('Add To Cart', $this->plugin_slug ),
				'buynowbtntext' => __('Buy Now', $this->plugin_slug ),
				'addcartbtn' => 'mppf-btn-addcart',
				'buynowbtn' => 'mppf-btn-buynow',
				'cartaction' => 'mppf-update-cart',
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

		  	global $id, $mp, $mppf;
		  	$post_id = ( NULL === $post_id ) ? $id : $post_id;

		  	$new_text = $mppf->load_option( $mppf->option_group . '_getthisfree_btn_text' , __( 'Get This FREE' , $this->plugin_slug ) );

		    $meta = get_post_custom($post_id);
		    if ( !empty($meta) ) {
			    foreach ($meta as $key => $val) {
			        $meta[$key] = maybe_unserialize($val[0]);
			        if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file" && $key != "mp_price_sort")
			            $meta[$key] = array($meta[$key]);
			    }	
		    }

		    if ( ( !empty($meta["mp_price"]) && is_array($meta["mp_price"]) && count($meta["mp_price"]) == 1) || !empty($meta["mp_file"]) ) {
		        if ( $meta["mp_is_sale"] ) {
		        	if ( $meta["mp_sale_price"][0] <= 0 )
		        		$text = apply_filters( $this->hook_prefix . '_get_this_free_btn_text' , $new_text );
		        } else {
		            if ( $meta["mp_price"][0] <= 0 )
		            	$text = apply_filters( $this->hook_prefix . '_get_this_free_btn_text' , $new_text );
		        }
		    }

		    return $text;
	    }

	} // end - class MPPremiumFeatures_GetThisFree