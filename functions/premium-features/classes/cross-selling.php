<?php

	class MPPremiumFeatures_CrossSelling {

	    private $plugin_path;
	    private $plugin_url;
	    private $plugin_slug;

	    function __construct( $args = array() ) {

			$defaults = array(
				'file' => '',
				'plugin_path' => MPPF_PATH,
				'plugin_url' => MPPF_URL,
				'plugin_slug' => 'mppremiumfeatures',
				'hook_prefix' => 'mppremiumfeatures',		
			);

			$instance = wp_parse_args( $args, $defaults );

	    	$this->plugin_unique_id = $instance['file'];
	        $this->plugin_path = $instance['plugin_path'];
	        $this->plugin_url = $instance['plugin_url'];
	        $this->plugin_slug = $instance['plugin_slug'];
	        $this->hook_prefix = $instance['hook_prefix'];
	        $this->func_hook_prefix = 'mppremiumfeatures_cross_selling_func_';

	        // ajax functions
    		add_action( 'wp_ajax_nopriv_mppf-cs-update-cart', array(&$this, 'cs_update_cart') );
    		add_action( 'wp_ajax_mppf-cs-update-cart', array(&$this, 'cs_update_cart') );

    		// add cross sell metaboxes
			add_filter( 'cmb_meta_boxes', array(&$this, 'load_cs_metaboxes') );
	    }

		/* CS Modal
		------------------------------------------------------------------------------------------------------------------- */

	    function load_cs_modal( $args = array() ) {

			$defaults = array(
				'post_id' => NULL,
				'btnclass' => '',
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $id, $mppf_p;

			$post_id = ( NULL === $post_id ) ? $id : $post_id;

    		$output = '<div id="mp-premium-features">';

	    		$output .= '<h3 class="mppf-cs-modal-title"><i class="fa fa-check-circle"></i>'.__( ' Item(s) Added To Cart' , $this->plugin_slug ).'</h3>';

	    		$output .= apply_filters( $this->hook_prefix . '_cs_modal_after_title' , '' , $instance );

	    		$output .= '<div class="mppf-cs-cart-info-container">';
	    			// cart info
			    	$output .= '<div class="mppf-cs-cart-info">';
			  			$output .= '<div class="mppf-cart-items"><strong>'.( function_exists('mp_items_count_in_cart') ? mp_items_count_in_cart() : '' ).'</strong>'.__( ' item(s) in your cart' , $this->plugin_slug ).'</div>';
			  			$output .= '<div class="mppf-cart-subtotal"><strong>'.__( 'Order subtotal: ' , $this->plugin_slug ).'</strong>'.$this->total_amount_in_cart().'</div>';
			    	$output .= '</div>'; // end - mppf-cs-cart-info
			    	//checkout button
		    		$output .= '<a href="'.( function_exists('mp_cart_link') ? mp_cart_link(false, true) : '' ).'" class="mppf-btn mppf-cs-modal-checkout-btn'.$btnclass.'"><i class="fa fa-shopping-cart"></i><span class="mppf-btn-text mppf-show">'.__( 'Proceed to Checkout &raquo;' , $this->plugin_slug ).'</span></a>';

		    		$output .= '<div class="clear"></div>';

		    		$output .= apply_filters( $this->hook_prefix . '_cs_modal_after_cart_info' , '' , $instance );

		    	$output .= '</div>'; // end - mppf-cs-cart-info-container

		    	$output .= '<div class="mppf-cs-product-listing">' . $this->load_cs_products( array(
			    			'post_id' => $post_id,
			    			'btnclass' => $btnclass,
			    		) ) . '</div>';

		    	$output .= '<div class="clear"></div>';

		    	$output .= '<button class="mppf-btn mppf-cs-modal-continue-btn mppf-main-modal-close'.$btnclass.'"><i class="fa fa-long-arrow-left"></i><span class="mppf-btn-text mppf-show">'.__( 'Close and Continue Shopping' , $this->plugin_slug ).'</span></button>';

		    	$output .= '<div class="clear"></div>';

		    $output .= '</div>'; // end - #mp-premium-features

	    	return apply_filters( $this->func_hook_prefix . 'load_cs_modal' , $output , $instance );
	    }

	    function load_cs_products( $args = array() ) {

			$defaults = array(
				'post_id' => NULL,
				'btnclass' => '',
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			// exit if no product id found
			if ( NULL === $post_id )
				return;

			global $mppf;

			$enablecs =  esc_attr(get_post_meta( $post_id, '_mpt_enable_cross_selling', true ));

			$csproduct = array();
			for ( $i=1; $i < 5 ; $i++) { 
				$csproduct[$i] = esc_attr(get_post_meta( $post_id, '_mpt_cs_product_'.$i, true ));
				$csproduct[$i] = ( $csproduct[$i] == $post_id ? 'empty' : $csproduct[$i] );
			}

			$product_output = '';
			$count = 0;
			if ( $enablecs == 'yes' ) {
				for ($i=1; $i < 5; $i++) { 
					
					if ($csproduct[$i] != 'empty') {

						$product_output .= '<div class="mppf-cs-modal-well">';

							$product_output .= '<div class="mppf-cs-modal-product-details">';
								$product_output .= ( function_exists('mp_product_image') ? '<div class="mppf-cs-modal-product-image">' . mp_product_image( false, 'widget', $csproduct[$i] , 100 ) . '</div>' : '' );
								$product_output .= '<div class="mppf-cs-modal-product-contents">';
									$product_output .= '<h4 class="mppf-cs-modal-product-title"><a href="'.get_permalink($csproduct[$i]).'">'.get_the_title($csproduct[$i]).'</a></h4>';
									$product_output .= '<p class="mppf-cs-modal-product-price">'.( function_exists('mp_product_price') ? mp_product_price( false , $csproduct[$i] , false ) : '' ).'</p>';
									$post = get_post($csproduct[$i] , ARRAY_A);
									$product_output .= '<p class="mppf-cs-modal-product-excerpt">' . (!empty($post['post_excerpt']) ? $post['post_excerpt'] : wp_trim_words($post['post_content'] , 15) ) .'</p>';
								$product_output .= '</div>'; // end - mppf-cs-modal-product-contents
							$product_output .= '</div>'; // end - mppf-cs-modal-product-details

							// buy button
							$product_output .= '<div class="mppf-cs-modal-buy-btn">';
								$product_output .= $mppf->load_buy_button( array(
			    						'post_id' => $csproduct[$i],
			    						'context' => 'list',
			    						'showbtntext' => true,
			    						'btnclass' => $btnclass,
										'addcartbtn' => 'mppf-btn-inside-cs',
										'buynowbtn' => 'mppf-btn-inside-cs',
										'cartaction' => 'mppf-cs-update-cart',
									) );
								$product_output .= '<a href="'.get_permalink($csproduct[$i]).'" class="mppf-cs-modal-more-details mppf-btn'.$btnclass.'"><span class="mppf-btn-text mppf-show mppf-btn-text-right">'.__( 'More Details' , $this->plugin_slug ).'</span><i class="fa fa-angle-right"></i></a>';
							$product_output .= '</div>'; // end - mppf-cs-modal-buy-btn

						$product_output .= '</div>'; // End mppf-cs-modal-well

					} else {
						$count++;
					}
				}
			}

			if ( $count == 4 || $enablecs != 'yes' ) {
				$output = '';
			} else {
		    	$output = apply_filters( $this->hook_prefix . '_cs_modal_listing_title' , '<h4 class="mppf-cs-listing-title">'.__( 'Customers Who Bought' , $this->plugin_slug).' <span class="mppf-cs-product-title">'.get_the_title($post_id).'</span> '.__( 'Also Bought: ' , $this->plugin_slug ).'</h4>' , $instance );
		    	$output .= '<div class="mppf-cs-products">' . $product_output . '</div>';
			}

	    	return apply_filters( $this->func_hook_prefix . 'load_cs_products' , $output , $instance );
	    }

		/* Update cart with CS
		------------------------------------------------------------------------------------------------------------------- */

	    function cs_update_cart( $args = array() ) {

			global $mp, $mppf, $blog_id, $mp_gateway_active_plugins;
			$blog_id = (is_multisite()) ? $blog_id : 1;
			$current_blog_id = $blog_id;

	    	$cart = $mp->get_cart_cookie();

			if ( isset($_POST['product_id']) ) { //add a product to cart

				//if not valid product_id return
		      	$product_id = apply_filters('mp_product_id_add_to_cart', intval($_POST['product_id']));
		      	$product = get_post($product_id);
		      	if (!$product || $product->post_type != 'product' || $product->post_status != 'publish')
		        	return false;

				//get quantity
		      	$quantity = (isset($_POST['quantity'])) ? intval(abs($_POST['quantity'])) : 1;

		      	//get variation
		      	$variation = (isset($_POST['variation'])) ? intval(abs($_POST['variation'])) : 0;

		      	//check max stores
		      	if ($mp->global_cart && count($global_cart = $mp->get_cart_cookie(true)) >= $mp_gateway_active_plugins[0]->max_stores && !isset($global_cart[$blog_id])) {
		        	if (defined('DOING_AJAX') && DOING_AJAX) {
			  			echo 'error|SEP|' . sprintf(__("Sorry, currently it's not possible to checkout with items from more than %s stores.", $this->plugin_slug ), $mp_gateway_active_plugins[0]->max_stores);
		          		exit;
		        	} else {
		          		$mp->cart_checkout_error(sprintf(__("Sorry, currently it's not possible to checkout with items from more than %s stores.", $this->plugin_slug ), $mp_gateway_active_plugins[0]->max_stores));
		          	return false;
		      		}
		    	}

		      	//calculate new quantity
		      	$new_quantity = $cart[$product_id][$variation] + $quantity;

		      	//check stock
		      	if (get_post_meta($product_id, 'mp_track_inventory', true)) {
		        	$stock = maybe_unserialize(get_post_meta($product_id, 'mp_inventory', true));
		        	if (!is_array($stock))
						$stock[0] = $stock;
		        	if ($stock[$variation] < $new_quantity) {
		          	if (defined('DOING_AJAX') && DOING_AJAX) {
		            	echo 'error|SEP|' . sprintf(__("Sorry, we don`t have enough of this item in stock. (%s remaining)", $this->plugin_slug ), number_format_i18n($stock[$variation]-$cart[$product_id][$variation]));
		            	exit;
		          	} else {
		            	$mp->cart_checkout_error( sprintf(__("Sorry, we don`t have enough of this item in stock. (%s remaining)", $this->plugin_slug ), number_format_i18n($stock[$variation]-$cart[$product_id][$variation])) );
		            	return false;
		          	}
		        }
		        	//send ajax leftover stock
		        	if (defined('DOING_AJAX') && DOING_AJAX) {
		          		$return = array_sum($stock)-$new_quantity . '|SEP|';
		        	}
		      	} else {
		        	//send ajax always stock if stock checking turned off
		        	if (defined('DOING_AJAX') && DOING_AJAX) {
		          		$return = 1 . '|SEP|';
		        	}
		      	}

		      	//check limit if tracking on or downloadable
		    	if (get_post_meta($product_id, 'mp_track_limit', true) || $file = get_post_meta($product_id, 'mp_file', true)) {

					$limit = empty($file) ? maybe_unserialize(get_post_meta($product_id, 'mp_limit', true)) : array($variation => 1);

			      	if ($limit[$variation] && $limit[$variation] < $new_quantity) {
			        	if (defined('DOING_AJAX') && DOING_AJAX) {
				  			echo 'error|SEP|' . sprintf(__('Sorry, there is a per order limit of %1$s for "%2$s".', $this->plugin_slug ), number_format_i18n($limit[$variation]), $product->post_title);
			          		exit;
			        	} else {
			          		$mp->cart_checkout_error( sprintf(__('Sorry, there is a per order limit of %1$s for "%2$s".', $this->plugin_slug ), number_format_i18n($limit[$variation]), $product->post_title) );
			          		return false;
			        	}
			      	}
		      	}

		      	$cart[$product_id][$variation] = $new_quantity;

		  		//save items to cookie
		  		$mp->set_cart_cookie($cart);

				$cartinfo = '<div class="mppf-cart-items"><strong>'.( function_exists('mp_items_count_in_cart') ? mp_items_count_in_cart() : '' ).'</strong>'.__( ' item(s) in your cart' , $this->plugin_slug ).'</div>';
			  	$cartinfo .= '<div class="mppf-cart-subtotal"><strong>'.__( 'Order subtotal: ' , $this->plugin_slug ).'</strong>'.$this->total_amount_in_cart().'</div>';

	  			$csmodal = $this->load_cs_modal( array( 
					'post_id' => $product_id,
					'btnclass' => $mppf->get_btn_color(),
	  			) );

		      	//if running via ajax return updated cart and die
		      	if (defined('DOING_AJAX') && DOING_AJAX) {
		        	$return .= mp_show_cart('widget', false, false) . '|SEP|' . $csmodal . '|SEP|' . $cartinfo;
		        	echo apply_filters( $this->func_hook_prefix . 'cs_update_cart' , $return , $product_id , $quantity , $variation );
					exit;
		      	}
		    }
	    }

		/* Cart
		------------------------------------------------------------------------------------------------------------------- */

	    function total_amount_in_cart() {

		  	global $mp, $blog_id;
		  	$blog_id = (is_multisite()) ? $blog_id : 1;
		  	$current_blog_id = $blog_id;

			$global_cart = $mp->get_cart_contents(true);

		  	if (!$mp->global_cart)  //get subset if needed
		  		$selected_cart[$blog_id] = $global_cart[$blog_id];
		  	else
		    	$selected_cart = $global_cart;

		    $totals = array();

		    if (!empty($selected_cart) && is_array($selected_cart)) {
			    foreach ($selected_cart as $bid => $cart) {

				if (is_multisite())
			        switch_to_blog($bid);

			    if (!empty($cart) && is_array($cart)){
				   	foreach ($cart as $product_id => $variations) {
				        foreach ($variations as $variation => $data) {
				          $totals[] = $data['price'] * $data['quantity'];
				        }
				      }
				    }
			    }
		    } 
		    
			if ( is_multisite() )
		      switch_to_blog($current_blog_id);

		    $total = array_sum($totals);

	    	return apply_filters( $this->func_hook_prefix . 'total_amount_in_cart' , $mp->format_currency( '' , $total ) );
	    }

		/* Metaboxes
		------------------------------------------------------------------------------------------------------------------- */

	    function load_cs_metaboxes( array $meta_boxes ) {

			$prefix = '_mpt_';

			$old_meta_boxes = $meta_boxes;

			$productlisting = array( array( 'name' => __( 'Select a Product' , $this->plugin_slug ) , 'value' => 'empty' ) );
			$products = get_posts( array('post_type' => 'product' , 'orderby' => 'title', 'order' => 'ASC' , 'numberposts' => -1) );

			if (!empty($products) && is_array($products)) {
				foreach ($products as $product) {
					$productlisting[] = array( 
						'name' => $product->post_title, 
						'value' => $product->ID
						);
				}
			}

			$meta_boxes[] = array(
				'id'         => 'cross_selling_metabox',
				'title'      => __( 'Cross Selling' , $this->plugin_slug ),
				'pages'      => array('product'), // Post type
				'context'    => 'normal',
				'priority'   => 'core',
				'show_names' => true, // Show field names on the left
				'fields'     => array(
					array(
						'name'    => __( 'Enable Cross Selling' , $this->plugin_slug ),
						'desc'    => '',
						'id'      => $prefix . 'enable_cross_selling',
						'type'    => 'select',
						'options' => array(
							array( 'name' => 'No', 'value' => 'no', ),
							array( 'name' => 'Yes', 'value' => 'yes', )
						),
					),
					array(
						'name'    => __( 'First Cross Selling Product' , $this->plugin_slug ),
						'desc'    => '',
						'id'      => $prefix . 'cs_product_1',
						'type'    => 'select',
						'options' => $productlisting,
					),
					array(
						'name'    => __( 'Second Cross Selling Product' , $this->plugin_slug ),
						'desc'    => '',
						'id'      => $prefix . 'cs_product_2',
						'type'    => 'select',
						'options' => $productlisting,
					),
					array(
						'name'    => __( 'Third Cross Selling Product' , $this->plugin_slug ),
						'desc'    => '',
						'id'      => $prefix . 'cs_product_3',
						'type'    => 'select',
						'options' => $productlisting,
					),
					array(
						'name'    => __( 'Fourth Cross Selling Product' , $this->plugin_slug ),
						'desc'    => '',
						'id'      => $prefix . 'cs_product_4',
						'type'    => 'select',
						'options' => $productlisting,
					),
				),
			);

			return apply_filters( $this->func_hook_prefix . 'load_cs_metaboxes' , $meta_boxes , $old_meta_boxes );	
	    }

	} // end - class MPPremiumFeatures_CrossSelling