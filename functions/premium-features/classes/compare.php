<?php

	class MPPremiumFeatures_CompareProducts {

	    private $plugin_path;
	    private $plugin_url;
	    private $plugin_slug;

	    function __construct( $args = array() ) {

			$defaults = array(
				'file' => '',
				'plugin_path' => MPPF_PATH,
				'plugin_url' => MPPF_URL,
				'plugin_slug' => 'mppremiumfeatures',
				'hook_prefix' => 'mppremiumfeatures',		
			);

			$instance = wp_parse_args( $args, $defaults );

	    	$this->plugin_unique_id = $instance['file'];
	        $this->plugin_path = $instance['plugin_path'];
	        $this->plugin_url = $instance['plugin_url'];
	        $this->plugin_slug = $instance['plugin_slug'];
	        $this->hook_prefix = $instance['hook_prefix'];
	        $this->func_hook_prefix = 'mppremiumfeatures_compareproducts_func_';

	        // compare results shortcode
	        if ( shortcode_exists( 'compare' ) )
	        	add_shortcode( 'mppfcompare', array(&$this, 'compare_results_sc') );
	        else
	        	add_shortcode( 'compare', array(&$this, 'compare_results_sc') );

	        // ajax functions
    		add_action( 'wp_ajax_nopriv_mppf-add-to-compare', array(&$this, 'add_to_compare') );
    		add_action( 'wp_ajax_mppf-add-to-compare', array(&$this, 'add_to_compare') );
    		add_action( 'wp_ajax_nopriv_mppf-update-compare-results', array(&$this, 'update_compare_results') );
    		add_action( 'wp_ajax_mppf-update-compare-results', array(&$this, 'update_compare_results') );
    		add_action( 'wp_ajax_nopriv_mppf-cp-update-product-list', array(&$this, 'load_products_option_ajax') );
    		add_action( 'wp_ajax_mppf-cp-update-product-list', array(&$this, 'load_products_option_ajax') );
	    }

		/* Compare button
		------------------------------------------------------------------------------------------------------------------- */

	    function load_product_compare_btn( $args = array() ) {

			$defaults = array(
				'post_id' => NULL,
				'context' => 'single',
				'integration' => 'none',
				'btnclass' => '',
				'btntext' => __( 'Add to Compare' ,  'pro' ),
				'resultsbtntext' => __( 'View Compare Chart' , 'pro' ),
				'hiddenphone' => true,
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

    		global $id;
    		$post_id = ( NULL === $post_id ) ? $id : $post_id;

    		$btntext = apply_filters( $this->hook_prefix . '_compare_btn_text' , $btntext , $instance );
    		$resultsbtntext = apply_filters( $this->hook_prefix . '_compare_results_btn_text' , $resultsbtntext , $instance );

	    	$output = '<form style="display:none" class="mppf-product-compare-form mppf-options-form'.( $hiddenphone ? ' mppf-hidden-phone' : '' ).'" method="post" action="'.get_permalink( $post_id ).'">';
		    	$output .= '<button id="single_product_page_comparebutton_live" class="mppf-btn'.$btnclass.' mppf-addtocompare-btn'.( $context == 'single' ? '' : ' add-fadein-effects-parent' ).'" type="submit">';
		    		//$output .= '<i class="fa fa-retweet'.( $context == 'single' ? '' : ' add-fadein-effects-child' ).'"></i>';
		    		$output .= '<span class="mppf-btn-text'.( $context == 'single' ? ' mppf-show' : ' add-fadein-effects-child' ).'">'. $btntext .'</span>';
		    	$output .= '</button>';
				
		    	$output_b = '<div class="col-lg-4 col-md-12 col-sm-4 col-xm-12 padding-sm"><button id="single_product_page_comparebutton" class="mppf-btn'.$btnclass.' mppf-addtocompare-btn'.( $context == 'single' ? '' : ' add-fadein-effects-parent' ).'" type="submit">';
		    		//$output_b .= '<i class="fa fa-retweet'.( $context == 'single' ? '' : ' add-fadein-effects-child' ).'"></i>';
		    		$output_b .= '<span class="mppf-btn-text'.( $context == 'single' ? ' mppf-show' : ' add-fadein-effects-child' ).'">'. $btntext .'</span>';
		    	$output_b .= '</button></div>';				
				
				$output .= '<input type="hidden" name="mppf_compare_item" id="mppf_compare_item" value="'.$post_id.'" />';
				$output .= is_multisite() ? '<input type="hidden" name="currentblogid" id="currentblogid" value="'.get_current_blog_id().'" />' : '';
				$output .= '<input type="hidden" name="action" value="mppf-add-to-compare" />';
	    	$output .= '</form>'; // end - mppf-product-compare-form

			// load compare results modal Trigger & container
			$output .= $output_b.'</div><a href="#mpt-main-modal" class="mppf-modal-trigger compare-results-modal-trigger mppf-btn'.$btnclass.' mppf-btn-hide"><span class="mppf-btn-text mppf-show">'. $resultsbtntext .'</span></a>';
	
	    	return apply_filters( $this->func_hook_prefix . 'load_product_compare_btn' , $output , $instance );
	    }

		/* Compare Results
		------------------------------------------------------------------------------------------------------------------- */

		function load_compare_results( $args = array() ) {

			$defaults = array(
				'showtitle' => 'no',
				'title' => __( 'Compare Chart: ' , 'pro' ),
				'hideprice' => false,
				'btnclass' => '',
				'class' => '',
				'style' => ''
			);		

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

	    	global $mp, $mppf;

			$btnclass = ( empty($btnclass) ? $mppf->get_btn_color() : $btnclass );
			$style = ( !empty($style) ? ' style="'.esc_attr($style).'"' : '' );
			$class = ( !empty($class) ? ' '.esc_attr($class) : '' );

			$session = !empty($_SESSION[ $this->hook_prefix . '_compare_items']) ? $_SESSION[ $this->hook_prefix . '_compare_items'] : '';

			$_SESSION[ $this->hook_prefix . '_compare_results_settings'] = array( 'showtitle' => $showtitle , 'title' => $title , 'hideprice' => $hideprice , 'btnclass' => $btnclass , 'class' => $class , 'style' => $style ); 	

				$output = ( $showtitle == 'yes' ? '<h3>'.__($title, 'pro').'</h3>' : '' );

			    $output .= '<table class="mppf-table mppf-compare-table'.$class.'"'.$style.'>';
					
			    	$output .= '<thead class="mppf-table-head"><tr class="mppf-table-head-row">';

			    		$output .= '<th></th>';
			    		$output .= '<th>'.__('Product 1', 'pro').'</th>';
			    		$output .= '<th>'.__('Product 2', 'pro').'</th>';
			    		$output .= '<th>'.__('Product 3', 'pro').'</th>';

			    	$output .= '</tr></thead>';

			    	$output .= '<tbody>';

			    	if (is_array($session) && !empty($session)) {

			    		$output .= $this->load_table_row( $session , 'basic' , __('Image','pro') , $btnclass );
			    		$output .= $this->load_table_row( $session , 'productname' , ''.__('Product Name' , 'pro').'');
			    		$output .= $this->load_table_row( $session , 'rating' , ''.__('Rating' , 'pro').'');
						$output .= $this->load_table_row( $session , 'category' , ''.__('Categories' , 'pro').'');
			    		$output .= $this->load_table_row( $session , 'excerpt' , ''.__('Description' , 'pro').'');
			    		$output .= ( $mp->get_setting('disable_cart') && $hideprice ? '' : $this->load_table_row( $session , 'price' , ''.__('Price: ' , 'pro').'') );
			    		//$output .= $this->load_table_row( $session , 'variation' , '<strong>'.__('Variation: ' , 'pro').'</strong>');			    		
			    		//$output .= $this->load_table_row( $session , 'tag' , '<strong>'.__('Product Tags: ' , 'pro').'</strong>');
			    		$output .= $this->load_table_row( $session , 'buynow' , __('Add to cart','pro'), $btnclass );
			    		if(is_user_logged_in()) {
			    			$output .= $this->load_table_row( $session , 'sendmessage' , __('Send a Message','pro') , $btnclass );
			    		}
			    		$output .= $this->load_table_row( $session , 'remove', __('Remove','pro') );

			    	} else {
			    		$output .= '<tr><td></td><td colspan="3"><span class="wishlist-empty-list">'.__( 'The chart is currently empty' , 'pro').'</span></td></tr>';
			    		$output .= '<tr><td></td>' . $this->load_add_new_item_option( 1 ) . $this->load_add_new_item_option( 2 ) . $this->load_add_new_item_option( 3 ) . '</tr>';
			    	}

			    	$output .= '<tr class="mppf-table-item-row"><td colspan="4" class="mppf-compare-table-item-update-btn">';
			    		$output .= '<span class="mppf-compare-table-update-msg">' . __( 'Click here to update chart' , 'pro' ) . '<i class="fa fa-long-arrow-right"></i></span>';
			    		//$output .= '<button type="submit" class="mppf-btn'.$btnclass.' mppf-update-compare-results"><i class="fa fa-save"></i><span class="mppf-btn-text mppf-show">'.__( 'Update &raquo;' , 'pro' ).'</span></button>';
			    		$output .= '<button type="submit" class="mppf-btn'.$btnclass.' mppf-update-compare-results"><span class="mppf-btn-text mppf-show" style="margin-left: 0px;">'.__( 'Update' , 'pro' ).'</span></button>';
			    		$output .= '<input type="hidden" name="action" value="mppf-update-compare-results" />';
			    	$output .= '</td></tr>';

			        $output .= '</tbody>';

			    $output .= '</table>';

		    return apply_filters( $this->func_hook_prefix . 'load_compare_results' , $output , $instance );
		}

		function load_table_row( $session = array() , $context = '' , $frontrow = '' , $btnclass = '' ){

			global $mppf;
			global $current_user;

			$count = 0;
			
			$output = '<tr class="mppf-table-item-row">';
			$output .= '<td class="mppf-compare-table-item-frontrow">'.$frontrow.'</td>';

			$current_blog_id = get_current_blog_id();

			if ( !empty($session) && is_array($session) ) {

				foreach ($session as $key => $value) {
		            if (!empty($value)) {

		            	$blog_id = intval(esc_attr(trim($value['blog'])));

		            	if (is_multisite() && !empty($blog_id)) {
		            		switch_to_blog($blog_id);
		            	}

		            	$item_id = intval(esc_attr(trim($value['item'])));

		            	if ($context == 'basic') {
			            	$output .= '<td class="mppf-compare-table-item-basic">';
			            		$output .= '<div class="mppf-compare-product-image">'.mp_product_image( false, 'list', $item_id , '100' ).'</div>';
			            		/**$output .= '<h4 class="mppf-compare-product-title"><a href="'.get_permalink($item_id).'">'.get_the_title($item_id).'</a></h4>';
			            		$output .= $mppf->load_buy_button( array( 
													'post_id' => $item_id,
													'product_page_link' => true,
													'context' => 'list',
													'showbtntext' => true,
													'btnclass' => $btnclass,
							               		 ) );**/
			            	$output .= '</td>';
		            	}  elseif ( $context == 'sendmessage' ) {
		            			if(is_user_logged_in()) { 
									global $blog_id;
									$store_owner = get_storeowner_data($blog_id);
		            				$main_site_url = get_site_url(1); 									
									$user_info = get_userdata($store_owner['store_owner']);								
		            				$output .= '<td class="mppf-compare-table-item-product-sendmessage"><a href="'.localize_url($main_site_url.'/members/'.$current_user->user_login.'/messages/compose/?r='.$user_info->user_login).'" class="sendamessage">'. __( 'Send  a Message' ).'</a></td>';
		            			}
		            	} elseif ( $context == 'productname' ) {
		            			$output .= '<td class="mppf-compare-table-item-product-name"><p class="mppf-compare-product-title"><a href="'.get_permalink($item_id).'">'.get_the_title($item_id).'</a></p></td>';
		            	} elseif ( $context == 'rating' ) {
		            			$output .= '<td class="mppf-compare-table-item-product-rating">'.show_store_ratings("", "", get_permalink($item_id)).'</td>';
		            	}
						elseif ( $context == 'price' ) {
		            			$output .= '<td class="mppf-compare-table-item-price">								
								'.( function_exists('mp_product_price') ? mp_product_price( false , $item_id , false ) : ' ' ).'
								</td>';
		            	}	elseif ($context == 'remove') {							
								$output .= '<td class="mppf-compare-table-item-remove">';
				            	$output .= '<input type="hidden" name="removefromcompare'.$key.'" value="0" /><p>
									<div class="input-control-checkbox" style="margin-left: 8px;">
											<input type="checkbox" name="removefromcompare'.$key.'" id="removefromcompare'.$key.'" value="1" />
											<label>&nbsp;</label>
									</div>'. __( 'Remove' , 'pro' ).'</p>';
				            	$output .= '<input type="hidden" name="compareitem'.$key.'" id="compareitem'.$key.'" value="'.$item_id.'" />';
				            	$output .= '<input type="hidden" name="compareitem'.$key.'blog" id="compareitem'.$key.'blog" value="'.$blog_id.'" />';
				            	$output .= '</td>';
		            	}	elseif ( $context == 'buynow' ) {
			            		$output .= '<td class="mppf-compare-table-item-buynow">';
			            			$output .= $mppf->load_buy_button( array( 
													'post_id' => $item_id,
													'product_page_link' => true,
													'context' => 'list',
													'showbtntext' => true,
													'btnclass' => $btnclass,
							               		 ) );
			            		$output .= '</td>';
		            	}	elseif ( $context == 'excerpt' ) {
		            			$post = get_post($item_id , ARRAY_A);
			            		$output .= '<td class="mppf-compare-table-item-excerpt">';
			            			$output .= ( !empty($post['post_excerpt']) ? $post['post_excerpt'] : wp_trim_words( $post['post_content'] , 15 ) );
			            		$output .= '</td>';
		            	} 	elseif ($context == 'variation') {
		            			global $mp;
							  	$meta = get_post_custom($item_id);
							  	if (!empty($meta)) {
								  	//unserialize
								  	foreach ($meta as $key => $val) {
									  	$meta[$key] = maybe_unserialize($val[0]);
									  	if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file")
									    	$meta[$key] = array($meta[$key]);
									}

									if (is_array($meta["mp_price"]) && count($meta["mp_price"]) > 1 && empty($meta["mp_file"])) {
					            		$output .= '<td class="mppf-compare-table-item-variation">';
					            			$output .= '<table class="mppf-product-variation-table mppf-table">';
												foreach ($meta["mp_price"] as $key => $value) {
													if ($meta["mp_is_sale"] && $meta["mp_sale_price"][$key]) {
										        		$output .= '<tr class="mppf-product-variation-table-row"><td class="mppf-product-variation-table-item">'.esc_html($meta["mp_var_name"][$key]) . '</td><td>' .$mp->format_currency('', $meta["mp_sale_price"][$key]).'</td></tr>';

										      		} else {
										        		$output .= '<tr class="mppf-product-variation-table-row"><td class="mppf-product-variation-table-item">'.esc_html($meta["mp_var_name"][$key]) . '</td><td>' .$mp->format_currency('', $value).'</td></tr>';
										      		}
										   		}
									   		$output .= '</table>'; // end - mppf-product-variation-table
					            		$output .= '</td>'; // end - mppf-compare-table-item-variation
									} else {
										$output .= '<td class="mppf-compare-table-item-variation"></td>';
									}
							  	}
		            	} elseif ($context == 'category') {
		            		$categories = get_the_terms( $item_id, 'product_category' );
		            		$output .= '<td class="mppf-compare-table-item-category">';
			            		if ( $categories && !is_wp_error( $categories ) ) {
				            		foreach ($categories as $category) {
				            			$output .= '<a href="'.get_term_link($category->slug, 'product_category').'" target="_blank" class="product-label-tag">'.$category->name.'</a>';
				            		}
			            		}
			            	$output .= '</td>'; // end - mppf-compare-table-item-category
		            	} elseif ($context == 'tag') {
		            		$tags = get_the_terms( $item_id, 'product_tag' );
		            		$output .= '<td class="mppf-compare-table-item-tag">';
			            		if ( $tags && !is_wp_error( $tags ) ) {
				            		foreach ($tags as $tag) {
				            			$output .= '<a href="'.get_term_link($tag->slug, 'product_tag').'" target="_blank" class="product-label-tag">'.$tag->name.'</a> ';
				            		}
			            		}
		            		$output .= '</td>'; // end - mppf-compare-table-item-tag
		            	}

		            	if (is_multisite())
		            		restore_current_blog();

		            }
		            $count++;
				}

			}

			if ( is_multisite() ) {
				$after_restore_blog_id = get_current_blog_id();
				if ( $current_blog_id != $after_restore_blog_id )
					switch_to_blog( $current_blog_id );
			}

			if ($count == 1) {
				if ( $context == 'basic' )
					$output .= $this->load_add_new_item_option( 2 ) . $this->load_add_new_item_option( 3 );
				else
					$output .= '<td class="mppf-compare-table-item-empty"></td><td class="mppf-compare-table-item-empty"></td>';
			} elseif ($count == 2) {
				if ( $context == 'basic' )
					$output .= $this->load_add_new_item_option( 3 );
				else
					$output .= '<td class="mppf-compare-table-item-empty"></td>';
			}
				
			$output .= '</tr>'; // mppf-compare-table-row

			return apply_filters( $this->func_hook_prefix . 'load_table_row' , $output , $session , $context , $frontrow , $btnclass );
		}

		function load_add_new_item_option( $key = 2 ) {

			global $mppf;

			$current_blog = ( is_multisite() ? get_current_blog_id() : 1 );
			$global_compare = $mppf->load_option( $mppf->option_group . '_compareproducts_enable_global_compare' , 'no' );

			$output = '<td class="mppf-compare-table-item-add-new">';

				$output .= __( 'Add Item' , 'pro' );

				if ( is_multisite() && $global_compare == 'yes' ) {

					$output .= '<div class="mppf-compare-table-select-blog">';
						$output .= '<select name="addnewcompare'.$key.'blog" id="addnewcompare'.$key.'blog">';
							$output .= $this->load_sites_option( $current_blog );
						$output .= '</select>';
					$output .= '</div>';

				}

				$output .= '<div class="mppf-compare-table-select-item">';
					$output .= '<select name="addnewcompare'.$key.'" id="addnewcompare'.$key.'">';
						$output .= $this->load_products_option( $current_blog );
					$output .= '</select>';
					$output .= '<input type="hidden" name="addnewcompare'.$key.'blog" id="addnewcompare'.$key.'blog" value="'.$current_blog.'" />';
				$output .= '</div>';


			$output .= '</td>';

			return apply_filters( $this->func_hook_prefix . 'load_add_new_item_option' , $output );
		}

		function load_products_option( $selected_blog = 1 ) {

			global $mppf;

			$global_compare = $mppf->load_option( $mppf->option_group . '_compareproducts_enable_global_compare' , 'no' );

			$output = '<option value="">' . __( 'Select Item:' , 'pro' ) . '</option>';

			$current_blog_id = get_current_blog_id();

			if ( is_multisite() && $global_compare == 'yes' ) {

				switch_to_blog($selected_blog);
				$products = get_posts( array('post_type' => 'product' , 'orderby' => 'title', 'order' => 'ASC' , 'numberposts' => -1) );
				restore_current_blog();

			} else {
				$products = get_posts( array('post_type' => 'product' , 'orderby' => 'title', 'order' => 'ASC' , 'numberposts' => -1) );
			}

			if (!empty($products) && is_array($products)) {
				foreach ($products as $product) {
					$output .= '<option value="'.$product->ID.'">' . $product->post_title . '</option>';
				}
			}

			if ( is_multisite() ) {
				$after_restore_blog_id = get_current_blog_id();
				if ( $current_blog_id != $after_restore_blog_id )
					switch_to_blog( $current_blog_id );
			}

			return apply_filters( $this->func_hook_prefix . 'load_products_option' , $output );
		}

		function load_products_option_ajax() {

			$select_id_attr = esc_html(esc_attr(trim($_POST['addnew_select_id'])));
			$blog_id = intval($_POST['current_blog_id']);

			$output = '<select name="'.$select_id_attr.'" id="'.$select_id_attr.'">' . $this->load_products_option( $blog_id ) . '</select>';
			$output .= '<input type="hidden" name="'.$select_id_attr.'blog" id="'.$select_id_attr.'blog" value="'.$blog_id.'" />';

	      	if (defined('DOING_AJAX') && DOING_AJAX) {
	        	echo apply_filters( $this->func_hook_prefix . 'load_products_option_ajax' , $output , $blog_id , $select_id_attr );
				exit;
	      	}
		}

		function load_sites_option( $current_blog = 1 ) {

			global $wpdb;

		    $query = "SELECT blog_id FROM " . $wpdb->base_prefix . "blogs WHERE spam != '1' AND archived != '1' AND deleted != '1' AND public = '1' ORDER BY path";
		    
		    $blogs = $wpdb->get_results($query);
		    $output = '';

			if (!empty($blogs)) {
			    foreach($blogs as $blog){
			    	$blog_id = intval($blog->blog_id);
					$blog_details = get_blog_details($blog_id);
					$selected = selected( $current_blog , $blog_id , false);
					$output .= '<option value="'.$blog_id.'"'.( !empty($selected) ? ' ' . $selected : '' ).'>'.$blog_details->blogname.'</option>';
				}
			}

			return apply_filters( $this->func_hook_prefix . 'load_sites_option' , $output );
		}

		function load_compare_modal( $args = array() ) {

			$defaults = array(
				'showtitle' => 'no',
				'title' => __( 'Compare Chart: ' , 'pro' ),
				'hideprice' => false,
				'btnclass' => '',
				'class' => '',
				'style' => ''
			);		

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			$output = '<div id="mp-premium-features">';

				$output .= apply_filters( $this->hook_prefix . 'compareproducts_modal_title' , '<h3 class="mppf-modal-title">'.__( 'Compare products' , 'pro' ).'</h3>' );

				$output .= apply_filters( $this->hook_prefix . 'compareproducts_modal_results' , '<div class="mppf-compare-results-container">
					<form id="mppf-compare-products" class="mppf-compare-products-form" method="post" action="">'. $this->load_compare_results( array(
						'showtitle' => $showtitle,
						'title' => $title,
						'hideprice' => $hideprice,
						'btnclass' => $btnclass,
						'class' => $class,
						'style' => $style
					) ) .'</form></div>' );

			$output .= '</div>'; // end - mp-premium-features

		    return apply_filters( $this->func_hook_prefix . 'load_compare_modal' , $output , $instance );
		}

		function compare_results_sc( $atts ) {

			extract( shortcode_atts( array(
				'showtitle' => 'yes',
				'title' => 'Compare Chart',
				'hideprice' => 'no',
				'btncolor' => 'lightblue',
				'style' => '',
				'class' => ''
			), $atts ) );

			$style = ( !empty($style) ? esc_attr($style) : '' );
			$class = ( !empty($class) ? esc_attr($class) : '' );
			$showtitle = ( !empty($showtitle) ? esc_attr($showtitle) : '' );
			$title = ( !empty($title) ? esc_attr($title) : '' );

			switch ($btncolor) {
				case 'grey':
					$btnclass = '';
					break;
				case 'blue':
					$btnclass = ' mppf-btn-blue';
					break;
				case 'lightblue':
					$btnclass = ' mppf-btn-lightblue';
					break;
				case 'green':
					$btnclass = ' mppf-btn-green';
					break;
				case 'yellow':
					$btnclass = ' mppf-btn-yellow';
					break;
				case 'red':
					$btnclass = ' mppf-btn-red';
					break;
				case 'black':
				default:
					$btnclass = ' mppf-btn-black';
					break;				
			}

			$args = array( 'showtitle' => $showtitle , 'title' => $title , 'hideprice' => ( $hideprice == 'yes' ? true : false ) , 'btnclass' => $btnclass , 'class' => $class , 'style' => $style );

			return apply_filters( $this->func_hook_prefix . 'compare_results_sc' , '<div id="mp-premium-features"><form id="mppf-compare-products" class="mppf-compare-products-form" method="post" action="">' . $this->load_compare_results( $args ) . '</form></div>' , $args );
		}

		/* Ajax functions
		------------------------------------------------------------------------------------------------------------------- */

		function add_to_compare() {

	    	global $mp, $mppf;

			$blog_id =  is_multisite() ? ( isset( $_POST['currentblogid'] ) ? esc_attr(intval($_POST['currentblogid'])) : 1 ) : 1;
			$post_id = isset( $_POST['mppf_compare_item'] ) ? esc_attr(intval($_POST['mppf_compare_item'])) : '';

			$settings = !empty($_SESSION[ $this->hook_prefix . '_compare_results_settings']) ? $_SESSION[ $this->hook_prefix . '_compare_results_settings'] : array( 'showtitle' => 'no' , 'title' => '' , 'hideprice' => false , 'btnclass' => '' , 'iconclass' => '' , 'class' => '' , 'style' => '' );

			$error = ( $this->item_in_compare($post_id) ? '<span class="mppf-btn-text mppf-show">' . __( 'Item already in the list.' , 'pro' ) . '</span>' : ( $this->max_items_in_compare() ? '<span class="mppf-btn-text mppf-show">' . __( 'There are already 3 items in the list.' , 'pro' ) . '</span>' : '' ) );
			$error = apply_filters( $this->hook_prefix . 'compareproducts_error_tag', $error, $post_id );

			$success = apply_filters( $this->hook_prefix . 'compareproducts_success_tag', '<span class="mppf-btn-text mppf-show">' . __( 'Item Added!' , 'pro' ) . '</span>' , $post_id );

		    $notinlist = $this->item_in_compare($post_id) ? false : true;
		    $spaceinlist = $this->max_items_in_compare() ? false : true;  

		    $compare_results_args = array( 
					'showtitle' => 'no',
					'title' => $settings['title'],
					'hideprice' => $settings['hideprice'],
					'btnclass' => $settings['btnclass'],
					'class' => $settings['class'],
					'style' => $settings['style']
				);

		    if (!empty($post_id) && $notinlist && $spaceinlist ) {

			    if (!empty($_SESSION[ $this->hook_prefix . '_compare_items']) && is_array($_SESSION[ $this->hook_prefix . '_compare_items'])) {
			    	$_SESSION[ $this->hook_prefix . '_compare_items'][] = array( 'item' => $post_id , 'blog' => $blog_id);
			    } else {
			    	$_SESSION[ $this->hook_prefix . '_compare_items'] = array();
			    	$_SESSION[ $this->hook_prefix . '_compare_items'][] = array( 'item' => $post_id , 'blog' => $blog_id);
			    }

				$compare_results = $this->load_compare_modal( $compare_results_args );

				if (defined('DOING_AJAX') && DOING_AJAX) {
					echo apply_filters ( $this->func_hook_prefix . 'add_to_compare' , 'success|SEP|' . $success . '|SEP|' . $compare_results , $post_id , $blog_id , $compare_results_args );
					exit;
				}
		    	
		    } else {

				$compare_results = $this->load_compare_modal( $compare_results_args );

				if (defined('DOING_AJAX') && DOING_AJAX) {
					echo apply_filters( $this->func_hook_prefix . 'add_to_compare' , 'error|SEP|' . $error . '|SEP|' . $compare_results , $post_id , $blog_id , $compare_results_args );
					exit;
				}
			} 
		}

		function update_compare_results(){

			global $mp;

			$session = array();
			$settings = !empty($_SESSION[ $this->hook_prefix . '_compare_results_settings']) ? $_SESSION[ $this->hook_prefix . '_compare_results_settings'] : array( 'showtitle' => 'no' , 'title' => '' , 'hideprice' => false , 'btnclass' => '' , 'class' => '' , 'style' => '' );

			for ($i=0; $i < 3 ; $i++) {
				if ( isset( $_POST['removefromcompare'.$i] ) && $_POST['removefromcompare'.$i] == '1' ) { 
					// do nothing
				} else {
					if ( isset( $_POST['compareitem'.$i] ) && !empty( $_POST['compareitem'.$i] ) && isset( $_POST['compareitem'.$i.'blog'] ) && !empty( $_POST['compareitem'.$i.'blog'] ) )
						$session[] = array( 'item' => esc_attr(intval($_POST['compareitem'.$i])) , 'blog' => esc_attr(intval($_POST['compareitem' . $i . 'blog'])) );
				}
			}

			for ($k=1; $k < 4; $k++) { 
			    if ( isset( $_POST['addnewcompare' . $k ] ) && !empty( $_POST['addnewcompare' . $k ] ) && isset( $_POST['addnewcompare'.$k.'blog'] ) && !empty( $_POST['addnewcompare'.$k.'blog'] ) )
			    	$session[] = array( 'item' => esc_attr(intval($_POST['addnewcompare' . $k ])) , 'blog' => esc_attr(intval($_POST['addnewcompare'.$k.'blog'])) );
			}

		    $_SESSION[ $this->hook_prefix . '_compare_items'] = $session;

	      	if (defined('DOING_AJAX') && DOING_AJAX) {
	        	echo apply_filters( $this->func_hook_prefix . 'update_compare_results' , 1 . '|SEP|' . $this->load_compare_results( array( 'showtitle' => $settings['showtitle'] , 'title' => $settings['title'] , 'btnclass' => $settings['btnclass'] , 'class' => $settings['class'] , 'style' => $settings['style'] ) ) , $session , $settings );
				exit;
	      	}
		}

		/* Validation functions
		------------------------------------------------------------------------------------------------------------------- */

		function item_in_compare($post_id) {

			$session = !empty($_SESSION[ $this->hook_prefix . '_compare_items']) ? $_SESSION[ $this->hook_prefix . '_compare_items'] : '';
			$blog_id = is_multisite() ? get_current_blog_id() : 1;

			$instance = array(
				'post_id' => $post_id,
				'blog_id' => $blog_id,
				'session' => $session
				);

			if (!empty($session) && is_array($session)) {

				foreach ($session as $key => $value) {

					$item = esc_html(esc_attr(trim($value['item'])));
					$blog = esc_html(esc_attr(trim($value['blog'])));

					if ( $post_id == $item && $blog_id == $blog ) {
						return apply_filters( 'item_in_compare' , true , $instance );
						exit;
					}
				}

				return apply_filters( $this->func_hook_prefix . 'item_in_compare' , false , $instance );

			} else {
				return apply_filters( $this->func_hook_prefix . 'item_in_compare' , false , $instance );
			}
		}

		function max_items_in_compare() {

			$session = !empty($_SESSION[ $this->hook_prefix . '_compare_items']) ? $_SESSION[ $this->hook_prefix . '_compare_items'] : '';
			$count = 0;

			if (!empty($session) && is_array($session)) {

				foreach ($session as $key => $value) {
					$item = esc_html(esc_attr(trim($value['item'])));
					if (!empty($item))
						$count++;
				}

				if ($count >= 3) 
					return apply_filters( $this->func_hook_prefix . 'max_items_in_compare' , true , $session );
				else
					return apply_filters( $this->func_hook_prefix . 'max_items_in_compare' , false , $session );

			} else {
				return apply_filters( $this->func_hook_prefix . 'max_items_in_compare' , false , $session );
			}
		}

	} // end - class MPPremiumFeatures_CompareProducts