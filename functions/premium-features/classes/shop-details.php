<?php

	class MPPremiumFeatures_ShopDetails {

	    private $plugin_path;
	    private $plugin_url;
	    private $plugin_slug;

	    function __construct( $args = array() ) {

			$defaults = array(
				'file' => '',
				'plugin_path' => MPPF_PATH,
				'plugin_url' => MPPF_URL,
				'plugin_slug' => 'mppremiumfeatures',
				'hook_prefix' => 'mppremiumfeatures',		
			);

			$instance = wp_parse_args( $args, $defaults );

	    	$this->plugin_unique_id = $instance['file'];
	        $this->plugin_path = $instance['plugin_path'];
	        $this->plugin_url = $instance['plugin_url'];
	        $this->plugin_slug = $instance['plugin_slug'];
	        $this->hook_prefix = $instance['hook_prefix'];
	        $this->func_hook_prefix = 'mppremiumfeatures_shopdetails_func_';

	        add_action( 'after_setup_theme', array(&$this, 'init') , 20 );

	    }

	    function init() {

	        define( 'MPPF_SHOP_DETAILS_WIDGET_RULES' , apply_filters( $this->hook_prefix . '_shop_details_widget_rules' , true ) );

	        // register widget
	        //if ( is_multisite() && ( true == MPPF_SHOP_DETAILS_WIDGET_RULES ) )
	        	add_action( 'widgets_init' , create_function('', 'return register_widget("MPPF_Shop_Details_Widget");') );
	    }

		/* Load Shop Details Container
		------------------------------------------------------------------------------------------------------------------- */

	    function load_container( $args = array() ) {

			$defaults = array(
				'shopname' => '',
				'shoplogo' => '',
				'shopdesc' => '',
				'aboutshopbgcolor' => '',
				'aboutshoptextcolor' => '',
				'aboutshopbordercolor' => '',
				'showlisting' => true,
				'listingtitle' => '',
				'listingbgcolor' => '',
				'listingtextcolor' => '',
				'listingbordercolor' => '',
				'per_page' => 6,
				'order_by' => 'date',
				'order' => 'DESC',
				'category' => '',
				'tag' => '',
				'showproductprice' => true,
				'showproductname' => true,
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $mppf;

			$output = '<div id="mp-premium-features">';
				$output .= '<div class="mppf-shop-details-widget">';

					if ( !empty($shoplogo) || !empty($shopname) || !empty($shopdesc) ) {
						$output .= '<div class="mppf-sd-about-shop" style="'.( !empty($aboutshopbgcolor) ? 'background: ' . esc_attr( $aboutshopbgcolor ) . ';' : '' ).( !empty($aboutshopbordercolor) ? 'border: 1px solid ' . esc_attr( $aboutshopbordercolor ) . ';' : '' ).'">';
							if ( !empty($shoplogo) )
								$output .= '<a href="'.home_url().'" class="mppf-sd-shop-logo"><img src="'.esc_url( $shoplogo ).'" /></a>';

							if ( !empty($shopname) )
								$output .= '<h2 class="mppf-sd-shop-name"'.( !empty($aboutshoptextcolor) ? ' style="color: ' . esc_attr( $aboutshoptextcolor ) . ';"' : '' ).'>'.esc_attr( $shopname ).'</h2>';

							if ( !empty($shopdesc) )
								$output .= '<div class="mppf-sd-shop-desc"'.( !empty($aboutshoptextcolor) ? ' style="color: ' . esc_attr( $aboutshoptextcolor ) . ';"' : '' ).'>'.wpautop( $mppf->kses_it( $shopdesc ) ).'</div>';
						$output .= '</div>';
					}

					$output .= ( $showlisting ? $this->load_products_list( array( 
						'listingtitle' =>$listingtitle,
						'listingbgcolor' => $listingbgcolor,
						'listingtextcolor' => $listingtextcolor,
						'listingbordercolor' => $listingbordercolor,
						'per_page' => $per_page,
						'order_by' => $order_by,
						'order' => $order,
						'category' => $category,
						'tag' => $tag,
						'showproductprice' => $showproductprice,
						'showproductname' => $showproductname,
					) ) : '' );

				$output .= '</div>'; // end - mppf-shop-details-widget
			$output .= '</div>'; // end - mp-premium-features

	    	return apply_filters( $this->func_hook_prefix . 'load_container' , $output , $instance );
	    }

		/* Load products list
		------------------------------------------------------------------------------------------------------------------- */

	    function load_products_list( $args = array() ) {

			$defaults = array(
				'listingtitle' => '',
				'listingbgcolor' => '',
				'listingtextcolor' => '',
				'listingbordercolor' => '',
				'per_page' => 6,
				'order_by' => 'date',
				'order' => 'DESC',
				'category' => '',
				'tag' => '',
				'showproductprice' => true,
				'showproductname' => true,
				'maxslides' => 3,
				'minslides' => 2,
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $wp_query, $mp;

			//setup taxonomy if applicable
			if ($category) {
			    $taxonomy_query = '&product_category=' . sanitize_title($category);
			} else if ($tag) {
			    $taxonomy_query = '&product_tag=' . sanitize_title($tag);
			} else if (isset($wp_query->query_vars['taxonomy']) && ($wp_query->query_vars['taxonomy'] == 'product_category' || $wp_query->query_vars['taxonomy'] == 'product_tag')) {
			    $term = get_queried_object(); //must do this for number tags
			    $taxonomy_query = '&' . $term->taxonomy . '=' . $term->slug;
			} else {
			    $taxonomy_query = '';
			}

			// setup pagination
			$paginate_query = '&showposts='.intval($per_page);

			//get order by
			if (!$order_by) {
			    if ($mp->get_setting('order_by') == 'price')
			      $order_by_query = '&meta_key=mp_price_sort&orderby=meta_value_num';
			    else if ($mp->get_setting('order_by') == 'sales')
			      $order_by_query = '&meta_key=mp_sales_count&orderby=meta_value_num';
			    else
			      $order_by_query = '&orderby='.$mp->get_setting('order_by');
			} else {
			  	if ('price' == $order_by)
			  		$order_by_query = '&meta_key=mp_price_sort&orderby=meta_value_num';
			    else if('sales' == $order_by)
			      $order_by_query = '&meta_key=mp_sales_count&orderby=meta_value_num';
			    else
			    	$order_by_query = '&orderby='.$order_by;
			}

			//get order direction
			if (!$order) {
			    $order_query = '&order='.$mp->get_setting('order');
			} else {
			    $order_query = '&order='.$order;
			}

			$the_query = new WP_Query( 'post_type=product&post_status=publish' . $taxonomy_query . $paginate_query . $order_by_query . $order_query );

			$randnum = rand( 1 , 999 );
		  	$output = '<div class="mppf-sd-products-list" style="'.( !empty($listingbgcolor) ? 'background: ' . esc_attr( $listingbgcolor ) . ';' : '' ).( !empty($listingbordercolor) ? 'border: 1px solid ' . esc_attr( $listingbordercolor ) . ';' : '' ).'">';

				$output .= ( !empty($listingtitle) ? '<h4 class="mppf-sd-products-list-title" style="'.( !empty($listingtextcolor) ? 'color: ' . esc_attr( $listingtextcolor ) . ';' : '' ).'">'.esc_attr( $listingtitle ).'</h4>' : '' );

				$output .= '<div class="mppf-sdslider-'.$randnum.'">';

			  	if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();

			  		$post_id = get_the_ID();
			  		$imagesize = apply_filters( $this->hook_prefix . '_single_product_image_size' , 'thumbnail' , $instance );
					//$default_img_url = esc_url( get_option('mpt_mp_default_product_img') );
					$default_img_url = esc_url( get_blog_option(1,'mpt_mp_default_product_img') );
					$default_img_id = ( !empty($default_img_url) ? intval( $this->get_image_id( $default_img_url ) ) : NULL );
					$product_price = ( function_exists('mp_product_price') ? mp_product_price( false , $post_id , false ) : '' );

					$output .= '<div class="mppf-sd-single-product">';

						if ( has_post_thumbnail( $post_id ) ) {
							$fullimage = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' );
							$output .= get_the_post_thumbnail($post_id , $imagesize );
						} elseif ( !empty($default_img_id) && is_numeric($default_img_id) ) {
							$output .= wp_get_attachment_image( $default_img_id, $imagesize );
						} else {
							$output .= '<img src="' . apply_filters('mp_default_product_img', $mp->plugin_url . 'images/default-product.png') . '" width="'.apply_filters( $this->hook_prefix . '_sdslider_image_width' , 100 ).'" height="'.apply_filters( $this->hook_prefix . '_sdslider_image_width' , 100 ).'" class="mppf-default-product-img" />';
						}

						//$output .= ( $showproductprice && !empty($product_price) ? '<span class="mppf-sd-single-product-price">' . $product_price . '</span>' : '' );

						$output .= '<a href="'.get_permalink( $post_id ).'" class="mppf-sd-single-product-link mppf-powertip-trigger" data-powertip="'.get_the_title( $post_id ).' - '.$this->load_price( $post_id ).'"><i class="fa fa-plus"></i></a>';

						$output .= '';

					$output .= '</div>'; // end - mppf-sd-single-product

				endwhile; endif;
				wp_reset_query();

				$output .= '</div>'; // end - mppf-sdslider

				$output .= '<div class="mppf-sdslider-control">';
					$output .= '<span class="mppf-sdslider-'.$randnum.'-prev mppf-sdslider-controller"></span>';
					$output .= '<span class="mppf-sdslider-'.$randnum.'-next mppf-sdslider-controller"></span>';
				$output .= '</div>'; // end - mppf-sdslider-control

				$output .= '
					<script type="text/javascript">
						jQuery(document).ready(function () {
					    	jQuery(".mppf-sdslider-'.$randnum.'").bxSlider({
					    		slideSelector: "div.mppf-sd-single-product",
					    		slideWidth: '.apply_filters( $this->hook_prefix . '_sdslider_image_width' , 100 ).',
    							minSlides: '.intval($minslides).',
    							maxSlides: '.intval($maxslides).',
    							slideMargin: 10,
						    	pager: false,
					    		nextText: "",
					    		prevText: "",
					    		nextSelector: ".mppf-sdslider-'.$randnum.'-next",
					    		prevSelector: ".mppf-sdslider-'.$randnum.'-prev",
							});
						});
					</script>';

			$output .= '</div>'; // end - mppf-sd-products-list

	    	return apply_filters( $this->func_hook_prefix . 'load_products_list' , $output , $instance );
	    }

	    function load_price( $post_id = null ) {

	    	global $id, $mp, $mppf, $mppf_getthisfree;
	    	$post_id = ( NULL === $post_id ) ? $id : $post_id;
	    	$meta = get_post_custom($post_id);

			//unserialize
			foreach ($meta as $key => $val) {
				$meta[$key] = maybe_unserialize($val[0]);
				if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file" && $key != "mp_price_sort")
						$meta[$key] = array($meta[$key]);
			}

			if ( is_array($meta["mp_price"]) && !empty($meta["mp_price"]) ) {
				if ( count($meta["mp_price"]) == 1 || !empty($meta["mp_file"]) ) {
					if ($meta["mp_is_sale"]) {
						$price = $mp->format_currency( '' , $meta["mp_sale_price"][0] );
					} else {
						$price = $mp->format_currency( '' , $meta["mp_price"][0] );
					}
				} else { 
					if ($meta["mp_is_sale"]) {
						asort($meta["mp_sale_price"], SORT_NUMERIC);
						$lowest = array_slice($meta["mp_sale_price"], 0, 1, true);
						$keys = array_keys($lowest);
						$mp_price = $meta["mp_price"][$keys[0]];
						$mp_sale_price = array_pop($lowest);
						$price = __('From', $this->plugin_slug ) . ' ' . $mp->format_currency( '' , $mp_sale_price );
					} else {
						sort($meta["mp_price"], SORT_NUMERIC);
						$price = __('From', $this->plugin_slug ) . ' ' . $mp->format_currency( '' , $meta["mp_price"][0] );
					}
				}
			}

			$getthisfree = $mppf->load_option( $mppf->option_group . '_getthisfree_enable_feature' , 'no' );
			if ( $getthisfree == 'yes' && $meta["mp_price"][0] <= 0 )
				$price = $mppf->load_option( $mppf->option_group . '_getthisfree_price_label' , __( 'FREE' , $this->plugin_slug ) );

	    	return apply_filters( $this->func_hook_prefix . 'load_price' , $price , $post_id );
	    }

		function get_image_id( $image_url = '' ) {
			global $wpdb;
			$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_url'";
			$id = $wpdb->get_var($query);
			return $id;
		}

	} // end - class MPPremiumFeatures_ShopDetails

	/* Register Shop Details Widget Class
	------------------------------------------------------------------------------------------------------------------- */

	class MPPF_Shop_Details_Widget extends WP_Widget {

		function MPPF_Shop_Details_Widget() {
			$widget_ops = array( 'classname' => 'mppf_shop_details_widget', 'description' => __( 'shop details widget description', MPPF_PLUGIN_SLUG ) );
			$this->WP_Widget('mppf_shop_details_widget', __('Shop Details Widget', MPPF_PLUGIN_SLUG), $widget_ops );
		}

		function widget( $args, $instance ) {

			if ($instance['only_store_pages'] && !mp_is_shop_page())
				return;

			extract( $args );

			global $mppf, $mppf_shopdetails;

			$title = esc_attr( $instance['title'] );
			//$c = $instance['count'] ? 'true' : 'false';
			//$taxonomy_type = !empty($instance['taxonomy_type']) ? $instance['taxonomy_type'] : 'product_category';

			echo $before_widget;

			if ( $title )
				echo $before_title . $title . $after_title;

				echo $mppf_shopdetails->load_container( array( 
						'shopname' => ( !empty( $instance['shopname'] ) ? esc_attr( $instance['shopname'] ) : '' ),
						'shoplogo' => ( !empty( $instance['shoplogo'] ) ? esc_url( $instance['shoplogo'] ) : '' ),
						'shopdesc' => ( !empty( $instance['shopdesc'] ) ? $mppf->kses_it( $instance['shopdesc'] ) : '' ),
						'aboutshopbgcolor' => ( !empty( $instance['aboutshopbgcolor'] ) ? esc_attr( $instance['aboutshopbgcolor'] ) : '' ),
						'aboutshoptextcolor' => ( !empty( $instance['aboutshoptextcolor'] ) ? esc_attr( $instance['aboutshoptextcolor'] ) : '' ),
						'aboutshopbordercolor' => ( !empty( $instance['aboutshopbordercolor'] ) ? esc_attr( $instance['aboutshopbordercolor'] ) : '' ),
						'listingtitle' => ( !empty( $instance['listingtitle'] ) ? esc_attr( $instance['listingtitle'] ) : '' ),
						'listingbgcolor' => ( !empty( $instance['listingbgcolor'] ) ? esc_attr( $instance['listingbgcolor'] ) : '#fafafa' ),
						'listingtextcolor' => ( !empty( $instance['listingtextcolor'] ) ? esc_attr( $instance['listingtextcolor'] ) : '' ),
						'listingbordercolor' => ( !empty( $instance['listingbordercolor'] ) ? esc_attr( $instance['listingbordercolor'] ) : '#d2d2d2' ),
						'per_page' => ( !empty($instance['showposts']) ? intval($instance['showposts']) : 6 ),
						'order_by' => ( !empty($instance['orderby']) ? esc_attr($instance['orderby']) : 'date' ),
						'order' => ( !empty($instance['order']) ? esc_attr($instance['order']) : 'DESC' ),
						'category' => ( $instance['taxonomy_type'] == 'category' ? esc_attr($instance['taxonomy']) : '' ),
						'tag' => ( $instance['taxonomy_type'] == 'tag' ? esc_attr($instance['taxonomy']) : '' ),
					) );

			echo $after_widget;
		}

		function update( $new_instance, $old_instance ) {

			global $mppf;

			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['shopname'] = esc_attr($new_instance['shopname']);
			$instance['shoplogo'] = esc_url($new_instance['shoplogo']);
			$instance['shopdesc'] = $mppf->kses_it($new_instance['shopdesc']);
			$instance['aboutshopbgcolor'] = esc_attr($new_instance['aboutshopbgcolor']);
			$instance['aboutshoptextcolor'] = esc_attr($new_instance['aboutshoptextcolor']);
			$instance['aboutshopbordercolor'] = esc_attr($new_instance['aboutshopbordercolor']);
			$instance['listingtitle'] = esc_attr($new_instance['listingtitle']);
			$instance['listingbgcolor'] = esc_attr($new_instance['listingbgcolor']);
			$instance['listingtextcolor'] = esc_attr($new_instance['listingtextcolor']);
			$instance['listingbordercolor'] = esc_attr($new_instance['listingbordercolor']);			
			$instance['showposts'] = !empty($new_instance['showposts']) ? intval($new_instance['showposts']) : 4;
			$instance['taxonomy_type'] = $new_instance['taxonomy_type'];
			$instance['taxonomy'] = ($new_instance['taxonomy_type']) ? sanitize_title($new_instance['taxonomy']) : '';
			$instance['orderby'] = !empty($new_instance['orderby']) ? esc_attr($new_instance['orderby']) : 'date';
			$instance['order'] = !empty($new_instance['order']) ? esc_attr($new_instance['order']) : 'DESC';
			//$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
			$instance['only_store_pages'] = !empty($new_instance['only_store_pages']) ? 1 : 0;

			return $instance;
		}

		function form( $instance ) {
			//Defaults
			$instance = wp_parse_args( (array) $instance, array( 
				'title' => '',
				'shopname' => NULL,
				'shoplogo' => NULL,
				'shopdesc' => NULL,
				'aboutshopbgcolor' => '',
				'aboutshoptextcolor' => '',
				'aboutshopbordercolor' => '',
				'listingtitle' => __( 'Featured Products' , MPPF_PLUGIN_SLUG ),
				'listingbgcolor' => '#fafafa',
				'listingtextcolor' => '',
				'listingbordercolor' => '#d2d2d2',
				'showposts' => 6,
				'orderby' => 'date',
				'order' => 'DESC',
				'taxonomy' => '',
				'taxonomy_type' => '',
				'only_store_pages' => 0
			) );

			global $mppf;

			//$mpt_logo_upload = esc_url( get_option('mpt_sitelogo') );
			$mpt_logo_upload = esc_url( get_blog_option(1,'mpt_sitelogo') );

			$title = esc_attr( $instance['title'] );
			$shopname = ( !is_null( $instance['shopname'] ) ? esc_attr( $instance['shopname'] ) : esc_attr( get_bloginfo('name') ) );
			$shoplogo = ( !is_null( $instance['shoplogo'] ) ? esc_url( $instance['shoplogo'] ) : ( !empty( $mpt_logo_upload ) ? $mpt_logo_upload : '' ) );
			$shopdesc = ( !is_null( $instance['shopdesc'] ) ? $mppf->kses_it( $instance['shopdesc'] ) : esc_attr( get_bloginfo('description') ) );
			$aboutshopbgcolor = ( !empty( $instance['aboutshopbgcolor'] ) ? esc_attr( $instance['aboutshopbgcolor'] ) : '' );
			$aboutshoptextcolor = ( !empty( $instance['aboutshoptextcolor'] ) ? esc_attr( $instance['aboutshoptextcolor'] ) : '' );
			$aboutshopbordercolor = ( !empty( $instance['aboutshopbordercolor'] ) ? esc_attr( $instance['aboutshopbordercolor'] ) : '' );
			$listingtitle = ( !empty( $instance['listingtitle'] ) ? esc_attr( $instance['listingtitle'] ) : '' );
			$listingbgcolor = ( !empty( $instance['listingbgcolor'] ) ? esc_attr( $instance['listingbgcolor'] ) : '#fafafa' );
			$listingtextcolor = ( !empty( $instance['listingtextcolor'] ) ? esc_attr( $instance['listingtextcolor'] ) : '' );
			$listingbordercolor = ( !empty( $instance['listingbordercolor'] ) ? esc_attr( $instance['listingbordercolor'] ) : '#d2d2d2' );
			$showposts = !empty($instance['showposts']) ? intval($instance['showposts']) : 6;
    		$taxonomy_type = (!empty($instance['taxonomy_type']) ? $instance['taxonomy_type'] : '' );
    		$taxonomy = (!empty($instance['taxonomy']) ? $instance['taxonomy'] : '' );
			$orderby = !empty($instance['orderby']) ? esc_attr($instance['orderby']) : 'date';
			$order = !empty($instance['order']) ? esc_attr($instance['order']) : 'DESC';
			//$count = isset($instance['count']) ? (bool) $instance['count'] : true;
			$only_store_pages = isset( $instance['only_store_pages'] ) ? (bool) $instance['only_store_pages'] : false;

			// widget title
			$output = '<div style="margin-bottom: 7px;">'; 
				$output .= '<label for="'.$this->get_field_id('title').'">'.__( 'Title:' , MPPF_PLUGIN_SLUG ).'</label>';
				$output .= '<input class="widefat" id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" type="text" value="'.$title.'" />';
			$output .= '</div>';

			// About Shop
			$output .= '<p style="margin-bottom: 10px; font-weight: bold;">'.__( 'About Shop' , MPPF_PLUGIN_SLUG ).'</p>';
			$output .= '<div style="border: 1px solid #dddddd; padding: 5px 10px; background: #fafafa; margin-bottom: 10px;">';

				// Shop Name
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('shopname').'">'.__( 'Name:' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<input class="widefat" id="'.$this->get_field_id('shopname').'" name="'.$this->get_field_name('shopname').'" type="text" value="'.$shopname.'" />';
				$output .= '</p>';

				// Shop Logo
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('shoplogo').'">'.__( 'Logo:' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<input class="widefat mppf-widget-input-upload" id="'.$this->get_field_id('shoplogo').'" name="'.$this->get_field_name('shoplogo').'" type="text" value="'.$shoplogo.'" />';
					$output .= '<a href="#" class="button sa_settings_upload_button" rel="image" style="margin-top: 5px;">'.__( 'Upload' , MPPF_PLUGIN_SLUG ).'</a>';

				$output .= '</p>';

				// Shop Desc
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('shopdesc').'">'.__( 'Description:' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<textarea class="widefat" id="'.$this->get_field_id('shopdesc').'" name="'.$this->get_field_name('shopdesc').'" rows="5">'.stripslashes( $shopdesc ).'</textarea>';
				$output .= '</p>';

				// background color
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('aboutshopbgcolor').'" style="line-height: 22px; vertical-align: top; margin-right: 5px;">'.__( 'Background Color:' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<input class="sa-input-color-picker" id="'.$this->get_field_id('aboutshopbgcolor').'" name="'.$this->get_field_name('aboutshopbgcolor').'" type="text" value="'.$aboutshopbgcolor.'" />';
				$output .= '</p>';

				// text color
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('aboutshoptextcolor').'" style="line-height: 22px; vertical-align: top; margin-right: 5px;">'.__( 'Text Color:' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<input class="sa-input-color-picker" id="'.$this->get_field_id('aboutshoptextcolor').'" name="'.$this->get_field_name('aboutshoptextcolor').'" type="text" value="'.$aboutshoptextcolor.'" />';
				$output .= '</p>';

				// border color
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('aboutshopbordercolor').'" style="line-height: 22px; vertical-align: top; margin-right: 5px;">'.__( 'Border Color:' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<input class="sa-input-color-picker" id="'.$this->get_field_id('aboutshopbordercolor').'" name="'.$this->get_field_name('aboutshopbordercolor').'" type="text" value="'.$aboutshopbordercolor.'" />';
				$output .= '</p>';

			$output .= '</div>'; // end - section

			// Product List Settings
			$output .= '<p style="margin-bottom: 10px; font-weight: bold;">'.__( 'Product List Settings' , MPPF_PLUGIN_SLUG ).'</p>';
			$output .= '<div style="border: 1px solid #dddddd; padding: 5px 10px; background: #fafafa; margin-bottom: 10px;">';

				$output .= '<div style="margin-bottom: 7px;">'; 
					$output .= '<label for="'.$this->get_field_id('listingtitle').'">'.__( 'Section Title:' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<input class="widefat" id="'.$this->get_field_id('listingtitle').'" name="'.$this->get_field_name('listingtitle').'" type="text" value="'.$listingtitle.'" />';
				$output .= '</div>';

				// number of products
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('showposts').'">'.__( 'Number of Products' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<input size="3" id="'.$this->get_field_id('showposts').'" name="'.$this->get_field_name('showposts').'" type="text" value="'.$showposts.'" />';
				$output .= '</p>';

				// taxonomy filter
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('taxonomy_type').'">'.__('Taxonomy Filter: ', MPPF_PLUGIN_SLUG) . '</label><br />';
					$output .= '<select id="'.$this->get_field_id('taxonomy_type').'" name="'.$this->get_field_name('taxonomy_type').'">';
						$output .= '<option value=""'.selected( $taxonomy_type , '' , false ).'>'.__( 'No Filter' , MPPF_PLUGIN_SLUG ).'</option>';
						$output .= '<option value="category"'.selected( $taxonomy_type , 'category' , false ).'>'.__( 'Category' , MPPF_PLUGIN_SLUG ).'</option>';
						$output .= '<option value="tag"'.selected( $taxonomy_type , 'tag' , false ).'>'.__( 'Tag' , MPPF_PLUGIN_SLUG ).'</option>';
					$output .= '</select>';
					$output .= '<input id="' . $this->get_field_id('taxonomy') . '" name="' . $this->get_field_name('taxonomy') . '" type="text" size="15" value="' . $taxonomy . '" title="' . __('Enter the Slug', MPPF_PLUGIN_SLUG) . '" />';
				$output .= '</p>';

				// order by
				$output .= '<p>';
					$output .= '<label for="'.$this->get_field_id('orderby').'">'.__('Order Products By: ', MPPF_PLUGIN_SLUG) . '</label><br />';
					$output .= '<select id="'.$this->get_field_id('orderby').'" name="'.$this->get_field_name('orderby').'">';
						$output .= '<option value="title"'.($orderby == 'title' ? ' selected="selected"' : '').'>'.__( 'Product Name' , MPPF_PLUGIN_SLUG ).'</option>';
						$output .= '<option value="date"'.($orderby == 'date' ? ' selected="selected"' : '').'>'.__( 'Publish Date' , MPPF_PLUGIN_SLUG ).'</option>';
						$output .= '<option value="ID"'.($orderby == 'ID' ? ' selected="selected"' : '').'>'.__( 'Product ID' , MPPF_PLUGIN_SLUG ).'</option>';
						$output .= '<option value="author"'.($orderby == 'author' ? ' selected="selected"' : '').'>'.__( 'Product Author' , MPPF_PLUGIN_SLUG ).'</option>';
						$output .= '<option value="sales"'.($orderby == 'sales' ? ' selected="selected"' : '').'>'.__( 'Number of Sales' , MPPF_PLUGIN_SLUG ).'</option>';
						$output .= '<option value="price"'.($orderby == 'price' ? ' selected="selected"' : '').'>'.__( 'Product Price' , MPPF_PLUGIN_SLUG ).'</option>';
						$output .= '<option value="rand"'.($orderby == 'rand' ? ' selected="selected"' : '').'>'.__( 'Random' , MPPF_PLUGIN_SLUG ).'</option>';
					$output .= '</select><br />';

					$output .= '<label>';
						$output .= '<input value="DESC" name="' . $this->get_field_name('order') . '" type="radio"' . checked($order, 'DESC',false) . ' /> ' . __('Descending', MPPF_PLUGIN_SLUG);
					$output .= '</label>';
					$output .= '<label style="margin-left: 5px;">';
						$output .= '<input value="ASC" name="' . $this->get_field_name('order') . '" type="radio"' . checked($order, 'ASC',false) . ' /> ' . __('Ascending', MPPF_PLUGIN_SLUG);
					$output .= '</label>';

				$output .= '</p>';

				// background color
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('listingbgcolor').'" style="line-height: 22px; vertical-align: top; margin-right: 5px;">'.__( 'Background Color:' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<input class="sa-input-color-picker" id="'.$this->get_field_id('listingbgcolor').'" name="'.$this->get_field_name('listingbgcolor').'" type="text" value="'.$listingbgcolor.'" />';
				$output .= '</p>';

				// text color
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('listingtextcolor').'" style="line-height: 22px; vertical-align: top; margin-right: 5px;">'.__( 'Text Color:' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<input class="sa-input-color-picker" id="'.$this->get_field_id('listingtextcolor').'" name="'.$this->get_field_name('listingtextcolor').'" type="text" value="'.$listingtextcolor.'" />';
				$output .= '</p>';

				// border color
				$output .= '<p>'; 
					$output .= '<label for="'.$this->get_field_id('listingbordercolor').'" style="line-height: 22px; vertical-align: top; margin-right: 5px;">'.__( 'Border Color:' , MPPF_PLUGIN_SLUG ).'</label>';
					$output .= '<input class="sa-input-color-picker" id="'.$this->get_field_id('listingbordercolor').'" name="'.$this->get_field_name('listingbordercolor').'" type="text" value="'.$listingbordercolor.'" />';
				$output .= '</p>';


			$output .= '</div>'; // end - section

			// show only in store pages
			$output .= '<div style="margin-bottom: 7px;">';
				$output .= '<input type="checkbox" class="checkbox" id="' . $this->get_field_id('only_store_pages') . '" name="' . $this->get_field_name('only_store_pages') . '"' . checked( $only_store_pages , true , false ). ' />';
				$output .= '<label for="' . $this->get_field_id('only_store_pages') . '" style="margin-left: 5px;">' . __( 'Only show on MarketPress related pages', MPPF_PLUGIN_SLUG ) . '</label>';
			$output .= '</div>';

			echo $output;
		}

	} // end - MPPF_Shop_Details_Widget