<?php
	class MPPremiumFeatures {

	    private $plugin_path;
	    private $plugin_url;
	    private $plugin_slug;

	    function __construct( $args = array() ) {

			$defaults = array(
				'file' => '',
				'plugin_path' => MPPF_PATH,
				'plugin_url' => MPPF_URL,
				'plugin_slug' => 'mppremiumfeatures',
				'hook_prefix' => 'mppremiumfeatures',
				'func_hook_prefix' => 'mppremiumfeatures_func_',
				'option_group' => 'mp_premium_features',
			);

			$instance = wp_parse_args( $args, $defaults );

	    	$this->plugin_unique_id = $instance['file'];
	        $this->plugin_path = $instance['plugin_path'];
	        $this->plugin_url = $instance['plugin_url'];
	        $this->plugin_slug = $instance['plugin_slug'];
	        $this->hook_prefix = $instance['hook_prefix'];
	        $this->func_hook_prefix = $instance['func_hook_prefix'];
	        $this->theme_usage = apply_filters( $this->hook_prefix . '_theme_usage' , false );
	        $this->option_group = $instance['option_group'];

			// Jeff Fixed Unable to scan text in wmpl

			$this->mpftext_adding_item = __('Adding Item','pro');
			$this->mpftext_item_added = __('Item Added!','pro');
			$this->mpftext_updatingdot = __('Updating...','pro');
			$this->mpftext_updating = __('Updating','pro');
			$this->mpftext_areyoursure = __('Are you sure you want to remove all items from your cart?','pro');
			$this->mpftext_loading = __('Loading...','pro');
			$this->mpftext_inyourcart = __('In Your Cart','pro');
			$this->mpftext_itemsadded = __('Item(s) Added!','pro');
			$this->mpftext_addmore = __('Add More','pro');
			$this->mpftext_outofstock = __('Out of Stock','pro');
			$this->mpftext_moredetails = __('More Details','pro');
			$this->mpftext_chooseoption = __('Choose Option','pro');
			$this->mpftext_addtocart = __('Add To Cart','pro');
			$this->mpftext_buynow = __('Buy Now','pro');
			$this->mpftext_choose = __('Choose','pro');
			$this->mpftext_itemleft = __(' items left!','pro');
			$this->mpftext_alreadyadded = __('Already added <span>%s</span> items to the cart','pro');

			// end

	        global $mp;

	        // define plugin slug for translation
	        define( 'MPPF_PLUGIN_SLUG', $this->plugin_slug );

	        // init main classes
	        add_action( 'after_setup_theme' , array(&$this, 'init') , 15 );

	        // register related CSS & JS
			add_action('wp_enqueue_scripts', array(&$this, 'register_css') , 10000 );
			add_action('wp_enqueue_scripts', array(&$this, 'register_js') , 999 );

			// Initialize the metabox class
			add_action( 'init', array(&$this, 'initialize_cmb_meta_boxes' ), 100 );

			// add settings page
			add_action( 'init', array(&$this, 'add_settings_page' ), 15 );

			// add js to wp footer
			add_action('wp_footer', array(&$this, 'add_js_to_footer') , 15 );

			// add main modal container to footer
			add_action('wp_footer', array(&$this, 'add_main_modal_container') , 15 );

			// add init hooks
			add_action( 'init' , array(&$this, 'init_hooks') , 15 );

			// default buy button ajax
    		add_action( 'wp_ajax_nopriv_mppf-update-cart', array(&$mp, 'update_cart') );
    		add_action( 'wp_ajax_mppf-update-cart', array(&$mp, 'update_cart') );

    		// load options menu ajax
    		add_action( 'wp_ajax_nopriv_mppf-load-snm', array(&$this, 'load_shop_now_menu') );
    		add_action( 'wp_ajax_mppf-load-snm', array(&$this, 'load_shop_now_menu') );

    		// update stock via ajax
    		add_action( 'wp_ajax_nopriv_mppf-stock-update', array(&$this, 'update_stock') );
    		add_action( 'wp_ajax_mppf-stock-update', array(&$this, 'update_stock') );

	    }

	    function init() {

	    	do_action( $this->hook_prefix . '_before_init_core_functions' );

	    	define( 'MPPF_INC_WISHLIST_FUNC' , apply_filters( $this->hook_prefix . '_inc_wishlist_class' , true ) );
	    	define( 'MPPF_INC_COMPAREPRODUCTS_FUNC' , apply_filters( $this->hook_prefix . '_inc_compare_products_class' , true ) );
	    	define( 'MPPF_INC_CROSSSELLING_FUNC' , apply_filters( $this->hook_prefix . '_inc_cross_selling_class' , true ) );
	    	define( 'MPPF_INC_IMAGETAXONOMY_FUNC' , apply_filters( $this->hook_prefix . '_inc_image_taxonomy_class' , true ) );
	    	define( 'MPPF_INC_SOCIALSHARING_FUNC' , apply_filters( $this->hook_prefix . '_inc_social_sharing_class' , true ) );
	    	define( 'MPPF_INC_GETTHISFREE_FUNC' , apply_filters( $this->hook_prefix . '_inc_get_this_free_class' , true ) );
	    	define( 'MPPF_INC_PURCHASEMOTIVATOR_FUNC' , apply_filters( $this->hook_prefix . '_inc_purchase_motivator_class' , true ) );
	    	define( 'MPPF_INC_SELLINPOST_FUNC' , apply_filters( $this->hook_prefix . '_inc_sell_in_post_class' , true ) );
	    	define( 'MPPF_INC_SHOPDETAILS_FUNC' , apply_filters( $this->hook_prefix . '_inc_shop_details_class' , true ) );

	        global $mppf_wishlist, $mppf_compare, $mppf_crossselling, $mppf_imagetaxonomy, $mppf_socialsharing, $mppf_getthisfree, $mppf_purchasemotivator, $mppf_sellinpost, $mppf_shopdetails;

	        $class_args = array(
					'file' => $this->plugin_unique_id,
					'plugin_path' => $this->plugin_path,
					'plugin_url' => $this->plugin_url,
					'plugin_slug' => $this->plugin_slug,
					'hook_prefix' => $this->hook_prefix,
	        	);

	        if ( true == MPPF_INC_WISHLIST_FUNC ) {
	        	require_once( $this->plugin_path .'classes/wishlist.php' );
	        	$mppf_wishlist = new MPPremiumFeatures_WishList( $class_args );
	        }

	        if ( true == MPPF_INC_COMPAREPRODUCTS_FUNC ) {
	        	require_once( $this->plugin_path .'classes/compare.php' );
	        	$mppf_compare = new MPPremiumFeatures_CompareProducts( $class_args );
	        }

	        if ( true == MPPF_INC_CROSSSELLING_FUNC ) {
	        	require_once( $this->plugin_path .'classes/cross-selling.php' );
	        	$mppf_crossselling = new MPPremiumFeatures_CrossSelling( $class_args );
	        }

	        if ( true == MPPF_INC_IMAGETAXONOMY_FUNC ) {
	        	require_once( $this->plugin_path .'classes/image-taxonomy.php' );
	        	$mppf_imagetaxonomy = new MPPremiumFeatures_ImageTaxomony( $class_args );
	        }

	        if ( true == MPPF_INC_SOCIALSHARING_FUNC ) {
	        	require_once( $this->plugin_path .'classes/social-sharing.php' );
	        	$mppf_socialsharing = new MPPremiumFeatures_SocialSharing( $class_args );
	        }

	        if ( true == MPPF_INC_GETTHISFREE_FUNC ) {
	        	require_once( $this->plugin_path .'classes/get-this-free.php' );
	        	$mppf_getthisfree = new MPPremiumFeatures_GetThisFree( $class_args );
	        }

	        if ( true == MPPF_INC_PURCHASEMOTIVATOR_FUNC ) {
	        	require_once( $this->plugin_path .'classes/purchase-motivator.php' );
	        	$mppf_purchasemotivator = new MPPremiumFeatures_PurchaseMotivator( $class_args );
	        }

	        if ( true == MPPF_INC_SELLINPOST_FUNC ) {
	        	require_once( $this->plugin_path .'classes/sell-in-post.php' );
	        	$mppf_sellinpost = new MPPremiumFeatures_SellInPost( $class_args );
	        }

	        if ( true == MPPF_INC_SELLINPOST_FUNC ) {
	        	require_once( $this->plugin_path .'classes/sell-in-post.php' );
	        	$mppf_sellinpost = new MPPremiumFeatures_SellInPost( $class_args );
	        }

	        /*
	         * Still under development
	        if ( true == MPPF_INC_SHOPDETAILS_FUNC ) {
	        	require_once( $this->plugin_path .'classes/shop-details.php' );
	        	$mppf_shopdetails = new MPPremiumFeatures_ShopDetails( $class_args );
	        }*/

	        do_action( $this->hook_prefix . '_init_core_functions' );

	    }

	    function init_hooks() {

	        define( 'MPPF_INC_MP_BUY_BTN_HOOK' , apply_filters( $this->hook_prefix . '_inc_mp_buy_btn_hook' , true ) );
	        define( 'MPPF_INC_FLEXMARKET_BUY_BTN_HOOK' , apply_filters( $this->hook_prefix . '_inc_flexmarket_buy_btn_hook' , true ) );
	        define( 'MPPF_INC_MPDG_BUY_BTN_HOOK' , apply_filters( $this->hook_prefix . '_inc_mpdg_buy_btn_hook' , true ) );
	        define( 'MPPF_INC_PRO_BUY_BTN_HOOK' , apply_filters( $this->hook_prefix . '_inc_pro_buy_btn_hook' , true ) );
	        define( 'MPPF_INC_GLOBAL_BTN_HOOK' , apply_filters( $this->hook_prefix . '_inc_global_btn_hook' , true ) );

			if ( true == MPPF_INC_MP_BUY_BTN_HOOK )
				add_filter( 'mp_buy_button_tag', array(&$this, 'buy_btn_hook_generic') , 99 , 3 ); // generic hook
			if ( true == MPPF_INC_FLEXMARKET_BUY_BTN_HOOK )
				add_filter( 'flexmarket_buy_button_tag', array(&$this, 'buy_btn_hook_flexmarket') , 99 , 2 ); // flexmarket specific hook
			if ( true == MPPF_INC_MPDG_BUY_BTN_HOOK )
				add_filter( 'mpdg_buy_button_tag', array(&$this, 'buy_btn_hook_mpdg') , 99 , 3 );
			if ( true == MPPF_INC_PRO_BUY_BTN_HOOK )
				add_filter( 'pro_buy_button_tag', array(&$this, 'buy_btn_hook_pro') , 99 , 2 );
			if ( true == MPPF_INC_GLOBAL_BTN_HOOK )
				add_filter( 'mpt_global_btn_hook', array(&$this, 'add_snm_to_global_btn') , 99 , 2 );
	    }

		/* Register related css
		------------------------------------------------------------------------------------------------------------------- */
		function register_css() {
			global $wp_query;
			wp_enqueue_style('css-animated-style', $this->plugin_url . 'css/animate.min.css', null, null);
			wp_enqueue_style('font-awesome-4-css', $this->plugin_url . 'css/font-awesome.min.css', null, '4.0.3' );
			
			if ($wp_query->is_single && $wp_query->query_vars['post_type'] == 'product') {
				wp_enqueue_style('magnific-popup', $this->plugin_url . 'css/magnific-popup.css', null, null);
			}
			
			//wp_enqueue_style('powertip-css', $this->plugin_url . 'css/powertip/jquery.powertip.min.css', null, null);
			wp_enqueue_style( 'bxslider-css', $this->plugin_url . 'css/jquery.bxslider.css', null, '4.1.1' );
			//wp_enqueue_style('mppf-css', $this->plugin_url . 'css/mppf.css', null, null);
			//wp_enqueue_style('mppf-responsive-css', $this->plugin_url . 'css/mppf-responsive.css', null, null);
		}

		/* Register related js
		------------------------------------------------------------------------------------------------------------------- */
		function register_js() {
			global $wp_query;
			
			wp_enqueue_script('cssanimated-hover-js', $this->plugin_url . 'js/css-animated-hover.js', array('jquery'));
			//wp_enqueue_script('powertip-js', $this->plugin_url . 'js/jquery.powertip.min.js', array('jquery'));
			wp_enqueue_script('bxsliderjs', $this->plugin_url . 'js/jquery.bxslider.min.js', array('jquery'),'',true);
			if ($wp_query->is_single && $wp_query->query_vars['post_type'] == 'product') {
				wp_enqueue_script('magnific-popup', $this->plugin_url . 'js/jquery.magnific-popup.min.js', array('jquery'),'',true);
			}
			wp_enqueue_script('mppf-js', $this->plugin_url . 'js/mppf.js', array('jquery'),'',true);
			wp_localize_script( 'mppf-js', 'MPPF_Ajax', array(
				'ajaxUrl' => admin_url( 'admin-ajax.php', ( is_ssl() ? 'https': 'http') ),
				'addingItem' => apply_filters( $this->hook_prefix . '_ajax_adding_item' , '<i class="fa fa-spinner fa-spin"></i><span class="mppf-btn-text mppf-show">' . $this->mpftext_adding_item . '</span>' ),
				'WLSuccess' => apply_filters( $this->hook_prefix . '_ajax_wl_item_added' , '<span class="mppf-btn-text mppf-show">' . $this->mpftext_item_added . '</span>' ),
				'UpdatingTxt' => apply_filters( $this->hook_prefix . '_ajax_updating_txt' , $this->mpftext_updatingdot ),
				'UpdatingTxtIcon' => apply_filters( $this->hook_prefix . '_ajax_updating_txt_with_icon' , '<i class="fa fa-spinner fa-spin"></i>' . $this->mpftext_updatingdot ),
				'UpdatingBtn' => apply_filters( $this->hook_prefix . '_ajax_updating_btn' , '<i class="fa fa-spinner fa-spin"></i><span class="mppf-btn-text mppf-show">' . $this->mpftext_updating . '</span>' ),
				'CPtimeout' => apply_filters( $this->hook_prefix . '_ajax_cp_timeout' , 2500 ),
				'emptyCartMsg' => $this->mpftext_areyoursure,
				'LoadingIcon' => apply_filters( $this->hook_prefix . '_ajax_loading_icon' , '<i class="fa fa-spinner fa-spin mppf-loading-icon"></i>' ),
				'LoadingBtn' => apply_filters( $this->hook_prefix . '_ajax_loading_btn' , '<i class="fa fa-spinner fa-spin"></i><span class="mppf-btn-text mppf-show">' . $this->mpftext_loading . '</span>' ),
				'noStockMsg' => apply_filters( $this->hook_prefix . '_ajax_no_stock_msg' , '<span class="mppf-btn-text mppf-show">' . $this->mpftext_inyourcart . '</span>' ),
				'bookingConfirm' => apply_filters( $this->hook_prefix . '_ajax_booking_confirmed' , '<span class="mppf-btn-text mppf-show">'.__('Booking Confirmed',$this->plugin_slug).'</span>' ),
				'bookingNot' => apply_filters( $this->hook_prefix . '_ajax_booking_confirmed' , '<span class="mppf-btn-text mppf-show">'.__('Booking Not Available',$this->plugin_slug).'</span>' ),				
				'cartSuccess' => apply_filters( $this->hook_prefix . '_ajax_item_added_to_cart' , '<span class="mppf-btn-text mppf-show">' . $this->mpftext_itemsadded  . '</span>' ),
				'Carttimeout' => apply_filters( $this->hook_prefix . '_ajax_cart_timeout' , 1000 ),
				'ModalLoading' => apply_filters( $this->hook_prefix . '_ajax_modal_loading' , '<div class="mpt-modal-loading-mode"><i class="fa fa-spinner fa-spin"></i></div>' ),
				'SNMenabled' => $this->load_option( $this->option_group . '_productlisting_enable_shopnow' , 'yes' ),
				'checkoutlink' => home_url('/store/shopping-cart/checkout/'),
				'addingBooking' => apply_filters( $this->hook_prefix . '_ajax_adding_booking' , '<i class="fa fa-spinner fa-spin"></i><span class="mppf-btn-text mppf-show"> ' .__('Confirming..',$this->plugin_slug) . '</span>' ),
				'bookingadded' => apply_filters( $this->hook_prefix . '_ajax_adding_booking' , '<span class="mppf-btn-text mppf-show"> ' .__('Booking is added in your cart.',$this->plugin_slug) . '</span>' ),	
			));
		}

		/* Settings
		------------------------------------------------------------------------------------------------------------------- */

		function add_settings_page() {

			define( 'MPPF_INIT_SETTINGS_PAGE' , apply_filters( $this->hook_prefix . '_init_settings_page' , true ) );
			if ( true == MPPF_INIT_SETTINGS_PAGE ) {

				require_once( $this->plugin_path . 'inc/settings/options-class.php' );
				// add main menu page
				$main_menu_page = new SAOptionsClass( array(
						'option_group' => $this->option_group,
						'plugin_path' => $this->plugin_path,
						'plugin_url' => $this->plugin_url,
						'plugin_slug' => $this->plugin_slug,
						'settings_hook' => $this->hook_prefix . '_menu_page_',
						'settings_file' => $this->plugin_path . 'inc/settings/main-page-settings.php',
						'page_name' => __( 'MP Premium Features', $this->plugin_slug ),
						'is_menu_page' => true,
						'menu_page_name' => __( 'MP Premium Features', $this->plugin_slug ),
						'menu_page_slug' => 'mppf_menu_page',
						'menu_page_access' => 'manage_options',
					) );

			}

		}

	    function load_option( $option = NULL , $default = NULL ) {

	    	$instance = array(
	    		'option' => $option,
	    		'default' => $default,
	    		'method' => $this->theme_usage,
	    	);

	    	if ( NULL == $option ) {} else {

		    	if ( $this->theme_usage ) {
		    		$value = get_blog_option(1, $option );
		    	} else {
	    			$option_group = get_blog_option(1, $this->option_group . '_settings' );
	    			if ( !empty($option_group) && is_array($option_group) )
	    				$value = ( !empty($option_group[$option]) ? $option_group[$option] : NULL );
		    	}

	    	}

	    	$value = ( !empty($value) ? $value : $default );

	    	return apply_filters( $this->func_hook_prefix . 'load_option' , $value , $instance );
	    }

		/* Initialize CMB class
		------------------------------------------------------------------------------------------------------------------- */

		function initialize_cmb_meta_boxes() {
			if ( !class_exists( 'cmb_Meta_Box' ) )
				require_once( $this->plugin_path . 'inc/metabox/init.php' );
		}

		/* Add js to footer
		------------------------------------------------------------------------------------------------------------------- */
		function add_js_to_footer() {
			global $wp_query;
		
			$showinventory = $this->load_option( $this->option_group . '_general_show_inventory' , 'yes' );

			if ($wp_query->is_single && $wp_query->query_vars['post_type'] == 'product') {
				
				$output = '<script type="text/javascript">
							jQuery(document).ready(function () {
								jQuery(".mppf-modal-trigger").magnificPopup({
										  type: "inline",
										  fixedContentPos: false,
										  fixedBgPos: true,
										  overflowY: "auto",
										  closeBtnInside: true,
										  preloader: false, 
										  midClick: true,
										  removalDelay: 300,
										  mainClass: "mppf-mfp-zoom-in"
										});
							});
						</script>';

				echo apply_filters( $this->func_hook_prefix . 'add_js_to_footer' , $output );
			}
		}


		/* Add modal container to footer
		------------------------------------------------------------------------------------------------------------------- */

		function add_main_modal_container() {
			global $wp_query;
			
			if ($wp_query->is_single && $wp_query->query_vars['post_type'] == 'product') {
				
				if( !defined( 'MPT_MAIN_MODAL_CONTAINER' ) ) {
					define( 'MPT_MAIN_MODAL_CONTAINER' , 'enabled' );
					echo apply_filters( $this->func_hook_prefix . 'add_main_modal_container' , '<div id="mpt-main-modal" class="mpt-modal zoom-anim-dialog mfp-hide"></div>' );
				}
			}

		}

		/* Single Page hook
		------------------------------------------------------------------------------------------------------------------- */

	    function buy_btn_hook_generic( $output = '' , $post_id = NULL , $context = 'list' ) {

			$current_theme = wp_get_theme();
			switch ($current_theme->Template) {
				case 'framemarket':
					$containerclass = ' mppf-framemarket-integration';
					break;
				case 'twentyfourteen':
					$containerclass = ' mppf-twentyfourteen-integration';
					break;
				case 'twentythirteen':
					$containerclass = ' mppf-twentythirteen-integration';
					break;
				case 'twentytwelve':
					$containerclass = ' mppf-twentytwelve-integration';
					break;
				default:
					$containerclass = '';
					break;
			}

	    	$instance = array(
				'integration' => 'none',
				'post_id' => $post_id,
				'context' => $context,
				'containerclass' => $containerclass,
				'btnclass' => $this->get_btn_color(),
	    		);

	    	return apply_filters( $this->func_hook_prefix . 'buy_btn_hook_generic' , $this->load_buy_options( $instance ) , $instance );
	    }

	    function buy_btn_hook_flexmarket( $output = '' , $instance = array() ) {

	    	$instance['integration'] = 'flexmarket';
	    	$instance['containerclass'] = ' mppf-flexmarket-integration';
	    	$instance['btnclass'] = $this->get_btn_color();

	    	return apply_filters( $this->func_hook_prefix . 'buy_btn_hook_flexmarket' , $this->load_buy_options( $instance ) , $instance );
	    }

	    function buy_btn_hook_mpdg( $output = '' , $post_id = NULL , $context = 'list' ) {

	    	$instance = array(
				'integration' => 'mpdg',
				'post_id' => $post_id,
				'context' => $context,
				'containerclass' => ' mppf-mpdg-integration mppf-container-block',
				'btnclass' => $this->get_btn_color(),
				'layout' => 'single',
				'displaysocialsharing' => false,
	    		);

	    	return apply_filters( $this->func_hook_prefix . 'buy_btn_hook_mpdg' , $this->load_buy_options( $instance ) , $instance );
	    }

	    function buy_btn_hook_pro( $output = '' , $args = array() ) {

	    	$instance = array(
				'integration' => 'pro',
				'post_id' => $args['post_id'],
				'context' => $args['context'],
				'containerclass' => ' mppf-pro-integration mppf-container-block',
				'btnclass' => $this->get_btn_color(),
				'layout' => 'quadruple',
	    		);

	    	return apply_filters( $this->func_hook_prefix . 'buy_btn_hook_pro' , $this->load_buy_options( $instance ) , $instance );
	    }

		/* Buy Button
		------------------------------------------------------------------------------------------------------------------- */
		//Jeff modification add text
		function new_add_cart_text() {
			return $this->mpftext_addmore;
		}
		//Jeff modification add text end
	    function load_buy_options( $args = array() ) {

			$defaults = array(
				'integration' => 'none',
				'post_id' => NULL,
				'blog_id' => NULL,
				'globalbtn' => false,
				'context' => 'single',
				'containerclass' => '',
				'btnclass' => '',
				'layout' => 'triple',
				'displaywishlist' => true,
				'displaycompare' => true,
				'displaysocialsharing' => true,
				'enableshopnow' => true,
				'snbtntext' => __( 'Shop Now' , $this->plugin_slug ),
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $id, $mppf_wishlist, $mppf_compare, $mppf_crossselling, $mppf_socialsharing, $mp, $blog_id;
			$post_id = ( NULL === $post_id ) ? $id : $post_id;

			if ( NULL === $blog_id )
				$blog_id = ( is_multisite() ? get_current_blog_id() : 1 );

			$all_out = $this->check_oos( $post_id );

			$enabledcs = $this->load_option( $this->option_group . '_crossselling_enable_cs' , 'no' );
			$enablewishlist = $this->load_option( $this->option_group . '_wishlist_enable_wishlist' , 'no' );
			$enablecompare = $this->load_option( $this->option_group . '_compareproducts_enable_compare' , 'no' );
			$enablesocialsharing = $this->load_option( $this->option_group . '_socialsharing_enable_socialsharing' , 'no' );
			$hidecompare = $this->load_option( $this->option_group . '_compareproducts_hide_compare_in_mobile' , 'yes' );

			//Jeff modification Cart Check
			$blog_id = (is_multisite()) ? $blog_id : 1;
			$current_blog_id = $blog_id;

			$global_cart = $mp->get_cart_contents(true);
			if ( ! $mp->global_cart ) {
				$selected_cart[$blog_id] = $global_cart[$blog_id];
			} else {
				$selected_cart = $global_cart;
			}
			$qty = '';
			$product_id = '';
			foreach ($selected_cart as $bid => $cart) {
				if ( is_multisite() ) {
					switch_to_blog($bid);
				}

				if ($cart) {
					foreach ($cart as $product_id => $variations) {
						foreach ($variations as $variation => $data) {
							if ($product_id == $post_id)
								$qty = $data['quantity'];
						}
					}
				}
			}

			if ( is_multisite() ) {
				switch_to_blog($current_blog_id);
			}

			if(!empty($qty))
				add_filter( $this->hook_prefix . '_add_cart_btn_text' ,array( $this, 'new_add_cart_text' ));
			//Jeff modification Cart Check END

	    	$buy_btn = $this->load_buy_button( array(
    						'post_id' => $post_id,
    						'product_page_link' => ( $globalbtn ? true : false ),
    						'context' => $context,
    						'integration' => $integration,
    						'showbtntext' => true,
    						'btnclass' => $btnclass,
    						'layout' => $layout,
							'addcartbtn' => ( $enabledcs == 'yes' ? 'mppf-btn-cs' : 'mppf-btn-addcart' ),
							'cartaction' => ( $enabledcs == 'yes' ? 'mppf-cs-update-cart' : 'mppf-update-cart' ),
	    				) );

	    	$wishlist_btn = ( $displaywishlist && $enablewishlist == 'yes' ? $mppf_wishlist->load_wishlist_btn( array(
		    						'post_id' => $post_id,
		    						'context' => $context,
		    						'integration' => $integration,
		    						'btnclass' => $btnclass,
		    					) ) : '' );

	    	$compare_btn = ( $displaycompare && $enablecompare == 'yes' ? $mppf_compare->load_product_compare_btn( array(
		    						'post_id' => $post_id,
		    						'context' => $context,
		    						'integration' => $integration,
		    						'btnclass' => $btnclass,
		    						'hiddenphone' => ( $hidecompare == 'yes' ? true : false )
		    					) ) : '' );

	    	$social_sharing = ( $displaysocialsharing && $enablesocialsharing == 'yes' ? ( $layout == 'single' ? '' : '<div class="clear"></div>' ) . $mppf_socialsharing->load_social_sharing( array(
					    			'post_id' => $post_id,
					    		) ) : '' );

			//Jeff modification Cart Check

			$items_in_cart = '<div id="pp-items-cart"'.(empty($qty)? ' style="display:none"' : '').'>'.sprintf( $this->mpftext_alreadyadded, $qty ).'</div><span id="cartAddMore" style="display:none">'.$this->mpftext_addmore.'</span>';

			//Jeff modification Cart Check End

	    	$output = '<div id="mp-premium-features"  class="mppf-container'.( $context == 'single' ? ' mppf-container-single' : ' mppf-container-list' ).$containerclass.'">';

		    	if ( $context == 'single' ) {
			    	$output .= $buy_btn . ( $layout == 'single' || $layout == 'double' ? '' : '' ) . $wishlist_btn . ( $layout == 'quadruple' ? '' : '' ) . $compare_btn . $items_in_cart.$social_sharing;
		    	} else {
		    		$shopnow = $this->load_option( $this->option_group . '_productlisting_enable_shopnow' , 'yes' );
		    		if ( $shopnow == 'yes' && $enableshopnow ) {
				    	$output .= '<button class="mppf-btn'.( $all_out ? ' mppf-btn-disabled mppf-oos-btn' : ' mppf-shop-now-btn' ).$btnclass.'" value="'.$post_id.'">';
				    		if ( $all_out ) {
			      				$output .= '<i class="fa fa-times-circle"></i>';
			      				$output .= '<span class="mppf-btn-text mppf-show">' . $this->mpftext_outofstock . '</span>';
				    		} else {
						    	$output .= '<span class="mppf-btn-text mppf-show mppf-btn-text-right">'.apply_filters( $this->hook_prefix . '_quick_shop_btn_text' , $snbtntext ).'</span>';
						    	$output .= '<i class="fa fa-arrow-circle-o-right"></i>';
				    		}
				    	$output .= '</button>';
				    	$output .= '<input type="hidden" id="snm-blog-id" name="current_blog_id" value="' . $blog_id . '" />';
				    	$output .= '<input type="hidden" id="snm-global-btn" name="global_btn" value="' . ( $globalbtn ? 'yes' : 'no' ) . '" />';
		    		} else {
		    			$output .= $buy_btn;
		    		}
	    		}

	    	$output .= '</div>'; // end - mp-premium-features

	    	return apply_filters( $this->func_hook_prefix . 'load_buy_options' , $output , $instance );
	    }

	    function load_shop_now_menu() {

	    	$product_id = ( isset( $_POST['product_id'] ) ? intval($_POST['product_id']) : NULL );
	    	$blog_id = ( isset( $_POST['blog_id'] ) ? intval($_POST['blog_id']) : NULL );
	    	$globalbtn = ( isset( $_POST['globalbtn'] ) ? esc_attr($_POST['globalbtn']) : 'no' );

			$args = array(
				'post_id' => $product_id,
				'blog_id' => ( !empty($blog_id) ? $blog_id : '' ),
				'context' => 'single',
				'globalbtn' => ( $globalbtn == 'yes' ? true : false ),
				'btnclass' => $this->get_btn_color(),
			);

	      	if (defined('DOING_AJAX') && DOING_AJAX) {
	        	echo apply_filters( $this->func_hook_prefix . 'load_shop_now_menu' , $this->shop_now_menu( $args ) , $args );
				exit;
	      	}
	    }

	    function shop_now_menu( $args = array() ) {

			$defaults = array(
				'post_id' => NULL,
				'blog_id' => NULL,
				'globalbtn' => false,
				'context' => 'single',
				'btnclass' => '',
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

			global $mppf_wishlist, $mppf_compare, $mppf_crossselling, $mp;

			$current_blog_id = get_current_blog_id();

			if ( is_multisite() && !empty($blog_id) && $globalbtn )
				switch_to_blog( $blog_id );

			$pinit_setting = $mp->get_setting('social->pinterest->show_pinit_button');
			$post_type = get_post_type( $post_id );
			$output = '<div id="mp-premium-features">';

			if ( NULL === $post_id || $post_type != 'product' ) {

				$output .= apply_filters( $this->hook_prefix . '_options_menu_error' , '<div class="mppf-alert-message">' . __( 'Failed to retrieve product details. Please try again.' , $this->plugin_slug ) . '</div>' , $instance );

			} else {

				$post = get_post( $post_id );
				$output .= '<div class="mppf-shop-now-menu">';
			    	$product_info = '<div class="mppf-snm-product-info">';
			    		$product_info .= ( function_exists('mp_product_image') ? '<div class="mppf-snm-product-image">' . mp_product_image( false , 'widget' , $post_id , 300 ) . '</div>' : '' );
			    		$product_info .= '<div class="mppf-snm-product-details">';
					    	$product_info .= apply_filters( $this->hook_prefix . '_shop_now_menu_product_title' , '<a href="'.get_permalink( $post_id ).'" class="mppf-snm-product-title-link"><h3 class="mppf-snm-product-title">' . get_the_title( $post_id ). '</h3></a>' , $instance );
					    	$product_info .= apply_filters( $this->hook_prefix . '_shop_now_menu_product_price' , '<div class="mppf-snm-product-price">' . ( function_exists('mp_product_price') ? mp_product_price( false , $post_id , false ) : '' ) . '</div>' , $instance );
					    	$moredetails = '<a href="'.get_permalink($post_id).'">'.$this->mpftext_moredetails.'</a>';
					    	$product_info .= apply_filters( $this->hook_prefix . '_shop_now_menu_product_desc' , '<div class="mppf-snm-product-desc">' . ( !empty($post->post_excerpt) ? $post->post_excerpt . $moredetails : ( !empty($post->post_content) ? wp_trim_words($post->post_content , apply_filters( $this->hook_prefix . '_snm_product_desc' , 20 ) ) . $moredetails : '' ) ) . '</div>' , $instance );
					    	$product_info .= apply_filters( $this->hook_prefix . '_shop_now_menu_pinit_btn' , ( function_exists('mp_pinit_button') && $pinit_setting != 'off' ? '<div class="mppf-pinterest-btn">' . mp_pinit_button( $post_id , 'single_view' ) . '</div>' : '' ) , $instance );
					    $product_info .= '</div>'; // end - mppf-snm-product-details
				    	$product_info .= '<div class="clear"></div>';
			    	$product_info .= '</div>'; // end - mppf-snm-product-info
			    	$output .= apply_filters( $this->hook_prefix . '_shop_now_menu_product_info' , $product_info , $instance );
			    	$output .= '<div class="mppf-shop-now-options">';
			    		$output .= $this->load_buy_options( array(
			    			'integration' => 'shopnowmenu',
							'post_id' => $post_id,
							'globalbtn' => $globalbtn,
							'layout' => ( $globalbtn ? 'single' : 'triple' ),
							'context' => 'single',
							'btnclass' => $btnclass,
			    		) );
			    	$output .= '</div>'; // end - mppf-shop-now-options
			    $output .= '</div>'; // end - mppf-quick-shop-menu

			}

			if ( is_multisite() && !empty($blog_id) && $globalbtn )
				restore_current_blog();

			$after_restore_blog_id = get_current_blog_id();
			if ( is_multisite() && $current_blog_id != $after_restore_blog_id )
				switch_to_blog( $current_blog_id );

	    	$output .= '</div>'; // end - mp-premium-features

	    	return apply_filters( $this->func_hook_prefix . 'shop_now_menu' , $output , $instance );
	    }

	    function load_buy_button( $args = array() ) {

			$defaults = array(
				'post_id' => NULL,
				'product_page_link' => false,
				'context' => 'single',
				'integration' => 'none',
				'showbtntext' => false,
				'showstock' => true,
				'btnclass' => '',
				'chooseoptionbtntext' => $this->mpftext_chooseoption,
				'addcartbtntext' => $this->mpftext_addtocart,
				'buynowbtntext' => $this->mpftext_buynow,
				'addcartbtn' => 'mppf-btn-addcart',
				'buynowbtn' => 'mppf-btn-buynow',
				'cartaction' => 'mppf-update-cart',
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

		  	global $id, $mp;
		  	$post_id = ( NULL === $post_id ) ? $id : $post_id;

		  	$btnclass .= ( $context == 'single' || $showbtntext ? '' : ' add-fadein-effects-parent' );
		  	$btn_text_class = ( $context == 'single' || $showbtntext ? ' mppf-show' : ' add-fadein-effects-child' );
		  	$btn_icon_class = ( $context == 'single' || $showbtntext ? '' : ' add-fadein-effects-child' );

		  	$chooseoptionbtntext = apply_filters( $this->hook_prefix . '_choose_options_btn_text' , $chooseoptionbtntext , $instance );
		  	$addcartbtntext = apply_filters( $this->hook_prefix . '_add_cart_btn_text' , $addcartbtntext , $instance );
		  	$buynowbtntext = apply_filters( $this->hook_prefix . '_buy_now_btn_text' , $buynowbtntext , $instance );

		  	$meta = get_post_custom($post_id);
		  	//unserialize
		  	if ( !empty($meta) ) {
			  	foreach ($meta as $key => $val) {
				  	$meta[$key] = maybe_unserialize($val[0]);
				  	if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file")
				    	$meta[$key] = array($meta[$key]);
				}
		  	}

		  	//check stock
		  	$no_inventory = array();
		  	$all_out = false;
		  	if ($meta['mp_track_inventory']) {

		    $cart = $mp->get_cart_contents();
		    if (isset($cart[$post_id]) && is_array($cart[$post_id])) {
			    	foreach ($cart[$post_id] as $variation => $data) {
				      if ($meta['mp_inventory'][$variation] <= $data['quantity'])
				        $no_inventory[] = $variation;
					}

					foreach ($meta['mp_inventory'] as $key => $stock) {
				      if (!in_array($key, $no_inventory) && $stock <= 0)
				        $no_inventory[] = $key;
					}
				}

				//find out of stock items that aren't in the cart
				foreach ($meta['mp_inventory'] as $key => $stock) {
		      	if (!in_array($key, $no_inventory) && $stock <= 0)
		        	$no_inventory[] = $key;
				}

				if (count($no_inventory) >= count($meta["mp_price"]))
				  $all_out = true;
		  	}
			$output_b = '';
		  	//display an external link or form button
		  	if (isset($meta['mp_product_link']) && $product_link = $meta['mp_product_link']) {
		    	$output = '<a class="mppf-btn'.$btnclass.'" href="' . esc_url($product_link) . '">';
		    		$output .= '<i class="fa fa-shopping-cart'.$btn_icon_class.'"></i>';
		    		$output .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $buynowbtntext . '</span>';
		    	$output .= '</a>';
		  	} else if ($mp->get_setting('disable_cart')) {
		    	$output = '';
		  	} else if ( $product_page_link ) {
		    	$output = '<a class="add-to-cart-compare mppf-global-listing-btn mppf-btn'.$btnclass.'" href="' . get_permalink( $post_id ) . '" data="'.$post_id.'">';
		    		$output .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $addcartbtntext . '</span>';
		    	$output .= '</a>';
		  	}else {

			    $variation_select = '';
			    $output = '<form class="mppf_buy_form mppf-options-form" method="post" action="' . mp_cart_link( false, true ) . '">';

			    if ($all_out) {
			      	$output .= '<a class="mppf-btn mppf-oos-btn mppf-btn-disabled'.$btnclass.'">';
			      		$output .= '<i class="fa fa-times-circle"></i>';
			      		$output .= '<span class="mppf-btn-text mppf-show">' . $this->mpftext_outofstock . '</span>';
			      	$output .= '</a>';
			    } else {

				    $output .= '<input type="hidden" name="product_id" value="' . $post_id . '" />';

						//create select list if more than one variation
					    if (is_array($meta["mp_price"]) && count($meta["mp_price"]) > 1 && empty($meta["mp_file"])) {

					    	$variation_select = '<div class="mppf-input-box mppf-single">';

					    	$variation_select .= '<span class="mppf-variations-label mppf-input-label">'.$this->mpftext_choose.'</span>';

				      		$variation_select .= '<select class="mppf-variations mppf-select'.( $showstock ? ' mppf-variations-listener' : '' ).'" name="variation">';

							foreach ($meta["mp_price"] as $key => $value) {

							  $disabled = (in_array($key, $no_inventory)) ? ' disabled="disabled"' : '';
							  $variation_select .= '<option data-options="parse_brackets" value="' . $key . '"' . $disabled . '>' . esc_html($meta["mp_var_name"][$key]) . ' - ';

							  if ($meta["mp_is_sale"] && $meta["mp_sale_price"][$key]) {

					        		$variation_select .= '[s style="color:#999"]' .
											$mp->format_currency('', $meta["mp_price"][$key]) .
											'[/s] - [b]' . $mp->format_currency('', $meta["mp_sale_price"][$key]) . '[/b]';

					     	 	} else {

					        		$variation_select .= $mp->format_currency('', $value);

					      		}

					      		$variation_select .= "</option>";

					    	}

				      		$variation_select .= "</select>";

				      		$variation_select .= '</div>'; // End input-prepend

				 		} else {
				 			// No variation
					      	$output .= '<input type="hidden" name="variation" value="0" />';
						}

						if ( $context == 'list' ) {

					      	if ($variation_select) {
				        		$output .= '<a href="' . get_permalink($post_id) . '" class="mppf-btn'.$btnclass.' mppf-choose-options-btn">';
				        			$output .= '<i class="fa fa-shopping-cart'.$btn_icon_class.'"></i>';
				        			$output .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $chooseoptionbtntext . '</span>';
				        		$output .= '</a>';
					      	} else if ($mp->get_setting('list_button_type') == 'addcart') {
					        	$output .= '<input type="hidden" name="action" value="'.$cartaction.'" />';
					        	$output .= '<button class="'.$addcartbtn.' mppf-btn'.$btnclass.' mppf-add-to-cart-btn" type="submit" name="addcart">';
					        		$output .= '<i class="fa fa-shopping-cart'.$btn_icon_class.'"></i>';
					        		$output .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $addcartbtntext . '</span>';
					        	$output .= '</button>';
								
					        	$output_b .= '<button class="'.$addcartbtn.' mppf-btn'.$btnclass.' mppf-add-to-cart-btn" type="submit" name="addcart">';
					        		$output_b .= '<i class="fa fa-shopping-cart'.$btn_icon_class.'"></i>';
					        		$output_b .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $addcartbtntext . '</span>';
					        	$output_b .= '</button>';								
								
					      	} else if ($mp->get_setting('list_button_type') == 'buynow') {
					        	$output .= '<button class="'.$buynowbtn.' mppf-btn'.$btnclass.' mppf-buy-now-btn" type="submit" name="buynow">';
					        		$output .= '<i class="fa fa-shopping-cart'.$btn_icon_class.'"></i>';
					        		$output .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $buynowbtntext . '</span>';
					        	$output .= '</button>';
								
					        	$output_b .= '<button class="'.$buynowbtn.' mppf-btn'.$btnclass.' mppf-buy-now-btn" type="submit" name="buynow">';
					        		$output_b .= '<i class="fa fa-shopping-cart'.$btn_icon_class.'"></i>';
					        		$output_b .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $buynowbtntext . '</span>';
					        	$output_b .= '</button>';								
								
					      	}

						} else {

					      	$output .= $variation_select;
					      	$output .= ( $layout == 'quadruple' ? '<div class="clear"></div>' : '' );

						    //add quantity field if not downloadable
						    $display = '';
						    if ( $mp->get_setting('show_quantity') && empty($meta["mp_file"]) ) {
						    	$display = ' style="display:none"';
						    }

					      	$output .= '<div class="mppf-input-box mppf-single row" style="margin: 0 -25px;" '. $display.'>';
					        	$output .= ' <div class="col-md-6 col-sm-12 mppf-product-quantity-container"><span class="mppf-product-quantity mppf-input-label">' . __('Quantity', $this->plugin_slug ) . '</span>';
					        	if ( $showstock )
					        		$output .= '<input class="mppf-product-quantity-field mppf-input mppf-powertip-trigger" data-powertip="'.$this->check_stock( array( 'post_id' => $post_id , 'variation' => 0 )).'" type="text" name="quantity" value="1" /></div>';
					        	else
					        		$output .= '<input class="mppf-product-quantity-field mppf-input" type="text" name="quantity" value="1" /></div>';
					       	$output .= '<div class="col-md-6 col-sm-12 mppf-product-stock-container">';
					       	$output .= '<span class="mppf-product-stock mppf-input-label pull-right" style="color: red !important;">'.$this->check_stock( array( 'post_id' => $post_id , 'variation' => 0 )).'</span>';
					       	$output .= '</div>';
					       	$output .= '</div>';

						    $output .= ( $layout == 'quadruple' ? '<div class="clear"></div>' : '' );

							 	
						    if ( $mp->get_setting('product_button_type') == 'buynow' ) {
						        $output .= '<button class="'.$buynowbtn.' mppf-btn'.$btnclass.' mppf-buy-now-btn" type="submit" name="buynow">';
						        	//$output .= '<i class="fa fa-shopping-cart'.$btn_icon_class.'"></i>';
						        	$output .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $buynowbtntext . '</span>';
						        $output .= '</button>';
								
						        $output_b .= '<button class="'.$buynowbtn.' mppf-btn'.$btnclass.' mppf-buy-now-btn" type="submit" name="buynow">';
						        	//$output_b .= '<i class="fa fa-shopping-cart'.$btn_icon_class.'"></i>';
						        	$output_b .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $buynowbtntext . '</span>';
						        $output_b .= '</button>';								
								
						    } else {
									$output .= '<input type="hidden" name="action" value="'.$cartaction.'" />';
									$output .= '<button id="product_page_addtocart_live" class="'.$addcartbtn.' mppf-btn'.$btnclass.' mppf-add-to-cart-btn" type="submit" name="addcart" style="display:none">';
										//$output .= '<i class="fa fa-shopping-cart'.$btn_icon_class.'"></i>';
										$output .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $addcartbtntext . '</span>';
									$output .= '</button>';
									

									$output_b .= '<div class="col-lg-4 col-md-12 col-sm-4 col-xm-12 padding-sm"><button id="product_page_addtocart" class="'.$addcartbtn.' mppf-btn'.$btnclass.' mppf-add-to-cart-btn" type="submit" name="addcart">';
										//$output_b .= '<i class="fa fa-shopping-cart'.$btn_icon_class.'"></i>';
										$output_b .= '<span class="mppf-btn-text'.$btn_text_class.'">' . $addcartbtntext . '</span>';
									$output_b .= '</button></div>';									
									
						    }
							
							

						}
			    }
		    	$output .= '</form><div style="clear:both;margin-bottom:15px;"></div><div class="row-fluid">'.$output_b;

		    }
			
	    	return apply_filters( $this->func_hook_prefix . 'load_buy_button' , $output , $instance );
	    }

	    function add_snm_to_global_btn( $contents = '' , $args = array() ) {

			$defaults = array(
				'integration' => '',
				'post_id' => NULL,
				'context' => 'list',
				'containerclass' => '',
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

		  	global $id, $mp;
		  	$post_id = ( NULL === $post_id ) ? $id : $post_id;

		  	$btn_args = array(
		  		'integration' => $integration,
		  		'post_id' => $post_id,
		  		'context' => $context,
		  		'containerclass' => $containerclass,
		  		'globalbtn' => true,
		  		'btnclass' => $this->get_btn_color()
		  	);

	    	return apply_filters( $this->func_hook_prefix . 'add_snm_to_global_btn' , $this->load_buy_options( $btn_args ) , $instance );
	    }

	    function check_stock( $args = array() ) {

			$defaults = array(
				'post_id' => NULL,
				'variation' => 0,
			);

			$instance = wp_parse_args( $args, $defaults );
			extract( $instance );

		  	global $id, $mp;
		  	$post_id = ( NULL === $post_id ) ? $id : $post_id;

		  	$cart = $mp->get_cart_cookie();
		  	$track_inventory = get_post_meta($post_id, 'mp_track_inventory', true);
		  	$current_stock = 0;

		    if ( $track_inventory ) {

		        $stock = maybe_unserialize( get_post_meta( $post_id, 'mp_inventory', true ) );

		        if (!is_array($stock))
					$stock[0] = $stock;

				$item_in_cart = false;
				foreach ($cart as $pid => $pv) {
					if ( $pid == $post_id ) {
						foreach ($pv as $key => $quantity) {
							if ( $key == $variation )
								$item_in_cart = true;
						}
					}
				}

				if ( $item_in_cart )
	            	$current_stock = number_format_i18n( $stock[$variation]-$cart[$post_id][$variation] );
	            else
	            	$current_stock = number_format_i18n( $stock[$variation] );
		    }

		    $output = ( $current_stock > 0 && $track_inventory ?
		    	apply_filters( $this->hook_prefix . '_show_stock_text' , ( $current_stock > 1 ? $current_stock . $this->mpftext_itemleft : $current_stock . $this->mpftext_itemleft ) , $current_stock )
 		    	: '' );

	    	return apply_filters( $this->func_hook_prefix . 'check_stock' , $output , $instance );
	    }

	    function check_oos( $post_id = NULL ) {

			global $id, $mp;
			$post_id = ( NULL === $post_id ) ? $id : $post_id;

			$meta = get_post_custom($post_id);
			//unserialize
			foreach ($meta as $key => $val) {
					$meta[$key] = maybe_unserialize($val[0]);
					if (!is_array($meta[$key]) && $key != "mp_is_sale" && $key != "mp_track_inventory" && $key != "mp_product_link" && $key != "mp_file")
							$meta[$key] = array($meta[$key]);
			}

			$no_inventory = array();
			$all_out = false;
			if ( $meta['mp_track_inventory'] ) {
					$cart = $mp->get_cart_contents();
					if (isset($cart[$post_id]) && is_array($cart[$post_id])) {
							foreach ($cart[$post_id] as $variation => $data) {
									if ($meta['mp_inventory'][$variation] <= $data['quantity'])
											$no_inventory[] = $variation;
							}
							foreach ($meta['mp_inventory'] as $key => $stock) {
									if (!in_array($key, $no_inventory) && $stock <= 0)
											$no_inventory[] = $key;
							}
					}

					//find out of stock items that aren't in the cart
					if ( !empty($meta['mp_inventory']) ) {
						foreach ($meta['mp_inventory'] as $key => $stock) {
								if (!in_array($key, $no_inventory) && $stock <= 0)
										$no_inventory[] = $key;
						}
					}

					if (count($no_inventory) >= count($meta["mp_price"]))
							$all_out = true;
			}

	    	return apply_filters( $this->func_hook_prefix . 'check_oos' , $all_out , $post_id );
	    }

	    function update_stock() {

		    $product_id = apply_filters('mp_product_id_add_to_cart', intval($_POST['product_id']));
		    $variation = (isset($_POST['variation'])) ? intval(abs($_POST['variation'])) : 0;

		    $args = array( 'post_id' => $product_id , 'variation' => $variation );

          	if (defined('DOING_AJAX') && DOING_AJAX) {
            	echo apply_filters( $this->func_hook_prefix . 'update_stock' , $this->check_stock( $args ) , $args );
            	exit;
          	}
	    }

		/* Retrieve Settings
		------------------------------------------------------------------------------------------------------------------- */

		function get_btn_color( $option = NULL ) {

			if ( NULL == $option )
				$btncolor = $this->load_option( $this->option_group . '_general_btn_color' , 'black' );
			else
				$btncolor = $this->load_option( $option );

			switch ($btncolor) {
				case 'black':
				default:
					$color = ' mppf-btn-black';
					break;
				case 'blue':
					$color = ' mppf-btn-blue';
					break;
				case 'lightblue':
					$color = ' mppf-btn-lightblue';
					break;
				case 'green':
					$color = ' mppf-btn-green';
					break;
				case 'yellow':
					$color = ' mppf-btn-yellow';
					break;
				case 'red':
					$color = ' mppf-btn-red';
					break;
				case 'grey':
					$color = '';
					break;
			}

			return apply_filters( $this->func_hook_prefix . 'get_btn_color' , $color , $option );
		}

		/* Kses it
		------------------------------------------------------------------------------------------------------------------- */

	    function kses_it( $text_before ) {

			$text_after = wp_kses( $text_before, array(
							'a' => array(
								'href' => array(),
								'title' => array(),
								'target' => array(),
								'class' => array(),
								'id' => array()
								),
							'br' => array(),
							'em' => array(),
							'strong' => array(),
							'span' => array(
									'class' => array()
								)
						) );

	    	return apply_filters( $this->func_hook_prefix . 'kses_it' , $text_after , $text_before );
	    }

	} // end - class MPPremiumFeatures