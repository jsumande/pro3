<?php
/* Registered Sidebars Blocks */
class AQ_Widgets_Block extends AQ_Block {
	
	function __construct() {
		$block_options = array(
			'name' => __('Widgets','pro'),
			'size' => 'span4',
		);
		
		parent::__construct('AQ_Widgets_Block', $block_options);
	}
	
	function form($instance) {
		
		
		//get all registered sidebars
		global $wp_registered_sidebars;
		$sidebar_options = array(); $default_sidebar = '';
		foreach ($wp_registered_sidebars as $registered_sidebar) {
			$default_sidebar = empty($default_sidebar) ? $registered_sidebar['id'] : $default_sidebar;
			$sidebar_options[$registered_sidebar['id']] = $registered_sidebar['name'];
		}
		
		$defaults = array(
			'sidebar' => $default_sidebar,
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);

		do_action( $id_base . '_before_form' , $instance );
		
		?>
		<div class="description">
			<label for="<?php echo $block_id ?>_title">
				<?php _e('Title (optional)','pro'); ?><br/>
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</div>
		
		<div class="cf"></div>

		<div class="description">
			<label for="">
				<?php _e('Choose widget area','pro'); ?><br/>
				<?php echo aq_field_select('sidebar', $block_id, $sidebar_options, $sidebar); ?>
			</label>
		</div>
		<?php

		do_action( $id_base . '_after_form' , $instance );
	}
	
	function block($instance) {
		extract($instance);

	?>
		<div id="sidebar">
			<?php dynamic_sidebar($sidebar); ?>
		</div>
	<?php

	}
	
}