<?php get_header();

	//$display_sidebar = get_option('mpt_pro_page_display_sidebar');
	//$sidebar_position = get_option('mpt_pro_page_sidebar_position');
	$display_sidebar = get_blog_option(1,'mpt_pro_page_display_sidebar');
	$sidebar_position = get_blog_option(1,'mpt_pro_page_sidebar_position');
	$selected_sidebar = mpt_load_selected_sidebar('product');

if (have_posts()) : while (have_posts()) : the_post(); 

	$meta_prefix = '_mpt_';
	$contentbgcolor = esc_attr(get_post_meta( $post->ID , $meta_prefix . 'post_content_bg_color', true ));
	$contenttextcolor = esc_attr(get_post_meta( $post->ID , $meta_prefix . 'post_content_text_color', true ));
	$loadtemplate = mpt_get_post_meta( $post->ID, $meta_prefix . 'product_page_layout', 'style-1' , true );

?>
		<?php if(is_main_site()) { ?>
			<?php 
			$new_l = $_SERVER["REQUEST_URI"];
			if(strstr($new_l,'/store/products/category/')) {  } else { ?>			
		<div id="breadcrumb" class="subheader-bc">
			<div class="outercontainer">
				<div class="container">
					<div class="row no-margin">
							<div class="col-md-12 col-sm-12 col-xs-12">
					 	<ul class="breadcrumbs-list">						
					 		<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home','trtm'); ?></a></li>
						<?php 						
							$post_id = get_the_ID();
							if ( get_post_type() == 'product' && !empty($post_id)) {
								if ( $terms = get_the_terms( $post_id, 'product_category' ) ) {
									$referer = wp_get_referer();
									$i = 1;
									foreach( $terms as $term) {
										$referer_slug = (strpos($referer, $term->slug));
										if(!$referer_slug == false) {
											$category_name = $term->name;
											$ancestors = get_ancestors( $term->term_id, 'product_category' );
											$ancestors = array_reverse( $ancestors );
											foreach ( $ancestors as $ancestor ) {
												$ancestor = get_term( $ancestor, 'product_category' );
												if ( ! is_wp_error( $ancestor ) && $ancestor ) {
													echo '<li><a href="' . get_term_link( $ancestor->slug, 'product_category' ) . '">' . $ancestor->name . '</a></li>';
												}
											}
												echo '<li><a href="' . get_term_link( $term->slug, 'product_category' ) . '">' . $category_name . '</a></li>';
										} else {
											$category_name = $term->name;
											$ancestors = get_ancestors( $term->term_id, 'product_category' );
											$ancestors = array_reverse( $ancestors );
											foreach ( $ancestors as $ancestor ) {
												$ancestor = get_term( $ancestor, 'product_category' );
												if ( ! is_wp_error( $ancestor ) && $ancestor ) {
													echo '<li><a href="' . get_term_link( $ancestor->slug, 'product_category' ) . '">' . $ancestor->name . '</a></li>';
												}
											}
												echo '<li><a href="' . get_term_link( $term->slug, 'product_category' ) . '">' . $category_name . '</a></li>';
											if ($i++ == 1) break;		
										}	
									}
								}
							}
						?>							
							
							<?php 
								$breadcrumb_title  = '';
								 if ( get_query_var('pagename') == 'performers'  && ICL_LANGUAGE_CODE =='bg' ) {
								    $breadcrumb_title =  __( "Изпълнители" , 'pro' );
								 } else {
								     $breadcrumb_title =  __( the_title() , 'pro' );
								 }
							?>
							<li class="active"><?php echo $breadcrumb_title;?></li>
					 	</ul>
							
					 </div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>			
		<?php } ?>		
	<!-- Post -->
	<div id="product-single-wrapper" class="<?php if($_GET['lang'] == 'bg'){echo 'bg';}?>">

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">
					<div class="row no-margin">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="row-fluid">

								<?php if ( empty($sidebar_position) || ( $display_sidebar == 'true' && $sidebar_position == 'Left' ) ) : ?>
									<div id="sidebar" class="col-md-3 col-sm-3 col-xs-12 sidebar-shopping-card-wrapper">
										
										<?php the_widget('TP_Featured_Widgets','title='.__('Featured Stores','pro'),'before_widget=<div class="well sidebar-widget-well">&before_title=<h4 class="sidebar-widget-title">&after_title=</h4>&after_widget=</div>'); ?>
										<?php // the_widget('TP_Featured_Products','title='.__('Featured Products','pro'),'before_widget=<div class="well sidebar-widget-well">&before_title=<h4 class="sidebar-widget-title">&after_title=</h4>&after_widget=</div>'); ?>
										<?php //get_sidebar( $selected_sidebar ); ?>
										<?php //echo '<div class="well">' . pro_load_global_product_taxonomy_menu( array( 'echo' => false , 'termslug' => '' , 'taxonomy' => 'product_category' , 'count' => '0' ) ) . '</div>'; ?>
										
										<?php // the_widget('PRO_Shopping_Cart','title='.__('Shopping Cart','pro'),'before_widget=<div class="well sidebar-widget-well">&before_title=<h4 class="sidebar-widget-title">&after_title=</h4>&after_widget=</div>'); ?> 
										<?php //get_sidebar(); ?>
									</div>
								<?php endif; ?>

								<div class="<?php echo ( $display_sidebar != 'false' ? 'col-md-9 col-sm-9 col-xs-12' : 'col-md-12' ); ?>">

									<div id="post-<?php the_ID(); ?>" <?php post_class( 'product-page-' . $loadtemplate ); ?><?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>

										<?php
											switch ($loadtemplate) {
												case 'style-2':
													get_template_part( 'templates/product' , 'style2' );
													break;
												case 'style-1':
													get_template_part( 'templates/product' , 'style1' );
													break;
												default:
													do_action( 'fws_product_page_load_template_default_hook' , $post->ID );
													break;
											}
										?>

									</div> <!-- End product -->

								</div><!-- / span8 -->

								<?php if ( !empty($sidebar_position) && $display_sidebar == 'true' && $sidebar_position == 'Right' ) : ?>
									<div id="sidebar" class="span3">
										<?php //get_sidebar( $selected_sidebar ); ?>
									</div>
								<?php endif; ?>
							</div><!-- / row-fluid -->
						</div><!-- / col-md-s -->
					</div><!-- / row no-margin -->

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / product-single-wrapper -->

	<span style="display:none" class="date updated"><?php echo get_the_time( 'Y-m-d\TG:i' , $post->ID ); ?></span>
	<span style="display:none" class="vcard author"><span class="fn"><?php echo get_the_author_meta( 'display_name' , $post->post_author ); ?></span></span>

<?php endwhile; endif;

get_template_part('footer', 'widget');

get_footer(); ?>