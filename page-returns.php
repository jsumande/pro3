<?php 
/*
Template Name: Returns Page
 */
	
?>
<?php get_header(); ?>

<!-- Page -->
<div id="page-wrapper">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
				<div class="row no-margin">
						<div class="col-md-12 col-sm-12 col-xs-12">
								<div id="breadcrumb">
								<div class="row">
								 <div class="col-md-12">
									<ul class="breadcrumbs-list">
										<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
										<li class="active"><?php the_title(); ?></li>
									</ul>
								 </div>
								</div>
								</div>
							
							<div class="clear padding10"></div>

							<div id="returns-page" class="page-content" style="">
								<div id="intro-rp" class="row">
										<div class="col-md-10 col-md-offset-2">
												<div class="box-white">
												<p class="intro" style="">
													<?php _e('We are glad that we have created Art Bulgaria. It is a place where any artist can create and manage their own art shop. We encourage the individuality and respect everyone’s right to run a store with unique terms and conditions on return and replacement of items.', 'pro'); ?>
													<?php _e('We hope, this freedom will be exercised in the best possible way and our customers will be satisfied with their unique artwork shopping experience. In case of any misunderstanding, do not hesitate to contact us via', 'pro'); ?><br><span class="lightblue">admin@artbulgaria.com</span>
													<br>
													<span class="step-intro"><?php _e('Should you decide to return or replace any product, please follow these three simple steps:', 'pro'); ?></span>
												</p>
												</div>
										</div>
								</div>
									<div class="row">
										<div class="col-md-6 col-md-offset-6">
											<div id="return-steps" class="row">
												<div class="col-sm-4 col-xs-4 col-md-4">
													<h4 class="text-center"><?php _e('Step 1', 'pro'); ?></h4>
													<div class="box-white">
														<p class="" style="">
															<?php _e('To return an item to Art Bulgaria, sign in to online Order Status with your Art Bg ID and password', 'pro'); ?>
														</p>
													</div>
												</div>
												<div class="col-sm-4 col-xs-4 col-md-4">
													<h4 class="text-center"><?php _e('Step 2', 'pro'); ?></h4>
													<div class="box-white">
														<p class="" style="">
															<?php _e('Find the store where you purchased the product and read the terms of use', 'pro'); ?>
														</p>
													</div>
												</div>
												<div class="col-sm-4 col-xs-4 col-md-4">
														<h4 class="text-center"><?php _e('Step 3', 'pro'); ?></h4>
													<div class="box-white">
														<p class="" style="">
															<?php _e('Make a connection with the owner of the store and find out the shortest way to return the product', 'pro'); ?>
														</p>
													</div>												
												</div>
											</div>
											<!--<h5 class="more-rp text-right" style="color: #8B8383;">For more information about common questions please follow the page <i id="arrow-d" class="fa fa-arrow-down"></i></h5> -->
										</div>
								</div>								
							</div>
							
							<!-- <div class="clear padding10"></div>
							
							<h4><strong>How to Return An Item?</strong></h4>
							<div class="page-content">
								<div class="row">
										<div class="col-md-12">
												<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
										</div>
								</div>								
							</div>
							
							<div class="clear padding10"></div>
							
							<h4><strong>How to Return An Item?</strong></h4>
							<div class="page-content">
								<div class="row">
										<div class="col-md-12">
												<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
										</div>
								</div>								
							</div>
							
							<div class="clear padding10"></div>
							
							<h4><strong>How to Return An Item?</strong></h4>
							<div class="page-content">
								<div class="row">
										<div class="col-md-12">
												<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
										</div>
								</div>								
							</div>

							</div> <!-- col-md-s -->
							</div> <!-- /row no-margin -->
						
				</div>
			</div>
		</div>
	<?php endwhile; endif; ?>	
</div>	
<?php get_template_part('footer', 'widget');
get_footer(); ?>