<?php get_header(); ?>
	<!-- Page -->
	<div id="page-wrapper">
		<div class="content-section">
			<div class="outercontainer">
				<div class="container" style="min-height: 450px;">	
					<div class="row no-margin">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div id="breadcrumb">
										<div class="row">
											 <div class="col-md-12">
												<ul class="breadcrumbs-list">
													<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
													<li class="active"><?php _e('Stores' , 'pro' ); ?></li>
												</ul>
											 </div>
										</div>
									</div>
								<div class="clear padding10"></div>		
								<div class="row-fluid">
									<div class="span12">
										
										<script type="text/javascript">
											$('.row-fluid').css('display','none');
	
											$(window).load(function() {
												$('.pifilter_category .checkbox :input[value="27"]').attr('checked', false);
												$('#tp_mpdgfilter_2 .btn-primary').click();
												$('.row-fluid').css('display','block');	
											});
										</script>
										<?php echo do_shortcode('[toprank_mpdgfilters store="1"]'); ?>
									</div><!-- / span8 --> 
								</div><!-- / row-fluid -->
							</div><!-- / col-md-s -->
					</div><!-- / row no-margin -->
				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	
	</div><!-- / page-wrapper -->
<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>