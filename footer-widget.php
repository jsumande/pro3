<?php		
/* ------------------- Pages ---------------------------------*/
//Contact Page 195
global $mp;

$blgt_id = icl_object_id(2313, 'page', TRUE, ICL_LANGUAGE_CODE);
$blogs_link = set_url_scheme(localize_url(get_blog_permalink( 1, $blgt_id )),'http');

$cpt_id = icl_object_id(195, 'page', TRUE, ICL_LANGUAGE_CODE);
$contact_link = set_url_scheme(get_blog_permalink( 1, $cpt_id ),'http');

//Privacy Policy 193
$ppt_id = icl_object_id(193, 'page', TRUE, ICL_LANGUAGE_CODE);
$ppt_link = set_url_scheme(get_blog_permalink( 1, $ppt_id ),'http');

//Terms 725
$trt_id = icl_object_id(725, 'page', TRUE, ICL_LANGUAGE_CODE);
$trt_link = set_url_scheme(get_blog_permalink( 1, $trt_id ),'http');

//About us 191
$aut_id = icl_object_id(191, 'page', TRUE, ICL_LANGUAGE_CODE);
$aut_link = set_url_scheme(get_blog_permalink( 1, $aut_id ),'http');

//FAQ 1601 - live 1580
$faqt_id = icl_object_id(1580, 'page', TRUE, ICL_LANGUAGE_CODE);
$faqt_link = set_url_scheme(get_blog_permalink( 1, $faqt_id ),'http');

//Return 1603 - live 1582
$rett_id = icl_object_id(1582, 'page', TRUE, ICL_LANGUAGE_CODE);
$rett_link = set_url_scheme(get_blog_permalink( 1, $rett_id ),'http');

//Shipping 1605 - live 1584
$sht_id = icl_object_id(1584, 'page', TRUE, ICL_LANGUAGE_CODE);
$sht_link = set_url_scheme(get_blog_permalink( 1, $sht_id ),'http');

//Support 1607 - live 1586
$sppt_id = icl_object_id(1586, 'page', TRUE, ICL_LANGUAGE_CODE);
$sppt_link = set_url_scheme(get_blog_permalink( 1, $sppt_id ),'http');

//Press 1607 - live 2081
$stmap_id = icl_object_id(2081, 'page', TRUE, ICL_LANGUAGE_CODE);
$stmap_link = set_url_scheme(get_blog_permalink( 1, $stmap_id ),'http');

//Sitemap 1607 - live 2216
$sppr_id = icl_object_id(2216, 'page', TRUE, ICL_LANGUAGE_CODE);
$sppr_link = set_url_scheme(get_blog_permalink( 1, $sppr_id ),'http');

?>	
    <div id="footer-widget">
        <div class="outercontainer">
            <div class="container">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-4">
                        <div class="widget widget_text">
                            <div  id="logo-container" class="textwidget">
								<img id="footer-artbg-logo" src="<?php echo get_site_url(); ?>/wp-content/themes/pro3/img/logo_beta_small.png" alt="Artbulgaria">
                                <label> <?php _e('Art Bulgaria, the marketplace for buying and selling custom arts, crafts and hire the best writers, singers, and artists from Bulgaria.'); ?> </label>
                       		</div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 col-xs-12">
					<div class="row-fluid">
                        <div class="footer-sub-menu col-md-3 col-sm-3 col-xs-6">
                            <span><?php _e('About us'); ?></span>
                            <ul>
	                            <li><a href="<?php echo $contact_link; ?>"><?php _e('Contact us'); ?></a></li>
	                            <li><a href="<?php echo $ppt_link; ?>"><?php _e('Privacy Policy'); ?></a></li>
	                            <li><a href="<?php echo $trt_link; ?>"><?php _e('Terms of use'); ?></a></li>
	                            <li><a href="<?php echo $sppr_link; ?>"><?php _e('Site map'); ?></a></li>
	                        </ul>
                        </div>
                        <div class="footer-sub-menu col-md-3 col-sm-3 col-xs-6">
                            <span><?php _e('Community'); ?></span>
                            <ul>
								 <li><a href="<?php echo $aut_link; ?>"><?php _e('About us'); ?></a></li>
	                            <li><a href="<?php echo network_site_url('/forums/','http');?>"><?php _e('Forums'); ?></a></li>
	                            <li><a href="<?php echo $blogs_link; ?>"><?php _e('Blog'); ?></a></li>
	                            <li><a href="<?php echo $stmap_link; ?>"><?php _e('Press'); ?></a></li>
	                        </ul>
                        </div>
                        <div class="footer-sub-menu col-md-3 col-sm-3 col-xs-6">
                            <span><?php _e('Stores'); ?></span>
                            <ul>
	                            <li><a href="<?php echo network_site_url('/store/','http'); ?>"><?php _e('Stores'); ?></a></li>
	                            <li><a href="<?php echo network_site_url('/store/products/','http'); ?>"><?php _e('Products'); ?></a></li>
	                            <li><a href="<?php echo add_lang_vars_custom($main_site_url.'/store/shopping-cart/checkout-to/'); ?>"><?php _e('Shopping cart'); ?></a></li>
	                            <li><a href="<?php echo network_site_url('/store/order-status/','http'); ?>"><?php _e('Order Status'); ?></a></li>
	                        </ul>
                        </div>
                        <div class="footer-sub-menu col-md-3 col-sm-3 col-xs-6">
                            <span><?php _e('Help'); ?></span>
                            <ul>
	                            <li><a href="<?php echo $faqt_link; ?>"><?php _e('FAQ'); ?></a></li>
	                            <li><a href="<?php echo $rett_link; ?>"><?php _e('Returns'); ?></a></li>
	                           <!-- <li><a href="<?php echo $sht_link;?>"><?php _e('Shipping'); ?></a></li> -->
	                            <li><a href="<?php echo $sppt_link;?>"><?php _e('Support'); ?></a></li>
	                        </ul>
                        </div>
					</div>
                    </div>
					 <div class="col-md-3 col-sm-12 col-xs-12">
							<?php echo do_shortcode('[trtm_tweeter]'); ?>
					</div>
				</div>
            </div>
            <hr class="break-footer">
            <div class="row footer-contact-wrapper">
            	<div class="col-md-4 col-sm-12 col-xs-12">
            		<div class="footer-contact">
	                   <?php _e('Phone'); ?>: <?php if(defined('ICL_LANGUAGE_CODE')) { ?> 
						<?php if(ICL_LANGUAGE_CODE=='bg') { ?>
							<?php _e('0879 278 247')?><br>	
						<?php } else { ?>	
							<?php _e('+ 359 879 278 247')?><br>	
						<?php } ?>
							<span class="info-helper"><?php _e('or simply select after the code the symbols "artbg7"','pro'); ?></span>
						<?php } ?>
	                    <br><?php _e('Email');?>: admin@artbulgaria.com
	                </div>
            	</div>
            	<div class="col-md-5 col-sm-12 col-xs-12">
					<?php if(function_exists('eemail_show')) eemail_show();  ?>
            	</div>
            	<div class="col-md-3 col-sm-12 col-xs-12">
            		<div class="social-links-wrapper">
	            		<label><?php _e('Find us on'); ?></label>
						<ul class="social-links">
							<?php tr_socail_icons(); ?>
						</ul>
					</div>
				</div>
            </div>
        </div>
    </div>