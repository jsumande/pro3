<?php get_header();

	$prefix = 'mpt';
	$sidebar_position = get_option( $prefix . '_wp_page_sidebar_position');
	$displaysidebar = get_option( $prefix . '_archive_search_page_display_sidebar');
	$displaysidebar = ( !empty($displaysidebar) ? $displaysidebar : 'true' );
	$selected_sidebar = mpt_load_selected_sidebar('page');

	$columns = mpt_get_option( $prefix . '_blog_pages_layout' , '1col' , true );
	$poststyle = mpt_get_option( $prefix . '_blog_pages_box_style' , 'style-1' , true ); 
	$btncolor = mpt_get_option( $prefix . '_blog_pages_button_color' , '' , true );
	$showexcerpt = mpt_get_option( $prefix . '_blog_pages_show_excerpt' , 'yes' , true );
	$showmeta = mpt_get_option( $prefix . '_blog_pages_show_meta' , 'yes' , true );
	$showauthorname = mpt_get_option( $prefix . '_blog_pages_show_author_name' , 'yes' , true );
	$showcomments = mpt_get_option( $prefix . '_blog_pages_show_comments' , 'yes' , true );
	$showdatetag = mpt_get_option( $prefix . '_blog_pages_show_date_tag' , 'yes' , true );
	$datetagcolor = mpt_get_option( $prefix . '_blog_pages_date_tag_color' , '' , true );
	$metatagscolor = mpt_get_option( $prefix . '_blog_pages_meta_tag_color' , '' , true );
?>

	<!-- Page -->
	<div id="page-wrapper">

		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
							
					<div class="row">

						<?php if ( ( empty($sidebar_position) || $sidebar_position == 'Left' ) && $displaysidebar == 'true' ) : ?>
							<div id="sidebar" class="col-md-3">
								<?php get_sidebar( $selected_sidebar ); ?>
							</div>
						<?php endif; ?>
						
						<div class="<?php echo ( $displaysidebar == 'true' ? 'col-md-9' : 'col-md-12' ); ?>">

							<div class="page-heading">

								<?php /* If this is a category archive */ if (is_category()) { ?>
									<h4><span><?php _e( 'Archive for the ' , 'pro' ); ?>&#8216;<?php single_cat_title(); ?>&#8217;<?php _e( ' Category' , 'pro' ); ?></span></h4>

								<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
									<h4><span><?php _e( 'Posts Tagged ' , 'pro' ); ?>&#8216;<?php single_tag_title(); ?>&#8217;</span></h4>

								<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
									<h4><span><?php _e( 'Archive for ' , 'pro' ); ?><?php the_time('F jS, Y'); ?></span></h4>

								<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
									<h4><span><?php _e( 'Archive for ' , 'pro' ); ?><?php the_time('F, Y'); ?></span></h4>

								<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
									<h4><span><?php _e( 'Archive for ' , 'pro' ); ?><?php the_time('Y'); ?></span></h4>

								<?php /* If this is an author archive */ } elseif (is_author()) { ?>
									<h4><span><?php _e( 'Author Archive' , 'pro' ); ?></span></h4>

								<?php /* If this is a paged archive */ } elseif ( isset($wp_query->query_vars['paged']) && !empty($wp_query->query_vars['paged']) ) { ?>
									<h4><span><?php _e( 'Blog Archives' , 'pro' ); ?></span></h4>
								
								<?php } ?>

							</div><!-- / page-heading -->

							<?php 
								$searchterms = isset($wp_query->query_vars['s']) ? get_query_var('s') : '';
								$category = is_category() ? get_query_var('category_name') : '';
								$tag = is_tag() ? get_query_var('tag') : '';
								$author = is_author() ? get_query_var('author_name') : '';
								$day = is_day() ? get_query_var('day') : '';
								$month = is_month() ? get_query_var('monthnum') : '';
								$year = is_year() ? get_query_var('year') : '';

								switch ($btncolor) {
									case 'grey': $btnclass = ' btn-default'; break;
									case 'blue': $btnclass = ' btn-primary'; break;
									case 'lightblue': $btnclass = ' btn-info'; break;
									case 'green': $btnclass = ' btn-success'; break;
									case 'yellow': $btnclass = ' btn-warning'; break;
									case 'red': $btnclass = ' btn-danger'; break;
									case 'black': $btnclass = ' btn-inverse'; break;
									default: $btnclass = ''; break;			
								}

								$args = array(
									'searchterms' => esc_attr( $searchterms ),
									'category' => esc_attr( $category ), 
									'tag' => esc_attr( $tag ),
									'author' => esc_attr( $author ),
									'day' => esc_attr( $day ),
									'month' => esc_attr( $month ),
									'year' => esc_attr( $year ),
									'columns' => $columns,
									'poststyle' => ( $poststyle == 'style-2' ? 'style-2' : 'style-1' ),
									'btnclass' => ( !empty($btnclass) ? $btnclass : '' ),
									'boxclass' => ( $poststyle == 'style-2' ? 'post-grid' : '' ),
									'extra' => array(
										'showexcerpt' => ( $showexcerpt == 'yes' ? true : false ),
										'showmeta' => ( $showmeta == 'yes' ? true : false ),
										'showauthordetails' => ( $showauthorname == 'yes' ? true : false ),
										'showcomments' => ( $showcomments == 'yes' ? true : false ),
										'showhovertag' => ( $showdatetag == 'yes' ? true : false ),
										'hovertagcolor' => ( !empty($datetagcolor) ? 'hover-tag-' . $datetagcolor : '' ),
										'metatagscolor' => ( !empty($metatagscolor) ? 'meta-tag-' . $metatagscolor : '' )
										)
								);
							?>

							<?php pro_display_wp_post_query( apply_filters( 'pro_post_query_archive_page' , $args ) ); ?>

						</div><!-- / span9 -->

						<?php if ( !empty($sidebar_position) && $sidebar_position == 'Right' && $displaysidebar == 'true' ) : ?>
							<div id="sidebar" class="col-md-4">
								<?php get_sidebar( $selected_sidebar ); ?>
							</div>
						<?php endif; ?>

					</div><!-- / row-fluid -->

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>