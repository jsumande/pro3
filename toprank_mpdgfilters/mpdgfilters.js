/**
 * @TopRank Software
 * @author Kevinralph Tenorio <tkralphdev@gmail.com>
 */

// namespace for TopRankWP Plugins
if (!window['TopRankWP']) TopRankWP = {};

// Sorry code kind'a php<->js thing...
// plugin is getting big.. :)

// Closure
(function (ns, $) {
    /**
     * Holds all MPDG Object instantiated
     * @type {{}}
     * @private
     */
    var _MPDG_STORE = {};
    var _AXEC_STORE = {};
    var _QUEUES = [];

    /**
     * used only in PHP
     * @param id
     */
    ns.MPDG = function (id, sort) {
        _MPDG_STORE[id] = new MPDG(id, sort);
    };

    /**
     * use only in PHP
     * @param id
     * @returns {*}
     */
    ns.getMPDG = function (id) {
        return _MPDG_STORE[id];
    };

    /**
     * MPDG Autoexec (plugin) use only in PHP
     * @param cb
     */
    ns.plugin = function (id, cb) {
        if (!_AXEC_STORE[id]) _AXEC_STORE[id] = [];
        _AXEC_STORE[id].push(cb);
    };

    /**
     * MPDG Run Autoexec (plugin)
     * @param id
     */
    ns.runPlugins = function (id, cb) {
        if (!_AXEC_STORE[id]) return;
        for (var i in _AXEC_STORE[id]) _AXEC_STORE[id][i](ns.getMPDG(id));
        if (typeof cb == 'function') cb();
    };

    /**
     * MPDG Object
     * @param id
     * @param sort
     * @constructor
     */
    function MPDG(id, sort) {
        var self = this;
        var pluginStore = {};

        /**
         * Element ID
         */
        self._id = id;
        self._sort = sort;

        /**
         * Get Current Page Container
         * @returns {*|jQuery|HTMLElement}
         */
        self.currentPageContainer = function () {
            return $('div[id^="post-"]');
        };

        /**
         * Get MPDGFilter Container
         * @returns {*|jQuery|HTMLElement}
         */
        self.filterContainer = function () {
            return $('div#tp_mpdgfilter_' + self._id);
        };

        /**
         * Get Filter
         * @returns {*}
         */
        self.filterForm = function () {
            return self.filterContainer().find('div.filter-aside');
        };

        /**
         * Get Content Box
         * @returns {*}
         */
        self.contentBox = function () {
            return self.filterContainer().find('.filter-content');
        };

        /**
         * Get Pagination Box
         * @returns {*}
         */
        self.paginationBox = function () {
            return self.filterContainer().find('.filter-pagination-box');
        };
		
        self.paginationBoxCount = function () {
            return self.filterContainer().find('.filter-pagination-count');
        };			

        /**
         * Get Sorting <select> combo
         * @returns {*}
         */
        self.sortingOptionControl = function () {
            return self.filterContainer().find('select[name="sorting"].sort-control-sort');
        };

        /**
         * Get Plugin Data
         * @param name
         */
        self.pluginData = function (name) {
            if (pluginStore[name] && 'getData' in pluginStore[name]) {
                return pluginStore[name].getData();
            } else if (!name) {
                var _temp = {},
                    pkeys = Object.keys(pluginStore);

                for (var i = 0; i < pkeys.length; i++) {
                    var p = pluginStore[pkeys[i]];
                    if (p && 'getData' in p) _temp[pkeys[i]] = p.getData();
                }

                return _temp;
            } else {
                console.error('Plugin MPDGFilter `' + name + '` is not registered.');
            }
        };

        /**
         * @param name
         * @returns {*}
         */
        self.validateData = function (name) {
            var valResult = [];

            if (pluginStore[name] && 'validations' in pluginStore['name']) {
                valResult.push(pluginStore[name].validations());
            } else if (!name) {
                var pkeys = Object.keys(pluginStore);

                for (var i = 0; i < pkeys.length; i++) {
                    var p = pluginStore[pkeys[i]];
                    if (p && 'validations' in p) {
                        var r = p.validations();
                        if (typeof r == 'string' || r instanceof String) {
                            valResult.push(r);
                        }
                    }
                }
            }

            if (valResult.length > 0) {
                self.displayValidationError(valResult);
                return false;
            } else {
                self.hideValidationError();
                return true;
            }
        };

        /**
         * @param errors
         */
        self.displayValidationError = function (errors) {
            var el = self.filterForm().find('.mpdgfilter_error');

            if (errors.constructor !== Array) {
                errors = [errors];
            }

            el.removeClass('hidden').empty();

            if (el && errors.length > 0) {
                var r = "";
                for (var i = 0; i < errors.length; i++) {
                    r += '<p>' + errors[i].trim() + '</p>';
                }
                el.html(r + '<hr/>');
            } else {
                el.addClass('hidden');
            }
        };

        /**
         * hide
         */
        self.hideValidationError = function () {
            self.displayValidationError([]);
        };

        /**
         * Reset filters
         * @param name
         */
        self.reset = function (name) {
            self.hideValidationError();
            if (pluginStore[name] && 'reset' in pluginStore[name]) {
                __execPluginEvent('reset', [], name);
                //pluginStore[name].reset();
            } else if (!name) {
                __execPluginEvent('reset');
                /*var pkeys = Object.keys(pluginStore);
                 for (var i = 0; i < pkeys.length; i++) {
                 var p = pluginStore[pkeys[i]];
                 if (p && 'reset' in p) p.reset();
                 }*/
            }
        };

        /**
         * Install plugin, get all plugin
         * @param name
         * @param option
         * @returns {Boolean|Object}
         */
        self.plugin = function (name, option) {
            // if no option provided, we get the plugin, and return false if not found
            if (!option) return pluginStore[name] || false;

            // if no name and no option, we return all the plugin
            if (!name && !option) return pluginStore;

            // if name and option are provided, we register
            if (name && option) {
                if (!pluginStore[name]) {

                    // defaults
                    var p = $.extend({
                        // Execute instantly on register
                        init: $.noop,
                        // Execute on document ready
                        ready: $.noop,
                        // Reset
                        reset: $.noop,
                        // Get Data
                        getData: $.noop
                    }, option);

                    pluginStore[name] = p;

                    if ('init' in p && typeof p['init']) {
                        p.init(ns);
                    }

                    return true;
                }

                return false;
            }
        };

        /**
         * Submit Filters
         * @param svc
         */
        self.submitFilters = function (svc) {
            var data = self.pluginData();

            // Check Validations
            if (self.validateData() === false) return;

            // Default
            if (!svc) {
				
               	if(isStoreListings()) {
					svc = 'mpdgfilter.storelist';
				} else if(isEventListings()) {
					svc = 'mpdgfilter.eventlist';
				} else{
					svc = 'mpdgfilter.productlist';
				}
            }

            window.svc(svc, [data], {
                beforeSend: function () {
                    __showFilterBlocker(true);
                    self.filterContainer().find('button[name=search]').blur();
                    self.contentBox().empty();
                    __showLoader();
                }
            }).fail(function (r) {
                //var msg = ["<div class=''>", r.status, "</h3>", r.responseText].join('');
                __showInvalidResponse(r.responseText, 'danger');
                __showFilterBlocker(false);
            }).success(function (r) {
                if ('data' in r && 'success' in r && r['success'] === true) {
                    if (isStoreListings()) {
                        __generateStoreGrid(r.data);
                    } else {
						
						if(isEventListings()){
							__generateEventGrid(r.data);
						}else{
							__generateResultGrid(r.data);
						}

                    }
                    __execPluginEvent('onDataReady', r.data);
                } else {
                    __showInvalidResponse('Invalid response from service api.');
                }
            }).done(function () {
                __showFilterBlocker(false);
            });
        };

        /**
         * Load All Plugins
         * @private
         */
        function __initAllPlugins() {
            ns.runPlugins(self._id, function () {
                __execPluginEvent('ready');
            });
        }

        /**
         * @param event_name
         * @param param
         * @param name
         * @private
         */
        function __execPluginEvent(event_name, param, name) {
            for (var key in pluginStore) {
                var p = pluginStore[key];
                if (name) if (name !== key) continue;
                if (event_name in p && typeof p[event_name] == 'function') {
                    p[event_name](param, ns);
                }
            }
        }

        /**
         * Get if Store Listings
         * @returns {boolean}
         */
        function isStoreListings() {
            var d = self.filterContainer();
            return d.attr('data-store') == '1';
        }
		
        function isEventListings() {
            var d = self.filterContainer();
            return d.attr('data-event') == '1';
        }			

        /**
         * Get Columns
         * @returns {*}
         * @private
         */
        function __getColumns() {
            /*var cols = self.contentBox().attr('data-columns')
             if (cols && cols.length > 0) {
             return cols;
             } else {
             return 4;
             }*/
            return 4;
        }

        /**
         * Shows Loading Indicator
         * @private
         */
        function __showLoader() {
            self.contentBox().html('<br/><br/><center><img src="' + mpdg_filter_assets_url + '/load.gif"/></center>');
        }

        /**
         * @param $visible
         * @private
         */
        var $__blocker = null;

        /**
         * @param $visible
         * @private
         */
        function __showFilterBlocker($visible) {
            if (!$__blocker) {
                $__blocker = $('<div class="filter_blocker"/>');
                $__blocker.appendTo(self.filterForm());
            }
            if ($visible === true) {
                $__blocker.show();
                $(document).focus();
            } else {
                $__blocker.hide();
            }
        }

        // Add for loader
        self.showLoading = __showFilterBlocker;

        /**
         * Generate Store Listings
         * @param response
         * @private
         */
        function __generateStoreGrid(response) {
            if ('items' in response) {
                if (response.items.length == 0) {
                    __showNoData();
                    return;
                }

                var con = self.contentBox();
                var stores = response.items;

                con.empty();

                $.each(stores, function (i, store) {
                    var store_box = $('<div class="store-box"/>');
                    var picture = $('<div class="store-picture"/>');
                    var name = $('<p class="store-name"/>');
					var pcm2 = ('__pcm' in store) ? true : false;
					var pcm = __parse_pcm(store);
					
					
                    name.html(store.storename);

                    picture.css({
                        'background-image': 'url(' + mpdg_filter_assets_url + '/load1.gif' + ')',
                        'background-size': 'auto'
                    });

                    // image cache loading
                    var imgcache = $('<img src="' + store.pictureurl + '"/>');
                    imgcache.error(function () {
                        picture.css({
                            'background-image': 'url(' + mpdg_filter_assets_url + '/no-image.png)',
                            'background-size': 'cover'
                        });
                        imgcache.remove();
                    }).load(function () {
                        picture.css({
                            'background-image': 'url(' + store.pictureurl + ')',
                            'background-size': 'cover'
                        });
                    });

                    // add the picture
                    picture.appendTo(store_box);

					// add fee box 
					if(pcm2) $(picture).html('<span class="store-fee">'+pcm.output+'/'+store.store_hour+'</span>');
					
                    // add the name after picture
                    name.appendTo(store_box);
                    // add the rating
                    if ('rating' in store && store['rating']) $(store.rating).appendTo(store_box);

                    // add buttons
                    var button_con = $('<div class="store-btncon row-fluid" /><');
                    $('<a href="' + store.site_url + '"/>').addClass('span5 btn btn-primary btn-mini')
                        .html(store.lbl_view_store).appendTo(button_con);

                    var smsg = $('<a href="#""/>')
                        .addClass('span7 btn btn-default btn-mini')
                        .prop('rel', 'popover')
                        .prop('data-id', store.blog_id)
                        .prop('data-ptoggle', 'hide')
                        .html(store.lbl_send_msg);

                    $(smsg).popover({
                        placement: 'bottom',
                        container: 'body',
                        html: true,
                        template: '<div class="popover sms_' + store.blog_id + '"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>',
                        content: function () {
                            return '<div class="form">' +
                                '<label>' + MPDGVal.subject + ':</label>' +
                                '<input type="text" class="form-control"/>' +
                                '<label>' + MPDGVal.msg + ':</label>' +
                                '<textarea class="form-control" style="margin-top:5px;resize:none;"></textarea>' +
                                '</div>' +
                                '<div class="sendmsg_info"></div>' +
                                '<div class="sendmsg_btns">' +
                                '<button class="btn btn-primary btn-sm" style="margin-right:3px;">' + MPDGVal.send + '</button>' +
                                '<button class="btn btn-default btn-sm">' + MPDGVal.close + '</button></div>';
                        }
                    }).on('show.bs.popover', function () {
                        $(this).prop('data-ptoggle', 'show');
                    }).on('hide.bs.popover', function () {
                        $(this).prop('data-ptoggle', 'hide');
                    }).click(function (e) {
                        e.preventDefault();

                        var toggle = $(this).prop('data-ptoggle');
                        var dataid = $(this).prop('data-id');
                        var con = $('.popover.sms_' + dataid + ' .popover-content');
                        var infobox = con.find('.sendmsg_info');
                        var formbox = con.find('.form');
                        var btnsbox = con.find('.sendmsg_btns');

                        con.css('min-width', '220px');

                        if (toggle == 'show') {
                            var sendbtn = con.find('button').first();
                            var closebtn = con.find('button').last();
                            var subject = con.find('input[type=text]');
                            var message = con.find('textarea');

                            sendbtn.off().on('click', function () {
                                window.svc('sendmsgstore', [dataid, message.val(), subject.val()], {
                                    beforeSend: function () {
                                        formbox.hide();
                                        btnsbox.hide();
                                        infobox.removeClass('alert alert-success alert-danger');
                                        infobox.addClass('tp_mpdgfilter_ajax_loadbg');
                                        infobox.show();
                                        infobox.html("&nbsp;");
                                    }
                                }).fail(function (xhr) {
                                    formbox.show();
                                    btnsbox.show();
                                    infobox.removeClass('tp_mpdgfilter_ajax_loadbg');
                                    infobox.addClass('alert alert-danger').show().text(MPDGVal.error + ': ' + xhr.responseText);
                                }).success(function (r) {
                                    formbox.hide();
                                    btnsbox.show();
                                    sendbtn.hide();
                                    closebtn.show();
                                    infobox.removeClass('tp_mpdgfilter_ajax_loadbg');
                                    infobox.css('margin-bottom', '10px');
                                    infobox.show().text(r.data);
                                });
                            });

                            closebtn.off().on('click', function () {
                                $(smsg).popover('hide');
                            });
                        }
                    });

                    smsg.appendTo(button_con);
                    button_con.appendTo(store_box);
                    store_box.appendTo(con);
                });
            } else {
                __showInvalidResponse('Response was invalid.');
            }
        }

        /**
         * @param item
         * @returns {*}
         * @private
         */
        function __parse_pcm(item) {
            var robj = {}, pcm = ('__pcm' in item) ? item['__pcm'] : null;

            if (pcm != null) {
				
				if(String(pcm.amount).indexOf('.') >= 0){
					
					var amount_splt = String(pcm.amount).split('.');

					// check for mantissa & <sup> it if needed.
					pcm.amount = (amount_splt.length > 0)
						? (amount_splt[0] + '<sup>.' + amount_splt[1] + '</sup>')
						: amount_splt[0];
				}
					
                // If the amount is just to large, last check was 9 is the highest
                var amount_len = String(pcm.amount).length;
                if (pcm.amount.indexOf('.') !== 0) amount_len -= 11; // the <sup></sup>;
                pcm.css = (amount_len > 9) ? ' style="zoom:0.75; -moz-transform: scale(0.75);"' : '';

                // replace keys to values
                Object.keys(pcm).filter(function (T) {
                    pcm.output = pcm.output.replace('{' + T + '}', pcm[T]);
                });
            } else {
                pcm = {
                    css: '',
                    output: '??<sup>.??</sup> $'
                };
            }

            return pcm;
        }

        /**
         * Generating Result Grids
         * @param response
         * @private
         */
        function __generateResultGrid(response) {
            var currentLang = $('#langCurrentPage').val();
            if ('items' in response) {
                if (response.items.length == 0) {
                    __showNoData();
                    return;
                }

                var con = self.contentBox();
                var products = response.items;

                con.empty();

                for (var i = 0; i <= products.length - 1; i++) {
                    var item = products[i];
                    var ishot = item.is_hot_item == '1';
                    var issale = item.is_sale == '1';
                    var pcm = __parse_pcm(item);

                    //var spanning = Math.round(12 / __getColumns());
                    //var box_tpl = "";
                    var box = $('<div class="product-box" />');

                    box.attr('data-pid', item.post_id + '_' + item.blog_id);
                    box.attr('data-col-span', ishot ? 2 : 1);
                    box.addClass('col-lg-3 col-md-4 col-sm-6' + (ishot ? ' hot' : ''));

                    if (issale) box.addClass('sale');

                    box.html($.map([
                        "   <div class='product-box-container'>",
                        "       <div class='row-fluid'>",
                        "           <a href='", item.post_permalink, currentLang, "' class='product-image row-fluid' style='background-image: url(", item.image_url, ");'></a>",
                        "       </div>",
                        "       <div class='row-fluid'>",
                        "           <div class='product-title col-md-12 col-xs-12'><a href='", item.post_permalink, currentLang,  "'>", item.post_title, "</a></div>",
                        "           <div class='product-views col-md-4 col-xs-4'><strong>", (item.total_reviews || 0), "</strong>&nbsp;" + MPDGVal.views + "</div>",
                        "       </div>",
                        "       <div class='row-fluid'>",
                        "           <div class='product-cat col-md-9 col-xs-9'>", item.term_names, "</div>",
                        "           <div class='product-rating col-md-3 col-xs-3'>", item.rating, "</div>",
                        "       </div>",
                        "       <div class='row-fluid'><hr/></div>",
                        "       <div class='row-fluid' style='min-height: 27px;'>",
                        "           <div class='product-price col-md-6 col-sm-6 col-xs-9'", pcm.css, ">", pcm.output, "</div>",
                        "           <div class='product-action col-md-6 col-sm-6 col-xs-3 no-padder'>",
                        "               <a href='", item.post_permalink, currentLang, "' class='btn btn-block btn-primary btn-small'>", "<i class='fa fa-shopping-cart'></i>&nbsp; ", MPDGVal.shopnow, "</a>",
                        "           </div>",
                        "       </div>",
                        "   </div>"
                    ], function (t) {
                        return String(t).trim();
                    }).join(''));

                    box.appendTo(con);
                }
            } else {
                __showInvalidResponse(MPDGVal.rwi);
            }
        }
		
		
        /**
         * Generating EventResult Grids
         * @param response
         * @private
         */
        function __generateEventGrid(response) {
            var currentLang = $('#langCurrentPage').val();
            if ('items' in response) {
                if (response.items.length == 0) {
                    __showNoData();
                    return;
                }

                var con = self.contentBox();
                var products = response.items;

                con.empty();

                for (var i = 0; i <= products.length - 1; i++) {
                    var item = products[i];
                    var ishot = item.is_hot_item == '1';
                    var issale = item.is_sale == '1';
                    var pcm = __parse_pcm(item);

                    //var spanning = Math.round(12 / __getColumns());
                    //var box_tpl = "";
                    var box = $('<div class="product-box" />');

                    box.attr('data-pid', item.post_id + '_' + item.blog_id);
                    box.attr('data-col-span', ishot ? 2 : 1);
                    box.addClass('col-lg-3 col-md-4 col-sm-6' + (ishot ? ' hot' : ''));

                    if (issale) box.addClass('sale');

					var event_location = '&nbsp;';
					if(item.event_location) {
						var event_location = item.event_location;
					}	
					
                    box.html($.map([
                        "   <div class='product-box-container'>",
                        "       <div class='row-fluid'>",
                        "           <a href='", item.post_permalink, currentLang, "' class='product-image row-fluid' style='background-image: url(", item.image_url, ");'></a>",
                        "       </div>",
                        "       <div class='row-fluid'>",
                        "           <div class='product-title text-center col-md-12 col-xs-12'><a href='", item.post_permalink, currentLang,  "'>", item.post_title, "</a></div>",
                        "       </div>",
                        "       <div class='row-fluid'>",
                        "           <div class='col-md-12 col-xs-12 text-center product-cat'>", event_location, "</div>",	
                        "       </div>",						
                        "       <div class='row-fluid'>",
                        "           <div class='col-md-12 col-xs-12 text-center product-cat'>", item.event_start, "</div>",	
                        "       </div>",						
                        "       <div class='row-fluid' style='min-height: 27px;'>",
                        "           <div class='product-action col-md-12 col-sm-12 col-xs-12 no-padder'>",
                        "               <a href='", item.post_permalink, currentLang, "' class='btn btn-block btn-primary btn-small'>", MPDGVal.viewevent, "</a>",
                        "           </div>",
                        "       </div>",
                        "   </div>"
                    ], function (t) {
                        return String(t).trim();
                    }).join(''));

                    box.appendTo(con);
                }
            } else {
                __showInvalidResponse(MPDGVal.rwi);
            }
        }		

        /**
         * Show Invalid Response Message
         * @param msg
         * @param type
         * @private
         */
        function __showInvalidResponse(msg, type) {
            if (!type) type = 'danger';
            var tpl = ["<div class='alert alert-" + type + "'>", msg, "</div>"].join('');
            //console.log(msg);
            self.contentBox().html(tpl);
        }

        /**
         * Show No Data
         * @private
         */
        function __showNoData() {
            // Deprecated
            __showInvalidResponse(MPDGVal.nmrf, "warning")
        }

        /**
         * Form Filter Bindings
         * @private
         */
        function __formBindings() {
            self.filterForm().find('button[name="search"]').click(function (e) {
                e.preventDefault();
                __execPluginEvent('onSearchClick');
                self.submitFilters();
            });

            self.filterForm().find('button[name="reset"]').click(function (e) {
                e.preventDefault();

                // YOUR CUSTOM RESET FILTER

                // --- Please allow this function to work... this plugin has logic to follow.
                self.reset();

                //location_country.selectbox('change', '', location_country.find("option[selected='selected']").text());
                //jQuery('.tp_mpdgfilter .pifilter_sorting .sbHolder .sbOptions li a[rel="newpop"]').click();

                /*jQuery('.tp_mpdgfilter .sbHolder .sbOptions li:first-child a').click();
                 jQuery('.tp_mpdgfilter .pifilter_sorting .sbHolder .sbOptions li a[rel="newpop"]').click();
                 jQuery('.tp_mpdgfilter .leftLabel').html('0 $');
                 jQuery('.tp_mpdgfilter .rightLabel').html('5000 $');
                 jQuery('.tp_mpdgfilter .bar.nst-animating').css("left","8px");
                 jQuery('.tp_mpdgfilter .bar.nst-animating').css("width","235px");
                 jQuery('.tp_mpdgfilter .leftGrip.nst-animating').css('left','0');
                 jQuery('.tp_mpdgfilter .rightGrip.nst-animating').css('left','235px');
                 jQuery('.tp_mpdgfilter .pifilter_category .checkbox input').attr('checked', true);
                 jQuery('.tp_mpdgfilter .pifilter_sortkeywords input').val('');*/
            });
        }

        /**
         * PreContent
         * @private
         */
        function __precontents() {
            self.reset();
            self.submitFilters();
        }

        // Bootstrap
        function __bootstrap() {
            self.currentPageContainer().addClass('mpdgfilter-container');
            __initAllPlugins();
            __formBindings();
            __showFilterBlocker(false);
            __precontents();
        }

        // On document ready
        __showFilterBlocker(true);
        $(document).ready(__bootstrap);
    }

}(TopRankWP, jQuery));