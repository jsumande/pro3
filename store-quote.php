<?php get_header(); ?>
	<div id="page-wrapper">
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
					<div class="row-fluid">
						<div class="span12">
							<div <?php post_class(); ?>>
								<div class="page-heading">
								<h1><span><?php echo $store_quote_title; ?></span></h1>
								</div>
								<div class="page-content">
									<?php do_shortcode('[toprank_request_quote_form]'); ?>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>	
	</div>
<?php get_template_part('footer', 'widget'); ?>
<?php get_footer(); ?>