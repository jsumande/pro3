<?php
// **********************************************************
// If you want to have an own template for this action
// just copy this file into your current theme folder
// and change the markup as you want to
// **********************************************************
if ( is_user_logged_in() ) {
	wp_safe_redirect( home_url( '/user-profile/' ) );
	exit;
}
?>
<?php get_header(); ?>
	<div id="page-wrapper">
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
						<div class="page-heading">	
							<h2><span><?php _e( 'Activation', UF_TEXTDOMAIN ); ?></span></h2>
						</div>						
						<div class="clear padding10"></div>
						<div class="page-content">
							<div id="useractivation" class="row">							
								<div class="col-md-6">							

									<?php echo apply_filters( 'uf_activation_messages', isset( $_SESSION['regecode'] ) ? $_SESSION['regecode'] : '' ); ?>
									<h3 class="activation-title" style="border-bottom-color: #000"><?php _e( 'Please enter your key.', UF_TEXTDOMAIN ) ?></h3>
									<hr>
									<form action="<?php echo uf_get_action_url( 'activation' ); ?>" method="post" class="sa-form">
										<?php wp_nonce_field( 'activation', 'wp_uf_nonce_activation' ); ?>
										<div class="form-group">
											<!-- <label class="control-label" for="user_key"><?php _e( 'Key:', UF_TEXTDOMAIN ); ?></label> -->
											<div class="controls">
												<input class="form-control" type="text" name="user_key" id="user_key" value="<?php echo isset( $_GET[ 'key' ] ) ? $_GET[ 'key' ] : ''; ?>">
											</div>
											<div class="pull-right">
												<input class="btn btn-inverse btn-large" type="submit" name="submit" id="submit" value="<?php _e( 'Activate' ); ?>">
											</div>	
										</div>				
									</form>
									
								</div>
							</div>
						</div>	
					</div>	
				</div>	
			</div>	
		</div>	
	</div>	
<?php uf_notification_unset(); ?>	
<?php get_template_part('footer', 'widget'); ?>	
<?php get_footer(); ?>