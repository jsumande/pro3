<?php 
/*
Template Name: FAQ Page
 */
get_header();

	//$sidebar_position = get_option('mpt_wp_page_sidebar_position');
	//$sidebar_position = get_blog_option(1,'mpt_wp_page_sidebar_position');
	//$selected_sidebar = mpt_load_selected_sidebar('page');
?>

	<!-- Page -->
	<div id="page-wrapper">

<?php if (have_posts()) : while (have_posts()) : the_post();

	$contentbgcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_bg_color', true ));
	$contenttextcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_text_color', true ));

	$meta_prefix = '_mpt_';
	$col = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_layout', '1col' , true );
	$entries = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_entries', 5 , true );
	$poststyle = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_box_style' , 'style-1' , true ); 
	$btncolor = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_btn_color' , '' , true );
	$showexcerpt = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_show_excerpt' , 'yes' , true );
	$showmeta = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_show_meta' , 'yes' , true );
	$showauthorname = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_show_author_name' , 'yes' , true );
	$showcomments = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_show_comments' , 'yes' , true );
	$showdatetag = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_show_date_tag' , 'yes' , true );
	$datetagcolor = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_date_tag_color' , '' , true );
	$metatagscolor = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_meta_tag_color' , '' , true );
	
?>

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">
				<div class="row">
						
						<div class="col-md-8 col-md-offset-2"<?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>
						<div id="breadcrumb">
								<div class="row">
									 <div class="col-md-12">
										<ul class="breadcrumbs-list">
											<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>						
											<li class="active"><?php the_title(); ?></li>	
										</ul>
									 </div>
									</div>
							</div>
				<div class="clear padding10"></div>									
									<div class="page-content">
									<div id="faq">
										
											
										
<div class="top text-center">
	<?php if(ICL_LANGUAGE_CODE=='bg'): ?>
	<h4>Често задавани въпроси</h4>
	<small>Тук можете да намерите отгвори на основните въпроси и често срещани проблеми.</small>
	<?php else: ?>
	<h4>Frequently asked questions</h4>
	<small>Here you can find answers to basic questions about the site troubleshoot some common issues.</small>
	<?php endif; ?>	
</div>

<?php the_content(); ?>
<?php /***
<!--TOPIC 1-->
<div class="faq-topic-title text-center">
	<h4>GETTING STARTED</h4>
</div>

<div class="panel-group" id="topic1" role="tablist" aria-multiselectable="true">
	<!--  Topic 1 content 1 -->
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingOne"><h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#topic1" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">What does Artbulgaria do?<i class="fa fa-angle-up pull-right"></i></a> </h4></div><div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
		<div class="panel-body">&nbsp; ArtBulgaria is an web portal that offers arts, crafts and performances from Bulgarian artists. If you are an artist, you can set up your own store at no cost and list your products or allow booking of your performances. If you are a customer looking for Bulgarian artists, you can browse the stores and purchase products or book a performance. </div></div>
	  </div>
	  <!--  Topic 1 content 2 -->
	 <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
		  <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#topic1" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Which currencies are usable with ArtBulgaria?  <i class="fa fa-angle-up pull-right"></i></a></h4></div>
		<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		  <div class="panel-body">We only accept USD, Lev and Euro currencies</div></div>
	</div>
	  <!--  Topic 1 content 3 -->
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThree">
		  <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#topic1" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> Which languages are supported?  <i class="fa fa-angle-up pull-right"></i>	</a></h4></div>
		<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
		  <div class="panel-body">We only support English and Bulgarian languages </div></div>
	  </div>
</div>
<!-- END OF TOPIC 1 --->

<!-- TOPIC 2 --->								
<div class="faq-topic-title text-center">
	<h4>Your Artbulgaria account</h4>
</div>

<div class="panel-group" id="faq-2" role="tablist" aria-multiselectable="true">
		<!--  content 1 -->
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingOne">
		  <h4 class="panel-title">
			<a class="collapsed" data-toggle="collapse" data-parent="#faq-2" href="#collapseOne-faq2" aria-expanded="true" aria-controls="collapseOne-faq2"> How do I activate my account? <i class="fa fa-angle-up pull-right"></i></a> </h4></div>
		<div id="collapseOne-faq2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
		  <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div></div>
	  </div>
	  <!--  content 2 -->
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
		  <h4 class="panel-title">
			<a class="collapsed" data-toggle="collapse" data-parent="#faq-2" href="#collapseTwo-faq2" aria-expanded="false" aria-controls="collapseTwo-faq2"> I can't find my confirmation email. Where is it?<i class="fa fa-angle-up pull-right"></i></a>
		  </h4>
		</div>
		<div id="collapseTwo-faq2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		  <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div></div>
	  </div>
	  
	   <!--  content 3 -->
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThree">
		  <h4 class="panel-title">
			<a class="collapsed" data-toggle="collapse" data-parent="#faq-2" href="#collapseThree-faq2" aria-expanded="false" aria-controls="collapseThree-faq2"> I signed up as a Buyer and wuld now like to become a Seller. How do I do this?  <i class="fa fa-angle-up pull-right"></i></a> </h4></div>
		<div id="collapseThree-faq2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
		  <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.  </div></div>
	  </div>
	</div>
	<!-- END OF TOPIC 2 -->
	<!---->
	<div class="faq-topic-title text-center">
	<h4>General</h4>
</div>

<div class="panel-group" id="faq-3" role="tablist" aria-multiselectable="true">
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingOne">
		  <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#faq-3" href="#collapseOne-faq3" aria-expanded="true" aria-controls="collapseOne-faq3"> How do I activate my account? <i class="fa fa-angle-up pull-right"></i></a> </h4></div>
		<div id="collapseOne-faq3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
		  <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.  </div></div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
		  <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#faq-3" href="#collapseTwo-faq3" aria-expanded="false" aria-controls="collapseTwo-faq3">  I can't find my confirmation email. Where is it?  <i class="fa fa-angle-up pull-right"></i></a>  </h4></div>
		<div id="collapseTwo-faq3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		  <div class="panel-body">	Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.  </div></div> </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThree">
		  <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#faq-3" href="#collapseThree-faq3" aria-expanded="false" aria-controls="collapseThree-faq3"> I signed up as a Buyer and wuld now like to become a Seller. How do I do this?  <i class="fa fa-angle-up pull-right"></i></a> </h4></div>
		<div id="collapseThree-faq3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
		  <div class="panel-body">	Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.  </div></div>
	  </div>
	</div> **/?>
									</div>
										<?php  //edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>

									
							<?php

		endwhile; endif;

								switch ($btncolor) {
									case 'grey': $btnclass = ' btn-default'; break;
									case 'blue': $btnclass = ' btn-primary'; break;
									case 'lightblue': $btnclass = ' btn-info'; break;
									case 'green': $btnclass = ' btn-success'; break;
									case 'yellow': $btnclass = ' btn-warning'; break;
									case 'red': $btnclass = ' btn-danger'; break;
									case 'black': $btnclass = ' btn-inverse'; break;
									default: $btnclass = ''; break;			
								}

							  	$query_args = array(
									'echo' => true,
									'paginate' => true,
									'per_page' => $entries, 
									'order_by' => 'date', 
									'order' => 'DESC',
									'columns' => $col,
									'poststyle' => ( $poststyle == 'style-2' ? 'style-2' : 'style-1' ),
									'btnclass' => ( !empty($btnclass) ? $btnclass : '' ),
									'boxclass' => ( $poststyle == 'style-2' ? 'post-grid' : '' ),
									'extra' => array(
										'showexcerpt' => ( $showexcerpt == 'yes' ? true : false ),
										'showmeta' => ( $showmeta == 'yes' ? true : false ),
										'showauthordetails' => ( $showauthorname == 'yes' ? true : false ),
										'showcomments' => ( $showcomments == 'yes' ? true : false ),
										'showhovertag' => ( $showdatetag == 'yes' ? true : false ),
										'hovertagcolor' => ( !empty($datetagcolor) ? 'hover-tag-' . $datetagcolor : '' ),
										'metatagscolor' => ( !empty($metatagscolor) ? 'meta-tag-' . $metatagscolor : '' )
										)
								);						 

								//pro_display_wp_post_query( apply_filters( 'pro_post_query_blog_page' , $query_args ) );
							?>

						</div><!-- / span -->

				<?php if ( $col == '1col' ) { ?>
						<?php if ( !empty($sidebar_position) && $sidebar_position == 'Right' ) : ?>
							<div id="sidebar" class="span4">
								<?php get_sidebar( $selected_sidebar ); ?>
							</div>
						<?php endif; ?>
					</div><!-- / row -->
				<?php } ?>

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget');

get_footer(); ?>