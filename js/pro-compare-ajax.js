/**** PRO Theme Product Comparision Ajax JS *********/
jQuery(document).ready(function() {

	function pro_compare_results_updater() {
		jQuery("button.update-compare-results").click(function() {
			var input = jQuery(this);
			var formElm = jQuery(input).parents('form.compare-products-form');
			var tempHtml = formElm.html();
			var serializedForm = formElm.serialize();
			formElm.html('<img src="'+PRO_PC_Ajax.imgUrl+'" alt="'+PRO_PC_Ajax.loadingPCMsg+'" /><span>'+PRO_PC_Ajax.loadingPCMsg+'</span>');
			jQuery.post(PRO_PC_Ajax.ajaxUrl, serializedForm, function(data) {
				var result = data.split('||', 2);
				if (result[0] == 'error') {
					formElm.html(tempHtml);
					pro_compare_results_updater();
				} else {
					formElm.html('<div class="alert alert-success nomargin">'+PRO_PC_Ajax.updatedMsg+'</div>').fadeIn('fast');
					formElm.fadeOut(1000, function(){
						formElm.html(result[1]).fadeIn('fast');
						pro_compare_results_updater();
					});
				}
			});
			return false;
		});
	}

	//add item to Compare
	function pro_compare_listeners() {
		jQuery("input.pro_button_addcompare").click(function() {
			var input = jQuery(this);
			var formElm = jQuery(input).parents('form.pro_compareproducts_form');
			var tempHtml = formElm.html();
			var serializedForm = formElm.serialize();
			formElm.html('<img src="'+PRO_PC_Ajax.imgUrl+'" alt="'+PRO_PC_Ajax.addingPCMsg+'" />');
			jQuery.post(PRO_PC_Ajax.ajaxUrl, serializedForm, function(data) {
				var result = data.split('||', 3);
				if (result[0] == 'error') {
					formElm.html(tempHtml);
					formElm.after(result[1]);
					pro_compare_listeners();
				} else {
					formElm.fadeOut(1000, function(){
						formElm.html(result[1]).fadeIn('fast');
						jQuery('form.compare-products-form').html(result[2]);
						pro_compare_listeners();
						pro_compare_results_updater();
					});
				}
			});
			return false;
		});
	}

	pro_compare_results_updater();
	pro_compare_listeners();
});