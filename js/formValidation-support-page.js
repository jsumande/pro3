jQuery(document).ready(function() {
    jQuery('div#page-support #support').formValidation({
        framework: 'bootstrap',
		err: {
            container: function($field, validator) {
                // Look at the markup
                //  <div class="col-xs-4">
                //      <field>
                //  </div>
                //  <div class="col-xs-5 messageContainer"></div>
                return $field.parent().prev('.messageContainer');
            }
        },
        fields: {
            support_name: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.supName
                    },
					stringLength: {
                        min: 4,
                        max: 30,
                        message: formVal.supNameLength
                    }					
                }
            },	            
			support_email: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.supEmail
                    },
                    emailAddress: {
                        message: formVal.supEmailNotVal
                    }
                }
            },
			support_about: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.supAbout
                    }					
                }
            },
			support_message: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.supMessage
                    }					
                }
            },	 			
        }
    });
});