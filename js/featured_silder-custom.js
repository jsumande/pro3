jQuery(document).ready(function(){
	jQuery(".featured_slider_fouc .featured_slider_set").show();
});
jQuery("html").addClass("featured_slider_fouc");
jQuery(".featured_slider_fouc .featured_slider_set").hide();
jQuery(document).ready(function() {
	jQuery("#featured_slider_1").cycle2({
		speed: 1000,
		slides :"div.featured_slide",
		next:   "#featured_slider_1_next", 
		 prev:   "#featured_slider_1_prev",
		pauseOnHover: true,
		fx: "scrollHorz",
		swipe: true,
		easing: "easeInExpo",
		timeout: 3000
	});

  jQuery("#featured_slider_1").featuredSlider({
		width		:1000,
		origLw		:500,
		origLh		:300,
		origSw		:500,
		origSh		:150
	});	
	
	
	jQuery("#featured_slider_2").cycle2({
		speed: 1000,
		slides :"div.featured_slide",
		next:   "#featured_slider_2_next", 
		 prev:   "#featured_slider_2_prev",
		pauseOnHover: true,
		fx: "scrollHorz",
		swipe: true,
		easing: "easeInExpo",
		timeout: 3000
	});

  jQuery("#featured_slider_2").featuredSlider({
		width		:1000,
		origLw		:500,
		origLh		:300,
		origSw		:500,
		origSh		:150
	});	
		
	
	
	jQuery("#index-blogs-con").cycle2({
		speed: 300,
		slides :"div.index-blogs-slide",
		next:   "#index_blogs_slider_next", 
		 prev:   "#index_blogs_slider_prev",
		pauseOnHover: true,
		fx: "scrollHorz",
		swipe: true,
		easing: "swing",
		auto:  false,
		timeout: 0,
	});	
	
	jQuery("#newest-products-slides-con").cycle2({
		speed: 300,
		slides :"div.newest-products-slides",
		next:   "#newest-products-slides_next", 
		 prev:   "#newest-products-slides_prev",
		pauseOnHover: true,
		fx: "scrollHorz",
		swipe: true,
		easing: "swing",
		timeout: 3000
	});		
	
});