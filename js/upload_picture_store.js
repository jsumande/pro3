var $j = jQuery.noConflict();
$j(document).ready(function($){
	$j(document).on('click', '.Upload_picture', function(event) {
		//var numItems = jQuery('.toprank_image_upload_container').length;		
		var $clicked = $(this), frame;	
		event.preventDefault();		
		if ( frame ) {
			frame.open();
			return;
		}
	
		frame = wp.media.frames.aq_media_uploader = wp.media({
			library: {
				type: 'image'
			},
			view: {				
			}
		});
		
		frame.on( 'select', function() {
			var attachment = frame.state().get('selection').first();
			var imgurl	= attachment.attributes.url;
			$j('#store_picture').val(imgurl);
			$j('#store_picture_holder img').attr('src',imgurl);
			$j('.Upload_picture').hide();
			$j('.Delete_picture').show();
		});

		frame.open();
		
	});
	
	$j(document).on('click', '.Delete_picture', function(event) {
		$j('#store_picture_holder img').attr('src',nopic_url);
		$j('#store_picture').val('');
		$j('.Delete_picture').hide('fast',function(){
		$j('.Upload_picture').show();
		});
	});
});	