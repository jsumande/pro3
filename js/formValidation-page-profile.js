$(document).ready(function() {	
		$('input[type="checkbox"]').change(function() {
			var data = ($( ".multiselect" ).attr( "title" ));
			var result = data.substring(30, data.length);					
			$( "#perf" ).val( result );			
		});		
		$('#fuck').formValidation({
			fields: {
				first_name: {
					validators: {
						notEmpty: {
							message: formVal.fname
						}
					}
				},
				last_name: {
					validators: {
						notEmpty: {
							message: formVal.lname
						}
					}
				},
				nickname: {
					validators: {
						notEmpty: {
							message: formVal.nname
						}
					}
				},
				phone: {
					validators: {
						notEmpty: {
							message: formVal.phone
						}
					}
				},
				address1: {
					validators: {
						notEmpty: {
							message: formVal.address
						}
					}
				},
				city: {
					validators: {
						notEmpty: {
							message: formVal.city
						}
					}
				},
				state: {
					validators: {
						notEmpty: {
							message: formVal.state
						}
					}
				},
				zip: {
					validators: {
						notEmpty: {
							message: formVal.zip
						}
					}
				},
				bcat_site_description: {
					validators: {
						notEmpty: {
							message: formVal.store
						}
					}
				},
				'bcat_site_categories[]': {
				   validators: {
						choice: {
							min: 2,
							max: 4,
							message: formVal.categories
						}
					 }
				},							
			 }
		}) 
	});