/**** PRO Theme Wishlist Ajax JS *********/
jQuery(document).ready(function() {

	//add item to Wishlist
	function pro_wishlist_listeners() {
		jQuery("input.pro_button_addwishlist").click(function() {
			var input = jQuery(this);
			var formElm = jQuery(input).parents('form.pro_wishlist_form');
			var tempHtml = formElm.html();
			var serializedForm = formElm.serialize();
			formElm.html('<img src="'+PRO_WL_Ajax.imgUrl+'" alt="'+PRO_WL_Ajax.addingWLMsg+'" />');
			jQuery.post(PRO_WL_Ajax.ajaxUrl, serializedForm, function(data) {
				var result = data.split('||', 2);
				if (result[0] == 'error') {
					formElm.html(result[1]);
					pro_wishlist_listeners();
				} else {
					formElm.fadeOut(1000, function(){
						formElm.html('<div class="alert alert-success nomargin">'+PRO_WL_Ajax.successWLMsg+'</div>').fadeIn('fast');
						pro_wishlist_listeners();
					});
				}
			});
			return false;
		});
	}
	
	pro_wishlist_listeners();

});