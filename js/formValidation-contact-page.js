$(document).ready(function() {
    $('#contact-us').formValidation({
        framework: 'bootstrap',
        fields: {
            con_name: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.conName
                    },
					stringLength: {
                        min: 4,
                        max: 30,
                        message: formVal.conNameLength
                    }					
                }
            },	            
			con_email: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.conEmail
                    },
                    emailAddress: {
                        message: formVal.conEmailNotVal
                    }
                }
            },		
			con_message: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.conMessage
                    }					
                }
            },	 			
        }
    });
});