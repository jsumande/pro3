/**** PRO Theme CS Ajax JS *********/
jQuery(document).ready(function() {

	//empty cart
	function pro_empty_cart() {
		if (jQuery("a.mp_empty_cart").attr("onClick") != undefined) {
			return;
		}

		jQuery("a.mp_empty_cart").click(function() {
			var answer = confirm(PRO_CS_Ajax.emptyCartMsg);
			if (answer) {
				jQuery(this).html('<img src="'+PRO_CS_Ajax.imgUrl+'" />');
				jQuery.post(PRO_CS_Ajax.ajaxUrl, {action: 'mp-update-cart', empty_cart: 1}, function(data) {
					jQuery("div.mp_cart_widget_content").html(data);
				});
			}
			return false;
		});
	}

	//add item to cart
	function pro_cart_listeners() {
		jQuery("input.pro_button_addcart").click(function() {
			var input = jQuery(this);
			var formElm = jQuery(input).parents('form.pro_buy_form');
			var tempHtml = formElm.html();
			var serializedForm = formElm.serialize();
			formElm.html('<img src="'+PRO_CS_Ajax.imgUrl+'" alt="'+PRO_CS_Ajax.addingMPMsg+'" />');
			jQuery.post(PRO_CS_Ajax.ajaxUrl, serializedForm, function(data) {
				var result = data.split('||', 4);
				if (result[0] == 'error') {
					alert(result[1]);
					formElm.html(tempHtml);
					pro_cart_listeners();
				} else {
					jQuery('.modal').modal('hide');
					formElm.html('<span class="mp_adding_to_cart">'+PRO_CS_Ajax.successMPMsg+'</span>');
					jQuery("div.mp_cart_widget_content").html(result[1]);
					jQuery(".cross-selling-modal div.cs-cart-info").html(result[3]);
					jQuery('#cross-selling-modal-' + result[2] ).modal('show');
					if (result[0] > 0) {
						formElm.fadeOut(1000, function(){
							formElm.html(tempHtml).fadeIn('fast');
							pro_cart_listeners();
						});
					} else {
						formElm.fadeOut(1000, function(){
							formElm.html('<span class="mp_no_stock">'+PRO_CS_Ajax.outMPMsg+'</span>').fadeIn('fast');
							pro_cart_listeners();
						});
					}
					pro_empty_cart(); //re-init empty script as the widget was reloaded
				}
			});
			return false;
		});
	}

	pro_cart_listeners();

});