/*
$(document).ready(function() {
    $('#store-create-form').formValidation({
        framework: 'bootstrap',
        fields: {
            user_name: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.UsernameReq
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: formVal.UsernameLength
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: formVal.UsernameChars
                    },
                    remote: {
                        message: formVal.UsernameNotAvail,					
						data: {
                            'action': 'request_username_validation'
                        },						
                        url: MP_Ajax.ajaxUrl,
                        type: 'POST'
                    }					
                }
            },	            
			user_email: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.EmailReq
                    },
                    emailAddress: {
                        message: formVal.EmailNotVal
                    },
                    remote: {
                        message: formVal.EmailNotAvail,
						data: {
                            'action': 'request_email_validation'
                        },
                        url: MP_Ajax.ajaxUrl,
                        type: 'POST'
                    }					
                }
            },	
			password_1: {
				row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.PassReq
                    },					
                    different: {
                        field: 'user_name',
                        message: formVal.PassNotTs
                    }
                }
            },
			password_2: {
                row: '.form-group',
                    validators: {
						notEmpty: {
							message: formVal.PassReq
						},						
                        identical: {
                            field: 'password_1',
                            message: formVal.PassMustTs
                        }
                    }
            },			
        }
    });
});
*/