$(document).ready(function() {
	$('form#new-post').formValidation({
        framework: 'bootstrap',
        fields: {
			        bbp_topic_title: {
			            validators: {
			                notEmpty: {
			                    message: 'Your topic needs a title.'

			                }
			            }
			        }			      
		}
    });
});