$(document).ready(function() {
	$( "input[name='submit-comment']" ).replaceWith( "<button type='submit' name='submit-comment' class='submit'>" +commentVal.pcomment+ "</button>" );	
    $('#comment-form form#pro-comment-form').formValidation({
        framework: 'bootstrap',
        fields: {
			        author: {
			            validators: {
			                notEmpty: {
			                    message: commentVal.namer

			                }
			            }
			        },
			        email: {
			            validators: {
			                notEmpty: {
			                    message: commentVal.emailr
			                },
			                regexp: {
			                    regexp: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
			                    message: commentVal.iemail
			                }
			            }
			        },
			        comment: {
		                validators: {
		                    notEmpty: {
		                        message: commentVal.reqc
		                    }
		                }

			        }
		}
    });
});