$(document).ready(function() {
	$('#comment-form #pro-comment-form .col-md-12').css('padding-left','0px');
	if ( $( "#pro-comment-form" ).length ) {
		document.getElementById("pro-comment-form").onsubmit = function () {
			if (!document.getElementById("comment").value) {
				$( "textarea#comment" ).after( '<small id="err-msg" class="help-block" data-fv-for="comment" data-fv-result="INVALID" style="">' + commentVal.reqc + '</small>' );
				$('textarea#comment').css('border','1px solid red');
				$('small#err-msg').css('color','red');
				$('small#err-msg').css('font-weight','bold');
				return false;
			}
			else {
				$( "input[name='submit-comment']" ).replaceWith( "<button disabled='disabled' name='submit-comment' class='submit'>" + commentVal.reqc + "</button>" );	
				return true;			
			}
		}
	}
});