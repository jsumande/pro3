$(document).ready(function() {
    $('#setupform').formValidation({
        framework: 'bootstrap',
		live: 'disabled',
        fields: {
            user_name: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.FieldReq
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: formVal.FieldLength
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: formVal.FieldChars
                    }				
                }
            },	            
			user_email: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.FieldReq
                    },
                    emailAddress: {
                        message: formVal.EmailNotVal
                    }					
                }
            },	
			/*
			user_password_1: {
				row: '.form-group',
                validators: {
                    different: {
                        field: 'user_name',
                        message: formVal.PassNotTs
                    }
                }
            },
			user_password_2: {
                row: '.form-group',
                    validators: {						
                        identical: {
                            field: 'user_password_1',
                            message: formVal.PassMustTs
                        }
                    }
            }	
			*/
            blogname: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.FieldReq
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: formVal.FieldLength
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: formVal.FieldChars
                    }
                }
            },
           blog_title: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.FieldReq
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: formVal.FieldLength
                    }
                }
            },	
			
			
           bcat_site_description: {
                row: '.form-group',
                validators: {
                    notEmpty: {
                        message: formVal.FieldReq
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: formVal.BlogdescLength
                    }
                }
            }			
        }
    });
});