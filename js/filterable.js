// Filterable jQuery

function scaleDown() {

	jQuery('#pro-product-grid .span6.well').removeClass('current').addClass('not-current');
	jQuery('#pro-product-grid .span4.well').removeClass('current').addClass('not-current');
	jQuery('#pro-product-grid .span3.well').removeClass('current').addClass('not-current');
	jQuery('ul.mpt-product-categories > li').removeClass('current-li');

}

function show(category) {

	scaleDown();

	jQuery('#' + category).addClass('current-li');
	jQuery('.' + category).removeClass('not-current');
	jQuery('.' + category).addClass('current');

	if (category == "all") {
		jQuery('ul.mpt-product-categories > li').removeClass('current-li');
		jQuery('#all').addClass('current-li');
		jQuery('#pro-product-grid .span6.well').removeClass('current, not-current').addClass('all');
		jQuery('#pro-product-grid .span4.well').removeClass('current, not-current').addClass('all');
		jQuery('#pro-product-grid .span3.well').removeClass('current, not-current').addClass('all');
	}

}

jQuery(document).ready(function(){

	jQuery('li#all').addClass('current-li');

	jQuery("ul.mpt-product-categories > li").click(function(){
		show(this.id);
	});

});