<?php 
/*
Template Name: Support Page
 */
 
//For email sending

if(isset($_REQUEST['support-submit'])){
	
	$post_status = false;
	
		$support_name 		= trim($_REQUEST['support_name']);
		$support_email 		= trim($_REQUEST['support_email']);
		$support_message	= trim($_REQUEST['support_message']);
		$support_about		= trim($_REQUEST['support_about']);
		$support_website	= trim($_REQUEST['support_website']);
		
		
		$support_pname = 'Blaga Vladinova';
		$support_position = 'Art Bulgaria consultant';
		$siteurl = get_site_url();
		$email_template = get_template_directory_uri().'/email-templates/';
		
		if(!empty($support_name) && !empty($support_email) && !empty($support_message) && !empty($support_about))
			$post_status = true;

		if(is_email($support_email ))
			$post_status = true;
		
		// get mainsite email
		if (function_exists('is_multisite') && is_multisite()) 
			$admin_email = get_site_option( 'admin_email' ); 
		else 
			$admin_email = get_option('admin_email');
		
		if($post_status){
			
			$subject = __('Support','pro').' - '.$support_about;
			$headers = array('Content-Type: text/html; charset=UTF-8','From: '.$support_name.' <'.$support_email.'>');		
			
			
			$facebook_url = esc_url(get_blog_option(1,'mpt_social_icon_1_url'));
			$twitter_url = esc_url(get_blog_option(1,'mpt_social_icon_12_url'));
			$gplus_url = esc_url(get_blog_option(1,'mpt_social_icon_2_url'));
			$linkedin_url = esc_url(get_blog_option(1,'mpt_social_icon_7_url'));
			$pinterest_url = esc_url(get_blog_option(1,'mpt_social_icon_8_url'));
			
			
			if( isset($_REQUEST["sendcopy"] )) {
				$to_copy = $support_email;
				$message_support = __('Thanks for reaching out to Art Bulgaria support.<br> If you have any questions or concerns, please dont hesitate to let me know.','pro').' <br><br>'.__('Your Message:','pro').'<br>'.$support_message.'<br><br>'.__('Regards:','pro').' '.$support_pname.' <br> '.$support_position;	
			
				include_once 'email-templates/support.php';	
			}
			
			$message_support_admin = __('Theres a new message support from','pro').' '.$support_name.' <br><br>'.$support_message;
			$support_name_admin = __('Admin','pro');
			$to = $admin_email;
			
			include_once 'email-templates/support-admin.php';
			
			if( isset($_REQUEST["sendcopy"] ))
				$status = wp_mail( $to_copy, $subject, $emailtemplate, $headers );
			
			$status = wp_mail( $to, $subject, $emailtemplate_admin, $headers );
			
			if($status)
				$post_status = true;
			else
				$post_status = false;
			
		}
	
} 

//FAQ 1601 - live 1580
$faqt_id = icl_object_id(1580, 'page', TRUE, ICL_LANGUAGE_CODE);
$faqt_link = get_blog_permalink( 1, $faqt_id );
 
get_header();
?>
<div id="page-wrapper">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div id="breadcrumb">
								<div class="row">
									 <div class="col-md-12">
										<ul class="breadcrumbs-list">
											<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>							
											<li class="active"><?php the_title(); ?></li>	
										</ul>
									 </div>
									</div>
							</div>
						<div class="clear padding10"></div>
							<?php $thecontent = get_the_content(); ?>						
									<div class="support-notif">
										<?php
											if (isset($post_status) && $post_status) echo '<div class="alert alert-success" role="alert">'.__('Thank you for your support! We will contact you as soon as we can.','pro').'</div><br>';
											elseif ( isset($post_status) && !$post_status ) echo '<div class="alert alert-danger" role="alert">'.__('Message was not sent.','pro').'</div> <br>';
											else echo '<div class="well " style="padding: 10px 20px;  font-weight: bolder;  color: rgb(51, 51, 51); letter-spacing: .3px;">'.__('How can you help?','pro').'</div>';
										?>
									</div>								
									<div class="page-content">
										<div id="page-support">
											<div id="header-support" class="text-center">
											<h4><?php echo sprintf(__('You may first check our<br>Frequently asked questions > <a href="%s">here</a>','pro'),$faqt_link); ?></h4>
											
											<small><?php _e('Please fill the form and we will contact you as soon as possible.','pro'); ?></small>
											</div>
											<br>
											<form id="support" method="post" action="<?php echo get_permalink( $post->ID ); ?>">
												<div class="row">
												  <div class="col-md-6">
														<!--
														<div class="form-group">
															<label for=""><span class="required">*</span> Name</label>
															<input name="support_name" type="text" class="form-control" id="" placeholder="">
														</div>
														-->
														<div class="form-group<?php if(isset($support_name) && empty($support_name)) echo ' has-error'; ?>">
															<label class=""><span class="required">*</span> <?php _e('Name','pro'); ?></label>
															<div class="messageContainer help-block"><?php if(isset($support_name) && empty($support_name)) { ?><small class="help-block" ><?php _e('The name is required','pro'); ?></small><?php } ?></div>
															
															<div class="sup-field">												
																<input type="text" class="form-control" name="support_name" />
															</div>																
														</div>
												  </div>
												  <div class="col-md-6">
														<div class="form-group<?php if(isset($support_email) && empty($support_email)) echo ' has-error'; ?>">
															<label for=""><span class="required">*</span> <?php _e('E-mail','pro'); ?></label>
															<div class="messageContainer help-block"><?php if(isset($support_email) && empty($support_email)) { ?><small class="help-block" ><?php _e('The email id required','pro'); ?></small><?php } ?>	</div>
															<div class="sup-field">
															<input name="support_email" type="text" class="form-control" id="" placeholder="">
															</div>
														</div>
												  </div>
												</div>
												<div class="row">
												  <div class="col-md-6">
														<div class="form-group">
															<label for=""><span class="required" style="color: transparent;   margin-right: -10px;">*</span> <?php _e('Web site','pro'); ?></label>
															<input name="support_website" type="text" class="form-control" id="" placeholder="">
														</div>
												  </div>
												  <div class="col-md-6">
														<div class="form-group<?php if(isset($support_about) && empty($support_about)) echo ' has-error'; ?>">
															<label for=""><span class="required">*</span> <?php _e('Its about','pro'); ?></label>
															<div class="messageContainer help-block"><?php if(isset($support_about) && empty($support_about)) { ?><small class="help-block" ><?php _e('About is required','pro'); ?></small><?php } ?>	</div>
															<div class="sup-field">
															<input name="support_about" type="text" class="form-control" id="" placeholder="">
															</div>
														</div>
												  </div>
												</div>
												<div class="row">
												  <div class="col-md-12">
														<div class="form-group<?php if(isset($support_message) && empty($support_message)) echo ' has-error'; ?>">
															<label for=""><span class="required">*</span> <?php _e('Message','pro'); ?></label>
															<div class="messageContainer help-block"><?php if(isset($support_message) && empty($support_message)) { ?><small class="help-block" ><?php _e('Message is required','pro'); ?></small><?php } ?></div>
															<div class="sup-field">
															<textarea name="support_message" class="form-control"></textarea>
															</div>
														</div>
												  </div>
												</div>
												<div class="row support-btns">
												  <div class="col-md-6">
														<div class="form-group">
														  <label class="control-label" for=""></label>
															<div class="input-control-checkbox">	
															<input checked="checked" id="" type="checkbox" name="sendcopy" value="1"><label for=""><?php _e('Send copy in my email.','pro'); ?></label>
															</div>
														</div>
												  </div>
												  <div class="col-md-6">
												        <?php wp_nonce_field( 'support_form_query', 'support_form_nounce' ); ?>
														<button id="support-submit" type="submit" name="support-submit" class="submit-btn pull-right"><?php _e('SEND','pro'); ?></button>
												  </div>
												</div>
											  
											</form>
											<!---
											<div id="support-info" class="row">
												<div class="col-md-12 text-center wrapp">
												<h2><i class="fa fa-phone"></i></h2>
												<h4><?php _e('OR BY TELEPHONE','pro'); ?></h4>
												<small><?php _e('Toll Free','pro'); ?>: +1-800-342-0620</small>
												<br>
												<small><?php _e('Local','pro'); ?>: +1-813-489-224</small>
												</div>
											</div>							
											--->
										</div>
										
									</div>
						</div>	
									<?php //edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>
					</div>
				</div>
			</div>	
		</div>		
	<?php endwhile; endif; ?>
</div>
<?php 
get_template_part('footer', 'widget');
get_footer(); 