	<!-- Footer section-->
	<div class="footer-wrapper">
		<div class="outercontainer">
			<div class="container">
				<div class="row-fluid">
					<input type="hidden" id="langCurrentPage" value="<?= '?lang='.ICL_LANGUAGE_CODE ?>">
					<div style="display:none">
						<select id="datePickerMonthBg" >
							<option value="0">Януари</option><option value="1">Февруари</option><option value="2">Март</option><option value="3">Април</option><option value="4">Май</option><option value="5">Юни</option><option value="6">Юли</option><option value="7">Август</option><option value="8">Септември</option><option value="9">Октомври</option><option value="10">Ноември</option><option value="11">Декември</option>
						</select>
					</div>
					<div class="col-md-4 col-sm-4 pull-left">
						<ul class="social-links">
							<?php //mpt_social_icon_section(); ?>
						</ul>
					</div><!-- End span -->
					<div class="col-md-8 col-sm-8">
						<div class="pull-right">
							<?php// mpt_load_footer_text(); ?>
						</div>
					</div><!-- End span -->
					<div class="col-md-12 copywrite">
						<p>&copy;<?php echo date('Y'); ?> <?php _e('Artbulgaria.com. All rights reserved.'); ?></p>
					</div><!-- End span -->
				</div><!-- End row-fluid -->
			</div><!-- End container -->
		</div><!-- End outercontainer -->
		<div class="clear"></div>
	</div>
	<!-- End Footer section -->
	<?php mpt_load_body_code(); ?>
	<?php do_action('tp_custom_footer_script_top');?>
	<?php wp_footer();?>
	<?php get_template_part('js') ?>
	<?php
		if( !isset($_GET['lang']) && $_GET['lang']!="en" ) {
			echo '
				<script>
					jQuery(document).ready(function(){
						jQuery("#tr_eventdate_div span.input-group-btn, .daterangepicker .table-condensed td").live("click", function() {
						   var datepicker = jQuery("select#datePickerMonthBg").html();
						   jQuery(".daterangepicker .calendar .monthselect").html(datepicker);
						   jQuery("#tr_eventdate_div").attr("state","true");
						});
					});
				</script>
			';
		}
	?>	
	<script type="text/javascript">
	jQuery(document).ready(function(){
		
		<!-- Tool Tip -->
		jQuery('[data-toggle="tooltip"]').tooltip({
			'placement': 'top'
		});
		<!-- Tool Tip End -->
		
		<!-- Pagination Add Class -->
		jQuery("ul.page-numbers").addClass("pagination");
		<!-- Pagination End -->		
		
		<!-- Tool Tip popover-->
		jQuery('.page-content [data-toggle="tooltip"]').popover({
			trigger: 'hover','placement': 'top', 'color': "red"
		});
		<!-- Tool Tip End-->
		<?php 
			$new_l = $_SERVER["REQUEST_URI"]; 
			if(strstr($new_l,'/manage-store/')) {
				if(!defined('COMPOSE_EVENT')){
		?>
		

		jQuery("#save-action-trigger").click(function(e){
			<!-- Tool Multiple Image Uploader-->
			var testervar = jQuery('[id^="toprank_multi_images"]').find('.toprank_image_input');
			var pricevar = jQuery('tr.variation .mp_price_col').find('input');
			var titlevar = jQuery('input#title').val();
			if (testervar.length == 0 || pricevar.val() == '0.00' || pricevar.val() == '0' || pricevar.val() == '' || titlevar == '') {
				
				
				if(testervar.length == 0) {
					
					if(jQuery("#miu_error").length == 0) {
						jQuery('#con_multi_images').after('<div id="miu_error" style="padding:10px 0;color:#ff0000">'+mui_lang_required_image+'</div>');
					}
					
					jQuery('html, body').animate({scrollTop: jQuery("#tr_mui_div").offset().top}, 800);
				
				}
				
				if(pricevar.val() == '0.00' || pricevar.val() == '0' || pricevar.val() == ''){
					
					if(jQuery("#miu_error_price").length == 0) {
						jQuery('.mp_price_col input').after('<div id="miu_error_price" style="color:#ff0000">'+mui_lang_required_price+'</div>');
					}				
					jQuery('html, body').animate({scrollTop: jQuery("#mp-meta-details").offset().top}, 800);
				}
				
				if(titlevar == ''){
					
					if(jQuery("#miu_error_title").length == 0) {
						jQuery('input#title').after('<div id="miu_error_title" style="color:#ff0000">'+mui_lang_required_title+'</div>');
					}				
					jQuery('html, body').animate({scrollTop: jQuery("#titlewrap").offset().top}, 800);
				}			
				
				e.preventDefault();
				return false;
				
			} else {
				var valueSelected = jQuery("#minor-publishing input[type='radio']:checked").val();
				if (valueSelected == 1) {
				   jQuery("#minor-publishing #publish").click();
				}
				if(valueSelected == 2) {
					jQuery("#minor-publishing #save-post").click();
				}
			}
		});	
		<!-- Tool Multiple Image Uploader End -->
		<!-- Require Price -->
		jQuery(".mp_price_col input").on("input", function(){
			
			if($(this).val() == '0.00' || $(this).val() == '0' || $(this).val() == ''){
				if(jQuery("#miu_error_price").length == 0) {
					$(this).after('<div id="miu_error_price" style="color:#ff0000">'+mui_lang_required_price+'</div>');
				}				
			} else {
				jQuery("#miu_error_price").remove();
			}
			
		});	
		<!-- Require Price End-->
				<?php } else { ?>
		jQuery("#save-action-trigger").click(function(e){
			var titlevar = jQuery('input#title').val();
			var testervar = jQuery('[id^="set-post-thumbnail"]').find('img');
			if (titlevar == '' || testervar.length == 0) {
											
				if(titlevar == ''){
					
					if(jQuery("#miu_error_title").length == 0) {
						jQuery('input#title').after('<div id="miu_error_title" style="color:#ff0000">'+mui_lang_required_title+'</div>');
					}				
					jQuery('html, body').animate({scrollTop: jQuery("#titlewrap").offset().top}, 800);
				}	

				if(testervar.length == 0) {
					
				if(jQuery("#miu_error").length == 0) {
						jQuery('a#set-post-thumbnail').after('<div id="miu_error" style="padding:10px 0;color:#ff0000">'+mui_lang_required_image+'</div>');
					}
					
					jQuery('html, body').animate({scrollTop: jQuery("#tr_mui_div").offset().top}, 800);
				
				}				
				
				e.preventDefault();
				return false;
				
			} else {
				var valueSelected = jQuery("#minor-publishing input[type='radio']:checked").val();
				if (valueSelected == 1) {
				   jQuery("#minor-publishing #publish").click();
				}
				if(valueSelected == 2) {
					jQuery("#minor-publishing #save-post").click();
				}
			}
			
		});	
		var year = moment().year();
		<?php     
			global $appointments;
			$time_increment = $appointments->options["min_time"]; 
			
			
			
			if(ICL_LANGUAGE_CODE == 'bg'){
				
		?>
			$('#daterangetoggler').daterangepicker({
				timePicker: true,
				timePicker24Hour: true,
				timePickerIncrement: <?php echo $time_increment; ?>,
				singleDatePicker: true,
				showDropdowns: true,
				opens: 'right'
			},
			function (start, end, label) {
				$('input[name="mp_event_start"]').val(start.format('MM/DD/YYYY H:mm'));
			}).on('show.daterangepicker', function (ev, picker) {
				$('.yearselect option').filter(function() {
					return $(this).val() < year;
				}).remove();
				
				//$('.hourselect option').filter(function() {
				//	return $(this).val() == 0;
				//}).remove();
				//$('<option value="24">24</option>').insertAfter('.calendar-time .hourselect option[value="23"]');
				
			}).on('showCalendar.daterangepicker', function (ev, picker) {
				$('.yearselect option').filter(function() {
					return $(this).val() < year;
				}).remove();
			});

			$('#daterangetoggler2').daterangepicker({
				timePicker: true,
				timePicker24Hour: true,
				timePickerIncrement: <?php echo $time_increment; ?>,
				singleDatePicker: true,
				showDropdowns: true,
				opens: 'right'
			},
			function (start, end, label) {
				$('input[name="mp_event_end"]').val(start.format('MM/DD/YYYY H:mm'));
			}).on('show.daterangepicker', function (ev, picker) {
				$('.yearselect option').filter(function() {
					return $(this).val() < year;
				}).remove();
			}).on('showCalendar.daterangepicker', function (ev, picker) {
				$('.yearselect option').filter(function() {
					return $(this).val() < year;
				}).remove();
			});			
			<?php } else { ?>
			
			$('#daterangetoggler').daterangepicker({
				timePicker: true,
				timePickerIncrement: <?php echo $time_increment; ?>,
				singleDatePicker: true,
				showDropdowns: true,
				opens: 'right'
			},
			function (start, end, label) {
				$('input[name="mp_event_start"]').val(start.format('MM/DD/YYYY hh:mm a'));
			}).on('show.daterangepicker', function (ev, picker) {
				$('.yearselect option').filter(function() {
					return $(this).val() < year;
				}).remove();
			}).on('showCalendar.daterangepicker', function (ev, picker) {
				$('.yearselect option').filter(function() {
					return $(this).val() < year;
				}).remove();
			});

			$('#daterangetoggler2').daterangepicker({
				timePicker: true,
				timePickerIncrement: <?php echo $time_increment; ?>,
				singleDatePicker: true,
				showDropdowns: true,
				opens: 'right'
			},
			function (start, end, label) {
				$('input[name="mp_event_end"]').val(start.format('MM/DD/YYYY hh:mm a'));
			}).on('show.daterangepicker', function (ev, picker) {
				$('.yearselect option').filter(function() {
					return $(this).val() < year;
				}).remove();
			}).on('showCalendar.daterangepicker', function (ev, picker) {
				$('.yearselect option').filter(function() {
					return $(this).val() < year;
				}).remove();
			});		
			
			<?php } 
			
			} ?>
		<!-- Require Title-->
		jQuery("input#title").on("input", function(){
			
			if($(this).val() == ''){
				if(jQuery("#miu_error_title").length == 0) {
					$(this).after('<div id="miu_error_title" style="color:#ff0000">'+mui_lang_required_title+'</div>');
				}				
			} else {
				jQuery("#miu_error_title").remove();
			}
			
		});	
		<!-- Require Title End-->
		<!-- PRODUCT CATEGORY LIST DOM CHANGE -->
		jQuery("#product_categorychecklist li label input, #product_categorychecklist-pop  li label input").each(function(index, value) { 
			var listId = $(this).attr('id'); 
			jQuery(this).insertBefore($(this).parent())
		});
		<!-- PRODUCT CATEGORY LIST DOM CHANGE End-->
		<?php } 
		
			if(!is_main_site()) { 
			global $appointments;
			$time_increment = $appointments->options["min_time"]; 
			
			if(ICL_LANGUAGE_CODE == 'bg'){	?>
				var year = moment().year();
				$('#booking-toggler').daterangepicker({
					timePicker: true,
					timePicker24Hour: true,
					timePickerIncrement: <?php echo $time_increment; ?>,
					singleDatePicker: true,
					showDropdowns: true,
					opens: 'center',
					drops: 'up'
				},
				function (start, end, label) {
					$('input[name="b-date"]').val(start.format('MM/DD/YYYY H:mm'));
				}).on('show.daterangepicker', function (ev, picker) {
					$('.yearselect option').filter(function() {
						return $(this).val() < year;
					}).remove();
					
					//$('.hourselect option').filter(function() {
					//	return $(this).val() == 0;
					//}).remove();
					//$('<option value="24">24</option>').insertAfter('.calendar-time .hourselect option[value="23"]');
					
				}).on('showCalendar.daterangepicker', function (ev, picker) {
					$('.yearselect option').filter(function() {
						return $(this).val() < year;
					}).remove();
				});				
				
			<?php } else { ?>
				$('#booking-toggler').daterangepicker({
					timePicker: true,
					timePickerIncrement: <?php echo $time_increment; ?>,
					singleDatePicker: true,
					showDropdowns: true,
					opens: 'center',
					drops: 'up'
				},
				function (start, end, label) {
					$('input[name="b-date"]').val(start.format('MM/DD/YYYY hh:mm a'));
				}).on('show.daterangepicker', function (ev, picker) {
					$('.yearselect option').filter(function() {
						return $(this).val() < year;
					}).remove();
				}).on('showCalendar.daterangepicker', function (ev, picker) {
					$('.yearselect option').filter(function() {
						return $(this).val() < year;
					}).remove();
				});			
			
			<?php } ?>
				$('#b-hours').on('change', function() {
					var bprice = $('.booking-price-rate').text();
					var bcurrency = $('.booking-currency').text();
					if(bprice){
						var btotal = (parseInt(this.value) * parseInt(bprice)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						$('.booking-total-fee').html(bcurrency+' '+btotal);
					}
				});
				
				$.fn.clearForm = function() {
				  return this.each(function() {
					var type = this.type, tag = this.tagName.toLowerCase();
					if (tag == 'form')
					  return $(':input',this).clearForm();
					if (type == 'text' || type == 'password' || tag == 'textarea')
					  this.value = '';
					else if (type == 'checkbox' || type == 'radio')
					  this.checked = false;
					else if (tag == 'select')
					  this.selectedIndex = -1;
				  });
				};				
								
				$('#booking-submit').click(function() {
					$(this).prop("disabled",true);
					var dataform = {
						'action': 'tp_ajax_booking_request',
						'b-name': $("#b-name").val(),
						'b-email': $("#b-email").val(),
						'b-phone': $("#b-phone").val(),
						'b-address': $("#b-address").val(),
						'b-country': $("#b-country").val(),
						'b-city': $("#b-city").val(),
						'b-state': $("#b-state").val(),
						'b-date': $("#b-date").val(),
						'b-hours': $("#b-hours").val(),
						'b-note': $("#b-note").val()
					};					
					$('.booking-message').html('<div class="alert alert-info"><?php _e('Please Wait. Sending..','pro'); ?></div>');
					$.ajax({url: ajaxurl, data : dataform, dataType: 'json', type: 'POST', cache: false, success : function(response) {
							var json = response;
							if(json.berror){
								$('.booking-message').html('<div class="alert alert-danger">'+json.bmessage+'</div>');
								$('#booking-submit').prop("disabled",false);
							} else {
								$('#booking-submit').prop("disabled",false);
								$('.booking-message').html('<div class="alert alert-success">'+json.bmessage+'</div>');
								$("#booking").clearForm();
							}													
						}
					});
					
				});
			<?php }	
		
			if(strstr($new_l,'/forums/') || strstr($new_l,'/forums')) {
		?>
		<!-- bbpress Pagination -->
		jQuery( "ul.bbp-topics" ).after($(".bbp-pagination").addClass("second"));
		jQuery("div.bbp-pagination:nth-last-child(2n)").hide();
		<!-- bbpress Pagination End-->
		<?php } ?>
	});
	<!-- media query event handler  used in jquery.touchcarousel-->
	if (matchMedia) {
		var mq = window.matchMedia("(max-width: 1079px)");
		mq.addListener(WidthChange);
		WidthChange(mq);
	}
	// media query change
	function WidthChange(mq) {
		if (mq.matches) {
			jQuery("#cat-arrow-left").css("display", "block");
			jQuery("#cat-arrow-right").css("display", "block");
		}
		else {
			jQuery("#cat-arrow-left").css("display", "none");
			jQuery("#cat-arrow-right").css("display", "none");
		}
	}
	<!-- media query event handler end-->
	/* center modal */
	function centerModals(){
	  $('.modal').each(function(i){
		var $clone = $(this).clone().css('display', 'block').appendTo('body');
		var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
		top = top > 0 ? top : 0;
		$clone.remove();
		$(this).find('.modal-content').css("margin-top", top);
	  });
	}
	$('.modal').on('show.bs.modal', centerModals);
	$(window).on('resize', centerModals);	
	</script>
<?php do_action('tp_custom_footer_script');?>

	<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-54359469-1', 'auto');
	ga('set', 'forceSSL', true);
	ga('send', 'pageview');
	</script>
</body>
</html>