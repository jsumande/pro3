<?php get_header(); ?>
	<!-- Page -->
	<div id="page-wrapper">

		<?php
			global $wp_query;
			$termname = $wp_query->queried_object->name;
			$termslug = $wp_query->queried_object->slug;
		?>


		<div class="content-section">
			<div class="outercontainer">
				<div class="container" style="min-height: 450px;">
					
					<div class="row no-margin">
						<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="span12">
							<div id="breadcrumb">
							<div class="row">
								 <div class="col-md-12">
									<ul class="breadcrumbs-list">
										<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>		
										<li><a href="<?php echo add_lang_vars_custom($main_site_url.'/store/products/'); ?>"><?php _e('Stores', 'pro'); ?></a></li>	
										<li class="active"><span><!--<?php _e('Items in ' , 'pro' ); ?>-->&#8216;<?php echo $termname; ?>&#8217;</span></li>
									</ul>
								 </div>
								</div>
							</div>

							<div class="clear padding10"></div>	

							<?php echo do_shortcode('[toprank_mpdgfilters category="'.$termslug.'|false"]'); ?>

						</div><!-- / span8 -->
					</div><!-- / col-md's -->
					</div><!-- / row no-margin -->

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>