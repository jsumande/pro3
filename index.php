<?php 

get_header(); 

	if(is_main_site()) 
			do_action('home_page_content');
		else  
			do_action('subsite_home_page_content'); 
 
		$selected = get_blog_option(1,'mpt_enable_homepage_footer_widget');
		if ($selected) {
			get_template_part('footer', 'widget'); 
		}
		
get_footer(); 

?>