<?php 
/*
Template Name: Page (Stores)
 */

get_header(); ?>

	<!-- Page -->
	<div id="page-wrapper">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php 
						$contentbgcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_bg_color', true ));
						$contenttextcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_text_color', true ));
					?>

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">
					<div class="row no-margin">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div id="post-<?php the_ID(); ?>" <?php post_class(); ?><?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>
									<div id="breadcrumb">
										<div class="row">
											 <div class="col-md-12">
												<ul class="breadcrumbs-list">
													<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
													<?php 
														$breadcrumb_title  = '';
														 if ( get_query_var('pagename') == 'stores'  && ICL_LANGUAGE_CODE =='bg' ) {
														    $breadcrumb_title =  __( "Магазини" , 'pro' );
														 } else {
														     $breadcrumb_title =  __( get_the_title() , 'pro' );
														 }
													?>
													<li class="active"><?php echo $breadcrumb_title;?></li>
												</ul>
											 </div>
										</div>
									</div>
									
										<div class="clear padding10"></div>

										<div class="page-content">
											<div class="row">
												<div class="col-md-12">							
													<?php 
														$terms = get_terms('bcat',array('hide_empty'=>false,'parent' => 0, 'exclude'=>array(27,60)));
														$in_terms_slug = array();					
														foreach($terms as $term){
															$in_terms_slug[] =  urldecode($term->slug);
														}
														$in_terms_slug_new = implode(',',$in_terms_slug);
														echo do_shortcode('[toprank_mpdgfilters category="'.$in_terms_slug_new.'|true" store="1"]'); 
													?>													
												</div>
											</div>
											<script>console.log('<?php echo $in_terms_slug_new; ?>'); </script>
											<?php // edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>

										</div>

										<?php wp_link_pages(array('before' => '<p class="margin-vertical-15">' . __( 'Pages:' , 'pro' ) , 'after' => '</p>' , 'next_or_number' => 'number')); ?>

										<?php 

											$showcomments = get_post_meta( $post->ID, '_mpt_page_show_comments', true );

											if ( $showcomments == 'on') {

												echo '<div class="page-comments">';
												comments_template();
												echo '</div>';
											} 
										?>

								</div>

								<?php endwhile; endif; ?>

					</div><!-- / col-md-'s -->
				</div><!-- / row no-margin -->
				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>