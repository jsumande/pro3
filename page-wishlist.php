<?php 
/*
Template Name: Page (wishlist)
 */
get_header(); ?>
	<!-- Page -->
	<div id="page-wrapper" class="wishlist-page">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php
						$btnclass = mpt_load_mp_btn_color();
						$iconclass = mpt_load_whiteicon_in_btn();
						$id = get_the_ID();
						$contentbgcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_bg_color', true ));
						$contenttextcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_text_color', true ));
					?>

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">
					<div class="row no-margin">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div id="post-<?php echo $id; ?>" <?php post_class(); ?><?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>
								<div id="breadcrumb">
									<div class="row">
										 <div class="col-md-12">
											<ul class="breadcrumbs-list">
												<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
												<?php 
													$breadcrumb_title  = '';
													 if ( get_query_var('pagename') == 'wishlist'  && ICL_LANGUAGE_CODE =='bg' ) {
													    $breadcrumb_title =  __( "Избрани продукти" , 'pro' );
													 } else {
													     $breadcrumb_title =  __( get_the_title() , 'pro' );
													 }
												?>
												<li class="active"><?php echo $breadcrumb_title;?></li>
											</ul>
										 </div>
										</div>
								</div>
								<div class="clear padding10"></div>
							</div>

									<div class="page-content">

										<?php
											$content = get_the_content(); 
											if ( !empty($content) ) {
												the_content();
												echo '<div class="clear padding10"></div>';
											}
										?>

										<?php 
											global $mppf, $mppf_wishlist;
											$enablewishlist = ( class_exists('MPPremiumFeatures') ? $mppf->load_option( $mppf->option_group . '_wishlist_enable_wishlist' , 'no' ) : 'no' );
										?>
										<?php if ( $enablewishlist == 'yes' ) : ?>

											<?php if ( is_user_logged_in() ) : ?>
											<form action="" method="get">
												<div id="search-product" class="input-append">
													<input type="text" class="input-medium search-product" value="<?php echo (isset($_GET['search'])? esc_attr($_GET['search']) : ''); ?>" name="search" id="post-search-input" placeholder="<?php _e('Search wish product'); ?>">
												  <button id="search-submit" type="submit" class="btn"><i class="fa fa-search"></i></button>
												</div>
											</form>	
												<?php 
													//$hideprice = get_option('mpt_mp_hide_product_price');
													$hideprice = get_blog_option(1,'mpt_mp_hide_product_price');
													echo ( class_exists('MPPremiumFeatures_WishList') ? $mppf_wishlist->load_wishlist_table( array( 'showtitle' => false , 'title' => '' , 'hideprice' => ( $hideprice == 'true' ? true : false ) , 'btnclass' => $mppf->get_btn_color() ) ) : '' );
												?>
											<?php else : ?>
													<style>
													div#mp-premium-features, div.padding10, div#wpcr_respond_1 { display: none; }
													</style>
												<?php $currentpage = get_permalink( $id ); ?>
												<div class="wish-login-link"><?php _e( 'Kindly ' , 'pro' ); ?><a href="<?php echo wp_login_url( $currentpage ); ?>"><?php _e( 'login' , 'pro' ); ?></a><?php _e( ' to view your wish list.' , 'pro' ); ?></div>

											<?php endif; ?>

										<?php endif; ?>
										
										<?php //edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>

									</div>

									<?php wp_link_pages(array('before' => '<p class="margin-vertical-15">' . __( 'Pages:' , 'pro' ) , 'after' => '</p>' , 'next_or_number' => 'number')); ?>

									<?php 

										$showcomments = get_post_meta( $post->ID, '_mpt_page_show_comments', true );
										if ( $showcomments == 'on') {
											echo '<div class="page-comments">';
											comments_template();
											echo '</div>';
										} 
									?>

							</div> <!-- / col-md-s -->
						</div> <!-- / row no-margin -->
					</div>

					<?php endwhile; endif; ?>

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>
<?php get_footer(); ?>