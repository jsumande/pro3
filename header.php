<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title><?php mpt_load_site_title(); ?></title>
	<meta name="description" content="<?php mpt_load_meta_desc(); ?>" />
	<meta name="keywords" content="<?php mpt_load_meta_keywords(); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="p:domain_verify" content="fe6d3c5f56c325af6812d68f7dc9834b"/>
	<meta name="language" content="<?php echo ICL_LANGUAGE_CODE; ?>"/>
	<?php mpt_load_google_web_font( true ); ?>	
	<link rel="shortcut icon" href="<?php mpt_load_favicon(); ?>" /> 
	<link href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" rel="stylesheet" />
	<?php wp_head(); ?>
	<?php //include(get_template_directory() . '/admin/custom-styles.php'); ?>
<?php if (is_page_template('login.php')):{?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery(".tab_content_login").hide();
		jQuery("ul.tabs_login li:first").addClass("active_login").show();
		jQuery(".tab_content_login:first").show();
		jQuery("ul.tabs_login li").click(function() {
			jQuery("ul.tabs_login li").removeClass("active_login");
			jQuery(this).addClass("active_login");
			jQuery(".tab_content_login").hide();
			var activeTab = jQuery(this).find("a").attr("href");
			if (jQuery.browser.msie) {jQuery(activeTab).show();}
			else {jQuery(activeTab).show();}
			return false;
		});
	});
</script>
<?php } endif; ?>	
	<!--[if gte IE 9]>
		<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<style type="text/css">.gradient {filter: none;}</style>
	<![endif]-->
	<?php mpt_load_header_code(); ?>
</head>
<body id="body-wrapper" <?php body_class(); ?>>
	<div style="display:none">
	<a href="https://plus.google.com/104456919019666398558" rel="publisher">Google+</a>
	</div>
	<!-- Floating Menu -->
	<?php 
		if ( class_exists('MarketPress') )
			pro_floating_menu(); 
	?>
	<!-- End Floating Menu -->
	<!-- Header Section -->	
	<!--<?php //mpt_load_header_style(); ?>-->
    <!-- Header Section -->
	<?php 
	// Get all vars
	global $current_user, $sitepress;
	$default_lang = $sitepress->get_default_language();
	$main_site_url = get_site_url(1); 
	$top_rank_var = get_query_var('tp_action'); 

	$mystoreid = get_my_store_id();
	$issuperadmin = is_super_admin();
	$sub_site_url = '';								
	if(!$issuperadmin){
		if($mystoreid)
			$sub_site_url = get_site_url($mystoreid,'','http');
	}								
	//Pages
	
	//stores
	$store_id = icl_object_id(839, 'page', TRUE, ICL_LANGUAGE_CODE);
	$store_link = set_url_scheme(get_blog_permalink( 1, $store_id ),'http');
	
	$blgt_id = icl_object_id(2313, 'page', TRUE, ICL_LANGUAGE_CODE);
	$blogs_link = set_url_scheme(get_blog_permalink( 1, $blgt_id ),'http');
	
	//products
	$prop_id = icl_object_id(1179, 'page', TRUE, ICL_LANGUAGE_CODE);
	$prop_link = set_url_scheme(get_blog_permalink( 1, $prop_id ),'http');

	//artists
	$artist_id = icl_object_id(2928, 'page', TRUE, ICL_LANGUAGE_CODE);
	$artist_link = set_url_scheme(get_blog_permalink(1, $artist_id),'http');

	if (empty($artist_link)) {
		$artist_link = set_url_scheme(get_term_link('performers', 'product_category'),'http');
	}
	
	$regst_id = icl_object_id(96, 'page', TRUE, ICL_LANGUAGE_CODE);
	$regst_link = set_url_scheme(get_blog_permalink( 1, $regst_id ),'https');
	
	$wish_id = icl_object_id(1594, 'page', TRUE, ICL_LANGUAGE_CODE);
	$wish_link = set_url_scheme(get_blog_permalink( 1, $wish_id ),'http');
	
	//$pfct_id = icl_object_id(27, 'performers', TRUE, ICL_LANGUAGE_CODE);
	//$pfct_link = get_translated_term(27,'bcat',ICL_LANGUAGE_CODE);	
	$pfct_link = '';
	
	//get store vars
	$tp_global_page_settings = (!get_site_transient( 'cache_global_page_settings' ))? get_site_transient( 'cache_global_page_settings' ) : get_site_option( 'tp_global_page_settings' );
	$page_en = $tp_global_page_settings['manage_store_slug_en'];
	$page_bg = $tp_global_page_settings['manage_store_slug_bg'];
	$manage_store_slug = $page_en;
	
	if(ICL_LANGUAGE_CODE=='bg')
		$col_adj = ' headerl-bg';
	else
		$col_adj = '';
	
	?>
    <div class="header-wrapper">
    <div id="header-wrapper">
       <div class="container">
            <div class="site-branding<?php echo $col_adj; ?>">
                <div class="col-md-4 col-sm-4 col-xs-5">
                    <div class=" header-top-right-menu pull-left">
                        <ul class="hl-link-list">
                            <li><a href="<?php echo $store_link; ?>"><?php _e('Stores'); ?></a>
                            </li>
                            <li class="hl-seperator">|</li>
                            <li><a href="<?php echo $artist_link; ?>"><?php _e('Performers'); ?></a>
                            </li>
                            <li class="hl-seperator">|</li>
                            <li><a href="<?php echo $blogs_link;?>"><?php _e('Blog'); ?></a>
                            </li>
                            <li class="hl-seperator">|</li><a href="<?php echo set_url_scheme(add_lang_vars_custom($main_site_url.'/forums/'),'http');?>"><?php _e('Forum'); ?></a>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-7">
                <div class="tool-menu nav-top-right header-top-right-menu pull-right">
                    <ul class="hl-link-list">
						<?php if(!is_user_logged_in()): ?>
						
							
							<li><a href="<?php echo add_lang_vars_custom(network_site_url('/user-login/', 'https')); ?>"><?php _e('Login'); ?></a></li>
							<li class="hl-seperator">|</li>
							<li><a href="<?php echo add_lang_vars_custom(network_site_url('/user-register/', 'https')); ?>"><?php _e('Register'); ?></a></li>
							<li class="hl-seperator">|</li>
							<li><a href="<?php echo $wish_link; ?>"><?php _e('Wish list'); ?></a></li>
						<?php else: ?>
								<li><?php  _e('Hello'); ?>, <span class="hl-name"><?php echo $current_user->display_name; ?></span> <a href="<?php echo localize_url(wp_logout_url(home_url())); ?>">(<?php _e('Logout'); ?>)</a></li><li class="hl-seperator">|</li>
								<?php if(!$mystoreid && !$issuperadmin): ?>
									<li><a href="<?php echo add_lang_vars_custom($main_site_url.'/user-profile/'); ?>"><?php _e('Profile'); ?></a></li>
								<?php else:
								$ms_url = 'allproduct';
								if(!is_main_site()) {
									
									$bcat_site_categories = get_option('bcat_site_categories');									
									if(!empty($bcat_site_categories)) {
										$bcat_site_categories = unserialize($bcat_site_categories);
																								
										if(is_array($bcat_site_categories)) {
											
											if((array(27,60) === $bcat_site_categories) || (array(60,27) === $bcat_site_categories)) {
												$ms_url = 'allevents';
											} elseif((array(60) == $bcat_site_categories) || (array(27) == $bcat_site_categories)) {
												$ms_url = 'allevents';						
											} elseif(in_array(27, $bcat_site_categories) || in_array(60, $bcat_site_categories)) {
												$ms_url = 'allevents';
											}	
											
										} elseif($bcat_site_categories == 27 || $bcat_site_categories == 60) {
											$ms_url = 'allevents';
										}
									}
									
								}
								?>	
									
									<li><a href="<?php echo localize_url($sub_site_url.'/'.$manage_store_slug.'/?mpfe_page='.$ms_url); ?>"><?php _e('Manage Store'); ?></a></li>							
								<?php endif; ?>
								<li class="hl-seperator">|</li>
								<li><a href="<?php echo add_lang_vars_custom($main_site_url.'/members/'.$current_user->user_login.'/messages/inbox/'); ?>"><?php _e('Messages'); ?></a></li>
								<li class="hl-seperator">|</li>
								<li><a href="<?php echo $wish_link; ?>"><?php _e('Wish list'); ?></a></li>
						<?php endif;?>
                        <li class="hl-seperator">|</li>
                        <li>
                            <div class="header-menu-cart">
                                <div>
                                   <?php echo do_shortcode('[mpawc design="link" cartstyle="modal"]'); ?>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

                </div>
            </div>
       </div>
    </div>
        <div id="page" class="hfeed site">
            <header id="masthead" class="site-header" role="banner">
                <div class="full-width-blue">
                    <div class="container">
                        <div class="site-branding ">
						<div class="row">
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <div class="pro-site-logo align-center">                                    
									<a href="<?php echo add_lang_vars_custom($main_site_url); ?>">
										<img id="artbg-logo" src="<?php echo get_site_url(); ?>/wp-content/themes/pro3/img/logo_beta_small.png" alt="Artbulgaria">
                                    </a>
                                </div>
                            </div>
                           <div class="col-md-10 col-sm-12 col-xs-12">
                            <div id="headsearch-container" class="col-md-8 col-sm-12 col-xs-12">
                                <div class="header-search-menu">
                                    <div class="widget mp_global_product_search_widget">
                                        <h2 class="widgettitle"><?php _e('Global Product Search'); ?></h2>
                                        <?php if (!is_admin()) echo render_g_search(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 pull-right">
                            	<div class="language-wrapper">
								<?php if(ICL_LANGUAGE_CODE=='bg'): ?>
									<div class="language-wrapper-bg">
								<?php else: ?>
									<div class="language-wrapper-en">
								<?php endif; ?>
						<?php 
						
						if($top_rank_var == $page_en) {
							$new_l = $_SERVER["REQUEST_URI"];
							
							if($default_lang == 'bg'){
								if(ICL_LANGUAGE_CODE=='bg') {										
									$langselect = '<a class="translate_en" href="'.tp_add_query_lang($new_l).'"></a>';
								} else {
									$langselect = '<a class="translate_bg" href="'.tp_remove_query_lang($new_l).'"></a>';
								}	
							} else {
								if(ICL_LANGUAGE_CODE=='en') {										
									$langselect = '<a class="translate_bg" href="'.tp_add_query_lang($new_l,'bg').'"></a>';
								} else {
									$langselect = '<a class="translate_en" href="'.tp_remove_query_lang($new_l).'"></a>';
								}								
							}	
	
						} else {
						
							$new_l = $_SERVER["REQUEST_URI"];
							if(strstr($new_l,'/store/') || strstr($new_l,'/store')){
							
								if(strstr($new_l,'/store/products/category/') || strstr($new_l,'/store/categories/')) {
									$settings = get_site_option( 'mp_network_settings' );
									$mp_cat_global = get_query_var('pagename');
									if ($mp_cat_global == 'mp_global_categories') 
										$cat_slug = get_query_var('global_taxonomy');
									else 
										$cat_slug = get_query_var( 'term' );
									
									
									$term = tp_get_term_by('slug', urldecode($cat_slug), 'product_category'); 
									$link = $new_l;
									if($term){
										if(ICL_LANGUAGE_CODE=='bg')
											$tans_lang = 'en';
										else
											$tans_lang = 'bg';
										
										$transtax = icl_object_id($term->term_id, 'product_category', true, $tans_lang);
										$term = tp_get_term_by('id', urlencode($transtax), 'product_category');
										if(strstr($new_l,'/store/products/category/')){
											$link = get_term_link( $term->slug, 'product_category' );
										} else {
											$link = localize_url(get_home_url( mp_main_site_id(), $settings['slugs']['marketplace'] . '/' . $settings['slugs']['categories'] . '/' . $term->slug . '/' ));
										}
									}
									
									if($default_lang == 'bg'){
										if(ICL_LANGUAGE_CODE=='bg'){																	
											$langselect = '<a class="translate_en" href="'.tp_add_query_lang($link).'"></a>';

										} else{
											$langselect = '<a class="translate_bg" href="'.$link.'"></a>';
										}
									} else {
										if(ICL_LANGUAGE_CODE=='en'){																	
											$langselect = '<a class="translate_bg" href="'.tp_add_query_lang($link,'bg').'"></a>';

										} else{
											$langselect = '<a class="translate_en" href="'.tp_remove_query_lang($link).'"></a>';
										}										
									}
								} else {
									if($default_lang == 'bg'){
										if(ICL_LANGUAGE_CODE=='bg')
											$langselect = '<a class="translate_en" href="'.tp_add_query_lang($new_l).'"></a>';
										else 
											$langselect = '<a class="translate_bg" href="'.tp_remove_query_lang($new_l).'"></a>';

									} else {
										if(ICL_LANGUAGE_CODE=='en')
											$langselect = '<a class="translate_bg" href="'.tp_add_query_lang($new_l,'bg').'"></a>';
										else 
											$langselect = '<a class="translate_en" href="'.tp_remove_query_lang($new_l).'"></a>';										
									}		
										
								}
								
							} else if(strstr($new_l,'/forums/') || strstr($new_l,'/forums') || strstr($new_l,'/user-login/') || strstr($new_l,'/user-login') || strstr($new_l,'/user-profile/') || strstr($new_l,'/user-profile') || strstr($new_l,'/user-register/') || strstr($new_l,'/user-register') || strstr($new_l,'/user-reset-password/') || strstr($new_l,'/user-reset-password') || strstr($new_l,'/user-forgot-password/') || strstr($new_l,'/user-forgot-password') || strstr($new_l,'/user-activation/') || strstr($new_l,'/user-activation') || strstr($new_l,'/quote-request') || strstr($new_l,'/members') || is_404()){ 
							
								if($default_lang == 'bg'){
							
									if(ICL_LANGUAGE_CODE=='bg')
										$langselect = '<a class="translate_en" href="'.tp_add_query_lang($new_l).'"></a>';
									else 
										$langselect = '<a class="translate_bg" href="'.tp_remove_query_lang($new_l).'"></a>';		
								} else {
									if(ICL_LANGUAGE_CODE=='en')
										$langselect = '<a class="translate_bg" href="'.tp_add_query_lang($new_l,'bg').'"></a>';
									else 
										$langselect = '<a class="translate_en" href="'.tp_remove_query_lang($new_l).'"></a>';										
								}
								
							} else {
								if(ICL_LANGUAGE_CODE=='bg'){
									$langselect = str_replace('<a ','<a class="translate_en" ',language_selector());
									$langselect = str_replace('English','',$langselect);
								} else {
									$langselect = str_replace('<a ','<a class="translate_bg" ',language_selector());
									$langselect = str_replace('Bulgarian','',$langselect);
								}
								
							}	
						
						}									
						?>
									<div class="pull-left">
									<label><?php _e('Bulgarian','pro3'); ?></label>
	                                <span><?php echo $langselect; ?></span>
									</div>
									<div class="pull-right">
									<?php if(!$mystoreid && !$issuperadmin): ?>
										<a href="<?php echo $regst_link; ?>" class="btn btn-default"><?php _e('Create Free Shop'); ?></a>
									<?php else: ?>
										<a href="<?php echo $sub_site_url; ?>" class="btn btn-default"><?php _e('Check My Store'); ?></a>
									<?php endif; ?>
									</div>
                               </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
            </header>

        </div>
    </div>
    <!-- End Header Section -->
	<?php //echo Tp_Category_Menu(); ?>
	<?php do_action('tr_category_menu'); ?>
	<?php do_action('sub_site_header'); ?>
	<!-- End Header Section -->
	<div class="clear"></div>
<!-- End header -->