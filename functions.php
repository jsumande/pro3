<?php

	$wmpl_lang = '';
	$wmpl_lang2 = '';
	$fucking_lang = ICL_LANGUAGE_CODE;
	if($fucking_lang == 'bg'){
		$wmpl_lang = esc_attr($fucking_lang);
		$wmpl_lang2 = '?lang='.$wmpl_lang;
		$wmpl_lang = '&amp;lang='.$wmpl_lang;
	}

	$is_store_review = false;
	$top_rank_var = (get_query_var('tp_action'))? get_query_var('tp_action') : '';
	$tp_global_page_settings = get_site_option( 'tp_global_page_settings' );
	$store_review_id = '00101';	
	
	

	if( $top_rank_var == $tp_global_page_settings['store_review_slug_en'] || $top_rank_var == $tp_global_page_settings['store_review_slug_bg'] || $top_rank_var == urlencode($tp_global_page_settings['store_review_slug_en']) || $top_rank_var == urlencode($tp_global_page_settings['store_review_slug_bg']) ){
		$is_store_review = true;
	}
		
	
	define( 'PRO_INC_CORE_FUNC' , apply_filters( 'pro_inc_core_func', true ) );
	define( 'PRO_INC_BLOG_FUNC' , apply_filters( 'pro_inc_blog_func', true ) );
	define( 'PRO_INC_GALLERY_FUNC' , apply_filters( 'pro_inc_gallery_func', true ) );
	define( 'PRO_INC_SHORTCODE_FUNC' , apply_filters( 'pro_inc_shortcode_func', true ) );
	define( 'PRO_INC_NAV_WALKER_FUNC' , apply_filters( 'pro_inc_nav_walker_func', true ) );
	define( 'PRO_INC_THEME_OPTIONS_FUNC' , apply_filters( 'pro_inc_theme_options_func', true ) );
	define( 'PRO_INC_PAGE_BUILDER_FUNC' , apply_filters( 'pro_inc_page_builder_func', true ) );
	define( 'PRO_INC_MP_FUNC' , apply_filters( 'pro_inc_mp_func', true ) );
	define( 'PRO_INC_REVSLIDER_FUNC' , apply_filters( 'pro_inc_revslider_func', true ) );
	define( 'PRO_INC_METABOXES_FUNC' , apply_filters( 'pro_inc_metaboxes_func', true ) );
	define( 'PRO_INC_MULTISITE_FUNC' , apply_filters( 'pro_inc_multisite_func', true ) );
	define( 'PRO_INC_PREMIUM_FEATURES_PLUGIN' , apply_filters( 'pro_inc_premium_features_plugin', true ) );
	define( 'PRO_INC_CORE_HOOKS' , apply_filters( 'pro_inc_core_hooks', true ) );
	define( 'PRO_VISIBLE_IN_ALL_SUBSITES' , apply_filters( 'pro_visible_in_all_subsites', true ) );


	/* Include Core functions
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_CORE_FUNC )
		require_once(get_template_directory() . '/functions/core.php');

	/* Include Blog related Functions
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_BLOG_FUNC )
		require_once(get_template_directory() . '/functions/blog-functions.php');

	/* Include gallery related Functions
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_GALLERY_FUNC ) {
		require_once(get_template_directory() . '/functions/gallery.php');
		$satfw_gallery = new SATFW_Gallery();
	}

	/* Include shortcodes
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_SHORTCODE_FUNC )
		require_once(get_template_directory() . '/functions/shortcodes.php');

	/* Include Nav Walker Functions
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_NAV_WALKER_FUNC )
		require_once(get_template_directory() . '/functions/twitter_bootstrap_nav_walker.php');

	/* Include Theme Options Functions
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_THEME_OPTIONS_FUNC && !function_exists('m413_options') ) {
		// Admin functions
		require_once(get_template_directory() . '/admin/admin-functions.php');
		require_once(get_template_directory() . '/admin/admin-interface.php');
		require_once(get_template_directory() . '/admin/theme-settings.php');
	}

	/* Include Page Builder functions
	------------------------------------------------------------------------------------------------------------------- */
	
	if( true == PRO_INC_PAGE_BUILDER_FUNC && !class_exists('AQ_Page_Builder') ) {
		//Register Aqua Page Builder
		define( 'AQPB_PATH', get_template_directory() . '/functions/aqua-page-builder/' );
		define( 'AQPB_DIR', get_template_directory_uri() . '/functions/aqua-page-builder/' );
		define( 'AQPB_TEXT_DOMAIN_SLUG' , 'pro' );
		require_once(get_template_directory() . '/functions/aqua-page-builder/aq-page-builder.php');
		require_once(get_template_directory() . '/functions/page-builder-blocks.php');
		if (class_exists( 'MarketPress' )) require_once(get_template_directory() . '/functions/page-builder-mp-blocks.php');
		require_once(get_template_directory() . '/functions/page-builder.php');
	}

	/* Include MarketPress related functions
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_MP_FUNC && class_exists( 'MarketPress' ) ) {
		//Register MarketPress related functions
		require_once(get_template_directory() . '/functions/mp-functions-pro.php');
		require_once(get_template_directory() . '/functions/mp-widgets-pro.php');

		// Register Multisite Functions
		if ( true == PRO_INC_MULTISITE_FUNC && is_multisite() )
			require_once(get_template_directory() . '/functions/mp-ms-functions-pro.php');		
	}

	/* Include meta box functions
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_METABOXES_FUNC && !class_exists( 'cmb_Meta_Box' ) ) {
		// register metaboxes
		require_once(get_template_directory() . '/functions/metaboxes.php');
		remove_filter( 'cmb_meta_boxes', 'mpt_metaboxes_for_multiple_product_image' );
		add_filter( 'cmb_meta_boxes', 'pro_theme_metaboxes' );
	}

	/* Include Rev Slider
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_REVSLIDER_FUNC )
		require_once(get_template_directory() . '/functions/tgm-plugin-activation/revslider.php');

	/* Include MP Premium Features
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_PREMIUM_FEATURES_PLUGIN && class_exists('MarketPress') ) {
		if ( !class_exists('MPPremiumFeatures') ) {
			define( 'MPPF_PATH', get_template_directory() . '/functions/premium-features/' );
			define( 'MPPF_URL', get_template_directory_uri() . '/functions/premium-features/' );
			add_filter( 'mppremiumfeatures_inc_image_taxonomy_class' , '__return_false' );
			global $mppf;
			require_once( MPPF_PATH . 'mp-premium-features-class.php' );
			$mppf = new MPPremiumFeatures( array(
					'plugin_path' => MPPF_PATH,
					'plugin_url' => MPPF_URL,
					'plugin_slug' => 'pro',
				) );
		}
		pro_mppf_integration();
	}

	/* Add core hooks
	------------------------------------------------------------------------------------------------------------------- */

	if ( true == PRO_INC_CORE_HOOKS ) {

		// custom comment form
		add_filter( 'comment_form_defaults' , 'pro_custom_comment_form');
		add_filter( 'comment_form_default_fields' , 'pro_custom_comment_fields');

		// custom search form
		add_filter( 'get_search_form', 'pro_search_form' );

		// register CSS & JS for PRO
		add_action('wp_enqueue_scripts', 'pro_register_style');
		add_action('wp_enqueue_scripts', 'pro_register_js');
		add_action('wp_enqueue_scripts', 'pro_remove_duplicate_bootstrap_css' , 10001);

		// Initialize the required functions to this theme
		add_action( 'init', 'pro_init_functions' , 96 );

		// call for product listing functions
		add_action('pro_product_listing_page' , 'pro_list_products' , 10 , 2);
		add_action('pro_category_page' , 'pro_list_products' , 10 , 2);
		add_action('pro_tag_page' , 'pro_list_products' , 10 , 2);
		add_action('pro_taxonomy_page' , 'pro_list_products' , 10 , 2);

		add_action('wp_enqueue_scripts', 'pro_custom_level_css', 9999);

	}

	/* Dev functions
	------------------------------------------------------------------------------------------------------------------- */
	if (!function_exists('is_dev')) {
		function is_dev($whitelist = [".dev", ".local", "127.0.0.1"])
		{
			if (in_array($_SERVER['SERVER_ADDR'], $whitelist)) return true;
			foreach ($whitelist as $w) {
				if (strpos($_SERVER['HTTP_HOST'], $w) !== false) {
					return true;
				}
			}
			return false;
		}
	}
		
	/* CSS & JS
	------------------------------------------------------------------------------------------------------------------- */

	function pro_remove_duplicate_bootstrap_css() {
		wp_dequeue_style('mppsw-bootstrap-css');
		wp_dequeue_style('mpdg-bootstrap-css');

		do_action( 'pro_remove_duplicate_bootstrap_css_hook' , 'pro3' );
	}

	function pro_register_style() {
		$tpl_dir = get_template_directory_uri();
		
		//wp_register_style('mp-reset-css', $tpl_dir . '/css/mp-reset.css', null, null);
		//wp_enqueue_style( 'mp-reset-css' );
		
		//combine bootstap and bootstrap responsive
		wp_register_style('bootstrap-css', $tpl_dir . '/css/bootstrap.css', null, '3.2.3');
		wp_enqueue_style( 'bootstrap-css' );
		
		//wp_register_style('bootstrap-responsive-css', $tpl_dir . '/css/bootstrap-responsive.css', null, '3.2.3');
		//wp_enqueue_style( 'bootstrap-responsive-css' );
		
		//wp_register_style('powertip-css', get_template_directory_uri() . '/css/powertip/jquery.powertip.min.css', null, null);
		//wp_enqueue_style( 'powertip-css' );
		
		//wp_register_style('font-awesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', null, '3.2.2');
		//wp_enqueue_style( 'font-awesome-css' );

		//wp_register_style('font-awesome-css-new', (!is_dev()
		//	? 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css'
		//	: $tpl_dir . '/dev') . '/font-awesome.min.css', null, '4.3.0');
		//wp_enqueue_style( 'font-awesome-css-new' );
		
		//wp_register_style('pro-flexslider-css', $tpl_dir . '/css/flexslider.css', null, null);
		//wp_enqueue_style( 'pro-flexslider-css' );
		
		//wp_register_style('pro-flexslider-custom-css', $tpl_dir . '/css/flexslider-custom.css', null, null);
		//wp_enqueue_style( 'pro-flexslider-custom-css' );
		
		wp_register_style('zoome-css', $tpl_dir . '/css/zoome-min.css', null, null);
		wp_enqueue_style( 'zoome-css' );
		if ( class_exists('AQ_Page_Builder') ){
			wp_enqueue_style('page-builder-custom-css', $tpl_dir . '/css/page-builder.css', null, null);
			wp_enqueue_style( 'page-builder-custom-css' );
		}
		
		//wp_enqueue_style('formValidation-min-css', $tpl_dir . '/css/formValidation.min.css', null, null);
		//wp_enqueue_style( 'formValidation-min-css' );
		
		//combines pro.css and pro-responsive.css 
		wp_register_style('pro-css', $tpl_dir . '/css/pro.css', null, '3.0.0');
		wp_enqueue_style( 'pro-css' );
		
		//wp_register_style('pro-responsive-css', $tpl_dir . '/css/pro-responsive.css', null, '3.0.0');
		//wp_enqueue_style( 'pro-responsive-css' );
		
		//wp_register_style('bbpress-css', $tpl_dir . '/bbpress/css/bbpress.css', null, null);
		//wp_enqueue_style( 'bbpress-css' );
		
		//$selected = get_option('mpt_theme_base_style');
		
		/* remove color-lightblue.css combine to pro.css
		$selected = get_blog_option(1,'mpt_theme_base_style');
		switch ($selected) {
			case 'Light Blue':
				$cssfile = '/styles/color-lightblue.css';
			break;
			case 'Blue':
				$cssfile = '/styles/color-blue.css';
			break;
			case 'Red':
				$cssfile = '/styles/color-red.css';
			break;
			case 'Yellow':
				$cssfile = '/styles/color-yellow.css';
			break;
			case 'Green':
				$cssfile = '/styles/color-green.css';
			break;
			case 'Grey':
				$cssfile = '/styles/color-grey.css';
			break;
			case 'Purple':
				$cssfile = '/styles/color-purple.css';
			break;
			default:
				$cssfile = '/styles/color-lightblue.css';
			break;
		}
		wp_register_style('pro-color-skin', $tpl_dir . $cssfile , null, '3.0.0');
		wp_enqueue_style( 'pro-color-skin' );
		*/
		
		// combined in this file frontend-dropdown/css/styles.css  toprank_gsearch/css/gsearch.cs plugins/mp-anywhere-cart/css/mpawc.css toprank_slider\css\slick.css /toprank_slider/css/slider.css
		wp_register_style( 'global-used-custom-made-scripts-styles', get_template_directory_uri() .'/css/global-used-custom-made-scripts-styles.css' );
		wp_enqueue_style( 'global-used-custom-made-scripts-styles' );		
		
		//Toprank Slider Assets
		//wp_register_style('toprank_slider_slick_css', "{$tpl_dir}/toprank_slider/css/slick.css");
		//wp_enqueue_style( 'toprank_slider_slick_css' );
		
		//wp_enqueue_style('toprank_slider_slider_css', "{$tpl_dir}/toprank_slider/css/slider.css");
		//wp_enqueue_style( 'toprank_slider_slider_css' );	
		
		
		//wp_register_style( 'frontend-dropdown-styles', get_template_directory_uri() .'/frontend-dropdown/css/styles.css' );
		//wp_enqueue_style( 'frontend-dropdown-styles' );		
		
		//wp_register_style('toprank_gsearch_css', "{$tpl_dir}/toprank_gsearch/css/gsearch.css");
		//wp_enqueue_style( 'toprank_gsearch_css' );

		$new_l = $_SERVER["REQUEST_URI"];
		if(strstr($new_l,'/manage-store/')){
			wp_register_style( 'dataTables-bootstrap-css', get_template_directory_uri() .'/css/dataTables.bootstrap.min.css' );
			wp_enqueue_style( 'dataTables-bootstrap-css' );	
			wp_register_style( 'buttons-bootstrap-css', get_template_directory_uri() .'/css/buttons.bootstrap.min.css' );
			wp_enqueue_style( 'buttons-bootstrap-css' );			
		}		
	
	}
	
	function pro_custom_level_css(){
		wp_enqueue_style('pro-custom-css', get_template_directory_uri() . '/custom.css');
	}
	
	
	function pro_register_js(){
		
		$tpl_dir = get_template_directory_uri();
		
		wp_enqueue_script( 'comment-reply' );
		// remove for testing
		//wp_enqueue_script( 'jquery-masonry' );
		
		wp_register_script('imagesloadedjs', $tpl_dir . '/js/imagesloaded.pkgd.min.js', array('jquery'));
		wp_enqueue_script('imagesloadedjs');
		
		wp_register_script('bootstrap', $tpl_dir . '/js/bootstrap.min.js', array('jquery'),'',true);
		wp_enqueue_script('bootstrap');

	   		
		$new_l = $_SERVER["REQUEST_URI"];
		if(strstr($new_l,'/manage-store/')) {
			wp_register_script( 'jquery-dataTables-js', $tpl_dir . '/js/jquery.dataTables.min.js', array('jquery'),'',true);
			wp_enqueue_script('jquery-dataTables-js');
			wp_register_script( 'buttons-bootstrap-js', $tpl_dir . '/js/buttons.bootstrap.min.js','','',true);
			wp_enqueue_script('buttons-bootstrap-js');
		}	
			
		

		//wp_register_script('pro-flexslider-js', $tpl_dir . '/js/jquery.flexslider-min.js', array('jquery'));
		//wp_enqueue_script('pro-flexslider-js');
		
		wp_register_script('filterablejs', $tpl_dir . '/js/filterable.js', array('jquery'));
		wp_enqueue_script('filterablejs');
		
		wp_register_script('powertip-js', $tpl_dir . '/js/jquery.powertip.min.js', array('jquery'),'',true);
		wp_enqueue_script('powertip-js');
		
		//AIzaSyBBXv_uL9nN1mvAwUAEEidQMGNyKL4nYSc
        if ( ! is_main_site() || strstr($new_l,'/manage-store/')) {
			if (!wp_script_is( 'googlemaps', 'enqueued' )){
				// Google Maps Js
				wp_register_script('googlemaps', '//maps.google.com/maps/api/js?key=AIzaSyBBXv_uL9nN1mvAwUAEEidQMGNyKL4nYSc&v=3', array('jquery'),'',true); 
				wp_enqueue_script('googlemaps');
			}		
		}
		
		//zoome tinynav waypoint
		wp_register_script('zoome-js', $tpl_dir . '/js/zoome.js', array('jquery'),'',true);
		wp_enqueue_script('zoome-js');
		
		wp_register_script('tinynavjs', $tpl_dir . '/js/tinynav.min.js', array('jquery'),'',true);
		wp_enqueue_script('tinynavjs');
		
		wp_register_script('waypoints', $tpl_dir . '/js/waypoints.min.js', array('jquery'),'',true);
		wp_enqueue_script('waypoints');
		
		//combine with formValidation-bootstrap-min-js
		wp_register_script('formValidation-min-js', $tpl_dir . '/js/formValidation.min.js', array('jquery'),'',true);
		wp_enqueue_script('formValidation-min-js');		
		
		//wp_register_script('formValidation-bootstrap-min-js', $tpl_dir . '/js/formValidation-bootstrap.min.js', array('jquery'));
		//wp_enqueue_script('formValidation-bootstrap-min-js');
		
		wp_register_script('jstimezonedetect', '//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js','','',true); 
		wp_register_script('jquery-cookie', $tpl_dir .'/js/jquery.cookie.js', array('jquery'),'',true); 
		wp_register_script('set_uct_timezone_cookie', $tpl_dir .'/js/set_uct_timezone_cookie.js', array('jquery', 'jquery-cookie', 'jstimezonedetect'),'',true);
		wp_enqueue_script('jstimezonedetect');
		wp_enqueue_script('jquery-cookie');
		wp_enqueue_script('set_uct_timezone_cookie');
		
		if((is_main_site() && is_page('signup'))){
	
			//wp_register_script('formValidation-create-shop', $tpl_dir . '/js/formValidation-create-shop.js', array('jquery'));
			//wp_enqueue_script('formValidation-create-shop');
			
			$translation_array = array(
				'FieldReq' => __( 'This field is required', 'pro' ),
				'FieldLength' => __( 'This field must be more than 4 and less than 30 characters long', 'pro' ),
				'FieldChars' => __( 'This field can only consist of alphabetical and number', 'pro' ),
				'EmailNotVal' => __( 'The input is not a valid email address', 'pro' ),
				'PassReq' => __( 'The password is required', 'pro' ),
				'PassNotTs' => __( 'The password cannot be the same as username', 'pro' ),
				'CpassNotTs' => __( 'The confirm password is required and cannot be empty', 'pro' ),
				'PassMustTs' => __( 'The password and its confirm must be the same', 'pro' ),			
				'BlogdescLength' => __( 'Store description must be more than 50 and less than 500 characters long', 'pro' ),		'BlogcatChoice' => __( 'Please choose 1 - 4 Store categories.', 'pro' )
			);

			//wp_localize_script( 'formValidation-create-shop', 'formVal', $translation_array );	
		}
		
		if(is_page_template( 'page-support.php' )){
			//support page
			wp_register_script('formValidation-support-page', $tpl_dir . '/js/formValidation-support-page.js', array('jquery'),'',true);
			wp_enqueue_script('formValidation-support-page');
			
			$translation_array = array(
				'supName' => __( 'The name is required', 'pro' ),
				'supNameLength' => __( 'The name must be atleast 4 letters', 'pro' ),
				'supEmail' => __( 'The email id required', 'pro' ),
				'supEmailNotVal' => __( 'The input is not a valid email address', 'pro' ),
				'supAbout' => __('About is required'),
				'supMessage' => __( 'Message is required', 'pro' )				
			);
			wp_localize_script( 'formValidation-support-page', 'formVal', $translation_array );					
		}
		if(is_page_template( 'page-contact-us.php' )){
			//support page
			wp_register_script('formValidation-contact-page', $tpl_dir . '/js/formValidation-contact-page.js', array('jquery'),'',true);
			wp_enqueue_script('formValidation-contact-page');
			
			$translation_array = array(
				'conName' => __( 'The name is required', 'pro' ),
				'conNameLength' => __( 'The name must be atleast 4 letters', 'pro' ),
				'conEmail' => __( 'The email id required', 'pro' ),
				'conEmailNotVal' => __( 'The input is not a valid email address', 'pro' ),
				'conMessage' => __( 'Message is required', 'pro' )				
			);
			wp_localize_script( 'formValidation-contact-page', 'formVal', $translation_array );					
		}
		
		if(get_post_type() == 'forum' OR get_post_type() == 'topic' OR get_post_type() == 'reply') {	
		wp_register_script('formValidation-topic', $tpl_dir . '/js/formValidation-topic.js', array('jquery'),'',true);
			wp_enqueue_script('formValidation-topic');
			
			$translation_array = array(
				'' => __( '', 'pro' )			
			);
			wp_localize_script( 'formValidation-topic', 'formVal', $translation_array );	

		}
		//for comment
		if( 'post' == get_post_type()){
			if ( !is_user_logged_in() ) {
				wp_register_script('formValidation-comment', $tpl_dir . '/js/formValidation-comment.js', array('jquery'),'',true);
				wp_enqueue_script('formValidation-comment');
				$ctranslation_array = array(
					'reqc' => __( 'Comment section is required', 'pro' ),
					'waits' => __( 'Please Wait. Sending...', 'pro' ),
					'namer' => __( 'Your Name is required', 'pro' ),
					'emailr' => __( 'The email address is required', 'pro' ),
					'iemail' => __( 'Invalid E-Mail address', 'pro' ),
					'pcomment' => __( 'Post Comment', 'pro' )		
				);
				wp_localize_script( 'formValidation-comment', 'commentVal', $ctranslation_array );					
			}	else {
				wp_register_script('formValidation-comment2', $tpl_dir . '/js/formValidation-comment2.js', array('jquery'),'',true);
				wp_enqueue_script('formValidation-comment2');		
				$ctranslation_array = array(
					'reqc' => __( 'Comment section is required', 'pro' ),
					'waits' => __( 'Please Wait. Sending...', 'pro' )			
				);
				wp_localize_script( 'formValidation-comment2', 'commentVal', $ctranslation_array );					
			}
		}
		wp_register_script('twitter_typeahead', $tpl_dir.'/js/typeahead.bundle.js', ['jquery'],'',true );
		wp_enqueue_script('twitter_typeahead');

		//combined to pro.css
		//wp_register_style('twitter_typeahead_css', $tpl_dir.'/css/typeahead.css', ['twitter_typeahead']);
		//wp_enqueue_style('twitter_typeahead_css');

		//wp_register_script('serializeObject', $tpl_dir.'/js/jquery.serializeObject.min.js', ['jquery'],'',true );
		//wp_enqueue_script('serializeObject');

		wp_register_script('ajaxq', $tpl_dir. '/js/ajaxq.js', array('jquery'),'',true );
		wp_enqueue_script('ajaxq');		
		
		wp_register_script( 'selectbox_js', $tpl_dir .'/frontend-dropdown/js/jquery.selectbox-0.2.js','','',true );
		wp_enqueue_script('selectbox_js');	
		
		//wp_register_script( 'select_custom', $tpl_dir .'/frontend-dropdown/js/custom.js' );
		//wp_enqueue_script('select_custom');			
		
		// remove unused
		//wp_register_script( 'custom_modal_boostrap', $tpl_dir .'/js/custom-modal-boostrap.js','','',true );
		//wp_enqueue_script('custom_modal_boostrap');	
		
		wp_register_script('toprank_gsearch_js', "{$tpl_dir}/toprank_gsearch/js/gsearch.js",'','',true);
		wp_enqueue_script('toprank_gsearch_js');	
		
		$translation_arrays2 = array(
				'gsoops' => __( 'Oops!', 'pro' ),
				'addtocart' => __( 'Add To Cart', 'pro' )
		);

		wp_localize_script( 'toprank_gsearch_js', 'GsearchVal', $translation_arrays2 );			
		
		//slick slider and slider
		wp_register_script('toprank_slider_slick_js', "{$tpl_dir}/toprank_slider/js/slick.min.js",'','',true);
		wp_enqueue_script('toprank_slider_slick_js');
		
		wp_register_script('toprank_slider_slider_js', "{$tpl_dir}/toprank_slider/js/slider.js",'','',true);
		wp_enqueue_script('toprank_slider_slider_js');			
		
			
		$tp_global_page_settings = (!get_site_transient( 'cache_global_page_settings' ))? get_site_transient( 'cache_global_page_settings' ) : get_site_option( 'tp_global_page_settings' );
		$langu = '';
		$requesturl = $tp_global_page_settings['store_quote_slug_en'];
		if(defined('ICL_LANGUAGE_CODE')){
			if(ICL_LANGUAGE_CODE=='bg'){
				$langu = '?lang=bg';
				$requesturl = $tp_global_page_settings['store_quote_slug_bg'];
			}
		}
		
		$translation_arrays = array(
				'viewstore' => __( 'View store', 'pro' ),
				'sendmsg' => __( 'Send message', 'pro' ),
				'booknow' => __( 'View Event', 'pro' ),
				'shopnow' => __( 'Shop Now', 'pro' ),
				'readmore' => __('Read more','pro'),
				'msg' => __( 'Message', 'pro' ),
				'subject' => __('Subject','pro'),
				'send' => __( 'Send', 'pro' ),
				'close' => __( 'Close', 'pro' ),
				'error' => __( 'Error', 'pro' ),
				'reqquote' => __( 'Request a<br/>Quote', 'pro' ),
				'gsno' => __( 'No Results', 'pro' ),
				'gstype' => __( 'Type in your keyword and try again.', 'pro' ),
				'gsfor' => __( ' ', 'pro' ),
				'lang' => $langu,
				'qrurl' => $requesturl
		);

		wp_localize_script( 'toprank_slider_slider_js', 'SliderVal', $translation_arrays );	

	}

	/* Init functions
	------------------------------------------------------------------------------------------------------------------- */

	function pro_init_functions() {

		// register menu
		if(function_exists('register_nav_menus') ){
			register_nav_menus(array(
			'mainmenu' => __('Main Menu','pro')
					)
				);
		}

		// add sidebar
		if(function_exists('register_sidebar')){
			register_sidebar(array(
					'name' => __('Sidebar 1','pro'),
					'id' => 'sidebar-1',
					'description' => __('Widgets in this area will be shown in sidebar one.','pro'),
					'before_widget' => '<div class="well sidebar-widget-well">',
					'after_widget' => '</div><div class="clear"></div>',
					'before_title' => '<h4 class="sidebar-widget-title">',
					'after_title' => '</h4>'
				)
			);

			register_sidebar(array(
					'name' => __('Sidebar 2','pro'),
					'id' => 'sidebar-2',
					'description' => __('Widgets in this area will be shown in sidebar two.','pro'),
					'before_widget' => '<div class="well sidebar-widget-well">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="sidebar-widget-title">',
					'after_title' => '</h5>'
				)
			);
					
			register_sidebar(array(
					'name' => __('Sidebar 3','pro'),
					'id' => 'sidebar-3',
					'description' => __('Widgets in this area will be shown in sidebar three.','pro'),
					'before_widget' => '<div class="well sidebar-widget-well">',
					'after_widget' => '</div>',
					'before_title' => '<h4 class="sidebar-widget-title">',
					'after_title' => '</h4>'
				)
			);			

			register_sidebar(array(
					'name' => __('Sidebar 4','pro'),
					'id' => 'sidebar-4',
					'description' => __('Widgets in this area will be shown in sidebar four.','pro'),
					'before_widget' => '<div class="well sidebar-widget-well">',
					'after_widget' => '</div>',
					'before_title' => '<h4 class="sidebar-widget-title">',
					'after_title' => '</h4>'
				)
			);
				
			register_sidebar(array(
					'name' => __('Footer One','pro'),
					'id' => 'footer-one',
					'description' => __('First Footer Widget','pro'),
					'before_widget' => '',
					'after_widget' => '',
					'before_title' => '<h4 class="page-header"><span>',
					'after_title' => '</span></h4>'
				)
			);
			register_sidebar(array(
					'name' => __('Footer Two','pro'),
					'id' => 'footer-two',
					'description' => __('Second Footer Widget','pro'),
					'before_widget' => '',
					'after_widget' => '',
					'before_title' => '<h4 class="page-header"><span>',
					'after_title' => '</span></h4>'
				)
			);
			register_sidebar(array(
					'name' => __('Footer Three','pro'),
					'id' => 'footer-three',
					'description' => __('Third Footer Widget','pro'),
					'before_widget' => '',
					'after_widget' => '',
					'before_title' => '<h4 class="page-header"><span>',
					'after_title' => '</span></h4>'
				)
			);

			register_sidebar(array(
					'name' => __('Footer Four','pro'),
					'id' => 'footer-fourth',
					'description' => __('Fourth Footer Widget','pro'),
					'before_widget' => '',
					'after_widget' => '',
					'before_title' => '<h4 class="page-header"><span>',
					'after_title' => '</span></h4>'
				)
			);	
			
			register_sidebar(array(
					'name' => __('(bbPress) Sidebar','pro'),
					'id' => 'bbp-sidebar',
					'description' => __('Widgets in this area will be shown in Forum page.','pro'),
					'before_widget' => '<div class="well sidebar-widget-well">',
					'after_widget' => '</div><div class="clear"></div>',
					'before_title' => '<h4 class="sidebar-widget-title">',
					'after_title' => '</h4>'
				)
			);
			
			register_sidebar(array(
					'name' => __('Search Sidebar','pro'),
					'id' => 'search-sidebar',
					'description' => __('Widgets in this area will be shown in sidebar one.','pro'),
					'before_widget' => '<div id="search" class="well sidebar-widget-well">',
					'after_widget' => '</div><div class="clear"></div>',
					'before_title' => '<h4 class="sidebar-widget-title">',
					'after_title' => '</h4>'
				)
			);
		}

		// add post type support to page and post
		add_post_type_support( 'page', 'excerpt' );
		add_post_type_support( 'page', 'thumbnail' );
		add_post_type_support( 'post', 'excerpt');
		add_post_type_support( 'post', 'custom-fields');
		add_post_type_support( 'post', 'comments');

		if ( class_exists( 'MarketPress' ) ) {
			add_post_type_support( 'product', 'comments' );
		}

		// add thumbnail support to theme
		if ( function_exists( 'add_theme_support' ) ) {
			add_theme_support( 'post-thumbnails' );
		}

		// add additional image size
		if ( function_exists( 'add_image_size' ) ) { 
			add_image_size( 'tb-360', 360, 270 );
			add_image_size( 'tb-860', 860, 300 );
			add_image_size( 'tb-600' , 600 , 450 , true ); // 4x3 ratio
			add_image_size( 'tb-600-masonry' , 600 , 9999 , true ); // unlimited height
			add_image_size( 'tb-full-width', 1200 , 9999 ); // full width image
			add_image_size( 'slider-blogroll' , 365 , 160 , true );
		}

		if ( ! class_exists( 'cmb_Meta_Box' ) )
			require_once(get_template_directory() . '/functions/metabox/init.php');

		if ( true == PRO_INC_PREMIUM_FEATURES_PLUGIN )
			add_filter( 'mppremiumfeatures_inc_image_taxonomy_class' , '__return_false' );

		// add product default image
		add_filter( 'mp_default_product_img' , 'pro_default_product_img' );
		
	}

	function pro_load_textdomain(){
		load_theme_textdomain( 'pro', get_template_directory() . '/languages' );
	}
	
	add_action( 'after_setup_theme', 'pro_load_textdomain' );
	
	/* Product Listing action hooks
	------------------------------------------------------------------------------------------------------------------- */

	function pro_list_products( $unique_id = '' , $context = '' ) {

		$btnclass = mpt_load_mp_btn_color();
		$iconclass = mpt_load_whiteicon_in_btn();
		$labelcolor = mpt_load_mp_label_color();
		$span = mpt_load_product_listing_layout();
		$counter = mpt_load_product_listing_counter();
		//$entries = get_option('mpt_mp_listing_entries');
		$entries = get_blog_option(1,'mpt_mp_listing_entries');
		$advancedsoft = mpt_enable_advanced_sort();
		$advancedsoftbtnposition = mpt_advanced_sort_btn_position();

		$args = array(
			'unique_id' => $unique_id,
			'sort' => $advancedsoft,
			'align' => $advancedsoftbtnposition,
			'context' => $context,
			'echo' => false,
			'paginate' => true,
			'per_page' => $entries,
			'category' => ( $context == 'category' ? $unique_id : '' ),
			'tag' => ( $context == 'tag' ? $unique_id : '' ),
			'counter' => $counter,
			'span' => $span,
			'btnclass' => $btnclass,
			'iconclass' => $iconclass,
			'labelcolor' => $labelcolor
		);

		echo apply_filters( 'func_pro_list_products' , pro_advance_product_sort( $args ) , $args );		

	}

	/* Comment Form functions
	------------------------------------------------------------------------------------------------------------------- */

	function pro_custom_comment_form($defaults) {
		$defaults['comment_notes_before'] = '';
		$defaults['id_form'] = 'pro-comment-form';
		$defaults['name_submit']       = 'submit-comment';
		$defaults['comment_field'] = '<div class="col-md-12 form-group"><textarea name="comment" id="comment" class="form-control" rows="10" placeholder="'.__( 'Leave Your Comments Here' , 'pro' ).'"></textarea></div></div>';
		$defaults['comment_notes_after'] = '<div class="form-allowed-tags alert alert-info"><strong>'.__( 'Head up!' , 'pro' ).'</strong> ' . __( 'You may use these HTML tags and attributes: ' , 'pro' ) . ' <code>' . allowed_tags() . '</code>' . '</div>';

		return $defaults;
	}

	function pro_custom_comment_fields() {
		$commenter = wp_get_current_commenter();
		//$req = get_option('require_name_email');
		$req = get_blog_option(1,'require_name_email');
		$aria_req = ( $req ? ' aria-required="true"' : '' );

		$fields = array(
			'author' => '<div class="row"><div class="col-md-6 form-group"><input type="text" id="author" name="author" value="'.esc_attr($commenter['comment_author']).'"'.$aria_req.' class="form-control" placeholder="'.__( 'Your Name' , 'pro' ).( $req ? ' ' . __( '(required)' , 'pro' ) : '' ).'" /></div>',
			'email' => '<div class="col-md-6 form-group"><input type="text" id="email" name="email" value="'.esc_attr($commenter['comment_author_email']).'"'.$aria_req.' class="form-control" placeholder="'.__( 'Your Email' , 'pro' ). ' (' .( $req ? __( 'required' , 'pro' ) : '' ).' - '.__( 'will not be published' , 'pro' ).')" /></div></div>',
			'url' => '<div class="row"><div class="col-md-12"><input type="text" id="url" name="url" value="'.esc_attr($commenter['comment_author_url']).'" class="form-control" placeholder="'.__( 'Your Website URL (optional)' , 'pro' ).'" /></div>',

		);

		return $fields;
	}

	// list Comments callback function
	function pro_list_comments( $comment , $args , $depth ) {

		$GLOBALS['comment'] = $comment;

		?>
		
		<?php if ( get_comment_type() == 'pingback' || get_comment_type() == 'trackback') : ?>

			<li id="comment-<?php comment_ID(); ?>">
				<div <?php comment_class('well well-small well-comment'); ?>>
					
					<h4><?php _e( 'Pingback: ' , 'pro' ); ?></h4>
					<div class="comment-pingback">
						<?php echo apply_filters( 'pro_comment_pingback' , get_comment_author_link($comment->comment_ID) ); ?>
					</div>

					<?php edit_comment_link(); ?>

				</div><!-- End well-comment-->		
			
		<?php elseif ( get_comment_type() == 'comment' ) : ?>

			<li id="comment-<?php comment_ID(); ?>">
				<div <?php comment_class('well well-small well-comment'); ?>>

					<div class="comment-author">

						<div class="comment-author-avatar">
							<?php
								$avatar_size = 48;
								if ( $comment->comment_parent != 0 ) {
									$avatar_size = 40;
								}

								echo get_avatar($comment , $avatar_size);
							?>
							<span class="comment-author-name"><?php comment_author_link(); ?></span>
						</div>

					</div>

					<div class="clear"></div>

					<div class="comment-time"><?php comment_date(); ?> - <?php comment_time(); ?><?php edit_comment_link( __( 'edit' , 'pro' ) , ' ( ' , ' )' );?></div>

					<?php if ($comment->comment_approved == '0') : ?>

						<div class="comment-awaiting-approval">
							<?php echo apply_filters( 'pro_comment_awaiting_approval' , __( 'Your comment is awaiting moderation' , 'pro' ) ); ?>
						</div>

					<?php else : ?>

						<div class="comment-contents">
							<?php echo wpautop(get_comment_text($comment->comment_ID)); ?>
						</div>

					<?php endif; ?>

					<div class="comment-reply-button">
						<?php comment_reply_link(array_merge( $args , array( 'depth' => $depth , 'max_depth' => $args['max_depth'] ) ) ); ?>
					</div><!-- End comment-reply-button -->

				</div><!-- End well-comment-->		
		<?php endif;
	}
	
	// list Comments callback function
	function pro_list_comments2( $comment , $args , $depth ) {

		$GLOBALS['comment'] = $comment;

		?>
		
		<?php if ( get_comment_type() == 'pingback' || get_comment_type() == 'trackback') : ?>

			<li id="comment-<?php comment_ID(); ?>">
				<div <?php comment_class('well well-small well-comment'); ?>>
					
					<h4><?php _e( 'Pingback: ' , 'pro' ); ?></h4>
					<div class="comment-pingback">
						<?php echo apply_filters( 'pro_comment_pingback' , get_comment_author_link($comment->comment_ID) ); ?>
					</div>

					<?php edit_comment_link(); ?>

				</div><!-- End well-comment-->		
			
		<?php elseif ( get_comment_type() == 'comment' ) : ?>

			<li id="comment-<?php comment_ID(); ?>">
				<div <?php comment_class('well well-small well-comment'); ?>>

					<div class="comment-author">

						<div class="comment-author-avatar">
							<span class="comment-author-name"><?php _e('Comment from','pro'); ?> <?php comment_author_link(); ?></span>,  <?php comment_date( 'j F Y' ); ?> - <?php comment_time(); ?><?php edit_comment_link( __( 'edit' , 'pro' ) , ' ( ' , ' )' );?>
						</div>

					</div>

					<div class="clear"></div>

					<?php if ($comment->comment_approved == '0') : ?>

						<div class="comment-awaiting-approval">
							<?php echo apply_filters( 'pro_comment_awaiting_approval' , __( 'Your comment is awaiting moderation' , 'pro' ) ); ?>
						</div>

					<?php else : ?>

						<div class="comment-contents">
							<?php echo wpautop(get_comment_text($comment->comment_ID)); ?>
						</div>

					<?php endif; ?>

					<!-- <div class="comment-reply-button">
						<?php //comment_reply_link(array_merge( $args , array( 'depth' => $depth , 'max_depth' => $args['max_depth'] ) ) ); ?>
					</div><!-- End comment-reply-button -->
				<hr>
				</div><!-- End well-comment-->		
		<?php endif;
	}

	/* Search Form function
	------------------------------------------------------------------------------------------------------------------- */
	
	function pro_search_form( $form ) {
		if ( is_page( 'blogs' ) ) {
	    $form = '<form role="search" class="form-search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	    <div class="input-group">
	    	<input type="text" value="' . get_search_query() . '" class="form-control" name="s" id="s" placeholder="Search in blogs"/>
			<span class="input-group-btn">
				<button class="btn btn-default" type="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
			</span>
	    </div>
	    </form>';
		} else {
		   $form = '<form role="search" class="form-search form-inline" method="get" id="searchform" action="' . home_url( '/' ) . '" >
			<div class="form-group" style="width: 75%;">
			<input type="text" value="' . get_search_query() . '" class="form-control" name="s" id="s" placeholder="Search"/>
			</div>
			<button class="btn btn-default" type="submit" id="searchsubmit"><i class="fa fa-search"></i> &nbsp;&nbsp; Search &nbsp;&nbsp;</button>
	    </form>';
		}

	    return $form;
	}

	/* Default Product Image Filter
	------------------------------------------------------------------------------------------------------------------- */

	function pro_default_product_img( $image = '' ) {
		//$default_img_url = esc_url( get_option('mpt_mp_default_product_img') );
		$default_img_url = esc_url( get_blog_option(1,'mpt_mp_default_product_img') );
		if ( !empty($default_img_url) )
			return $default_img_url;
		else
			return $image;
	}

	/* MP Premium Features Integration
	------------------------------------------------------------------------------------------------------------------- */

	function pro_mppf_integration() {

		if ( class_exists('MPPremiumFeatures') ) {

			//$initial_integration = get_option( 'mpt_pro_mppf_integration' );
			$initial_integration = get_blog_option(1, 'mpt_pro_mppf_integration' );

			if ( empty($initial_integration) ) {

				//$wishlist = get_option( 'mpt_enable_wishlist_feature' );
				//$compare = get_option( 'mpt_enable_compare_feature' );
				//$crossselling = get_option( 'mpt_enable_cross_selling_feature' );
				//$mppf_settings = get_option('mp_premium_features_settings');
				
				$wishlist = get_blog_option(1, 'mpt_enable_wishlist_feature' );
				$compare = get_blog_option(1, 'mpt_enable_compare_feature' );
				$crossselling = get_blog_option(1, 'mpt_enable_cross_selling_feature' );
				$mppf_settings = get_blog_option(1,'mp_premium_features_settings');				

				if ( $wishlist == 'true' )
					$mppf_settings['mp_premium_features_wishlist_enable_wishlist'] = 'yes';
				else
					$mppf_settings['mp_premium_features_wishlist_enable_wishlist'] = 'no';

				if ( $compare == 'true' )
					$mppf_settings['mp_premium_features_compareproducts_enable_compare'] = 'yes';
				else
					$mppf_settings['mp_premium_features_compareproducts_enable_compare'] = 'no';

				if ( $crossselling == 'true' )
					$mppf_settings['mp_premium_features_crossselling_enable_cs'] = 'yes';
				else
					$mppf_settings['mp_premium_features_crossselling_enable_cs'] = 'no';

				update_option( 'mp_premium_features_settings', $mppf_settings );
				update_option( 'mpt_pro_mppf_integration', 'yes' );
			}

		}

	}
	
///Jeffs Modification //////////
add_filter( 'show_admin_bar', '__return_false' );

//Disable Sitepress CSS
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS',true);

/* JAYVEE ADDED FILE*/
if (!is_admin()) {
	require_once( 'wp_bootstrap_navwalker.php' );
    require_once( 'toprank_gsearch/gsearch.php' );
}

function getNavString() {
	$getNav = array(
	  'menu' => 'Menu 1',
	  'depth' => 2,
	  'container' => 'div',
	  'echo'            => 0,
	  'container_class' => 'navbar-collapse navbar-ex1-collapse collapse',
	  'menu_class' => 'nav navbar-nav',
	  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	  //Process nav menu using our custom nav walker
	  'walker' => new wp_bootstrap_navwalker()
		);
    $menu = wp_nav_menu( $getNav );
    return $menu;

}

function set_content_type( $content_type ) {
	return 'text/html';
}

//Accepted Mime Types
add_filter('upload_mimes','acceptFormat');
function acceptFormat($mimes) {
	$mimes = array(
			'eps' => 'application/eps',
			'png' => 'image/png',
			'jpg' => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'gif' => 'image/gif',
			'zip' => 'application/zip',
			'rar' => 'application/rar',
			'pdf' => 'application/pdf',
			'avi' => 'video/avi',
			'wmv' => 'video/x-ms-wmv',
			'flv' => 'video/x-flv',
			'mov|qt' => 'video/quicktime',
			'mpeg|mpg|mpe' => 'video/mpeg',
			'mp4|m4v|mp4a' => 'video/mp4',
			'webm' => 'video/webm',
			'mkv' => 'video/x-matroska',
			'3gp|3gpp' => 'video/3gpp', // Can also be audio
			'3g2|3gp2' => 'video/3gpp2', // Can also be audio
			'mp3' => 'video/mp3', // Can also be audio
			'midi|mid' => 'video/midi', // Can also be audio
			'aac' => 'video/aac', // Can also be audio
			'swf' =>	'video/x-swf'
	);
	return $mimes;
}


function wpse_77441_change_time_format( $anchor, $forum_id )
{
    $last_active = get_post_meta( $forum_id, '_bbp_last_active_time', true );

    if ( empty( $last_active ) ) {
        $reply_id = bbp_get_forum_last_reply_id( $forum_id );

        if ( !empty( $reply_id ) ) {
            $last_active = get_post_field( 'post_date', $reply_id );
        } else {
            $topic_id = bbp_get_forum_last_topic_id( $forum_id );

            if ( !empty( $topic_id ) ) {
                $last_active = bbp_get_topic_last_active_time( $topic_id );
            }
        }
    }
	
      $date   = get_post_time( 'jS F Y', $gmt, $reply_id, true );
      $time   = get_post_time( get_option( 'time_format' ), $gmt, $reply_id, true );
      $dt = sprintf( _x( '%1$s - %2$s', 'date at time', 'bbpress' ), $date, $time );    

	  $time_since = bbp_get_forum_last_active_time( $forum_id );
	
	  $newdate = str_replace( "$time_since</a>", "$dt</a>", $anchor );
	  
		if(ICL_LANGUAGE_CODE=='bg'){
			$search = array('1st', '2nd', '3rd', '4th', '5th', '6th', '7th', '8th', '9th', '10th', '11th', '12th', '13th', '14th', '15th', '16th', '17th', '18th', '19th', '20th', '21th', '22th', '23th', '24th', '25th', '26th', '27th', '28th', '29th', '30th', '31th');
	
			$replace = array('1-ви', '2-ри', '3-ти', '4-ти', '5-ти', '6-ти', '7-ти', '8-ти', '9-ти', '10-ти', '11-ти', '12-ти', '13-ти', '14-ти', '15-ти', '16-ти', '17-ти', '18-ти', '19-ти', '20-ти', '21-ти', '22-ти', '23-ти', '24-ти', '25-ти', '26-ти', '27-ти', '28-ти', '29-ти', '30-ти', '31-ти');				
			$newdate = str_replace($search, $replace, $newdate);	
		}
				
    return $newdate;
}
add_filter( 'bbp_get_forum_freshness_link', 'wpse_77441_change_time_format', 10, 2 );

/// Press page Social icons
function press_social_icons( $echo = true ) {

	$output = '';

	for ($i=1; $i < 9; $i++) { 

		$profileurl = esc_url(get_blog_option(1,'mpt_social_icon_'.$i.'_url'));
		$selected = get_blog_option(1,'mpt_social_icon_'.$i.'_icon');

		switch ( $selected ) {
			case 'Facebook':
				$output .= '<div class="col-md-2 press-related-link facebook">
					<a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Facebook">
					
						<div class="rl-holder">
							<div class="icon"><i class="fa fa-facebook"></i></div>
							<p> '. __("Art Bulgaria on Facebook","pro") .'</p>
						</div>
					</a>

				</div>';
				break;

			case 'Twitter':
				$output .= '<div class="col-md-2 press-related-link twitter">
					<a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Twitter">
						<div class="rl-holder">
							<div class="icon"><i class="fa fa-twitter"></i></div>
							<p> '. __("Art Bulgaria on Twitter","pro") .'</p>
						</div>
					</a>

				</div>';
				break;				

			case 'Google Plus':
				$output .= '<div class="col-md-2 press-related-link google">
					<a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Google Plus">
						<div class="rl-holder">
							<div class="icon"><i class="fa fa-google-plus"></i></div>
							<p> '. __("Art Bulgaria on Google +","pro") .'</p>
						</div>
					</a>

				</div>';
				break;

			case 'Pinterest':
				$output .= '<div class="col-md-2 press-related-link pinterest">
					<a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Pinterest">	
						<div class="rl-holder">
							<div class="icon"><i class="fa fa-pinterest"></i></div>
							<p> '. __("Art Bulgaria on Pinterest","pro") .'</p>
						</div>
					</a>

				</div>';
				break;	

			/*case 'Linkedin':
				$output .= '<a href="'.( $profileurl ? $profileurl : '#' ).'" target="_blank" Title="Linkedin">
					<div class="col-md-2 press-related-link linkedin">
						<div class="rl-holder">
							<div class="icon"><i class="fa fa-linkedin"></i></div>
							<p> '. __("Art Bulgaria on Linkedin","pro") .'</p>
						</div>
					</div>

				</a>';
				break;

			*/
			case 'None':
			default:
				$output .= '';
				break;
		}
	}

	if ( $echo )
		echo $output;
	else
		return $output;
}


// Register Custom Post Type for bulletin

function custom_post_bulletin() {

	$labels = array(
		'name'                => _x( 'Bulletins', 'Post Type General Name', 'pro' ),
		'singular_name'       => _x( 'Bulletin', 'Post Type Singular Name', 'pro' ),
		'menu_name'           => __( 'Bulletin', 'pro' ),
		'name_admin_bar'      => __( 'Bulletin', 'pro' ),
		'parent_item_colon'   => __( 'Parent Item:', 'pro' ),
		'all_items'           => __( 'All Items', 'pro' ),
		'add_new_item'        => __( 'Add New Item', 'pro' ),
		'add_new'             => __( 'Add New', 'pro' ),
		'new_item'            => __( 'New Item', 'pro' ),
		'edit_item'           => __( 'Edit Item', 'pro' ),
		'update_item'         => __( 'Update Item', 'pro' ),
		'view_item'           => __( 'View Item', 'pro' ),
		'search_items'        => __( 'Search Item', 'pro' ),
		'not_found'           => __( 'Not found', 'pro' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'pro' ),
	);
	$args = array(
		'label'               => __( 'Bulletin', 'pro' ),
		'description'         => __( 'Post type for Bulletin', 'pro' ),
		'labels'              => $labels,
		'supports'            => array( ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'post_type_bulletin', $args );

}
add_action( 'init', 'custom_post_bulletin', 0 );


// Register Custom Post Type for Press Page

/******* LOGO ********/
function post_type_press_logo_new() {

	$labels = array(
		'name'                => _x( 'Logos', 'Post Type General Name', 'pro' ),
		'singular_name'       => _x( 'Download Logo', 'Post Type Singular Name', 'pro' ),
		'menu_name'           => __( 'Press - Logo', 'pro' ),
		'name_admin_bar'      => __( 'Press - Logo', 'pro' ),
		'parent_item_colon'   => __( 'Parent Item:', 'pro' ),
		'all_items'           => __( 'All Items', 'pro' ),
		'add_new_item'        => __( 'Add New Item', 'pro' ),
		'add_new'             => __( 'Add New', 'pro' ),
		'new_item'            => __( 'New Item', 'pro' ),
		'edit_item'           => __( 'Edit Item', 'pro' ),
		'update_item'         => __( 'Update Item', 'pro' ),
		'view_item'           => __( 'View Item', 'pro' ),
		'search_items'        => __( 'Search Item', 'pro' ),
		'not_found'           => __( 'Not found', 'pro' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'pro' ),
	);
	$args = array(
		'label'               => __( 'Download Logo', 'pro' ),
		'description'         => __( 'Press Page - Download Logo', 'pro' ),
		'labels'              => $labels,
		'supports'            => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'press_logo_new', $args );

}
add_action( 'init', 'post_type_press_logo_new', 0 );

/******* Content ********/
function post_type_press_content() {

	$labels = array(
		'name'                => _x( 'Contents', 'Post Type General Name', 'pro' ),
		'singular_name'       => _x( 'Post Content', 'Post Type Singular Name', 'pro' ),
		'menu_name'           => __( 'Press - Content', 'pro' ),
		'name_admin_bar'      => __( 'Press - Content', 'pro' ),
		'parent_item_colon'   => __( 'Parent Item:', 'pro' ),
		'all_items'           => __( 'All Items', 'pro' ),
		'add_new_item'        => __( 'Add New Item', 'pro' ),
		'add_new'             => __( 'Add New', 'pro' ),
		'new_item'            => __( 'New Item', 'pro' ),
		'edit_item'           => __( 'Edit Item', 'pro' ),
		'update_item'         => __( 'Update Item', 'pro' ),
		'view_item'           => __( 'View Item', 'pro' ),
		'search_items'        => __( 'Search Item', 'pro' ),
		'not_found'           => __( 'Not found', 'pro' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'pro' ),
	);
	$args = array(
		'label'               => __( 'Post Content', 'pro' ),
		'description'         => __( 'Press Page - Post Content', 'pro' ),
		'labels'              => $labels,
		'supports'            => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'press_content', $args );

}
add_action( 'init', 'post_type_press_content', 0 );

/******* Screenshots ********/
function post_type_press_screen() {

	$labels = array(
		'name'                => _x( 'Screenshots', 'Post Type General Name', 'pro' ),
		'singular_name'       => _x( 'Download Screenshot', 'Post Type Singular Name', 'pro' ),
		'menu_name'           => __( 'Press - Screenshot', 'pro' ),
		'name_admin_bar'      => __( 'Press - Screenshot', 'pro' ),
		'parent_item_colon'   => __( 'Parent Item:', 'pro' ),
		'all_items'           => __( 'All Items', 'pro' ),
		'add_new_item'        => __( 'Add New Item', 'pro' ),
		'add_new'             => __( 'Add New', 'pro' ),
		'new_item'            => __( 'New Item', 'pro' ),
		'edit_item'           => __( 'Edit Item', 'pro' ),
		'update_item'         => __( 'Update Item', 'pro' ),
		'view_item'           => __( 'View Item', 'pro' ),
		'search_items'        => __( 'Search Item', 'pro' ),
		'not_found'           => __( 'Not found', 'pro' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'pro' ),
	);
	$args = array(
		'label'               => __( 'Download Screenshot', 'pro' ),
		'description'         => __( 'Press Page - Download Screenshot', 'pro' ),
		'labels'              => $labels,
		'supports'            => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'press_screen', $args );

}
add_action( 'init', 'post_type_press_screen', 0 );


// Register Custom fields for Press Page

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_press-contents',
		'title' => 'Press Contents',
		'fields' => array (
			array (
				'key' => 'field_5704486488089',
				'label' => 'Press Content-400 words',
				'name' => 'press_content-400_words',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_570448a38808a',
				'label' => 'Press Content-100 words',
				'name' => 'press_content-100_words',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_570448c68808b',
				'label' => 'Press Content-50 words',
				'name' => 'press_content-50_words',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'press_content',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_press-page-logo',
		'title' => 'Press Page - Logo',
		'fields' => array (
			array (
				'key' => 'field_55de763bd945c',
				'label' => 'Press page Download logo',
				'name' => 'press_page_download_logo',
				'type' => 'image',
				'required' => 1,
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'press_logo_new',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_press-page-logo-file',
		'title' => 'Press Page Logo File (Optional)',
		'fields' => array (
			array (
				'key' => 'field_571ac7e584a50',
				'label' => 'Press Page Download Logo - File',
				'name' => 'press_page_download_logo_-_file',
				'type' => 'file',
				'save_format' => 'object',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'press_logo_new',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_press-page-screenshot',
		'title' => 'Press Page – Screenshot',
		'fields' => array (
			array (
				'key' => 'field_55dea5b4c5fae',
				'label' => 'ScreenShot',
				'name' => 'screenshot',
				'type' => 'image',
				'required' => 1,
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'press_screen',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

add_action('init', 'init_remove_support',100);
function init_remove_support(){
    $post_type = 'press_logo_new';
    remove_post_type_support( $post_type, 'editor');

    $post_type = 'press_content';
    remove_post_type_support( $post_type, 'editor');

    $post_type = 'press_screen';
    remove_post_type_support( $post_type, 'editor');
}