<?php get_header();

	$prefix = 'mpt';
	//$sidebar_position = get_option( $prefix . '_wp_page_sidebar_position');
	//$displaysidebar = get_option( $prefix . '_archive_search_page_display_sidebar');
	$sidebar_position = get_blog_option(1, $prefix . '_wp_page_sidebar_position');
	$displaysidebar = get_blog_option(1, $prefix . '_archive_search_page_display_sidebar');	
	$displaysidebar = ( !empty($displaysidebar) ? $displaysidebar : 'true' );
	$selected_sidebar = mpt_load_selected_sidebar('page');

	$columns = mpt_get_option( $prefix . '_blog_pages_layout' , '1col' , true );
	$poststyle = mpt_get_option( $prefix . '_blog_pages_box_style' , 'style-1' , true ); 
	$btncolor = mpt_get_option( $prefix . '_blog_pages_button_color' , '' , true );
	$showexcerpt = mpt_get_option( $prefix . '_blog_pages_show_excerpt' , 'yes' , true );
	$showmeta = mpt_get_option( $prefix . '_blog_pages_show_meta' , 'yes' , true );
	$showauthorname = mpt_get_option( $prefix . '_blog_pages_show_author_name' , 'yes' , true );
	$showcomments = mpt_get_option( $prefix . '_blog_pages_show_comments' , 'yes' , true );
	$showdatetag = mpt_get_option( $prefix . '_blog_pages_show_date_tag' , 'yes' , true );
	$datetagcolor = mpt_get_option( $prefix . '_blog_pages_date_tag_color' , '' , true );
	$metatagscolor = mpt_get_option( $prefix . '_blog_pages_meta_tag_color' , '' , true );
?>

	<!-- Page -->
	<div id="page-wrapper">

		<div class="content-section">
			<div class="outercontainer">
				<div class="container">

					<div class="row-fluid">

						<?php if ( ( empty($sidebar_position) || $sidebar_position == 'Left' ) && $displaysidebar == 'true' ) : ?>
							<div id="sidebar" class="span3">
								<?php get_sidebar( '2' ); ?>
							</div>
						<?php endif; ?>
						
						<div class="<?php echo ( $displaysidebar == 'true' ? 'span9' : 'span12' ); ?>">

							<div class="page-heading">

								<h1><span><?php _e('Search Results' , 'pro'); ?></span></h1>

							</div>

							<?php 

								$searchterms = isset($wp_query->query_vars['s']) ? get_query_var('s') : '';

								switch ($btncolor) {
									case 'grey': $btnclass = ' btn-default'; break;
									case 'blue': $btnclass = ' btn-primary'; break;
									case 'lightblue': $btnclass = ' btn-info'; break;
									case 'green': $btnclass = ' btn-success'; break;
									case 'yellow': $btnclass = ' btn-warning'; break;
									case 'red': $btnclass = ' btn-danger'; break;
									case 'black': $btnclass = ' btn-inverse'; break;
									default: $btnclass = ''; break;			
								}

								$args = array(
									'searchterms' => esc_attr( $searchterms ),
									'issearchpage' => true,
									'columns' => $columns,
									'poststyle' => ( $poststyle == 'style-2' ? 'style-2' : 'style-1' ),
									'btnclass' => ( !empty($btnclass) ? $btnclass : '' ),
									'boxclass' => ( $poststyle == 'style-2' ? 'post-grid' : '' ),
									'extra' => array(
										'showexcerpt' => ( $showexcerpt == 'yes' ? true : false ),
										'showmeta' => ( $showmeta == 'yes' ? true : false ),
										'showauthordetails' => ( $showauthorname == 'yes' ? true : false ),
										'showcomments' => ( $showcomments == 'yes' ? true : false ),
										'showhovertag' => ( $showdatetag == 'yes' ? true : false ),
										'hovertagcolor' => ( !empty($datetagcolor) ? 'hover-tag-' . $datetagcolor : '' ),
										'metatagscolor' => ( !empty($metatagscolor) ? 'meta-tag-' . $metatagscolor : '' )
										)
								);

							?>

							<?php pro_display_wp_post_query( apply_filters( 'pro_post_query_search_page' , $args ) ); ?>

						</div><!-- / span9 -->

						<?php if ( !empty($sidebar_position) && $sidebar_position == 'Right' && $displaysidebar == 'true' ) : ?>
							<div id="sidebar" class="span4">
								<?php get_sidebar( $selected_sidebar ); ?>
							</div>
						<?php endif; ?>

					</div><!-- / row-fluid -->

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>