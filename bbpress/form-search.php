<?php

/**
 * Search 
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<form role="search" method="get" id="bbp-search-form" action="<?php bbp_search_url(); ?>">
	<?php /* Original.. Modified by Mae.<!--<div>
		<label class="screen-reader-text hidden" for="bbp_search"><?php _e( 'Search for:', 'pro' ); ?></label>
		<input type="hidden" name="action" value="bbp-search-request" />
		<input tabindex="<?php bbp_tab_index(); ?>" type="text" value="<?php echo esc_attr( bbp_get_search_terms() ); ?>" name="bbp_search" id="bbp_search" />
		<input tabindex="<?php bbp_tab_index(); ?>" class="button" type="submit" id="bbp_search_submit" value="<?php esc_attr_e( 'Search', 'pro' ); ?>" />
	</div>--> */ ?>
	 <div class="input-append">
			<label class="screen-reader-text hidden" for="bbp_search"><?php _e( 'Search for:', 'pro' ); ?></label>
			<input type="hidden" name="action" value="bbp-search-request" />
			<input id="appendedInputButton" class="input-medium" tabindex="<?php bbp_tab_index(); ?>" type="text" value="<?php echo esc_attr( bbp_get_search_terms() ); ?>" name="bbp_search" id="bbp_search" placeholder="<?php __('Search in forum'); ?>" />	
			<button id="bbp_search_submit" tabindex="<?php bbp_tab_index(); ?>"  class="btn"  type="submit"><i class="fa fa-search"></i></button>
	</div>
	
</form>
