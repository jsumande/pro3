<?php

/**
 * Forums Loop - Single Forum
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<ul id="bbp-forum-<?php bbp_forum_id(); ?>" <?php bbp_forum_class();?>>

	<li class="bbp-forum-info">
		
		<?php if ( bbp_is_user_home() && bbp_is_subscriptions() ) : ?>

			<span class="bbp-row-actions">

				<?php do_action( 'bbp_theme_before_forum_subscription_action' ); ?>

				<?php bbp_forum_subscription_link( array( 'before' => '', 'subscribe' => '+', 'unsubscribe' => '&times;' ) ); ?>

				<?php do_action( 'bbp_theme_after_forum_subscription_action' ); ?>

			</span>

		<?php endif; ?>

		<?php do_action( 'bbp_theme_before_forum_title' ); 
		$sticky = get_post_meta( bbp_get_forum_id(), 'bbp_forum_trsticky', true );
		?>
		
		<div class="folder-topic"<?php echo $sticky == 1 ? ' data-toggle="tooltip" title="" data-original-title="Important Forum Discussion"' : ''; ?>></div>
		<div class="title-topic">	
			<a href="<?php bbp_forum_permalink(); ?>"><?php _e(bbp_forum_title(),'bbpress'); ?></a>
			<div class="bbp-forum-content"><?php _e(bbp_forum_content(),'bbpress'); ?></div>
		</div>	
			<?php do_action( 'bbp_theme_after_forum_title' ); ?>

			<?php do_action( 'bbp_theme_before_forum_description' ); ?>

							
			
		<?php do_action( 'bbp_theme_after_forum_description' ); ?>

		<?php do_action( 'bbp_theme_before_forum_sub_forums' ); ?>

		<?php bbp_list_forums(); ?>

		<?php do_action( 'bbp_theme_after_forum_sub_forums' ); ?>

		<?php bbp_forum_row_actions(); ?>
	
	</li>

	<li class="bbp-forum-topic-count"><?php bbp_forum_topic_count(); ?></li>

	<li class="bbp-forum-reply-count"><?php bbp_show_lead_topic() ? bbp_forum_reply_count() : bbp_forum_post_count(); ?></li>

	<li class="bbp-forum-freshness">
		<?php if(bbp_get_forum_last_active_id()){ ?>
			
		
		<?php do_action( 'bbp_theme_before_forum_freshness_link' ); ?>

		<?php bbp_forum_freshness_link(); ?>

		<?php do_action( 'bbp_theme_after_forum_freshness_link' ); ?>

		<p class="bbp-topic-meta">

			<?php do_action( 'bbp_theme_before_topic_author' ); ?>

			<span class="bbp-topic-freshness-author">			
				<?php if(bbp_get_forum_last_active_id()){
				$last_post_id = bbp_get_forum_last_active_id(bbp_get_forum_last_topic_id()); 
				$last_user_id = bbp_get_topic_author_id($last_post_id); ?>
				<?php echo _x('Author:', 'bbpress'); ?> <a href="<?php bbp_topic_author_url($last_post_id) ?>"><?php $user = get_user_by( 'id',$last_user_id ); echo $user->user_login ; ?></a>
				<?php }  ?>
			</span>

			<?php do_action( 'bbp_theme_after_topic_author' ); ?>

		</p>
		<?php } else { ?>
		<li class="no-topic bbp-forum-freshness"><?php _e('No Topic','bbpress'); ?></li>
		<?php } ?>
	</li>
	<li class="bbp-forum-arrow">
	<?php if(bbp_get_forum_last_active_id()){ ?>
	<a data-toggle="tooltip" title="" data-original-title="<?php _e('Go to last post','bbpress'); ?>" href="<?php bbp_forum_last_topic_permalink(); ?>"><i class="fa fa-angle-double-right"></a></i></li>
	<?php } ?>
</ul><!-- #bbp-forum-<?php bbp_forum_id(); ?> -->
