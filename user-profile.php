<?php
// **********************************************************
// If you want to have an own template for this action
// just copy this file into your current theme folder
// and change the markup as you want to
// **********************************************************
if ( ! is_user_logged_in() ) {
	wp_safe_redirect( home_url( '/user-login/' ) );
	exit;
}

// get profile user
$user_get_id = get_current_user_id();
$user = get_userdata($user_get_id);
$user->filter = 'edit';
$profileuser = $user;
$shipping_info = get_user_meta($user_get_id, 'mp_shipping_info',true);
$billing_info = get_user_meta($user_get_id, 'mp_billing_info',true);

$profileuser->email = isset($profileuser->email)? $profileuser->email : '';
$profileuser->name = isset($profileuser->name)? $profileuser->name : '';
$profileuser->address1 = isset($profileuser->address1)? $profileuser->address1 : '';
$profileuser->address2 = isset($profileuser->address2)? $profileuser->address2 : '';
$profileuser->city = isset($profileuser->city)? $profileuser->city : '';
$profileuser->state = isset($profileuser->state)? $profileuser->state : '';
$profileuser->zip = isset($profileuser->zip)? $profileuser->zip : '';
$profileuser->country = isset($profileuser->country)? $profileuser->country : '';
$profileuser->phone = isset($profileuser->phone)? $profileuser->phone : '';


$errorcode = '';

if(isset($_SESSION['proucode'])) {
	$errorcode = $_SESSION['proucode'];
}

?>
<?php get_header(); ?>
<div id="page-wrapper">
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
				<div class="row no-margin">
						<div class="col-md-12 col-sm-12 col-xs-12">
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div id="breadcrumb">
						<div class="row">
							 <div class="col-md-12">
								<ul class="breadcrumbs-list">
									<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
									<li class="active"><?php _e( 'My Account', UF_TEXTDOMAIN ); ?></li>
								</ul>
							 </div>
							</div>
					</div>
					<div class="clear padding10"></div>

						<div id="user-profile" class="page-content ">
					
					<?php echo isset($errorcode['success'])? '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button> '.$errorcode['success'].'</div>' : ''; ?>
					<?php echo isset($errorcode['error_process'])? '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button> '.$errorcode['error_process'].'</div>' : ''; ?>
						<form action="<?php echo uf_get_action_url( 'profile' ); ?>" method="post" <?php do_action( 'user_edit_form_tag' ); ?>>
							<?php wp_nonce_field( 'profile', 'wp_uf_nonce_profile' ); ?>
							<input type="hidden" name="lang" value="<?php echo ICL_LANGUAGE_CODE; ?>"/>
							
						<!-- Name -->
						<h3 class="user-profile-title"><?php _e("Name", "mp"); ?></h3>						
						<div class="by-section">
							<div class="row">					
								<div class="col-md-6">
									<div class="form-group">
										<label for="first_name"><span class="required">*</span><?php _e( 'First Name' ) ?>:</label>
										<?php 
											if(isset($errorcode['up_first_name_error'])) {		
												$pu_b1 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_first_name_error'].'</div>';
											}
										?>										
										<input type="text" name="first_name" id="first_name" value="<?php echo esc_attr( $profileuser->first_name ) ?>" class="form-control <?php echo isset($pu_b1)? $pu_b1: '';?>"/>
									</div>									
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<?php 
											if(isset($errorcode['up_last_name_error'])) {		
												$pu_b2 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_last_name_error'].'</div>';
											}
										?>									
										<label for="last_name"><span class="required">*</span><?php _e( 'Last Name' ) ?>:</label>
										<input type="text" name="last_name" id="last_name" value="<?php echo esc_attr( $profileuser->last_name ) ?>" class="form-control <?php echo isset($pu_b2)? $pu_b2: '';?>"/>
									</div>
								</div>
							</div> <!--./row-->
						</div>
					<!-- ./Name -->
					
					<!-- Contact Info -->
						<h3 class="user-profile-title"><?php _e("Contact Info", "mp"); ?></h3>						
						<div class="by-section">
							<div class="row">					
								<div class="col-md-6">
									<div class="form-group">
										<label for="email"><span class="required">*</span><?php _e( 'E-mail' ); ?> :</label>
										<?php 
											if(isset($errorcode['up_email_error']) || isset($errorcode['up_email_invalid_error']) || isset($errorcode['up_email_inuse_error'])) {		
												$pu_b3 = ' input-border-error';
												
												if(isset($errorcode['up_email_error']))
													$pu_m3 = $errorcode['up_email_error'];
												if(isset($errorcode['up_email_invalid_error']))
													$pu_m3 = $errorcode['up_email_invalid_error'];
												if(isset($errorcode['up_email_inuse_error']))
													$pu_m3 = $errorcode['up_email_inuse_error'];
												
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$pu_m3.'</div>';
											}
										?>										
										<input type="text" name="email" id="email" value="<?php echo esc_attr( $profileuser->user_email ) ?>" class="form-control <?php echo isset($pu_b3)? $pu_b3: '';?>"/>
									</div>									
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="url"><span class="required"></span><?php _e( 'Website' ) ?>:</label>
										<input type="text" name="url" id="url" value="<?php echo esc_attr( $profileuser->user_url ) ?>"  class="form-control"/>
									</div>
								</div>
							</div> <!--./row-->
						</div>
					<!-- ./Contact Info -->
					
					<!-- Password -->						
						<?php $show_password_fields = apply_filters( 'show_password_fields', TRUE, $profileuser ); 
						if ( $show_password_fields ) { ?>
						<h3 class="user-profile-title"><?php _e("Password", "mp"); ?></h3>
						<div id="password-section" class="by-section">
							<div class="row">					
								<div class="col-md-6">
									<div class="form-group">
										<label for="pass1" class="pass1"><?php _e( 'Password :' ); ?></label>
										<span class="description-pass"><?php _e( 'If you would like to change the password type a new one. Otherwise leave this blank.' ); ?></span>
										
										<?php 
											if(isset($errorcode['up_password_error']) || isset($errorcode['up_password_empty_error'])) {		
												$pu_b4 = ' input-border-error';
												
												if(isset($errorcode['up_password_error']))
													$pu_m4 = $errorcode['up_password_error'];
												if(isset($errorcode['up_password_empty_error']))
													$pu_m4 = $errorcode['up_password_empty_error'];
												
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$pu_m4.'</div>';
											}
										?>										
										
										<input type="password" name="pass1" id="pass1" class="form-control <?php echo isset($pu_b4)? $pu_b4: '';?>" size="16" value="" autocomplete="off" />
									</div>									
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="pass2" class="pass2"><?php _e( 'New Password :' ); ?></label>
										<span class="description-pass"><?php _e( 'Type your new password again.'); ?></span>
										<?php 
											if(isset($errorcode['up_password2_empty_error']) || isset($errorcode['up_password2_lenght_error'])) {		
												$pu_b5 = ' input-border-error';
												
												if(isset($errorcode['up_password2_empty_error']))
													$pu_m5 = $errorcode['up_password2_empty_error'];
												if(isset($errorcode['up_password2_lenght_error']))
													$pu_m5 = $errorcode['up_password2_lenght_error'];
												
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$pu_m5.'</div>';
											}
										?>											
										<input type="password" name="pass2" class="form-control <?php echo isset($pu_b5)? $pu_b5: '';?>" id="pass2" size="16" value="" autocomplete="off" />
									</div>
								</div>
							</div> <!--./row-->
							<div class="row">
								<div class="col-md-12">
										<div id="pass-strength-result"><?php _e( 'Strength indicator' ); ?></div>
										<p class="description indicator-hint"><?php _e( 'Hint: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers and symbols like ! " ? $ % ^ &amp; ).' ,'mp'); ?></p>
								</div>
							</div> <!--./row-->
						</div>
						<?php } ?>
					<!-- ./Password -->
					
					<!-- Shipping Info -->
						<h3 class="user-profile-title"><?php _e("Shipping Info", "mp"); ?></h3>						
						<div class="by-section"> <!--1-->
							<div class="row">					
								<div class="col-md-6">
									<div class="form-group">
										<label for="mp_shipping_info_name"><span class="required">*</span><?php _e( 'Full Name:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_shipping_fullname_error'])) {		
												$pu_b6 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_shipping_fullname_error'].'</div>';
											}
										?>
										<input class="form-control <?php echo isset($pu_b6)? $pu_b6: '';?>" id="mp_shipping_info_name" name="mp_shipping_info[name]" value="<?php echo isset($shipping_info['name'])? $shipping_info['name'] : ''; ?>" type="text">
									</div>
									<div class="form-group">
										<label for="mp_shipping_info_address1"><span class="required">*</span><?php _e( 'Address:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_shipping_address1_error'])) {		
												$pu_b7 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_shipping_address1_error'].'</div>';
											}
										?>
										<input class="form-control <?php echo isset($pu_b7)? $pu_b7: '';?>" id="mp_shipping_info_address1" name="mp_shipping_info[address1]" value="<?php echo isset($shipping_info['address1'])? $shipping_info['address1']: ''; ?>" type="text">
									</div>
									<div class="form-group" id="bulgaria-country-shipping">
										<label for="mp_shipping_info_country"><span class="required">*</span><?php _e( 'Country:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_shipping_country_error'])) {		
												$pu_b9 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_shipping_country_error'].'</div>';
											}
										?>
										<?php
											$countries = array("Bulgaria", "United States", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
											?>
											<select id="country" name="mp_shipping_info[country]" class="form-control <?php echo isset($pu_b9)? $pu_b9: '';?>">
												<?php
												$shipping_info['country'] = isset($shipping_info['country'])? $shipping_info['country'] : '';
												foreach ($countries as $country) { ?>
													<option value="<?php echo $country; ?>" <?php echo(($shipping_info['country'] == $country) ? 'selected' : ''); ?> ><?php echo $country; ?></option>
												<?php } ?>
											</select>
									</div>

									<?php
								       $cities = '';
										if( ICL_LANGUAGE_CODE == 'bg' ) {
										       $cities = array
												  (
												 	array("","Всички"),
													array("Aytos","Айтос"),
													array("Aksakovo","Аксаково"),
													array("Alfatar","Алфатар"),
													array("Antonovo","Антоново"),
													array("Apriltsi","Априлци"),
													array("Ardino","Ардино"),
													array("Asenovgrad","Асеновград"),
													array("Aheloy","Ахелой"),
													array("Ahtopol","Ахтопол"),
													array("Balchik","Балчик"),
													array("Bankya","Банкя"),
													array("Bansko","Банско"),
													array("Banya","Баня"),
													array("Batak","Батак"),
													array("Batanovtsi","Батановци"),
													array("Belene","Белене"),
													array("Belitsa","Белица"),
													array("Belovo","Белово"),
													array("Belogradchik","Белоградчик"),
													array("Beloslav","Белослав"),
													array("Berkovitsa","Берковица"),
													array("Blagoevgrad","Благоевград"),
													array("Bobov Dol","Бобов дол"),
													array("Boboshevo","Бобошево"),
													array("Bozhurishte","Божурище"),
													array("Boychinovtsi","Бойчиновци"),
													array("Bolyarovo","Болярово"),
													array("Borovo","Борово"),
													array("Botevgrad","Ботевград"),
													array("Bratsigovo","Брацигово"),
													array("Bregovo","Брегово"),
													array("Breznik","Брезник"),
													array("Brezovo","Брезово"),
													array("Brusartsi","Брусарци"),
													array("Burgas","Бургас"),
													array("Buhovo","Бухово"),
													array("Balgarovo","Българово"),
													array("Byala","Бяла"),
													array("Byala Slatina","Бяла Слатина"),
													array("Byala Cherkva","Бяла Черква"),
													array("Varbitsa","Върбица"),
													array("Varshets","Вършец"),
													array("Veliki Preslav","Велики Преслав"),
													array("Veliko Tarnovo","Велико Търново"),
													array("Velingrad","Велинград"),
													array("Vetovo","Ветово"),
													array("Vetren","Ветрен"),
													array("Vidin","Видин"),
													array("Ugarchin","Угърчин"),
													array("Valchedram","Вълчедръм"),
													array("Valchi Dol","Вълчи дол"),
													array("Varna","Варна"),
													array("Gabrovo","Габрово"),
													array("General Toshevo","Генерал Тошево"),
													array("Glavinitsa","Главиница"),
													array("Glodzhevo","Глоджево"),
													array("Godech","Годеч"),
													array("Gorna Oryahovitsa","Горна Оряховица"),
													array("Gotse Delchev","Гоце Делчев"),
													array("Gramada","Грамада"),
													array("Gulyantsi","Гулянци"),
													array("Gurkovo","Гурково"),
													array("Galabovo","Гълъбово"),
													array("Dve Mogili","Две могили"),
													array("Debelets","Дебелец"),
													array("Devin","Девин"),
													array("Devnya","Девня"),
													array("Dzhebel","Джебел"),
													array("Dimitrovgrad","Димитровград"),
													array("Dimovo","Димово"),
													array("Dobrinishte","Добринище"),
													array("Dobrich","Добрич"),
													array("Dolna Banya","Долна Баня"),
													array("Dolna Mitropoliya","Долна Митрополия"),
													array("Dolna Oryahovitsa","Долна Оряховица"),
													array("Dolni Dabnik","Долни Дъбник"),
													array("Dolni Chiflik","Долни Чифлик"),
													array("Dospat","Доспат"),
													array("Dragoman","драгоман"),
													array("Dryanovo","Дряново"),
													array("Dulovo","Дулово"),
													array("Dunavtsi","Дунавци"),
													array("Dupnitsa","Дупница"),
													array("Dalgopol","Дългопол"),
													array("Elena","Елена"),
													array("Elin Pelin","Елин Пелин"),
													array("Elhovo","Елхово"),
													array("Etropole","Етрополе"),
													array("Novi Pazar","Нови Пазар"),
													array("Obzor","Обзор"),
													array("Omurtag","Омуртаг"),
													array("Opaka","Опака"),
													array("Oryahovo","Оряхово"),
													array("Panagyurishte","Панагюрище"),
													array("Ivaylovgrad","Ивайловград"),
													array("Ignatievo","Игнатиево"),
													array("Iskar","Искър"),
													array("Isperih","Исперих"),
													array("Ihtiman","Ихтиман"),
													array("Kableshkovo","Каблешково"),
													array("Kavarna","Каварна"),
													array("Kazanlak","Казанлък"),
													array("Kalofer","Калофер"),
													array("Kameno","Калофер"),
													array("Kaolinovo","Камено"),
													array("Karlovo","Каолиново"),
													array("Karnobat","Карлово"),
													array("Kaspichan","Карнобат"),
													array("Kermen","Каспичан"),
													array("Kilifarevo","Кермен"),
													array("Kiten","Килифарево"),
													array("Klisura","Китен"),
													array("Knezha","Клисура"),
													array("Kozloduy","Кнежа"),
													array("Koynare","Козлодуй"),
													array("Koprivshtitsa","Койнаре"),
													array("Kostandovo","Копривщица"),
													array("Kostenets","Костандово"),
													array("Kostinbrod","Костенец"),
													array("Kotel","Костинброд"),
													array("Kocherinovo","Котел"),
													array("Knezha","Кочериново"),
													array("Krivodol","Кнежа"),
													array("Krichim","Криводол"),
													array("Krumovgrad","Кричим"),
													array("Kran","Крумовград"),
													array("Kubrat","Крън"),
													array("Kuklen","Кубрат"),
													array("Kula","Куклен"),
													array("Kardzhal","	Kardzhal"),
													array("Kyustendil","Кула"),
													array("Levski","Кюстендил"),
													array("Letnitsa","Левски"),
													array("Lovech","Летница"),
													array("Loznitsa","Ловеч"),
													array("Lom","Лозница"),
													array("Lukovit","Лом"),
													array("Laki","Луковит"),
													array("Lyubimets","Лъки"),
													array("Lyaskovets","Любимец"),
													array("Madan","Лясковец"),
													array("Malko Tarnovo","Малко Търново"),
													array("Marten","Мадан"),
													array("Mezdra","бялка"),
													array("Merichleri","Мездра"),
													array("Miziya","Меричлери"),
													array("Momin","Prohod	Мизия"),
													array("Momchilgrad","Момин проход"),
													array("Montana","Момчилград"),
													array("Maglizh","Монтана"),
													array("Madzharo","Madzharo"),
													array ("Melnik","Мелник"),
													array("Nedelino","Мъглиж"),
													array("Nesebar","Несебър"),
													array("Nikolaevo","Неделино"),
													array("Nikopol","Несебър"),
													array("Nova Zagora","Николаево"),
													array("Novi Iskar","Никопол"),
													array("Novi Pazar","Нова Загора"),
													array("Obzor","Нови Искър"),
													array("Omurtag","Нови Пазар"),
													array("Opaka","Обзор"),
													array("Oryahovo","Омуртаг"),
													array("Pavel Banya","Опака"),
													array("Pavlikeni","Оряхово"),
													array("Pazardzhik","Павел Баня"),
													array("Panagyurishte","Павликени"),
													array("Pernik","Пазарджик"),
													array("Perushtitsa","Панагюрище"),
													array("Petrich","Перник"),
													array("Peshtera","Перущица"),
													array("Pirdop","Петрич"),
													array("Plachkovtsi","Пещера"),
													array("Pleven","Пирдоп"),
													array("Pliska","Плачковци"),
													array("Plovdiv","Плевен"),
													array("Polski Trambesh","Плиска"),
													array("Pomorie","Пловдив"),
													array("Popovo","Полски Тръмбеш"),
													array("Pordim","Поморие"),
													array("Pravets","Попово"),
													array("Primorsko","Пордим"),
													array("Provadiya","Правец"),
													array("Parvomay","Приморско"),
													array("Radnevo","Провадия"),
													array("Radomir","Първомай"),
													array("Razgrad","Раднево"),
													array("Razlog","Радомир"),
													array("Rakitovo","Разград"),
													array("Rakovski","Разлог"),
													array("Rila","Ракитово"),
													array("Roman","Раковски"),
													array("Rudozem","Рила"),
													array("Ruse","Yловка"),
													array("Sadovo","Рудозем"),
													array("Sandanski","Садово"),
													array("Sapareva Banya","Самоков"),
													array("Sveti Vlas","Сандански"),
													array("Suvorovo","Сапарева баня"),
													array("Svilengrad","Свети Влас"),
													array("Svishtov","Суворово"),
													array("Septemvri","Свиленград"),
													array("Sarnitsa","Свищов"),
													array("Senovo","Септември"),
													array("Shumen","Сърница"),
													array("Silistra","Сеново"),
													array("Simitli","Силистра"),
													array("Slavyanovo","Симеоновград"),
													array("Sliven","Симитли"),
													array("Slivnitsa","Славяново"),
													array("Slivo Pole","Сливен"),
													array("Smolyan","Сливница"),
													array("Sopot","Сливо поле"),
													array("Sofia","Смолян"),
													array("Smyadovo","Сопот"),
													array("Sozopol","София"),
													array("Sredets","Смядово"),
													array("Stamboliyski","Созопол"),
													array("Straldzha","Средец"),
													array("Stara Zagora","Стамболийски"),
													array("Strazhitsa","Стралджа"),
													array("Sungurlare","Стара Загора"),
													array("Suhindol","Стражица"),
													array("Strelcha	","Сунгурларе"),
													array("Saedinenie","Сухиндол"),
													array("Sapareva Banya","Стрелча"),
													array("Tutrakan","Съединение"),
													array("Targovishte","Сапарева баня"),
													array("Tervel","Тутракан"),
													array("Teteven","Търговище"),
													array("Trastenik","Тервел"),
													array("Topolovgrad","Тетевен"),
													array("Tran","Тръстеник"),
													array("Troyan","Тополовград"),
													array("Tsarevo","Тran"),
													array("Svoge","Троян"),
													array("Tvarditsa","Царево"),
													array("Hadzhidimovo","Своге"),
													array("Harmanli","Твърдица"),
													array("Haskovo","Хаджидимово"),
													array("Hisarya","Харманли"),
													array("Tryavna","Хасково"),
													array("Tsar Kaloyan","Хисаря"),
													array("Chepelare","Трявна"),
													array("Cherven Bryag","Цар Калоян"),
													array("Chernomorets","Чепеларе"),
													array("Chiprovtsi","Червен Бряг"),
													array("Chirpan Stara","Черноморец"),
													array("Sevlievo","Чипровци"),
													array("Shipka","Чирпан"),
													array("Vidin","Шабла"),
													array("Vratsa","Шивачево"),
													array("Shivachevo","Шипка"),
													array("Simeonovgrad","Ябланица"),
													array("Yakoruda","Якоруда"),
													array("Yambol","Ямбол"),
													array("Jablanica","Шумен"),
													array("Shabla","Севлиево"),
													array("Yablanitsa","Видин")
												  );

										} else {
										       $cities = array
												 (
												 	array("","All"),
													array ("Aheloy","Aheloy"),
													array ("Ahtopol","Ahtopol"),
													array ("Aksakovo","Aksakovo"),
													array ("Alfatar","Alfatar"),
													array ("Antonovo","Antonovo"),
													array ("Apriltsi","Apriltsi"),
													array ("Ardino","Ardino"),
													array ("Asenovgrad","Asenovgrad"),
													array ("Aytos","Aytos"),
													array ("Balchik","Balchik"),
													array ("Balgarovo","Balgarovo"),
													array ("Bankya","Bankya"),
													array ("Bansko","Bansko"),
													array ("Banya","Banya"),
													array ("Batak","Batak"),
													array ("Batanovtsi","Batanovtsi"),
													array ("Belene","Belene"),
													array ("Belitsa","Belitsa"),
													array ("Belogradchik","Belogradchik"),
													array ("Beloslav","Beloslav"),
													array ("Belovo","Belovo"),
													array ("Berkovitsa","Berkovitsa"),
													array ("Blagoevgrad","Blagoevgrad"),
													array ("Boboshevo","Boboshevo"),
													array ("Bobov Dol","Bobov Dol"),
													array ("Bolyarovo","Bolyarovo"),
													array ("Borovo","Borovo"),
													array ("Botevgrad","Botevgrad"),
													array ("Boychinovtsi","Boychinovtsi"),
													array ("Bozhurishte","Bozhurishte"),
													array ("Bratsigovo","Bratsigovo"),
													array ("Bregovo","Bregovo"),
													array ("Breznik","Breznik"),
													array ("Brezovo","Brezovo"),
													array ("Brusartsi","Brusartsi"),
													array ("Buhovo","Buhovo"),
													array ("Burgas","Burgas"),	
													array ("Byala","Byala"),
													array ("Byala Cherkva","Byala Cherkva"),
													array ("Byala Slatina","Byala Slatina"),
													array ("Chepelare","Chepelare"),
													array ("Chernomorets","Chernomorets"),
													array ("Cherven Bryag","Cherven Bryag"),
													array ("Chiprovtsi","Chiprovtsi"),
													array ("Chirpan","Chirpan"),
													array ("Dalgopol","Dalgopol"),
													array ("Debelets","Debelets"),
													array ("Devin","Devin"),
													array ("Devnya","Devnya"),
													array ("Dimitrovgrad","Dimitrovgrad"),
													array ("Dimovo","Dimovo"),
													array ("Dobrich","Dobrich"),
													array ("Dobrinishte","Dobrinishte"),
													array ("Dolna Banya","Dolna Banya"),
													array ("Dolna Mitropoliya","Dolna Mitropoliya"),
													array ("Dolna Oryahovitsa","Dolna Oryahovitsa"),
													array ("Dolni Chiflik","Dolni Chiflik"),
													array ("Dolni Dabnik","Dolni Dabnik"),
													array ("Dospat","Dospat"),
													array ("Dragoman","Dragoman"),
													array ("Dryanovo","Dryanovo"),
													array ("Dulovo","Dulovo"),
													array ("Dunavtsi","Dunavtsi"),
													array ("Dupnitsa","Dupnitsa"),
													array ("Dve Mogili","Dve Mogili"),
													array ("Dzhebel","Dzhebel"),
													array ("Elena","Elena"),
													array ("Elhovo","Elhovo"),
													array ("Elin Pelin","Elin Pelin"),
													array ("Etropole","Etropole"),
													array ("Gabrovo","Gabrovo"),
													array ("Galabovo","Galabovo"),
													array ("General Toshevo","General Toshevo"),
													array ("Glavinitsa","Glavinitsa"),
													array ("Glodzhevo","Glodzhevo"),
													array ("Godech","Godech"),
													array ("Gorna Oryahovitsa","Gorna Oryahovitsa"),
													array ("Gotse Delchev","Gotse Delchev"),
													array ("Gramada","Gramada"),
													array ("Gulyantsi","Gulyantsi"),
													array ("Gurkovo","Gurkovo"),
													array ("Hadzhidimovo","Hadzhidimovo"),
													array ("Harmanli","Harmanli"),
													array ("Haskovo","Haskovo"),
													array ("Hisarya","Hisarya"),
													array ("Ignatievo","Ignatievo"),
													array ("Ihtiman","Ihtiman"),
													array ("Iskar","Iskar"),
													array ("Isperih","Isperih"),
													array ("Ivaylovgrad","Ivaylovgrad"),
													array ("Jablanica","Jablanica"),
													array ("Kableshkovo","Kableshkovo"),
													array ("Kalofer","Kalofer"),
													array ("Kameno","Kameno"),
													array ("Kaolinovo","Kaolinovo"),
													array ("Kardzhali","Kardzhali"),
													array ("Karlovo","Karlovo"),
													array ("Karnobat","Karnobat"),
													array ("Kaspichan","Kaspichan"),
													array ("Kavarna","Kavarna"),
													array ("Kazanlak","Kazanlak"),
													array ("Kermen","Kermen"),
													array ("Kilifarevo","Kilifarevo"),
													array ("Kiten","Kiten"),
													array ("Klisura","Klisura"),
													array ("Knezha","Knezha"),
													array ("Kocherinovo","Kocherinovo"),
													array ("Koprivshtitsa","Koprivshtitsa"),
													array ("Kostandovo","Kostandovo"),
													array ("Kostenets","Kostenets"),
													array ("Kostinbrod","Kostinbrod"),
													array ("Kotel","Kotel"),
													array ("Koynare","Koynare"),
													array ("Kozloduy","Kozloduy"),
													array ("Kran","Kran"),
													array ("Kresna","Kresna"),
													array ("Krichim","Krichim"),
													array ("Krivodol","Krivodol"),
													array ("Krumovgrad","Krumovgrad"),
													array ("Kubrat","Kubrat"),
													array ("Kuklen","Kuklen"),
													array ("Kula","Kula"),
													array ("Kyustendil","Kyustendil"),
													array ("Laki","Laki"),
													array ("Letnitsa","Letnitsa"),
													array ("Levski","Levski"),
													array ("Lom","Lom"),
													array ("Lovech","Lovech"),
													array ("Loznitsa","Loznitsa"),
													array ("Lukovit","Lukovit"),
													array ("Lyaskovets","Lyaskovets"),
													array ("Lyubimets","Lyubimets"),
													array ("Madan","Madan"),
													array ("Madzharo","Madzharo"),
													array ("Maglizh","Maglizh"),
													array ("Malko Tarnovo","Malko Tarnovo"),
													array ("Marten","Marten"),
													array ("Merichleri","Merichleri"),
													array ("Mezdra","Mezdra"),
													array ("Melnik","Melnik"),
													array ("Miziya","Miziya"),
													array ("Momchilgrad","Momchilgrad"),
													array ("Momin Prohod","Momin Prohod"),
													array ("Montana","Montana"),
													array ("Nedelino","Nedelino"),
													array ("Nesebar","Nesebar"),
													array ("Nikolaevo","Nikolaevo"),
													array ("Nikopol","Nikopol"),
													array ("Nova Zagora","Nova Zagora"),
													array ("Novi Iskar","Novi Iskar"),
													array ("Novi Pazar","Novi Pazar"),
													array ("Obzor","Obzor"),
													array ("Omurtag","Omurtag"),
													array ("Opaka","Opaka"),
													array ("Oryahovo","Oryahovo"),
													array ("Panagyurishte","Panagyurishte"),
													array ("Parvomay","Parvomay"),
													array ("Pavel Banya","Pavel Banya"),
													array ("Pavlikeni","Pavlikeni"),
													array ("Pazardzhik","Pazardzhik"),
													array ("Pernik","Pernik"),
													array ("Perushtitsa","Perushtitsa"),
													array ("Peshtera","Peshtera"),
													array ("Petrich","Petrich"),
													array ("Pirdop","Pirdop"),
													array ("Plachkovtsi","Plachkovtsi"),
													array ("Pleven","Pleven"),
													array ("Pliska","Pliska"),
													array ("Plovdiv","Plovdiv"),
													array ("Polski Trambesh","Polski Trambesh"),
													array ("Pomorie","Pomorie"),
													array ("Popovo","Popovo"),
													array ("Pordim","Pordim"),
													array ("Pravets","Pravets"),
													array ("Primorsko","Primorsko"),
													array ("Provadiya","Provadiya"),
													array ("Radnevo","Radnevo"),
													array ("Radomir","Radomir"),
													array ("Rakitovo","Rakitovo"),
													array ("Rakovski","Rakovski"),
													array ("Razgrad","Razgrad"),
													array ("Razlog","Razlog"),
													array ("Rila","Rila"),
													array ("Roman","Roman"),
													array ("Rudozem","Rudozem"),
													array ("Ruse","Ruse"),
													array ("Sadovo","Sadovo"),
													array ("Saedinenie","Saedinenie"),
													array ("Samokov","Samokov"),
													array ("Sandanski","Sandanski"),
													array ("Sapareva Banya","Sapareva Banya"),
													array ("Sarnitsa","Sarnitsa"),
													array ("Senovo","Senovo"),
													array ("Septemvri","Septemvri"),
													array ("Sevlievo","Sevlievo"),
													array ("Shabla","Shabla"),
													array ("Shipka","Shipka"),
													array ("Shivachevo","Shivachevo"),
													array ("Shumen","Shumen"),
													array ("Silistra","Silistra"),
													array ("Simeonovgrad","Simeonovgrad"),
													array ("Simitli","Simitli"),
													array ("Slavyanovo","Slavyanovo"),
													array ("Sliven","Sliven"),
													array ("Slivnitsa","Slivnitsa"),
													array ("Slivo Pole","Slivo Pole"),
													array ("Smolyan","Smolyan"),
													array ("Smyadovo","Smyadovo"),
													array ("Sofia","Sofia"),
													array ("Sopot","Sopot"),
													array ("Sozopol","Sozopol"),
													array ("Sredets","Sredets"),
													array ("Stamboliyski","Stamboliyski"),
													array ("Stara Zagora","Stara Zagora"),
													array ("Straldzha","Straldzha"),
													array ("Strazhitsa","Strazhitsa"),
													array ("Strelcha","Strelcha"),
													array ("Suhindol","Suhindol"),
													array ("Sungurlare","Sungurlare"),
													array ("Suvorovo","Suvorovo"),
													array ("Sveti Vlas","Sveti Vlas"),
													array ("Svilengrad","Svilengrad"),
													array ("Svishtov","Svishtov"),
													array ("Svoge","Svoge"),
													array ("Targovishte","Targovishte"),
													array ("Tervel","Tervel"),
													array ("Teteven","Teteven"),
													array ("Topolovgrad","Topolovgrad"),
													array ("Tran","Tran"),
													array ("Trastenik","Trastenik"),
													array ("Troyan","Troyan"),
													array ("Tryavna","Tryavna"),
													array ("Tsar Kaloyan","Tsar Kaloyan"),
													array ("Tsarevo","Tsarevo"),
													array ("Tutrakan","Tutrakan"),
													array ("Tvarditsa","Tvarditsa"),
													array ("Ugarchin","Ugarchin"),
													array ("Valchedram","Valchedram"),
													array ("Valchi Dol","Valchi Dol"),
													array ("Varbitsa","Varbitsa"),
													array ("Varna","Varna"),
													array ("Varshets","Varshets"),
													array ("Veliki Preslav","Veliki Preslav"),
													array ("Veliko Tarnovo","Veliko Tarnovo"),
													array ("Velingrad","Velingrad"),
													array ("Vetovo","Vetovo"),
													array ("Vetren","Vetren"),
													array ("Vidin","Vidin"),
													array ("Vratsa","Vratsa"),
													array ("Yablanitsa","Yablanitsa"),
													array ("Yakoruda","Yakoruda"),
													array ("Yambol","Yambol"),
													array ("Zavet","Zavet"),
													array ("Zemen","Zemen"),
													array ("Zlataritsa","Zlataritsa"),
													array ("Zlatitsa","Zlatitsa"),
													array ("Zlatograd","Zlatograd")
												 );

										}
										$dpCity = '';
										$textCity = '';
										$dpCityName = '';
										$textCityName = '';
										if ( strtolower( $shipping_info['country'] ) == 'bulgaria') {
											$dpCity = '<script>
															jQuery(window).load(function(){
																setTimeout(
																  function() 
																  {
																    jQuery("#bulgaria-cities-shipping .sbHolder").attr("style","display:block!important");
																    jQuery("#bulgaria-cities-shipping .sbHolder .sbOptions [rel='. $shipping_info['city'].']").click();
																  }, 1000);
																
															});
														</script>';
											$textCity = 'display:none;';
											$dpCityName = 'mp_shipping_info[city]';
											$textCityName = '';
										} else {
											$dpCity = '<script>
															jQuery(window).load(function(){
																setTimeout(
																  function() 
																  {
																	jQuery("#bulgaria-cities-shipping .sbHolder").attr("style","display:none!important");
																   }, 1000);
															});
														</script>';
											$textCity = 'display:block;';
											$dpCityName = '';
											$textCityName = 'mp_shipping_info[city]';
										}
									$cities_data ='';
										foreach ($cities as $row) {
											$cities_data .= "<option value='" . strtolower($row[0]) . "'" .">".$row[1]."</option>";

										}
									?>
									<div class="form-group" id="bulgaria-cities-shipping">
										<label for="mp_shipping_info_city"><span class="required">*</span><?php _e( 'City:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_shipping_city_error'])) {		
												$pu_b8 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_shipping_city_error'].'</div>';
											}
										?>
										<select name="<?= $dpCityName ?>">
											<?php echo $cities_data ?>
										</select>
										<input style="<?= $textCity ?>" type="text" name="<?= $textCityName ?>" class="form-control <?php echo isset($pu_b8)? $pu_b8: '';?>" id="mp_shipping_info_city" value="<?php echo isset($shipping_info['city'])? $shipping_info['city']: ''; ?>" type="text">
										<?= $dpCity ?>
										<script type="text/javascript">
											$('body').on('click','#bulgaria-cities-shipping .sbOptions',function(e){
											  var getCity = $( "#bulgaria-country-shipping select option:selected" ).val();
											  if (getCity == 'bulgaria' ){
											    $('#bulgaria-cities-shipping .sbHolder ').attr('style','display:block!important');
											    $('#bulgaria-cities-shipping input ').css('display','none');

											    $('#bulgaria-cities-shipping select ').attr('name','mp_shipping_info[city]');
											    $('#bulgaria-cities-shipping input ').attr('name','');

											  } else {
											  	$('#bulgaria-cities-shipping .sbHolder ').attr('style','display:none!important');
											    $('#bulgaria-cities-shipping input ').css('display','block');

											    $('#bulgaria-cities-shipping select ').attr('name','');
											    $('#bulgaria-cities-shipping input ').attr('name','mp_shipping_info[city]');
											  }
											});
										</script>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="mp_shipping_info_email"><span class="required">*</span><?php _e( 'Email:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_shipping_email_error']) || isset($errorcode['up_shipping_email_invalid_error'])) {		
												$pu_b10 = ' input-border-error';
												
												if(isset($errorcode['up_shipping_email_error']))
													$pu_m10 = $errorcode['up_shipping_email_error'];
												
												if(isset($errorcode['up_shipping_email_invalid_error']))
													$pu_m10 = $errorcode['up_shipping_email_invalid_error'];
												
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$pu_m10.'</div>';
											}
										?>
										<input class="form-control <?php echo isset($pu_b10)? $pu_b10: '';?>" id="mp_shipping_info_email" name="mp_shipping_info[email]" value="<?php echo isset($shipping_info['email'])? $shipping_info['email'] : ''; ?>" type="text">
									</div>
									<div class="form-group">
										<label for="mp_shipping_info_address2"><span class="required"></span><?php _e( 'Address 2:'); ?>&nbsp;</label>
										<input class="form-control" id="mp_shipping_info_address2" name="mp_shipping_info[address2]" value="<?php echo isset($shipping_info['address2'])? $shipping_info['address2'] : ''; ?>" type="text">
									</div>
									<div class="form-group">
										<label for="mp_shipping_info_state"><span class="required">*</span><?php _e( 'State/Province/Region:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_shipping_state_error'])) {		
												$pu_b11 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_shipping_state_error'].'</div>';
											}
										?>
										<input class="form-control <?php echo isset($pu_b11)? $pu_b11: '';?>" id="mp_shipping_info_state" name="mp_shipping_info[state]" value="<?php echo isset($shipping_info['state'])? $shipping_info['state'] : ''; ?>" type="text">
									</div>
									<div class="form-group">
										<label for="mp_shipping_info_zip"><span class="required">*</span><?php _e( 'Postal/Zip Code:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_shipping_zip_error'])) {		
												$pu_b12 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_shipping_zip_error'].'</div>';
											}
										?>
										<input class="form-control <?php echo isset($pu_b12)? $pu_b12: '';?>" id="mp_shipping_info_zip" name="mp_shipping_info[zip]" value="<?php echo isset($shipping_info['zip'])? $shipping_info['zip'] : ''; ?>" type="text">
									</div>

								</div>
								<div class="col-md-6">
								
									<div class="form-group">
										<label for="mp_shipping_info_phone"><?php _e( 'Phone Number:'); ?>&nbsp;</label>
										<?php echo apply_filters( 'mp_shipping_info_error_phone', ''); ?>
										<input class="form-control" id="mp_shipping_info_phone" name="mp_shipping_info[phone]" value="<?php echo isset($shipping_info['phone'])? $shipping_info['phone'] : ''; ?>" type="text">
									</div>
									
								</div>
							</div> <!--./row-->
						</div>
					<!-- ./Shipping Info -->
					
					<!-- Billing Info -->
						<h3 class="user-profile-title"><?php _e("Billing Info", "mp"); ?></h3>						
						<div class="by-section"> <!--1-->
							<div class="row">					
								<div class="col-md-6">
									<div class="form-group">
										<label for="mp_shipping_info_name"><span class="required">*</span><?php _e( 'Full Name:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_billing_fullname_error'])) {		
												$pu_b13 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_billing_fullname_error'].'</div>';
											}
										?>
										<input class="form-control <?php echo isset($pu_b13)? $pu_b13: '';?>" id="mp_shipping_info_name" name="mp_billing_info[name]" value="<?php echo isset($billing_info['name'])? $billing_info['name'] : ''; ?>" type="text">
									</div>
									<div class="form-group">
										<label for="mp_shipping_info_address1"><span class="required">*</span><?php _e( 'Address:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_billing_address_error'])) {		
												$pu_b14 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_billing_address_error'].'</div>';
											}
										?>
										<input class="form-control <?php echo isset($pu_b14)? $pu_b14: '';?>" id="mp_shipping_info_address1" name="mp_billing_info[address1]" value="<?php echo isset($billing_info['address1'])? $billing_info['address1']: ''; ?>" type="text">
									</div>
									<div class="form-group" id="bulgaria-country-billing">
										<label for="mp_shipping_info_country"><span class="required">*</span><?php _e( 'Country:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_billing_country_error'])) {
												$pu_b16 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_billing_country_error'].'</div>';
											}
										?>
										<?php
											$countries = array("Bulgaria", "United States", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
											?>
											<select id="country" name="mp_billing_info[country]" class="form-control <?php echo isset($pu_b16)? $pu_b16: '';?>">
												<?php
												$billing_info['country'] = isset($billing_info['country'])? $billing_info['country'] : '';
												foreach ($countries as $country) { ?>
													<option value="<?php echo $country; ?>" <?php echo(($billing_info['country'] == $country) ? 'selected' : ''); ?> ><?php echo $country; ?></option>
												<?php } ?>
											</select>
									</div>
									<?php
										$dpCity = '';
										$textCity = '';
										$dpCityName = '';
										$textCityName = '';
										$cities_data ='';
										if ( strtolower($billing_info['country']) == 'bulgaria') {
											$dpCity = '<script>
															jQuery(window).load(function(){
																setTimeout(
																  function() 
																  {
																    jQuery("#bulgaria-cities-billing .sbHolder").attr("style","display:block!important");
																    jQuery("#bulgaria-cities-billing .sbHolder .sbOptions [rel='. $billing_info['city'].']").click();
																  }, 1000);
																
															});
														</script>';
											$textCity = 'display:none;';
											$dpCityName = 'mp_billing_info[city]';
											$textCityName = '';
										} else {
											$dpCity = '<script>
															jQuery(window).load(function(){
																setTimeout(
																  function() 
																  {
																	jQuery("#bulgaria-cities-billing .sbHolder").attr("style","display:none!important");
																   }, 1000);
															});
														</script>';
											$textCity = 'display:block;';
											$dpCityName = '';
											$textCityName = 'mp_billing_info[city]';
										}
									$cities_data = '';
											foreach ($cities as $row) {
												$cities_data .= "<option value='" . strtolower($row[0]) . "'" .">".$row[1]."</option>";

											}
									?>
									<div class="form-group" id="bulgaria-cities-billing">
										<label for="mp_shipping_info_city"><span class="required">*</span><?php _e( 'City:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_billing_city_error'])) {
												$pu_b15 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_billing_city_error'].'</div>';
											}
										?>

										<select name="<?= $dpCityName ?>">
											<?php echo $cities_data ?>
										</select>

										<input style="<?= $textCity ?>" type="text" name="<?= $textCityName ?>"  class="form-control <?php echo isset($pu_b15)? $pu_b15: '';?>" id="mp_shipping_info_city" value="<?php echo isset($billing_info['city'])? $billing_info['city']: ''; ?>" type="text">
										<?= $dpCity ?>
										
										<script type="text/javascript">
												$('body').on('click','#bulgaria-cities-billing .sbOptions',function(e){
												  var getCity = $( "#bulgaria-country-billing select option:selected" ).val();
												  if (getCity == 'bulgaria' ){
												    $('#bulgaria-cities-billing .sbHolder ').attr('style','display:block!important');
												    $('#bulgaria-cities-billing input ').css('display','none');

												    $('#bulgaria-cities-billing select ').attr('name','mp_billing_info[city]');
												    $('#bulgaria-cities-billing input ').attr('name','');

												  } else {
												  	$('#bulgaria-cities-billing .sbHolder ').attr('style','display:none!important');
												    $('#bulgaria-cities-billing input ').css('display','block');

												    $('#bulgaria-cities-billing select ').attr('name','');
												    $('#bulgaria-cities-billing input ').attr('name','mp_billing_info[city]');
												  }
												});
										</script>
									</div>									
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="mp_shipping_info_email"><span class="required">*</span><?php _e( 'Email:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_billing_email_error']) || isset($errorcode['up_billing_email_invalid_error'])) {		
												$pu_b17 = ' input-border-error';
												
												if(isset($errorcode['up_billing_email_error']))
													$pu_m17 = $errorcode['up_billing_email_error'];
												
												if(isset($errorcode['up_billing_email_invalid_error']))
													$pu_m17 = $errorcode['up_billing_email_invalid_error'];
												
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$pu_m17.'</div>';
											}
										?>
										<input class="form-control <?php echo isset($pu_b17)? $pu_b17: '';?>" id="mp_shipping_info_email" name="mp_billing_info[email]" value="<?php echo isset($billing_info['email'])? $billing_info['email'] : ''; ?>" type="text">
									</div>
									<div class="form-group">
										<label for="mp_shipping_info_address2"><span class="required"></span><?php _e( 'Address 2:'); ?>&nbsp;</label>
										<?php echo apply_filters( 'mp_billing_info_error_address2', ''); ?>
										<input class="form-control" id="mp_shipping_info_address2" name="mp_billing_info[address2]" value="<?php echo isset($billing_info['address2'])? $billing_info['address2'] : ''; ?>" type="text">
									</div>
									<div class="form-group">
										<label for="mp_shipping_info_state"><span class="required">*</span><?php _e( 'State/Province/Region:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_billing_state_error'])) {
												$pu_b18 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_billing_state_error'].'</div>';
											}
										?>
										<input class="form-control <?php echo isset($pu_b18)? $pu_b18: '';?>" id="mp_shipping_info_state" name="mp_billing_info[state]" value="<?php echo isset($billing_info['state'])? $billing_info['state'] : ''; ?>" type="text">
									</div>
									<div class="form-group">
										<label for="mp_shipping_info_zip"><span class="required">*</span><?php _e( 'Postal/Zip Code:'); ?>&nbsp;</label>
										<?php 
											if(isset($errorcode['up_billing_zip_error'])) {
												$pu_b19 = ' input-border-error';
												echo '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errorcode['up_billing_zip_error'].'</div>';
											}
										?>
										<input class="form-control <?php echo isset($pu_b19)? $pu_b19: '';?>" id="mp_shipping_info_zip" name="mp_billing_info[zip]" value="<?php echo isset($billing_info['zip'])? $billing_info['zip'] : ''; ?>" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="mp_shipping_info_phone"><?php _e( 'Phone Number:'); ?>&nbsp;</label>
										<?php echo apply_filters( 'mp_billing_info_error_phone', ''); ?>
										<input class="form-control" id="mp_shipping_info_phone" name="mp_billing_info[phone]" value="<?php echo isset($billing_info['phone'])? $billing_info['phone'] : ''; ?>" type="text">
									</div>
								</div>
							</div> <!--./row-->
						</div>
					<!-- ./Billing Info -->
					
					<?php //do_action( 'show_user_profile', $profileuser ); ?>
							
							<div class="pull-left update-wrapper">
								<input class="btn btn-inverse btn-large" type="submit" name="submit" id="submit" value="<?php _e( 'Update Info' ); ?>">							
							</div>
						</form>							
				</div>
			</div>
		</div> <!-- / col-md-s -->
		</div> <!-- /row no-margin -->
		</div>
	</div>
</div>
<?php uf_notification_unset(); ?>
<?php get_template_part('footer', 'widget'); ?>
<?php get_footer(); ?>