<?php get_header(); ?>
	<!-- Page -->
	<div id="page-wrapper">

		<div class="content-section">
			<div class="outercontainer">
				<div class="container" style="min-height: 450px;">
					<div class="row no-margin">
						<div class="col-md-12 col-sm-12 col-xs-12">
								<!-- <h4><span><?php _e('Our Products' , 'pro' ); ?></span></h4> -->
							<div id="breadcrumb">
								<div class="row">
								 <div class="col-md-12">
									<ul class="breadcrumbs-list">
										<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
										<li><a href="<?php echo add_lang_vars_custom($main_site_url.'/store/products/'); ?>"><?php _e('Stores', 'pro'); ?></a></li>
										<li class="active"><?php _e('Products', 'pro'); ?></li>
									</ul>
								 </div>
								</div>
							</div>

							<div class="clear padding10"></div>

								<div class="row-fluid">
									<div class="span12">
									<?php if(is_main_site()) { 
									
										$terms = get_terms('product_category',array('hide_empty'=>false,'parent' => 0, 'exclude'=>array(27,49)));
										$in_terms_slug = array();									
											foreach($terms as $term){
												$in_terms_slug[] =  urldecode($term->slug);
											}
										$in_terms_slug_new = implode(',',$in_terms_slug);
										echo do_shortcode('[toprank_mpdgfilters category="'.$in_terms_slug_new.'|true"]'); 
									 } else {
										echo do_shortcode('[toprank_mpdgfilters]'); 
									 } 
									?>
									</div><!-- / span8 --> 
								</div><!-- / row-fluid -->
					</div><!-- / col-md-s -->
				</div><!-- / row no-margin -->
				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	
	</div><!-- / page-wrapper -->
<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>