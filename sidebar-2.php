  	<?php do_action('sidebar-begin'); ?>
		<div class="well sidebar-widget-well"><h5 class="sidebar-widget-title"><?php _e('Search','pro'); ?></h5>
			<form role="search" class="form-search" method="get" id="searchform" action="<?php echo localize_url(get_site_url()); ?>">
			<div class="input-group">
				<input type="text" value="" class="form-control" name="s" id="s" placeholder="<?php _e('Search','pro'); ?>">
				<span class="input-group-btn">
					<button class="btn btn-default" type="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
				</span>
			</div>
			</form>
		</div>		
		<div class="well sidebar-widget-well"><h5 class="sidebar-widget-title"><?php _e('Recent Comments','pro'); ?></h5>
			<?php	
			$args = array(
				'status' => 'approve',
				'number' => '5'
			);
			$comment = get_comments($args);
			?>
			<div id="r-comments" class="row r-comm">
				<div class="col-md-12">
				<ul>
					<?php
						if ( $comment ) {
							foreach ( (array) $comment as $comments) {
								$d = "j F Y";
								echo  '<li>' . sprintf(__('%1$s'), '<a href="'.  get_comment_link($comments->comment_ID)  . '">' . get_the_title($comments->comment_post_ID) . '</a> ');
								?>
								<div class="row sub-txt">
								<p class="col-md-6 pull-left" id="c-author"><?php _e('Comment from','pro'); ?> <a href="#"><?php echo comment_author_link( $comments->comment_ID ); ?></a></p>
								<p class="col-md-6 text-right pull-right" id="c-cdate"><?php comment_date( $d, $comments->comment_ID )?></p><br>
								</div>
								</li>
							<?php
							}
						} ?>
					</ul>
				</div>
			</div>
		</div>
  	<?php do_action('sidebar-end'); ?>