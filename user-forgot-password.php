<?php
// **********************************************************
// If you want to have an own template for this action
// just copy this file into your current theme folder
// and change the markup as you want to
// **********************************************************
if ( is_user_logged_in() ) {
	wp_safe_redirect( home_url( '/user-profile/' ) );
	exit;
}
?>
<?php get_header(); ?>
	<!-- Page -->
	<div id="page-wrapper">
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						
							<div class="page-heading">
								<h1><span><?php _e( 'Lost your password?', UF_TEXTDOMAIN ); ?></h1>
							</div>
							
							<div class="clear padding10"></div>
								<div class="page-content">
									<div id="lost-pass" class="row">									
									<div class="col-md-9">									
										<form action="<?php echo uf_get_action_url( 'forgot_password' ); ?>" method="post" class="sa-form">
											<?php wp_nonce_field( 'forgot_password', 'wp_uf_nonce_forgot_password' ); ?>
											<?php if("confirmationsend" == $_GET['message']) {?>
											<div class="alert alert-success" style="width: 97.4%;"><button type="button" class="close" data-dismiss="alert">×</button> <i class="fa fa-check-circle"></i> <?php _e( 'Check your E-mail for the confirmation link.', UF_TEXTDOMAIN ); ?></div>
											<?php } ?>
												<h4><?php _e( 'Please enter your username or email address. You will receive a link to create a new password via email.' ) ?></h4>
											<div class="form-group">
												<label class="control-label" for="user_login"><?php _e( 'Username or E-mail:' ); ?></label>
												<div class="controls">
													<input class="form-control" style="width: 97.4%;" type="text" name="user_login" id="user_login">
												</div>	
											</div>	
											<?php echo  apply_filters( 'uf_forgot_password_messages', isset( $_GET[ 'message' ] ) ? $_GET[ 'message' ] : '' ); ?>
											<div class="align-left">
												<input class="forgotpass-button" type="submit" name="submit" id="submit" value="<?php _e( 'Get New Password' ); ?>">
											</div>
										</form>
									</div>
									</div>
								</div>	
					</div>	
				</div>	
			</div>	
		</div>	
	</div>
<?php get_template_part('footer', 'widget'); ?>		
<?php get_footer(); ?>
