<?php 
/*
Template Name: Blog Page
 */

get_header();

	//$sidebar_position = get_option('mpt_wp_page_sidebar_position');
	$sidebar_position = get_blog_option(1,'mpt_wp_page_sidebar_position');
	$selected_sidebar = mpt_load_selected_sidebar('page');
?>

	<!-- Page -->
	<div id="page-wrapper">

<?php if (have_posts()) : while (have_posts()) : the_post();

	$contentbgcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_bg_color', true ));
	$contenttextcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_text_color', true ));

	$meta_prefix = '_mpt_';
	$col = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_layout', '1col' , true );
	$entries = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_entries', 5 , true );
	$poststyle = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_box_style' , 'style-1' , true ); 
	$btncolor = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_btn_color' , '' , true );
	$showexcerpt = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_show_excerpt' , 'yes' , true );
	$showmeta = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_show_meta' , 'yes' , true );
	$showauthorname = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_show_author_name' , 'yes' , true );
	$showcomments = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_show_comments' , 'yes' , true );
	$showdatetag = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_show_date_tag' , 'yes' , true );
	$datetagcolor = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_date_tag_color' , '' , true );
	$metatagscolor = mpt_get_post_meta( $post->ID, $meta_prefix . 'blog_page_meta_tag_color' , '' , true );
?>

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">
					<div class="row no-margin">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div id="breadcrumb">
								<div class="row">
									 <div class="col-md-12">
										<ul class="breadcrumbs-list">
											<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
											<li class="active"><?php the_title(); ?></li>
										</ul>
									 </div>
									</div>
							</div>
							<div class="clear padding10"></div>
							<?php if ( $col == '1col' ) { ?>

								<div class="row">
									<?php if ( empty($sidebar_position) || $sidebar_position == 'Left' ) : ?>
										<div id="sidebar" class="col-md-3">
											<?php get_sidebar( '2' ); ?>
										</div>
									<?php endif; ?>
							<?php } ?>

								<div class="<?php echo ($col == '1col' ? 'col-md-9 col-sm-12 col-xs-12' : '') ?>"<?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>

									<!-- <div class="page-heading">
										<h4><span<?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>><?php the_title(); ?></span></h4>
									</div> -->

									<?php $thecontent = get_the_content(); ?>
									<?php if (!empty($thecontent)) { ?>
											<div class="page-content">
												<div class="row">
													<?php the_content(); ?>
												</div>
												<?php edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>
											</div>
									<?php	}
									endwhile; endif;

										switch ($btncolor) {
											case 'grey': $btnclass = ' btn-default'; break;
											case 'blue': $btnclass = ' btn-primary'; break;
											case 'lightblue': $btnclass = ' btn-info'; break;
											case 'green': $btnclass = ' btn-success'; break;
											case 'yellow': $btnclass = ' btn-warning'; break;
											case 'red': $btnclass = ' btn-danger'; break;
											case 'black': $btnclass = ' btn-inverse'; break;
											default: $btnclass = ''; break;			
										}

										$query_args = array(
											'echo' => true,
											'paginate' => true,
											'per_page' => $entries, 
											'order_by' => 'date', 
											'order' => 'DESC',
											'columns' => $col,
											'poststyle' => ( $poststyle == 'style-2' ? 'style-2' : 'style-1' ),
											'btnclass' => ( !empty($btnclass) ? $btnclass : '' ),
											'boxclass' => ( $poststyle == 'style-2' ? 'post-grid' : '' ),
											'extra' => array(
												'showexcerpt' => ( $showexcerpt == 'yes' ? true : false ),
												'showmeta' => ( $showmeta == 'yes' ? true : false ),
												'showauthordetails' => ( $showauthorname == 'yes' ? true : false ),
												'showcomments' => ( $showcomments == 'yes' ? true : false ),
												'showhovertag' => ( $showdatetag == 'yes' ? true : false ),
												'hovertagcolor' => ( !empty($datetagcolor) ? 'hover-tag-' . $datetagcolor : '' ),
												'metatagscolor' => ( !empty($metatagscolor) ? 'meta-tag-' . $metatagscolor : '' )
												)
										);
										pro_display_wp_post_query( apply_filters( 'pro_post_query_blog_page' , $query_args ) );
									?>

								</div><!-- / span -->

							<?php if ( $col == '1col' ) { ?>
									<?php if ( !empty($sidebar_position) && $sidebar_position == 'Right' ) : ?>
										<div id="sidebar" class="col-md-4">
											<?php get_sidebar( $selected_sidebar ); ?>
										</div>
									<?php endif; ?>
								</div><!-- / row -->
							<?php } ?>
						</div><!-- / col-md-12 col-sm-12 col-xs-12 -->
					</div><!-- / row no-margin -->
				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget');

get_footer(); ?>