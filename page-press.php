<?php
/*
Template Name: Press
*/
/** Sets up the WordPress Environment. */

get_header(); ?>
	<!-- Page -->
	<div id="page-wrapper" class="wishlist-page">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">
				<div class="row no-margin">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div id="post-<?php echo $id; ?>" <?php post_class(); ?><?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>
								<div id="breadcrumb">
									<div class="row">
										 <div class="col-md-12">
											<ul class="breadcrumbs-list">
												<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
                                                <li class="active"><span><?php _e(get_the_title(), 'pro'); ?></span></li>
											</ul>
										 </div>
									</div>
								</div>
								<div class="clear padding10"></div>
							</div>
							<div class="sub-heading">
								<h5><span> <?php _e('Download the several size and format logos','pro'); ?></span></h5>
							</div>
									<div class="page-content press-download-logo">
										<div class="row">
											<?php
											$query = get_posts(array(
												'posts_per_page'  => -5,
												'post_type'     => 'press_logo_new',
												'orderby'          => 'date',
												'order'            => 'DESC'
											));

												if( $query ): ?>
																  
												  <?php foreach( $query as $post ):setup_postdata( $post )
												  ?>
													
														<div class="col-md-2">
															<div class="inner">
																<?php 

																$image = get_field('press_page_download_logo', $post->ID);
																$file = get_field('press_page_download_logo_-_file', $post->ID);

																if( !empty($image) ): ?>

																	 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /> 
																<?php endif; ?>
															</div>
															<?php 
																$download = $image['url'];
															 	if ( $file['url'] ) {
															 		$download = $file['url'];
															 	} 

															?>
															<a href="<?php echo $download; ?>" download>
																<p><?php _e(get_the_title(),'pro'); ?>
																	<span class="icon"></span>
																</p>
															</a>
														</div>
												  <?php endforeach; ?>
												  
												
												  
												  <?php wp_reset_postdata(); ?>

											  <?php endif; ?>
											</div>

									</div>
									<?php
									$query = get_posts(array(
										'posts_per_page'  => -1,
										'post_type'     => 'press_content',
										'orderby'          => 'date',
										'order'            => 'DESC'
									));

										if( $query ): ?>
														  
										  <?php foreach( $query as $post ):setup_postdata( $post )?>
											<?php
											$contentTitle =  get_the_title();
											$contentOutput = get_field('press_content-400_words', $post->ID);
											?>

										  <?php endforeach; ?>
										  
										
										  
										  <?php wp_reset_postdata(); ?>

									  <?php endif; ?>

									<div class="sub-heading-about">
										<h5><?php _e($contentTitle,'pro'); ?><span><?php _e('(400 words)','pro'); ?></span></h5>
									</div>
									<div class="page-content press-about">
										<p>
											<!--<span><?php// _e('Art Bulgaria','pro'); ?></span> -->
											<?php _e( $contentOutput,'pro'); ?>

										</p>
									</div>
									<?php
									$query = get_posts(array(
										'posts_per_page'  => -1,
										'post_type'     => 'press_content',
										'orderby'          => 'date',
										'order'            => 'DESC'
									));

										if( $query ): ?>
														  
										  <?php foreach( $query as $post ):setup_postdata( $post )?>
											<?php
											$contentTitle =  get_the_title();
											$contentOutput = get_field('press_content-100_words', $post->ID);
											?>

										  <?php endforeach; ?>
										  
										
										  
										  <?php wp_reset_postdata(); ?>

									  <?php endif; ?>

									<div class="sub-heading-about">
										<h5><?php _e($contentTitle,'pro'); ?><span><?php _e('(100 words)','pro'); ?></span></h5>
									</div>
									<div class="page-content press-about">
										<p>
											<!--<span><?php// _e('Art Bulgaria','pro'); ?></span> -->
											<?php _e( $contentOutput,'pro'); ?>

										</p>
									</div>
									<?php
									$query = get_posts(array(
										'posts_per_page'  => -1,
										'post_type'     => 'press_content',
										'orderby'          => 'date',
										'order'            => 'DESC'
									));

										if( $query ): ?>
														  
										  <?php foreach( $query as $post ):setup_postdata( $post )?>
											<?php
											$contentTitle =  get_the_title();
											$contentOutput = get_field('press_content-50_words', $post->ID);
											?>

										  <?php endforeach; ?>
										  
										
										  
										  <?php wp_reset_postdata(); ?>

									  <?php endif; ?>

									<div class="sub-heading-about">
										<h5><?php _e($contentTitle,'pro'); ?><span><?php _e('(50 words)','pro'); ?></span></h5>
									</div>
									<div class="page-content press-about">
										<p>
											<!--<span><?php// _e('Art Bulgaria','pro'); ?></span> -->
											<?php _e($contentOutput,'pro'); ?>

										</p>
									</div>
									<div class="sub-heading">
										<h5><span><?php _e('Download screenshots to Art Bulgaria','pro'); ?></span></h5>
									</div>
									<div class="page-content press-download-ss">
										
										<?php
										$query = get_posts(array(
											'posts_per_page'  => -6,
											'post_type'     => 'press_screen',
											'orderby'          => 'date',
											'order'            => 'DESC'
										));

										$count = 1;
										$html = '';

											if( $query ): ?>
															  
											  <?php foreach( $query as $post ):setup_postdata( $post )?>
												<?php

													$image = get_field('screenshot', $post->ID);

													if( !empty($image) ): 

														 $imgUrl = $image['url']; 
														 $imgAlt = $image['alt']; 
													?>
													<?php endif; ?>
												<?php
													//echo $count;
													if($count == 1 ) {
														$count = $count+1;
														$html .= '<div class="row">
																	<div class="col-md-4">
																		<div class="inner">
																			<img src="'. $imgUrl .'" alt="'. $imgAlt .'">
																		</div>
																		<a href= '. $imgUrl .' download>
																			<p> '. __('Download this screenshot','pro') .'<span class="icon"></span></p>
																		</a>
																	</div>
																	
																  
																';
													} else {
															if($count == 3 ) {
																$count = 1;
																$html .= '
																			<div class="col-md-4">
																				<div class="inner">
																					<img src="'. $imgUrl .'" alt="'. $imgAlt .'">
																				</div>
																				<a href= '. $imgUrl .' download>
																					<p> '. __('Download this screenshot','pro') .'<span class="icon"></span></p>
																				</a>
																			</div>
																		</div>

																		';

															} else {

																$html .= '<div class="col-md-4">
																			<div class="inner">
																				<img src="'. $imgUrl .'" alt="'. $imgAlt .'">
																			</div>
																			<a href= '. $imgUrl .' download>
																				<p> '. __('Download this screenshot','pro') .'<span class="icon"></span></p>
																			</a>
																		</div>
																		';
																$count = $count+1;
															}

													}
													
												?>

												
											  <?php endforeach; ?>
											  <?php echo $html; ?>
											  
											
											  
											  <?php wp_reset_postdata(); ?>

										  <?php endif; ?>
									</div>
									<div class="sub-heading">
										<h5><span><?php _e('Art Bulgaria - related links','pro'); ?></span></h5>
									</div>
									<div class="page-content press-related">
										<div class="row">
											<?php press_social_icons();?>
											<div class="col-md-2 press-related-link linkedin">
                                                <a href="https://www.linkedin.com/company/art-bulgaria" target="_blank" title="Linkedin">
                                                    <div class="rl-holder">
                                                        <div class="icon"><i class="fa fa-linkedin"></i></div>
                                                        <p class="press-related-link-desc">
                                                            <?php _e('Art Bulgaria on Linkedin','pro'); ?>
                                                        </p>
                                                    </div>
                                                </a>
											</div>
										</div>
									</div>
						</div> <!-- col-ms-'s -->
					</div> <!-- row no-margin -->
				</div>

					<?php endwhile; endif; ?>

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>
<?php get_footer(); ?>