<?php 
/*
Template Name: Page (full width)
 */

get_header(); ?>

	<!-- Page -->
	<div id="page-wrapper">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php 
						$contentbgcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_bg_color', true ));
						$contenttextcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_text_color', true ));
					?>

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">
					<div class="row no-margin">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div id="post-<?php the_ID(); ?>" <?php post_class(); ?><?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>
									<div id="breadcrumb">
										<div class="row">
											 <div class="col-md-12">
												<ul class="breadcrumbs-list">
													<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
													<?php 
														$breadcrumb_title  = '';
														 if ( get_query_var('pagename') == 'performers'  && ICL_LANGUAGE_CODE =='bg' ) {
														    $breadcrumb_title =  __( "Изпълнители" , 'pro' );
														 } else {
														     $breadcrumb_title =  __( get_the_title() , 'pro' );
														 }
													?>
													<li class="active"><?php echo $breadcrumb_title;?></li>
												</ul>
											 </div>
										</div>
									</div>
									
										<div class="clear padding10"></div>

										<div class="page-content">
											<div class="row">
												<div class="col-md-12">	
												<?php the_content(); ?>
												</div>
											</div>

											<?php // edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>

										</div>

										<?php wp_link_pages(array('before' => '<p class="margin-vertical-15">' . __( 'Pages:' , 'pro' ) , 'after' => '</p>' , 'next_or_number' => 'number')); ?>

										<?php 

											$showcomments = get_post_meta( $post->ID, '_mpt_page_show_comments', true );

											if ( $showcomments == 'on') {

												echo '<div class="page-comments">';
												comments_template();
												echo '</div>';
											} 
										?>

								</div>

								<?php endwhile; endif; ?>

					</div><!-- / col-md-'s -->
				</div><!-- / row no-margin -->
				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>