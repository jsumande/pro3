<?php 
/*
Template Name: Page Buddypress (full width)
 */

get_header(); ?>

<!-- Page -->
<!-- buddypress.php -->
<div id="page-wrapper">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php 
					$contentbgcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_bg_color', true ));
					$contenttextcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_text_color', true ));
				?>

	<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
		<div class="outercontainer">
			<div class="container">
				<div class="row no-margin">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?><?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>

<!--<div class="page-heading"><h4><span<?php//echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>><?php the_title(); ?></span></h4></div>-->
							
							<div id="breadcrumb">
								<div class="row">
									 <div class="col-md-12">
										<ul class="breadcrumbs-list">
										<li><a href="<?php echo get_home_url(); ?>"><?php echo _e('Home', 'mp'); ?></a></li>			
											<?php
											$t = explode('/', $_SERVER['REQUEST_URI']);
											$ctr = 0;
											$val = array();
											foreach ( $t as $a ) {
												$val[$ctr] = $a;
												$ctr++;
											}			
											switch($val[3]) {
												case 'profile';
														switch ($val[4]) {
															case 'change-avatar':
																echo '<li style="text-transform: capitalize;">'.__('Profile').'</li><li class="active" style="text-transform: capitalize;">'.__('Change Profile Photo').'</li>';
																break;
															case 'edit':
																echo '<li> '.__('Profile', 'mp').'</li><li class="active"> '.__('Edit', 'mp').'</li>';									
																break;
															default:
																echo '<li class="active"> '.__('Profile').'</li>';
																break;
														}
													break;
												case 'notifications';
															switch ($val[4]) {
																case 'read':
																	echo '<li class="" style="text-transform: capitalize;">'.__('Notifications').'</li><li class="active" style="text-transform: capitalize;">'.__('Read').'</li>';
																	break;
																default:
																		echo '<li class="active" style="text-transform: capitalize;">'.__('Notifications').'</li>';
																	break;
															}
													break;
												case 'messages';
															switch ($val[4]) {
																case 'compose':
																	echo '<li class="" style="text-transform: capitalize;">'.__('Messages').'</li><li class="active" style="text-transform: capitalize;">'.__('Compose').'</li>';
																	break;
																case 'sentbox':
																	echo '<li class="" style="text-transform: capitalize;">'.__('Messages').'</li><li class="active" style="text-transform: capitalize;">'.__('Sent').'</li>';
																	break;
																default:
																	echo '<li class="" style="text-transform: capitalize;">'.__('Messages').'</li><li class="active" style="text-transform: capitalize;">'.__('Inbox').'</li>';
																	break;
															}
													break;
												case 'forums';
															switch ($val[4]) {
																case 'favorites':
																	echo '<li class="" style="text-transform: capitalize;">'.__('Forums').'</li><li class="active" style="text-transform: capitalize;">'.__('Favorites').'</li>';
																	break;
																case 'replies':
																	echo '<li class="" style="text-transform: capitalize;">'.__('Forums').'</li><li class="active" style="text-transform: capitalize;">'.__('Replies Created').'</li>';
																	break;
																case 'subscriptions':
																	echo '<li class="" style="text-transform: capitalize;">'.__('Forums').'</li><li class="active" style="text-transform: capitalize;">'.__('subscriptions').'</li>';
																	break;
																default:
																	echo '<li class="" style="text-transform: capitalize;">'.__('Forums').'</li><li class="active" style="text-transform: capitalize;">'.__('Topic Started').'</li>';
																	break;
															}
													break;
												case 'settings';
															switch ($val[4]) {
																case 'notifications':
																	echo '<li class="" style="text-transform: capitalize;">'.__('Settings').'</li><li class="active" style="text-transform: capitalize;">'.__('Email').'</li>';
																	break;
																case 'profile':
																	echo '<li class="" style="text-transform: capitalize;">'.__('Settings').'</li><li class="active" style="text-transform: capitalize;">'.__('Profile Visibility').'</li>';
																	break;
																default:
																	echo '<li class="active" style="text-transform: capitalize;">'.__('Settings').'</li>';
																	break;
															}
													break;
												default:
													switch ($val[4]) {
																case 'mentions':
																	echo '<li class="" style="text-transform: capitalize;">'.__('Activity').'</li><li class="active" style="text-transform: capitalize;">'.__('Mentions').'</li>';
																	break;
																case 'favorites':
																	echo '<li class="" style="text-transform: capitalize;">'.__('Activity').'</li><li class="active" style="text-transform: capitalize;">'.__('Favorites').'</li>';
																	break;
																default:
																	echo '<li class="active" style="text-transform: capitalize;">'.__('Activity').'</li>';
																	break;
															}
													break;
											}
											?>
										</ul>
									 </div>
									</div>
							</div>							

							<div class="clear padding10"></div>
							<div class="page-content" style="border: none; background: transparent;">
								<div class="row">
									<?php the_content(); ?>
								</div>
								<?php edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>
							</div>

							<?php wp_link_pages(array('before' => '<p class="margin-vertical-15">' . __( 'Pages:' , 'pro' ) , 'after' => '</p>' , 'next_or_number' => 'number')); ?>

							<?php 

								$showcomments = get_post_meta( $post->ID, '_mpt_page_show_comments', true );

								if ( $showcomments == 'on') {

									echo '<div class="page-comments">';
									comments_template();
									echo '</div>';
								} 
							?>

						</div>
					</div> <!-- / <div class="col-md-12 col-sm-12 col-xs-12"> -->
				</div> <!-- / row -->

				<?php endwhile; endif; ?>

			</div><!-- / container -->
		</div><!-- / outercontainer -->	
	</div><!-- / content-section -->	

</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>