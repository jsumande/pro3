(function (imports, $) {

    /**
     * @param item
     * @returns {*}
     * @private
     */
    function __parse_pcm(item) {
        var pcm = ('__pcm' in item) ? item['__pcm'] : null;
        if (pcm != null) {
            var amount_splt = String(pcm.amount).split('.');

            // check for mantissa & <sup> it if needed.
            pcm.amount = (amount_splt.length > 0)
                ? (amount_splt[0] + (amount_splt[1] > 0 ? ('<sup>.' + amount_splt[1] + '</sup>') : ''))
                : amount_splt[0];

            // If the amount is just to large, last check was 9 is the highest
            var amount_len = String(pcm.amount).length;
            if (pcm.amount.indexOf('.') !== 0) amount_len -= 11; // the <sup></sup>;
            pcm.css = (amount_len > 9) ? ' style="zoom:0.75; -moz-transform: scale(0.75);"' : '';

            // replace keys to values
            Object.keys(pcm).filter(function (T) {
                pcm.output = pcm.output.replace('{' + T + '}', pcm[T]);
            });
        } else {
            pcm = {
                css: '',
                output: '??<sup>.??</sup> $'
            };
       }

       return pcm;
    }

    /**
     * Namespace
     * @type {{}}
     */
    var TopRankSlider = {};

    /**
     * init Slider
     * @param className
     */
    TopRankSlider.initSliders = function (className) {
        $(className).each(function (i, slider) {
            var dataOpt = $(slider).attr('data-options'),
                jsonData = JSON.parse(dataOpt),
                layout = ('layout' in jsonData) ? jsonData.layout : 'store';

            // Preloader information
            var _p_info = $('<span class="preloader"/>');
            _p_info.appendTo($(slider));

            /**
             * @param r
             */
            function onDone(r) {
                // Remove the preloader.
                _p_info.hide();

                if ('data' in r) {
                    if ('items' in r.data) {
                        var items = r.data.items,
                            isStore = ('isStore' in r.data) ? r.data['isStore'] : false,
                            item_count = r.data.count;

                        if (item_count > 0) {

                            // Check for layout (custom)
                            if (layout == 'store_1x4') {

                                function generate_items() {
                                    var tpl = "", _buff = "", wrap_count = 1;

                                    for (var i = 1; i < item_count - 1; i++) {
                                        var item = items[i], aC = '<div class="item-mini-container">', bC = '</div>';

                                        tpl += [
                                            '<div class="item-container">',
                                            '   <a href="', item.site_url, '', SliderVal.lang ,'" class="item-image" style="background-image: url(', item.pictureurl, ')"></a>',
                                            '   <div class="item-title"><a href="', item.site_url, '', SliderVal.lang ,'">', item.storename, '</a></div>',
                                            '   <div title="', item.term_names, '" class="item-cat ', (item.ratings ? 'half' : ''), '">(',
                                            (!item.term_names ? 'Home' : item.term_names), ')</div>',
                                            (item.ratings
                                                ? '<div class="item-rating">' + item.ratings + '</div>'
                                                : ''),
                                            '</div>'
                                        ].join('');

                                        if (wrap_count === 4) {
                                            _buff += aC + tpl + bC;
                                            wrap_count = 0;
                                            tpl = "";
                                        }

                                        wrap_count++;
                                    }

                                    return _buff;
                                }

                                // Should be getting max count / limit of 5 only
                                // We will take the first item as the big picture
                                var item = items[0];

                                var imu = [
                                    '<div class="', layout, '">',
                                    '<div class="sec_one">',
                                    '<div class="item">',
                                    '<div class="item-container">',
                                    '<a href="', item.site_url, '', SliderVal.lang ,'" class="item-image" style="background-image: url(',
                                    item.pictureurl, ')"></a>',
                                    '<div class="item-title"><a href="', item.site_url, '">',
                                    item.storename, '</a></div>',
                                    (item.ratings
                                        ? '<div class="item-rating">' + item.ratings + '</div>'
                                        : ''),
                                    '<div class="item-desc">', item.description.substring(0, 160), '...</div>',
                                    '<div class="item-buttons">',
                                    "<a href='", item.site_url, "", SliderVal.lang ,"' class='btn btn-primary btn-small'>"+ SliderVal.viewstore +"</a>",
                                    "<a href='#' rel='popover' data-ptoggle='hide' data-id='", item.blog_id ,"' class='btn btn-primary btn-small'>"+ SliderVal.sendmsg +"</a>",
                                    '</div>',
                                    '</div>',
                                    '</div>',
                                    '</div>',
                                    '<div class="sec_two">', generate_items(), '</div>',
                                    '</div>'
                                ].join('');

                                $(slider).html(imu);

                                // Init the slider for mini items
                                $(slider).find('.' + layout + ' .sec_two').slick({
                                    dots: false,
                                    arrows: false,
                                    infinite: true,
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    variableWidth: true,
                                    autoplay: true,
                                    autoplaySpeed: 5000
                                });
                            } else {

                                var markup_open = "<div class='item'>";
                                var markup_close = "</div>";
                                var imu = "";

                                $(slider).slick({
                                    dots: false,
                                    infinite: true,
                                    speed: 300,
                                    slidesToShow: 5,
                                    variableWidth: true,
                                    autoplay: true,
                                    autoplaySpeed: 5000
                                });

                                for (var i = 0; i < item_count; i++) {
                                    var item = items[i];

                                    // Clear template
                                    imu = "";

                                    if (layout == 'store') {
                                        imu = [
                                            '<div class="item-container ', layout, '">',
                                            '<a href="', item.site_url, '', SliderVal.lang, '" class="item-image" style="background-image: url(', item.pictureurl, ')"></a>',
                                            '<div class="item-title"><a title="', item.storename, '" href="', item.site_url, '">',
                                            item.storename, '</a></div>',
                                            '<div title="', item.term_names, '" class="item-cat ',
                                            (item.ratings ? 'half' : ''), '">(',
                                            (!item.term_names ? 'Home' : item.term_names), ')</div>',
                                            (item.ratings
                                                ? '<div class="item-rating">' + item.ratings + '</div>'
                                                : ''),
                                            '</div>'
                                        ].join('');
                                    } else if (layout == 'storelist') {
										var pcm2 = ('__pcm' in item) ? true : false;
										var pcm = __parse_pcm(item);
										if(pcm2){
											var store_fee = '<span class="item-fee">'+pcm.output+'/'+item.store_hour+'</span>';
										}else{
											var store_fee = '';
										}
                                        imu = [
                                            '<div class="item-container ', layout, '">',
                                            '<a href="', item.site_url, '/',SliderVal.qrurl,'/',SliderVal.lang,'" class="btn-quote btn btn-xs btn-primary pull-right">'+ SliderVal.reqquote +'</a>',
											'',store_fee,'',
                                            '<a href="', item.site_url, '', SliderVal.lang ,'" class="item-image" style="background-image: url(', item.pictureurl, ')"></a>',
                                            '<div class="item-title" title="', item.post_title, '"><a href="', item.site_url, '', SliderVal.lang ,'">', item.storename, '</a></div>',
                                            '<div title="', item.term_names, '" class="item-cat ', (item.ratings ? 'half' : ''), '">(',
                                            (!item.term_names ? 'Home' : item.term_names), ')</div>',
                                            '<div class="item-buttons">',
                                            "<a href='#' rel='popover' data-ptoggle='hide' data-id='", item.blog_id ,"' class='btn btn-primary btn-small' title='"+ SliderVal.sendmsg +"'>", ""+ SliderVal.sendmsg +"", "</a>",
                                            '</div>',
                                            '</div>'
                                        ].join('');
                                    } else if (layout == 'products') {
                                        var pcm = __parse_pcm(item),
                                            url =  item.image_url;
                                            value = url.substring(url.lastIndexOf('-') + 0),
                                                ext =  url.substring(url.lastIndexOf('.') + 1);
                                            url = url.replace(value, '.');
                                            var imgUrl;
                                            //if ( url+ext != "http://artbulgaria.com/wp.jpg") {
                                              //  imgUrl = url+ext;
                                           // } else {
                                                imgUrl = item.image_url;
                                           // }
                                        imu = [
                                            '<div class="item-container ', layout, '">',
                                            '<a href="', item.post_permalink, '', SliderVal.lang ,'" class="item-image t" style="background-image: url(', imgUrl , ')"></a>',
                                            '<div class="item-title">', '<a title="', item.post_title ,'', SliderVal.lang ,'" href="', item.post_permalink, '">',
                                            item.post_title, '</a></div>',
                                            '<div title="', item.term_names, '" class="item-cat ', (item.ratings ? 'half' : ''), '">(',
                                            (!item.term_names ? 'Home' : item.term_names), ')</div>',
                                            (item.ratings
                                                ? '<div class="item-rating">' + item.ratings + '</div>'
                                                : ''),
                                            '<hr/>',
                                            '<div class="item-price">',
                                            '<span', pcm.css,'>', pcm.output, '</span>',
                                            '</div>',
                                            '<div class="item-buttons">',
                                            "<a href='", item.post_permalink, "", SliderVal.lang ,"' class='btn btn-primary btn-small'>",
                                            "<i class='fa fa-shopping-cart'></i>&nbsp; "+ SliderVal.shopnow +"</a>",
                                            '</div>',
                                            '</div>'
                                        ].join('');
                                    } else if (layout == 'performers') {
										var pcm2 = ('__pcm' in item) ? true : false;
										var pcm = __parse_pcm(item);
										if(pcm2){
											var store_fee = '<span class="item-fee">'+pcm.output+'/'+item.store_hour+'</span>';
										}else{
											var store_fee = '';
										}
                                        imu = [
                                            '<div class="item-container ', layout, '">',
                                                '<a href="', item.site_url, '/',SliderVal.qrurl,'/',SliderVal.lang,'" class="btn-quote btn btn-xs btn-primary pull-right">'+ SliderVal.reqquote +'</a>',
												'',store_fee,'',
                                                '<a href="', item.post_permalink, '', SliderVal.lang ,'" class="item-image" style="background-image: url(', item.image_url, ')"></a>',
                                                '<div class="item-title" title="', item.post_title, '"><a href="', item.post_permalink, '', SliderVal.lang ,'">', item.post_title, '</a></div>',
                                                '<div title="', item.term_names, '" class="item-cat ', (item.ratings ? 'half' : ''), '">(',
                                                    (!item.term_names ? 'Home' : item.term_names), ')</div>',
                                                '<div class="item-buttons">',
                                                    "<a href='#' rel='popover' data-ptoggle='hide' data-id='", item.blog_id ,"' class='btn btn-primary btn-small' title='"+ SliderVal.sendmsg +"'>", ""+ SliderVal.sendmsg +"", "</a>",
                                                '</div>',
                                            '</div>'
                                        ].join('');
                                    } else if (layout == 'events') {
										var date_event = '';
										var event_location = '&nbsp;';
										if(item.event_start){
											var date_event = '<div class="item-date" title="'+item.event_start+'">'+item.event_start+'</div>';
										}
										
										if(item.event_location){
											var event_location = item.event_location;
										}										
										
                                        imu = [
                                            '<div class="item-container ', layout, '">',
                                                '<a href="', item.post_permalink, '', SliderVal.lang ,'" class="item-image" style="background-image: url(', item.image_url, ')"></a>',
                                                '<div class="item-title" title="', item.post_title, '"><a href="', item.post_permalink, '', SliderVal.lang ,'">', item.post_title, '</a></div>',
												'<div class="item-date" title="'+event_location+'">'+event_location+'</div>', date_event, '',
                                                '<div class="item-buttons">',
                                                    "<a href='", item.post_permalink, "", SliderVal.lang ,"' class='btn btn-primary btn-small' title='"+ SliderVal.booknow +"'>", ""+ SliderVal.booknow +"", "</a>",
                                                '</div>',
                                            '</div>'
                                        ].join('');										
                                    } else if (layout == 'blogroll') {
                                        imu = [
                                            '<div class="item-container ', layout, '">',
                                                '<a href="', item.guid, '" class="item-image">',
                                                    '<div class="item-imagesrc" style="background-image: url(', item.image_url ,')"></div>',
                                                '</a>',
                                                '<div class="item-title" title="', item.post_title, '">', item.post_title, '</div>',
                                                '<div class="item-content clearfix">', item.content, '</div>',
                                                '<div class="pull-left item-date">', item.post_date_gmt, '</div>',
                                                '<div class="pull-right item-readmore">',
                                                    '<a href="', item.guid, '">'+ SliderVal.readmore +'</a>',
                                                '</div>',
                                            '</div>'
                                        ].join('');
                                    }
                                    $(slider).slick('slickAdd', markup_open + imu + markup_close);
                                }
                            }
                        }
                    }
                }

                bindSendMessage($(slider));
            };

            setTimeout(function () {
                window.svc('tpslider.svc', jsonData).done(onDone);
            }, 0);
        });
    };

    // Global
    imports.TopRankSlider = TopRankSlider;

    /**
     * Send Message
     */
    function bindSendMessage($scope) {
        $scope.find('a[rel="popover"]').each(function(i, btn) {
            // Create a Dynamic ID
            var dynid = (new Date()).getTime();
            $(btn).attr('data-dynid', dynid);

            // Create Popover
            $(btn).popover({
                placement: 'bottom',
                html: true,
                container: 'body',
                template: '<div data-pop-id="' + dynid + '" class="popover">' +
                            '<div class="arrow"></div>' +
                            '<div class="popover-content"></div></div>',
                content: function () {
                    return '<div class="form">' +
                        '<label>'+ SliderVal.subject +':</label>' +
                        '<input type="text" class="form-control"/>' +
                        '<label>'+ SliderVal.msg +':</label>' +
                        '<textarea class="form-control" style="margin-top:5px;resize:none;"></textarea>' +
                        '</div>' +
                        '<div class="sendmsg_info"></div>' +
                        '<div class="sendmsg_btns">' +
                        '<button class="btn btn-primary btn-sm" style="margin-right:3px;">'+ SliderVal.send +'</button>' +
                        '<button class="btn btn-default btn-sm">'+ SliderVal.close +'</button></div>';
                }
            }).on('show.bs.popover', function () {
                $(btn).prop('data-ptoggle', 'show');
            }).on('hide.bs.popover', function () {
                $(btn).prop('data-ptoggle', 'hide');
            }).click(function (e) {
                e.preventDefault();

                var toggle = $(btn).prop('data-ptoggle');
                var con = $('[data-pop-id="' + dynid + '"]').find('.popover-content');
                var infobox = con.find('.sendmsg_info');
                var formbox = con.find('.form');
                var btnsbox = con.find('.sendmsg_btns');

                con.css('width', '250px');

                if (toggle == 'show') {
                    var sendbtn = con.find('button').first();
                    var closebtn = con.find('button').last();
                    var subject = con.find('input[type=text]');
                    var message = con.find('textarea');
                    var dataid = $(btn).attr('data-id');

                    sendbtn.off().on('click', function () {
                        window.svc('sendmsgstore', [dataid, message.val(), subject.val()], {
                            beforeSend: function () {
                                formbox.hide();
                                btnsbox.hide();
                                infobox.removeClass('alert alert-success alert-danger');
                                infobox.addClass('tp_slider_ajax_loadbg');
                                infobox.show();
                                infobox.html("&nbsp;");
                            }
                        }).fail(function (xhr) {
                            formbox.show();
                            btnsbox.show();
                            infobox.removeClass('tp_slider_ajax_loadbg');
                            infobox.addClass('alert alert-danger').show().text(SliderVal.error + ': ' + xhr.responseText);
                        }).success(function (r) {
                            formbox.hide();
                            btnsbox.show();
                            sendbtn.hide();
                            closebtn.show();
                            infobox.css('margin-bottom', '10px');
                            infobox.removeClass('tp_slider_ajax_loadbg');
                            infobox.show().text(r.data);
                        });
                    });

                    closebtn.off().on('click', function () {
                        $(btn).popover('hide');
                    });
                }
            });
        });
    };

})(window, jQuery);