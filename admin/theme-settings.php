<?php
add_action('init','m413_options');

if (!function_exists('m413_options')) {
	function m413_options(){

		$themename = 'pro';
		$shortname = "mpt";  // you need to replace this with your own theme name so you avoid naming collisions
		global $mbb_options, $mp;  // add all options to an array
		$mbb_options = get_option('m413_options');

		$mbb_pages = array(); // get all wp pages and view them as an array
		$mbb_pages_obj = get_pages('sort_column=post_parent,menu_order');
		foreach ($mbb_pages_obj as $mbb_page) {
			$mbb_pages[$mbb_page->ID] = $mbb_page->post_name; }
		$mbb_pages_tmp = array_unshift($mbb_pages, "Select a page:");

		$mbb_posts = array(); // get all wp posts and view them as an array
		$mbb_posts_obj = get_posts();
		foreach ($mbb_posts_obj as $mbb_post){
			$mbb_posts[$mbb_post->ID] = $mbb_post->post_title; }
		$mbb_posts_tmp = array_unshift($mbb_posts, "Select a post:");

		$mbb_categories = array(); // get all wp categories and view them as an array
		$mbb_categories_obj = get_categories('hide_empty=0');
		foreach ($mbb_categories_obj as $mbb_cat) {
			$mbb_categories[$mbb_cat->cat_ID] = $mbb_cat->cat_name;}
		$mbb_categories_tmp = array_unshift($mbb_categories, "Select a category:");

		$sample_array = array("1","2","3","4","5");  // sample for testing
		$sample_advanced_array = array("image" => "The Image","post" => "The Post"); // sample for testing

		$adminimagefolder =  get_template_directory_uri() . '/admin/images/'; // where saving images
		$themefolder =  get_template_directory_uri(); // theme directory

		$mpt_google_web_fonts = array(
			'abeezee' => 'ABeeZee',
			'abel' => 'Abel',
			'abrilfatface' => 'Abril Fatface',
			'aclonica' => 'Aclonica',
			'acme' => 'Acme',
			'actor' => 'Actor',
			'adamina' => 'Adamina',
			'adventpro' => 'Advent Pro',
			'aguafinascript' => 'Aguafina Script',
			'akronim' => 'Akronim',
			'aladin' => 'Aladin',
			'aldrich' => 'Aldrich',
			'alegreya' => 'Alegreya',
			'alegreyasc' => 'Alegreya SC',
			'alexbrush' => 'Alex Brush',
			'alfaslabone' => 'Alfa Slab One',
			'alice' => 'Alice',
			'alike' => 'Alike',
			'alikeangular' => 'Alike Angular',
			'allan' => 'Allan',
			'allerta' => 'Allerta',
			'allertastencil' => 'Allerta Stencil',
			'allura' => 'Allura',
			'almendra' => 'Almendra',
			'almendrasc' => 'Almendra SC',
			'amarante' => 'Amarante',
			'amaranth' => 'Amaranth',
			'amaticsc' => 'Amatic SC',
			'amethysta' => 'Amethysta',
			'anaheim' => 'Anaheim',
			'andada' => 'Andada',
			'andika' => 'Andika',
			'angkor' => 'Angkor',
			'annieuseyourtelescope' => 'Annie Use Your Telescope',
			'anonymouspro' => 'Anonymous Pro',
			'antic' => 'Antic',
			'anticdidone' => 'Antic Didone',
			'anticslab' => 'Antic Slab',
			'anton' => 'Anton',
			'arapey' => 'Arapey',
			'arbutus' => 'Arbutus',
			'arbutusslab' => 'Arbutus Slab',
			'architectsdaughter' => 'Architects Daughter',
			'archivoblack' => 'Archivo Black',
			'archivonarrow' => 'Archivo Narrow',
			'arimo' => 'Arimo',
			'arizonia' => 'Arizonia',
			'armata' => 'Armata',
			'artifika' => 'Artifika',
			'arvo' => 'Arvo',
			'asap' => 'Asap',
			'asset' => 'Asset',
			'astloch' => 'Astloch',
			'asul' => 'Asul',
			'atomicage' => 'Atomic Age',
			'aubrey' => 'Aubrey',
			'audiowide' => 'Audiowide',
			'autourone' => 'Autour One',
			'average' => 'Average',
			'averagesans' => 'Average Sans',
			'averiagruesalibre' => 'Averia Gruesa Libre',
			'averialibre' => 'Averia Libre',
			'averiasanslibre' => 'Averia Sans Libre',
			'averiaseriflibre' => 'Averia Serif Libre',
			'badscript' => 'Bad Script',
			'balthazar' => 'Balthazar',
			'bangers' => 'Bangers',
			'basic' => 'Basic',
			'battambang' => 'Battambang',
			'baumans' => 'Baumans',
			'bayon' => 'Bayon',
			'belgrano' => 'Belgrano',
			'belleza' => 'Belleza',
			'benchnine' => 'BenchNine',
			'bentham' => 'Bentham',
			'berkshireswash' => 'Berkshire Swash',
			'bevan' => 'Bevan',
			'bigshotone' => 'Bigshot One',
			'bilbo' => 'Bilbo',
			'bilboswashcaps' => 'Bilbo Swash Caps',
			'bitter' => 'Bitter',
			'blackopsone' => 'Black Ops One',
			'bokor' => 'Bokor',
			'bonbon' => 'Bonbon',
			'boogaloo' => 'Boogaloo',
			'bowlbyone' => 'Bowlby One',
			'bowlbyonesc' => 'Bowlby One SC',
			'brawler' => 'Brawler',
			'breeserif' => 'Bree Serif',
			'bubblegumsans' => 'Bubblegum Sans',
			'bubblerone' => 'Bubbler One',
			'buda' => 'Buda',
			'buenard' => 'Buenard',
			'butcherman' => 'Butcherman',
			'butterflykids' => 'Butterfly Kids',
			'cabin' => 'Cabin',
			'cabincondensed' => 'Cabin Condensed',
			'cabinsketch' => 'Cabin Sketch',
			'caesardressing' => 'Caesar Dressing',
			'cagliostro' => 'Cagliostro',
			'calligraffitti' => 'Calligraffitti',
			'cambo' => 'Cambo',
			'candal' => 'Candal',
			'cantarell' => 'Cantarell',
			'cantataone' => 'Cantata One',
			'cantoraone' => 'Cantora One',
			'capriola' => 'Capriola',
			'cardo' => 'Cardo',
			'carme' => 'Carme',
			'carroisgothic' => 'Carrois Gothic',
			'carroisgothicsc' => 'Carrois Gothic SC',
			'carterone' => 'Carter One',
			'caudex' => 'Caudex',
			'cedarvillecursive' => 'Cedarville Cursive',
			'cevicheone' => 'Ceviche One',
			'changaone' => 'Changa One',
			'chango' => 'Chango',
			'chauphilomeneone' => 'Chau Philomene One',
			'chelaone' => 'Chela One',
			'chelseamarket' => 'Chelsea Market',
			'chenla' => 'Chenla',
			'cherrycreamsoda' => 'Cherry Cream Soda',
			'cherryswash' => 'Cherry Swash',
			'chewy' => 'Chewy',
			'chicle' => 'Chicle',
			'chivo' => 'Chivo',
			'cinzel' => 'Cinzel',
			'cinzeldecorative' => 'Cinzel Decorative',
			'coda' => 'Coda',
			'codacaption' => 'Coda Caption',
			'codystar' => 'Codystar',
			'combo' => 'Combo',
			'comfortaa' => 'Comfortaa',
			'comingsoon' => 'Coming Soon',
			'concertone' => 'Concert One',
			'condiment' => 'Condiment',
			'content' => 'Content',
			'contrailone' => 'Contrail One',
			'convergence' => 'Convergence',
			'cookie' => 'Cookie',
			'copse' => 'Copse',
			'corben' => 'Corben',
			'courgette' => 'Courgette',
			'cousine' => 'Cousine',
			'coustard' => 'Coustard',
			'coveredbyyourgrace' => 'Covered By Your Grace',
			'craftygirls' => 'Crafty Girls',
			'creepster' => 'Creepster',
			'creteround' => 'Crete Round',
			'crimsontext' => 'Crimson Text',
			'crushed' => 'Crushed',
			'cuprum' => 'Cuprum',
			'cutive' => 'Cutive',
			'cutivemono' => 'Cutive Mono',
			'damion' => 'Damion',
			'dancingscript' => 'Dancing Script',
			'dangrek' => 'Dangrek',
			'dawningofanew' => 'Dawning of a New Day',
			'daysone' => 'Days One',
			'delius' => 'Delius',
			'deliusswashcaps' => 'Delius Swash Caps',
			'deliusunicase' => 'Delius Unicase',
			'dellarespira' => 'Della Respira',
			'devonshire' => 'Devonshire',
			'didactgothic' => 'Didact Gothic',
			'diplomata' => 'Diplomata',
			'diplomatasc' => 'Diplomata SC',
			'doppioone' => 'Doppio One',
			'dorsa' => 'Dorsa',
			'dosis' => 'Dosis',
			'drsugiyama' => 'Dr Sugiyama',
			'droidsans' => 'Droid Sans',
			'droidsansmono' => 'Droid Sans Mono',
			'droidserif' => 'Droid Serif',
			'durusans' => 'Duru Sans',
			'dynalight' => 'Dynalight',
			'ebgaramond' => 'EB Garamond',
			'eaglelake' => 'Eagle Lake',
			'eater' => 'Eater',
			'economica' => 'Economica',
			'electrolize' => 'Electrolize',
			'emblemaone' => 'Emblema One',
			'emilyscandy' => 'Emilys Candy',
			'engagement' => 'Engagement',
			'enriqueta' => 'Enriqueta',
			'ericaone' => 'Erica One',
			'esteban' => 'Esteban',
			'euphoriascript' => 'Euphoria Script',
			'ewert' => 'Ewert',
			'exo' => 'Exo',
			'expletussans' => 'Expletus Sans',
			'fanwoodtext' => 'Fanwood Text',
			'fascinate' => 'Fascinate',
			'fascinateinline' => 'Fascinate Inline',
			'fasterone' => 'Faster One',
			'fasthand' => 'Fasthand',
			'federant' => 'Federant',
			'federo' => 'Federo',
			'felipa' => 'Felipa',
			'fenix' => 'Fenix',
			'fingerpaint' => 'Finger Paint',
			'fjordone' => 'Fjord One',
			'flamenco' => 'Flamenco',
			'flavors' => 'Flavors',
			'fondamento' => 'Fondamento',
			'fontdinerswanky' => 'Fontdiner Swanky',
			'forum' => 'Forum',
			'francoisone' => 'Francois One',
			'frederickathegreat' => 'Fredericka the Great',
			'fredokaone' => 'Fredoka One',
			'freehand' => 'Freehand',
			'fresca' => 'Fresca',
			'frijole' => 'Frijole',
			'fugazone' => 'Fugaz One',
			'gfsdidot' => 'GFS Didot',
			'gfsneohellenic' => 'GFS Neohellenic',
			'galdeano' => 'Galdeano',
			'galindo' => 'Galindo',
			'gentiumbasic' => 'Gentium Basic',
			'gentiumbookbasic' => 'Gentium Book Basic',
			'geo' => 'Geo',
			'geostar' => 'Geostar',
			'geostarfill' => 'Geostar Fill',
			'germaniaone' => 'Germania One',
			'giveyouglory' => 'Give You Glory',
			'glassantiqua' => 'Glass Antiqua',
			'glegoo' => 'Glegoo',
			'gloriahallelujah' => 'Gloria Hallelujah',
			'goblinone' => 'Goblin One',
			'gochihand' => 'Gochi Hand',
			'gorditas' => 'Gorditas',
			'goudybookletter1911' => 'Goudy Bookletter 1911',
			'graduate' => 'Graduate',
			'gravitasone' => 'Gravitas One',
			'greatvibes' => 'Great Vibes',
			'griffy' => 'Griffy',
			'gruppo' => 'Gruppo',
			'gudea' => 'Gudea',
			'habibi' => 'Habibi',
			'hammersmithone' => 'Hammersmith One',
			'handlee' => 'Handlee',
			'hanuman' => 'Hanuman',
			'happymonkey' => 'Happy Monkey',
			'headlandone' => 'Headland One',
			'hennypenny' => 'Henny Penny',
			'herrvonmuellerhoff' => 'Herr Von Muellerhoff',
			'holtwoodonesc' => 'Holtwood One SC',
			'homemadeapple' => 'Homemade Apple',
			'homenaje' => 'Homenaje',
			'imfelldwpica' => 'IM Fell DW Pica',
			'imfelldwpica' => 'IM Fell DW Pica SC',
			'imfelldoublepica' => 'IM Fell Double Pica',
			'imfelldoublepica' => 'IM Fell Double Pica SC',
			'imfellenglish' => 'IM Fell English',
			'imfellenglishsc' => 'IM Fell English SC',
			'imfellfrenchcanon' => 'IM Fell French Canon',
			'imfellfrenchcanon' => 'IM Fell French Canon SC',
			'imfellgreatprimer' => 'IM Fell Great Primer',
			'imfellgreatprimer' => 'IM Fell Great Primer SC',
			'iceberg' => 'Iceberg',
			'iceland' => 'Iceland',
			'imprima' => 'Imprima',
			'inconsolata' => 'Inconsolata',
			'inder' => 'Inder',
			'indieflower' => 'Indie Flower',
			'inika' => 'Inika',
			'irishgrover' => 'Irish Grover',
			'istokweb' => 'Istok Web',
			'italiana' => 'Italiana',
			'italianno' => 'Italianno',
			'jacquesfrancois' => 'Jacques Francois',
			'jacquesfrancoisshadow' => 'Jacques Francois Shadow',
			'jimnightshade' => 'Jim Nightshade',
			'jockeyone' => 'Jockey One',
			'jollylodger' => 'Jolly Lodger',
			'josefinsans' => 'Josefin Sans',
			'josefinslab' => 'Josefin Slab',
			'judson' => 'Judson',
			'julee' => 'Julee',
			'juliussansone' => 'Julius Sans One',
			'junge' => 'Junge',
			'jura' => 'Jura',
			'justanotherhand' => 'Just Another Hand',
			'justmeagaindown' => 'Just Me Again Down Here',
			'kameron' => 'Kameron',
			'karla' => 'Karla',
			'kaushanscript' => 'Kaushan Script',
			'kellyslab' => 'Kelly Slab',
			'kenia' => 'Kenia',
			'khmer' => 'Khmer',
			'kiteone' => 'Kite One',
			'knewave' => 'Knewave',
			'kottaone' => 'Kotta One',
			'koulen' => 'Koulen',
			'kranky' => 'Kranky',
			'kreon' => 'Kreon',
			'kristi' => 'Kristi',
			'kronaone' => 'Krona One',
			'labelleaurore' => 'La Belle Aurore',
			'lancelot' => 'Lancelot',
			'lato' => 'Lato',
			'leaguescript' => 'League Script',
			'leckerlione' => 'Leckerli One',
			'ledger' => 'Ledger',
			'lekton' => 'Lekton',
			'lemon' => 'Lemon',
			'lifesavers' => 'Life Savers',
			'lilitaone' => 'Lilita One',
			'limelight' => 'Limelight',
			'lindenhill' => 'Linden Hill',
			'lobster' => 'Lobster',
			'lobstertwo' => 'Lobster Two',
			'londrinaoutline' => 'Londrina Outline',
			'londrinashadow' => 'Londrina Shadow',
			'londrinasketch' => 'Londrina Sketch',
			'londrinasolid' => 'Londrina Solid',
			'lora' => 'Lora',
			'loveyalikea' => 'Love Ya Like A Sister',
			'lovedbytheking' => 'Loved by the King',
			'loversquarrel' => 'Lovers Quarrel',
			'luckiestguy' => 'Luckiest Guy',
			'lusitana' => 'Lusitana',
			'lustria' => 'Lustria',
			'macondo' => 'Macondo',
			'macondoswashcaps' => 'Macondo Swash Caps',
			'magra' => 'Magra',
			'maidenorange' => 'Maiden Orange',
			'mako' => 'Mako',
			'marcellus' => 'Marcellus',
			'marcellussc' => 'Marcellus SC',
			'marckscript' => 'Marck Script',
			'markoone' => 'Marko One',
			'marmelad' => 'Marmelad',
			'marvel' => 'Marvel',
			'mate' => 'Mate',
			'matesc' => 'Mate SC',
			'mavenpro' => 'Maven Pro',
			'mclaren' => 'McLaren',
			'meddon' => 'Meddon',
			'medievalsharp' => 'MedievalSharp',
			'medulaone' => 'Medula One',
			'megrim' => 'Megrim',
			'meiescript' => 'Meie Script',
			'meriendaone' => 'Merienda One',
			'merriweather' => 'Merriweather',
			'metal' => 'Metal',
			'metalmania' => 'Metal Mania',
			'metamorphous' => 'Metamorphous',
			'metrophobic' => 'Metrophobic',
			'michroma' => 'Michroma',
			'miltonian' => 'Miltonian',
			'miltoniantattoo' => 'Miltonian Tattoo',
			'miniver' => 'Miniver',
			'missfajardose' => 'Miss Fajardose',
			'modernantiqua' => 'Modern Antiqua',
			'molengo' => 'Molengo',
			'molle' => 'Molle',
			'monofett' => 'Monofett',
			'monoton' => 'Monoton',
			'monsieurladoulaise' => 'Monsieur La Doulaise',
			'montaga' => 'Montaga',
			'montez' => 'Montez',
			'montserrat' => 'Montserrat',
			'montserratalternates' => 'Montserrat Alternates',
			'montserratsubrayada' => 'Montserrat Subrayada',
			'moul' => 'Moul',
			'moulpali' => 'Moulpali',
			'mountainsofchristmas' => 'Mountains of Christmas',
			'mrbedfort' => 'Mr Bedfort',
			'mrdafoe' => 'Mr Dafoe',
			'mrdehaviland' => 'Mr De Haviland',
			'mrssaintdelafield' => 'Mrs Saint Delafield',
			'mrssheppards' => 'Mrs Sheppards',
			'muli' => 'Muli',
			'mysteryquest' => 'Mystery Quest',
			'neucha' => 'Neucha',
			'neuton' => 'Neuton',
			'newscycle' => 'News Cycle',
			'niconne' => 'Niconne',
			'nixieone' => 'Nixie One',
			'nobile' => 'Nobile',
			'nokora' => 'Nokora',
			'norican' => 'Norican',
			'nosifer' => 'Nosifer',
			'nothingyoucoulddo' => 'Nothing You Could Do',
			'noticiatext' => 'Noticia Text',
			'novacut' => 'Nova Cut',
			'novaflat' => 'Nova Flat',
			'novamono' => 'Nova Mono',
			'novaoval' => 'Nova Oval',
			'novaround' => 'Nova Round',
			'novascript' => 'Nova Script',
			'novaslim' => 'Nova Slim',
			'novasquare' => 'Nova Square',
			'numans' => 'Numans',
			'nunito' => 'Nunito',
			'odormeanchey' => 'Odor Mean Chey',
			'offside' => 'Offside',
			'oldstandardtt' => 'Old Standard TT',
			'oldenburg' => 'Oldenburg',
			'oleoscript' => 'Oleo Script',
			'opensans' => 'Open Sans',
			'opensanscondensed' => 'Open Sans Condensed',
			'oranienbaum' => 'Oranienbaum',
			'orbitron' => 'Orbitron',
			'oregano' => 'Oregano',
			'orienta' => 'Orienta',
			'originalsurfer' => 'Original Surfer',
			'oswald' => 'Oswald',
			'overtherainbow' => 'Over the Rainbow',
			'overlock' => 'Overlock',
			'overlocksc' => 'Overlock SC',
			'ovo' => 'Ovo',
			'oxygen' => 'Oxygen',
			'oxygenmono' => 'Oxygen Mono',
			'ptmono' => 'PT Mono',
			'ptsans' => 'PT Sans',
			'ptsanscaption' => 'PT Sans Caption',
			'ptsansnarrow' => 'PT Sans Narrow',
			'ptserif' => 'PT Serif',
			'ptserifcaption' => 'PT Serif Caption',
			'pacifico' => 'Pacifico',
			'paprika' => 'Paprika',
			'parisienne' => 'Parisienne',
			'passeroone' => 'Passero One',
			'passionone' => 'Passion One',
			'patrickhand' => 'Patrick Hand',
			'patuaone' => 'Patua One',
			'paytoneone' => 'Paytone One',
			'peralta' => 'Peralta',
			'permanentmarker' => 'Permanent Marker',
			'petitformalscript' => 'Petit Formal Script',
			'petrona' => 'Petrona',
			'philosopher' => 'Philosopher',
			'piedra' => 'Piedra',
			'pinyonscript' => 'Pinyon Script',
			'plaster' => 'Plaster',
			'play' => 'Play',
			'playball' => 'Playball',
			'playfairdisplay' => 'Playfair Display',
			'playfairdisplaysc' => 'Playfair Display SC',
			'podkova' => 'Podkova',
			'poiretone' => 'Poiret One',
			'pollerone' => 'Poller One',
			'poly' => 'Poly',
			'pompiere' => 'Pompiere',
			'pontanosans' => 'Pontano Sans',
			'portlligatsans' => 'Port Lligat Sans',
			'portlligatslab' => 'Port Lligat Slab',
			'prata' => 'Prata',
			'preahvihear' => 'Preahvihear',
			'pressstart2p' => 'Press Start 2P',
			'princesssofia' => 'Princess Sofia',
			'prociono' => 'Prociono',
			'prostoone' => 'Prosto One',
			'puritan' => 'Puritan',
			'quando' => 'Quando',
			'quantico' => 'Quantico',
			'quattrocento' => 'Quattrocento',
			'quattrocentosans' => 'Quattrocento Sans',
			'questrial' => 'Questrial',
			'quicksand' => 'Quicksand',
			'qwigley' => 'Qwigley',
			'racingsansone' => 'Racing Sans One',
			'radley' => 'Radley',
			'raleway' => 'Raleway',
			'ralewaydots' => 'Raleway Dots',
			'rammettoone' => 'Rammetto One',
			'ranchers' => 'Ranchers',
			'rancho' => 'Rancho',
			'rationale' => 'Rationale',
			'redressed' => 'Redressed',
			'reeniebeanie' => 'Reenie Beanie',
			'revalia' => 'Revalia',
			'ribeye' => 'Ribeye',
			'ribeyemarrow' => 'Ribeye Marrow',
			'righteous' => 'Righteous',
			'rochester' => 'Rochester',
			'rocksalt' => 'Rock Salt',
			'rokkitt' => 'Rokkitt',
			'romanesco' => 'Romanesco',
			'ropasans' => 'Ropa Sans',
			'rosario' => 'Rosario',
			'rosarivo' => 'Rosarivo',
			'rougescript' => 'Rouge Script',
			'ruda' => 'Ruda',
			'rugeboogie' => 'Ruge Boogie',
			'ruluko' => 'Ruluko',
			'ruslandisplay' => 'Ruslan Display',
			'russoone' => 'Russo One',
			'ruthie' => 'Ruthie',
			'rye' => 'Rye',
			'sail' => 'Sail',
			'salsa' => 'Salsa',
			'sanchez' => 'Sanchez',
			'sancreek' => 'Sancreek',
			'sansitaone' => 'Sansita One',
			'sarina' => 'Sarina',
			'satisfy' => 'Satisfy',
			'scada' => 'Scada',
			'schoolbell' => 'Schoolbell',
			'seaweedscript' => 'Seaweed Script',
			'sevillana' => 'Sevillana',
			'seymourone' => 'Seymour One',
			'shadowsintolight' => 'Shadows Into Light',
			'shadowsintolighttwo' => 'Shadows Into Light Two',
			'shanti' => 'Shanti',
			'share' => 'Share',
			'sharetech' => 'Share Tech',
			'sharetechmono' => 'Share Tech Mono',
			'shojumaru' => 'Shojumaru',
			'shortstack' => 'Short Stack',
			'siemreap' => 'Siemreap',
			'sigmarone' => 'Sigmar One',
			'signika' => 'Signika',
			'signikanegative' => 'Signika Negative',
			'simonetta' => 'Simonetta',
			'sirinstencil' => 'Sirin Stencil',
			'sixcaps' => 'Six Caps',
			'skranji' => 'Skranji',
			'slackey' => 'Slackey',
			'smokum' => 'Smokum',
			'smythe' => 'Smythe',
			'sniglet' => 'Sniglet',
			'snippet' => 'Snippet',
			'sofadione' => 'Sofadi One',
			'sofia' => 'Sofia',
			'sonsieone' => 'Sonsie One',
			'sortsmillgoudy' => 'Sorts Mill Goudy',
			'sourcecodepro' => 'Source Code Pro',
			'sourcesanspro' => 'Source Sans Pro',
			'specialelite' => 'Special Elite',
			'spicyrice' => 'Spicy Rice',
			'spinnaker' => 'Spinnaker',
			'spirax' => 'Spirax',
			'squadaone' => 'Squada One',
			'stalinistone' => 'Stalinist One',
			'stardosstencil' => 'Stardos Stencil',
			'stintultracondensed' => 'Stint Ultra Condensed',
			'stintultraexpanded' => 'Stint Ultra Expanded',
			'stoke' => 'Stoke',
			'strait' => 'Strait',
			'sueellenfrancisco' => 'Sue Ellen Francisco',
			'sunshiney' => 'Sunshiney',
			'supermercadoone' => 'Supermercado One',
			'suwannaphum' => 'Suwannaphum',
			'swankyandmoomoo' => 'Swanky and Moo Moo',
			'syncopate' => 'Syncopate',
			'tangerine' => 'Tangerine',
			'taprom' => 'Taprom',
			'telex' => 'Telex',
			'tenorsans' => 'Tenor Sans',
			'thegirlnextdoor' => 'The Girl Next Door',
			'tienne' => 'Tienne',
			'tinos' => 'Tinos',
			'titanone' => 'Titan One',
			'titilliumweb' => 'Titillium Web',
			'tradewinds' => 'Trade Winds',
			'trocchi' => 'Trocchi',
			'trochut' => 'Trochut',
			'trykker' => 'Trykker',
			'tulpenone' => 'Tulpen One',
			'ubuntu' => 'Ubuntu',
			'ubuntucondensed' => 'Ubuntu Condensed',
			'ubuntumono' => 'Ubuntu Mono',
			'ultra' => 'Ultra',
			'uncialantiqua' => 'Uncial Antiqua',
			'underdog' => 'Underdog',
			'unicaone' => 'Unica One',
			'unifrakturcook' => 'UnifrakturCook',
			'unifrakturmaguntia' => 'UnifrakturMaguntia',
			'unkempt' => 'Unkempt',
			'unlock' => 'Unlock',
			'unna' => 'Unna',
			'vt323' => 'VT323',
			'varela' => 'Varela',
			'varelaround' => 'Varela Round',
			'vastshadow' => 'Vast Shadow',
			'vibur' => 'Vibur',
			'vidaloka' => 'Vidaloka',
			'viga' => 'Viga',
			'voces' => 'Voces',
			'volkhov' => 'Volkhov',
			'vollkorn' => 'Vollkorn',
			'voltaire' => 'Voltaire',
			'waitingforthesunrise' => 'Waiting for the Sunrise',
			'wallpoet' => 'Wallpoet',
			'walterturncoat' => 'Walter Turncoat',
			'warnes' => 'Warnes',
			'wellfleet' => 'Wellfleet',
			'wireone' => 'Wire One',
			'yanonekaffeesatz' => 'Yanone Kaffeesatz',
			'yellowtail' => 'Yellowtail',
			'yesevaone' => 'Yeseva One',
			'yesteryear' => 'Yesteryear',
			'zeyada' => 'Zeyada'
			);

		$mpt_bg_patterns = array(
			'none' => $themefolder . '/img/patterns/none.png',
			'pattern_1' => $themefolder . '/img/patterns/pat_01_preview.png',
			'pattern_2' => $themefolder . '/img/patterns/pat_02_preview.png',
			'pattern_3' => $themefolder . '/img/patterns/pat_03_preview.png',
			'pattern_4' => $themefolder . '/img/patterns/pat_04_preview.png',
			'pattern_5' => $themefolder . '/img/patterns/pat_05_preview.png',
			'pattern_6' => $themefolder . '/img/patterns/pat_06_preview.png',
			'pattern_7' => $themefolder . '/img/patterns/pat_07_preview.png',
			'pattern_8' => $themefolder . '/img/patterns/pat_08_preview.png',
			'pattern_9' => $themefolder . '/img/patterns/pat_09_preview.png',
			'pattern_10' => $themefolder . '/img/patterns/pat_10_preview.png'
			);

		$btncolor_options = array(
			'grey' => 'Grey',
			'blue' => 'Blue',
			'lightblue' => 'Light Blue',
			'green' => 'Green',
			'red' => 'Red',
			'yellow' => 'Yellow',
			'black' => 'Black',
		);

		$labelcolor_options = array(
			'grey' => 'Grey',
			'blue' => 'Blue',
			'green' => 'Green',
			'red' => 'Red',
			'yellow' => 'Yellow',
			'black' => 'Black',
		);

		$tagcolor_options = array(
			'white' => 'White',
			'blue' => 'Blue',
			'lightblue' => 'Light Blue',
			'green' => 'Green',
			'red' => 'Red',
			'yellow' => 'Yellow',
			'black' => 'Black',
		);

		$socialicon_options = array(
			'none' => 'None',
			'facebook' => 'Facebook',
			'dribbble' => 'Dribbble',
			'flickr' => 'Flickr',
			'foursquare' => 'FourSquare',
			'google-plus' => 'Google Plus',
			'instagram' => 'Instagram',
			'linkedin' => 'Linkedin',
			'pinterest' => 'Pinterest',
			'renren' => 'RenRen',
			'skype' => 'Skype',
			'tumblr' => 'Tumblr',
			'twitter' => 'Twitter',
			'vk' => 'VK',
			'weibo' => 'Weibo',
			'xing' => 'Xing',
			'youtube' => 'YouTube',
			'rss' => 'RSS'
		);

/*  THE OPTIONS PANEL CREATION STARTS HERE */

		$options = array(); // DO NOT REMOVE
		
	/**** General Settings **************************************************************************************************/

		$options[] = array( "name" => __('General Settings',$themename),
					"type" => "heading");

		$options[] = array( "name" => __('Website Logo',$themename),
					"desc" => __('Upload a custom logo for your Website.',$themename),
					"id" => $shortname."_sitelogo",
					"std" => "",
					"type" => "upload");

		$options[] = array( "name" => __('Select Logo Position',$themename),
					"desc" => __('Select the position of your custom logo (This will not affects the logo position of Header Style 3).',$themename),
					"id" => $shortname."_sitelogo_position",
					"std" => "Align Center",
					"type" => "select",
					"options" => array(
						'align-left' => 'Align Left',
						'align-center' => 'Align Center',
						'align-right' => 'Align Right',
						));

		$options[] = array( "name" => __('Favicon',$themename),
					"desc" => __('Upload a 16px x 16px image that will represent your website\'s favicon.<br /><br /><em>To ensure cross-browser compatibility, we recommend converting the favicon into .ico format before uploading.</em>',$themename),
					"id" => $shortname."_favicon",
					"std" => "",
					"type" => "upload");			

		$options[] = array( "name" => __('Footer Text',$themename),
					"desc" => __('Override the default footer links by entering your own footer text here.',$themename),
					"id" => $shortname."_cus_footer",
					"std" => "",
					"type" => "textarea");

		$options = apply_filters( 'mpt_theme_options_after_general_settings' , $options , $themename );
					
	/**** Styling Options **************************************************************************************************/

		$options[] = array( "name" => __('Styling Options',$themename),
					"type" => "heading");
					
		$options[] = array( "name" => __('Select Color Skin',$themename),
					"desc" => __('This theme comes with 5 predefined color skins. Select the skin that is best for you!',$themename),
					"id" => $shortname."_theme_base_style",
					"std" => "Light Blue",
					"type" => "select",
					"options" => array(
						'lightblue' => 'Light Blue',
						'blue' => 'Blue',
						'green' => 'Green',
						'red' => 'Red',
						'yellow' => 'Yellow',
						'grey' => 'Grey',
						'purple' => 'Purple',
						));

		$options[] = array( "name" => __('Enable Google Web Font',$themename),
					"desc" => __('By default, this option is enabled. if you want to disable google web font then simply uncheck this option.',$themename),
					"id" => $shortname."_enable_google_web_font",
					"std" => "true",
					"type" => "checkbox");		
					
		$options[] = array( "name" => __('Header Font',$themename),
					"desc" => __('Select the font you want to use for header text',$themename),
					"id" => $shortname."_theme_header_font",
					"std" => "Arvo",
					"type" => "select",
					"options" => $mpt_google_web_fonts);	

		$options[] = array( "name" => __('Body Font',$themename),
					"desc" => __('Select the font you want to use for body text',$themename),
					"id" => $shortname."_theme_body_font",
					"std" => "PT Sans",
					"type" => "select",
					"options" => $mpt_google_web_fonts);	

		$options[] = array( "name" => __('Use Custom Google Web Font',$themename),
					"desc" => __('By default, this option is unchecked. If you want to use your own google web font then simply check this option.',$themename),
					"id" => $shortname."_theme_custom_web_font",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Custom Google Web Font For Header Text',$themename),
					"desc" => __('Enter your custom google web font here.',$themename),
					"id" => $shortname."_theme_custom_web_font_header",
					"std" => "Arvo:400,700",
					"type" => "text");	

		$options[] = array( "name" => __('Custom Google Web Font For Body Text',$themename),
					"desc" => __('Enter your custom google web font here.',$themename),
					"id" => $shortname."_theme_custom_web_font_body",
					"std" => "PT+Sans:400,700",
					"type" => "text");	

		$options[] = array( "name" => __('link Color',$themename),
					"desc" => __('Pick a custom color for links.',$themename),
					"id" => $shortname."_link_font_color",
					"std" => "",
					"type" => "color");
					
		$options[] = array( "name" => __('link hover Color',$themename),
					"desc" => __('Pick a custom color for links hover.',$themename),
					"id" => $shortname."_link_hover_font_color",
					"std" => "",
					"type" => "color");

		$options = apply_filters( 'mpt_theme_options_after_styling_options' , $options , $themename );

	/**** Header Section **************************************************************************************************/

		$options[] = array( "name" => __('Header Section',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Select Header Style',$themename),
					"desc" => __('Select the design you want to use in header',$themename),
					"id" => $shortname."_header_section_main_style",
					"std" => "Style 1",
					"type" => "select",
					"options" => array(
						'style-1' => 'Style 1',
						'style-2' => 'Style 2',
						'style-3' => 'Style 3'
						));

		$options[] = array( "name" => __('Background Color',$themename),
					"desc" => __('Pick a custom background color for header section.',$themename),
					"id" => $shortname."_header_section_bg_color",
					"std" => "",
					"type" => "color");	

		$options[] = array( "name" => __('Background Pattern',$themename),
					"desc" => __('Select what type of background pattern you want to display.',$themename),
					"id" => $shortname."_header_section_bg_pattern",
					"std" => "none",
					"type" => "images",
					"options" => $mpt_bg_patterns);	

		$options = apply_filters( 'mpt_theme_options_after_header_section' , $options , $themename );

	/**** Menu Settings **************************************************************************************************/

		$options[] = array( "name" => __('Menu Settings',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Enable Sticky Menu',$themename),
					"desc" => __('if you want to make the nav menu sticky then simply enable this option.',$themename),
					"id" => $shortname."_enable_sticky_header",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Background Color',$themename),
					"desc" => __('Pick a custom background color for Nav Menu.',$themename),
					"id" => $shortname."_menu_settings_bg_color",
					"std" => "",
					"type" => "color");	

		$options[] = array( "name" => __('Text Color',$themename),
					"desc" => __('Pick a custom text color for Nav Menu.',$themename),
					"id" => $shortname."_menu_settings_text_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Text Color (hover)',$themename),
					"desc" => __('Pick a custom text hover color for Nav Menu.',$themename),
					"id" => $shortname."_menu_settings_text_color_hover",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Background Color (hover)',$themename),
					"desc" => __('Pick a custom background color for nav menu.',$themename),
					"id" => $shortname."_menu_settings_nav_bgcolor_hover",
					"std" => "",
					"type" => "color");	

		$options[] = array( "name" => __('Dropdown Menu: Background Color',$themename),
					"desc" => __('Pick a custom background color for dropdown menu.',$themename),
					"id" => $shortname."_menu_settings_dropdown_bgcolor",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Dropdown Menu: Text Color',$themename),
					"desc" => __('Pick a custom text color for dropdown menu.',$themename),
					"id" => $shortname."_menu_settings_dropdown_textcolor",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Dropdown Menu: Background Color (Hover)',$themename),
					"desc" => __('Pick a custom background (hover) color for dropdown menu.',$themename),
					"id" => $shortname."_menu_settings_dropdown_bgcolor_hover",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Dropdown Menu: Text Color (Hover)',$themename),
					"desc" => __('Pick a custom text (hover) color for dropdown menu.',$themename),
					"id" => $shortname."_menu_settings_dropdown_textcolor_hover",
					"std" => "",
					"type" => "color");

		$options = apply_filters( 'mpt_theme_options_after_menu_settings' , $options , $themename );

	/**** Homepage Layout **************************************************************************************************/						
		
		$options[] = array( "name" => __('Homepage Layout',$themename),
					"type" => "heading");

		if ( class_exists('KickAssSlider') && class_exists('RevSliderAdmin') ) {

			$options[] = array( "name" => __('Select Slider',$themename),
						"desc" => __('Select the slider you want to use in homepage',$themename),
						"id" => $shortname."_homepage_selected_slider",
						"std" => "None",
						"type" => "select",
						"options" => array(
							'none' => 'None',
							'revslider' => 'Revolution Slider',
							'kaslider' => 'KickAss Slider'
							));

			$options[] = array( "name" => __('Revolution Slider Alias',$themename),
						"desc" => "Enter the alias for your homepage slider here. <em>Example: homepage</em>",
						"id" => $shortname."_rev_slider_alias",
						"std" => "",
						"type" => "text");

			$options[] = array( "name" => __('KickAss Slider ID',$themename),
						"desc" => 'Enter the alias for your homepage slider here. if your slider shortcode is [kickass-slider id="1"], then you should enter only 1 here.',
						"id" => $shortname."_kaslider_id",
						"std" => "",
						"type" => "text");		

		} elseif ( class_exists('KickAssSlider') && !class_exists('RevSliderAdmin') ) {

			$options[] = array( "name" => __('Show KickAss Slider',$themename),
						"desc" => __('By default, this option is disabled. If you want to enable KickAss Slider in homepage then simply check this option.',$themename),
						"id" => $shortname."_enable_kaslider",
						"std" => "false",
						"type" => "checkbox");				

			$options[] = array( "name" => __('KickAss Slider ID',$themename),
						"desc" => 'Enter the alias for your homepage slider here. if your slider shortcode is [kickass-slider id="7"], then you should enter only 7 here.',
						"id" => $shortname."_kaslider_id",
						"std" => "",
						"type" => "text");

		} elseif ( class_exists('RevSliderAdmin') && !class_exists('KickAssSlider') ) {

			$options[] = array( "name" => __('Show Revolution Slider',$themename),
						"desc" => __('By default, this option is disabled. If you want to enable Revolution Slider in homepage then simply check this option.',$themename),
						"id" => $shortname."_enable_rev_slider",
						"std" => "false",
						"type" => "checkbox");				

			$options[] = array( "name" => __('Revolution Slider Alias',$themename),
						"desc" => "Enter the alias for your homepage slider here. <em>Example: homepage</em>",
						"id" => $shortname."_rev_slider_alias",
						"std" => "",
						"type" => "text");
		}

		$options[] = array( "name" => __('Homepage Template ID',$themename),
					"desc" => __('Insert the template id of your homepage here. If your template shortcode is [template id="22"], then you should enter only 22 here.',$themename),
					"id" => $shortname."_homepage_layout_code",
					"std" => "",
					"type" => "text");

		$options[] = array( "name" => __('Show Footer Widget In Homepage',$themename),
					"desc" => __('By default, this option is enabled. If you want to disable footer widget in homepage then simply uncheck this option.',$themename),
					"id" => $shortname."_enable_homepage_footer_widget",
					"std" => "true",
					"type" => "checkbox");

		$options[] = array( "name" => __('Text Color',$themename),
					"desc" => __('Pick a custom text color for this section.',$themename),
					"id" => $shortname."_homepage_text_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Background Color',$themename),
					"desc" => __('Pick a custom background color for this section.',$themename),
					"id" => $shortname."_homepage_bg_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Background Pattern',$themename),
					"desc" => __('Select what type of background pattern you want to display.',$themename),
					"id" => $shortname."_homepage_bg_pattern",
					"std" => "none",
					"type" => "images",
					"options" => $mpt_bg_patterns);	

		$options = apply_filters( 'mpt_theme_options_after_homepage_layout' , $options , $themename );

	/**** MarketPress **************************************************************************************************/

	if ( class_exists( 'MarketPress' ) ) {

		$options[] = array( "name" => __('MarketPress Settings',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Store Page Template ID',$themename),
					"desc" => __('Insert the template id for the store page here. If your template shortcode is [template id="22"], then you should enter only 22 here.',$themename),
					"id" => $shortname."_storepage_layout_code",
					"std" => "",
					"type" => "text");

		$options[] = array( "name" => __('Product Listing Page Template ID',$themename),
					"desc" => __('Insert the template id for the product listing page here. If your template shortcode is [template id="22"], then you should enter only 22 here.',$themename),
					"id" => $shortname."_productlisting_layout_code",
					"std" => "",
					"type" => "text");

	if ( is_multisite() && is_main_site() ) {

		$options[] = array( "name" => __('Global Marketplace Page Template ID',$themename),
					"desc" => __('Insert the template id for the global marketplace page here. If your template shortcode is [template id="22"], then you should enter only 22 here.',$themename),
					"id" => $shortname."_global_marketplace_layout_code",
					"std" => "",
					"type" => "text");
	}

		$options[] = array( "name" => __('Product Listing Layout',$themename),
					"desc" => __('Select the layout you want to display in the product listing page. This setting will only valid if there is no inserted template ID for product listing page.',$themename),
					"id" => $shortname."_mp_listing_layout",
					"std" => "3 Columns",
					"type" => "select",
					"options" => array(
						'2col' => '2 Columns',
						'3col' => '3 Columns',
						'4col' => '4 Columns'
						));

		$options[] = array( "name" => __('Product Listing Entries (Per Page)',$themename),
					"desc" => __('Select the number of entries you want to display in each product listing page. This setting will only valid if there is no template ID inserted for product listing page.',$themename),
					"id" => $shortname."_mp_listing_entries",
					"std" => "9",
					"type" => "select",
					"options" => array(
						'2' => '2',
						'3' => '3',
						'4' => '4',
						'5' => '5',
						'6' => '6',
						'7' => '7',
						'8' => '8',
						'9' => '9',
						'10' => '10',
						'11' => '11',
						'12' => '12',
						'13' => '13',
						'14' => '14',
						'15' => '15',
						'16' => '16',
						'17' => '17',
						'18' => '18',
						'19' => '19',
						'20' => '20'
						));		

		$options[] = array( "name" => __('Button Color',$themename),
					"desc" => __('Select the color you want to use for all the buttons in MarketPress pages',$themename),
					"id" => $shortname."_mp_main_btn_color",
					"std" => "Black",
					"type" => "select",
					"options" => $btncolor_options);	

		$options[] = array( "name" => __('Price Label Color',$themename),
					"desc" => __('Select the color you want to use for all the price label in MarketPress pages',$themename),
					"id" => $shortname."_mp_main_label_color",
					"std" => "Black",
					"type" => "select",
					"options" => $labelcolor_options);	

		$options[] = array( "name" => __('Icon Tag Color',$themename),
					"desc" => __('Select the color you want to use for all the Icon Tag in MarketPress pages',$themename),
					"id" => $shortname."_mp_main_icon_tag_color",
					"std" => "Black",
					"type" => "select",
					"options" => $tagcolor_options);

		$options[] = array( "name" => __('Enable Advanced Sort',$themename),
					"desc" => __('if you want to enable the advanced soft feature in the product listing page then simply enable this option.',$themename),
					"id" => $shortname."_enable_advanced_sort",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Advanced Soft: Button Position',$themename),
					"desc" => __('Select the position of advanced soft button.',$themename),
					"id" => $shortname."_advanced_soft_btn_position",
					"std" => "Right",
					"type" => "select",
					"options" => array(
						'left' => 'Left',
						'center' => 'Center',
						'right' => 'Right',
						));

		$options[] = array( "name" => __('Upload a Default Product Image',$themename),
					"desc" => __('Upload a default product image for all the products without featured image. Ideal image size: 1000 x 750 pixel.',$themename),
					"id" => $shortname."_mp_default_product_img",
					"std" => "",
					"type" => "upload");

		if ( $mp->get_setting('disable_cart') ) {

			$options[] = array( "name" => __('Hide Product Price','fws'),
						"desc" => __('if you want to hide product price in all level then simply enable this option.','fws'),
						"id" => $shortname."_mp_hide_product_price",
						"std" => "false",
						"type" => "checkbox");

		}

		$options = apply_filters( 'mpt_theme_options_after_marketpress_settings' , $options , $themename );

	}

	/**** MP Dynamic Gird **************************************************************************************************/

	if ( class_exists( 'MPDG' ) && !class_exists( 'MPDG_Integration' ) ) {

		$options[] = array( "name" => __('MPDG Settings',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Enable MP Dynamic Grid In All Product Listing Pages',$themename),
					"desc" => __('if you want to enable the MP dynamic gird in all product listing pages then simply enable this option.',$themename),
					"id" => $shortname."_mpdg_enable_dg",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Product Listing Layout',$themename),
					"desc" => __('Select the layout you want to display in the product listing page',$themename),
					"id" => $shortname."_mpdg_listing_layout",
					"std" => "3 Columns",
					"type" => "select",
					"options" => array(
						'2col' => '2 Columns',
						'3col' => '3 Columns',
						'4col' => '4 Columns',
						'5col' => '5 Columns'
						));

		$options[] = array( "name" => __('Product Listing Entries (Per Page)',$themename),
					"desc" => __('Select the number of entries you want to display in each product listing page',$themename),
					"id" => $shortname."_mpdg_listing_entries",
					"std" => "9",
					"type" => "select",
					"options" => array(
						'2' => '2',
						'3' => '3',
						'4' => '4',
						'5' => '5',
						'6' => '6',
						'7' => '7',
						'8' => '8',
						'9' => '9',
						'10' => '10',
						'11' => '11',
						'12' => '12',
						'13' => '13',
						'14' => '14',
						'15' => '15',
						'16' => '16',
						'17' => '17',
						'18' => '18',
						'19' => '19',
						'20' => '20',
						'21' => '21',
						'22' => '22',
						'23' => '23',
						'24' => '24',
						'25' => '25',
						'26' => '26',
						'27' => '27',
						'28' => '28',
						'29' => '29',
						'30' => '30',
						'31' => '31',
						'32' => '32',
						'33' => '33',
						'34' => '34',
						'35' => '35',
						'36' => '36',
						'37' => '37',
						'38' => '38',
						'39' => '39',
						'40' => '40'
						));		

		$options[] = array( "name" => __('Button Color',$themename),
					"desc" => __('Select the color you want to use for the button',$themename),
					"id" => $shortname."_mpdg_btn_color",
					"std" => "Black",
					"type" => "select",
					"options" => $btncolor_options);	

		$options[] = array( "name" => __('Icon Tag Color',$themename),
					"desc" => __('Select the color you want to use for all the icon tag',$themename),
					"id" => $shortname."_mpdg_icon_tag_color",
					"std" => "Black",
					"type" => "select",
					"options" => $tagcolor_options);	

		$options[] = array( "name" => __('Price Tag Color',$themename),
					"desc" => __('Select the color you want to use for the price tag',$themename),
					"id" => $shortname."_mpdg_price_tag_color",
					"std" => "Black",
					"type" => "select",
					"options" => array(
						'grey' => 'Grey',
						'lightblue' => 'Light Blue',
						'green' => 'Green',
						'red' => 'Red',
						'yellow' => 'Yellow',
						'black' => 'Black',
					));	

		$options = apply_filters( 'mpt_theme_options_after_mpdg_settings' , $options , $themename );
	}

	/**** Floating Menu **************************************************************************************************/

	if ( class_exists( 'MarketPress' ) ) {

		$options[] = array( "name" => __('Floating Menu Settings',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Floating Menu Position',$themename),
					"desc" => __('Select the position of floating cart button',$themename),
					"id" => $shortname."_floating_cart_button_position",
					"std" => "Top Right",
					"type" => "select",
					"options" => array(
		                'top-left' => 'Top Left',
		                'top-right' => 'Top Right',
		                'middle-left' => 'Middle Left',
		                'middle-right' => 'Middle Right',
		                'bottom-left' => 'Bottom Left',
		                'bottom-right' => 'Bottom Right'
		            ));

		$options[] = array( "name" => __('Show Floating Cart',$themename),
					"desc" => __('By default, this option is enabled. If you want to hide floating cart then simply uncheck this option.',$themename),
					"id" => $shortname."_enable_floating_cart",
					"std" => "true",
					"type" => "checkbox");

		$options[] = array( "name" => __('Floating Cart: Show Total Items',$themename),
					"desc" => __('By default, this option is disabled. If you want to show cart total items then simply check this option.',$themename),
					"id" => $shortname."_floating_cart_show_cart_item",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Floating Cart: Show Total Amount',$themename),
					"desc" => __('By default, this option is disabled. If you want to show cart total amount then simply check this option.',$themename),
					"id" => $shortname."_floating_cart_show_cart_amount",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Floating Cart: Show Button Text',$themename),
					"desc" => __('By default, this option is enabled. If you want to hide button text then simply uncheck this option.',$themename),
					"id" => $shortname."_floating_cart_show_button_text",
					"std" => "true",
					"type" => "checkbox");	

		$options[] = array( "name" => __('Floating Cart: Button Text',$themename),
					"desc" => __('Enter the text for floating cart.',$themename),
					"id" => $shortname."_floating_cart_text",
					"std" => "View Cart",
					"type" => "text");	

		$options[] = array( "name" => __('Floating Cart: Button Color',$themename),
					"desc" => __('Select the color of floating cart button',$themename),
					"id" => $shortname."_floating_cart_button_color",
					"std" => "Grey",
					"type" => "select",
					"options" => array(
		                'grey' => 'Grey',
		                'blue' => 'Blue',
		                'lightblue' => 'Light Blue',
		                'green' => 'Green',
		                'red' => 'Red',
		                'yellow' => 'Yellow',
		                'black' => 'Black'
		            ));

		if ( is_multisite() && true == PRO_VISIBLE_IN_ALL_SUBSITES ) {

			$options[] = array( "name" => __('Show Global Store List',$themename),
				"desc" => __('By default, this option is disabled. If you want to show global store list then simply check this option.',$themename),
				"id" => $shortname."_enable_global_store",
				"std" => "false",
				"type" => "checkbox");

			$options[] = array( "name" => __('Global Store List: Show Button Text',$themename),
						"desc" => __('By default, this option is enabled. If you want to hide text in Global Store List button then simply uncheck this option.',$themename),
						"id" => $shortname."_global_store_show_button_text",
						"std" => "true",
						"type" => "checkbox");	

			$options[] = array( "name" => __('Global Store List: Button Text',$themename),
						"desc" => __('Enter the text in Global Store List button.',$themename),
						"id" => $shortname."_global_store_text",
						"std" => "Visit a Shop",
						"type" => "text");	

			$options[] = array( "name" => __('Global Store List: Button Color',$themename),
						"desc" => __('Select the color of Global Store List button',$themename),
						"id" => $shortname."_global_store_button_color",
						"std" => "Grey",
						"type" => "select",
						"options" => array(
			                'grey' => 'Grey',
			                'blue' => 'Blue',
			                'lightblue' => 'Light Blue',
			                'green' => 'Green',
			                'red' => 'Red',
			                'yellow' => 'Yellow',
			                'black' => 'Black'
			            ));

		}

		$options = apply_filters( 'mpt_theme_options_after_floating_menu_settings' , $options , $themename );
	}

	/**** Product Taxonomy Settings **************************************************************************************************/

	if ( class_exists( 'MarketPress' ) ) {

		$options[] = array( "name" => __('Product Taxonomy Settings',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Show Category Menu in Product Category Page',$themename),
					"desc" => __('By default, this option is disabled. If you want to show category menu in the product category page then simply check this option.',$themename),
					"id" => $shortname."_enable_taxonomy_menu_product_category",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Show Category Menu in Product Listing Page',$themename),
					"desc" => __('By default, this option is disabled. If you want to show category menu in the product listing page then simply check this option.',$themename),
					"id" => $shortname."_enable_taxonomy_menu_product_listing",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Category Menu & Sidebar: Position',$themename),
					"desc" => __('Select the position of the category menu and sidebar',$themename),
					"id" => $shortname."_taxonomy_menu_product_category_position",
					"std" => "Left",
					"type" => "select",
					"options" => array(
						'left' => 'Left',
						'right' => 'Right'
					));	

		$options[] = array( "name" => __('Category Menu: Show Product Count',$themename),
					"desc" => __('By default, this option is disabled. If you want to show product count in category menu then simply check this option.',$themename),
					"id" => $shortname."_taxonomy_menu_product_category_count",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Show Tag Menu in Product Tag Page',$themename),
					"desc" => __('By default, this option is disabled. If you want to show tag menu in the product tag page then simply check this option.',$themename),
					"id" => $shortname."_enable_taxonomy_menu_product_tag",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Tag Menu & Sidebar: Position',$themename),
					"desc" => __('Select the position of the tag menu and sidebar',$themename),
					"id" => $shortname."_taxonomy_menu_product_tag_position",
					"std" => "Left",
					"type" => "select",
					"options" => array(
						'left' => 'Left',
						'right' => 'Right'
					));	

		$options[] = array( "name" => __('Tag Menu: Show Product Count',$themename),
					"desc" => __('By default, this option is disabled. If you want to show product count in tag menu then simply check this option.',$themename),
					"id" => $shortname."_taxonomy_menu_product_tag_count",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Display sidebar in Product Category & Tag Page',$themename),
					"desc" => __('By default, this option is disabled. If you want to show sodebar in the product category & tag page then simply check this option.',$themename),
					"id" => $shortname."_enable_sidebar_taxonomy_page",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Select Sidebar',$themename),
					"desc" => __('Select the sidebar you want to use for product category & tag page',$themename),
					"id" => $shortname."_taxonomy_page_select_sidebar",
					"std" => "Sidebar 1",
					"type" => "select",
					"options" => array(
						'sidebar-1' => 'Sidebar 1',
						'sidebar-2' => 'Sidebar 2',
						'sidebar-3' => 'Sidebar 3',
						'sidebar-4' => 'Sidebar 4',
					));

		$options = apply_filters( 'mpt_theme_options_after_product_taxonomy_settings' , $options , $themename );
	}


	/**** Product Page Settings **************************************************************************************************/

	if ( class_exists( 'MarketPress' ) ) {

		$options[] = array( "name" => __('Product Page Settings',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Enable Image Zoom Feature',$themename),
					"desc" => __('By default, this option is enabled. If you want to disable the image zoom feature then simply uncheck this option.',$themename),
					"id" => $shortname."_pro_page_enable_image_zoom_feature",
					"std" => "true",
					"type" => "checkbox");

		$options[] = array( "name" => __('Display Sidebar',$themename),
					"desc" => __('By default, this option is enabled. If you want to hide sidebar in the product page then simply uncheck this option.',$themename),
					"id" => $shortname."_pro_page_display_sidebar",
					"std" => "true",
					"type" => "checkbox");

		$options[] = array( "name" => __('Select Sidebar',$themename),
					"desc" => __('Select the sidebar you want to use for product page',$themename),
					"id" => $shortname."_pro_page_select_sidebar",
					"std" => "Sidebar 1",
					"type" => "select",
					"options" => array(
						'sidebar-1' => 'Sidebar 1',
						'sidebar-2' => 'Sidebar 2',
						'sidebar-3' => 'Sidebar 3',
						'sidebar-4' => 'Sidebar 4',
					));

		$options[] = array( "name" => __('Sidebar Position',$themename),
					"desc" => __('Select the position of the sidebar widget',$themename),
					"id" => $shortname."_pro_page_sidebar_position",
					"std" => "Left",
					"type" => "select",
					"options" => array(
						'left' => 'Left',
						'right' => 'Right'
					));

		if ( is_multisite() && true == PRO_VISIBLE_IN_ALL_SUBSITES ) {
			
			$options[] = array( "name" => __('Enable Global Tags',$themename),
						"desc" => __('By default, this option is disabled. If you want to turn all the product tags into global tags then simply check this option.',$themename),
						"id" => $shortname."_pro_page_enable_global_tags",
						"std" => "false",
						"type" => "checkbox");
		}

		$options[] = array( "name" => __('Show Product Short Description',$themename),
					"desc" => __('By default, this option is enabled. If you want to hide the product short description on product page then simply uncheck this option.',$themename),
					"id" => $shortname."_pro_page_show_product_short_desc",
					"std" => "true",
					"type" => "checkbox");

		$options[] = array( "name" => __('Enable Related Products Feature',$themename),
					"desc" => __('By default, this option is enabled. If you want to disable the related products feature then simply uncheck this option.',$themename),
					"id" => $shortname."_pro_page_enable_related_products_feature",
					"std" => "true",
					"type" => "checkbox");

		$options[] = array( "name" => __('Related Products: Query Type',$themename),
					"desc" => __('Select the type of query you want to use for related products.',$themename),
					"id" => $shortname."_pro_page_related_products_query_type",
					"std" => "By Categories",
					"type" => "select",
					"options" => array(
						'categories' => 'By Categories',
						'tags' => 'By Tags',
						'id' => 'By IDs'
					));

		$options[] = array( "name" => __('Related Products: Interval',$themename),
					"desc" => __('The amount of time to delay between each slides.',$themename),
					"id" => $shortname."_pro_page_related_products_interval",
					"std" => "10000",
					"type" => "text");	

		if ( is_multisite() && true == PRO_VISIBLE_IN_ALL_SUBSITES ) {

			$options[] = array( "name" => __('Related Products: Use Global Products',$themename),
						"desc" => __('By default, this option is disabled. If you want to use global products in the related products section then simply check this option. Please note that Query Type - "By IDs" won\'t work in global listing.',$themename),
						"id" => $shortname."_pro_page_enable_global_related_products",
						"std" => "false",
						"type" => "checkbox");

		}

		if ( class_exists('CustomPress_Core') ) {
			$options[] = array( "name" => __('Show Custom Fields in Product Page',$themename),
						"desc" => __('By default, this option is disabled. If you want to display custom fields (from CustomPress) on product page then simply check this option.',$themename),
						"id" => $shortname."_pro_page_show_custom_field",
						"std" => "false",
						"type" => "checkbox");

			$options[] = array( "name" => __('Display Custom Fields as',$themename),
						"desc" => __('Select the structure of the custom fields block.',$themename),
						"id" => $shortname."_pro_page_custom_field_structure",
						"std" => "table",
						"type" => "select",
						"options" => array(
							'table' => 'table',
							'ul' => 'ul',
							'div' => 'div'
						));
		}

		$options = apply_filters( 'mpt_theme_options_after_product_page_settings' , $options , $themename );

	}

	/**** Page Settings **************************************************************************************************/

		$options[] = array( "name" => __('Page Settings',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Content Section: Background Color',$themename),
					"desc" => __('Pick a custom background color for content section.',$themename),
					"id" => $shortname."_page_content_section_bg_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Content Section: Text Color',$themename),
					"desc" => __('Pick a custom text color for content section.',$themename),
					"id" => $shortname."_page_content_section_text_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Content Section: Background Pattern',$themename),
					"desc" => __('Select what type of background pattern you want to display.',$themename),
					"id" => $shortname."_page_content_section_bg_pattern",
					"std" => "none",
					"type" => "images",
					"options" => $mpt_bg_patterns);

		$options[] = array( "name" => __('Select Sidebar',$themename),
					"desc" => __('Select the sidebar you want to use for pages',$themename),
					"id" => $shortname."_wp_page_select_sidebar",
					"std" => "Sidebar 1",
					"type" => "select",
					"options" => array(
						'sidebar-1' => 'Sidebar 1',
						'sidebar-2' => 'Sidebar 2',
						'sidebar-3' => 'Sidebar 3',
						'sidebar-4' => 'Sidebar 4',
					));

		$options[] = array( "name" => __('Sidebar Position',$themename),
					"desc" => __('Select the position of the sidebar widget',$themename),
					"id" => $shortname."_wp_page_sidebar_position",
					"std" => "Left",
					"type" => "select",
					"options" => array(
						'left' => 'Left',
						'right' => 'Right'
					));

		$options[] = array( "name" => __('Display Sidebar In Archive Page & Search Page',$themename),
					"desc" => __('By default, this option is enabled. If you want to hide sidebar in Archive & Search page then simply uncheck this option.',$themename),
					"id" => $shortname."_archive_search_page_display_sidebar",
					"std" => "true",
					"type" => "checkbox");

		$options = apply_filters( 'mpt_theme_options_after_page_settings' , $options , $themename );

	/**** Post Settings **************************************************************************************************/

		$options[] = array( "name" => __('Post Settings',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Content Section: Background Color',$themename),
					"desc" => __('Pick a custom background color for content section.',$themename),
					"id" => $shortname."_post_content_section_bg_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Content Section: Text Color',$themename),
					"desc" => __('Pick a custom text color for content section.',$themename),
					"id" => $shortname."_post_content_section_text_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Content Section: Background Pattern',$themename),
					"desc" => __('Select what type of background pattern you want to display.',$themename),
					"id" => $shortname."_post_content_section_bg_pattern",
					"std" => "none",
					"type" => "images",
					"options" => $mpt_bg_patterns);	

		$options[] = array( "name" => __('Display Sidebar',$themename),
					"desc" => __('By default, this option is enabled. If you want to hide sidebar in posts then simply uncheck this option.',$themename),
					"id" => $shortname."_wp_post_display_sidebar",
					"std" => "true",
					"type" => "checkbox");

		$options[] = array( "name" => __('Select Sidebar',$themename),
					"desc" => __('Select the sidebar you want to use for posts',$themename),
					"id" => $shortname."_wp_post_select_sidebar",
					"std" => "Sidebar 1",
					"type" => "select",
					"options" => array(
						'sidebar-1' => 'Sidebar 1',
						'sidebar-2' => 'Sidebar 2',
						'sidebar-3' => 'Sidebar 3',
						'sidebar-4' => 'Sidebar 4',
					));

		$options[] = array( "name" => __('Sidebar Position',$themename),
					"desc" => __('Select the position of the sidebar widget',$themename),
					"id" => $shortname."_wp_post_sidebar_position",
					"std" => "Left",
					"type" => "select",
					"options" => array(
						'left' => 'Left',
						'right' => 'Right'
					));

		$options = apply_filters( 'mpt_theme_options_after_post_settings' , $options , $themename );

	/**** Blog Settings **************************************************************************************************/

		$options[] = array( "name" => __('Blog Settings',$themename),
					"type" => "heading");	

		$options[] = array( "name" => '',
					"type" => "info",
					'std' => __( 'Use the settings below to adjust the look and feel of the archive & search page' , $themename )
					);	

		$options[] = array( "name" => __('Layout',$themename),
					"desc" => __('Select the layout for archive and search page',$themename),
					"id" => $shortname."_blog_pages_layout",
					"std" => "1 Columns",
					"type" => "select",
					"options" => array(
						'1col' => '1 Columns',
						'2col' => '2 Columns',
						'3col' => '3 Columns',
						'4col' => '4 Columns',
					));

		$options[] = array( "name" => __('Post Box Style',$themename),
					"desc" => __('Select the style you want to use for the post box',$themename),
					"id" => $shortname."_blog_pages_box_style",
					"std" => "Style 1",
					"type" => "select",
					"options" => array(
						'style-1' => 'Style 1',
						'style-2' => 'Style 2',
					));

		$options[] = array( "name" => __('Show Excerpt',$themename),
					"desc" => __('Whether to show excerpt',$themename),
					"id" => $shortname."_blog_pages_show_excerpt",
					"std" => "Yes",
					"type" => "select",
					"options" => array(
						'yes' => 'Yes',
						'no' => 'No'
					));

		$options[] = array( "name" => __('Show Meta',$themename),
					"desc" => __('Whether to show meta info',$themename),
					"id" => $shortname."_blog_pages_show_meta",
					"std" => "Yes",
					"type" => "select",
					"options" => array(
						'yes' => 'Yes',
						'no' => 'No'
					));

		$options[] = array( "name" => __('Show Author Name',$themename),
					"desc" => __('Whether to show author name',$themename),
					"id" => $shortname."_blog_pages_show_author_name",
					"std" => "Yes",
					"type" => "select",
					"options" => array(
						'yes' => 'Yes',
						'no' => 'No'
					));

		$options[] = array( "name" => __('Show Comment Number',$themename),
					"desc" => __('Whether to show comment number',$themename),
					"id" => $shortname."_blog_pages_show_comments",
					"std" => "Yes",
					"type" => "select",
					"options" => array(
						'yes' => 'Yes',
						'no' => 'No'
					));

		$options[] = array( "name" => __('Show Date Tag',$themename),
					"desc" => __('Whether to show Show Date Tag',$themename),
					"id" => $shortname."_blog_pages_show_date_tag",
					"std" => "Yes",
					"type" => "select",
					"options" => array(
						'yes' => 'Yes',
						'no' => 'No'
					));

		$options[] = array( "name" => __('Date Tag Color',$themename),
					"desc" => __('Select the date tag color',$themename),
					"id" => $shortname."_blog_pages_date_tag_color",
					"std" => "Black",
					"type" => "select",
					"options" => array(
						'black' => 'Black',
						'grey' => 'Grey',
						'blue' => 'Blue',
						'yellow' => 'Yellow',
						'red' => 'Red',
						'lightblue' => 'Light Blue',
						'green' => 'Green',
					));

		$options[] = array( "name" => __('Button Color',$themename),
					"desc" => __('Select the button color ( only applicable to box style 1 ).',$themename),
					"id" => $shortname."_blog_pages_button_color",
					"std" => "Black",
					"type" => "select",
					"options" => array(
						'black' => 'Black',
						'grey' => 'Grey',
						'blue' => 'Blue',
						'yellow' => 'Yellow',
						'red' => 'Red',
						'lightblue' => 'Light Blue',
						'green' => 'Green',
					));

		$options[] = array( "name" => __('Category Tag Color',$themename),
					"desc" => __('Select the category tag color ( only applicable to box style 2 ).',$themename),
					"id" => $shortname."_blog_pages_meta_tag_color",
					"std" => "Black",
					"type" => "select",
					"options" => array(
						'black' => 'Black',
						'grey' => 'Grey',
						'blue' => 'Blue',
						'yellow' => 'Yellow',
						'red' => 'Red',
						'lightblue' => 'Light Blue',
						'green' => 'Green',
					));

		$options = apply_filters( 'mpt_theme_options_after_blog_settings' , $options , $themename );

	/**** Sidebar Widget **************************************************************************************************/

		$options[] = array( "name" => __('Sidebar Widget Settings',$themename),
					"type" => "heading");	


		$options[] = array( "name" => __('Background Color',$themename),
					"desc" => __('Pick a custom background color for sidebar widget.',$themename),
					"id" => $shortname."_sidebar_widget_bg_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Text Color',$themename),
					"desc" => __('Pick a custom text color for sidebar widget.',$themename),
					"id" => $shortname."_sidebar_widget_text_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Link Color',$themename),
					"desc" => __('Pick a custom link color for sidebar widget.',$themename),
					"id" => $shortname."_sidebar_widget_link_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Icon Color',$themename),
					"desc" => __('Select the color of arrow icon in the sidebar widget .',$themename),
					"id" => $shortname."_sidebar_widget_icon_color",
					"std" => "Black",
					"type" => "select",
					"options" => array(
						'black' => 'Black',
						'white' => 'White',
						'blue' => 'Blue',
						'yellow' => 'Yellow',
						'red' => 'Red',
						'lightblue' => 'Light Blue',
						'green' => 'Green',
						));

		$options[] = array( "name" => __('Background Color (hover)',$themename),
					"desc" => __('Pick a custom background color for sidebar widget (on hover).',$themename),
					"id" => $shortname."_sidebar_widget_bg_color_hover",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Text Color (hover)',$themename),
					"desc" => __('Pick a custom text color for sidebar widget (on hover).',$themename),
					"id" => $shortname."_sidebar_widget_text_color_hover",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Link Color (Hover)',$themename),
					"desc" => __('Pick a custom link color for sidebar widget (on hover).',$themename),
					"id" => $shortname."_sidebar_widget_link_color_hover",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Icon Color (hover)',$themename),
					"desc" => __('Select the color of arrow icon in the sidebar widget (on hover).',$themename),
					"id" => $shortname."_sidebar_widget_icon_color_hover",
					"std" => "Black",
					"type" => "select",
					"options" => array(
						'black' => 'Black',
						'white' => 'White',
						'blue' => 'Blue',
						'yellow' => 'Yellow',
						'red' => 'Red',
						'lightblue' => 'Light Blue',
						'green' => 'Green',
						));

		$options = apply_filters( 'mpt_theme_options_after_sidebar_widget' , $options , $themename );


	/**** Footer Widget **************************************************************************************************/

		$options[] = array( "name" => __('Footer Widget Settings',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Show Footer Widget',$themename),
					"desc" => __('By default, this option is enabled. If you want to disable the footer widget then simply uncheck this option.',$themename),
					"id" => $shortname."_enable_footer_widget",
					"std" => "true",
					"type" => "checkbox");

		$options[] = array( "name" => __('Footer Widget Layout',$themename),
					"desc" => __('Select the layout you want to display in footer widget.',$themename),
					"id" => $shortname."_footer_widget_setting",
					"std" => "widget444",
					"type" => "images",
					"options" => array(
						'widget444' => $adminimagefolder . 'sample-layouts/footer-widget-layout-444.png',
						'widget633' => $adminimagefolder . 'sample-layouts/footer-widget-layout-633.png',
						'widget336' => $adminimagefolder . 'sample-layouts/footer-widget-layout-336.png',
						'widget3333' => $adminimagefolder . 'sample-layouts/footer-widget-layout-3333.png',
						));	

		$options[] = array( "name" => __('Background Color',$themename),
					"desc" => __('Pick a custom background color for footer widget.',$themename),
					"id" => $shortname."_footer_widget_bg_color",
					"std" => "",
					"type" => "color");	

		$options[] = array( "name" => __('Text Color',$themename),
					"desc" => __('Pick a custom text color for footer widget.',$themename),
					"id" => $shortname."_footer_widget_text_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Background Pattern',$themename),
					"desc" => __('Select what type of background pattern you want to display.',$themename),
					"id" => $shortname."_footer_widget_bg_pattern",
					"std" => "none",
					"type" => "images",
					"options" => $mpt_bg_patterns);	

		$options[] = array( "name" => __('Link Color',$themename),
					"desc" => __('Pick a custom link color for footer widget.',$themename),
					"id" => $shortname."_footer_widget_link_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Link Color (hover)',$themename),
					"desc" => __('Pick a custom link hover color for footer widget.',$themename),
					"id" => $shortname."_footer_widget_link_color_hover",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Icon Color',$themename),
					"desc" => __('Select the color of arrow icon in the footer widget .',$themename),
					"id" => $shortname."_footer_widget_icon_color",
					"std" => "White",
					"type" => "select",
					"options" => array(
						'black' => 'Black',
						'white' => 'White',
						'blue' => 'Blue',
						'yellow' => 'Yellow',
						'red' => 'Red',
						'lightblue' => 'Light Blue',
						'green' => 'Green',
						));

		$options = apply_filters( 'mpt_theme_options_after_footer_widget_settings' , $options , $themename );

	/**** Footer Section **************************************************************************************************/

		$options[] = array( "name" => __('Footer Section',$themename),
					"type" => "heading");	


		$options[] = array( "name" => __('Background Color',$themename),
					"desc" => __('Pick a custom background color for footer section.',$themename),
					"id" => $shortname."_footer_section_bg_color",
					"std" => "",
					"type" => "color");	

		$options[] = array( "name" => __('Text Color',$themename),
					"desc" => __('Pick a custom text color for footer section.',$themename),
					"id" => $shortname."_footer_section_text_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Background Pattern',$themename),
					"desc" => __('Select what type of background pattern you want to display.',$themename),
					"id" => $shortname."_footer_section_bg_pattern",
					"std" => "none",
					"type" => "images",
					"options" => $mpt_bg_patterns);	

		$options[] = array( "name" => __('Link Color',$themename),
					"desc" => __('Pick a custom link color for footer section.',$themename),
					"id" => $shortname."_footer_section_link_color",
					"std" => "",
					"type" => "color");

		$options[] = array( "name" => __('Link Color (hover)',$themename),
					"desc" => __('Pick a custom link hover color for footer section.',$themename),
					"id" => $shortname."_footer_section_link_color_hover",
					"std" => "",
					"type" => "color");

		$options = apply_filters( 'mpt_theme_options_after_footer_section' , $options , $themename );

	/**** Social Icon **************************************************************************************************/

		$options[] = array( "name" => __('Social Icon',$themename),
					"type" => "heading");	

		$options[] = array( "name" => __('Social Icon 1: Icon',$themename),
					"desc" => __('Select what type of icon you want to use for social icon 1.',$themename),
					"id" => $shortname."_social_icon_1_icon",
					"std" => "Facebook",
					"type" => "select",
					"options" => $socialicon_options);							

		$options[] = array( "name" => __('Social Icon 1: Profile Url',$themename),
					"desc" => __('Enter your profile / website url here for social icon 1.',$themename),
					"id" => $shortname."_social_icon_1_url",
					"std" => "",
					"type" => "text");

		$options[] = array( "name" => __('Social Icon 2: Icon',$themename),
					"desc" => __('Select what type of icon you want to use for social icon 2.',$themename),
					"id" => $shortname."_social_icon_2_icon",
					"std" => "Twitter",
					"type" => "select",
					"options" => $socialicon_options);							

		$options[] = array( "name" => __('Social Icon 2: Profile Url',$themename),
					"desc" => __('Enter your profile / website url here for social icon 2.',$themename),
					"id" => $shortname."_social_icon_2_url",
					"std" => "",
					"type" => "text");

		$options[] = array( "name" => __('Social Icon 3: Icon',$themename),
					"desc" => __('Select what type of icon you want to use for social icon 3.',$themename),
					"id" => $shortname."_social_icon_3_icon",
					"std" => "Google Plus",
					"type" => "select",
					"options" => $socialicon_options);							

		$options[] = array( "name" => __('Social Icon 3: Profile Url',$themename),
					"desc" => __('Enter your profile / website url here for social icon 3.',$themename),
					"id" => $shortname."_social_icon_3_url",
					"std" => "",
					"type" => "text");

		$options[] = array( "name" => __('Social Icon 4: Icon',$themename),
					"desc" => __('Select what type of icon you want to use for social icon 4.',$themename),
					"id" => $shortname."_social_icon_4_icon",
					"std" => "Dribbble",
					"type" => "select",
					"options" => $socialicon_options);							

		$options[] = array( "name" => __('Social Icon 4: Profile Url',$themename),
					"desc" => __('Enter your profile / website url here for social icon 4.',$themename),
					"id" => $shortname."_social_icon_4_url",
					"std" => "",
					"type" => "text");

		$options[] = array( "name" => __('Social Icon 5: Icon',$themename),
					"desc" => __('Select what type of icon you want to use for social icon 5.',$themename),
					"id" => $shortname."_social_icon_5_icon",
					"std" => "YouTube",
					"type" => "select",
					"options" => $socialicon_options);							

		$options[] = array( "name" => __('Social Icon 5: Profile Url',$themename),
					"desc" => __('Enter your profile / website url here for social icon 5.',$themename),
					"id" => $shortname."_social_icon_5_url",
					"std" => "",
					"type" => "text");


		$options[] = array( "name" => __('Social Icon 6: Icon',$themename),
					"desc" => __('Select what type of icon you want to use for social icon 6.',$themename),
					"id" => $shortname."_social_icon_6_icon",
					"std" => "RSS",
					"type" => "select",
					"options" => $socialicon_options);							

		$options[] = array( "name" => __('Social Icon 6: Profile Url',$themename),
					"desc" => __('Enter your profile / website url here for social icon 6.',$themename),
					"id" => $shortname."_social_icon_6_url",
					"std" => "",
					"type" => "text");

		$options[] = array( "name" => __('Social Icon 7: Icon',$themename),
					"desc" => __('Select what type of icon you want to use for social icon 7.',$themename),
					"id" => $shortname."_social_icon_7_icon",
					"std" => "None",
					"type" => "select",
					"options" => $socialicon_options);							

		$options[] = array( "name" => __('Social Icon 7: Profile Url',$themename),
					"desc" => __('Enter your profile / website url here for social icon 7.',$themename),
					"id" => $shortname."_social_icon_7_url",
					"std" => "",
					"type" => "text");

		$options[] = array( "name" => __('Social Icon 8: Icon',$themename),
					"desc" => __('Select what type of icon you want to use for social icon 8.',$themename),
					"id" => $shortname."_social_icon_8_icon",
					"std" => "None",
					"type" => "select",
					"options" => $socialicon_options);							

		$options[] = array( "name" => __('Social Icon 8: Profile Url',$themename),
					"desc" => __('Enter your profile / website url here for social icon 8.',$themename),
					"id" => $shortname."_social_icon_8_url",
					"std" => "",
					"type" => "text");

		$options = apply_filters( 'mpt_theme_options_after_social_icon' , $options , $themename );
					
	/**** SEO Settings **************************************************************************************************/

		$options[] = array( "name" => __('SEO Settings',$themename),
					"type" => "heading");
		
		$options[] = array( "name" => __('Enable Custom Title',$themename),
					"desc" => __('if you want to create a custom title then simply enable this option and fill in the custom title field below.',$themename),
					"id" => $shortname."_enable_custom_title",
					"std" => "false",
					"type" => "checkbox");
					
		$options[] = array( "name" => __('Enable Meta Description',$themename),
					"desc" => __('If you would like to use a different description then enable this option and fill in the custom description field below.',$themename),
					"id" => $shortname."_enable_meta_desc",
					"std" => "false",
					"type" => "checkbox");
					
		$options[] = array( "name" => __('Enable Meta Keywords',$themename),
					"desc" => __('If you want to add meta keywords to your header then enable this option and fill in the custom keywords field below.',$themename),
					"id" => $shortname."_enable_meta_keywords",
					"std" => "false",
					"type" => "checkbox");
					
		$options[] = array( "type" => "divider");
					
		$options[] = array( "name" => __('Custom Title (If enable)',$themename),
					"desc" => __('If you have enabled custom titles you can add your custom title here.',$themename),
					"id" => $shortname."_cus_title",
					"std" => "",
					"type" => "text");
					
		$options[] = array( "name" => __('Meta Description (If enable)',$themename),
					"desc" => __('If you have enabled meta descriptions you can add your custom description here.',$themename),
					"id" => $shortname."_cus_meta_desc",
					"std" => "",
					"type" => "textarea");					

		$options[] = array( "name" => __('Meta Keywords (If enable)',$themename),
					"desc" => __('If you have enabled meta keywords you can add your custom keywords here. Keywords should be separated by comas. For example: keyword1,keyword2,keyword3',$themename),
					"id" => $shortname."_cus_meta_keywords",
					"std" => "",
					"type" => "text");	

		$options = apply_filters( 'mpt_theme_options_after_seo_settings' , $options , $themename );				

	/**** Code Integration **************************************************************************************************/

		$options[] = array( "name" => __('Code Integration',$themename),
					"type" => "heading");
					
		$options[] = array( "name" => __('Enable Header Code',$themename),
					"desc" => __('Uncheck this option will disable the header code below from your site.',$themename),
					"id" => $shortname."_enable_header_code",
					"std" => "false",
					"type" => "checkbox");

		$options[] = array( "name" => __('Enable Body Code',$themename),
					"desc" => __('Uncheck this option will disable the body code below from your site.',$themename),
					"id" => $shortname."_enable_body_code",
					"std" => "false",
					"type" => "checkbox");
					
		$options[] = array( "name" => __('Enable Top Code (Single Post)',$themename),
					"desc" => __('Uncheck this option will disable the top code (single post) below from your site.',$themename),
					"id" => $shortname."_enable_top_code",
					"std" => "false",
					"type" => "checkbox");
					
		$options[] = array( "name" => __('Enable Bottom Code (Single Post)',$themename),
					"desc" => __('Uncheck this option will disable the bottom code (single post) below from your site.',$themename),
					"id" => $shortname."_enable_bottom_code",
					"std" => "false",
					"type" => "checkbox");
					
		$options[] = array( "name" => __('Header Code',$themename),
					"desc" => __('Add code to the &lt;head&gt; section of your website. If you want to add javascript or css to all pages, you can place the code here.',$themename),
					"id" => $shortname."_header_code",
					"std" => "",
					"type" => "textarea-large");

		$options[] = array( "name" => __('Body Code',$themename),
					"desc" => __('Add code to the &lt;body&gt; section of your website. If you want to add tracking code to your website (such as Google Analytics), you can place the code here.',$themename),
					"id" => $shortname."_body_code",
					"std" => "",
					"type" => "textarea-large");
					
		$options[] = array( "name" => __('Top Code (single post)',$themename),
					"desc" => __('Add code to the top section of your posts. If you want to add social bookmarking links to the top of your posts, you can place the code here.',$themename),
					"id" => $shortname."_top_code",
					"std" => "",
					"type" => "textarea");
					
		$options[] = array( "name" => __('Bottom Code (single post)',$themename),
					"desc" => __('Add code to the bottom section of your posts. If you want to add social bookmarking links to the bottom of your posts (before the comments), you can place the code here.',$themename),
					"id" => $shortname."_bottom_code",
					"std" => "",
					"type" => "textarea");

		$options = apply_filters( 'mpt_theme_options_after_code_integration' , $options , $themename );
					

		update_option('m413_template',$options);
		update_option('m413_themename',$themename);
		update_option('m413_shortname',$shortname);

	}
}