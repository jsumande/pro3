<?php
	// custom styling options

	$themefolder = get_template_directory_uri();

	$header_font = get_blog_option(1,'mpt_theme_header_font');
	$body_font = get_blog_option(1,'mpt_theme_body_font');
	$customfont = get_blog_option(1,'mpt_theme_custom_web_font');
	$customheaderfont = esc_attr(get_blog_option(1,'mpt_theme_custom_web_font_header'));
	$custombodyfont = esc_attr(get_blog_option(1,'mpt_theme_custom_web_font_body'));

	$bodybgcolor = esc_attr(get_blog_option(1,'mpt_body_bg_color'));
	$bodytextcolor = esc_attr(get_blog_option(1,'mpt_body_text_color'));

	$linkcolor = esc_attr(get_blog_option(1,'mpt_link_font_color'));
	$linkhovercolor = esc_attr(get_blog_option(1,'mpt_link_hover_font_color'));

	$headersectionbg = esc_attr(get_blog_option(1,'mpt_header_section_bg_color'));
	$headersectionbgpattern = get_blog_option(1,'mpt_header_section_bg_pattern');
	$headerstyle = esc_attr(get_blog_option(1,'mpt_header_section_main_style'));

	$menusettingsbg = esc_attr(get_blog_option(1,'mpt_menu_settings_bg_color'));
	$menusettingstext = esc_attr(get_blog_option(1,'mpt_menu_settings_text_color'));
	$menusettingstexthover = esc_attr(get_blog_option(1,'mpt_menu_settings_text_color_hover'));
	$menusettingsnavbgcolorhover = esc_attr(get_blog_option(1,'mpt_menu_settings_nav_bgcolor_hover'));
	$menusettingsdropdownbgcolor = esc_attr(get_blog_option(1,'mpt_menu_settings_dropdown_bgcolor'));
	$menusettingsdropdowntextcolor = esc_attr(get_blog_option(1,'mpt_menu_settings_dropdown_textcolor'));
	$menusettingsdropdownbgcolorhover = esc_attr(get_blog_option(1,'mpt_menu_settings_dropdown_bgcolor_hover'));
	$menusettingsdropdowntextcolorhover = esc_attr(get_blog_option(1,'mpt_menu_settings_dropdown_textcolor_hover'));

	$homepagebgpattern = get_blog_option(1,'mpt_homepage_bg_pattern');
	$homepagebgcolor = esc_attr(get_blog_option(1,'mpt_homepage_bg_color'));
	$homepagetextcolor = esc_attr(get_blog_option(1,'mpt_homepage_text_color'));

	$floatincartposition = get_blog_option(1,'mpt_floating_cart_button_position');

	$pagecontentbg = esc_attr(get_blog_option(1,'mpt_page_content_section_bg_color'));
	$pagecontenttext = esc_attr(get_blog_option(1,'mpt_page_content_section_text_color'));
	$pagecontentbgpattern = get_blog_option(1,'mpt_page_content_section_bg_pattern');

	$postcontentbg = esc_attr(get_blog_option(1,'mpt_post_content_section_bg_color'));
	$postcontenttext = esc_attr(get_blog_option(1,'mpt_post_content_section_text_color'));	
	$postcontentbgpattern = esc_attr(get_blog_option(1,'mpt_post_content_section_bg_pattern'));

	$sidebarwidgetbg = esc_attr(get_blog_option(1,'mpt_sidebar_widget_bg_color'));
	$sidebarwidgettext = esc_attr(get_blog_option(1,'mpt_sidebar_widget_text_color'));
	$sidebarwidgetlink = esc_attr(get_blog_option(1,'mpt_sidebar_widget_link_color'));
	$sidebarwidgeticoncolor = esc_attr(get_blog_option(1,'mpt_sidebar_widget_icon_color'));
	$sidebarwidgetbghover = esc_attr(get_blog_option(1,'mpt_sidebar_widget_bg_color_hover'));
	$sidebarwidgettexthover = esc_attr(get_blog_option(1,'mpt_sidebar_widget_text_color_hover'));
	$sidebarwidgetlinkhover = esc_attr(get_blog_option(1,'mpt_sidebar_widget_link_color_hover'));
	$sidebarwidgeticoncolorhover = esc_attr(get_blog_option(1,'mpt_sidebar_widget_icon_color_hover'));

	$footerwidgetbg = esc_attr(get_blog_option(1,'mpt_footer_widget_bg_color'));
	$footerwidgettext = esc_attr(get_blog_option(1,'mpt_footer_widget_text_color'));
	$footerwidgetbgpattern = esc_attr(get_blog_option(1,'mpt_footer_widget_bg_pattern'));
	$footerwidgetlink = esc_attr(get_blog_option(1,'mpt_footer_widget_link_color'));
	$footerwidgetlinkhover = esc_attr(get_blog_option(1,'mpt_footer_widget_link_color_hover'));
	$footerwidgeticoncolor = esc_attr(get_blog_option(1,'mpt_footer_widget_icon_color'));

	$footersectionbg = esc_attr(get_blog_option(1,'mpt_footer_section_bg_color'));
	$footersectiontext = esc_attr(get_blog_option(1,'mpt_footer_section_text_color'));
	$footersectionbgpattern = esc_attr(get_blog_option(1,'mpt_footer_section_bg_pattern'));
	$footersectionlink = esc_attr(get_blog_option(1,'mpt_footer_section_link_color'));
	$footersectionlinkhover = esc_attr(get_blog_option(1,'mpt_footer_section_link_color_hover'));

	switch ($headerstyle) {
		case 'Style 2':
			$headerstyle = '.header-style-2';
			break;
		
		case 'Style 3':
			$headerstyle = '.header-style-3';
			break;

		case 'Style 1':
		default:
			$headerstyle = '';
			break;
	}
	
?>

<style type="text/css" media="all">
<?php 
	// Text Font

	if ($header_font != "" && $customfont != 'true') { 
		$header_font = str_replace('+', ' ', $header_font);

		echo "h1,h2,h3,h4,h5,h6 {font-family: '$header_font', Arial, sans-serif;}";
	}
	if ($body_font != "" && $customfont != 'true') { 
		$body_font = str_replace('+', ' ', $body_font);

		echo "body {font-family: '$body_font', Arial, sans-serif;}";
	} 


	if ($customfont == 'true' && !empty($customheaderfont)) {

		if (strpos($customheaderfont, '&')) {
			$customheaderfont = str_replace(strstr($customheaderfont, '&'),'',$customheaderfont);
		}

		if (strpos($customheaderfont, ':')) {
			$customheaderfont = str_replace(strstr($customheaderfont,':'),'',$customheaderfont);
		}
		
		$customheaderfont = str_replace('+', ' ', $customheaderfont);

		echo "h1,h2,h3,h4,h5,h6 {font-family: '$customheaderfont', Arial, sans-serif;}";

	}

	if ($customfont == 'true' && !empty($custombodyfont)) {

		if (strpos($custombodyfont, '&')) {
			$custombodyfont = str_replace(strstr($custombodyfont, '&'),'', $custombodyfont);
		}

		if (strpos($custombodyfont, ':')) {
			$custombodyfont = str_replace(strstr($custombodyfont, ':'),'', $custombodyfont);
		}

		$custombodyfont = str_replace('+', ' ', $custombodyfont);

		echo "body {font-family: '$custombodyfont', Arial, sans-serif;}";

	}

	// Main Body color
	if ($bodybgcolor !="") {
		echo 'body { background: ' . $bodybgcolor . ';}';
	}

	if ($bodytextcolor !="") {
		echo 'body { color: ' . $bodytextcolor . ';}';
	}

	// Link color
	if ($linkcolor !="") {
		echo 'a { color: ' . $linkcolor . ';}';
		echo '#portfolio-wrapper .tooltip4background { -webkit-box-shadow: 0px 0px 3px 2px ' . $linkcolor . '; box-shadow: 0px 0px 3px 2px ' . $linkcolor . ';}';
		echo '.port-folio .image-box:hover { -webkit-box-shadow: 0px 0px 3px 2px ' . $linkcolor . '; box-shadow: 0px 0px 3px 2px ' . $linkcolor . ';}';
		echo '#product-single-wrapper .image-box:hover { -webkit-box-shadow: 0px 0px 3px 2px ' . $linkcolor . '; box-shadow: 0px 0px 3px 2px ' . $linkcolor . ';}';
	}

	if ($linkhovercolor !="") {
		echo 'a:hover { color: ' . $linkhovercolor . ';}';
	}

	// Header Section

	if ($headersectionbg != "") { 
		echo '#header-wrapper { background: ' . $headersectionbg . ';}';
		echo '#header-wrapper .navbar-inverse .btn-navbar { background: ' . $headersectionbg . ';}';
	}

	if ($headersectionbgpattern != "none") { 
		echo '#header-wrapper {' . mpt_load_bg_patterns($headersectionbgpattern) . '}';
	}	

	// menu settings

	if ($menusettingsbg != "") { 
		echo '#header-wrapper .navbar-inner { background: ' . $menusettingsbg . ';}';
	}

	if ($menusettingstext != "") { 
		echo '#header-wrapper'.$headerstyle.' .nav > li > a { color: ' . $menusettingstext . ';}';
		echo '#header-wrapper'.$headerstyle.' .nav li.dropdown > .dropdown-toggle .caret { border-top-color: ' . $menusettingstext . '; border-bottom-color: ' . $menusettingstext . '; }';
	}

	if ($menusettingstexthover != "") { 
		echo '#header-wrapper'.$headerstyle.' .nav > li.dropdown:hover > a.dropdown-toggle, #header-wrapper'.$headerstyle.' .nav > li > a:hover, #header-wrapper'.$headerstyle.' .nav > .active > a, #header-wrapper'.$headerstyle.' .nav > .active > a:hover, #header-wrapper'.$headerstyle.' .nav > .active > a:focus { color: ' . $menusettingstexthover . ';}';
		echo '#header-wrapper'.$headerstyle.' .nav li.dropdown:hover > .dropdown-toggle .caret { border-top-color: ' . $menusettingstexthover . '; border-bottom-color: ' . $menusettingstexthover . '; }';
	}

	if ($menusettingsnavbgcolorhover != "") { 
		echo '#header-wrapper'.$headerstyle.' .nav > li > a:hover { background: ' . $menusettingsnavbgcolorhover . ';}';
		echo '#header-wrapper'.$headerstyle.' .nav > li.dropdown:hover > a.dropdown-toggle, #header-wrapper'.$headerstyle.' .nav > .active > a, #header-wrapper'.$headerstyle.' .nav > .active > a:hover, #header-wrapper'.$headerstyle.' .nav > .active > a:focus { background: ' . $menusettingsnavbgcolorhover . ';}';
		echo '#header-wrapper'.$headerstyle.' .nav li.dropdown.open > .dropdown-toggle, #header-wrapper'.$headerstyle.' .nav li.dropdown.active > .dropdown-toggle, #header-wrapper'.$headerstyle.' .nav li.dropdown.open.active > .dropdown-toggle { background: ' . $menusettingsnavbgcolorhover . ';}';
	}

	if ($menusettingsdropdownbgcolor != "") { 
		echo '#header-wrapper'.$headerstyle.' .dropdown-menu a,#header-wrapper'.$headerstyle.' .dropdown-submenu a,#header-wrapper'.$headerstyle.' .dropdown-menu,#header-wrapper'.$headerstyle.' .dropdown-menu li > a { background: ' . $menusettingsdropdownbgcolor . ';}';
		echo '.navbar .nav > li > .dropdown-menu:after {border-bottom: 6px solid ' . $menusettingsdropdownbgcolor . ';}';
	}

	if ($menusettingsdropdowntextcolor != "") { 
		echo '#header-wrapper'.$headerstyle.' .dropdown-menu a,#header-wrapper'.$headerstyle.' .dropdown-submenu a,#header-wrapper'.$headerstyle.' .dropdown-menu,#header-wrapper'.$headerstyle.' .dropdown-menu li > a { color: ' . $menusettingsdropdowntextcolor . ';}';
	}

	if ($menusettingsdropdownbgcolorhover != "") { 
		echo '#header-wrapper'.$headerstyle.' .dropdown-menu .active > a,#header-wrapper'.$headerstyle.' .dropdown-menu .active > a:hover,#header-wrapper'.$headerstyle.' .dropdown-menu li > a:hover,#header-wrapper'.$headerstyle.' .dropdown-menu li > a:focus,#header-wrapper'.$headerstyle.' .dropdown-submenu:hover > a { background: ' . $menusettingsdropdownbgcolorhover . ';}';
	}

	if ($menusettingsdropdowntextcolorhover != "") { 
		echo '#header-wrapper'.$headerstyle.' .dropdown-menu .active > a,#header-wrapper'.$headerstyle.' .dropdown-menu .active > a:hover,#header-wrapper'.$headerstyle.' .dropdown-menu li > a:hover,#header-wrapper'.$headerstyle.' .dropdown-menu li > a:focus,#header-wrapper'.$headerstyle.' .dropdown-submenu:hover > a { color: ' . $menusettingsdropdowntextcolorhover . ';}';
	}

	// Homepage Section

	if ($homepagebgcolor !="") {
		echo '#homepage-content-wrapper { background: ' . $homepagebgcolor . ';}';
	}

	if ($homepagetextcolor !="") {
		echo '#homepage-content-wrapper { color: ' . $homepagetextcolor . ';}';
	}

	if ($homepagebgpattern != "none") { 
		echo '#homepage-content-wrapper {' . mpt_load_bg_patterns($homepagebgpattern) . '}';
	}

	// MarketPress Settings

	switch ($floatincartposition) {
		case 'Top Left':
			echo '#floating-menu {position:fixed;top: 5%;right:auto;left: 0px;z-index: 1050;}';
			echo '#floating-menu button {float: left;-webkit-border-top-left-radius: 0px;border-top-left-radius: 0px;-webkit-border-bottom-left-radius: 0px;border-bottom-left-radius: 0px;-webkit-border-top-right-radius: 4px;border-top-right-radius: 4px;-webkit-border-bottom-right-radius: 4px;border-bottom-right-radius: 4px;}';
			break;

		case 'Top Right':
			echo '#floating-menu {position:fixed;top: 5%;right:0px;z-index: 1050;}';
			echo '#floating-menu button {float: right;-webkit-border-top-right-radius: 0px;border-top-right-radius: 0px;-webkit-border-bottom-right-radius: 0px;border-bottom-right-radius: 0px;}';
			break;

		case 'Middle Left':
			echo '#floating-menu {position:fixed;top: 50%;right:auto;left: 0px;z-index: 1050;}';
			echo '#floating-menu button {float: left;-webkit-border-top-left-radius: 0px;border-top-left-radius: 0px;-webkit-border-bottom-left-radius: 0px;border-bottom-left-radius: 0px;-webkit-border-top-right-radius: 4px;border-top-right-radius: 4px;-webkit-border-bottom-right-radius: 4px;border-bottom-right-radius: 4px;}';
			break;


		case 'Middle Right':
			echo '#floating-menu {position:fixed;top: 50%;right:0px;z-index: 1050;}';
			echo '#floating-menu button {float: right;-webkit-border-top-right-radius: 0px;border-top-right-radius: 0px;-webkit-border-bottom-right-radius: 0px;border-bottom-right-radius: 0px;}';
			break;


		case 'Bottom Left':
			echo '#floating-menu {position:fixed;top: 85%;right:auto;left: 0px;z-index: 1050;}';
			echo '#floating-menu button {float: left;-webkit-border-top-left-radius: 0px;border-top-left-radius: 0px;-webkit-border-bottom-left-radius: 0px;border-bottom-left-radius: 0px;-webkit-border-top-right-radius: 4px;border-top-right-radius: 4px;-webkit-border-bottom-right-radius: 4px;border-bottom-right-radius: 4px;}';
			break;


		case 'Bottom Right':
			echo '#floating-menu {position:fixed;top: 85%;right:0px;z-index: 1050;}';
			echo '#floating-menu button {float: right;-webkit-border-top-right-radius: 0px;border-top-right-radius: 0px;-webkit-border-bottom-right-radius: 0px;border-bottom-right-radius: 0px;}';
			break;
		
		default:
			echo '#floating-menu {position:fixed;top: 5%;right:0px;z-index: 1050;}';
			echo '#floating-menu button {-webkit-border-top-right-radius: 0px;border-top-right-radius: 0px;-webkit-border-bottom-right-radius: 0px;border-bottom-right-radius: 0px;}';
			break;
	}

	// Page Settings 

	if ($pagecontentbg != "") { 
		echo '#page-wrapper .content-section { background: ' . $pagecontentbg . ';}';
	}

	if ($pagecontenttext != "") { 
		echo '#page-wrapper .content-section { color: ' . $pagecontenttext . ';}';
	}

	if ($pagecontentbgpattern != "none") { 
		echo '#page-wrapper .content-section {' . mpt_load_bg_patterns($pagecontentbgpattern) . '}';
	}	


	// Post Settings
	if ($postcontentbg != "") { 
		echo '#post-wrapper .content-section { background: ' . $postcontentbg . ';}';
		echo '#product-single-wrapper .content-section { background: ' . $postcontentbg . ';}';
	}

	if ($postcontenttext != "") { 
		echo '#post-wrapper .content-section { color: ' . $postcontenttext . ';}';
		echo '#product-single-wrapper .content-section { color: ' . $postcontenttext . ';}';
	}

	if ($postcontentbgpattern != "none") { 
		echo '#post-wrapper .content-section {' . mpt_load_bg_patterns($postcontentbgpattern) . '}';
		echo '#product-single-wrapper .content-section {' . mpt_load_bg_patterns($postcontentbgpattern) . '}';
	}	


	// Sidebar Widget Settings

	if ($sidebarwidgetbg != "") { 
		echo '#sidebar .well { background: ' . $sidebarwidgetbg . ';}';
	}

	if ($sidebarwidgettext != "") { 
		echo '#sidebar .well { color: ' . $sidebarwidgettext . ';}';
	}

	if ($sidebarwidgetlink != "") { 
		echo '#sidebar .well a { color: ' . $sidebarwidgetlink . ';}';
	}

	switch ($sidebarwidgeticoncolor) {
		case 'White':
			echo '#sidebar ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-white.png");}';
			echo '#sidebar ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-white.png");}';
			break;
		case 'Blue':
			echo '#sidebar ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-blue.png");}';
			echo '#sidebar ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-blue.png");}';
			break;
		case 'Light Blue':
			echo '#sidebar ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-lightblue.png");}';
			echo '#sidebar ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-lightblue.png");}';
			break;
		case 'Green':
			echo '#sidebar ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-green.png");}';
			echo '#sidebar ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-green.png");}';
			break;
		case 'Yellow':
			echo '#sidebar ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-yellow.png");}';
			echo '#sidebar ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-yellow.png");}';
			break;
		case 'Red':
			echo '#sidebar ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-red.png");}';
			echo '#sidebar ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-red.png");}';
			break;
		case 'Black':
			echo '#sidebar ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings.png");}';
			echo '#sidebar ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings.png");}';
			break;
		
		default:
			echo '#sidebar ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings.png");}';
			echo '#sidebar ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings.png");}';
			break;
	}

	if ($sidebarwidgetbghover != "") { 
		echo '#sidebar .well:hover { background: ' . $sidebarwidgetbghover . ';}';
	}

	if ($sidebarwidgettexthover != "") { 
		echo '#sidebar .well:hover { color: ' . $sidebarwidgettexthover . ';}';
	}

	if ($sidebarwidgetlinkhover != "") { 
		echo '#sidebar .well:hover a { color: ' . $sidebarwidgetlinkhover . ';}';
	}

	switch ($sidebarwidgeticoncolorhover) {
		case 'White':
			echo '#sidebar .well:hover ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-white.png");}';
			echo '#sidebar .well:hover ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-white.png");}';
			break;
		case 'Blue':
			echo '#sidebar .well:hover ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-blue.png");}';
			echo '#sidebar .well:hover ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-blue.png");}';
			break;
		case 'Light Blue':
			echo '#sidebar .well:hover ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-lightblue.png");}';
			echo '#sidebar .well:hover ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-lightblue.png");}';
			break;
		case 'Green':
			echo '#sidebar .well:hover ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-green.png");}';
			echo '#sidebar .well:hover ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-green.png");}';
			break;
		case 'Yellow':
			echo '#sidebar .well:hover ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-yellow.png");}';
			echo '#sidebar .well:hover ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-yellow.png");}';
			break;
		case 'Red':
			echo '#sidebar .well:hover ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-red.png");}';
			echo '#sidebar .well:hover ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings-red.png");}';
			break;
		case 'Black':
			echo '#sidebar .well:hover ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings.png");}';
			echo '#sidebar .well:hover ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings.png");}';
			break;
		
		default:
			echo '#sidebar .well:hover ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings.png");}';
			echo '#sidebar .well:hover ul#mp_product_list li i.icon-tags {background-image: url("'.$themefolder.'/img/glyphicons-halflings.png");}';
			break;
	}

	// Footer Widget Settings

	if ($footerwidgetbg != "") { 
		echo '#footer-widget { background: ' . $footerwidgetbg . ';}';
	}

	if ($footerwidgettext != "") { 
		echo '#footer-widget { color: ' . $footerwidgettext . ';}';
	}

	if ($footerwidgetbgpattern != "none") { 
		echo '#footer-widget {' . mpt_load_bg_patterns($footerwidgetbgpattern) . '}';
	}	

	if ($footerwidgetlink != "") { 
		echo '#footer-widget a { color: ' . $footerwidgetlink . ';}';
	}

	if ($footerwidgetlinkhover != "") { 
		echo '#footer-widget a:hover { color: ' . $footerwidgetlinkhover . ';}';
	}

	switch ($footerwidgeticoncolor) {
		case 'White':
			echo '#footer-widget ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-white.png");}';
			break;
		case 'Blue':
			echo '#footer-widget ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-blue.png");}';
			break;
		case 'Light Blue':
			echo '#footer-widget ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-lightblue.png");}';
			break;
		case 'Green':
			echo '#footer-widget ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-green.png");}';
			break;
		case 'Yellow':
			echo '#footer-widget ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-yellow.png");}';
			break;
		case 'Red':
			echo '#footer-widget ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings-red.png");}';
			break;
		case 'Black':
			echo '#footer-widget ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings.png");}';
			break;
		
		default:
			echo '#footer-widget ul li:before {background-image: url("'.$themefolder.'/img/glyphicons-halflings.png");}';
			break;
	}

	// Footer Section

	if ($footersectionbg != "") { 
		echo '.footer-wrapper { background: ' . $footersectionbg . ';}';
	}

	if ($footersectiontext != "") { 
		echo '.footer-wrapper { color: ' . $footersectiontext . ';}';
	}

	if ($footersectionbgpattern != "none") { 
		echo '.footer-wrapper {' . mpt_load_bg_patterns($footersectionbgpattern) . '}';
	}	

	if ($footersectionlink != "") { 
		echo '.footer-wrapper a { color: ' . $footersectionlink . ';}';
	}

	if ($footersectionlinkhover != "") { 
		echo '.footer-wrapper a:hover { color: ' . $footersectionlinkhover . ';}';
	}
	

		echo '#product_category-adder{display: none;}';
	

?>
 
</style>