<ul id="menu-menu-2" class="nav nav-pills pull-right"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home active menu-item-9"><a href="http://artbulgaria.com/">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-42 dropdown"><a href="http://artbulgaria.com/stores/" data-toggle="dropdown" class="dropdown-toggle">Stores <span class="caret"></span></a>
<ul class="dropdown-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1228"><a href="http://artbulgaria.com/products/">Products</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1226"><a href="http://artbulgaria.com/store/shopping-cart/">Shopping Cart</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1227"><a href="http://artbulgaria.com/store/order-status/">Order Status</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-201"><a href="http://artbulgaria.com/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-200"><a href="http://artbulgaria.com/privacy-policy/">Privacy Policy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-202"><a href="http://artbulgaria.com/contact-us/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-640"><a href="http://artbulgaria.com/blogs/">Blogs</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1190"><a href="http://artbulgaria.com/forums/">Forums</a></li>
</ul>