<?php get_header(); ?>

<?php
	//$sidebar_position = get_option('mpt_wp_page_sidebar_position');
	$sidebar_position = get_blog_option(1,'mpt_wp_page_sidebar_position');
	$selected_sidebar = mpt_load_selected_sidebar('page');
?>
	<!-- Page -->
	<div id="page-wrapper">
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">

					<div class="row-fluid">

						<?php if ( empty($sidebar_position) || $sidebar_position == 'Left' ) : ?>
							<div id="sidebar" class="span3">
								<?php the_widget('TP_Featured_Widgets','title='.__('Featured Stores'),'before_widget=<div class="well sidebar-widget-well">&before_title=<h4 class="sidebar-widget-title">&after_title=</h4>&after_widget=</div><div class="clear"></div>',array('order_by'=>'name','limit'=>5)); ?>
								<?php the_widget('TP_Featured_Products','title='.__('Featured Products'),'before_widget=<div class="well sidebar-widget-well">&before_title=<h4 class="sidebar-widget-title">&after_title=</h4>&after_widget=</div><div class="clear"></div>',array('order_by'=>'name','limit'=>5)); ?>
							</div>
						<?php endif; ?>

						<div class="span9">

							<div <?php post_class(); ?>>

								<div class="page-heading">
									<h4><span<?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : ''); ?>><?php echo $store_reviews_title; ?></span></h4>
								</div>

								<div class="page-content">

									<?php //the_content(); ?>
									<?php echo do_shortcode('[WPCR_INSERT]'); ?>
									<?php //echo do_shortcode('[WPCR_SHOW POSTID="ALL" NUM="" SNIPPET="" MORE="" HIDECUSTOM="0" HIDERESPONSE="0"]'); ?>
				
									<?php edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>

								</div>
							</div>
						</div><!-- / span8 -->

						<?php if ( !empty($sidebar_position) && $sidebar_position == 'Right' ) : ?>
							<div id="sidebar" class="span3">
								<?php get_sidebar( $selected_sidebar ); ?>
							</div>
						<?php endif; ?>
						
					</div><!-- / row-fluid -->

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>