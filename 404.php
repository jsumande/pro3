<?php get_header(); ?>

	<!-- Page  Samrtgit Test-->
	<div id="page-wrapper">

		<div class="content-section">
			<div class="outercontainer" style="background: url('<?php echo get_template_directory_uri(); ?>/img/pnf_bg.jpg'); background-size: cover;">
				<div id="page-not-found" class="container">
					<div class="row">
						<div class="col-md-12" style="min-height: 430px;">
							
							<h1 class="pnf-title text-center"><?php if( isset($_GET['sr']) ) echo _e('Oppps... no results for '.$_GET['sr'],'pro'); else echo _e('Oppps... page not found!','pro'); ?></h1>
							<h5 class="pnf-sub text-center"><?php _e('It seems that we are unable to find what you are looking for. Perhaps try one of the links below:' , 'pro') ?></h5>
								<form role="search" class="form-search form-inline" method="get" id="searchform" action="<?php echo localize_url(get_home_url()); ?>">
									<div id="pnf-search" class="row">
										<div class="col-sm-12 col-xs-12 col-md-4">
										<?php
											$archive_stuff = '<p>' . __('Try looking in the monthly archives:' , 'pro') . '</p>';
											//the_widget( 'WP_Widget_Archives', array('count' => 0 , 'dropdown' => 1, 'format' => 'option', 'echo' => 0 ), array( 'before_title' => '<h4 style="display: none;">', 'after_title' => '</h4>'.$archive_stuff ) );
										?>
										<?php
										echo $archive_stuff ;
										global $wpdb;
										$limit = 0;
										$months = $wpdb->get_results("SELECT DISTINCT MONTH( post_date ) AS month ,  YEAR( post_date ) AS year, COUNT( id ) as post_count FROM $wpdb->posts WHERE post_status = 'publish' and post_date <= now( ) and post_type = 'post' GROUP BY month , year ORDER BY post_date DESC");
										echo '<select name="archive"><option>'. __('Select Month','pro').'</option>';
										foreach($months as $month) :	?>
											<option value="<?php echo $month->year . ' ' . $month->month; ?>"><?php echo $month->year; ?> <?php _e(date("F", mktime(0, 0, 0, $month->month, 1, $month->year)), 'pro') ?></option>
										<?php
										if(++$limit >= 18) { break; }
										endforeach;
										echo '</select>';
										?>
										</div>
										<div class="col-sm-12 col-xs-12 col-md-8">
											<p><?php _e('Try looking in the keywords:' , 'pro') ?></p>										
												<div id="pnf-keywords" class="row">
													<div class="col-xs-12 col-sm-9 col-md-9" style="padding-left: 0;">
														<div class="form-group pnf-search">
															<input type="text" value="" class="form-control" name="s" id="s" placeholder="<?php _e('Search','pro'); ?>">
														</div>
													</div>
													<div class="col-xs-12 col-sm-3 col-md-3">
														<button class="btn btn-default" type="submit" id="searchsubmit"><i class="fa fa-search"></i> &nbsp;&nbsp; <?php _e('Search','pro'); ?> &nbsp;&nbsp;</button>
													</div>
												</div>
																						
										</div>
									</div>
								</form>
								<!--
								<ul id="pnf-cat" class='list-category text-center'>
								<?php 
									if(!defined('ICL_LANGUAGE_CODE')){
										define('ICL_LANGUAGE_CODE','en');
									}
										
									$taxonomy = 'product_category';
									$settings = get_site_option( 'mp_network_settings' );
									$args = array( 'hide_empty' => 0,'hierarchical'=> 0,'parent'=> 0 );	
									$terms = tp_get_terms($taxonomy, $args);

									if ( !empty( $terms ) && !is_wp_error( $terms ) ) {
										
										foreach ($terms as $term) {
											
											$link = localize_url(get_home_url( mp_main_site_id(), $settings['slugs']['marketplace'] . '/' . $settings['slugs']['categories'] . '/' . $term->slug . '/' ));
											
											if($term->name == 'Clothes / Shoes' || $term->name == 'Дрехи и обувки'){
												$output .= '<li><a href="'.add_lang_vars_custom($link).'">'.__('Clothes','pro').'</a></li>';
												$output .= '<li><a href="'.add_lang_vars_custom($link).'">'.__('Shoes','pro').'</a></li>';
											} else {
												$output .= '<li><a href="'.add_lang_vars_custom($link).'">'.$term->name .'</a></li>';
											}	
											
										}
											echo $output;
									}
								?>								
							</ul>
							-->
						</div><!-- / span8 -->					
						
				
					</div><!-- / row -->

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>