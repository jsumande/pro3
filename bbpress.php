<?php get_header(); ?>

<?php
	//$sidebar_position = get_option('mpt_wp_page_sidebar_position');
	$sidebar_position = get_blog_option(1,'mpt_wp_page_sidebar_position');
	$selected_sidebar = mpt_load_selected_sidebar('page');
?>
	<!-- Page -->
	<div id="page-wrapper">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php 
						$contentbgcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_bg_color', true ));
						$contenttextcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_text_color', true ));
					?>

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">
					
					<div class="row no-margin">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="bbpress-bc">										
						<?php bbp_breadcrumb(); ?>
						</div>
						<div class="clear padding10"></div>
						
						<div class="row ">
							<div id="sidebar" class="col-md-3 col-sm-3 col-xs-12">
								<?php get_sidebar( 'bbpress' ); ?>
							</div>					
							<div class="col-md-9 col-sm-9 col-xs-12">
								<div id="post-<?php the_ID(); ?>" <?php post_class(); ?><?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>
									<div class="page-heading">
										<h4><span<?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>><?php the_title(); ?></span></h4>
									</div>

									<div class="page-content">
										<?php the_content(); ?>
										<?php //edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>
									</div>

									<?php wp_link_pages(array('before' => '<p class="margin-vertical-15">' . __( 'Pages:' , 'pro' ) , 'after' => '</p>' , 'next_or_number' => 'number')); ?>
									<?php 

										$showcomments = get_post_meta( $post->ID, '_mpt_page_show_comments', true );
										if ( $showcomments == 'on') {
											echo '<div class="page-comments">';
											comments_template();
											echo '</div>';
										} 
									?>
								</div>

							<?php endwhile; endif; ?>							
							</div><!-- / col-md-9 -->
						</div><!-- / row -->
					</div><!-- / col-md-12 col-sm-12 col-xs-12 -->
					</div><!-- / row -->
				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	
	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>