/*!
 * jQuery Selectbox plugin 0.2
 *
 * Copyright 2011-2012, Dimitar Ivanov (http://www.bulgaria-web-developers.com/projects/javascript/selectbox/)
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 * 
 * Date: Tue Jul 17 19:58:36 2012 +0300
 */
(function ($, undefined) {
	var PROP_NAME = 'selectbox',
		FALSE = false,
		TRUE = true;
	/**
	 * Selectbox manager.
	 * Use the singleton instance of this class, $.selectbox, to interact with the select box.
	 * Settings for (groups of) select boxes are maintained in an instance object,
	 * allowing multiple different settings on the same page
	 */
	function Selectbox() {
		this._state = [];
		this._defaults = { // Global defaults for all the select box instances
			classHolder: "sbHolder",
			classHolderDisabled: "sbHolderDisabled",
			classSelector: "sbSelector",
			classOptions: "sbOptions",
			classGroup: "sbGroup",
			classSub: "sbSub",
			classDisabled: "sbDisabled",
			classToggleOpen: "sbToggleOpen",
			classToggle: "sbToggle",
			classFocus: "sbFocus",
			speed: 200,
			effect: "slide", // "slide" or "fade"
			onChange: null, //Define a callback function when the selectbox is changed
			onOpen: null, //Define a callback function when the selectbox is open
			onClose: null //Define a callback function when the selectbox is closed
		};
	}
	
	$.extend(Selectbox.prototype, {
		/**
		 * Is the first field in a jQuery collection open as a selectbox
		 * 
		 * @param {Object} target
		 * @return {Boolean}
		 */
		_isOpenSelectbox: function (target) {
			if (!target) {
				return FALSE;
			}
			var inst = this._getInst(target);
			return inst.isOpen;
		},
		/**
		 * Is the first field in a jQuery collection disabled as a selectbox
		 * 
		 * @param {HTMLElement} target
		 * @return {Boolean}
		 */
		_isDisabledSelectbox: function (target) {
			if (!target) {
				return FALSE;
			}
			var inst = this._getInst(target);
			return inst.isDisabled;
		},
		/**
		 * Attach the select box to a jQuery selection.
		 * 
		 * @param {HTMLElement} target
		 * @param {Object} settings
		 */
		_attachSelectbox: function (target, settings) {
			if (this._getInst(target)) {
				return FALSE;
			}
			var $target = $(target),
				self = this,
				inst = self._newInst($target),
				sbHolder, sbSelector, sbToggle, sbOptions,
				s = FALSE, optGroup = $target.find("optgroup"), opts = $target.find("option"), olen = opts.length;
				
			$target.attr("sb", inst.uid);
				
			$.extend(inst.settings, self._defaults, settings);
			self._state[inst.uid] = FALSE;
			$target.hide();
			
			function closeOthers() {
				var key, sel,
					uid = this.attr("id").split("_")[1];
				for (key in self._state) {
					if (key !== uid) {
						if (self._state.hasOwnProperty(key)) {
							sel = $("select[sb='" + key + "']")[0];
							if (sel) {
								self._closeSelectbox(sel);
							}
						}
					}
				}
			}
			
			sbHolder = $("<div>", {
				"id": "sbHolder_" + inst.uid,
				"class": inst.settings.classHolder,
				"tabindex": $target.attr("tabindex")
			});
			
			sbSelector = $("<a>", {
				"id": "sbSelector_" + inst.uid,
				"href": "#",
				"class": inst.settings.classSelector,
				"click": function (e) {
					e.preventDefault();
					closeOthers.apply($(this), []);
					var uid = $(this).attr("id").split("_")[1];
					if (self._state[uid]) {
						self._closeSelectbox(target);
					} else {
						self._openSelectbox(target);
					}
				}
			});
			
			sbToggle = $("<a>", {
				"id": "sbToggle_" + inst.uid,
				"href": "#",
				"class": inst.settings.classToggle,
				"click": function (e) {
					e.preventDefault();
					closeOthers.apply($(this), []);
					var uid = $(this).attr("id").split("_")[1];
					if (self._state[uid]) {
						self._closeSelectbox(target);
					} else {
						self._openSelectbox(target);
					}
				}
			});
			sbToggle.appendTo(sbHolder);

			sbOptions = $("<ul>", {
				"id": "sbOptions_" + inst.uid,
				"class": inst.settings.classOptions,
				"css": {
					"display": "none"
				}
			});
			
			$target.children().each(function(i) {
				var that = $(this), li, config = {};
				if (that.is("option")) {
					getOptions(that);
				} else if (that.is("optgroup")) {
					li = $("<li>");
					$("<span>", {
						"text": that.attr("label")
					}).addClass(inst.settings.classGroup).appendTo(li);
					li.appendTo(sbOptions);
					if (that.is(":disabled")) {
						config.disabled = true;
					}
					config.sub = true;
					getOptions(that.find("option"), config);
				}
			});
			
			function getOptions () {
				var sub = arguments[1] && arguments[1].sub ? true : false,
					disabled = arguments[1] && arguments[1].disabled ? true : false;
				arguments[0].each(function (i) {
					var that = $(this),
						li = $("<li>"),
						child;
					if (that.is(":selected")) {
						sbSelector.html(that.html());
						s = TRUE;
					}
					if (i === olen - 1) {
						li.addClass("last");
					}
					if (!that.is(":disabled") && !disabled) {
						child = $("<a>", {
							"href": "#" + that.val(),
							"rel": that.val()
						}).html(that.html()).bind("click.sb", function (e) {
							if (e && e.preventDefault) {
								e.preventDefault();
							}
							var t = sbToggle,
							 	$this = $(this),
								uid = t.attr("id").split("_")[1];
							self._changeSelectbox(target, $this.attr("rel"), $this.html());
							self._closeSelectbox(target);
						}).bind("mouseover.sb", function () {
							var $this = $(this);
							$this.parent().siblings().find("a").removeClass(inst.settings.classFocus);
							$this.addClass(inst.settings.classFocus);
						}).bind("mouseout.sb", function () {
							$(this).removeClass(inst.settings.classFocus);
						});
						if (sub) {
							child.addClass(inst.settings.classSub);
						}
						if (that.is(":selected")) {
							child.addClass(inst.settings.classFocus);
						}
						child.appendTo(li);
					} else {
						child = $("<span>", {
							"text": that.html()
						}).addClass(inst.settings.classDisabled);
						if (sub) {
							child.addClass(inst.settings.classSub);
						}
						child.appendTo(li);
					}
					li.appendTo(sbOptions);
				});
			}
			
			if (!s) {
				sbSelector.html(opts.first().html());
			}

			$.data(target, PROP_NAME, inst);
			
			sbHolder.data("uid", inst.uid).bind("keydown.sb", function (e) {
				var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0,
					$this = $(this),
					uid = $this.data("uid"),
					inst = $this.siblings("select[sb='"+uid+"']").data(PROP_NAME),
					trgt = $this.siblings(["select[sb='", uid, "']"].join("")).get(0),
					$f = $this.find("ul").find("a." + inst.settings.classFocus);
				switch (key) {
					case 37: //Arrow Left
					case 38: //Arrow Up
						if ($f.length > 0) {
							var $next;
							$("a", $this).removeClass(inst.settings.classFocus);
							$next = $f.parent().prevAll("li:has(a)").eq(0).find("a");
							if ($next.length > 0) {
								$next.addClass(inst.settings.classFocus).focus();
								$("#sbSelector_" + uid).html($next.html());
							}
						}
						break;
					case 39: //Arrow Right
					case 40: //Arrow Down
						var $next;
						$("a", $this).removeClass(inst.settings.classFocus);
						if ($f.length > 0) {
							$next = $f.parent().nextAll("li:has(a)").eq(0).find("a");
						} else {
							$next = $this.find("ul").find("a").eq(0);
						}
						if ($next.length > 0) {
							$next.addClass(inst.settings.classFocus).focus();
							$("#sbSelector_" + uid).html($next.html());
						}
						break;				
					case 13: //Enter
						if ($f.length > 0) {
							self._changeSelectbox(trgt, $f.attr("rel"), $f.html());
						}
						self._closeSelectbox(trgt);
						break;
					case 9: //Tab
						if (trgt) {
							var inst = self._getInst(trgt);
							if (inst/* && inst.isOpen*/) {
								if ($f.length > 0) {
									self._changeSelectbox(trgt, $f.attr("rel"), $f.html());
								}
								self._closeSelectbox(trgt);
							}
						}
						var i = parseInt($this.attr("tabindex"), 10);
						if (!e.shiftKey) {
							i++;
						} else {
							i--;
						}
						$("*[tabindex='" + i + "']").focus();
						break;
					case 27: //Escape
						self._closeSelectbox(trgt);
						break;
				}
				e.stopPropagation();
				return false;
			}).delegate("a", "mouseover", function (e) {
				$(this).addClass(inst.settings.classFocus);
			}).delegate("a", "mouseout", function (e) {
				$(this).removeClass(inst.settings.classFocus);	
			});
			
			sbSelector.appendTo(sbHolder);
			sbOptions.appendTo(sbHolder);			
			sbHolder.insertAfter($target);
			
			$("html").live('mousedown', function(e) {
				e.stopPropagation();          
				$("select").selectbox('close'); 
			});
			$([".", inst.settings.classHolder, ", .", inst.settings.classSelector].join("")).mousedown(function(e) {    
				e.stopPropagation();
			});
		},
		/**
		 * Remove the selectbox functionality completely. This will return the element back to its pre-init state.
		 * 
		 * @param {HTMLElement} target
		 */
		_detachSelectbox: function (target) {
			var inst = this._getInst(target);
			if (!inst) {
				return FALSE;
			}
			$("#sbHolder_" + inst.uid).remove();
			$.data(target, PROP_NAME, null);
			$(target).show();			
		},
		/**
		 * Change selected attribute of the selectbox.
		 * 
		 * @param {HTMLElement} target
		 * @param {String} value
		 * @param {String} text
		 */
		_changeSelectbox: function (target, value, text) {
			var onChange,
				inst = this._getInst(target);
			if (inst) {
				onChange = this._get(inst, 'onChange');
				$("#sbSelector_" + inst.uid).html(text);
			}
            value = String(value).replace(/\'/g, "\\'");
            $(target).find("option[selected='selected']").removeAttr("selected");
			$(target).find("option[value='" + value + "']").attr("selected", "selected");
			if (inst && onChange) {
				onChange.apply((inst.input ? inst.input[0] : null), [value, inst]);
			} else if (inst && inst.input) {
				inst.input.trigger('change');
			}
		},
		/**
		 * Enable the selectbox.
		 * 
		 * @param {HTMLElement} target
		 */
		_enableSelectbox: function (target) {
			var inst = this._getInst(target);
			if (!inst || !inst.isDisabled) {
				return FALSE;
			}
			$("#sbHolder_" + inst.uid).removeClass(inst.settings.classHolderDisabled);
			inst.isDisabled = FALSE;
			$.data(target, PROP_NAME, inst);
		},
		/**
		 * Disable the selectbox.
		 * 
		 * @param {HTMLElement} target
		 */
		_disableSelectbox: function (target) {
			var inst = this._getInst(target);
			if (!inst || inst.isDisabled) {
				return FALSE;
			}
			$("#sbHolder_" + inst.uid).addClass(inst.settings.classHolderDisabled);
			inst.isDisabled = TRUE;
			$.data(target, PROP_NAME, inst);
		},
		/**
		 * Get or set any selectbox option. If no value is specified, will act as a getter.
		 * 
		 * @param {HTMLElement} target
		 * @param {String} name
		 * @param {Object} value
		 */
		_optionSelectbox: function (target, name, value) {
			var inst = this._getInst(target);
			if (!inst) {
				return FALSE;
			}
			//TODO check name
			inst[name] = value;
			$.data(target, PROP_NAME, inst);
		},
		/**
		 * Call up attached selectbox
		 * 
		 * @param {HTMLElement} target
		 */
		_openSelectbox: function (target) {
			var inst = this._getInst(target);
			//if (!inst || this._state[inst.uid] || inst.isDisabled) {
			if (!inst || inst.isOpen || inst.isDisabled) {
				return;
			}
			var	el = $("#sbOptions_" + inst.uid),
				viewportHeight = parseInt($(window).height(), 10),
				offset = $("#sbHolder_" + inst.uid).offset(),
				scrollTop = $(window).scrollTop(),
				height = el.prev().height(),
				diff = viewportHeight - (offset.top - scrollTop) - height / 2,
				onOpen = this._get(inst, 'onOpen');
			el.css({
				"top": height + "px",
				"maxHeight": (diff - height) + "px"
			});
			inst.settings.effect === "fade" ? el.fadeIn(inst.settings.speed) : el.slideDown(inst.settings.speed);
			$("#sbToggle_" + inst.uid).addClass(inst.settings.classToggleOpen);
			this._state[inst.uid] = TRUE;
			inst.isOpen = TRUE;
			if (onOpen) {
				onOpen.apply((inst.input ? inst.input[0] : null), [inst]);
			}
			$.data(target, PROP_NAME, inst);
		},
		/**
		 * Close opened selectbox
		 * 
		 * @param {HTMLElement} target
		 */
		_closeSelectbox: function (target) {
			var inst = this._getInst(target);
			//if (!inst || !this._state[inst.uid]) {
			if (!inst || !inst.isOpen) {
				return;
			}
			var onClose = this._get(inst, 'onClose');
			inst.settings.effect === "fade" ? $("#sbOptions_" + inst.uid).fadeOut(inst.settings.speed) : $("#sbOptions_" + inst.uid).slideUp(inst.settings.speed);
			$("#sbToggle_" + inst.uid).removeClass(inst.settings.classToggleOpen);
			this._state[inst.uid] = FALSE;
			inst.isOpen = FALSE;
			if (onClose) {
				onClose.apply((inst.input ? inst.input[0] : null), [inst]);
			}
			$.data(target, PROP_NAME, inst);
		},
		/**
		 * Create a new instance object
		 * 
		 * @param {HTMLElement} target
		 * @return {Object}
		 */
		_newInst: function(target) {
			var id = target[0].id.replace(/([^A-Za-z0-9_-])/g, '\\\\$1');
			return {
				id: id, 
				input: target, 
				uid: Math.floor(Math.random() * 99999999),
				isOpen: FALSE,
				isDisabled: FALSE,
				settings: {}
			}; 
		},
		/**
		 * Retrieve the instance data for the target control.
		 * 
		 * @param {HTMLElement} target
		 * @return {Object} - the associated instance data
		 * @throws error if a jQuery problem getting data
		 */
		_getInst: function(target) {
			try {
				return $.data(target, PROP_NAME);
			}
			catch (err) {
				throw 'Missing instance data for this selectbox';
			}
		},
		/**
		 * Get a setting value, defaulting if necessary
		 * 
		 * @param {Object} inst
		 * @param {String} name
		 * @return {Mixed}
		 */
		_get: function(inst, name) {
			return inst.settings[name] !== undefined ? inst.settings[name] : this._defaults[name];
		}
	});

	/**
	 * Invoke the selectbox functionality.
	 * 
	 * @param {Object|String} options
	 * @return {Object}
	 */
	$.fn.selectbox = function (options) {
		
		var otherArgs = Array.prototype.slice.call(arguments, 1);
		if (typeof options == 'string' && options == 'isDisabled') {
			return $.selectbox['_' + options + 'Selectbox'].apply($.selectbox, [this[0]].concat(otherArgs));
		}
		
		if (options == 'option' && arguments.length == 2 && typeof arguments[1] == 'string') {
			return $.selectbox['_' + options + 'Selectbox'].apply($.selectbox, [this[0]].concat(otherArgs));
		}
		
		return this.each(function() {
			typeof options == 'string' ?
				$.selectbox['_' + options + 'Selectbox'].apply($.selectbox, [this].concat(otherArgs)) :
				$.selectbox._attachSelectbox(this, options);
		});
	};
	
	$.selectbox = new Selectbox(); // singleton instance
	$.selectbox.version = "0.2";
})(jQuery);




/*** EXTEND SELECT BOX TO HAVE KeyUp feature modified by jeff to work with scroll when pressing a letter*********/
jQuery.fn.KeyUpSelectBox = function (options)
{
var myHandler = this;

this.init = function ()
{
    var sb = $(this).selectbox(options);
    var sbSelector = sb.attr('sb');

    $("#sbHolder_" + sbSelector).on('keyup', function (e)
    {
        var currentOptionText = $("#sbSelector_" + sbSelector).text().toLowerI();
		var currentOptionValue = $("#sbSelector_" + sbSelector).val().toLowerI();
        var keyText = String.fromCharCode(e.keyCode).toLowerI();

        var selectOptions = myHandler.find('option');
        var firstIndexWithLetter = -1;
        var bSet = false;
        var moveToNext = false;
        var bFoundOption = false;

        if (currentOptionText.substring(0, 1) == keyText)
        {
            moveToNext = true;
        }

        for (var i = 0; i < selectOptions.length; i++)
        {
            var optionText = $(selectOptions[i]).text().toLowerI();
			var optionValue = $(selectOptions[i]).val().toLowerI();
            //console.log(optionText + ': ' + optionText.substring(0, 1));
            if (optionText.substring(0, 1) == keyText)
            {
                if (moveToNext)
                {
                    if (firstIndexWithLetter == -1) { firstIndexWithLetter = i; }

                    if (bFoundOption)
                    {
                        sb.selectbox("change", $(selectOptions[i]).attr('value'), $(selectOptions[i]).html());
				
						
						if(optionText == optionValue){
							var  arelValue = optionText;
						} else {
							var  arelValue = optionValue;
						}
						
						var checkifexist = $('ul#sbOptions_' + sbSelector+' a[rel="'+arelValue+'"]');
						
						if(checkifexist.length){
							$("ul#sbOptions_" + sbSelector).scrollTop($("ul#sbOptions_" + sbSelector).scrollTop()+$('a[rel="'+arelValue+'"]').position().top);
						}

                        sb.val($(selectOptions[i]).attr('value'));
                        bSet = true;
                        break;
                    }

                    if (optionText == currentOptionText)
                    {
                        bFoundOption = true;
                    }
                }
                else
                {
                    sb.selectbox("change", $(selectOptions[i]).attr('value'), $(selectOptions[i]).html());
					
						if(optionText == optionValue){
							var  arelValue = optionText;
						} else {
							var  arelValue = optionValue;
						}
						
						var checkifexist = $('ul#sbOptions_' + sbSelector+' a[rel="'+arelValue+'"]');
							if(checkifexist.length){
								$("ul#sbOptions_" + sbSelector).scrollTop($("ul#sbOptions_" + sbSelector).scrollTop()+$('a[rel="'+arelValue+'"]').position().top);	
							}	
					
                    sb.val($(selectOptions[i]).attr('value'));
                    break;
                }
            }
        }

        if (moveToNext && !bSet && (firstIndexWithLetter != -1))
        {
			if(currentOptionText == currentOptionValue) {
				var  arelValue = currentOptionText;
			} else {
				var  arelValue = currentOptionValue;
			}
			
			var checkifexist = $('ul#sbOptions_' + sbSelector+' a[rel="'+arelValue+'"]');
			
            sb.selectbox("change", $(selectOptions[firstIndexWithLetter]).attr('value'), $(selectOptions[firstIndexWithLetter]).html());
			
			if(checkifexist.length){
				$("ul#sbOptions_" + sbSelector).scrollTop($("ul#sbOptions_" + sbSelector).scrollTop()+$('a[rel="'+arelValue+'"]').position().top);
			}
			
            sb.val($(selectOptions[firstIndexWithLetter]).attr('value'));
        }
    });
}

myHandler.init();

return myHandler;
}

// Bind for jQuery.selectbox
// @jayvee
// @kevy <kevy@kevmt.com>
//
jQuery(window).load(function () {
    var $ = jQuery;

    /**
     * @param text
     * @returns {*}
     */
    function parse_brackets(text) {
        var tags = String(text).match(/\[[^\[]*\]/gm);
        if (tags != null) {
            if (tags.length > 0) {
                for (var i in tags) {
                    text = String(text)
                        .replace('[', '<')
                        .replace('[/', '</')
                        .replace(']', '>');
                }
            }
        }

        return text;
    }

	var patternLetters = /[öäüÖÄÜáàâéèêúùûóòôÁÀÂÉÈÊÚÙÛÓÒÔßãÃõÕçÇñÑ]/g;
	var patternDateDmy = /^(?:\D+)?(\d{1,2})\.(\d{1,2})\.(\d{2,4})$/;
	var lookupLetters = {
	"ä": "a", "ö": "o", "ü": "u",
	"Ä": "A", "Ö": "O", "Ü": "U",
	"á": "a", "à": "a", "â": "a",
	"é": "e", "è": "e", "ê": "e",
	"ú": "u", "ù": "u", "û": "u",
	"ó": "o", "ò": "o", "ô": "o",
	"Á": "A", "À": "A", "Â": "A",
	"É": "E", "È": "E", "Ê": "E",
	"Ú": "U", "Ù": "U", "Û": "U",
	"Ó": "O", "Ò": "O", "Ô": "O",
	"ß": "s", "ã": "a", "Ã": "A",
	"õ": "o", "Õ": "O", "ç": "c",
	"Ç": "C", "ñ": "n", "Ñ": "N"
	};

	var letterTranslator = function (match)
	{
		return lookupLetters[match] || match;
	}

	String.prototype.toLowerI = function ()
	{
		return this.toLowerCase().replace(patternLetters, letterTranslator);
	};	
	
	
    // Loop to each select
    $("select").each(function () {

        // convert to html tag before selectbox,
        // option must have [data-options='parse_brackets'] to parse it
        $(this).find('option[data-options~="parse_brackets"]').each(function () {
            var _o = $(this), _text = _o.text();
            // check for brackets
            if (_text.indexOf('[') != -1 && _text.indexOf('[/') != -1 && _text.indexOf(']') != -1) {
                _o.html(parse_brackets(_text));
            }
        });

        // this checks if the class has .nosbholder -- if it exists,
        // dont bind `.selectbox()` jquery plugin
		if ($(this).hasClass('nosbholder') || $(this).hasClass('monthselect') || $(this).hasClass('yearselect') || $(this).hasClass('hourselect') || $(this).hasClass('minuteselect') || $(this).hasClass('ampmselect')) {
            return;
        }

        // check for ID?
        var chckId = $(this).attr('id');
        if (chckId != "display_pname" && chckId != "display_subpercat") {

            // bind `.selectbox()` jquery plugin
            $(this).KeyUpSelectBox();

            var chckClass = $(this).attr('class');
            if (chckClass == 'input-xlarge') {
                $('.sbHolder').addClass('sbClassHolder');
            }
            else {
                $('.sbHolder').css('min-width', '100%');
            }
        }

        // check for attribute names?
        var chckname = $(this).attr('name');
        if (chckname) {
            if (chckname.indexOf('mp[') === 0) {
                $('.sbHolder')
                    .css('min-width', '57%')
                    .css('margin-right', '100%');
            }
        }

    });

    // sb list anchor
    var sb_a = $('.sbOptions li a');
    sb_a.prepend("&nbsp;");

    $('.sbHolder').click(function () {
        $('.sbHolder').css('border', '1px #cccccc solid');
        $(this).css('border', '1px #5ea6ce solid');
    });


    // --- kevs --
    // to all select that inherits selectbox, apply force triggering onchange

    var originalVal = $.fn.val;

    $.fn.val = function (value) {
        var self = $(this), sb = self.attr('sb');

        if (self[0]) {
            var tag = self[0].tagName;
            if (tag == 'SELECT') {
                if (typeof sb !== typeof undefined
                    && sb !== false
                    && typeof value != 'undefined'
                    && !self.hasClass('nosbholder')) {
                    self.selectbox('change', value,
                        self.find("option[value='" + value + "']").text());
                }
                else if (typeof value != 'undefined') {
                    self.find("option[selected='selected']").removeAttr("selected");
                    self.find("option[value='" + value + "']").attr("selected", "selected");
                }
            }
        }

        if (arguments.length >= 1) {
            return originalVal.call(this, value);
        }

        return originalVal.call(this);
    };

    // ---- DONE ----
});