// Bind for jQuery.selectbox
// @jayvee
// @kevy <kevy@kevmt.com>
//
jQuery(window).load(function () {
    var $ = jQuery;

    /**
     * @param text
     * @returns {*}
     */
    function parse_brackets(text) {
        var tags = String(text).match(/\[[^\[]*\]/gm);
        if (tags != null) {
            if (tags.length > 0) {
                for (var i in tags) {
                    text = String(text)
                        .replace('[', '<')
                        .replace('[/', '</')
                        .replace(']', '>');
                }
            }
        }

        return text;
    }

	var patternLetters = /[öäüÖÄÜáàâéèêúùûóòôÁÀÂÉÈÊÚÙÛÓÒÔßãÃõÕçÇñÑ]/g;
	var patternDateDmy = /^(?:\D+)?(\d{1,2})\.(\d{1,2})\.(\d{2,4})$/;
	var lookupLetters = {
	"ä": "a", "ö": "o", "ü": "u",
	"Ä": "A", "Ö": "O", "Ü": "U",
	"á": "a", "à": "a", "â": "a",
	"é": "e", "è": "e", "ê": "e",
	"ú": "u", "ù": "u", "û": "u",
	"ó": "o", "ò": "o", "ô": "o",
	"Á": "A", "À": "A", "Â": "A",
	"É": "E", "È": "E", "Ê": "E",
	"Ú": "U", "Ù": "U", "Û": "U",
	"Ó": "O", "Ò": "O", "Ô": "O",
	"ß": "s", "ã": "a", "Ã": "A",
	"õ": "o", "Õ": "O", "ç": "c",
	"Ç": "C", "ñ": "n", "Ñ": "N"
	};

	var letterTranslator = function (match)
	{
		return lookupLetters[match] || match;
	}

	String.prototype.toLowerI = function ()
	{
		return this.toLowerCase().replace(patternLetters, letterTranslator);
	};
    $(".media-modal-content .media-router a.media-menu-item").click(function(){
        selectCustomdd();
        console.log('click');
    });	
	
    selectCustomdd();
	function selectCustomdd () {
        // Loop to each select
        $("select").each(function () {

            // convert to html tag before selectbox,
            // option must have [data-options='parse_brackets'] to parse it
            $(this).find('option[data-options~="parse_brackets"]').each(function () {
                var _o = $(this), _text = _o.text();
                // check for brackets
                if (_text.indexOf('[') != -1 && _text.indexOf('[/') != -1 && _text.indexOf(']') != -1) {
                    _o.html(parse_brackets(_text));
                }
            });

            // this checks if the class has .nosbholder -- if it exists,
            // dont bind `.selectbox()` jquery plugin
            if ($(this).hasClass('nosbholder') || $(this).hasClass('monthselect') || $(this).hasClass('yearselect') || $(this).hasClass('hourselect') || $(this).hasClass('minuteselect') || $(this).hasClass('ampmselect')) {
                return;
            }

            // check for ID?
            var chckId = $(this).attr('id');
            if (chckId != "display_pname" && chckId != "display_subpercat") {

                // bind `.selectbox()` jquery plugin
                $(this).KeyUpSelectBox();

                var chckClass = $(this).attr('class');
                if (chckClass == 'input-xlarge') {
                    $('.sbHolder').addClass('sbClassHolder');
                }
                else {
                    $('.sbHolder').css('min-width', '100%');
                }
            }

            // check for attribute names?
            var chckname = $(this).attr('name');
            if (chckname) {
                if (chckname.indexOf('mp[') === 0) {
                    $('.sbHolder')
                        .css('min-width', '57%')
                        .css('margin-right', '100%');
                }
            }

        });
    }


    // sb list anchor
    var sb_a = $('.sbOptions li a');
    sb_a.prepend("&nbsp;");

    $('.sbHolder').click(function () {
        $('.sbHolder').css('border', '1px #cccccc solid');
        $(this).css('border', '1px #5ea6ce solid');
    });


    // --- kevs --
    // to all select that inherits selectbox, apply force triggering onchange

    var originalVal = $.fn.val;

    $.fn.val = function (value) {
        var self = $(this), sb = self.attr('sb');

        if (self[0]) {
            var tag = self[0].tagName;
            if (tag == 'SELECT') {
                if (typeof sb !== typeof undefined
                    && sb !== false
                    && typeof value != 'undefined'
                    && !self.hasClass('nosbholder')) {
                    self.selectbox('change', value,
                        self.find("option[value='" + value + "']").text());
                }
                else if (typeof value != 'undefined') {
                    self.find("option[selected='selected']").removeAttr("selected");
                    self.find("option[value='" + value + "']").attr("selected", "selected");
                }
            }
        }

        if (arguments.length >= 1) {
            return originalVal.call(this, value);
        }

        return originalVal.call(this);
    };

    // ---- DONE ----
});