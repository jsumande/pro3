<?php 
/*
Template Name: Contact Us Page
 */

	//For email sending
	if(isset($_REQUEST['consubmit'])){
		
		$post_status = false;
		
		$name 		= trim($_REQUEST['con_name']);
		$email 		= trim($_REQUEST['con_email']);
		$message 	= trim($_REQUEST['con_message']);
		$website 	= trim($_REQUEST['con_website']);
		
		
		if(!empty($name) && !empty($email) && !empty($message))
			$post_status = true;

		if(is_email($email ))
			$post_status = true;		
		
		
		// get mainsite email
		if (function_exists('is_multisite') && is_multisite())
			$to = get_site_option( 'admin_email' ); 
		else 
			$to = get_option('admin_email');		
		
		
		$siteurl = get_site_url();
		$email_template = get_template_directory_uri().'/email-templates/';			

		if($post_status) {
			
			remove_all_filters( 'wp_mail_from_name' );
			add_filter('wp_mail_from_name', create_function('', "return '{$name}';"));
			
			$subject = __('Contact Us','pro').' - '.$name;

			$headers = array('Content-Type: text/html; charset=UTF-8','From: '.$name.' <'.$email.'>');	

			$facebook_url = esc_url(get_blog_option(1,'mpt_social_icon_1_url'));
			$twitter_url = esc_url(get_blog_option(1,'mpt_social_icon_12_url'));
			$gplus_url = esc_url(get_blog_option(1,'mpt_social_icon_2_url'));
			$linkedin_url = esc_url(get_blog_option(1,'mpt_social_icon_7_url'));
			$pinterest_url = esc_url(get_blog_option(1,'mpt_social_icon_8_url'));			
			
			$formmessage = __('Name:','pro').' '.$name .'<br> '.__('Email:','pro').' '.$email.' '.(!empty($website)? '<br> '.__('Website:','pro').' '.$website : '').' <br><br> '.$message.' ';
			
			$message = __('Theres a new contact us message from','pro').' '.$name.' <br><br>'.$formmessage;
			$contact_admin = __('Admin','pro');
			include_once 'email-templates/contact.php';
			
			$status = wp_mail( $to, $subject, $emailtemplate, $headers );
			
		}	

	}
	
?>
<?php get_header(); ?>

<!-- Page -->
<div id="page-wrapper">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
					<div class="row no-margin">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div id="breadcrumb">
								<div class="row">
									 <div class="col-md-12">
										<ul class="breadcrumbs-list">
											<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>		
											<li class="active"><?php the_title(); ?></li>	
										</ul>
									 </div>
									</div>
							</div>
						<div class="clear padding10"></div>
								<?php //$thecontent = get_the_content(); ?>						
										<div class="contactus-notif">
											<?php
												if (isset($post_status) && $post_status) echo '<div class="alert alert-success" role="alert">'.__('Message Sent! We will contact you as soon as we can.','pro').'</div><br>';
												elseif ( isset($post_status) && !$post_status ) echo '<div class="alert alert-danger" role="alert">'.__('Message was not sent.','pro').'</div> <br>';
											?>
										</div>
										<div class="page-content">
											<div id="page-contact" class="row">
												<div id="contact-left" class="col-md-3 col-sm-3 col-xs-12">
													<div class="well"><?php _e('Contacts','pro'); ?></div>
													<div class="address">													
														<address>
														<i class="fa fa-map-marker"></i>
														<span class="info"> <?php _e('Location:','pro'); ?> P.O. Box 75,  San Antonio, FL 33576</span>
														</address>
														<address>
														<i class="fa fa-phone"></i> 
														<span class="info">														
														<?php _e('Phone','pro'); ?>: <?php if(defined('ICL_LANGUAGE_CODE')) { ?> 
														<?php if(ICL_LANGUAGE_CODE=='bg') { ?>
															0879 278 247<br>	
														<?php } else { ?>	
															+ 359 879 278 247<br>
														<?php } ?>
															<span class="info-helper"><?php _e('or simply select after the code the symbols "artbg7"','pro'); ?></span>
														<?php } ?>
														</span>
														</address>
														<address>
														<i class="fa fa-envelope-o"></i>
														<span class="info emails">admin@artbulgaria.com  <br/>advertising@artbulgaria.com</span>
														</address>
														</div>
												</div>
												<div id="contact-right"  class="col-md-9 col-sm-9 col-xs-12">
												<div class="well"><?php _e('Feel free to Contact Us','pro'); ?></div>
												<form id="contact-us" action="<?php echo get_permalink( $post->ID ); ?>" method="post" class="">
													<input type="hidden" id="wp_uf_nonce_register" name="wp_uf_nonce_register" value="eba7e57778"><input type="hidden" name="_wp_http_referer" value="/user-register/">
															<div class="row">
															<div class="col-md-4 col-sm-4 col-xs-12">
																<div class="form-group">
																	<label class="form-label" for="user_login"><span class="required">*</span> <?php _e('Name','pro'); ?></label>
																	<input type="text" name="con_name" id="user_login" class="form-control">
																</div>
																	<div class="form-group">
																	<label class="form-label" for="user_login"><span class="required">*</span> <?php _e('Email','pro'); ?></label>
																	<input type="text" name="con_email" id="user_login" class="form-control">
																</div>
																<div class="form-group">
																	<label class="form-label" for="user_login"><?php _e('Website','pro'); ?></label>
																	<input type="text" name="con_website" id="user_login" class="form-control">
																</div>															
															</div>
															<div class="col-md-8 col-sm-8 col-xs-12">
																<div class="form-group">
																	<label class="form-label" for="con_message"><span class="required">*</span> <?php _e('Message','pro'); ?></label>
																	<textarea name="con_message" class="form-control"></textarea>
																</div>
															</div>
														</div>					
													<div class="row">													
														<div class="col-md-12 text-right">
																<button type="submit" name="consubmit" class="submit-btn"><?php _e('Send','pro'); ?></button>
														</div> 
													</div>												
												</form>
											</div>
											</div>
										</div>

							</div>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; endif; ?>	
</div>	
<?php get_template_part('footer', 'widget');
get_footer(); ?>