<?php
// **********************************************************
// If you want to have an own template for this action
// just copy this file into your current theme folder
// and change the markup as you want to
// **********************************************************
if ( is_user_logged_in() ) {
	wp_safe_redirect( home_url( '/user-profile/' ) );
	exit;
}
?>
<?php get_header(); ?>
	<!-- Page -->
	<div id="page-wrapper">
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						
							<div class="page-heading">	
								<h1><span><?php _e( 'Reset your password?', UF_TEXTDOMAIN ); ?></h1>
							</div>
							
							
							<div class="clear padding10"></div>
								<div class="page-content">
									<div class="row-fluid">	
									
<?php $message_text = apply_filters( 'uf_reset_password_messages', isset( $_GET[ 'message' ] ) ? $_GET[ 'message' ] : '' ); 

																	if(!empty($message_text)){ ?>
											<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button> <?php echo strip_tags($message_text); ?></div>
										<?php } ?>										
										<h4><?php _e( 'Please enter your username, your key and your new password.', UF_TEXTDOMAIN ) ?></h4>
										<form action="<?php echo uf_get_action_url( 'reset_password' ); ?>" method="post" class="sa-form">
											<?php wp_nonce_field( 'reset_password', 'wp_uf_nonce_reset_password' ); ?>
											
											<div class="control-group">
												<label class="control-label" for="user_login"><?php _e( 'Username:', UF_TEXTDOMAIN ); ?></label>
												<div class="controls">
													<input class="width-100 mobile-full-width" type="text" name="user_login" id="user_login" value="<?php echo isset( $_GET[ 'login' ] ) ? $_GET[ 'login' ] : ''; ?>">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="user_key"><?php _e( 'Key:', UF_TEXTDOMAIN ); ?></label>
												<div class="controls">
													<input class="width-100 mobile-full-width" type="text" name="user_key" id="user_key" value="<?php echo isset( $_GET[ 'key' ] ) ? $_GET[ 'key' ] : ''; ?>">
												</div>
											</div>	
											<div class="control-group">
												<label class="control-label" for="pass1"><?php _e( 'New Password' ); ?></label>
												<div class="controls">
													<input class="width-100 mobile-full-width" type="password" name="pass1" id="pass1" size="16" value="" autocomplete="off" />
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="pass1"><?php _e( 'Confirm Password', UF_TEXTDOMAIN ); ?></label>
												<div class="controls">
													<input class="width-100 mobile-full-width" type="password" name="pass2" id="pass2" size="16" value="" autocomplete="off" />
												</div>
											</div>	
											
												<div id="pass-strength-result"><?php _e( 'Strength indicator' ); ?></div>
												<div class="alert alert-info"><?php _e( 'Hint: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers and symbols like ! " ? $ % ^ &amp; ).' ); ?></div>
											
											
												<div class="align-left"><input class="btn btn-inverse btn-large" type="submit" name="submit" id="submit" value="<?php _e( 'Reset Password' ); ?>">
												</div>
										</form>
										
							</div>	
						</div>	
					</div>	
				</div>	
			</div>	
		</div>	
	</div>
<?php get_template_part('footer', 'widget'); ?>		
<?php get_footer(); ?>
