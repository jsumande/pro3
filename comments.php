<?php
	// Prevent load the file directly
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ( __('Please do not load this page directly. Thanks!' , 'pro') );

	// Hide comment section if the post is password protected
	if ( post_password_required() ) { ?>
		<div class="alert alert-error"><strong><?php _e('This post is password protected.', 'pro'); ?></strong><?php _e(' Enter the password to view comments.', 'pro' ); ?></div>
	<?php
		return;
	}
?>
<div id="comment-list" class="post-comments">
	<div class="comment-section">

		<!-- Display Comments -->
		<?php if ( have_comments() || comments_open() ) : ?>

			 <!-- <h3 class="comment-section-headline<?php //echo ( have_comments() ? '' : ' nocomments' ); ?>" id="comments"><?php //comments_number( __('No Comments' , 'pro') , __('One Comment' , 'pro') , __('% Comments','pro') );?></h3> -->
			<h3 class="comment-section-headline<?php echo ( have_comments() ? '' : ' nocomments' ); ?>" id="comments"><?php comments_number( __('No Comments' , 'pro') , __('One Comment' , 'pro') , __('Recent Comments','pro') );?></h3>

			<ul class="comment-section-comment-list">
				<?php wp_list_comments(array(
					'callback' => 'pro_list_comments2',
					'avatar_size' => 0
					)); ?>
			</ul>

			<?php if ( get_comment_pages_count() > 1 && get_option('page_comments') ) : ?>
				<div class="comment-section-navigation">
					<div class="pull-left"><?php previous_comments_link() ?></div>
					<div class="pull-right"><?php next_comments_link() ?></div>
				</div>
			<?php endif; ?>

		<?php elseif ( !comments_open() && !is_page() && post_type_supports( get_post_type() , 'comments' ) ) : ?>

			<p><?php _e( 'Comments are closed.' , 'pro' ); ?></p>

		<?php endif; ?>

		<!-- <div class="line-divider20"></div> -->

		<!-- Display Comment Form -->
		

	</div>
	</div><!-- /comment-section -->
		<div class="clear padding10"></div>
	<div id="comment-form" class="post-comments">
	<?php 

		$comments_args = array(
			'label_submit'=> __('Post Comment','pro')
		);	
	
	comment_form($comments_args); ?>
	</div>