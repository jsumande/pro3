<?php 
/*
Template Name: Page (Product Comparison)
 */

get_header(); ?>

	<!-- Page -->
	<div id="page-wrapper">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php
						$btnclass = mpt_load_mp_btn_color();
						$iconclass = mpt_load_whiteicon_in_btn();
						$id = get_the_ID();
						$contentbgcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_bg_color', true ));
						$contenttextcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_text_color', true ));
					?>

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">

					<div id="post-<?php echo $id; ?>" <?php post_class(); ?><?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>

							<div class="page-heading">
								<h4><span<?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>><?php the_title(); ?></span></h4>
							</div>					

							<div class="clear padding10"></div>

							<div class="page-content">

								<?php
									$content = get_the_content(); 
									if ( !empty($content) ) {
										the_content();
										echo '<div class="clear padding10"></div>';
									}
								?>

								<?php 
									global $mppf, $mppf_compare;
									$enablecompare = ( class_exists('MPPremiumFeatures') ? $mppf->load_option( $mppf->option_group . '_compareproducts_enable_compare' , 'no' ) : 'no' ); 
								?>
								<?php if ( $enablecompare == 'yes' ) : ?>
									<?php
										$hideprice = get_blog_option(1,'mpt_mp_hide_product_price');
										//$hideprice = get_option('mpt_mp_hide_product_price');
										$cr_args = array( 'showtitle' => 'no' , 'title' => '' , 'hideprice' => ( $hideprice == 'true' ? true : false ) , 'btnclass' => $mppf->get_btn_color() );
										echo ( class_exists('MPPremiumFeatures_CompareProducts') ? '<div id="mp-premium-features">' . $mppf_compare->load_compare_results( $cr_args ) . '</div>' : '' );
									?>
								<?php endif; ?>
								
								<?php edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>

							</div>

							<?php wp_link_pages(array('before' => '<p class="margin-vertical-15">' . __( 'Pages:' , 'pro' ) , 'after' => '</p>' , 'next_or_number' => 'number')); ?>

							<?php 

								$showcomments = get_post_meta( $post->ID, '_mpt_page_show_comments', true );

								if ( $showcomments == 'on') {

									echo '<div class="page-comments">';
									comments_template();
									echo '</div>';
								} 
							?>

					</div>

					<?php endwhile; endif; ?>

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>