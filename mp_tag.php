<?php get_header(); ?>

	<!-- Page -->
	<div id="page-wrapper">

		<?php
			global $wp_query;
			$termname = $wp_query->queried_object->name;
			$termslug = $wp_query->queried_object->slug;
			//$showtaxonomymenu = get_option('mpt_enable_taxonomy_menu_product_tag');
			//$taxmenuposition = get_option('mpt_taxonomy_menu_product_tag_position');
			//$showsidebar = get_option('mpt_enable_sidebar_taxonomy_page');
			$selected_sidebar = mpt_load_selected_sidebar('taxonomypage');
			//$count = get_option('mpt_taxonomy_menu_product_tag_count');
			
			$showtaxonomymenu =get_blog_option(1,'mpt_enable_taxonomy_menu_product_tag');
			$taxmenuposition = get_blog_option(1,'mpt_taxonomy_menu_product_tag_position');
			$showsidebar = get_blog_option(1,'mpt_enable_sidebar_taxonomy_page');	
 			$count = get_blog_option(1,'mpt_taxonomy_menu_product_tag_count');
		?>

		<div class="content-section">
			<div class="outercontainer">
				<div class="container" style="min-height: 450px;">

					<div class="row-fluid">

					<?php if ( $showtaxonomymenu == 'true' || $showsidebar == 'true' ) { ?>	

						<?php if ($taxmenuposition == 'Left') { ?> 

							<!-- Product Taxonomy Menu -->

							<div id="sidebar" class="span4">

								<?php 
									echo ( $showtaxonomymenu == 'true' ? '<div class="well">' . pro_load_product_taxonomy_menu( array( 'echo' => false , 'termslug' => $termslug , 'taxonomy' => 'product_tag' , 'count' => $count ) ) . '</div>' : '' );

									if ( $showsidebar == 'true' )
										get_sidebar( $selected_sidebar );
								?>

							</div><!-- / span4 -->

						<?php } ?>

						<!-- Product Listing -->

						<div class="span8">

					<?php } else { ?>

						<div class="span12">

					<?php } ?>				

							<div class="page-heading">

								<h4><span><?php _e('Items tagged with ' , 'pro'); ?>&#8216;<?php echo $termname; ?>&#8217;</span></h4>		

							</div>	

							<div class="clear padding10"></div>	

							<?php //do_action('pro_tag_page' , $termslug , 'tag'); ?>
							<?php echo do_shortcode('[toprank_mpdgfilters tag="'.$termname.'|false"]'); ?>

						</div><!-- / span8 --> 

					<?php if ( ( $showtaxonomymenu == 'true'|| $showsidebar == 'true' ) && $taxmenuposition == 'Right') { ?>	

						<!-- Product Taxonomy Menu -->

						<div id="sidebar" class="span4">

							<?php 
								echo ( $showtaxonomymenu == 'true' ? '<div class="well">' . pro_load_product_taxonomy_menu( array( 'echo' => false , 'termslug' => $termslug , 'taxonomy' => 'product_tag' , 'count' => $count ) ) . '</div>' : '' );

								if ( $showsidebar == 'true' )
									get_sidebar( $selected_sidebar );
							?>	

						</div><!-- / span4 -->

					<?php } ?>

					</div><!-- / row-fluid -->

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>