<?php
// **********************************************************
// If you want to have an own template for this action
// just copy this file into your current theme folder
// and change the markup as you want to
// **********************************************************

if(isset($_GET['action']) && $_GET['action'] == 'bp-spam' && isset($_GET['reauth']) && $_GET['reauth']=='1') {
	$message_spam = '<div class="alert alert-error"  data-dismiss="alert"  title="Click to dismiss"><i class="fa fa-info-circle"></i> '.__('<strong>ERROR</strong>: Your account has been marked as a spammer.','pro').'</div>';
} else {
	$message_spam = '';
	
	if ( is_user_logged_in() ) {
		wp_safe_redirect( home_url( '/user-profile/' ) );
		exit;
	}
}	

global $captcha_instance;

$word = $captcha_instance->generate_random_word();
$prefix = mt_rand();
$cimage = $captcha_instance->generate_image( $prefix, $word );

$errorcode = $userErrorInput = $userErrorMsg = $errmsg = $passErrorInput = $passErrorMsg = $capErrorInput = $capErrorMsg = '';
if(isset($_SESSION['logcode'])) {
	$errorcode = $_SESSION['logcode'];
}

add_action('tp_custom_footer_script','add_fb_script_footer');

function add_fb_script_footer(){ ?>
	<script type="text/javascript">
	(function ($) {
	$(function () {
		$("#login_fb img").on("click", function () {
			FB.login(function(response) {
				if (response.authResponse) {
					_wdfb_notifyAndRedirect();
				}
			});
		});
	});
	})(jQuery);
	</script>	
<?php }
get_header(); ?>
	<!-- Page -->
	<div id="page-wrapper">
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>						
							<div class="clear padding10"></div>
								<div class="login-page">
								<div class="col-md-12">								
								<div class="page-content">
									<div class="row">																
										<form action="<?php echo uf_get_action_url( 'login' ); ?>" method="post" class="login-page-form sa-form" autocomplete="off">	
											<?php echo apply_filters( 'login_form_top', '', uf_login_form_args() ); ?>
											<?php wp_nonce_field( 'login', 'wp_uf_nonce_login' ); ?>
											<?php echo isset( $_GET[ 'redirect_to' ] ) ? '<input type="hidden" name="redirect_to" value="' . esc_url( $_GET[ 'redirect_to' ] ) . '">' : ''; ?>
											
											<?php echo apply_filters( 'uf_login_messages', isset( $_SESSION['regecode'] ) ? $_SESSION['regecode'] : '' ); 
												  echo $message_spam;
											?>				
											
												<div class="form-group">
													<label class="control-label" for="user_login"><?php _e( 'Username or E-mail', 'pro' ); ?>:</label>
													<?php 
														if(isset($errorcode['user_name_no_data']) || isset($errorcode['user_name_incorrect'])) {
															if(isset($errorcode['user_name_no_data']))
																$errmsg = __( 'Username / E-mail is empty!', 'pro' );
															else 
																$errmsg = __( 'Username / E-mail is incorrect!', 'pro' );
															
															$userErrorInput = ' input-border-error';
															$userErrorMsg = '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errmsg.'</div>';
														}
													?>														
													<div class="controls">
														<?php echo $userErrorMsg; ?> 
														<input class="form-control<?php echo $userErrorInput; ?>" type="text" name="user_login" id="user_login" autocomplete="off" value="<?php echo isset($_SESSION['slog_username'])? $_SESSION['slog_username'] : ''; ?>"/> 
													</div>
												</div>
																							
											<div class="form-group">
												<label class="control-label" for="user_pass"><?php _e( 'Password', 'pro' ); ?>:</label>
													<?php 
														if(isset($errorcode['user_pass_no_data']) || isset($errorcode['user_pass_incorrect'])) {
															
															if(isset($errorcode['user_pass_no_data']))
																$errmsg = __( 'Password is empty!', 'pro' );
															else 
																$errmsg = __( 'Password is incorrect!', 'pro' );															
															
															$passErrorInput = ' input-border-error';
															$passErrorMsg = '<div style="margin-top: -15px;margin-bottom: 0px" class="alert alert-error">'.$errmsg.'</div>';
														}
													?>												
												<div class="controls">
												    <?php echo $passErrorMsg; ?> 
													<input class="form-control<?php echo $passErrorInput; ?>" type="password" name="user_pass" id="user_pass" autocomplete="off" value="<?php echo isset($_SESSION['slog_password'])? $_SESSION['slog_password'] : ''; ?>"/>
												</div>
											</div>	
											<div class="form-group form-group-emails">
												<label class="control-label" for="user_username"><?php _e( 'User Username', 'pro' ); ?>:</label>																
												<div class="controls">												
													<input class="form-control" type="text" name="user_username" id="user_username" autocomplete="off"/> 
												</div>
											</div>												
											<div class="form-group">
												<img src="<?php echo esc_url( home_url( '/wp-content/plugins/really-simple-captcha/tmp/'.$cimage ) );?>"/>
												<label class="form-label" for="ccode"><?php _e( 'Enter Captcha', 'pro' ); ?>:</label>
													<?php 
														if(isset($errorcode['user_cap_no_data']) || isset($errorcode['user_cap_incorrect'])) {
															
															if(isset($errorcode['user_cap_no_data']))
																$errmsg = __( 'Captcha is empty!', 'pro' );
															else 
																$errmsg = __( 'You entered a wrong captcha!', 'pro' );															
															$capErrorInput = ' input-border-error';
															$capErrorMsg = '<div style="margin-top: -15px; margin-bottom: 0px" class="alert alert-error" data-dismiss="alert">'.$errmsg.'</div>';
														}
													?>
 												<?php echo $capErrorMsg; ?> 	
												<div class="controls">
													<input class="form-control<?php echo $capErrorInput; ?>" type="text" name="ccode" id="ccode">
													<input type="hidden" value="<?php echo $prefix; ?>" name="pcode">
												</div>
												
											</div>												
											<?php echo apply_filters( 'login_form_middle', '', uf_login_form_args() ); ?>
											<div class="login-checkbox-remember">
												<div class="alignleft">
													<p class="login-forgot-pass">
														<a href="<?php echo get_site_url(1).'/user-forgot-password/'; ?>"><?php _e( 'Forgot Password?', 'pro' ); ?></a>
													</p>												
													<div class="form-group">
													  <label class="control-label" for="remembermes"></label>
														<div class="input-control-checkbox">															
																<input checked id="checkbox2" type="checkbox" name="checkbox" value="2"><label for="checkbox2"><?php _e('Remember?','pro'); ?></label>
														</div>
													</div>												
												</div>
												<div class="user-login-normal alignright">
													<input class="login-button" type="submit" name="submit" id="submit" value="<?php _e( 'Sign in', 'pro' ); ?>">
												</div>													
												<div class="clear"></div>	
											</div>
											<div class="form-group">
												<p class="login-form-policy text-center" style="color: #a0a0a0; font-size: 0.967em;"><?php _e('Sign in I accept the Terms of use & Private policy','pro'); ?></p>	
												<div class="clear"></div>
												<div class="login-form-buttons">
													<div class="user-login-facebook" style="text-align:center;border-top:1px solid #E0E0E0;padding-top:20px">
													<?php //echo apply_filters( 'login_form_bottom', '', uf_login_form_args() ); ?>
													<?php // echo do_shortcode('[wdfb_connect]Login with Facebook[/wdfb_connect]'); ?>
													<div id='login_fb'><?php if(ICL_LANGUAGE_CODE == 'bg') { ?><img src="<?php echo get_template_directory_uri(); ?>/img/facebookbg.png" /><?php } else { ?> <img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" /><?php } ?></div>
													</div>												
												</div>
											</div>
										</form>
							</div>	
						</div>	
						<p class="login-footer-text">
						<?php _e('Not a registered user yet?','pro'); ?> <br /><a href="<?php echo get_site_url(1).'/user-register/'; ?>"><?php _e( 'Register now <strong>EASY</strong> and <strong>FREE</strong>!', 'pro' ); ?></a></p>
						</div>
						</div>	
					</div>	
				</div>	
			</div>	
		</div>	
	</div>
<?php uf_notification_unset(); ?>
<?php get_template_part('footer', 'widget'); ?>
<?php get_footer(); ?>