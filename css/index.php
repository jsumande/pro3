<?php get_header(); ?>

	<!-- Slider -->

	<?php
		/*
		if ( class_exists('KickAssSlider') && class_exists('RevSliderFront') ) {
			//$sliderselected = get_option('mpt_homepage_selected_slider');
			//$revslideralias = esc_attr(get_option('mpt_rev_slider_alias'));
			//$kasliderid = esc_attr(get_option('mpt_kaslider_id'));
			
			$sliderselected = get_blog_option(1,'mpt_homepage_selected_slider');
			$revslideralias = esc_attr(get_blog_option(1,'mpt_rev_slider_alias'));
			$kasliderid = esc_attr(get_blog_option(1,'mpt_kaslider_id'));			

			if ( $sliderselected == 'Revolution Slider' && !empty($revslideralias) ) {
				echo '<div id="slider-wrapper">';
					putRevSlider( $revslideralias ); 
				echo '</div>';
			} elseif ( $sliderselected == 'KickAss Slider' && !empty($kasliderid) ) {
				echo do_shortcode('[kickass-slider id="'.$kasliderid.'"]');
			}

		} elseif ( class_exists('KickAssSlider') && !class_exists('RevSliderFront') ) {

			//$kasliderselected = get_option('mpt_enable_kaslider');
			//$kasliderid = esc_attr(get_option('mpt_kaslider_id'));
			
			$kasliderselected = get_blog_option(1,'mpt_enable_kaslider');
			$kasliderid = esc_attr(get_blog_option(1,'mpt_kaslider_id'));			

			if ( $kasliderselected == 'true' && !empty($kasliderid) )
				echo do_shortcode('[kickass-slider id="'.$kasliderid.'"]'); 

		} elseif ( class_exists('RevSliderFront') && !class_exists('KickAssSlider') ) {

			//$revsliderselected = get_option('mpt_enable_rev_slider');
			//$revslideralias = esc_attr(get_option('mpt_rev_slider_alias'));
			
			$revsliderselected = get_blog_option(1,'mpt_enable_rev_slider');
			$revslideralias = esc_attr(get_blog_option(1,'mpt_rev_slider_alias'));			

			if ($revsliderselected == 'true' && !empty($revslideralias)) {
				echo '<div id="slider-wrapper">';
					if ( function_exists('putRevSlider') ) 
						putRevSlider( $revslideralias ); 
				echo '</div>';
			}

		}		
		*/
		if(function_exists('Featured_Store_Slider') && is_main_site()){ ?>
			<div id="slider-wrapper">
			<div class="index-blog-headings">
				<h3><?php _e('FEATURED STORES '); ?></h3>
				<h5><?php _e('Our Selection of Outstanding Stores'); ?></h5>
				<a class="index-blog-headings-more" href="#"><?php _e('View All Stores'); ?> <i class="fa fa-chevron-right"></i></a>
			</div>			
			<?php Featured_Store_Slider(); ?>
			</div>
		<?php } 
		
		if(function_exists('Tp_Featured_Product_Slider') && is_main_site()){ ?>
			<div id="slider-wrapper">
			<div class="index-blog-headings">
				<h3><?php _e('Featured Products'); ?></h3>
				<h5><?php _e('best and recommended products in artbulgaria'); ?></h5>
				<!--<a class="index-blog-headings-more" href="#"><?php _e('View All Stores'); ?> <i class="fa fa-chevron-right"></i></a>-->
			</div>			
			<?php Tp_Featured_Product_Slider(); ?>
			</div>		
		<?php } ?>
	<!-- End Slider -->

	<!-- Homepage Content -->

	<div id="homepage-content-wrapper">
		<div class="outercontainer">
			<div class="container">
				<div class="row-fluid">
					<div id="sidebar" class="span3">
						<?php //the_widget( 'MarketPress_Global_Category_List_Widget','title=Product Categories','before_widget=<div class="well sidebar-widget-well">&after_widget=</div><div class="clear"></div>&before_title=<h4 class="page-header"><span>&after_title=</span></h4>' ); ?> 
						
						<?php echo '<div class="well">' . pro_load_global_product_taxonomy_menu( array( 'echo' => false , 'termslug' => '' , 'taxonomy' => 'product_category' , 'count' => '0' ) ) . '</div>';
						?>
					</div>
					<div class="span9">
				<?php 

					$homepagetemplateid = esc_attr(get_option('mpt_homepage_layout_code'));
					//$homepagetemplateid = esc_attr(get_blog_option(1,'mpt_homepage_layout_code'));

					if ( !empty($homepagetemplateid) && is_numeric($homepagetemplateid) && class_exists('AQ_Page_Builder') ) { 

						echo do_shortcode('[template id="'.$homepagetemplateid.'"]'); 

					}  else {
						if ( class_exists( 'MarketPress' ) ) {
							do_action('pro_product_listing_page' , 'productlistingpage' , 'list');
						}
					} 

				?>
					</div><!-- / span8 -->
				<?php  if(is_main_site()){ ?>
					<div class="clear"></div>
					<div class="">
						<hr class="index-blog">
						<div class="index-blog-headings">
							<h3><?php _e('NEWS AND STORIES'); ?></h3>
							<h5><?php _e('From our Blog'); ?></h5>
							<a class="index-blog-headings-more" href="<?php echo get_page_link(637); ?>"><?php _e('MORE STORIES'); ?> <i class="fa fa-chevron-right"></i></a>
						</div>
						<div id="index-blogs-con">
							<?php $the_query = new WP_Query( 'showposts=10' ); 
								$loopcount = 0;
								$loopcolumn = 2;
							?>
								<?php while ($the_query -> have_posts()) : $the_query -> the_post(); $loopcount++;?>
								
								<?php if (0 == ( $loopcount - 1 ) % $loopcolumn || 1 == $loopcolumn ) { ?>
									<div class="index-blogs-slide row-fluid">
								<?php } ?>
								
									<div class="index-blogs-post span6 well <?php if ( 0 == $loopcount % $loopcolumn) echo 'art'; ?>">
										<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
										<?php echo wp_trim_words(get_the_content() , 35,'... <br><a class="index-blogs-readmore" href="'.get_the_permalink().'">'.__('Read the full story').'</a>' ); ?>
											<?php if ( has_post_thumbnail( $the_query->ID ) ) { 
											$image = wp_get_attachment_image_src( get_post_thumbnail_id( $the_query->ID ), 'large' );
											
											?>
											<a class="blog-image-cover" href="<?php the_permalink(); ?>" style="background-image: url(<?php echo $image[0]; ?>);"></a>
											
										<?php } ?>
									</div>	
								<?php if ( 0 == $loopcount % $loopcolumn) { ?>
									</div>
								<?php } ?>	
							<?php endwhile;?>	
								<?php if ($loopcount % $loopcolumn != 0) { //add fix if not?>
									</div>
								<?php } ?>	
								
							<div id="index_blogs_slider_next" class="index_blogs_next index_blogs_navi"><i class="fa fa-angle-right"></i></div>
							<div id="index_blogs_slider_prev" class="index_blogs_prev index_blogs_navi"><i class="fa fa-angle-left"></i></div>		
						</div>
					</div>
					
				<?php  } ?>
					
				</div><!-- / row-fluid -->
			</div><!-- / container -->
		</div><!-- / outercontainer -->	
	</div><!-- / homepage-content-wrapper -->

	<!-- End Homapage Content -->

	<!-- Footer Widget -->

	<?php 
		//$selected = get_option('mpt_enable_homepage_footer_widget');
		$selected = get_blog_option(1,'mpt_enable_homepage_footer_widget');

		if ($selected) {
			get_template_part('footer', 'widget'); 
		}
		
	?>

	<!-- End Footer Widget -->

<?php get_footer(); ?>