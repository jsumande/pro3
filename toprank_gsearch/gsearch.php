<?php

function render_g_search() {

    // Menu Items
    //----------------------------------------------------------------

    $menu_items = array(
        'gs_all' => __('All','pro'),
        '--' => '--',
        'gs_products' => __('Products','pro'),
		'gs_events' => __('Events/Gigs','pro'),
        'gs_performers' => __('Performers','pro'),
        'gs_stores' => __('Stores','pro'),
        'gs_blogs' => __('Blogs','pro'),
        'gs_forum' => __('Forum','pro')
    );

    // Templates
    //----------------------------------------------------------------

    $menu_items_html = implode('', array_map(function($v, $k) {
        if ($k == '--') {
            return "<li class='divider'></li>";
        } else {
            return "<li><a href='#' data-ref='{$k}'>" . $v . "</a></li>";
        }
    }, $menu_items, array_keys($menu_items)));

    $tpl = array();
    $tpl[] = "<div id='tp_global_search' class='col-md-12 col-sm-12 col-xs-12'>";
    $tpl[] = "    <div class='input-group'>";
    $tpl[] = "        <div class='input-group-btn'>";
    $tpl[] = "            <button data-ref='" . array_keys($menu_items)[0] . "' type='button' class='btn btn-primary " .
                    "dropdown-toggle' data-toggle='dropdown' aria-expanded='false'><span>".__('All','pro')."</span>&nbsp;&nbsp;" .
                    "<span class='caret'></span></button>";
    $tpl[] = "            <ul class='dropdown-menu' role='menu'>" . $menu_items_html . "</ul>";
    $tpl[] = "        </div>";
    $tpl[] = "        <input type='text' class='form-control'/>";
    $tpl[] = "        <span class='input-group-btn'>";
    $tpl[] = "            <button type='button' class='btn btn-default'><i class='fa fa-search'></i></button>";
    $tpl[] = "        </span>";
    $tpl[] = "    </div>";
    $tpl[] = "</div>";

    $bs_searchbox = implode('', array_map(function($t) { return trim($t); }, $tpl));

    return $bs_searchbox;
}