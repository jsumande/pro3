jQuery(document).ready(function ($) {
    //-----------------------------------
    // @kevs - 03-13-2015 (Friday 13th)
    //-----------------------------------

    // Control Mapping
    var g_search_control = $('#tp_global_search');
    var g_search_dd_btn = g_search_control.find('button.btn-primary');
    var g_search_dd_items = g_search_control.find('.dropdown-menu > li > a');
    var g_search_btn = g_search_control.find('button.btn-default');
    var g_search_txt = g_search_control.find('input[type=text]');

    // Currents
    var _c_dref = g_search_dd_btn.attr('data-ref'),
        _c_dtxt = g_search_dd_btn.text(),
        _r_prevtxt = "",
        _r_response = false,
        _r_error = false;

    // Events for search text
    g_search_txt.keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            g_search_btn.trigger('click');
        } else if (code == 27) {
            g_search_txt.popover('hide');
        } else if (code == 40) {
            if (!_isvisible()) {
                g_search_txt.popover('show');
            }
        }
    });

    // Events for search Button
    g_search_btn.click(function (e) {
        var _c_srch = $(g_search_txt).val().trim();
        if (_c_srch.length > 0) do_search(_c_srch);
    });

    // Events for dropdown items
    g_search_dd_items.each(function (i, m) {
        var btn = $(m);
        btn.click(function (e) {
            e.preventDefault();

            // Set current -- for ajax references
            _c_dref = $(this).attr('data-ref');
            _c_dtxt = $(this).text();

            // assign dom attributes -- in-case needed elsewhere as reference..
            g_search_dd_btn.attr('data-ref', _c_dref);
            g_search_dd_btn.find('>span:first-child').text(_c_dtxt);
        });
    });

    // Attach a popover to textfield
    g_search_txt.popover({
        placement: 'bottom',
        html: true,
        trigger: 'manual',
        container: 'body',
        template: '<div class="popover tp-gsearch-popover"><div class="arrow"></div><div class="popover-content"></div></div>',
        content: function () {
            return '<i class="fa fa-refresh fa-spin"></i>';
        }
    }).on('shown.bs.popover', function () {
        var popover = _getpopover().popover;
        var popover_content = _getpopover().popover_content;

        popover.css('left', (g_search_txt.offset().left) + 'px');
        popover_content.css('width', g_search_txt.width() + 10);

        if (_r_error === true) {
            popover_content.html(_render_error());
        } else {
            if (!_r_response) {
                popover_content.html(_render_no_items());
            } else {
                popover_content.html(_render_items());
            }
        }
    }).on('click', function () {
        if (g_search_txt.hasClass('disabled')) return;
        var popover = $('.tp-gsearch-popover');
        g_search_txt.popover('toggle');
    }).on('blur', function () {
        if (g_search_txt.hasClass('disabled')) return;
        g_search_txt.popover('hide');
    });

    $('body').resize(function () {
        g_search_txt.blur();
    });

    function _isvisible() {
        var val = g_search_txt.attr('aria-describedby');
        return (typeof val != 'undefined');
    }

    /**
     * @returns {{popover: (*|jQuery|HTMLElement), popover_content: *}}
     * @private
     */
    function _getpopover() {
        var popover = $('.tp-gsearch-popover');
        var popover_content = popover.find(' > .popover-content');

        return {
            'popover': popover,
            'popover_content': popover_content
        };
    }


    /**
     * @returns {string}
     * @private
     */
    function _render_items() {
        if (('data' in _r_response) && _r_response['data'].length > 0) {
            return _r_response.data;
        } else {
            _getpopover().popover_content.html(_render_no_items(_r_prevtxt));
        }
    }

    /**
     * @param keyword
     * @returns {string}
     * @private
     */
    function _render_no_items(keyword) {
        return '<div class="alert alert-info" role="alert">' +
            '<strong>' +SliderVal.gsno + (keyword ? (' '+SliderVal.gsfor+' `' + keyword + '`') : '') +
            '.</strong> '+ SliderVal.gstype +'</div>';
    }

    /**
     * @returns {string}
     * @private
     */
    function _render_error() {
        return '<div class="alert alert-danger" role="alert">' +
            '<strong>' + GsearchVal.gsoops + '</strong> ' + _r_response.responseText + '</div>';
    }

    /**
     * @param state
     * @private
     */
    function _setLoadState(state) {
        var controls = [g_search_btn, g_search_txt, g_search_dd_btn];

        $(controls).each(function (i, c) {
            if (state === true) {
                c.attr('disabled', true);
                c.addClass('disabled');
                g_search_btn.find('>i.fa').removeClass('fa-search').addClass('fa-refresh fa-spin');
            } else {
                c.removeAttr('disabled');
                c.removeClass('disabled');
                g_search_btn.find('>i.fa').addClass('fa-search').removeClass('fa-refresh fa-spin');
            }
        });
    }

    /**
     * @param search_phrase
     */
    function do_search(search_phrase) {
        var r = window.svc('gsearch', [search_phrase, _c_dref], {
            beforeSend: function () {
                g_search_txt.popover('hide');
                _r_prevtxt = search_phrase;
                _setLoadState(true);
            }
        });

        r.always(function () {
            _setLoadState(false);
        });

        r.fail(function (res) {
            _r_error = true;
            _r_response = res;
            g_search_txt.popover('show');
        });

        r.success(function (red) {
            _r_error = false;
            _r_response = red;
            g_search_txt.popover('show');
            g_search_txt.focus();
        });
    }
});