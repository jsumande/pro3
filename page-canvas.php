<?php 
/*
Template Name: Page (canvas)
 */

get_header(); ?>

	<!-- Page -->
	<div id="page-wrapper">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php 
						$contentbgcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_bg_color', true ));
						$contenttextcolor = esc_attr(get_post_meta( $post->ID, '_mpt_page_content_text_color', true ));
					?>

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">

					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?><?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>

							<?php $showpagetitle = get_post_meta( $post->ID, '_mpt_canvas_show_page_title', true );  ?>
							<?php if ( $showpagetitle == 'on') {  ?>
								<div class="page-heading">
									<h4><span<?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>><?php the_title(); ?></span></h4>
								</div>
							
								<div class="clear padding25"></div>

							<?php } ?>

								<?php the_content(); ?>

								<?php // edit_post_link( __('Edit this entry.','pro') , '<div class="clear"></div><p class="margin-vertical-15">', '</p>'); ?>

							<?php wp_link_pages(array('before' => '<p class="margin-vertical-15">' . __( 'Pages:' , 'pro' ) , 'after' => '</p>' , 'next_or_number' => 'number')); ?>

							<?php 

								$showcomments = get_post_meta( $post->ID, '_mpt_page_show_comments', true );

								if ( $showcomments == 'on') {

									echo '<div class="page-comments">';
									comments_template();
									echo '</div>';
								} 
							?>

					</div>

					<?php endwhile; endif; ?>

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>