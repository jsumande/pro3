<?php
// **********************************************************
// If you want to have an own template for this action
// just copy this file into your current theme folder
// and change the markup as you want to
// **********************************************************
if ( is_user_logged_in() ) {
	wp_safe_redirect( get_bloginfo( 'url' ) . '/user-profile/' );
	exit;
}
$errorcode = $userErrorInput = $userErrorMsg = $errmsg = $passErrorInput = $passErrorMsg = $emailErrorInput = $emailErrorMsg = '';
if(isset($_SESSION['regecode'])) {
	$errorcode = $_SESSION['regecode'];
	$errmsg = $_SESSION['regmsg'];
}	
get_header(); ?>
	<div id="page-wrapper">
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="clear padding10"></div>
								
								<div class="page-content">									
										<div class="user-register-page-inner">
											<div class="page-heading">	
												<h1 class="text-center"><span><?php _e( 'Register', UF_TEXTDOMAIN ); ?></h1>
											</div>
											<form action="<?php echo uf_get_action_url( 'register' ); ?>" method="post" class="sa-form" autocomplete="off">
											
												<?php wp_nonce_field( 'register', 'wp_uf_nonce_register' ); ?>
												<div class="row">
													<div class="col-md-6 form-group">
															<label class="" for="user_login"><span class="required">*</span><?php _e( 'Username' ); ?></label>
																<?php 
																	if($errorcode == "user_name") {
																		$userErrorInput = 'input-border-error';
																		$userErrorMsg = '<div class="alert alert-error">'.$errmsg.'</div>';
																	}
																?>															
																<input title="<?php _e( 'Must be at least 4 characters, letters and number only.' ); ?>" type="text" name="user_login" id="user_login" class="form-control <?php echo $userErrorInput; ?>" placeholder="<?php _e( 'Usernames cannot be changed.' ); ?>" value="<?php echo $_POST['user_login'];?>"/>
																<?php //<p class="help-block description"><?php  _e( 'Usernames cannot be changed.' ); ?><!--</p> -->
																<?php echo $userErrorMsg; ?> 
													</div>
													
													<div class="col-md-6 form-group">
															<label class="" for="email"><span class="required">*</span><?php _e( 'E-mail' ); ?></label>
																<?php 
																	if($errorcode == "user_email") {
																		$emailErrorInput = 'input-border-error';
																		$emailErrorMsg = '<div class="alert alert-error">'.$errmsg.'</div>';
																	}
																?>																	
															<input type="text" name="zxbasdk" id="email" class="form-control <?php echo $emailErrorInput;?>" value="<?php echo $_POST['zxbasdk'];?>"/>
															<?php echo $emailErrorMsg; ?> 
													</div>
																									
												</div>	
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
																<?php 
																	if($errorcode == "password_1") {
																		$passErrorInput = 'input-border-error';
																		$passErrorMsg = '<div class="alert alert-error">'.$errmsg.'</div>';
																	}
																?>															
															<label class="control-label" for="password"><?php _e('Password', 'signup_password'); ?>:</label>
																<input class="form-control <?php echo $passErrorInput; ?>" name="password_1" type="password" id="password_1" value="" autocomplete="off" maxlength="20" placeholder="<?php _e('Leave fields blank for a random password to be generated.', 'pro'); ?>"/>
																<?php echo $passErrorMsg; ?> 
														</div>	
													</div>
													<div class="col-md-6">
														<div class="form-group">
																<?php 
																	if($errorcode == "password_1") {
																		$passErrorInput = 'input-border-error';
																		$passErrorMsg = '<div class="alert alert-error">'.$errmsg.'</div>';
																	}
																?>															
															<label class="control-label" for="password"><?php _e('Confirm Password', 'signup_password'); ?>:</label>
																<input class="form-control  <?php echo $passErrorInput; ?>" name="password_2" type="password" id="password_2" value="" autocomplete="off" maxlength="20" placeholder="<?php _e('Type your new password again.', 'pro'); ?>"/>
																<?php echo $passErrorMsg; ?> 
														</div>	
													</div>	
												</div>	
												<div class="row">	
													<div class="col-md-6">
														<div class="form-group">								
															<label class="control-label" for="password"><?php _e('Preferred Language', 'pro'); ?>:</label>
														</div>	
													</div>	
													<div class="col-md-6">
														<div class="form-group">								
															<?php $language = isset($_POST['tp_language'])?  $_POST['tp_language'] : 'en'; ?>
															<select class="form-control" name="tp_language">	
																<option value="en"<?php echo $language == 'en' ? ' selected' : ''; ?>><?php _e('English'); ?></option>
																<option value="bg"<?php echo $language == 'bg' ? ' selected' : ''; ?>><?php _e('Bulgarian'); ?></option>
															</select>
														</div>	
													</div>													
												</div>													

												<?php
													if( isset($_GET['message']) && $_GET['message'] == "password_1") {
												?>
													<script type="text/javascript">
														$(document).ready(function(){
															$("#password_1").addClass('input-border-error');
															$("#password_2").addClass('input-border-error');
															$("[for='password']").parent().append('<div class="alert alert-error">Password not match.</div>');
							
														});
													</script>
												<?php		
													}
												?>
												
												<div class="row">
												<div class="register-store-buttons">
													<div class="col-md-6 text-left">
														<div class="row-fluid facebookbtn">
														<div class="col-md-8 col-sm-12 container-buttons">
														<iframe id="facebookgenFrame" style="">
												
														</iframe>
															<div class="user-login-facebook">
																	<i class="fa fa-facebook"></i>
																	<span><?php _e('Register with Facebook', UF_TEXTDOMAIN) ?>
</span>
																	<?php //echo apply_filters( 'login_form_bottom', '', uf_login_form_args() ); ?>
																	<div style="display:none;">
																	<?php echo do_shortcode('[wdfb_connect size="xlarge"]<span>Register with Facebook &nbsp;&nbsp;&nbsp;&nbsp; asassas</span>[/wdfb_connect]'); ?>
																	</div>

															</div>
														</div>
														</div>
													</div>
													<div class="col-md-6 form-group form-group-emails">
															<label class="" for="name"><span class="required">*</span><?php _e( 'Re-enter E-mail' ); ?></label>																	
															<input type="text" name="email" id="name" class="form-control" autocomplete="off"/>
													</div>														
													<div class="col-md-6 user-login-normal text-right">	
														<div class="row-fluid">	
															<div class="col-md-7 col-sm-12"></div>
															<div class="col-md-5 col-sm-12 container-buttons">
																<input type="hidden" name="zcqweopasm" value="<?php echo uf_generate_password(); ?>"/>
																<input type="submit" name="submit" id="submit" value="<?php _e( 'Register', UF_TEXTDOMAIN ); ?>" class="login-button login-button-x">																													
															</div>
														</div>
													</div>														
												</div> 
												</div> 												
											</form>
											<form id="facebookgen" action="">
												<input type="submit">
											</form>

											<script type="text/javascript">
											jQuery(document).ready(function($){
												setTimeout(	function() {
														var source = $(".user-login-facebook iframe").attr("src");
														$("#facebookgenFrame").attr("src", source);
													}, 1000);
											});
											</script>
										</div>	
						</div>	
					</div>	
				</div>	
			</div>	
		</div>	
	</div>
<?php uf_notification_unset(); ?>	
<?php get_template_part('footer', 'widget'); ?>	
<?php get_footer(); ?>
