<?php

/// User Signup Function

function cs_generate_password( $length = 12, $special_chars = true, $extra_special_chars = false ) {
	$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	if ( $special_chars )
		$chars .= '!@#$%^&*()';
	if ( $extra_special_chars )
		$chars .= '-_ []{}<>~`+=,.;:/?|';

	$password = '';
	for ( $i = 0; $i < $length; $i++ ) {
		$password .= substr($chars, wp_rand(0, strlen($chars) - 1), 1);
	}
	return $password;
}

function create_store_urlfix($vars){
	global $cur_url;
	if(strstr($cur_url,"?"))
		$str_lang = '&'.$vars;
	else
		$str_lang = '?'.$vars;	
	
	return $str_lang;
}	

function validate_user_signup() {
	global $cur_url;
	$result = validate_user_form();
	extract($result);	
	if ( $errors->get_error_code() ) {
		signup_user($user_name, $user_email, $errors);
		return false;
	}
	if ( 'blog' == $_POST['signup_for'] ) {
	
		wp_safe_redirect($cur_url.create_store_urlfix('step=signup-blog-form'));
		exit;
	}
}

function save_bcat_terms_relation_store($datas = array(),$blog_id){
	global $wpdb;
	$datas = array_filter($datas);
	if(!empty($datas)){
		$datas = array_map( 'intval', $datas );
		$datas = array_unique( $datas );
		
		foreach ($datas as $data){
			
			$tax_select = "SELECT tt.term_taxonomy_id FROM {$wpdb->base_prefix}terms AS t INNER JOIN {$wpdb->base_prefix}term_taxonomy as tt ON tt.term_id = t.term_id WHERE t.term_id = %d AND tt.taxonomy = %s";
			$term_info = $wpdb->get_row( $wpdb->prepare( $tax_select, $data, 'bcat' ), ARRAY_A );
			
			if(count($term_info)){
				$result =  $wpdb->insert( $wpdb->base_prefix."term_relationships", array( 'object_id' => $blog_id, 'term_taxonomy_id' => $term_info['term_taxonomy_id'] ) );
			}
		}
	}
}

function get_bcat_oritrans_id_store($datas, $show_translated_only = false){
	
	$cat_ids = array();
	if($show_translated_only) 
		$id_translated = array();
	
	foreach($datas as $data) {
			if(defined('ICL_LANGUAGE_CODE')) {
				
				if(ICL_LANGUAGE_CODE=='en')
					$get_lang_term = 'bg';
				else 
					$get_lang_term = 'en';

				$get_origtermid = icl_object_id($data, 'bcat', false, $get_lang_term);
				
				if($get_origtermid) {
					$cat_ids[] = $get_origtermid;
					if($show_translated_only) 
						$id_translated[] = $get_origtermid;
				}

			}
			$cat_ids[] = $data;	

	}	
	
	if($show_translated_only)
		return $id_translated;
	else
		return $cat_ids;
	
}

function select_registration_data($ses_id){
	global $wpdb;
	$table_reg = $wpdb->base_prefix .'store_registration_data';
	$reg_data = $wpdb->get_row( "SELECT * FROM `{$table_reg}` WHERE `reg_session_id` = '{$ses_id}'", ARRAY_A );
	
	if($reg_data != null) {
		
		$reg_datas = array();
		$reg_datas['reg_data'] 			= $reg_data['reg_data'];
		$reg_datas['store_title'] 		= $reg_data['store_title'];
		$reg_datas['store_title_bg'] 	= $reg_data['store_title_bg'];
		$reg_datas['store_desc'] 		= $reg_data['store_desc'];
		$reg_datas['store_desc_bg'] 	= $reg_data['store_desc_bg'];
		
		return $reg_datas;
	
	} else
		return false;
}

function insert_registration_data($ses_id,$data,$store_title='',$store_title_bg='',$store_desc = '',$store_desc_bg = ''){
	global $wpdb;
	
	$table_reg = $wpdb->base_prefix .'store_registration_data';
	$wpdb->insert( $table_reg , array( 'reg_session_id' => $ses_id, 'reg_data' => $data,'store_title' => $store_title,'store_title_bg' => $store_title_bg, 'store_desc' =>  $store_desc, 'store_desc_bg' => $store_desc_bg,'reg_time' => time(),  ), array( '%s',  '%s',  '%s', ' %s', ' %s', ' %s', '%d' ) );	
	
}

function update_registration_data($ses_id, $data, $update_title = false, $store_title='', $store_title_bg='', $update_desc = false, $store_desc = '', $store_desc_bg = ''){
	global $wpdb;
	$table_reg = $wpdb->base_prefix .'store_registration_data';
	
	if($update_desc || $update_title) {
		$wpdb->update( $table_reg , array( 'reg_data' => $data, 'store_title' => $store_title, 'store_title_bg' => $store_title_bg, 'store_desc' =>  $store_desc, 'store_desc_bg' => $store_desc_bg, 'reg_time' => time()), array( 'reg_session_id' => $ses_id ), array( '%s','%s','%s','%s','%s','%d'), array( '%s' ) );
	} else {
		$wpdb->update( $table_reg , array( 'reg_data' => $data, 'reg_time' => time()), array( 'reg_session_id' => $ses_id ), array( '%s','%d'), array( '%s' ) );		
	}	
	
	
}

function delete_registration_data($ses_id){
	global $wpdb;
	$table_reg = $wpdb->base_prefix .'store_registration_data';
	
	$wpdb->delete( $table_reg, array( 'reg_session_id' => $ses_id ), array( '%s' ) );
	
	
}

function validate_user_form() {
	
	if(isset($_POST['newsletter']))
		$newsletterval = 'yes';
	else
		$newsletterval = 'no';
	
	if(isset($_POST['rememberme']))
		$rememberval = 'yes';
	else
		$rememberval = 'no';
	
	$language  = isset($_POST['tp_language'])? $_POST['tp_language'] : 'en';
	
	$use_this_data = false;	
	if(isset($_COOKIE['ssinfo'])){
		if(select_registration_data($_COOKIE['ssinfo'])) 
			$use_this_data = true;
	} 
		
	if(!$use_this_data){
	
		if((!empty($_POST['user_password_1']) && !empty($_POST['user_password_2'])) && ($_POST['user_password_1'] == $_POST['user_password_2']))
			$array = array('username' => $_POST['user_name'],'useremail' => $_POST['user_email'],'userpass' => $_POST['user_password_1'],'usernewsletter' => $newsletterval,'userrememberme' => $rememberval,'language' => $language);
		else 
			$array = array('username' => $_POST['user_name'],'useremail' => $_POST['user_email'],'userpass' => cs_generate_password(),'usernewsletter' => $newsletterval,'userrememberme' => $rememberval,'language' => $language);
		
		$userinfo = json_encode($array);
		$ses_id = md5(time().cs_generate_password());
		insert_registration_data($ses_id,$userinfo);
		setcookie('ssinfo',$ses_id);
	} else {
		$data = select_registration_data($_COOKIE['ssinfo']);
		$datas = stripslashes($data['reg_data']);
		$array = json_decode($datas, true);
		$array['username']  = $_POST['user_name'];
		$array['useremail']  = $_POST['user_email'];
		$array['usernewsletter']  = $newsletterval;
		$array['userrememberme']  = $rememberval;
		$array['language'] = $language;
		
		if((!empty($_POST['user_password_1']) && !empty($_POST['user_password_2'])) && ($_POST['user_password_1'] == $_POST['user_password_2'])) {
			$array['userpass'] = $_POST['user_password_1'];
		}
		
		$userinfo = json_encode($array);
		update_registration_data($_COOKIE['ssinfo'],$userinfo);
	}	
	
	return wpmu_validate_user_signup($_POST['user_name'], $_POST['user_email']);
}

add_filter('wpmu_validate_user_signup', 'createstore_password_filter');
function createstore_password_filter($content) {
	$password_1 = isset($_POST['user_password_1'])?$_POST['user_password_1']:'';
	$password_2 = isset($_POST['user_password_2'])?$_POST['user_password_2']:'';
	if ( !empty( $password_1 )  && $_POST['stage'] == 'validate-user-signup' ) {
		if ( $password_1 != $password_2 ) {
			$content['errors']->add('password_1', __('Passwords do not match.', 'pro'));
		}
	}
	return $content;
}

//Create Account
function signup_user($user_name = '', $user_email = '', $errors = '') {
	global $current_site, $active_signup, $cur_url;
	if ( !is_wp_error($errors) )
		$errors = new WP_Error();
	if ( isset( $_POST[ 'signup_for' ] ) )
		$signup[ esc_html( $_POST[ 'signup_for' ] ) ] = 'checked="checked"';
	else
		$signup[ 'blog' ] = 'checked="checked"';
	
	$signup['user'] = isset( $signup['user'] ) ? $signup['user'] : '';
	
	$filtered_results = apply_filters('signup_user_init', array('user_name' => $user_name, 'user_email' => $user_email, 'errors' => $errors ));
	$user_name = $filtered_results['user_name'];
	$user_email = $filtered_results['user_email'];
	$errors = $filtered_results['errors'];
				//file_put_contents(get_template_directory().'/debug.log',print_r($datas, true));
	if(isset($_COOKIE['ssinfo']) && select_registration_data($_COOKIE['ssinfo'])){
		$datas2 = select_registration_data($_COOKIE['ssinfo']);
		$datas = stripslashes($datas2['reg_data']);
		$datas = json_decode($datas, true);
		$user_name = $datas['username'];
		$user_email = $datas['useremail'];
		$newsletter = $datas['usernewsletter'];
		$rememberme = $datas['userrememberme'];
		$userpass = $datas['userpass'];
		$language = $datas['language'];
	}
	?>

	<form id="setupform" method="post" action="<?php echo $cur_url.create_store_urlfix('step=validate-user-signup'); ?>" class="sa-form">
		<input type="hidden" name="stage" value="validate-user-signup" />
		<?php do_action( 'signup_hidden_fields' ); ?>
		<div class="create-acc-phase">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label" for="user_name"><span class="required">*</span><?php _e('Username:','pro'); ?></label>
							<input class="form-control <?php if ( $errmsg = $errors->get_error_message('user_name') ) { echo "input-border-error";} ?>" name="user_name" type="text" id="user_name" value="<?php echo esc_attr($user_name); ?>" maxlength="60" placeholder="<?php _e( 'Must be at least 4 characters, letters and numbers only.','pro' ); ?>" />
							<?php if ( $errmsg = $errors->get_error_message('user_name') ) { echo "<br>" . error_style_message(__($errmsg)); }  ?>
					</div>	
				</div>
				
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label" for="user_email"><span class="required">*</span><?php _e( 'Email:','pro' ) ?></label>															
							<input class="form-control <?php if ( $errmsg = $errors->get_error_message('generic') ) { echo "input-border-error";} ?> <?php if ( $errmsg = $errors->get_error_message('user_email') ) { echo "input-border-error"; } ?>" name="user_email" type="text" id="user_email" value="<?php  echo esc_attr($user_email) ?>" maxlength="200" />
						<?php if ( $errmsg = $errors->get_error_message('generic') ) { echo error_style_message(__($errmsg)); } ?>
						<?php if ( $errmsg = $errors->get_error_message('user_email') ) { echo error_style_message(__($errmsg)); } ?>
					</div>
				</div>
			</div>
		<?php //do_action( 'signup_extra_fields', $errors ); ?>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label" for="password"><?php _e('Password', 'pro'); ?>:</label>
							<input class="form-control <?php if ( $errmsg = $errors->get_error_message('password_1') ) { echo 'input-border-error'; } ?>" name="user_password_1" type="password" id="password_1" value="<?php if(isset($userpass)) echo $userpass; ?>" autocomplete="off" maxlength="20" placeholder="<?php _e('Leave fields blank for a random password to be generated.', 'pro'); ?>"/>
							<?php if ( $errmsg = $errors->get_error_message('password_1') ) { echo error_style_message(__($errmsg)); } ?>
					</div>	
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label" for="password"><?php _e('Confirm Password', 'pro'); ?>:</label>
							<input class="form-control  <?php if ( $errmsg = $errors->get_error_message('password_1') ) { echo 'input-border-error'; } ?>" name="user_password_2" type="password" id="password_2" value="<?php if(isset($userpass)) echo $userpass; ?>" autocomplete="off" maxlength="20" placeholder="<?php _e('Type your new password again.', 'pro'); ?>"/>
							<?php if ( $errmsg = $errors->get_error_message('password_1') ) { echo error_style_message(__($errmsg)); } ?>			
					</div>	
				</div>	
			</div>	
			<div class="row">	
				<div class="col-md-6">
					<div class="form-group">								
						<label class="control-label" for="tp_language"><?php _e('Preferred Language', 'pro'); ?>:</label>
					</div>	
				</div>	
				<div class="col-md-6">
					<div class="form-group">
						<select class="form-control" name="tp_language">	
							<option value="en"<?php echo $language == 'en' ? ' selected' : ''; ?>><?php _e('English'); ?></option>
							<option value="bg"<?php echo $language == 'bg' ? ' selected' : ''; ?>><?php _e('Bulgarian'); ?></option>
						</select>
					</div>	
				</div>													
			</div>				
		</div>
		<p class="register-store-extext">
			<?php 
			$term_page_id = icl_object_id(725, 'page', TRUE, ICL_LANGUAGE_CODE);
			$term_page_link = get_blog_permalink( 1, $term_page_id );
			printf(__('<span>By clicking Signup, you confirm that you accept our</span> <a href="%s">Terms of Use</a>','pro'),$term_page_link); ?> 
		</p>
		<div class="form-group">
			<div class="input-control-checkbox">
				<input type="checkbox" name="newsletter" id="checkboxes" value="1"<?php if((!empty($newsletter) && $newsletter == 'yes') || !isset($datas['usernewsletter'])) echo ' checked';  ?>/>
				<label for="checkboxes">&nbsp;</label><span class="nwslttr"><?php _e('I want to receive your e-mail newsletter about new and interesting products from Bulgarian artists..','pro') ?></span>
			</div>
			<div class="input-control-checkbox">
				<input type="checkbox" name="rememberme" id="remembermes" value="1"<?php if((!empty($rememberme) && $rememberme == 'yes') || !isset($datas['userrememberme'])) echo ' checked';  ?>/>
				<label for="remembermes" class="remembermes"><?php _e('Remember your password','pro'); ?></label>
			</div>											
		</div>	
		<input id="signupblog" type="hidden" name="signup_for" value="blog" />
		<div class="row register-store-buttons">
			<div class="col-md-12 text-right step">
				<input type="submit" name="submit" class="login-button login-button-x" value="<?php esc_attr_e('Next Step','pro') ?>" />
			</div> 
		</div> 
		<div class="row register-store-buttons">
			<div class="col-md-12 fb-wide">
			<hr>
				<?php if(!is_user_logged_in()){ ?>
				<a id="store-fb-btn" class="login-button login-button-x fb-signup-botton<?php echo ICL_LANGUAGE_CODE == 'bg'? ' fb-signup-botton-bg': ''; ?>" href="<?php bloginfo('url') ?>/wp-signup.php?fb_registration_page=1">&nbsp;</a>
				<?php } ?>
			</div>
		</div>
	</form>

<?php }

/////////////////////////////////////// Store Creation ////////////////////////////////////////////

function validate_blog_signup() {
	global $cur_url;
	
	//user is not login
	
	if(is_user_logged_in() == false) {
		$for_registered = false;
		// Re-validate user info.
		if(isset($_COOKIE['ssinfo']) && select_registration_data($_COOKIE['ssinfo'])){
			$datas2 = select_registration_data($_COOKIE['ssinfo']);
			$datas = stripslashes($datas2['reg_data']);
			$datas = json_decode($datas, true);
			
			if(!empty($datas['username']) && !empty($datas['useremail'])) {
				$user_name = $datas['username'];
				$user_email = $datas['useremail'];
			} else {
				wp_safe_redirect($cur_url); 
				exit;
			}		
			
		} else {
			wp_safe_redirect($cur_url);
			exit;
		}

		$result = wpmu_validate_user_signup($user_name, $user_email);
		extract($result);
		if ( $errors->get_error_code() ) {
			wp_safe_redirect($cur_url);
			exit;
		}
		
		$_POST['blogname'] = isset($_POST['blogname'])? ltrim(str_replace(' ','',strtolower($_POST['blogname']))) : '';
		
		if (strpos(strtolower($_POST['blogname']), '.'.DOMAIN_CURRENT_SITE) !== false) {
			 $_POST['blogname'] =  	array_shift((explode(".",strtolower($_POST['blogname']))));
		} else {
			$_POST['blogname'] 	= 	strtolower($_POST['blogname']);
		}		
		
		$result = wpmu_validate_blog_signup($_POST['blogname'], ltrim($_POST['blog_title']));
	} else {
		$for_registered = true;
		$current_user 	= wp_get_current_user();
		
		if (strpos(strtolower($_POST['blogname']), '.'.DOMAIN_CURRENT_SITE) !== false) {
			 $_POST['blogname'] =  	array_shift((explode(".",strtolower($_POST['blogname']))));
		} else {
			$_POST['blogname'] 	= 	strtolower($_POST['blogname']);
		}		
		$result = wpmu_validate_blog_signup($_POST['blogname'], $_POST['blog_title'],$current_user);
	}	
	
		
	extract($result);
	
	if ( $errors->get_error_code() ) {
		if(is_user_logged_in() == false)
			signup_blog($user_name, $user_email, $blogname, $blog_title, $errors);
		else
			signup_another_blog($blogname, $blog_title, $errors);
			
		return false;
	}											
	
	if($for_registered)
		$datas = array();
	
	$datas['userblogname'] = $blogname;	
	if(!empty($_POST['bcat_site_categories'])) {
		$cats = array();
		foreach($_POST['bcat_site_categories'] as $cat) {
			   $cats[] = $cat;
		}
		$datas['userblogcat'] = implode(',',$cats);
	} else 	
		$datas['userblogcat'] = '';
	
	$store_title = $blog_title;
	$store_title_bg = isset($_POST['input_translate_blog_title_bg']) ? $_POST['input_translate_blog_title_bg'] : '';
	$store_desc = $_POST['bcat_site_description'];
	$store_desc_bg = (isset($_POST['input_translate_bcat_site_description_bg'] ) && !empty($_POST['input_translate_bcat_site_description_bg'])) ? $_POST['input_translate_bcat_site_description_bg'] : $_POST['bcat_site_description'];
	
	$userinfo = json_encode($datas);
	if($for_registered) {
		
		if(isset($_COOKIE['ssinfo'])){
			if(select_registration_data($_COOKIE['ssinfo'])) 
				$use_this_data = true;
		} 		
		
		if($use_this_data) {
			update_registration_data($_COOKIE['ssinfo'], $userinfo, true, $store_title, $store_title_bg, true, $store_desc, $store_desc_bg);
		} else {
			$ses_id = md5(time().cs_generate_password());
			insert_registration_data($ses_id,$userinfo,$store_title,$store_title_bg,$store_desc, $store_desc_bg);
			setcookie('ssinfo',$ses_id);
		}
		
	} else { 
		update_registration_data($_COOKIE['ssinfo'], $userinfo,true, $store_title, $store_title_bg, true, $store_desc, $store_desc_bg);
	}	
	
	if ( 'profile' == $_POST['signup_for'] ) {
		wp_safe_redirect($cur_url.create_store_urlfix('step=signup-profile-form'));
		exit;		
		
	}
	
	return true;
}


function signup_blog($user_name = '', $user_email = '', $blogname = '', $blog_title = '', $errors = '') {
	global $cur_url;
	
	// allow definition of default variables
	$filtered_results = apply_filters('signup_blog_init', array('user_name' => $user_name, 'user_email' => $user_email, 'blogname' => $blogname, 'blog_title' => $blog_title, 'errors' => $errors ));
	$user_name = $filtered_results['user_name'];
	$user_email = $filtered_results['user_email'];
	$blogname = $filtered_results['blogname'];
	$blog_title = $filtered_results['blog_title'];
	$errors = $filtered_results['errors'];
	if ( empty($blogname) )
		$blogname = $user_name;
	?>
	<!-- Step 2 button -->
	<form id="setupform" method="post" action="<?php echo $cur_url.create_store_urlfix('step=validate-blog-signup'); ?>" class="sa-form">
		<input type="hidden" name="stage" value="validate-blog-signup" />
		<?php do_action( 'signup_hidden_fields' ); ?>
		<?php show_blog_form($blogname, $blog_title, $errors); ?>
		<input id="signupprofile" type="hidden" name="signup_for" value="profile" />
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-6 step_back">
				<a class="btn btn-link" href="<?php echo $cur_url;?>"> <span style="vertical-align: top;"> &lt; </span> <?php _e('Back','pro'); ?> </a>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6 text-right">
				<div class="align-right"><input type="submit" name="for_step_3" class="login-button login-button-x" value="<?php esc_attr_e('Next Step','pro') ?>" /></div>
			</div> 
		</div>
	</form>
	<?php
}

add_filter( 'wpmu_validate_blog_signup','add_validate_store_cat');

function add_validate_store_cat($content){

	if ( isset($_POST['stage']) && $_POST['stage'] == 'validate-blog-signup' ) {
		if(empty($_POST['bcat_site_categories'])) {
			$errmsg = __('You must select at least 1 unique categories', 'pro');
			$content['errors']->add('bcat_site_categories', $errmsg);			
		}
		
		$bcat_site_description = '';
		if (isset($_POST['bcat_site_description'])) {
			$bcat_site_description = esc_attr($_POST['bcat_site_description']);
		}

		if (!strlen($bcat_site_description)) {
			$errmsg = __('Please provide a site description','pro');
			$content['errors']->add('bcat_site_description', $errmsg);		
		}
	}
	
	return $content;
}

function show_blog_form($blogname = '', $blog_title = '', $errors = '') {
	global $current_site;

	if ( !is_wp_error($errors) )
		$errors = new WP_Error();
	
	?>
	<div class="form-group">											   
	<?php if ( !is_subdomain_install() )
		echo '<span class="required">*</span><label class="control-label" for="storename">' . __('Store Name:','pro') . '</label>';
	else
		echo '<span class="required">*</span><label class="control-label" for="storedomain">' . __('Store Domain:','pro') . '</label>';

	if(isset($_COOKIE['ssinfo']) && select_registration_data($_COOKIE['ssinfo']) ){
		$datas2 = select_registration_data($_COOKIE['ssinfo']);
		$datas = stripslashes($datas2['reg_data']);
		$datas = json_decode($datas, true);
		
		if(!isset($_POST['blog_name']))
			$_POST['blog_name'] = $datas['userblogname'];
		
		if(!isset($_POST['blog_title']))
			$_POST['blog_title'] = $datas2['store_title'];
		
		if(!isset($_POST['bcat_site_categories'])) {
			
			if(!empty($datas['userblogcat']))
				$_POST['bcat_site_categories'] = explode(',',$datas['userblogcat']);
			else
				$_POST['bcat_site_categories'] = '';
			
		}
		
		if(!isset($_POST['bcat_site_description']))
			$_POST['bcat_site_description'] = $datas2['store_desc'];
		
	}
	
	$cats = array();
	if(!empty($_POST['bcat_site_categories']) && is_array($_POST['bcat_site_categories'])) {
		foreach($_POST['bcat_site_categories'] as $cat) {
			   $cats[] = $cat;
		}
		$_POST['bcat_site_categories'] = $cats;
	} else 	
		$_POST['bcat_site_categories'] = $_POST['bcat_site_categories'];
	 
	 
	 
	 
	?> 
	<div class="form-group" style="margin-top: -10px;">
	<?php if ( !is_subdomain_install() ) { ?>
		<span class="prefix_address"><?php echo $current_site->domain . $current_site->path; ?></span>
		<input name="blogname" class="form-control <?php if ( $errmsg = $errors->get_error_message('blogname') ) { echo 'input-border-error'; } ?>" type="text" id="blogname" value="<?php if(isset($_POST['blog_name'])) echo esc_attr($_POST['blog_name']); ?>" maxlength="60" /><br />
	<?php } else { ?>
		<p class="help-block"><?php _e('Must be at least 4 characters, letters and numbers only. It cannot be changed, so choose carefully.<br>Example:  yourdomain.'.DOMAIN_CURRENT_SITE,'pro'); ?></p>
		<?php if ( $errmsg = $errors->get_error_message('blogname') ) echo error_style_message($errmsg); ?>
		<input name="blogname" class="form-control <?php if ( $errmsg = $errors->get_error_message('blogname') ) { echo 'input-border-error'; } ?>" type="text" id="blogname" value="<?php if(isset($_POST['blog_name'])) echo esc_attr($_POST['blog_name']).'.'.DOMAIN_CURRENT_SITE; ?>" />												
		<!-- <p class="help-block"><?php //echo ($site_domain = preg_replace( '|^www.|', '', $current_site->domain)); ?></p> -->
	<?php } ?>
	
	<?php if ( !is_user_logged_in() ) { 
		if ( !is_subdomain_install() )
			$site = $current_site->domain . $current_site->path . __( 'sitename' );
		else
			//$site = __( 'domain' ) . '.' . $site_domain . $current_site->path;
	?>		
	
		<!--<p>(<strong><?php sprintf( __('Your address will be %s.','pro'), $site ); ?> </strong>) <?php _e( 'Must be at least 4 characters, letters and numbers only. It cannot be changed, so choose carefully!','pro' ); ?></p>-->
		
	<?php } ?>
	</div>
	</div>
	<div class="form-group">
	<span class="required">*</span><label class="control-label" for="blog_title"><?php _e('Store name:','pro') ?></label>  <?php translate_popup('blog_title'); ?>
	<?php if ( $errmsg = $errors->get_error_message('blog_title') ) echo error_style_message($errmsg); ?>
	<input class="form-control <?php if ( $errmsg = $errors->get_error_message('blog_title') ) { echo 'input-border-error'; } ?>" name="blog_title" type="text" id="blog_title" value="<?php if(isset($_POST['blog_title'])) echo $_POST['blog_title']; ?>" />												
	</div>
	<div id="blogform">
	
		<div id="bcat_site_categories_wrapper" style="<?php echo $wrapper_style; ?>">
		<div id="bcat_site_categories_section">
		<div class="form-group">
		<label class="form-label" for=""><span class="required">*</span><?php _e('Store Categories: ','pro'); ?></label>
		<p class="help-block"><?php _e('Select one or more categories for your store.', 'pro'); ?></p>
		<?php if ( $errmsg = $errors->get_error_message('bcat_site_categories') ) echo error_style_message($errmsg); ?>
		<div class="form-control well <?php if ( $errmsg = $errors->get_error_message('bcat_site_categories') ) { echo 'input-border-error'; } ?>">
			<ul id="site-categories-list">
				<?php
				$cat_excludes = '';
				$cat_counter = 1;
				$terms_count = wp_count_terms('bcat', array('hide_empty' => false));
				$blog_category_limit = $terms_count;
					
				$categories = get_terms('bcat', array('parent' => 0, 'hide_empty' => false, 'exclude' =>	$cat_excludes) );
					 foreach ( $categories as $term ) {
						
						if(is_array($_POST['bcat_site_categories'])) {
							if (in_array($term->term_id,$_POST['bcat_site_categories'])) 
								$checked = 'checked';
							else 
								$checked = "";
							
						} else {
							if($term->term_id==$_POST['bcat_site_categories'])
								$checked = 'checked';
							else 
								$checked = "";
							
						}
					 
					  echo "<li>";
					  echo '
						<div class="input-control-checkbox">															
							<input id="checkbox'. $cat_counter .'" type="checkbox" name="bcat_site_categories[]" value="'. $term->term_id .'" ' . $checked . '/>
							<label for="checkbox'. $cat_counter .'">'. $term->name .'</label>
						</div>
					  ';
					  echo '</li>';
					  
					  $cat_counter += 1;
						if ($cat_counter > $blog_category_limit)
							break;
					 }
				?>
			</ul>
		</div>
		</div></div><?php

		?>

		<div id="bcat_site_description_section">
			<div class="form-group">
			<label class="control-label" for="bcat_site_description"><span class="required">*</span><?php _e('Store Description','pro'); ?></label>
				<p class="help-block"><?php _e('Please check your store description carefully it will be displayed to the public.', 'pro'); ?> <?php translate_popup('bcat_site_description'); ?>
				<div class="controls">
				<?php if ( $errmsg = $errors->get_error_message('bcat_site_description') ) echo error_style_message($errmsg); ?>
					<textarea name="bcat_site_description" class="form-control <?php if ( $errmsg = $errors->get_error_message('bcat_site_description') ) { echo 'input-border-error'; } ?>" cols="30" rows="10" id="bcat_site_description"><?php
						if (isset($_POST['bcat_site_description'])) {
							echo $_POST['bcat_site_description'];
						}
					?></textarea>
				</div>	
			</div>
		</div>
		</div>	
	<?php //do_action('signup_blogform', $errors); ?>
	</div>
	<div id="privacy">
		<p class="privacy-intro">
			<label style="display: none;" class="checkbox" for="blog_public_off">
				<input type="radio" id="blog_public_off" name="blog_public" value="0" checked="checked" />
				<strong><?php _e( 'No','pro' ); ?></strong>
			</label>
		</p>
	</div>
<?php
}

function validate_blog_form() {
	$user = '';
	if ( is_user_logged_in() )
		$user = wp_get_current_user();
	return wpmu_validate_blog_signup($_POST['blogname'], $_POST['blog_title'], $user);
}

/////////////////////////////// Validate for Another Store Creation ///////////////////////////////////////////////////
function signup_another_blog($blogname = '', $blog_title = '', $errors = '') {
	global $current_site, $cur_url;
	$current_user = wp_get_current_user();
	
	if ( ! is_wp_error($errors) ) {
		$errors = new WP_Error();
	}
	
	$filtered_results = apply_filters('signup_another_blog_init', array('blogname' => $blogname, 'blog_title' => $blog_title, 'errors' => $errors ));
	$blogname = $filtered_results['blogname'];
	$blog_title = $filtered_results['blog_title'];
	$errors = $filtered_results['errors']; 
	
	if ( empty($blogname) )
		$blogname = $current_user->user_login;	
	
	
	?>

	<h3 class="page-header" style="border-bottom-color: #000"><?php sprintf( __( 'Get an %s site in seconds' ), $current_site->site_name ); ?></h3>
	
	<?php if ( $errors->get_error_code() ) { echo error_style_message(__( 'There was a problem, please correct the form below and try again.','pro' )); } ?>
	
	<?php 
	//$blogs = get_blogs_of_user($current_user->ID);
	//$blogsite = get_active_blog_for_user($current_user->ID);
	
	$mystore = get_my_store_id();
	
	echo '<h4>'.__('Welcome','pro').', '.$current_user->display_name.'.</h4>';
	if (!$mystore) {
		_e( '<p>By filling out the form below, you can <strong>add 1 Store to your account</strong>.</p> <p>You can create maximum 1 Store and you are required to use it responsibly!</p> <p>Members that will use the Store to spam or for any use different from testing Art Bulgaria Site, will be immediately banned!</p>','pro');
	}
	
	//$roles = $current_user->roles;	
	
	if ($mystore) {
		$mystoredata = get_storeowner_data($mystore);
	
	?>
			<p><?php _e( 'Store that you owned','pro') ?></p>
			<ul>
				<li><a href="<?php echo $mystoredata['site_url']; ?>"><?php echo $mystoredata['site_url']; ?></a></li>												
			</ul>
			
	<?php } else { ?>
	
		<div class="alert"><?php _e( 'If you&#8217;re not going to use a great site domain, leave it for a new user. Now have at it!','pro' ) ?></div>
		
		<form id="setupform" method="post" action="<?php echo $cur_url.create_store_urlfix('step=validate-blog-signup'); ?>" class="sa-form">
			<input type="hidden" name="stage" value="validate-blog-signup" />
			<?php do_action( 'signup_hidden_fields' ); ?>
			<?php show_blog_form($blogname, $blog_title, $errors); ?>
			<input id="signupprofile" type="hidden" name="signup_for" value="profile" />
			<div class="row">
				<div class="col-md-12 text-right">
					<div class="align-right"><input type="submit" name="for_step_3" class="login-button login-button-x" value="<?php esc_attr_e( 'Next Step','pro' ) ?>" /></div>
				</div> 
			</div>	
		</form>
		
	<?php }
}

///////////////////////////////////////////////////////////////////////////////////////

function validate_profile_signup() {
	global $cur_url;
	
	if(isset($_COOKIE['ssinfo']) && select_registration_data($_COOKIE['ssinfo']) ){

			$data2 = select_registration_data($_COOKIE['ssinfo']);
			$data = stripslashes($data2['reg_data']);
			$data = json_decode($data, true);
			
			//User Info
			if(is_user_logged_in() == false) {
				$user_name 		= 	$data['username'];
				$user_email 	= 	$data['useremail'];
				$userpass 		= 	$data['userpass'];
				$newsletter	 	= 	$data['usernewsletter'];
				$rememberme 	= 	$data['userrememberme'];
				$language 		= 	$data['language'];
			} else {
				$current_user = wp_get_current_user();
				$user_name 		= 	$current_user->user_login;
				$user_email 	= 	$current_user->user_email;				
			}
			//Store Info
			$blog_name 		= 	$data['userblogname'];
			$blog_title 	= 	$data2['store_title'];
			$blog_title_bg 	= 	$data2['store_title_bg'];
			$site_cat 		= 	$data['userblogcat'];
			$site_desc 		= 	$data2['store_desc'];
			$site_desc_bg 	= 	$data2['store_desc_bg'];
		
			if (!empty($site_cat) && strpos($site_cat,',') !== false)
				$cat = explode(',',$site_cat);
			else
				$cat = array($site_cat);
			
		if(is_user_logged_in() == false) {
			$result = wpmu_validate_user_signup($user_name, $user_email);
			extract($result);
			if ( $errors->get_error_code() ) {
				wp_safe_redirect($cur_url);
				exit;
			}
			$result = wpmu_validate_blog_signup($blog_name , $blog_title);
		} else {
			$result = wpmu_validate_blog_signup($blog_name , $blog_title,$current_user);
		}

		extract($result);
		if ( $errors->get_error_code() ) {
			if(is_user_logged_in() == false)
				wp_safe_redirect($cur_url.create_store_urlfix('step=signup-blog-form'));
			else
				wp_safe_redirect($cur_url);
			
			exit;
		}
		
		$errors = wpmu_validate_profile_signup();
		if ($errors->get_error_code() ) {
			signup_profile($user_name, $user_email, $blog_name,$blog_title, $cat, $site_desc, $errors);
			return false;
		}
		
		$all_fields = array();
		if(isset($domain))
			$all_fields['domain'] = $domain;
		

			
		$all_fields['username'] = $user_name;
		$all_fields['useremail'] = $user_email;
		if(is_user_logged_in() == false) {
			
			if(isset($data['userpass']))
				$all_fields['userpass'] = $userpass;
		
			$all_fields['usernewsletter'] = $newsletter;
			$all_fields['userrememberme'] = $rememberme;
			$all_fields['language']		  = $language;
		
		}
			
			//Store Info
			$all_fields['userblogname'] 	= $blog_name;
			$all_fields['userblogtitle'] 	= $blog_title;
			$all_fields['userblogtitle_bg'] = $blog_title_bg;
			$all_fields['userblogcat'] 		= $site_cat;
			$all_fields['userblogdesc'] 	= $site_desc;
			$all_fields['userblogdesc_bg'] 	= $site_desc_bg;		
		
		
		foreach ($_POST as $key => $value) {
			$name = $key;
			$value = $value;
			$all_fields[$name] = $value ;
		}										
		
		validate_final_registration($all_fields);
		return true;		
	
	} else {
		wp_safe_redirect($cur_url);
		exit;		
	}	

}

function wpmu_validate_profile_signup(){
	
	if ( ! is_wp_error($errors) )
		$errors = new WP_Error();

	if(empty($_POST['store_picture']))
		$errors->add('store_picture', __('Picture is required!.', 'pro'));
	
	if(empty($_POST['nickname']))
		$errors->add('nickname', __('Nickname is required!.', 'pro'));
	
	if(empty($_POST['first_name']))
		$errors->add('first_name', __('First name is required!.', 'pro'));	

	if(empty($_POST['last_name']))
		$errors->add('last_name', __('Last name is required!.', 'pro'));	

	//if(empty($_POST['phone']))
	//	$errors->add('phone', __('Phone is required!.', 'pro'));	

	if(empty($_POST['display_name']))
		$errors->add('display_name', __('Display name is required!.', 'pro'));	

	//if(empty($_POST['address_1']))
	//	$errors->add('address_1', __('Address is required!.', 'pro'));	

	if(empty($_POST['country']))
		$errors->add('country', __('Country is required!.', 'pro'));	

	if(empty($_POST['city']))
		$errors->add('city', __('City is required!.', 'pro'));	

	//if(empty($_POST['zipcode']))
	//	$errors->add('zipcode', __('Zipcode is required!.', 'pro'));	


	return $errors;
}	


function validate_final_registration($fields) {
	global $cur_url, $wp_rewrite, $wpdb;
		//Personal Info
		
		$user_name 		= 	$fields['username'];
		$user_email 	= 	$fields['useremail'];
		$userpass 		= 	isset($fields['userpass'])? $fields['userpass'] : '';
		if(is_user_logged_in()== false) {	
			$newsletter	 	= 	$fields['usernewsletter'];
			$rememberme 	= 	$fields['userrememberme'];
			$language 		= 	$fields['language'];
		}	
		//Store Info
		$blog_name 		= 	$fields['userblogname'];
		$blog_title 	= 	$fields['userblogtitle'];
		$blog_title_bg 	= 	$fields['userblogtitle_bg'];
		$site_cat 		= 	$fields['userblogcat'];
		$site_desc 		= 	tp_remove_links($fields['userblogdesc']);	
		$site_desc_bg 	= 	tp_remove_links($fields['userblogdesc_bg']);
		
		
		if (!empty($site_cat) && strpos($site_cat,',') !== false) {
			$cat = explode(',',$site_cat);
			$cat = get_bcat_oritrans_id_store($cat);
		} else {
			$cat = array($site_cat);
			$cat = get_bcat_oritrans_id_store($cat);
		}	
	
		//Personal Info
		$store_picture = $fields['store_picture'];
		$store_picture = set_url_scheme($store_picture,'http');
		$nickname = $fields['nickname'];
		$website = $fields['website'];
		$fee_range = $fields['fee_range'];
		$first_name = $fields['first_name'];
		$last_name = $fields['last_name'];
		$first_name_bg = $fields['input_translate_first_name_bg'];
		$last_name_bg = $fields['input_translate_last_name_bg'];		
		$display_name = $fields['display_name'];
		$phone = $fields['phone'];
		$minimumfee = $fields['minimumfee'];				
		$fb_link 		= validateSocial($fields['fb_link'], 'facebook') ? esc_attr($fields['fb_link']) : ''; 
		$gplus_link 	= validateSocial($fields['twitter_link'], 'twitter') ? esc_attr($fields['twitter_link']) : ''; 
		$twitter_link 	= validateSocial($fields['gplus_link'], 'googleplus') ? esc_attr($fields['gplus_link']) : '';
		$pinterest_link = validateSocial($fields['pinterest_link'], 'pinterest') ? esc_attr($fields['pinterest_link']) : '';		
		$address_1 = $fields['address_1'];
		$address_1_bg = $fields['input_translate_address_1_bg'];
		$address_2 = $fields['address_2'];
		$address_2_bg = $fields['input_translate_address_line2_bg'];
		$city = $fields['city'];
		$state = $fields['state'];
		$zipcode = $fields['zipcode'];
		$country = $fields['country'];		
		$domain = $fields['domain'];
		
		if (!empty($country) && !empty($state)) {
			$address = $address_1 . ' ' . $city . ' ' . $state . ' ' . $country;
			$map = explode(',', cs_get_lat_long($address));
			$map_lat = isset($map[0])? $map[0] : '';
			$map_long = isset($map[1])? $map[1] : '';
		}
																						
		$public = (int) $_POST['blog_public'];
		if(empty($blog_title))
			$blog_title = $blog_name;
		
		$meta = array (
			'lang_id' => 1, 
			'public' => $public,
			'blogname' => esc_attr($blog_title),	
			'blogname_bg' => esc_attr($blog_title_bg), 	
			'tp_store_picture' => esc_attr($store_picture),
			'tp_store_website' => esc_attr( $website ),
			'tp_store_phone' =>  esc_attr( $phone ),
			'tp_store_free_range' => esc_attr( $fee_range ),
			'tp_store_minimum_fee' => esc_attr( $minimumfee ),
			'tp_store_facebook' => esc_attr( $fb_link ),
			'tp_store_twitter' => esc_attr( $twitter_link ),
			'tp_store_googleplus' => esc_attr( $gplus_link ),
			'tp_store_pinterest' => esc_attr( $pinterest_link ),
			'tp_store_address1' => esc_attr( $address_1 ),
			'tp_store_address2' => esc_attr( $address_2 ),
			'tp_store_address1_bg' => esc_attr( $address_1_bg ),
			'tp_store_address2_bg' => esc_attr( $address_2_bg ),
			'tp_store_city' => esc_attr( $city ),
			'tp_store_state' => esc_attr( $state ),
			'tp_store_zip' => esc_attr( $zipcode ),
			'tp_store_country' => esc_attr( $country ),
			'bcat_site_categories' => serialize($cat),
			'blogdescription' => esc_attr($site_desc),
			'bcat_site_description' => esc_attr($site_desc),	
			'bcat_site_description_bg' => esc_attr($site_desc_bg),
			'tp_store_map_lat' => $map_lat,
			'tp_store_map_long' => $map_long
		);
		
		$meta = apply_filters( 'add_signup_meta', $meta );
		
		
		if(is_user_logged_in()== false) {
			wpmu_signup_user($user_name, $user_email);
		}
		wpmu_signup_blog($domain, $path, $blog_title, $user_name, $user_email, $meta);
				
		// get user id
		$userdata = array();
		if(is_user_logged_in()) {
			$current_user = wp_get_current_user();
			$userdata['ID'] = $current_user->ID;		
		} else {
			$user = get_user_by( 'login', $user_name );
			$userdata['ID'] = $user->ID;
		}
		$user_get_id =	$userdata['ID'];
		$userdata['first_name'] = esc_attr($first_name);
		$userdata['last_name'] = esc_attr($last_name);
		$userdata['nickname'] = esc_attr($nickname);
		$userdata['display_name'] = esc_attr($display_name);
		wp_update_user( $userdata );

		//add to email subscription
		if(isset($newsletter))
			email_subscribed_signup($user_email);
		
		//get blog id
		$new_store_id = get_blog_id_from_url($domain);

		//Buglarian First Name and Last Name
		add_user_meta( $user_get_id, 'first_name_bg', esc_attr( $first_name_bg ) );
		add_user_meta( $user_get_id, 'last_name_bg', esc_attr( $last_name_bg ) );		
		add_user_meta( $user_get_id, 'my_avatar_picture', esc_attr( $store_picture));
		
		if(isset($language))
			add_user_meta( $user_get_id, 'default_user_language', esc_attr( $language));
		
		if (is_array($cat)) {
			save_bcat_terms_relation_store($cat,$new_store_id);
		}
	
		if ( is_wp_error( $term_taxonomy_ids ) ) {
			$error = 'has error';
		} else {
			$error = 'no error';
		}
				
		//do_action('blog_site_page_created');
		
		//file_put_contents(get_template_directory().'/debug.log',print_r($cat, true)."/n/n".print_r($cat_ids, true)."/n/n".$error);	
}

function email_subscribed_signup($Email){
	global $wpdb;
	
	$result = '0';
	$sSql = $wpdb->prepare(
		"SELECT COUNT(*) AS `count` FROM ".WP_eemail_TABLE_SUB."
		WHERE `eemail_email_sub` = %s", $Email);
	$result = $wpdb->get_var($sSql);
	
	if ($result == '0') {
		$eemail_opt_option = get_site_option('eemail_opt_option');
		if($eemail_opt_option == "double-optin")
		{
			$doubleoptin = "PEN";
		}
		else
		{
			$doubleoptin = "SIG";
		}
		
		$CurrentDate = date('Y-m-d G:i:s'); 
		$sql = $wpdb->prepare(
			"INSERT INTO `". WP_eemail_TABLE_SUB ."`
			(`eemail_name_sub`,`eemail_email_sub`, `eemail_status_sub`, `eemail_date_sub`)
			VALUES(%s, %s, %s, %s)",
			array('NA', $Email, $doubleoptin, $CurrentDate)
		);
		$wpdb->query($sql);
	}
}

function signup_profile($user_name = '', $user_email ='', $blogname= '', $blog_title= '', $site_cat ='', $site_desc ='', $errors = '') {
	global $cur_url;
	
	if ( ! is_wp_error($errors) )
		$errors = new WP_Error();	
	
	//wp_enqueue_media();
	//add_thickbox();
	//wp_enqueue_script('media-upload');
	  
	//wp_enqueue_script('toprank-picture-upload', get_template_directory_uri().'/js/upload_picture_store.js', array('jquery'));
	$fee = false;
	$filtered_results = apply_filters('signup_blog_init', array('user_name' => $user_name, 'user_email' => $user_email, 'blogname' => $blogname, 'blog_title' => $blog_title, 'errors' => $errors ));
	$user_name = $filtered_results['user_name'];
	$user_email = $filtered_results['user_email'];
	$blogname = $filtered_results['blogname'];
	$blog_title = $filtered_results['blog_title'];
	$errors = $filtered_results['errors'];
	if(is_array($site_cat)){
		
		if (in_array("Performers", $site_cat))
			$fee = true;
		
	} else {
		
		if($site_cat == "Performers")
			$fee = true;
	}	
	
	if(isset($_COOKIE['ssinfo']) && select_registration_data($_COOKIE['ssinfo']) ){
		$datas2 = select_registration_data($_COOKIE['ssinfo']);
		$datas	= stripslashes($datas2['reg_data']);
		$datas 	= json_decode($datas, true);
		
		if(!isset($_POST['blog_name']))
			$_POST['blog_name'] = $datas['userblogname'];
		
		if(!isset($_POST['blog_title']))
			$_POST['blog_title'] = $datas2['store_title'];
		
		if(!isset($_POST['bcat_site_categories'])) {
			
			if(!empty($datas['userblogcat']))
				$_POST['bcat_site_categories'] = explode(',',$datas['userblogcat']);
			else
				$_POST['bcat_site_categories'] = '';
			
		}
		
		if(!isset($_POST['bcat_site_description']))
			$_POST['bcat_site_description'] = $datas2['store_desc'];
		
		if(!isset($_POST['store_picture']) || (isset($_COOKIE['store_picture']) || !empty($_COOKIE['store_picture'])))
			$store_picture_post = $_COOKIE['store_picture'];
		else
			$store_picture_post = $_POST['store_picture'];
			
		
	}	

	?>
	<form id="setupform" method="post" action="<?php echo $cur_url.create_store_urlfix('step=validate-profile-signup'); ?>" class="sa-form">
		<input type="hidden" name="stage" value="validate-profile-signup" />
	<div id="form-step3">
		<div class="well well-small"> <?php _e('About','pro'); ?> </div>
		<div class="row">
			<div class="col-md-12 create-store-pic">
				<?php
					echo do_shortcode('[tp_profile_imgupload createstore="yes"]');
				?>
				<input type="hidden" name="store_picture" id="store_picture" value="<?php echo esc_attr($store_picture_post) ?>"/>
			</div>
		  <div class="col-md-12">
			<div class="row">
				<div class="col-md-6">
				<label class="" for="image"><span class="required">*</span><?php _e('Picture:', 'mp') ?></label>
				<?php if($errmsg = $errors->get_error_message('store_picture')) echo error_style_message($errmsg); ?>
					<div id="store_picture_holder">
						<?php
						$store_pic = esc_attr($store_picture_post);
						if (!empty($store_pic)) { ?>
							<img src="<?php echo $store_pic; ?>"
								 alt="<?php echo esc_attr($profileuser->nickname) ?>" class="img-thumbnail" width="160px" height="160px"/>
						<?php } else { ?>
							<img 
								 src="<?php echo get_template_directory_uri().'/img/no-image-store.png'; ?>"
								 alt="<?php echo esc_attr($profileuser->nickname) ?>" width="200px"
								 height="110px"/>
						<?php } ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label" for="first_name"><span class="required">*</span><?php _e('First Name:','pro') ?></label>		<?php translate_popup('first_name'); ?>
								<?php if($errmsg = $errors->get_error_message('first_name')) echo error_style_message($errmsg); ?>
								<input class="form-control <?php if ( $errmsg = $errors->get_error_message('first_name') ) { echo 'input-border-error'; } ?>" name="first_name" type="text" id="first_name" value="<?php echo esc_attr($_POST['first_name']); ?>" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label" for="last_name"><span class="required">*</span><?php _e('Last Name:','pro') ?></label>	<?php translate_popup('last_name'); ?>	
								<?php if($errmsg = $errors->get_error_message('last_name')) echo error_style_message($errmsg); ?>
								<input class="form-control <?php if ( $errmsg = $errors->get_error_message('last_name') ) { echo 'input-border-error'; } ?>" name="last_name" type="text" id="last_name" value="<?php echo esc_attr($_POST['last_name']); ?>" />
							</div>
						</div>
					</div>	
				</div>		
			</div>	
		  </div>
		</div>
		
		<div class="row about">
		  <div class="col-md-6">											
				<div class="form-group">
					<label class="control-label" for="nickname"><span class="required">*</span><?php _e('Nickname:','pro') ?></label>
					<?php if($errmsg = $errors->get_error_message('nickname')) echo error_style_message($errmsg); ?>
					<input class="form-control <?php if ( $errmsg = $errors->get_error_message('nickname') ) { echo 'input-border-error'; } ?>" name="nickname" type="text" id="nickname" value="<?php echo esc_attr($_POST['nickname']); ?>" />
				</div>
				<div class="form-group">
					<label class="control-label" for="website"><?php _e('Website:','pro') ?></label>		
						<input class="form-control" name="website" type="text" id="website" value="<?php echo esc_attr($_POST['website']); ?>" />
				</div>
				<?php if( $fee == true ) : ?>
				<div class="form-group">
					<label class="control-label" for="fee_range"><?php _e('Fee Range:','pro') ?></label>
					<p class="help-block"><?php _e('What is your usual charge?','pro') ?></p>
						<input style="margin-bottom: 0;" class="form-control" name="fee_range" type="text" id="fee_range" value="<?php echo esc_attr($_POST['fee_range']); ?>" />																
				</div>
				<?php endif; ?>
		  </div>
		  <div class="col-md-6">
				<div class="form-group">
					<label class="control-label" for="display_name"><span class="required">*</span><?php _e('Display public name as:','pro') ?></label>		
					<?php if($errmsg = $errors->get_error_message('display_name')) echo error_style_message($errmsg); ?>
					<input class="form-control <?php if ( $errmsg = $errors->get_error_message('display_name') ) { echo 'input-border-error'; } ?>" name="display_name" type="text" id="display_name" value="<?php echo esc_attr($_POST['display_name']); ?>" />
				</div>
				<div class="form-group">
					<label class="control-label" for="phone"><?php _e('Phone:','pro') ?></label>	
					<?php if($errmsg = $errors->get_error_message('phone')) echo error_style_message($errmsg); ?>	
					<input class="form-control <?php if ( $errmsg = $errors->get_error_message('phone') ) { echo 'input-border-error'; } ?>" name="phone" type="text" id="phone" value="<?php echo esc_attr($_POST['phone']); ?>" />
				</div>
				
				<?php if($fee) : ?>
				<div class="form-group">
					<label class="control-label" for="minimumfee"><?php _e('Minimum Fee:','pro') ?></label>
					<p class="help-block"><?php _e('What is your minimum fee? Ex: (160)','pro') ?></p>
						 <div class="input-group">
						  <div class="input-group-btn">
							<button style="border: 1px solid rgb(204, 204, 204);" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">$ <span class="caret"></span></button>
							<ul class="dropdown-menu" role="menu" style="background: #fff; border: 1px solid #fff;">
							 <li><a href="#">&#36;</a></li>
							  <li><a href="#">&#162;</a></li>
							  <li><a href="#">&#128;</a></li>
							</ul>
						  </div><!-- /btn-group -->
						  <input type="text" class="form-control" aria-label="..." name="minimumfee" value="<?php echo esc_attr($_POST['minimumfee']); ?>">
						</div><!-- /input-group -->																
				</div>
				<?php endif; ?>
		  </div>
		</div>

		<div class="well well-small"> <?php _e('Social Links','pro'); ?> </div>

		<div class="row">
		  <div class="col-md-6">
				<div class="form-group">
					<label class="control-label" for="fb_link"><?php _e('Facebook','pro') ?></label>		
					<input class="form-control" name="fb_link" type="text" id="fb_link" value="<?php echo esc_attr($_POST['fb_link']); ?>" />
				</div>
				<div class="form-group">
					<label class="control-label" for="gplus_link"><?php _e('Google +','pro') ?></label>		
						<input class="form-control" name="gplus_link" type="text" id="gplus_link" value="<?php echo esc_attr($_POST['gplus_link']); ?>" />
				</div>
			</div>

		  <div class="col-md-6">
				<div class="form-group">
					<label class="control-label" for="twitter_link"><?php _e('Twitter','pro') ?></label>		
					<input class="form-control" name="twitter_link" type="text" id="twitter_link" value="<?php echo esc_attr($_POST['twitter_link']); ?>" />
				</div>
				<div class="form-group">
					<label class="control-label" for="pinterest_link"><?php _e('Pinterest','pro') ?></label>		
						<input class="form-control" name="pinterest_link" type="text" id="pinterest_link" value="<?php echo esc_attr($_POST['pinterest_link']); ?>" />
				</div>		
		  </div>
		</div>
		<div class="well well-small"> <?php _e('Location','pro'); ?> </div>

		<div class="row">
		  <div class="col-md-6">
				<div class="form-group">
					<label class="control-label" for="address_1"><?php _e('Address Line 1:','pro') ?></label>		<?php translate_popup('address_1'); ?><?php //do_action('icl_language_selector'); ?>
					<?php if($errmsg = $errors->get_error_message('address_1')) echo error_style_message($errmsg); ?>	
					<input class="form-control <?php if ( $errmsg = $errors->get_error_message('address_line1') ) { echo 'input-border-error'; } ?>" name="address_1" type="text" id="address_1" value="<?php echo esc_attr($_POST['address_1']); ?>" />
				</div>
				<div class="form-group">
					<label class="control-label" for="country"><span class="required">*</span><?php _e('Country','pro') ?></label>		
						<?php if($errmsg = $errors->get_error_message('country')) echo error_style_message($errmsg); ?>	
						<input class="form-control <?php if ( $errmsg = $errors->get_error_message('country') ) { echo 'input-border-error'; } ?>" name="country" type="text" id="country" value="<?php echo esc_attr($_POST['country']); ?>" />
				</div>
				<div class="form-group">
					<label class="control-label" for="state"><?php _e('State:','pro') ?></label>
					<?php if($errmsg = $errors->get_error_message('state')) echo error_style_message($errmsg); ?>	
					<input class="form-control <?php if ( $errmsg = $errors->get_error_message('state') ) { echo 'input-border-error'; } ?>" name="state" type="text" id="state" value="<?php echo esc_attr($_POST['state']); ?>" />
				</div>
		  </div>
		  <div class="col-md-6">
				<div class="form-group">
					<label class="control-label" for="address_line2"><?php _e('Address line 2:','pro') ?></label>		<?php translate_popup('address_2'); ?><?php //do_action('icl_language_selector'); ?>
					<input class="form-control" name="address_line2" type="text" id="blog_title" value="<?php echo esc_attr($_POST['address_line2']); ?>" />
				</div>
				<div class="form-group">
					<label class="control-label" for="city"><span class="required">*</span><?php _e('City:','pro') ?></label>	 <?php if($errmsg = $errors->get_error_message('city')) echo error_style_message($errmsg); ?>		
						<input class="form-control <?php if ( $errmsg = $errors->get_error_message('city') ) { echo 'input-border-error'; } ?>" name="city" type="text" id="city" value="<?php echo esc_attr($_POST['city']); ?>" />
				</div>
				<div class="form-group">
					<label class="control-label" for="zipcode"><?php _e('Zip Code:','pro') ?></label>	
						<?php if($errmsg = $errors->get_error_message('zipcode')) echo error_style_message($errmsg); ?>
						<input class="form-control <?php if ( $errmsg = $errors->get_error_message('zipcode') ) { echo 'input-border-error'; } ?>" name="zipcode" type="text" id="zipcode" value="<?php echo esc_attr($_POST['zipcode']); ?>" />
				</div>			
		  </div>
		</div>
		<div class="row">
			<div class="col-md-6 col-xs-4 col-sm-6 step_back">
				<a class="btn btn-link" href="<?php echo $cur_url;?><?php echo is_user_logged_in()== false? create_store_urlfix('step=signup-blog-form'): ''; ?>"> <span style="vertical-align: top;"> &lt; </span> <?php _e('Back','pro'); ?> </a>
			</div>
			<div class="col-md-6 col-xs-8 col-sm-6 text-right">
				<div class="align-right"><input type="submit" name="for_finish" class="login-button login-button-x" value="<?php esc_attr_e('Register your new store','pro') ?>" /></div>
			</div> 
		</div> 


		</div>
	</form>
	<?php
}


add_filter('wpmu_welcome_notification','wpmu_welcome_notification_remove');
add_filter('wpmu_signup_user_notification','wpmu_signup_user_notification_remove');

function wpmu_welcome_notification_remove($blog_id = '', $user_id = '', $password = '', $title = '', $meta = array() ){
	return false;	
}

function wpmu_signup_user_notification_remove($user = '', $user_email = '', $key = '', $meta = array()){
	return false;	
}	

function cs_get_lat_long($address)
{
    $address = str_replace(" ", "+", $address);

    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
    $json = json_decode($json);

    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
    return $lat . ',' . $long;
}
?>