<?php global $mp;
/**
 * Single Product Page Template - Default Layout
 */

	$post_id = get_the_ID();
	$pinit_setting = $mp->get_setting('social->pinterest->show_pinit_button');
	$btnclass = mpt_load_mp_btn_color();
	$tagcolor = mpt_load_icontag_color();
	$labelcolor = mpt_load_mp_label_color();
	$meta_prefix = '_mpt_';
	$contenttextcolor = esc_attr(get_post_meta( $post_id , $meta_prefix . 'post_content_text_color', true ));
	$mp_is_event = get_post_meta( $post_id , 'mp_is_event', true );
	$mp_is_booking = get_post_meta( $post_id , 'mp_is_booking', true );
	$tp_global_page_settings = (!get_site_transient( 'cache_global_page_settings' ))? get_site_transient( 'cache_global_page_settings' ) : get_site_option( 'tp_global_page_settings' );
	if(ICL_LANGUAGE_CODE=='bg'){
		$store_quote_slug = '/'.$tp_global_page_settings['store_quote_slug_bg'].'/?lang=bg';
	} else {
		$store_quote_slug = '/'.$tp_global_page_settings['store_quote_slug_en'].'/?lang=en';
	}
	
	$uct_cookie_name = 'wordpress_useclientstimezone_timezone';
	if(isset($_COOKIE[$uct_cookie_name])) {
	$date = new DateTime("now", new DateTimeZone($_COOKIE[$uct_cookie_name]) );
		$newdate = $date->format('Y-m-d H:i:s');
	} else {
		$newdate = current_time('mysql');
	}	
	
?>

<?php mpt_load_top_code(); ?>	

	<?php if(!$mp_is_event && !$mp_is_booking) { ?>
	<div class="product-upfold-contents row-fluid">

		<?php 

			//$default_img_url = esc_url( get_option('mpt_mp_default_product_img') );
			$default_img_url = esc_url( get_blog_option(1,'mpt_mp_default_product_img') );
			$default_img_id = ( !empty($default_img_url) ? intval( get_image_id( $default_img_url ) ) : NULL );

			if ( has_post_thumbnail( $post_id ) || ( !empty($default_img_id) && is_numeric($default_img_id) ) ) {
				echo '<div class="col-md-6 col-sm-12 col-xs-12">' . pro_product_page_load_images( array(
						'post_id' => $post_id, 
						'default_img_id' => $default_img_id,
					) ) . '</div>';
				echo '<div class="col-md-6 col-sm-12 col-xs-12">';
			} else {
				echo '<div class="span12">';
			}
		?>
			
			
		<div class="product-title">
			<h3><span<?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>><?php the_title(); ?></span></h3>
			<!--<div class="product-price align-right">
				<?php 

					pro_product_price( array( 'echo' => true , 'post_id' => $post_id , 'label' => '<i class="icon-tags'.$tagcolor.'"></i> ' . __( 'Price: ' , 'pro' ) , 'context' => 'widget'
						) );

					echo ( function_exists('mp_pinit_button') && $pinit_setting != 'off' ? '<div class="single-page-pinit-btn">' . mp_pinit_button( $post_id ) . '</div>' : '' ); 
				?>
			</div>-->
			<hr>
			<?php 
				//if ( get_option('mpt_pro_page_show_product_short_desc') != 'false' )
				if ( get_blog_option(1,'mpt_pro_page_show_product_short_desc') != 'false' ) {
					if ( !empty($post->post_excerpt) ) {
						$post_excerpt = tp_remove_links( strip_shortcodes($post->post_excerpt) );
					} else {	
						if ( !empty($post->post_content) ) {
							$post_excerpt = wp_trim_words( tp_remove_links(strip_shortcodes($post->post_content) ) , 20 ) ;
						} else {
							$post_excerpt = '';
						} 
					}
						// $post_excerpt =  htmlentities($post_excerpt, null, 'utf-8');
						$string_excerpt = str_replace("&nbsp;", "", $post_excerpt);
						$string_excerpt = str_replace("&hellip;", "", $string_excerpt);
						echo '<div class="product-excerpt"><h5>'.__('Description','pro').'</h5>'. $string_excerpt;
				}
			?>
			<hr>
		</div>
			
			<div class="product-categories">
				<?php $categories = get_the_terms( $post_id , 'product_category' ); ?>
				<?php echo '<span class="cat-in">' .  ((count($categories)>1)? __( 'Categories:', 'pro' ) : __( 'Category:', 'pro' ) ) . '</span>' . pro_load_product_page_category( array( 'post_id' => $post_id ) ); ?>
			</div>				
			<div class="product-categories">
				<?php $pro_tags = get_the_terms( $post_id , 'product_tag' ); ?>
				<?php echo '<span class="cat-in">' .  ((count($pro_tags)>1)? __( 'Tags:', 'pro' ) : __( 'Tag:', 'pro' ) ) . '</span>' . pro_load_product_page_tags( array( 'post_id' => $post_id ) ); ?>
			</div>		
			<?php 

			$proprice_args = array(
				'echo' => false,
				'post_id' => $post_id,
				'label' => false,
				'context' => ''
			);

			if ( pro_product_price( $proprice_args ) != '') : ?>
				<hr>
				<div class="product-pricing">
					<?php echo ' <span class="price">'.__('Price','pro').'</span> ' .  pro_product_price( $proprice_args ); ?>
					
				</div>
			
			<?php endif; ?>
			<div class="product-buy-now align-left" style="overflow: hidden;">
			<hr style="margin: 0px -10px 10px -10px;">
				
				<?php 

					$buybtn_args = array(
						'echo' => true,
						'post_id' => $post_id,
						'btnclass' => $btnclass . ' btn-block',
						'context' => 'single'
					);
					
					pro_buy_button( $buybtn_args ); 
				?>
				
			</div>	    
	    </div>
	</div> <!-- End product-upfold-contents -->
<div class="clear"></div>	

<!-- Content tabs -->
<div class="product-content-tabs">

  <!-- menu -->
    <ul class="nav nav-tabs producttab" id="producttab" data-tabs="tabs">

    <?php
    	$comments_number = get_comments_number( $post_id );
    	//$get_cp_structure = esc_attr( get_option('mpt_pro_page_custom_field_structure') );
		$get_cp_structure = esc_attr( get_blog_option(1,'mpt_pro_page_custom_field_structure') );
    	$cp_customfields_structure = ( !empty( $get_cp_structure ) ? $get_cp_structure : 'table' );
    	//$cp_customfields = ( get_option('mpt_pro_page_show_custom_field') == 'true' ? do_shortcode("[custom_fields_block wrap='{$cp_customfields_structure}']") : '' );
		$cp_customfields = ( get_blog_option(1,'mpt_pro_page_show_custom_field') == 'true' ? do_shortcode("[custom_fields_block wrap='{$cp_customfields_structure}']") : '' );
      	$producttab_links = '<li class="active"><a href="#product-contents" data-toggle="tab">' . __('Product Description', 'pro') . '</a></li>';
      	$producttab_links .= ( !empty($cp_customfields) && pro_check_if_cf_available() ? '<li><a href="#product-extra-info" data-toggle="tab">'.__( 'Additional Info' , 'pro' ).'</a></li>' : '' );
      	//$producttab_links .= ( comments_open() ? '<li><a href="#product-comments" data-toggle="tab">' . __('Comments ', 'pro') . ( !empty($comments_number) ? '(' . esc_attr($comments_number) . ')' : '' ) . '</a></li>' : '' );
		
		$producttab_links .= '<li><a href="#product-reviews" data-toggle="tab">'. __('Ratings &amp; Reviews ', 'pro') .'</a></li>';
		
		
      	echo apply_filters( 'pro_product_page_producttab_links' , $producttab_links , $post_id );
    ?>

    </ul>
     <!-- product description -->
    <div class="tab-content">
        <!-- contents -->
        <div class="tab-pane fade in active" id="product-contents">

        	<?php 
				$post_content = preg_replace("/<img[^>]+\>/i", " ", get_the_content()); 
				$post_content = preg_replace("~(?:\[/?)[^/\]]+/?\]~s", '', $post_content);
        		echo $post_content;
        		edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); 
        	?>

        </div>

      	<!-- Additional Info -->
      	<?php 
      		if ( !empty($cp_customfields) && pro_check_if_cf_available() )
      			echo '<div class="tab-pane fade" id="product-extra-info">' . $cp_customfields . '</div>';
      	 ?>
      	
      	<!-- reviews -->
      	<?php /* if ( comments_open() =='jeff') : ?>
            <div class="tab-pane fade" id="product-comments">
               <?php comments_template(); ?>
            </div>
   	 	<?php endif; */?>
		
        <div class="tab-pane fade" id="product-reviews">
           <?php echo do_shortcode('[WPCR_INSERT]'); ?>
        </div>	

        <?php do_action( 'pro_product_page_custom_product_tabs' , $post_id ); ?>

    </div>

</div>
<!-- End Content Tabs -->


 <?php
    	$comments_number = get_comments_number( $post_id );
    	//$get_cp_structure = esc_attr( get_option('mpt_pro_page_custom_field_structure') );
		$get_cp_structure = esc_attr( get_blog_option(1,'mpt_pro_page_custom_field_structure') );
    	$cp_customfields_structure = ( !empty( $get_cp_structure ) ? $get_cp_structure : 'table' );
    	//$cp_customfields = ( get_option('mpt_pro_page_show_custom_field') == 'true' ? do_shortcode("[custom_fields_block wrap='{$cp_customfields_structure}']") : '' );
		$cp_customfields = ( get_blog_option(1,'mpt_pro_page_show_custom_field') == 'true' ? do_shortcode("[custom_fields_block wrap='{$cp_customfields_structure}']") : '' );
      	$producttab_links = '<li class="active"><a href="#product-contents" data-toggle="tab">' . __('Product Description', 'pro') . '</a></li>';
      	$producttab_links .= ( !empty($cp_customfields) && pro_check_if_cf_available() ? '<li><a href="#product-extra-info" data-toggle="tab">'.__( 'Additional Info' , 'pro' ).'</a></li>' : '' );
      	//$producttab_links .= ( comments_open() ? '<li><a href="#product-comments" data-toggle="tab">' . __('Comments ', 'pro') . ( !empty($comments_number) ? '(' . esc_attr($comments_number) . ')' : '' ) . '</a></li>' : '' );
		
		$producttab_links .= '<li><a href="#product-reviews" data-toggle="tab">'. __('Ratings &amp; Reviews ', 'pro') .'</a></li>';
		
		
      	//echo apply_filters( 'pro_product_page_producttab_links' , $producttab_links , $post_id );
    ?>
<div class="clear"></div>	

<!-- Related Products -->
<?php
	$related_args = array(
		'echo' => true,
		//'querytype' => get_option('mpt_pro_page_related_products_query_type'),
		'querytype' => get_blog_option(1,'mpt_pro_page_related_products_query_type'),
		'current_post' => $post_id,
		'btnclass' => $btnclass,
		'labelcolor' => $labelcolor
	);

	//if ( get_option('mpt_pro_page_enable_related_products_feature') != 'false' ) {
	if ( get_blog_option(1,'mpt_pro_page_enable_related_products_feature') != 'false' ) {

		//if ( is_multisite() && function_exists('pro_load_global_related_products') && get_option('mpt_pro_page_enable_global_related_products') == 'true' )
		if ( is_multisite() && function_exists('pro_load_global_related_products') && get_blog_option(1,'mpt_pro_page_enable_global_related_products') == 'true' )
			pro_load_global_related_products( $related_args );
		else
			pro_load_related_products( $related_args );

		echo '<div class="clear"></div>';
	}
	
	//Booking
	} elseif($mp_is_booking){ ?>
		<div class="events-upfold-contents row-fluid">
		<?php 		
		$booking_date 		= get_post_meta( $post_id , 'mp_booking_date', true );
	    $booking_hours 		= get_post_meta( $post_id , 'mp_booking_hours', true );
		$booking_start = date('Y-m-d H:i:s', strtotime($booking_date) + $booking_hours*60*60);
		
		$start_ended = false;		
		if(!empty($booking_start)){
			if($booking_start > $event_start_mysql ){
				$start_ended = true;
			}
		}	
		?>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="product-title">
					<h3><span><?php the_title();?></span></h3>
					
					<?php 
						global  $mp,$appointments;
					    $booking_name 		= get_post_meta( $post_id , 'mp_booking_name', true );
					    $booking_email 		= get_post_meta( $post_id , 'mp_booking_email', true );
					    $booking_phone 		= get_post_meta( $post_id , 'mp_booking_phone', true );
					    $booking_date 		= get_post_meta( $post_id , 'mp_booking_date', true );
					    $booking_note 		= get_post_meta( $post_id , 'mp_booking_note', true );
					    $booking_hours 		= get_post_meta( $post_id , 'mp_booking_hours', true );
					    $booking_id 		= get_post_meta( $post_id , 'mp_booking_id', true );
					    $booking_address = get_post_meta( $post_id , 'mp_booking_address', true );
						$booking_city = get_post_meta( $post_id , 'mp_booking_city', true );
						$booking_state = get_post_meta( $post_id , 'mp_booking_state', true );
						$booking_country = get_post_meta( $post_id , 'mp_booking_country', true );	
						
							
						$booking_helper_text = '';		
						$booking_button_disabled = '';
						$booking_button_text = __('Confirm Booking','pro');
						$cart = $mp->get_cart_contents();
						if (isset($cart[$post_id]) && is_array($cart[$post_id])) {
							$booking_button_disabled = 'mppf-btn-disabled';
							$booking_helper_text = __('Booking is already in your cart please pay as soon as posible.','pro');	
							$booking_button_text = __('Booking Confirmed','pro');
						}	
						
						if(!empty($booking_id)){
							$app_helper = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$appointments->app_table} WHERE ID=%d", $booking_id));
							if($app_helper != null) {
								if($app_helper->status == 'completed'){
									$booking_helper_text = __('Booking is Paid.','pro');
									$booking_button_text = __('Paid','pro');
									$booking_button_disabled = 'mppf-btn-disabled';
								}					
							}								
						}					
					
					?>
					<hr>
					<div class="product-categories">
					<p><span class="cat-in"><?php _e('Name','pro'); ?>: <?php echo $booking_name; ?></span></p>
					<p><span class="cat-in"><?php _e('Email','pro'); ?>: <?php echo $booking_email; ?></span></p> 
					<p><span class="cat-in"><?php _e('Phone','pro'); ?>: <?php echo $booking_phone; ?></span></p> 
					<p><span class="cat-in"><?php _e('Address','pro'); ?>: <?php echo $booking_address; ?></span></p> 
					<p><span class="cat-in"><?php _e('City','pro'); ?>: <?php echo $booking_city; ?></span></p> 
					<p><span class="cat-in"><?php _e('State','pro'); ?>: <?php echo $booking_state; ?></span></p>					
					<p><span class="cat-in"><?php _e('Country','pro'); ?>: <?php echo $booking_country; ?></span></p> 					
					<p><span class="cat-in"><?php _e('Booking Date','pro'); ?>: <?php echo date_i18n("F d, Y H:i A", strtotime($booking_date)); ?></span></p> 	
					<p><span class="cat-in"><?php _e('Note','pro'); ?>: <?php echo $booking_note; ?></span></p> 					
					<p><span class="cat-in"><?php _e('Hours','pro'); ?>: <?php echo $booking_hours; ?></span></p>
					<div class="product-pricing" style="padding:0px"><span class="cat-in"><?php _e('Price','pro'); ?>: <?php echo pro_product_price( array('echo' => false,'post_id' => $post_id,'label' => false,'context' => '') ); ?></span></div>	
					</div>
					<hr>
					<?php if($start_ended) { ?>
					<div class="product-buy-now align-center" style="overflow: hidden;">
						<div class="booking-msg-helper"><?php echo $booking_helper_text;?></div>
						<div id="mp-premium-features" class="mppf-container mppf-container-single mppf-pro-integration mppf-container-block" style="text-align:center">
							<form class="mppf_booking_form">
							<input type="hidden" name="variation" value="0"/>	
							<input type="hidden" name="product_id" value="<?php echo $post_id;?>"/>
							<input type="hidden" name="action" value="mppf-update-cart">
							<input type="hidden" name="quantity" value="1">
							<a id="booking-add-button" class="col-md-4 mppf-btn mppf-btn-black mppf-addtowishlist-btn <?php echo $booking_button_disabled;?>"><span class="mppf-btn-text mppf-show"><?php echo $booking_button_text; ?></span></a>
							</form>
						</div>
					</div>
					<?php } else { ?>
						<div class="product-buy-now align-center" style="overflow: hidden;">
							<div id="mp-premium-features" class="mppf-container mppf-container-single mppf-pro-integration mppf-container-block" style="text-align:center">
								<button class="col-md-4 mppf-btn mppf-btn-black mppf-addtowishlist-btn"><span class="mppf-btn-text mppf-show"><?php _e('Booking Expired','pro'); ?></span></button>
							</div>
						</div>						
					<?php  } ?>
				</div>
			</div>
		</div>
	<?php
	/// Event
	} else { ?>
		<div class="events-upfold-contents row-fluid">
			<div class="col-md-7 col-sm-12 col-xs-12">
			<?php 
				$event_date_start = get_post_meta( $post_id , 'mp_event_start', true );
				$event_date_end = get_post_meta( $post_id , 'mp_event_end', true );
				if ( has_post_thumbnail($post_id) ) { 
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'tb-600' );
					echo '<img src="'.$image[0].'" alt=""/>';
				} 
				
		$start_ended = false;		
		$end_ended = false;			
		if(!empty($event_date_start) && empty($event_date_end)){
			 $date_start_e = date('Y-m-d H:i:s', strtotime($event_date_start));
			if($date_start_e > $event_start_mysql ){
				$start_ended = true;
			}
		} else if( (!empty($event_date_start) && !empty($event_date_end)) || (empty($event_date_start) && !empty($event_date_end))) {
			 $date_end_e = date('Y-m-d H:i:s', strtotime($event_date_end));
			if($newdate > $event_date_start ){
				$end_ended = true;
			}
				
		}
				echo  $date_end_e;
				
				
				?>
				<div>
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-md-5 col-sm-12 col-xs-12">
				<div class="product-title">
					<h3><span><?php the_title();?></span></h3>
					
					<?php 
					    $event_address = get_post_meta( $post_id , 'mp_event_address', true );
						$event_city = get_post_meta( $post_id , 'mp_event_city', true );
						$event_state = get_post_meta( $post_id , 'mp_event_state', true );
						$event_country = get_post_meta( $post_id , 'mp_event_country', true );
						
						$address = array();
						if(!empty($event_address) && $event_address != ' ')
							$address[] = $event_address;
						if(!empty($event_city) && $event_city != ' ')	
							$address[] = $event_city;
						if(!empty($event_state) && $event_state != ' ' )
							$address[] = $event_state;
						if(!empty($event_country) && $event_country != ' '){
							$country_text = TP_CHelper::get_country_text($event_country,ICL_LANGUAGE_CODE);
							if($country_text){
								$address[] = $country_text;
							}	
						}	
						
						if(count($address))
							$event_location = implode(', ',$address);
						else
							$event_location = '';	

					
					?>
					<?php if(!empty($event_location)) { ?>
					<hr>
					<div class="product-categories">
						<span class="cat-in"><?php _e('Location','pro'); ?>: <?php echo $event_location; ?></span> 
					</div>
					<?php } ?>
					<hr>
					<div class="product-categories">
						<?php if(!empty($event_date_start)) { ?>
							<span class="cat-in"><?php _e('Date Start','pro'); ?>: <?php echo date_i18n("F d, Y", strtotime($event_date_start)); ?></span> 
							<hr>
							<span class="cat-in"><?php _e('Time Start','pro'); ?>: <?php echo date_i18n("H:i A", strtotime($event_date_start)); ?></span> 
						<?php } ?>
						
						<?php if(!empty($event_date_end)) { ?>
								<hr>
								<span class="cat-in"><?php _e('Date End','pro'); ?>: <?php echo date_i18n("F d, Y", strtotime($event_date_end)); ?></span> 
								<hr>
								<span class="cat-in"><?php _e('Time End','pro'); ?>: <?php echo date_i18n("H:i A", strtotime($event_date_end)); ?></span> 
						<?php } ?>						
					</div>
					<hr>
					<?php if( (!$start_ended && empty($event_date_end)) || !$end_ended) { ?>
					<div class="product-buy-now align-center" style="overflow: hidden;">
						<div id="mp-premium-features" class="mppf-container mppf-container-single mppf-pro-integration mppf-container-block" style="text-align:center">
							<a href="<?php echo $store_quote_slug; ?>" class="col-md-4 mppf-btn mppf-btn-black mppf-addtowishlist-btn"><span class="mppf-btn-text mppf-show"><?php _e('Book Now','pro'); ?></span></a>
						</div>
					</div>
					<?php } else { ?>
						<div class="product-buy-now align-center" style="overflow: hidden;">
							<div id="mp-premium-features" class="mppf-container mppf-container-single mppf-pro-integration mppf-container-block" style="text-align:center">
								<button class="col-md-4 mppf-btn mppf-btn-black mppf-addtowishlist-btn"><span class="mppf-btn-text mppf-show"><?php _e('Event Ended','pro'); ?></span></button>
							</div>
						</div>						
					<?php  } ?>
					<?php 
						global $mppf_socialsharing;
						echo $mppf_socialsharing->load_social_sharing( array('post_id' => $post_id));
					?>
				</div>
			</div>
		</div>
	<?php }	
	mpt_load_bottom_code();