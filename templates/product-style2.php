<?php global $mp, $mppf;
/**
 * Single Product Page Template - Style 2
 */

	$post_id = get_the_ID();
	$pinit_setting = $mp->get_setting('social->pinterest->show_pinit_button');
	$btnclass = mpt_load_mp_btn_color();
	$tagcolor = mpt_load_icontag_color();
	$labelcolor = mpt_load_mp_label_color();
	$meta_prefix = '_mpt_';
	$contenttextcolor = esc_attr(get_post_meta( $post_id , $meta_prefix . 'post_content_text_color', true ));
?>

<!-- ___________________ Product title & price box __________________ -->

<div class="row-fluid">

	<div class="span9">
		<div class="product-page-title-container">
			<h1 class="product-page-title"<?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>><?php the_title(); ?></h1>
			<div class="product-page-tags">
				<?php echo pro_load_product_page_tags( array( 'post_id' => $post_id ) ); ?>
			</div>
		</div><!-- End product-page-title-container -->
	</div><!-- End span -->

	<div class="span3">
		<div class="product-page-price-box">
			<?php echo apply_filters( 'pro_product_page_price_box_title' , '<div class="product-page-price-box-title">' . __( 'Price' , 'pro' ) . '</div>' , $post_id ); ?>
			<h3 class="product-page-price">
				<?php echo pro_product_price( array( 'echo' => false , 'post_id' => $post_id , 'label' => false , 'context' => 'widget' ) ); ?>
			</h3>
		</div><!-- End product-page-price-box -->
	</div><!-- End span -->

</div><!-- End row-fluid -->

<!-- ___________________ Product images __________________ -->

<?php 

	mpt_load_top_code();

	///$default_img_url = esc_url( get_option('mpt_mp_default_product_img') );
	$default_img_url = esc_url( get_blog_option(1,'mpt_mp_default_product_img') );
	$default_img_id = ( !empty($default_img_url) ? intval( get_image_id( $default_img_url ) ) : NULL );

	if ( has_post_thumbnail( $post_id ) || ( !empty($default_img_id) && is_numeric($default_img_id) ) ) {
		echo  '<div class="product-page-image-container">' . pro_product_page_load_images( array( 
				'post_id' => $post_id, 
				'default_img_id' => $default_img_id,
				'carouselitemwidth' => apply_filters( 'pro_product_page_carousel_item_width' , 100 , $post_id ),
				'carouselitemmargin' => 10,
				) ) . '</div>';
		echo '<div class="clear"></div>';
	}
?>

	<!-- ___________________ CTA section __________________ -->
	<?php 
		if ( class_exists('MPPremiumFeatures') ) {
			echo '<div class="product-page-cta-section">';
				echo '<span class="product-cta-title">' . apply_filters( 'pro_product_page_cta_title' , __( 'Buy Now' , 'pro' ) , $post_id ) . '</span>';
				echo $mppf->load_buy_options( array( 
						'integration' => 'pro', 
						'post_id' => $post_id, 
						'context' => 'single', 
						'containerclass' => ' align-left', 
						'btnclass' => $mppf->get_btn_color(),
						'layout' => 'single'
					) );
			echo '</div>';			
		}
	?>

<div class="row-fluid">

	<!-- ___________________ Contents Tab __________________ -->

	<div class="span12">

		<?php
    		$comments_number = get_comments_number( $post_id );
    		//$get_cp_structure = esc_attr( get_option('mpt_pro_page_custom_field_structure') );
			$get_cp_structure = esc_attr( get_blog_option(1,'mpt_pro_page_custom_field_structure') );
    		$cp_customfields_structure = ( !empty( $get_cp_structure ) ? $get_cp_structure : 'table' );
    		//$cp_customfields = ( get_option('mpt_pro_page_show_custom_field') == 'true' ? do_shortcode("[custom_fields_block wrap='{$cp_customfields_structure}']") : '' );
			$cp_customfields = ( get_blog_option(1,'mpt_pro_page_show_custom_field') == 'true' ? do_shortcode("[custom_fields_block wrap='{$cp_customfields_structure}']") : '' );
		?>

		<div class="accordion product-page-accordion" id="product-accordion">

			<!-- product description -->
			<div class="accordion-group product-page-accordion-desc">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#product-accordion" href="#product-desc">
						<?php _e( 'Description' , 'pro' ); ?><i class="icon-caret-down"></i>
					</a>
				</div><!-- End accordion-heading -->
				<div id="product-desc" class="accordion-body collapse in">
					<div class="accordion-inner">
        			<?php the_content(); ?>
					</div><!-- End accordion-inner -->
				</div><!-- End accordion-body -->
			</div><!-- End accordion-group -->

			<!-- Custom Press -->
			<?php if ( !empty($cp_customfields) && pro_check_if_cf_available() ) : ?>
				<div class="accordion-group product-page-accordion-custompress">
					<div class="accordion-heading">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#product-accordion" href="#custompress">
							<?php _e( 'Additional Info' , 'pro' ); ?><i class="icon-caret-down"></i>
						</a>
					</div><!-- End accordion-heading -->
					<div id="custompress" class="accordion-body collapse">
						<div class="accordion-inner">
							<?php echo $cp_customfields; ?>
						</div><!-- End accordion-inner -->
					</div><!-- End accordion-body -->
				</div><!-- End accordion-group -->
			<?php endif; ?>

			<!-- Comments -->
			<?php if ( comments_open() ) : ?>
				<div class="accordion-group product-page-accordion-comments">
					<div class="accordion-heading">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#product-accordion" href="#comments">
							<?php echo __( 'Comments' , 'pro' ) . ( !empty($comments_number) ? '<span class="product-comment-numbers">' . esc_attr($comments_number) . '</span>' : '' ); ?><i class="icon-caret-down"></i>
						</a>
					</div><!-- End accordion-heading -->
					<div id="comments" class="accordion-body collapse">
						<div class="accordion-inner">
							<?php comments_template(); ?>
						</div><!-- End accordion-inner -->
					</div><!-- End accordion-body -->
				</div><!-- End accordion-group -->
			<?php endif; ?>

			<?php do_action( 'pro_product_page_custom_accordion' , $post_id , 'style-2' ); ?>

		</div><!-- End accordion -->

	</div><!-- End span -->

</div><!-- End row-fluid -->

<div class="clear"></div>

<!-- ___________________ Related Products __________________ -->

<?php
	$related_args = array(
		'echo' => true,
		//'querytype' => get_option('mpt_pro_page_related_products_query_type'),
		'querytype' => get_blog_option(1,'mpt_pro_page_related_products_query_type'),
		'current_post' => $post_id,
		'btnclass' => $btnclass,
		'labelcolor' => $labelcolor
	);

	//if ( get_option('mpt_pro_page_enable_related_products_feature') != 'false' ) {
	if ( get_blog_option(1,'mpt_pro_page_enable_related_products_feature') != 'false' ) {

		//if ( is_multisite() && function_exists('pro_load_global_related_products') && get_option('mpt_pro_page_enable_global_related_products') == 'true' )
		if ( is_multisite() && function_exists('pro_load_global_related_products') && get_blog_option(1,'mpt_pro_page_enable_global_related_products') == 'true' )
			pro_load_global_related_products( $related_args );
		else
			pro_load_related_products( $related_args );

		echo '<div class="clear"></div>';
	}
		
	mpt_load_bottom_code();