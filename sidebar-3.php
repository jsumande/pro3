  	<?php do_action('sidebar-begin'); ?>
	    <?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
	    	<?php dynamic_sidebar( 'sidebar-3' ); ?>
		<?php else : ?>
	      	<!-- Sidebar Widget (Default)-->
	      	<div class="sidebar-widget">
	        	<h4>Sidebar Widget</h4>
	        	<p>Go to Appearance > Widget to add sidebar widget into this area.</p>
	      	</div>
	    <?php endif; ?>
  	<?php do_action('sidebar-end'); ?>