<?php get_header(); ?>
	<!-- Page -->
	<div id="page-wrapper">
		<div class="content-section">
			<div class="outercontainer">
				<div class="container">
					<div class="row-fluid">
						<div class="span12">					
							<div <?php post_class(); ?>>
								<div class="clear padding10"></div>
							</div>
								<div class="page-content">
									<?php 
									
									if(!empty($store_policy_content))
										echo $store_policy_content; 
									else
										_e('This store has not added Store Policy','trtm');
									
									?>
								</div>	
							</div>
						</div>						
						</div><!-- / span8 -->						
					</div><!-- / row-fluid -->
				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	
	</div><!-- / page-wrapper -->
<?php get_template_part('footer', 'widget'); ?>
<?php get_footer(); ?>