<?php get_header(); ?>

<?php 
	$btnclass = mpt_load_mp_btn_color();
	$iconclass = mpt_load_whiteicon_in_btn();
	//$display_sidebar = get_option('mpt_wp_post_display_sidebar');
	//$sidebar_position = get_option('mpt_wp_post_sidebar_position');
	$display_sidebar = get_blog_option(1,'mpt_wp_post_display_sidebar');
	$sidebar_position = get_blog_option(1,'mpt_wp_post_sidebar_position');	
	$selected_sidebar = mpt_load_selected_sidebar('post');
?>
	<!-- Post -->
	<div id="post-wrapper">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php
						$contentbgcolor = esc_attr(get_post_meta( $post->ID, '_mpt_post_content_bg_color', true ));
						$contenttextcolor = esc_attr(get_post_meta( $post->ID, '_mpt_post_content_text_color', true ));
					?>

		<div class="content-section"<?php echo ($contentbgcolor != '#' && !empty($contentbgcolor) ? ' style="background: '.$contentbgcolor.';"' : '') ?>>
			<div class="outercontainer">
				<div class="container">					
					<div class="row no-margin">						
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div id="breadcrumb">
								<div class="row">
									 <div class="col-md-12">
										<ul class="breadcrumbs-list">
											<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
											<?php if(get_post_type( get_the_ID() ) == 'post') echo '<li><a href="'. $blogs_link .'">'.__('Blog').'</a></li>'; ?>
											<li class="active"><?php the_title(); ?></li>
										</ul>
									 </div>
								</div>
							</div>
							<div class="clear padding10"></div>
							<div class="row">
							<?php if ( empty($sidebar_position) || ( $display_sidebar == 'true' && $sidebar_position == 'Left' ) ) : ?>
								<div id="sidebar" class="col-md-3">
									<?php get_sidebar( '2' ); ?>
								</div>
							<?php endif; ?>
							<div class="<?php echo ( $display_sidebar == 'true' || empty($display_sidebar) ? 'col-md-9' : 'col-md-12' ); ?>">
								<div id="post-<?php the_ID(); ?>" <?php post_class(); ?><?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>>
									<div class="row">
										<div class="col-md-12">
											<div class="post-heading">										
												<h3 style="margin-top: -4px">
												<span<?php echo ($contenttextcolor != '#' && !empty($contenttextcolor) ? ' style="color: '.$contenttextcolor.';"' : '') ?>><?php the_title(); ?></span>
												</h3>
												
												<div class="row">
													<div class="col-md-6 author-line">
													<?php _e( 'Posted By ', 'pro' ); ?><a href="#"><?php the_author(); ?></a> <?php /* _e( 'In', 'pro' ); ?> <span class="label label-tag"><?php the_category('</span> <span class="label label-tag">'); ?></span> <?php the_tags( __( 'Tagged with ', 'pro' ).'<span class="label label-tag">' , '</span> <span class="label label-tag">' , '</span>'); */
												echo "<p>" . get_the_date( 'j F Y', $post->ID ) . "</p>";
												?>												
													</div>
													<div class="col-md-6 text-right">
															<a href="<?php comments_link(); ?>"><?php comments_number( __('No Comments','pro') , __('1 Comment','pro') , __('% Comments','pro') ); ?></a>														
													</div>
												</div>
																							
													<div class="clear padding10"></div>
													<div class="row" style="text-align: justify;">
														<div class="col-md-12">
														<?php $temp = get_post_meta( $post->ID, '_mpt_post_select_temp', true ); ?>
																<?php if (has_post_thumbnail( $post->ID ) || $temp == 'video' ) : ?>

																<?php
																/*
																	$id = get_the_ID(); 
																	$featured_args = array(
																		'echo' => true,
																		'post_id' => $id,
																		'content_type' => 'single',
																		'prettyphoto' => true,
																		'imagesize' => 'tb-860',
																		'videoheight' => 300,
																		'btnclass' => '',
																		'iconclass' => '',
																	);

																	if ($temp == 'image-carousel') {
																		mpt_load_image_carousel( $featured_args );
																	} else if ($temp == 'video') {
																		mpt_load_video_post( $featured_args );
																	} else {
																		mpt_load_featured_image( $featured_args );
																	}
																*/	
																?>

																<?php endif; ?>			
																	<?php the_content(); ?>
																<?php // edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>									
														</div>
														<div class="col-md-7">
														<?php //the_content();?>
														</div>											
													</div>
											</div>								
											<!-- <div class="post-meta">
												<?php _e( 'Posted By ', 'pro' ); ?><?php the_author(); ?> <?php _e( 'In', 'pro' ); ?> <span class="label label-tag"><?php the_category('</span> <span class="label label-tag">'); ?></span> <?php the_tags( __( 'Tagged with ', 'pro' ).'<span class="label label-tag">' , '</span> <span class="label label-tag">' , '</span>'); ?>
											</div> -->
										</div>
										<!--
										<div class="span2">
											<div class="date-comment-box">
												<?php 
													$year  = get_the_time('Y'); 
													$month = get_the_time('M'); 
													$day   = get_the_time('j'); 
												?>
												<div class="date">
													<span class="month"><?php echo $month; ?></span>
													<div class="clear"></div>
													<span class="day"><?php echo $day; ?></span>
													<div class="clear"></div>
													<span class="year"><?php echo $year; ?></span>
												</div>
												<div class="comments"><a href="<?php comments_link(); ?>"><?php comments_number( __('No Comments','pro') , __('1 Comment','pro') , __('% Comments','pro') ); ?></a></div>
											</div>
										</div>-->
									</div>
									<?php mpt_load_top_code(); ?>								

										<div class="clear padding10"></div>
										<!--	<div class="post-content">
											<div class="row">
												<?php ///the_content(); ?>
											</div>
											<?php  // edit_post_link( __('Edit this entry.','pro') , '<p class="margin-vertical-15">', '</p>'); ?>
										</div> -->
										<?php comments_template(); ?>

								</div>

						<?php endwhile; endif; ?>

							<?php mpt_load_bottom_code(); ?>
							
							</div><!-- /  -->
							</div><!-- /  -->

							<?php if ( !empty($sidebar_position) && $display_sidebar == 'true' && $sidebar_position == 'Right' ) : ?>
								<div id="sidebar" class="col-md-4">
									<?php get_sidebar( $selected_sidebar ); ?>
								</div>
							<?php endif; ?>

					</div><!-- / col-md-12 col-sm-12 col-xs-12 -->
					</div><!-- / row no margin-->

				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->
	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>