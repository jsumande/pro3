<?php get_header(); ?>

<?php 
	$btnclass = mpt_load_mp_btn_color();
	$iconclass = mpt_load_whiteicon_in_btn();
?>

	<!-- Page -->
	<div id="page-wrapper">

		<div class="content-section">
			<div class="outercontainer">
				<div class="container" style="min-height: 450px;">
					<div class="row no-margin">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div id="breadcrumb">
								<div class="row">
									 <div class="col-md-12">
										<ul class="breadcrumbs-list">
											<li><a href="<?php echo get_home_url(); ?>"><?php _e('Home', 'pro'); ?></a></li>
											<li class="active"><?php _e('Marketplace Product Tags' , 'pro' ); ?></li>
										</ul>
									 </div>
								</div>
							</div>

							<div class="clear padding10"></div>

							<?php if ( class_exists( 'MarketPress' ) ) { ?>

								<?php pro_load_global_taxonomies_listing( array( 'echo' => true , 'taxonomy' => 'product_tag' , 'btnclass' => $btnclass , 'iconclass' => $iconclass ) ) ?>

							<?php } ?>

						</div><!-- / col-md-s -->
					</div><!-- / row no-margin -->
				</div><!-- / container -->
			</div><!-- / outercontainer -->	
		</div><!-- / content-section -->	

	</div><!-- / page-wrapper -->

<?php get_template_part('footer', 'widget'); ?>

<?php get_footer(); ?>