	<!-- Load JS -->
	<script type="text/javascript">
	jQuery(document).ready(function () {
    	// tooltip
    	jQuery('.tool-tip').tooltip();

		jQuery("#header-wrapper .nav-collapse.collapse .nav li.dropdown").hover(function() {
		    var navMenu = jQuery(this).find("#header-wrapper .nav-collapse.collapse");
		    navMenu.toggleClass("in");
		});

		<?php if ( get_blog_option(1,'mpt_header_section_main_style') == 'Style 3' ) { ?> 
			jQuery("#main-nav-menu").tinyNav({
				active: 'active'
			});
		<?php } ?>

	<?php if (is_singular('product')) { ?>	
		//Product Content Tabs
		jQuery('.producttab').tab();

		<?php if ( get_blog_option(1,'mpt_pro_page_enable_related_products_feature') != 'false' ) { 
			$carousel_speed = intval( get_blog_option(1,'mpt_pro_page_related_products_interval') );
			?>
			//Related Products carousel
			jQuery('#related-products-holder').carousel({
				interval: <?php echo ( is_numeric($carousel_speed) ? $carousel_speed : 10000 ); ?>
			});
		<?php } ?>
	<?php } ?>
	<?php if (get_blog_option(1,'mpt_enable_sticky_header') == 'true') { ?>
    	//Waypoints
		jQuery.waypoints.settings.scrollThrottle = 30;
		jQuery('#body-wrapper').waypoint(function(event, direction) {
			offset: '-100%'
		}).find('#menu-wrapper').waypoint(function(event, direction) {
			jQuery(this).toggleClass('navbar-fixed-top', direction === "down");
			jQuery('#header-wrapper .navbar-inner').toggleClass('zero-border-radius');
			event.stopPropagation();
		});
	<?php } ?>
	});
	</script>