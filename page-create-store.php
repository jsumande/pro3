<?php
/*
Template Name: Register Store
*/
/** Sets up the WordPress Environment. */
add_action( 'wp_head', 'signuppageheaders' ) ;

if ( is_array( get_site_option( 'illegal_names' )) && isset( $_GET[ 'new' ] ) && in_array( $_GET[ 'new' ], get_site_option( 'illegal_names' ) ) == true ) {
	wp_redirect( network_home_url() );
	die();
}

function do_signup_header() {
	do_action( 'signup_header' );
}

add_action( 'wp_head', 'do_signup_header' );
function signuppageheaders() {
	echo "<meta name='robots' content='noindex,nofollow' />\n";
}

if ( !is_multisite() ) {
	wp_redirect( site_url('/user-register/') );
	die();
}

if ( !is_main_site() ) {
	wp_redirect( network_home_url( 'signup' ) );
	die();
}
// Fix for page title
$wp_query->is_404 = false;



do_action( 'before_signup_form' );

include 'templates/store-creation-functions.php';

$cstep = isset($_GET['step'])? $_GET['step'] : '';

$new_store_create = false;
$mystore = '';
if(is_user_logged_in()) {
	$user_ID = get_current_user_id();
	$mystore = get_my_store_id();	
	if(!$mystore){
		$new_store_create = true;
	} else {
		wp_redirect(get_site_url($mystore));
		exit;
	}	
}

$reg_step_2 = false;
$reg_step_3 = false;
if(empty($cstep) && !$mystore) {
	$reg_step_2 = false; 
	$reg_step_3 = false;
} else if($cstep == 'validate-blog-signup' || $cstep == 'signup-blog-form') {
	$reg_step_2 = true; 	
} else if($cstep == 'validate-profile-signup' || $cstep == 'validate-final-registration' || $cstep == 'signup-profile-form') {
	$reg_step_3 = true;
}	

$cur_url = esc_url(get_permalink(get_the_ID()));

get_header(); 

?>
<!-- Page -->
<div id="page-wrapper">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="content-section">
		<div class="outercontainer">
			<div class="container">
				<div <?php post_class(); ?>>
					<div class="post-content custom-page">
						<div class="page-content">
							<div class="row">
								<div class="register-page-inner">
									<p class="register-page-title"><?php _e('REGISTER YOUR STORE NOW WITH JUST 3 EASY STEPS','pro');?></p>
									<div class="register-page-steps">
										<div class="steps-names">										
											<span<?php if(!$reg_step_2 AND !$reg_step_3 AND !$new_store_create) echo ' class="stepn-active"'; ?>><?php _e('1. Create Account','pro'); ?></span>
											<span class="stepn-arrow">&nbsp;</span>
											<span <?php if($reg_step_2 || ($new_store_create AND !$reg_step_3)) echo ' class="stepn-active"';?>><?php _e('2. Create Store','pro'); ?></span>
											<span class="stepn-arrow">&nbsp;</span>
											<span<?php if($reg_step_3) echo' class="stepn-active"';?>><?php _e('3. Create Profile','pro'); ?></span>
										</div>
									</div>		
									
									<?php

										// Main
										$active_signup = get_site_option( 'registration' );
										if ( !$active_signup )
											$active_signup = 'all';
										$active_signup = apply_filters( 'wpmu_active_signup', $active_signup ); // return "all", "none", "blog" or "user"

										// Make the signup type translatable.
										$i18n_signup['all'] = _x('all', 'Multisite active signup type');
										$i18n_signup['none'] = _x('none', 'Multisite active signup type');
										$i18n_signup['blog'] = _x('blog', 'Multisite active signup type');
										$i18n_signup['user'] = _x('user', 'Multisite active signup type');
										if ( is_super_admin() )
											echo sprintf( __( '<div class="alert alert-block alert-info"><button type="button" class="close" data-dismiss="alert">×</button><h4>Greetings Site Administrator!</h4> You are currently allowing &#8220;%s&#8221; registrations. To change or disable registration go to your <a href="%s">Options page</a>.</div>','pro' ), $i18n_signup[$active_signup], esc_url( network_admin_url( 'settings.php' ) ) );
										$newblogname = isset($_GET['new']) ? strtolower(preg_replace('/^-|-$|[^-a-zA-Z0-9]/', '', $_GET['new'])) : null;
										$current_user = wp_get_current_user();
										
										if ( $active_signup == 'none' ) { // Notice Message for Existing Store
											_e( '<h3>Looks like you already registered your Store site. <br/><br/>If you don&#8217;t remember the URL or your access credentials, check you email inbox for the confirmation email. <br/><br/> If you still have probelms, contact us through the contact form.</h3>','pro' );
										} elseif ( $active_signup == 'blog' && !is_user_logged_in() ) { // Change http
											if ( is_ssl() )
												$proto = 'https://';
											else 
												$proto = 'http://';
											$login_url = get_site_url().'/login';
											echo sprintf( __( 'You must first <a class="show" href="%s">log in</a>, and then you can create a new site.','pro' ), $login_url );
										} else { // Signup Process
											//$stage = isset( $_POST['stage'] ) ?  $_POST['stage'] : 'default';
											$stage = isset( $_GET['step'] ) ?  $_GET['step'] : 'default';
											switch ( $stage ) {
												case 'validate-user-signup' : // User Signup
													if ( $active_signup == 'all' || $_POST[ 'signup_for' ] == 'blog' && $active_signup == 'blog' || $_POST[ 'signup_for' ] == 'user' && $active_signup == 'user' )
														validate_user_signup();
													else
														_e( 'User registration has been disabled.','pro' );
												break;
												case 'signup-blog-form': // Store Creation
													if ( $active_signup == 'all' || $active_signup == 'blog' ){
														
														//Check Cookie if Exist
														if(isset($_COOKIE['ssinfo']) && select_registration_data($_COOKIE['ssinfo'])){
															$data2 = select_registration_data($_COOKIE['ssinfo']);
															$data = stripslashes($data2['reg_data']);
															$data = json_decode($data, true);	
																
																if(!empty($data['username']) && !empty($data['useremail'])) {
																	$_POST['user_name'] = $data['username'];
																	$_POST['user_email'] = $data['useremail'];
																} else {
																	wp_safe_redirect($cur_url); 
																	exit;
																}	
																
															} else {
																wp_safe_redirect($cur_url); 
																exit;
															}
														signup_blog($_POST['user_name'], $_POST['user_email']);
													} else
														_e( 'Site registration has been disabled.','pro' );
													break;												
												case 'validate-blog-signup': // Store Creation
													if ( $active_signup == 'all' || $active_signup == 'blog' )
														validate_blog_signup(); 
													else
														_e( 'Site registration has been disabled.','pro' );
													break;
												case 'signup-profile-form': // Profile Creation
													if ( $active_signup == 'all' || $active_signup == 'blog' ){	

														//Check Cookie if Exist
														if(isset($_COOKIE['ssinfo']) && select_registration_data($_COOKIE['ssinfo'])){
															$data2 = select_registration_data($_COOKIE['ssinfo']);
															$data = stripslashes($data2['reg_data']);
															$data = json_decode($data, true);	
																if(is_user_logged_in()){
																	$current_user 	= wp_get_current_user();
																	$data['username'] 	= 	$current_user->user_login;
																	$data['useremail'] 	= 	$current_user->user_email;	
																}		
																if(!empty($data['username']) && !empty($data['useremail']) && !empty($data['userblogname']) && !empty($data2['store_title']) && !empty($data['userblogcat']) && !empty($data2['store_desc'])) {
																	
																	if(is_user_logged_in() == false) {
																		$_POST['user_name']	 	= $data['username'];
																		$_POST['user_email'] 	= $data['useremail'];
																	} else {
																		$_POST['user_name'] 	= 	$current_user->user_login;
																		$_POST['user_email'] 	= 	$current_user->user_email;	
																	}
																	$_POST['blogname']  = $data['userblogname'];
																	$_POST['blog_title'] = $data2['store_title'];
																	$_POST['bcat_site_description'] = $data2['store_desc'];
																		
																	if(!empty($datas['userblogcat']))
																		$_POST['bcat_site_categories'] = explode(',',$datas['userblogcat']);
																	else
																		$_POST['bcat_site_categories'] = '';
																	
																} else {
																	wp_safe_redirect($cur_url.'?step=signup-blog-form');
																	exit;
																}	
																
															} else {
																wp_safe_redirect($cur_url.'?step=signup-blog-form'); 
																exit;
															}
														$errors = '';
														signup_profile($_POST['user_name'], $_POST['user_email'], $_POST['blogname'], $_POST['blog_title'], $_POST['bcat_site_categories'], $_POST['bcat_site_description'], $errors);
														
													} else
														_e( 'Please try again.' );
													break;													
												case 'validate-profile-signup': // Profile Creation
													if ( $active_signup == 'all' || $active_signup == 'blog' )				
														validate_profile_signup();
													else
														_e( 'Please try again.' );
													break;
												case 'validate-final-registration': // Final Submit
													if ( $active_signup == 'all' || $active_signup == 'blog' )				
														validate_final_registration();
													else
														_e( 'Please try again.','pro' );
													break;
												case 'default':
												default :
													$user_email = isset( $_POST[ 'user_email' ] ) ? $_POST[ 'user_email' ] : '';
													do_action( 'preprocess_signup_form' ); // populate the form from invites, elsewhere?
													if ( is_user_logged_in() && ( $active_signup == 'all' || $active_signup == 'blog' ) )
														signup_another_blog($newblogname);
													elseif ( is_user_logged_in() == false && ( $active_signup == 'all' || $active_signup == 'user' ) )
														signup_user( $newblogname, $user_email );
													elseif ( is_user_logged_in() == false && ( $active_signup == 'blog' ) )
														echo error_style_message(__('Sorry, new registrations are not allowed at this time.','pro'));
													else
														echo error_style_message(__( '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>You are logged in already. No need to register again!</div>','pro' ));
														
														
												if ( $newblogname ) {	
														$newblog = get_blogaddress_by_name( $newblogname );
													if ( $active_signup == 'blog' || $active_signup == 'all' )
															printf( error_style_message(__('The site you were looking for, <strong>%s</strong> does not exist, but you can create it now!','pro')), $newblog );
														else
															printf(error_style_message(__('The site you were looking for, <strong>%s</strong>, does not exist.','pro')), $newblog );
													}
													break;
											}
										}									
									
									?>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- / container -->
		</div><!-- / outercontainer -->	
	</div><!-- / content-section -->				
<?php endwhile; endif; ?>
	</div><!-- / page-wrapper -->
<?php //get_sidebar(); ?>	
<?php do_action( 'after_signup_form' ); ?>
<?php get_template_part('footer', 'widget'); ?>
<?php get_footer() ?>